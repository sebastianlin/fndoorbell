package com.fusionnextinc.doorbell.fragement;

import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.TextView;

import androidx.appcompat.app.AlertDialog;

import com.fusionnext.cloud.iot.objects.THINGSTATUS;
import com.fusionnextinc.doorbell.Adapter.ThingsListAdapter;
import com.fusionnextinc.doorbell.DefaultAppConfig;
import com.fusionnextinc.doorbell.MyApplication;
import com.fusionnextinc.doorbell.R;
import com.fusionnextinc.doorbell.manager.data.AppDataBase;
import com.fusionnextinc.doorbell.manager.data.AppDataHandler;
import com.fusionnextinc.doorbell.manager.data.AppDataThing;
import com.fusionnextinc.doorbell.manager.data.ListMap;
import com.fusionnextinc.doorbell.manager.mqtt.MqttManager;
import com.fusionnextinc.doorbell.manager.smartConnection.SmartConnectionManager;
import com.fusionnextinc.doorbell.manager.thingsManager.ThingsHandler;
import com.fusionnextinc.doorbell.manager.thingsManager.ThingsManager;
import com.fusionnextinc.doorbell.objects.FNError;
import com.fusionnextinc.doorbell.objects.FNEvent;
import com.fusionnextinc.doorbell.objects.FNFirmware;
import com.fusionnextinc.doorbell.objects.FNSchedule;
import com.fusionnextinc.doorbell.objects.FNThing;
import com.fusionnextinc.doorbell.objects.FNUserProfile;
import com.fusionnextinc.doorbell.utils.FNLog;
import com.fusionnextinc.doorbell.utils.Utils;
import com.fusionnextinc.doorbell.widget.FNAlert.FNAlertDialog;
import com.fusionnextinc.doorbell.widget.FNDialog;
import com.fusionnextinc.doorbell.widget.FNHeaderListView.FNHeaderListView;
import com.fusionnextinc.doorbell.widget.FNLayoutUtil;
import com.fusionnextinc.doorbell.widget.FNProgressBarCustom;

import java.util.ArrayList;
import java.util.Objects;
import java.util.regex.Pattern;

/**
 * Created by Mike Chang on 2017/6/22
 */
public class ThingsFragment extends FNFragment implements View.OnClickListener, AbsListView.OnItemClickListener {
    private static final String TAG = ThingsFragment.class.getSimpleName();

    private FNLayoutUtil layoutUtil;
    private FNProgressBarCustom progressBar;
    private ThingsManager thingsManager;
    private AppDataThing appDataThing;
    private ThingsListAdapter thingsListAdapter = null;
    private FNHeaderListView thingsListView;
    private ListMap mOwnedThingsList;
    private ListMap mSharedThingsList;
    private ListMap mInvitedThingsList;

    private final int LOW_BATTERY_NOTIFY = 5;
    private static final int CHECK_NEW_FIRMWARE_TIME = 1000;

    public static final int THINGS_FRAGMENT_TYPE_NORMAL = 0;
    public static final int THINGS_FRAGMENT_TYPE_OTA_SUCCESS = 1;
    public static final int THINGS_FRAGMENT_TYPE_OTA_FAIL = 2;
    public static final int THINGS_FRAGMENT_TYPE_INVITATION = 3;

    private int thingsFragmentType = THINGS_FRAGMENT_TYPE_NORMAL;
    private String thingInfo = "";
    public static final String THINGS_UPDATE_STATUS_OLD = "old";
    public static final String THINGS_UPDATE_STATUS_UPDATED = "updated";
    private static String updateThingStatus = THINGS_UPDATE_STATUS_OLD;

    public static void openFragment(boolean clearOtherStack, int fragmentType, String info) {
        ThingsFragment thingsFragment = new ThingsFragment();
        thingsFragment.thingsFragmentType = fragmentType;
        thingsFragment.thingInfo = info;
        FNFragmentManager.getInstance().openFragment(thingsFragment, clearOtherStack);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        layoutUtil = new FNLayoutUtil(getActivity(), MyApplication.LAYOUT_WIDTH, MyApplication.LAYOUT_HEIGHT, FNLayoutUtil.BASE_WIDTH);
        thingsManager = ThingsManager.getInstance(getActivity());
        appDataThing = AppDataThing.getInstance(getActivity());
        appDataThing.setAppDataThingListener(appDataThingListener);
        getNewFirmware();
        setUpdateThing(true, null);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        final ViewGroup nullParent = null;

        getActivity().setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        if (!DefaultAppConfig.APP_SOLY) {
            getActionBar().resetAll();
            getActionBar().setTitle(getString(R.string.fn_title_tabbar_device), Gravity.CENTER, null);
            getActionBar().setCoverMode(false);
            getActionBar().setStartImage(R.drawable.btn_account_settings, onAccountSettingsClickListener);
            getActionBar().addEndImage(R.drawable.btn_add_thing, onAddThingClickListener);
        }
        getActionBar().show();
        View view = inflater.inflate(R.layout.fragment_things, nullParent);
        layoutUtil.fitAllView(view);
        thingsListView = (FNHeaderListView) view.findViewById(R.id.things_list);
        thingsListView.setOnScrollListener(onScrollListener);
        thingsListView.setOnItemClickListener(this);
        progressBar = new FNProgressBarCustom(getActivity(), (ViewGroup) view.findViewById(R.id.fragment_things));
        initThingsList();

        if (Utils.isNetworkConnected(getActivity())) {
            if (updateThingStatus.equals(THINGS_UPDATE_STATUS_OLD)) {
                appDataThing.setAppDataThingListener(appDataThingListener);
                appDataThing.loadAllThingsList(true, true);
            } else if (!updateThingStatus.equals(THINGS_UPDATE_STATUS_UPDATED)) {
                appDataThing.setAppDataThingListener(appDataThingListener);
                appDataThing.updateThingInfo(updateThingStatus);
            }
        } else {
            FNDialog.showNoNetworkDialog(getActivity());
        }

        if (thingsFragmentType == THINGS_FRAGMENT_TYPE_OTA_SUCCESS) {
            showOtaStatusDialog(true);
        } else if (thingsFragmentType == THINGS_FRAGMENT_TYPE_OTA_FAIL) {
            showOtaStatusDialog(false);
        } else if (thingsFragmentType == THINGS_FRAGMENT_TYPE_INVITATION) {
            showInvitationDialog(thingInfo);
        }
        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        if (DefaultAppConfig.MQTT_ENABLE) {
            if (!MqttManager.getInstance(getContext()).isConnected() && Utils.isNetworkConnected(getActivity())) {
                MqttManager.getInstance(getContext()).connect();
            }
            thingsManager.registerMqttListener(mMqttListener);
        }
    }

    @Override
    public void onPause() {
        super.onPause();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        thingsManager.unregisterMqttListener();
    }

    @Override
    public void onClick(View view) {

    }

    private void startWait() {
        progressBar.show();
    }


    private void stopWait() {
        progressBar.hide();
    }

    private View.OnClickListener onAddThingClickListener = new View.OnClickListener() {

        private void showGPSDisabledAlertToUser(){
            AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(getActivity());
            alertDialogBuilder.setMessage("GPS is disabled in your device. Would you like to enable it?")
                    .setCancelable(false)
                    .setPositiveButton("Goto Settings Page To Enable GPS",
                            new DialogInterface.OnClickListener(){
                                public void onClick(DialogInterface dialog, int id){
                                    Intent callGPSSettingIntent = new Intent(
                                            android.provider.Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                                    startActivity(callGPSSettingIntent);
                                }
                            });
            alertDialogBuilder.setNegativeButton("Cancel",
                    new DialogInterface.OnClickListener(){
                        public void onClick(DialogInterface dialog, int id){
                            dialog.cancel();
                        }
                    });
            AlertDialog alert = alertDialogBuilder.create();
            alert.show();
        }
        @Override
        public void onClick(View v) {
            SmartConnectionFragment.openFragment(Objects.requireNonNull(getActivity()),false, SmartConnectionManager.SMART_CONNECTION_CREATE_THING);
        }
    };

    private View.OnClickListener onAccountSettingsClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            AccountSettingsFragment.openFragment(false);
        }
    };

    private boolean initThingsList() {
        if (thingsListView == null) return false;

        mOwnedThingsList = new ListMap();
        mSharedThingsList = new ListMap();
        mInvitedThingsList = new ListMap();

        mOwnedThingsList.addAll(appDataThing.getAdapterData(AppDataBase.THING_TYPE_THINGS_OWNED));
        mSharedThingsList.addAll(appDataThing.getAdapterData(AppDataBase.THING_TYPE_THINGS_SHARED));
        mInvitedThingsList.addAll(appDataThing.getAdapterData(AppDataBase.THING_TYPE_THINGS_INVITED));
        //Setup the adapter
        thingsListAdapter = new ThingsListAdapter(getActivity(), mOwnedThingsList, mSharedThingsList, mInvitedThingsList, R.layout.adapter_things_list);
        thingsListView.setAdapter(thingsListAdapter);
        thingsListView.setPullRefreshEnable(true);
        thingsListView.setPullLoadEnable(false);
        //Ready the item click listener
        AdapterView.OnItemClickListener itemListener = new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                TextView textThingId = (TextView) view.findViewById(R.id.textThingId);
                TextView textThingType = (TextView) view.findViewById(R.id.textThingType);

                if (textThingId == null || textThingType == null || i < 0) {
                    FNLog.d(TAG, "textThingId == null || textThingType == null || i < 0");
                } else {
                    int thingType;
                    final String thingId = textThingId.getText().toString();

                    try {
                        thingType = Integer.valueOf(textThingType.getText().toString());
                    } catch (NumberFormatException e) {
                        thingType = -1;
                    }
                    if (thingId.length() > 0) {
                        FNThing fnThing = AppDataThing.getInstance(getActivity()).getThing(thingId);
                        int productId = 0;
                        try {
                            productId = Integer.valueOf(fnThing.getProductId());
                        } catch (NumberFormatException e) {
                            e.printStackTrace();
                        }

                        switch (thingType) {
                            case ThingsListAdapter.TYPE_OWNED_THINGS_LIST:
                                if (productId == ThingsManager.PRODUCT_ID_PLUG) {
                                    SmartPlugFragment.openFragment(false, fnThing, thingType);
                                } else {
                                    EventsFragment.openFragment(false, fnThing, thingType);
                                }
                                break;
                            case ThingsListAdapter.TYPE_SHARED_THINGS_LIST:
                                if (productId == ThingsManager.PRODUCT_ID_PLUG) {
                                    SmartPlugFragment.openFragment(false, fnThing, thingType);
                                } else {
                                    fnThing = AppDataThing.getInstance(getActivity()).getThing(thingId);
                                    EventsFragment.openFragment(false, fnThing, thingType);
                                }
                                break;
                            case ThingsListAdapter.TYPE_INVITATIONS_LIST:
                                if (Utils.isNetworkConnected(getActivity())) {
                                    showInvitationDialog(thingId);
                                    layoutUtil.fitAllView(view);
                                } else {
                                    FNDialog.showNoNetworkDialog(getActivity());
                                    layoutUtil.fitAllView(view);
                                }
                                break;
                            default:
                                break;
                        }
                    }
                }
            }
        };

        thingsListView.setOnItemClickListener(itemListener);
        thingsListView.setPullListViewListener(new FNHeaderListView.IPullListViewListener() {

            @Override
            public void onRefresh() {
                if (Utils.isNetworkConnected(getActivity())) {
                    appDataThing.refreshThingsListData();
                } else {
                    FNDialog.showNoNetworkDialog(getActivity());
                    if (thingsListView != null) {
                        thingsListView.setPullLoadEnable(false);
                        thingsListView.stopLoadMore();
                        thingsListView.stopRefresh();
                    }
                }
            }

            @Override
            public void onLoadMore() {
                FNLog.d(TAG, "onLoadMoreData: Click share footer bar");
            }
        });
        return true;
    }

    @Override
    public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {

    }

    private AbsListView.OnScrollListener onScrollListener = new AbsListView.OnScrollListener() {
        @Override
        public void onScrollStateChanged(AbsListView view, int scrollState) {

            switch (scrollState) {
                case SCROLL_STATE_IDLE:
                    break;
                case SCROLL_STATE_FLING:
                    break;
            }
        }

        @Override
        public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {

        }
    };

    final ThingsHandler.ThingsListener thingsListener = new ThingsHandler.ThingsListener() {

        @Override
        public void onListUserOwnedThings(ThingsManager instance, boolean result, String userId, ArrayList<FNThing> thingArrayList, FNError error) {

        }

        @Override
        public void onListUserInvitedThings(ThingsManager instance, boolean result, String userId, ArrayList<FNThing> thingArrayList, FNError error) {

        }

        @Override
        public void onListUserSharedThings(ThingsManager instance, boolean result, String userId, ArrayList<FNThing> thingArrayList, FNError error) {

        }

        @Override
        public void onListUserAllThings(ThingsManager instance, boolean result, String userId, ArrayList<FNThing> ownedThingArrayList, ArrayList<FNThing> sharedThingArrayList, ArrayList<FNThing> invitedThingArrayList, FNError error) {
            if (thingsListAdapter != null) {
                thingsListAdapter.updateAdapterData(AppDataThing.THING_TYPE_THINGS_ALL);
            }
            setUpdateThing(false, null);
        }

        @Override
        public void onListThingEvents(ThingsManager instance, boolean result, final String thingId, String eventName, ArrayList<FNEvent> eventList, FNError error) {
            MyApplication.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    if (thingsListAdapter != null) {
                        thingsListAdapter.updateAdapterDataById(thingId);
                    }
                    setUpdateThing(false, null);
                }
            });
        }

        @Override
        public void onListThingEventsByTime(ThingsManager instance, boolean result, final String thingId, String eventName, String startTime, String endTime, ArrayList<FNEvent> eventList, FNError error) {
            MyApplication.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    if (thingsListAdapter != null) {
                        thingsListAdapter.updateAdapterDataById(thingId);
                    }
                    setUpdateThing(false, null);
                }
            });
        }

        @Override
        public void onCurBatteryLevel(ThingsManager instance, boolean result, String thingId, String batteryLevel, FNError error) {

        }

        @Override
        public void onCurFirmwareVersion(ThingsManager instance, boolean result, String thingId, String fwVersion, FNError error) {

        }

        @Override
        public void onCurSwitchedStatus(ThingsManager instance, boolean result, String thingId, boolean isSwitch, FNError error) {

        }

        @Override
        public void onNewFirmwareVersion(ThingsManager instance, boolean result, String thingId, FNFirmware fnFirmware) {
//            if (isAdded()) {
//                final FNThing fnThing = appDataThing.getThing(thingId);
//
//                if (fnFirmware != null && fnThing != null) {
//                    boolean isNewFw = isShowFwUpdate(fnThing.getFirmwareVer(), fnFirmware.getVersion());
//
//                    if (isNewFw) {
//                        new FNAlertDialog(getActivity(), FNAlertDialog.WARNING_TYPE)
//                                .setTitleText(getActivity().getString(R.string.fn_dialog_firmware_new_title, fnFirmware.getVersion()))
//                                .setContentText(getActivity().getString(R.string.fn_dialog_firmware_new_description, fnThing.getName()))
//                                .setCancelText(getActivity().getString(R.string.fn_dialog_cancel))
//                                .setConfirmText(getActivity().getString(R.string.fn_btn_firmware_update))
//                                .showCancelButton(true)
//                                .setCancelClickListener(new FNAlertDialog.OnSweetClickListener() {
//                                    @Override
//                                    public void onClick(FNAlertDialog sDialog) {
//                                        sDialog.dismiss();
//                                    }
//                                })
//                                .setConfirmClickListener(new FNAlertDialog.OnSweetClickListener() {
//                                    @Override
//                                    public void onClick(FNAlertDialog sDialog) {
//                                        sDialog.dismiss();
//                                        FirmwareVersionFragment.openFragment(false, fnThing);
//                                    }
//                                })
//                                .show();
//                    }
//                }
//            }
        }

        @Override
        public void onStartFwUpdate(ThingsManager instance, boolean result, String thingId, String fwId) {

        }

        @Override
        public void onCurRingTone(ThingsManager instance, boolean result, String thingId, String ringtoneId, FNError error) {

        }

        @Override
        public void onGetScheduleList(ThingsManager instance, boolean result, String thingId, ArrayList<FNSchedule> scheduleList, FNError error) {

        }

        @Override
        public void onListInvitedUsers(ThingsManager instance, boolean result, String thingId, ArrayList<FNUserProfile> userList, FNError error) {

        }

        @Override
        public void onListSharedUsers(ThingsManager instance, boolean result, String thingId, ArrayList<FNUserProfile> userList, FNError error) {

        }

        @Override
        public void onCreateClaimCode(ThingsManager instance, boolean result, String claimCode, FNError error) {

        }

        @Override
        public void onCreateThing(ThingsManager instance, boolean result, FNThing fnThing, FNError error) {

        }

        @Override
        public void onUpdateThing(ThingsManager instance, boolean result, FNThing fnThing, FNError error) {

        }

        @Override
        public void onDeleteThing(ThingsManager instance, boolean result, FNError error) {

        }

        @Override
        public void onGetThing(ThingsManager instance, boolean result, FNThing fnThing, FNError error) {

        }

        @Override
        public void onChangeRingtone(ThingsManager instance, boolean result, String thingId, int ringtoneId) {

        }

        @Override
        public void onSetTimer(ThingsManager instance, boolean result, String thingId, String value) {

        }

        @Override
        public void onSetSchedule(ThingsManager instance, boolean result, String thingId, String value) {

        }

        @Override
        public void onShareInvited(ThingsManager instance, boolean result, String thingId, FNError error) {

        }

        @Override
        public void onShareAccepted(ThingsManager instance, boolean result, String thingId, FNError error) {
            FNThing fnThing;
            String user = getString(R.string.fn_text_none);
            String thing = getString(R.string.fn_text_none);

            if (thingId != null && thingId.length() > 0) {
                fnThing = AppDataThing.getInstance(getActivity()).getThing(thingId);
                if (fnThing != null) {
                    user = fnThing.getUserName();
                    thing = fnThing.getName();
                }
            }

            if (isAdded()) {
                stopWait();
                if (result) {
                    FNDialog.showSuccessDialog(getActivity(), getString(R.string.fn_dialog_invitations).replace("${USER}", user).replace("${ITEM}", thing), getString(R.string.fn_dialog_invitation_accepted));
                    appDataThing.refreshThingsListData();
                } else {
                    FNDialog.showErrorDialog(getActivity(), getString(R.string.fn_dialog_invitation_accept_failed), error);
                }
            }
        }

        @Override
        public void onShareRejected(ThingsManager instance, boolean result, String thingId, FNError error) {
            FNThing fnThing;
            String user = getString(R.string.fn_text_none);
            String thing = getString(R.string.fn_text_none);

            if (thingId != null && thingId.length() > 0) {
                fnThing = AppDataThing.getInstance(getActivity()).getThing(thingId);
                if (fnThing != null) {
                    user = fnThing.getUserName();
                    thing = fnThing.getName();
                }
            }

            stopWait();
            if (result) {
                FNDialog.showSuccessDialog(getActivity(), getString(R.string.fn_dialog_invitations).replace("${USER}", user).replace("${ITEM}", thing), getString(R.string.fn_dialog_invitation_rejected));
                appDataThing.refreshThingsListData();
            } else {
                FNDialog.showErrorDialog(getActivity(), getString(R.string.fn_dialog_invitation_reject_failed), error);
            }
        }

        @Override
        public void onSwitchOnOff(ThingsManager instance, boolean result, String thingId, boolean isTurnOn) {

        }

        @Override
        public void onCloseCallSlot(ThingsManager instance, boolean result, FNError error) {

        }

        @Override
        public void onTakeCallSlot(ThingsManager instance, boolean result, String thingId, String callSlot, FNError error) {

        }
    };

    final AppDataHandler.AppDataThingListener appDataThingListener = new AppDataHandler.AppDataThingListener() {
        @Override
        public void onRefreshEventCounts(final String thingId) {
            MyApplication.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    if (thingsListAdapter != null) {
                        thingsListAdapter.updateAdapterDataById(thingId);
                    }
                    setUpdateThing(false, null);
                }
            });
        }

        @Override
        public void onRefreshBatteryLevel(final String thingId) {
            MyApplication.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    if (thingsListAdapter != null) {
                        thingsListAdapter.updateAdapterDataById(thingId);
                    }
                    showLowBatteryWarning(thingId);
                    setUpdateThing(false, null);
                }
            });
        }

        @Override
        public void onRefreshListAdapter(final int type) {
            MyApplication.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    if (thingsListAdapter != null) {
                        thingsListAdapter.updateAdapterData(type);
                    }

                    if (thingsListView != null) {
                        thingsListView.setPullLoadEnable(false);
                        thingsListView.stopLoadMore();
                        thingsListView.stopRefresh();
                    }
                    setUpdateThing(false, null);
                    if (MqttManager.getInstance(getContext()).isConnected()) {
                        thingsManager.subscribeTopic(appDataThing.getAdapterData(AppDataBase.THING_TYPE_THINGS_OWNED)
                                , appDataThing.getAdapterData(AppDataBase.THING_TYPE_THINGS_SHARED));
                    }
                }
            });
        }
    };

    private void showLowBatteryWarning(String thingId) {
        FNThing fnThing = appDataThing.getThing(thingId);
        if (fnThing != null) {
            String sBatteryLevel = fnThing.getBatteryLevel();
            int batteryLevel = 100;
            if (sBatteryLevel != null && sBatteryLevel.length() > 0) {
                try {
                    batteryLevel = Integer.valueOf(sBatteryLevel);
                } catch (NumberFormatException e) {
                    e.printStackTrace();
                }

                if (batteryLevel <= LOW_BATTERY_NOTIFY) {
                    new FNAlertDialog(getActivity(), FNAlertDialog.WARNING_TYPE)
                            .setTitleText(getActivity().getString(R.string.fn_dialog_low_battery_title))
                            .setContentText(getActivity().getString(R.string.fn_dialog_low_battery_description, fnThing.getName()))
                            .setConfirmText(getActivity().getString(R.string.fn_dialog_ok))
                            .showCancelButton(false)
                            .setConfirmClickListener(new FNAlertDialog.OnSweetClickListener() {
                                @Override
                                public void onClick(FNAlertDialog sDialog) {
                                    sDialog.dismiss();
                                }
                            })
                            .show();
                }
            }
        }
    }

    private void getNewFirmware() {
        final ListMap ownedThingsList = new ListMap();

        ownedThingsList.addAll(appDataThing.getAdapterData(AppDataBase.THING_TYPE_THINGS_OWNED));
        new Thread(new Runnable() {
            public void run() {
                try {
                    Thread.sleep(CHECK_NEW_FIRMWARE_TIME);
                } catch (InterruptedException e) {
                    e.printStackTrace();

                }
                for (int i = 0; i < ownedThingsList.size(); i++) {
                    String thingId = (String) ownedThingsList.get(i).get(AppDataThing.TAG_THING_ID);
                    thingsManager.setThingsListener(thingsListener);
                    thingsManager.getNewFirmwareVersion(appDataThing.getThing(thingId));
                }
            }
        }).start();
    }

    private boolean isShowFwUpdate(String curFwVersion, String newFwVersion) {
        if (curFwVersion != null && curFwVersion.length() > 0 && newFwVersion != null && newFwVersion.length() > 0) {
            String[] oldVersionArray = curFwVersion.split(Pattern.quote("."));
            String[] newVersionArray = newFwVersion.split(Pattern.quote("."));

            try {
                for (int i = 0; i < newVersionArray.length; i++) {
                    int newVersion = Integer.valueOf(newVersionArray[i]);
                    int oldVersion = Integer.valueOf(oldVersionArray[i]);

                    if (newVersion > oldVersion) {
                        return true;
                    } else if (newVersion < oldVersion) {
                        return false;
                    }
                }
            } catch (NumberFormatException e) {
                return false;
            }
        }
        return false;
    }


    private void showOtaStatusDialog(boolean isSuccess) {
        if (isSuccess) {
            new FNAlertDialog(getActivity(), FNAlertDialog.WARNING_TYPE)
                    .setTitleText(getActivity().getString(R.string.fn_title_firmware_update_successfully))
                    .setContentText(getActivity().getString(R.string.dialog_firmware_update_successfully, thingInfo))
                    .setConfirmText(getActivity().getString(R.string.fn_dialog_ok))
                    .setConfirmClickListener(new FNAlertDialog.OnSweetClickListener() {
                        @Override
                        public void onClick(FNAlertDialog sDialog) {
                            sDialog.dismiss();
                        }
                    })
                    .show();
            thingsFragmentType = THINGS_FRAGMENT_TYPE_NORMAL;
        } else {
            new FNAlertDialog(getActivity(), FNAlertDialog.WARNING_TYPE)
                    .setTitleText(getActivity().getString(R.string.fn_title_firmware_update_failed))
                    .setContentText(getActivity().getString(R.string.fn_dialog_firmware_update_failed, thingInfo))
                    .setConfirmText(getActivity().getString(R.string.fn_dialog_ok))
                    .setConfirmClickListener(new FNAlertDialog.OnSweetClickListener() {
                        @Override
                        public void onClick(FNAlertDialog sDialog) {
                            sDialog.dismiss();
                        }
                    })
                    .show();
            thingsFragmentType = THINGS_FRAGMENT_TYPE_NORMAL;
        }
    }

    private void showInvitationDialog(final String thingId) {
        FNThing fnThing;
        String user = getString(R.string.fn_text_none);
        String thing = getString(R.string.fn_text_none);

        if (thingId != null && thingId.length() > 0) {
            fnThing = AppDataThing.getInstance(getActivity()).getThing(thingId);
            if (fnThing != null) {
                if (fnThing.getUserName() != null) {
                    user = fnThing.getUserName();
                }
                if (fnThing.getName() != null) {
                    thing = fnThing.getName();
                }
            }
        }

        new FNAlertDialog(getActivity(), FNAlertDialog.WARNING_TYPE)
                .setTitleText(getActivity().getString(R.string.fn_dialog_are_you_sure))
                .setContentText(getActivity().getString(R.string.fn_dialog_invitations).replace("${USER}", user).replace("${ITEM}", thing))
                .setCancelText(getActivity().getString(R.string.fn_dialog_reject))
                .setConfirmText(getActivity().getString(R.string.fn_dialog_accept))
                .showCancelButton(true)
                .setCancelClickListener(new FNAlertDialog.OnSweetClickListener() {
                    @Override
                    public void onClick(FNAlertDialog sDialog) {
                        startWait();
                        thingsManager.setThingsListener(thingsListener);
                        thingsManager.shareReject(thingId);
                        sDialog.dismiss();
                    }
                })
                .setConfirmClickListener(new FNAlertDialog.OnSweetClickListener() {
                    @Override
                    public void onClick(FNAlertDialog sDialog) {
                        startWait();
                        thingsManager.setThingsListener(thingsListener);
                        thingsManager.shareAccept(thingId);
                        sDialog.dismiss();
                    }
                })
                .show();
        thingsFragmentType = THINGS_FRAGMENT_TYPE_NORMAL;
    }

    public void setUpdateThing(boolean isUpdate, String thingId) {
        if (isUpdate && thingId != null) {
            updateThingStatus = thingId;
        } else if (isUpdate) {
            updateThingStatus = THINGS_UPDATE_STATUS_OLD;
        } else {
            updateThingStatus = THINGS_UPDATE_STATUS_UPDATED;
        }
    }

    public void setThingsFragmentType(int fragmentType, String info) {
        thingsFragmentType = fragmentType;
        thingInfo = info;
    }

    private void updateOnlineStatus() {
        if (thingsListAdapter != null) {
            thingsListAdapter.updateAdapterData(AppDataThing.THING_TYPE_THINGS_OWNED);
            thingsListAdapter.updateAdapterData(AppDataThing.THING_TYPE_THINGS_SHARED);
        }
    }

    private ThingsHandler.mqttListener mMqttListener = new ThingsHandler.mqttListener() {
        @Override
        public void onMqttConnected() {
            thingsManager.subscribeTopic(appDataThing.getAdapterData(AppDataBase.THING_TYPE_THINGS_OWNED)
                    , appDataThing.getAdapterData(AppDataBase.THING_TYPE_THINGS_SHARED));
        }

        @Override
        public void onDeviceSwitchOn(ThingsManager instance, String thingId, boolean isTurnOn) {

        }

        @Override
        public void onDeviceOnLine(ThingsManager instance, String thingId, THINGSTATUS isOnline) {
            MyApplication.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    updateOnlineStatus();
                }
            });
        }
    };
}
