package com.fusionnextinc.fndoorbell;

import android.app.Application;
import android.content.Context;
import androidx.test.platform.app.InstrumentationRegistry;
import android.test.ApplicationTestCase;

import com.fusionnextinc.doorbell.manager.data.AppDataThing;

import java.util.concurrent.CountDownLatch;

/**
 * To work on unit tests, switch the Test Artifact in the Build Variants view.
 */
public class AppDataThingUnitTest extends ApplicationTestCase<Application> {
    protected final static String _TAG = "AppDataThingUnitTest";
    Context context;
    private CountDownLatch latch;
    private AppDataThing appDataThing = null;
    public AppDataThingUnitTest() {
        super(Application.class);
    }

    public void setUp() throws Exception {
        super.setUp();
        context = getTestContext();
        appDataThing = AppDataThing.getInstance(context);
        assertNotNull(context);

    }

    public void testStartThingsListTask() {
        latch = new CountDownLatch(1); //創建CountDownLatch
        appDataThing.startThingsListTask(AppDataThing.THING_TYPE_THINGS_OWNED,false);
        try {
            latch.await();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }


    protected void tearDown() throws Exception {
        //super.tearDown();
    }

    /**
     * @return The {@link Context} of the test project.
     */
    private Context getTestContext() {
        try {
            return (Context) InstrumentationRegistry.getTargetContext();
        } catch (final Exception exception) {
            exception.printStackTrace();
            return null;
        }
    }


}