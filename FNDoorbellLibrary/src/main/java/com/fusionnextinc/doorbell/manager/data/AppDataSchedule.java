package com.fusionnextinc.doorbell.manager.data;

import android.content.Context;

import com.fusionnextinc.doorbell.objects.FNSchedule;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by Mike Chang on 2018/8/13
 */
public class AppDataSchedule extends AppDataBase {
    protected final static String TAG = "AppDataSchedule";
    public final static String TAG_SCHEDULE_ID = "TAG_SCHEDULE_ID";
    public final static String TAG_SCHEDULE_WEEKS = "TAG_SCHEDULE_WEEKS";
    public final static String TAG_SCHEDULE_HOUR = "TAG_SCHEDULE_HOUR";
    public final static String TAG_SCHEDULE_MIN = "TAG_SCHEDULE_MIN";
    public final static String TAG_SCHEDULE_SECOND = "TAG_SCHEDULE_SECOND";
    public final static String TAG_SCHEDULE_DEVICE_ONOFF = "TAG_SCHEDULE_DEVICE_ONOFF";
    public final static String TAG_SCHEDULE_SWITCH_ONOFF = "TAG_SCHEDULE_SWITCH_ONOFF";

    private volatile static AppDataSchedule instance;
    private Context mContext;
    private String mThingId;

    private AppDataSchedule(Context context) {
        this.mContext = context;
    }

    public static AppDataSchedule getInstance(Context context) {
        if (instance == null) {
            synchronized (AppDataSchedule.class) {
                if (instance == null) {
                    instance = new AppDataSchedule(context);
                }
            }
        }
        return instance;
    }

    @Override
    public ArrayList<String> getList(int tag) {
        return super.getList(tag);
    }

    @Override
    public void initList(boolean cleanIfExist) {
        super.initList(cleanIfExist);
    }

    // get the total numbers of saved share items
    public int genScheduleId(String thingId) {
        ArrayList<String> list = DataManager.getInstance(mContext).loadScheduleList(thingId);
        if (list == null || list.size() == 0) return 0;
        Collections.sort(list, new Comparator<String>(){
            @Override
            public int compare(String o1, String o2) {
                return Integer.valueOf(o1).compareTo(Integer.valueOf(o2));
            }
        });
        return Integer.valueOf(list.get(list.size() - 1)) + 1;
    }

    public boolean deleteSchedule(int type, String thingId, String scheduleId) {
        if (DataManager.getInstance(mContext).deleteScheduleList(thingId, scheduleId)) {
            ArrayList<String> list = DataManager.getInstance(mContext).loadScheduleList(thingId);
            //Put this list to list cache
            updateList(type, list);
            return DataManager.getInstance(mContext).deleteSchedule(Integer.valueOf(scheduleId));
        }
        return false;
    }

    //Ready the adapter data, this class will not save the adapter data
    public ArrayList<FNSchedule> loadScheduleList(int type, String thingId) {
        ArrayList<FNSchedule> scheduleList = new ArrayList<>();
        ArrayList<String> list = DataManager.getInstance(mContext).loadScheduleList(thingId);
        scheduleList.clear();
        if (list == null || list.size() <= 0) return scheduleList;

        //Put this list to list cache
        updateList(type, list);

        for (String str : list) {
            FNSchedule fnSchedule = DataManager.getInstance(mContext).loadSchedule(Integer.valueOf(str), thingId);
            scheduleList.add(fnSchedule);
        }

        return scheduleList;
    }

    public FNSchedule loadSchedule(String thingId, int scheduleId) {
        FNSchedule fnSchedule = DataManager.getInstance(mContext).loadSchedule(scheduleId, thingId);
        return fnSchedule;
    }

    //Ready the adapter data, this class will not save the adapter data
    public ListMap getAdapterData(int type, String thingId) {
        ListMap adapter = new ListMap();
        ArrayList<String> list = DataManager.getInstance(mContext).loadScheduleList(thingId);
        adapter.clear();
        if (list == null || list.size() <= 0) return adapter;

        //Put this list to list cache
        updateList(type, list);

        for (String str : list) {
            Map<String, Object> map = getScheduleData(Integer.valueOf(str), thingId);
            adapter.add(map);
        }

        mThingId = thingId;
        return adapter;
    }

    private Map<String, Object> getScheduleData(int scheduleId, String thingId) {
        FNSchedule fnSchedule = DataManager.getInstance(mContext).loadSchedule(scheduleId, thingId);
        Map<String, Object> map = new HashMap<String, Object>();

        if (fnSchedule != null) {
            map.put(TAG_SCHEDULE_ID, fnSchedule.getScheduleId());
            map.put(TAG_SCHEDULE_WEEKS, fnSchedule.getStrWeeks());
            map.put(TAG_SCHEDULE_HOUR, fnSchedule.getHour());
            map.put(TAG_SCHEDULE_MIN, fnSchedule.getMin());
            map.put(TAG_SCHEDULE_SECOND, fnSchedule.getSec());
            map.put(TAG_SCHEDULE_DEVICE_ONOFF, fnSchedule.isDeviceOn());
            map.put(TAG_SCHEDULE_SWITCH_ONOFF, fnSchedule.isSwitchOn());
        }
        return map;
    }

    public boolean updateSchedule(int type, String thingId, FNSchedule fnSchedule, boolean isNewSchedule) {
        ArrayList<String> list = new ArrayList<>();
        QueryStatus status = getQueryStatus(type);

        list.clear();
        if (getList(type) != null && mThingId.equals(thingId)) {
            list.addAll(getList(type));
        }

        if (fnSchedule == null) return false;
        DataManager.getInstance(mContext).saveSchedule(fnSchedule, thingId);
        if (isNewSchedule) {
            list.add(String.valueOf(fnSchedule.getScheduleId()));
        }

        if (status.isQueryEnd()) {
            status.setLastItemId("");
        } else {
            int lastIndex = list.size();
            if (lastIndex > 0) {
                status.setLastItemId(String.valueOf(lastIndex));
            }
        }
        updateQueryStatus(type, status);
        //Put this list to list cache
        updateList(type, list);
        //Save this list to database
        DataManager.getInstance(mContext).saveScheduleList(getList(type), thingId);
        return true;
    }

    public boolean updateScheduleList(int type, ArrayList<FNSchedule> scheduleList, String thingId) {
        ArrayList<String> list = new ArrayList<>();

        list.clear();
        QueryStatus status = getQueryStatus(type);

        if (!status.isNewQuery()) {
            list.addAll(getList(type));
        }

        if (scheduleList == null) {
            status.setQueryEnd(true);
            status.setLastItemId("");
            updateQueryStatus(type, status);
            return false;
        }
        for (int i = 0; i < scheduleList.size(); i++) {
            FNSchedule fnSchedule = scheduleList.get(i);
            if (fnSchedule == null) return false;
            DataManager.getInstance(mContext).saveSchedule(fnSchedule, thingId);
            list.add(String.valueOf(fnSchedule.getScheduleId()));
        }

        if (scheduleList.size() < status.getPageSize()) {
            status.setQueryEnd(true);
        }

        if (status.isQueryEnd()) {
            status.setLastItemId("");
        } else {
            int lastIndex = list.size();
            if (lastIndex > 0) {
                //status.setLastItemId( list.get(lastIndex - 1));
                status.setLastItemId(String.valueOf(lastIndex));
            }
        }
        updateQueryStatus(type, status);

        //Put this list to list cache
        updateList(type, list);
        //Save this list to database
        DataManager.getInstance(mContext).saveScheduleList(getList(type), thingId);
        return true;
    }
}
