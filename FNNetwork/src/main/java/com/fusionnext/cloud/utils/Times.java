package com.fusionnext.cloud.utils;

import android.content.Context;

import java.io.File;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.TimeZone;

/**
 * Created by alfie.si on 2014/5/9.
 * Times module
 */
public class Times {

    public final static String _TIME_FORMAT_STANDARD = "yyyy-MM-dd HH:mm:ss";
    public final static String _TIME_FORMAT_STANDARD_DATE = "yyyy-MM-dd";
    // public final static String _TIME_FORMAT_SUBMIT_SERVER   = "yyyyMMddHHmmss";
    public static final String _TIME_FORMAT_NO_SECONDS = "yyyy-MM-dd HH:mm";
    public static final String _TIME_FORMAT_NO_SECONDS_YEARS = "MM-dd HH:mm";
    public static final String _TIME_FORMAT_RFC3339 = "yyyy-MM-dd'T'HH:mm:ssZ"; // spec for RFC3339
    public static final String _TIME_FORMAT_RFC3339_UTC = "yyyy-MM-dd'T'HH:mm:ss'Z'"; //spec for RFC3339
    public static final String _TIME_FORMAT_SHORT_12 = "yyyy-MM-dd h:mm a";
    public static final String _TIME_FORMAT_SHORT_24 = "yyyy-MM-dd HH:mm";
    public static final String _TIME_FORMAT_HOUR_MIN_SEC = "HH:mm:ss";

    public static final String _HOUR_AGO = "hour ago";
    public static final String _HOURS_AGO = "hours ago";
    public static final String _MINUTE_AGO = "minute ago";
    public static final String _MINUTES_AGO = "minutes ago";
    public static final String _JUST_NOW = "Just now";

    public static String formatTransfer(String timeStr, String srcFormat, String destFormat, TimeZone timezone) {
        if (timeStr == null || timeStr.equals("")) return "";
        SimpleDateFormat formatter = new SimpleDateFormat(srcFormat);
        formatter.setTimeZone(timezone);
        Date time;
        try {
            time = formatter.parse(timeStr);
        } catch (ParseException pe) {
            pe.printStackTrace();
            return "";
        }
        SimpleDateFormat destFormatter = new SimpleDateFormat(destFormat);
        destFormatter.setTimeZone(timezone);
        return destFormatter.format(time.getTime());
    }

    public static String utcTimeToLocal(String utcTimeStr, String utcFormat, String localFormat) {
        if (utcTimeStr == null || utcTimeStr.equals("")) return "";

        SimpleDateFormat utcFormatter = new SimpleDateFormat(utcFormat);
        utcFormatter.setTimeZone(TimeZone.getTimeZone("UTC"));
        Date utcTime;
        try {
            utcTime = utcFormatter.parse(utcTimeStr);
        } catch (ParseException pe) {
            pe.printStackTrace();
            return "";
        }

        SimpleDateFormat localFormatter = new SimpleDateFormat(localFormat);
        localFormatter.setTimeZone(TimeZone.getDefault());
        return localFormatter.format(utcTime.getTime());
    }

    public static String localToUTC(String localTimeStr, String localFormat, String utcFormat) {
        if (localTimeStr == null || localTimeStr.equals("")) return "";

        SimpleDateFormat localFormatter = new SimpleDateFormat(localFormat);
        localFormatter.setTimeZone(TimeZone.getDefault());
        Date localTime;
        try {
            localTime = localFormatter.parse(localTimeStr);
        } catch (ParseException pe) {
            pe.printStackTrace();
            return "";
        }

        SimpleDateFormat utcFormatter = new SimpleDateFormat(utcFormat);
        utcFormatter.setTimeZone(TimeZone.getTimeZone("UTC"));
        return utcFormatter.format(localTime.getTime());
    }

    public static boolean compareTimes(String timeStr1, String timeStr2) {
        SimpleDateFormat dfs = new SimpleDateFormat(_TIME_FORMAT_STANDARD);
        if (timeStr1 != null && timeStr2 != null) {
            try {
                Date d1 = dfs.parse(timeStr1);
                Date d2 = dfs.parse(timeStr2);
                return d1.getTime() > d2.getTime();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return false;
    }

    public static String calcTimeBetween(
            Context context, String newTimeStr, String oldTimeStr) {
        SimpleDateFormat dfs = new SimpleDateFormat(_TIME_FORMAT_STANDARD);
        Date oldTime;
        Date newTime;
        try {
            oldTime = dfs.parse(oldTimeStr);
            newTime = dfs.parse(newTimeStr);
        } catch (ParseException pe) {
            pe.printStackTrace();
            return oldTimeStr;
        }

        long between = (newTime.getTime() - oldTime.getTime()) / 1000;//除以1000是为了转换成秒
        long day1 = between / (24 * 3600);

        if (day1 > 0) {
            SimpleDateFormat yearOnly = new SimpleDateFormat("yyyy");
            SimpleDateFormat dayFormat = null;
            if (yearOnly.format(oldTime).equals(yearOnly.format(newTime)))
                dayFormat = new SimpleDateFormat(_TIME_FORMAT_NO_SECONDS_YEARS);
            else
                dayFormat = new SimpleDateFormat(_TIME_FORMAT_NO_SECONDS);

            return dayFormat.format(oldTime);
        }
        long hour1 = between % (24 * 3600) / 3600;
        long minute1 = between % 3600 / 60;
        String retStr = "";
        if (hour1 > 0) {
            String hourSuffix = hour1 > 1 ?
                    _HOURS_AGO :
                    _HOUR_AGO;
            retStr = hour1 + " " + hourSuffix;
            return retStr;
        }
        if (minute1 >= 0) {
            if (minute1 <= 1) retStr = _JUST_NOW;
            else {
                String minuteSuffix = minute1 > 1 ?
                        _MINUTES_AGO :
                        _MINUTE_AGO;
                retStr = minute1 + " " + minuteSuffix;
            }
            return retStr;
        }
        //if (second1 > 0) retStr = retStr + second1 + "秒";
        //if (!retStr.equals("")) retStr = retStr + "前";

        if (retStr.equals("")) retStr = oldTimeStr;

        return retStr;
    }

    public static String calcTimeTillNow(Context context, String oldTimeStr) {
        return calcTimeBetween(context, nowTime(_TIME_FORMAT_STANDARD), oldTimeStr);
    }

    public static String nowTime(String format) {
        return (getDateString(new Date(), format));
    }

    public static String getDateString(Date date, String format) {
        SimpleDateFormat dfs = new SimpleDateFormat(format);
        return dfs.format(date);
    }

    public static String getFileModifyDate(String path, String format) {
        File file = new File(path);
        Date date = new Date(file.lastModified());
        return getDateString(date, format);
    }

    public static Date parseTime(String timeStr, String format) {
        SimpleDateFormat dfs = new SimpleDateFormat(format);
        Date retDate = null;
        try {
            retDate = dfs.parse(timeStr);
        } catch (ParseException ex) {
            ex.printStackTrace();
        }
        return retDate;
    }

    public static String parseUnixTime(long time, String format) {
        Long timestamp = time * 1000;
        String date = new SimpleDateFormat(format).format(new Date(timestamp));
        return date;
    }

    public static int getDateDays(String date1, String date2) {
        date1 = date1.split(" ")[0];
        date2 = date2.split(" ")[0];
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        int days = 0;
        try {
            Date date = sdf.parse(date1);// 通过日期格式的parse()方法将字符串转换成日期
            Date dateBegin = sdf.parse(date2);
            // System.out.println("getDateDays:date" + date);
            // ;
            // System.out.println("dateBegin" + dateBegin);
            long betweenTime = date.getTime() - dateBegin.getTime();
            days = (int) (betweenTime / 1000 / 60 / 60 / 24);
        } catch (Exception e) {
            // System.out.println(e.toString());
        }
        // System.out.println("day==" + days);
        return days;
    }

    /* 將Server傳送的UTC時間轉換为指定時區的時間 */
    public static String converTime(String srcTime, TimeZone timezone) {
        SimpleDateFormat sdf = new SimpleDateFormat(_TIME_FORMAT_STANDARD);
        SimpleDateFormat dspFmt = new SimpleDateFormat(_TIME_FORMAT_STANDARD);
        String convertTime;

        Date result_date;
        long result_time = 0;

        // 如果傳入参數異常，使用本地時間
        if (null == srcTime) {
            result_time = System.currentTimeMillis();
        } else {
            // 將輸入時間字串轉換为UTC時間
            try {
                sdf.setTimeZone(TimeZone.getTimeZone("GMT00:00"));
                result_date = sdf.parse(srcTime);
                result_time = result_date.getTime();
            } catch (Exception e) {
                // 出現異常時，使用本地時間
                result_time = System.currentTimeMillis();
                dspFmt.setTimeZone(TimeZone.getDefault());
                convertTime = dspFmt.format(result_time);
                return convertTime;
            }
        }
        // 設定時區
        dspFmt.setTimeZone(timezone);
        convertTime = dspFmt.format(result_time);
        return convertTime;
    }

    public static String convertSecondToHHMMString(long secondtTime) {
        TimeZone tz = TimeZone.getTimeZone("UTC");
        SimpleDateFormat df = new SimpleDateFormat(_TIME_FORMAT_HOUR_MIN_SEC);
        df.setTimeZone(tz);
        String time = df.format(new Date(secondtTime * 1000L));

        return time;
    }
}
