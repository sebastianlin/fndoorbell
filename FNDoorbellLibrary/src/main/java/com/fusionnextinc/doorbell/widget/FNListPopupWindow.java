package com.fusionnextinc.doorbell.widget;

import android.content.Context;
import android.graphics.drawable.BitmapDrawable;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.TranslateAnimation;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.PopupWindow;

import com.fusionnextinc.doorbell.R;


/**
 * Created by chocoyang on 2016/4/23.
 */
public class FNListPopupWindow extends PopupWindow implements View.OnClickListener {
    private static final String TAG = "FNListPopupWindow";

    public interface OnPopupItemClickListener {
        void onPopupItemClick(FNListPopupWindow parent, View view, int position);
    }

    private static FNListPopupWindow popupWindow;
    private OnPopupItemClickListener onPopupItemClickListener;
    private LinearLayout layout;
    private boolean startAnim;

    public FNListPopupWindow(Context context, boolean isPopBar) {
        super(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
        setAnimationStyle(R.style.PopupAnimation);
        LinearLayout contentView = new LinearLayout(context);
        contentView.setOnClickListener(this);
        contentView.setBackgroundResource(android.R.drawable.screen_background_dark_transparent);
        startAnim = false;
        layout = new LinearLayout(context) {
            @Override
            protected void onLayout(boolean changed, int l, int t, int r, int b) {
                super.onLayout(changed, l, t, r, b);
                if (changed && !startAnim) {
                    startAnim = true;
                    int count = layout.getChildCount();
                    int eachHeight = count > 0 ? (b - t) / count : 0;
                    for (int i = 0; i < count; i++) {
                        View child = layout.getChildAt(i);
                        TranslateAnimation anim = new TranslateAnimation(0, 0, eachHeight * (-i - 1), 0);
                        anim.setDuration(200);
                        child.startAnimation(anim);
                    }
                }
            }
        };
        layout.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT));
        layout.setOrientation(LinearLayout.VERTICAL);
        if (!isPopBar) {
            contentView.setGravity(Gravity.RIGHT);
        }
        contentView.addView(layout);
        setBackgroundDrawable(new BitmapDrawable());
        setFocusable(true);
        setContentView(contentView);
    }

    public void setAdapter(ListAdapter adapter) {
        if (adapter != null) {
            for (int i = 0; i < adapter.getCount(); i++) {
                View view = adapter.getView(i, null, null);
                if (view != null) {
                    view.setTag(R.id.position, i);
                    view.setOnClickListener(this);
                    layout.addView(view);
                }
            }
        } else {
            layout.removeAllViews();
        }
    }

    public void setOnPopupItemClickListener(OnPopupItemClickListener onPopupItemClickListener) {
        this.onPopupItemClickListener = onPopupItemClickListener;
    }

    @Override
    public void onClick(View v) {
        Object position = v.getTag(R.id.position);
        if (position != null && onPopupItemClickListener != null) {
            onPopupItemClickListener.onPopupItemClick(this, v, (int) position);
        }
        dismiss();
    }

    @Override
    public void dismiss() {
        super.dismiss();
        popupWindow = null;
    }

    @Override
    public void showAtLocation(View parent, int gravity, int x, int y) {
        if (popupWindow != null && popupWindow.isShowing())
            popupWindow.dismiss();
        super.showAtLocation(parent, gravity, x, y);
        popupWindow = this;
    }

    @Override
    public void showAsDropDown(View anchor) {
        if (popupWindow != null && popupWindow.isShowing())
            popupWindow.dismiss();
        super.showAsDropDown(anchor);
        popupWindow = this;
    }

    @Override
    public void showAsDropDown(View anchor, int xoff, int yoff) {
        if (popupWindow != null && popupWindow.isShowing())
            popupWindow.dismiss();
        super.showAsDropDown(anchor, xoff, yoff);
        popupWindow = this;
    }

    @Override
    public void showAsDropDown(View anchor, int xoff, int yoff, int gravity) {
        if (popupWindow != null && popupWindow.isShowing())
            popupWindow.dismiss();
        super.showAsDropDown(anchor, xoff, yoff, gravity);
        popupWindow = this;
    }
}
