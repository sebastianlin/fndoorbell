package com.fusionnextinc.doorbell.info;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

/**
 * Created by Mike Chang on 2016/7/12
 */
public class SharedPreferencesInfo {
    private static final String TAG = "SharedPreferencesInfo";
    private static SharedPreferences settings;

    public static final String KEY_RINGING_THING_ID = "vr_ringing_thing_id";

    public static SharedPreferences getDefaultSharedPreferences() {
        return settings;
    }

    public static void initDefaultSharedPreferences(Context context) {
        settings = PreferenceManager.getDefaultSharedPreferences(context);
    }

    public static int getInt(String key, int value) {
        return settings.getInt(key, value);
    }

    public static boolean putInt(String key, int value) {
        return settings.edit().putInt(key, value).commit();
    }

    public static String getString(String key, String defValue) {
        return settings.getString(key, defValue);
    }

    public static boolean putString(String key, String value) {
        return settings.edit().putString(key, value).commit();
    }

    public static boolean getBoolean(String key, boolean defValue) {
        return settings.getBoolean(key, defValue);
    }

    public static boolean putBoolean(String key, boolean value) {
        return settings.edit().putBoolean(key, value).commit();
    }

    public static boolean remove(String key) {
        return settings.edit().remove(key).commit();
    }

    public static SharedPreferences getSharedPreferences() {
        return settings;
    }
}
