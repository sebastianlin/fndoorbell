package com.fusionnextinc.fndoorbell;

import android.app.Application;
import android.content.Context;
import androidx.test.platform.app.InstrumentationRegistry;
import android.test.ApplicationTestCase;

import com.fusionnextinc.doorbell.manager.account.AaccountHandler;
import com.fusionnextinc.doorbell.manager.account.AccountManager;
import com.fusionnextinc.doorbell.manager.data.DataManager;
import com.fusionnextinc.doorbell.objects.FNError;

import java.util.concurrent.CountDownLatch;

/**
 * To work on unit tests, switch the Test Artifact in the Build Variants view.
 */
public class AccountUnitTest extends ApplicationTestCase<Application> {
    protected final static String _TAG = "AccountUnitTest";
    Context context;
    private CountDownLatch latch;

    public AccountUnitTest() {
        super(Application.class);
    }

    public void setUp() throws Exception {
        super.setUp();
        context = getTestContext();
        assertNotNull(context);
    }

    public void testUserLogin() {
        latch = new CountDownLatch(1); //創建CountDownLatch
        AccountManager accountManager = AccountManager.getInstance(context);
        accountManager.setAccountListener(accountListener);
        accountManager.setUserAccount("bastard_john@starks.com");
        accountManager.setUserPassword("111111");
        accountManager.setFirebaseToken(DataManager.getInstance(context).loadFireBasePushToken());
        AccountManager.getInstance(context).userLogin();
        try {
            latch.await();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public void testUserRegister() {
        latch = new CountDownLatch(1); //創建CountDownLatch
        AccountManager accountManager = AccountManager.getInstance(context);
        accountManager.setAccountListener(accountListener);
        accountManager.setUserAccount("mikechang5@fusionnextinc.com");
        accountManager.setUserPassword("111111");
        accountManager.setUserName("Mike Chang");
        AccountManager.getInstance(context).userRegister();
        try {
            latch.await();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public void testUserLogout() {
        latch = new CountDownLatch(1); //創建CountDownLatch
        AccountManager accountManager = AccountManager.getInstance(context);
        accountManager.userLogout();
        try {
            latch.await();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public void testResetPassword() {
        AccountManager accountManager = AccountManager.getInstance(context);
        accountManager.resetUserPassword(accountManager.getUserAccount());
    }

    public void testChangePassword() {
        String newPwd = "111111";
        String oldPwd = "111111";

        AccountManager accountManager = AccountManager.getInstance(context);
        accountManager.changUserPassword(oldPwd, newPwd);
    }

    protected void tearDown() throws Exception {
        //super.tearDown();
    }

    /**
     * @return The {@link Context} of the test project.
     */
    private Context getTestContext() {
        try {
            return (Context) InstrumentationRegistry.getTargetContext();
        } catch (final Exception exception) {
            exception.printStackTrace();
            return null;
        }
    }


    final AaccountHandler.AccountListener accountListener = new AaccountHandler.AccountListener() {
        @Override
        public void onUserRegistered(AccountManager instance, boolean result, FNError error) {
            latch.countDown();
            assertEquals(true, result);
        }

        @Override
        public void onUserLogined(AccountManager instance, boolean result, FNError error) {
            latch.countDown();
            assertEquals(true, result);
        }

        @Override
        public void onGetUserInfo(AccountManager instance, boolean result, FNError error) {

        }

        @Override
        public void onUpdateUserInfo(AccountManager instance, boolean result, FNError error) {

        }

        @Override
        public void onUserLogout(AccountManager instance, boolean result, FNError error) {
            latch.countDown();
            assertEquals(true, result);
        }

        @Override
        public void onResetPassword(AccountManager instance, boolean result, FNError error) {
            latch.countDown();
            assertEquals(true, result);
        }

        @Override
        public void onSetPassword(AccountManager instance, boolean result, FNError error) {

        }

    };
}