package com.fusionnextinc.doorbell.objects;

/**
 * Created by Mike Chang on 2017/6/5
 */
public class FNUserInfo {

    protected final static String TAG = "FNUserInfo";

    public FNUserInfo() {
        name = "none";
        account = "";
        password = "";
        firebaseToken = "";
    }

    private String name;
    private String account;
    private String password;
    private String firebaseToken;


    public void clean() {
        name = "none";
        account = "";
        password = "";
        firebaseToken = "";
    }

    public void setUserAccount(String account) {
        this.account = account;
    }

    public String getUserAccount() {
       return this.account;
    }

    public void setUserPassword(String password) {
        this.password = password;
    }

    public String getUserPassword() {
        return this.password;
    }

    public void setUserName(String name) {
        this.name = name;
    }

    public String getUserName() {
        return this.name;
    }

    public void setFirebaseToken(String firebaseToken) {
        this.firebaseToken = firebaseToken;
    }

    public String getFirebaseToken() {
        return this.firebaseToken;
    }
}
