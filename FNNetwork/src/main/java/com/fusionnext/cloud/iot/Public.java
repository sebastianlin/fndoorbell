package com.fusionnext.cloud.iot;

import android.content.Context;
import android.net.wifi.WifiManager;
import android.os.Build;
import android.os.Build;
public class Public {
    static MyParameter instance = null;
    public static boolean STAGING_MODE = false;
    public static String mIPaddress = "";
    private synchronized static MyParameter getMyParam(Context context) {
        if (instance == null) {
            instance = MyParameter.newInstance();
            STAGING_MODE = context.getApplicationContext().getPackageName().contains("staging");
            if( STAGING_MODE )
                _HOST_SERVER = _HOST_API_STAGING_SERVER;
            else
                _HOST_SERVER = _HOST_API_SERVER;

            mIPaddress = getServerIP(context);
        }
        return instance;
    }

    public static boolean isStagingMode()
    {
        return STAGING_MODE;
    }

    public static String DeviceModeString()
    {
        if (STAGING_MODE) {
            return "staging";
        }
        return "productiom";
    }

    private static String getServerIP(Context context)
    {
        WifiManager wifiManager = (WifiManager)context.getApplicationContext().getSystemService(Context.WIFI_SERVICE);
        int ipAddress = wifiManager.getConnectionInfo().getIpAddress();
        String IpAddress = String.format("%03d.%d.%d.%d",
                (ipAddress & 0xff),
                (ipAddress >> 8 & 0xff),
                (ipAddress >> 16 & 0xff),
                (ipAddress >> 24 & 0xff));
        return IpAddress;
    }
    public static String ServerIP() {

        return mIPaddress;
    }

    public static String MakeClaimString( String ClaimCode ,String ipAddress ) {
        String result;

        if (android.os.Build.VERSION.SDK_INT < android.os.Build.VERSION_CODES.R){
  //          return ClaimCode;
        }

        if(ClaimCode.length() == 0 )
        {
            ClaimCode = "NULLNULLNULLNULLNULLNULLNULLNULL";
        }
        if (STAGING_MODE) {
            result = ClaimCode+"@";
        } else {
            result = ClaimCode+"#";
        }
        return result+ipAddress;
    }

    public synchronized static Authorization getAuthorization(Context context) {
        return getMyParam(context).getAuthorization(context);
    }

    public synchronized static void setAuthorization(Context context, Authorization authorization) {
        getMyParam( context ).setAuthorization(authorization);
    }

    public synchronized static Devices getDevices(Context context) {
        return getMyParam(context).getDevices(context);
    }

    public synchronized static void setDevices(Context context, Devices devices) {
        getMyParam(context).setDevices(devices);
    }

    public synchronized static User getUser(Context context) {
        return getMyParam(context).getUser(context);
    }

    public synchronized static void setUser(Context context, User user) {
        getMyParam(context).setUser(user);
    }

    public synchronized static DataManager getDataMgr(Context context) {
        return getMyParam(context).getDataMgr(context);
    }

    public synchronized static void setDataMgr(Context context, DataManager dm) {
        getMyParam(context).setDataMgr(dm);
    }

    //============================================================================
    //Interface url for Doweing
    //Beta server, internet

    public final static String _HOST_STAGING_SERVER = "staging.api.iot.fusionnextinc.com";
    public final static String _HOST_PRODUCTION_SERVER = "api.iot.fusionnextinc.com";
    public  final static String _HOST_API_SERVER = "api.iot.ptfog.cc";
    public  final static String _HOST_API_STAGING_SERVER = "staging.api.iot.ptfog.cc";

    public final static String _HOST_PRODUCTION_SERVER_OLD = "https://exos.ptfog.cc/v1";

    public  static String _HOST_SERVER = "";
    public static String _HOST_V1() {

        return "https://"+_HOST_SERVER+"/v1";
    }
    //Need authorization
    public static String _INTERFACE_POST_GET_CLIENT_TOKEN() { return _HOST_V1() + "/auth"; }
    public static String _INTERFACE_POST_REFRESH_DEVICE_TOKEN() { return _HOST_V1() + "/auth"; }
    public  static String _INTERFACE_POST_LOGIN_BY_EMAIL() { return _HOST_V1() + "/auth"; }
    //Users

    public  static String _INTERFACE_POST_CREATE_USER() { return _HOST_V1() + "/users"; }
    public  static String _INTERFACE_POST_RESEND() { return _HOST_V1() + "/users/resend_verification_code"; }
    public  static String _INTERFACE_POST_RESET_USER_PASSWORD() { return _HOST_V1() + "/users/reset_password"; }
    public  static String _INTERFACE_POST_SET_USER_PASSWORD() { return _HOST_V1() + "/users/set_password"; }
    public  static String _INTERFACE_PUT_UPDATE_USER() { return _HOST_V1() + "/users/{user_id}"; }
    public  static String _INTERFACE_GET_LIST_USER_OWNED_THINGS(){ return _HOST_V1() + "/users/{user_id}/things?role=owner&include=owners,invitees,settings,last_unique_states:sort(created_at):order(desc):unique(name),events:sort(created_at):order(desc):limit(1)&"; }
    public  static String _INTERFACE_GET_LIST_USER_INVITED_THINGS(){ return _HOST_V1() + "/users/{user_id}/things?role=invitee&include=owners,invitees,settings,last_unique_states:sort(created_at):order(desc):unique(name),events:sort(created_at):order(desc):limit(1)&"; }
    public  static String _INTERFACE_GET_LIST_USER_SHARED_THINGS(){ return _HOST_V1() + "/users/{user_id}/things?role=guest&include=owners,invitees,settings,last_unique_states:sort(created_at):order(desc):unique(name),events:sort(created_at):order(desc):limit(1)&"; }
    public  static String _INTERFACE_GET_LIST_USER_ALL_THINGS(){ return _HOST_V1() + "/users/{user_id}?include=owned_things:limit(500),owned_things.last_unique_states:sort(created_at):order(desc):unique(name):limit(2),owned_things.events:sort(created_at):order(desc):limit(1),owned_things.settings,owned_things.owners,owned_things.invitees,shared_things:limit(500),shared_things.last_unique_states:sort(created_at):order(desc):unique(name):limit(2),shared_things.states:sort(created_at):order(desc):unique(name):limit(2),shared_things.events:sort(created_at):order(desc):limit(1),shared_things.settings,shared_things.owners,shared_things.invitees,invited_things:limit(500),invited_things.last_unique_states:sort(created_at):order(desc):unique(name):limit(2),invited_things.events:sort(created_at):order(desc):limit(1),invited_things.settings,invited_things.owners,invited_things.invitees&"; }
    public  static String _INTERFACE_GET_GET_USER() { return _HOST_V1() + "/users/{user_id}"; }
    //Devices
    public  static String _INTERFACE_POST_CREATE_DEVICE() { return _HOST_V1() + "/devices"; }
    public  static String _INTERFACE_PUT_UPDATE_DEVICE() { return _HOST_V1() + "/devices/{device_id}"; }
    public  static String _INTERFACE_DEL_DELETE_DEVICE(){ return _HOST_V1() + "/devices/{device_id}"; }
    //Files
    public  static String _INTERFACE_POST_UPLOAD_FILE() { return _HOST_V1() + "/files"; }
    //Pushtokens
    public  static String _INTERFACE_POST_CREATE_PUSH_TOKEN() { return _HOST_V1() + "/push_tokens"; }
    //Settings
    public  static String _INTERFACE_POST_CREATE_SETTINGS() { return _HOST_V1() + "/settings"; }
    //Things
    public  static String _INTERFACE_POST_CREATE_CLAIMCODE() { return _HOST_V1() + "/claim_codes"; }
    public  static String _INTERFACE_POST_CREATE_THING() { return _HOST_V1()+ "/things"; }
    public  static String _INTERFACE_POST_THING_INVITE(){ return _HOST_V1() + "/things/{thing_id}/users"; }
    public  static String _INTERFACE_PUT_UPDATE_THING(){ return _HOST_V1() + "/things/{thing_id}"; }
    public  static String _INTERFACE_DEL_DELETE_THING(){ return _HOST_V1() + "/things/{thing_id}"; }
    public  static String _INTERFACE_DEL_THING_REJECT(){ return _HOST_V1() + "/things/{thing_id}/users"; }
    public  static String _INTERFACE_PUT_THING_ACCEPT(){ return _HOST_V1() + "/things/{thing_id}/users";}
    public  static String _INTERFACE_PUT_THING_TAKE_CALL_SLOT(){ return _HOST_V1() + "/call_slots/{call_slot}";}
    public  static String _INTERFACE_DEL_THING_CLOSE_CALL_SLOT(){ return _HOST_V1() + "/call_slots/{call_slot}";}
    public  static String _INTERFACE_GET_LIST_THING_STATES(){ return _HOST_V1() + "/things/{thing_id}/states"; }
    public  static String _INTERFACE_GET_LIST_THING_SETTINGS(){ return _HOST_V1() + "/things/{thing_id}/settings";}
    public  static String _INTERFACE_GET_LIST_THING_EVENTS(){ return _HOST_V1() + "/things/{thing_id}/events";}
    public  static String _INTERFACE_GET_LIST_THING_INVITED_USERS(){ return _HOST_V1() + "/things/{thing_id}/users?role=invitee"; }
    public  static String _INTERFACE_GET_LIST_THING_SHARED_USERS(){ return _HOST_V1() + "/things/{thing_id}/users?role=guest"; }
    public  static String _INTERFACE_GET_GET_THING_INFO(){ return _HOST_V1() + "/things/{thing_id}?include=owners,invitees,settings,states:sort(created_at):order(desc):unique(name),events:sort(created_at):order(desc):limit(1)";}
    //OTA
    public static  String _INTERFACE_SEARCH_FW_UPDATE_LIST(){ return _HOST_V1() + "/products/{product_id}/fw_updates?sort=version&"; }
    public static  String _INTERFACE_GET_FW_UPDATE_LINK(){ return _HOST_V1() + "/fw_updates/{fw_id}"; }
    public static  String _INTERFACE_GET_SW_UPDATE_LIST(){ return _HOST_V1() + "/software_updates/?client_id= {client_id}&sort=created_at&limit=1"; }
    public static  String _INTERFACE_GET_SW_UPDATE_LINK(){ return _HOST_V1() + "/software_updates/{client_id}"; }
    public  static String _INTERFACE_POST_FW_START_UPDATE(){ return _HOST_V1()  + "/commands"; }
    //Smart plug
    public  static String _INTERFACE_POST_SMART_PLUG_SWITCH_ON_OFF(){ return _HOST_V1() + "/commands"; }

    //PtCom Wifi module
    public final static String _PTCOM_HOST_V1 = "http://192.168.30.1:32000";

    //Get Wifi AP list
    public final static String _PTCOM_INTERFACE_GET_WIFI_AP_LIST = _PTCOM_HOST_V1 + "/info";
    //Register
    public final static String _PTCOM_INTERFACE_GET_REGISTER = _PTCOM_HOST_V1 + "/provision?ssid={ssid}&pwd={pwd}&sec={sec}&claimcode={claim_code}&ipaddr={ipaddr}&mode={mode}";
    //Change SSID
    public final static String _PTCOM_INTERFACE_GET_CHANGE_SSID = _PTCOM_HOST_V1 + "/provision?ssid={ssid}&pwd={pwd}&sec={sec}&claimcode=''";

}
