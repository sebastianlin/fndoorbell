package com.fusionnextinc.doorbell.Adapter;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.fusionnextinc.doorbell.MyApplication;
import com.fusionnextinc.doorbell.R;
import com.fusionnextinc.doorbell.objects.FNWifiAp;
import com.fusionnextinc.doorbell.widget.FNLayoutUtil;

import java.util.ArrayList;

/**
 * Created by Mike Chang on 2017/7/13
 */
public class WifiAPListAdapter extends BaseAdapter {
    private static final String TAG = "WifiAPListAdapter";

    private FNLayoutUtil mLayoutUtil;
    private Activity mActivity;
    private ArrayList<FNWifiAp> mWifiApList;
    private int mSelectedId;

    public WifiAPListAdapter(Activity mActivity, ArrayList<FNWifiAp> wifiApList) {
        mLayoutUtil = new FNLayoutUtil(mActivity, MyApplication.LAYOUT_WIDTH, MyApplication.LAYOUT_HEIGHT, FNLayoutUtil.BASE_WIDTH);
        this.mActivity = mActivity;
        this.mWifiApList = wifiApList;
        this.mSelectedId = -1;
    }

    @Override
    public int getCount() {
        return mWifiApList.size();
    }

    @Override
    public Object getItem(int position) {
        return mWifiApList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        final ViewHolder holder;
        final ViewGroup nullParent = null;

        if (convertView == null) {
            holder = new ViewHolder();
            convertView = LayoutInflater.from(mActivity).inflate(R.layout.adapter_wifiap_list, nullParent);
            mLayoutUtil.fitAllView(convertView);
            holder.mTxtWifiApType = (TextView) convertView.findViewById(R.id.txt_wifap_type);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }
        if (mWifiApList != null && position < mWifiApList.size() && position >= 0) {
            holder.mTxtWifiApType.setText(mWifiApList.get(position).getSsid());
        }

        return convertView;
    }

    private class ViewHolder {
        private TextView mTxtWifiApType;
    }

    public ArrayList<FNWifiAp> getWifiApList() {
        return mWifiApList;
    }

    public void updateWifiApList(ArrayList<FNWifiAp> mWifiApList) {
        this.mWifiApList = mWifiApList;
        notifyDataSetChanged();
    }

}
