package com.fusionnextinc.doorbell.objects;

import android.content.Context;

import com.fusionnext.cloud.iot.objects.THINGSTATUS;
import com.fusionnext.cloud.iot.objects.Thing;
import com.fusionnextinc.doorbell.manager.thingsManager.ThingsManager;

import java.util.ArrayList;

/**
 * Created by Mike Chang on 2016/6/3
 */
public class FNThing {
    protected final static String TAG = "FNThing";

    public FNThing() {
        thingId = "";
        productId = "";
        productSlug = "";
        identifier = "";
        name = "";
        firmwareVer = "";
        ringtoneId = "";
        batteryLevel = "";
        createdAt = "";
        updatedAt = "";
        readedAt = "";
        userId = "";
        organizationId = "";
        userName = "";
        userEmail = "";
        eventCounts = "";
        lastestEventTime = "";
        switched = 1;
        online = THINGSTATUS.EMPTY;
    }

    private String thingId;
    private String productId;
    private String productSlug;
    private String identifier;
    private String name;
    private String firmwareVer;
    private String ringtoneId;
    private String batteryLevel;
    private String createdAt;
    private String updatedAt;
    private String readedAt;
    private String userId;
    private String organizationId;
    private String userName;
    private String userEmail;
    private String eventCounts;
    private String inviteesCounts;
    private String lastestEventTime;
    private int switched;
    private THINGSTATUS online;

    public void clean() {
        thingId = "";
        productId = "";
        productSlug = "";
        identifier = "";
        name = "";
        firmwareVer = "";
        ringtoneId = "";
        batteryLevel = "";
        createdAt = "";
        updatedAt = "";
        readedAt = "";
        userId = "";
        organizationId = "";
        userName = "";
        userEmail = "";
        eventCounts = "";
        inviteesCounts = "";
        lastestEventTime = "";
        switched = 1;
        online = THINGSTATUS.EMPTY;
    }

    public static FNThing parseThing(Context context, Thing thing) {
        FNThing fnThing = new FNThing();

        if (thing == null) {
            return null;
        }
        fnThing.thingId = thing._thingId;
        fnThing.productId = thing._productId;
        fnThing.productSlug = thing._productSlug;
        fnThing.identifier = thing._identifier;
        fnThing.name = thing._name;
        fnThing.firmwareVer = thing._firmwareVer;
        fnThing.batteryLevel = thing._batteryLevel;
        fnThing.createdAt = thing._createdAt;
        fnThing.updatedAt = thing._updatedAt;
        fnThing.userId = thing._userId;
        fnThing.organizationId = thing._organizationId;
        fnThing.userName = thing._userName;
        fnThing.userEmail = thing._userEmail;
        fnThing.lastestEventTime = thing._lastestEventTime;
        fnThing.switched = thing._switched;
        fnThing.online = thing._online;
        if (thing._ringtone != null) {
            String sRingtoneId = "";
            try {
                sRingtoneId = String.valueOf(ThingsManager.getInstance(context).getRingtoneIdByFile(thing._ringtone));
            } catch (NumberFormatException e) {
                e.printStackTrace();
            }
            fnThing.ringtoneId = sRingtoneId;
        }
        String sInviteesCount = "0";
        try {
            sInviteesCount = String.valueOf(thing._inviteesCount);
        } catch (NumberFormatException e) {
            e.printStackTrace();
        }
        fnThing.inviteesCounts = sInviteesCount;
        return fnThing;
    }

    public static Thing parseFNThing(Context context, FNThing fnThing) {
        Thing thing = new Thing();

        if (fnThing == null) {
            return null;
        }
        if (fnThing.thingId != null && fnThing.thingId.length() > 0) {
            thing._thingId = fnThing.thingId;
        }
        if (fnThing.productId != null && fnThing.productId.length() > 0) {
            thing._productId = fnThing.productId;
        }
        if (fnThing.productSlug != null && fnThing.productSlug.length() > 0) {
            thing._productSlug = fnThing.productSlug;
        }
        if (fnThing.identifier != null && fnThing.identifier.length() > 0) {
            thing._identifier = fnThing.identifier;
        }
        if (fnThing.name != null && fnThing.name.length() > 0) {
            thing._name = fnThing.name;
        }
        if (fnThing.firmwareVer != null && fnThing.firmwareVer.length() > 0) {
            thing._firmwareVer = fnThing.firmwareVer;
        }
        if (fnThing.batteryLevel != null && fnThing.batteryLevel.length() > 0) {
            thing._batteryLevel = fnThing.batteryLevel;
        }
        if (fnThing.createdAt != null && fnThing.createdAt.length() > 0) {
            thing._createdAt = fnThing.createdAt;
        }
        if (fnThing.updatedAt != null && fnThing.updatedAt.length() > 0) {
            thing._updatedAt = fnThing.updatedAt;
        }

        if (fnThing.userId != null && fnThing.userId.length() > 0) {
            thing._userId = fnThing.userId;
        }
        if (fnThing.organizationId != null && fnThing.organizationId.length() > 0) {
            thing._organizationId = fnThing.organizationId;
        }
        if (fnThing.userName != null && fnThing.userName.length() > 0) {
            thing._userName = fnThing.userName;
        }
        if (fnThing.userEmail != null && fnThing.userEmail.length() > 0) {
            thing._userEmail = fnThing.userEmail;
        }
        if (fnThing.lastestEventTime != null && fnThing.lastestEventTime.length() > 0) {
            thing._lastestEventTime = fnThing.lastestEventTime;
        }
        if (fnThing.ringtoneId != null && fnThing.ringtoneId.length() > 0) {
            int iRingtone;
            try {
                iRingtone = Integer.parseInt(fnThing.getRingtoneId());
            } catch (NumberFormatException e) {
                iRingtone = 0;
                e.printStackTrace();
            }
            thing._ringtone = ThingsManager.getInstance(context).getRingtoneFileById(iRingtone);
        }

        if (fnThing.inviteesCounts != null && fnThing.inviteesCounts.length() > 0) {
            int iInviteesCount;
            try {
                iInviteesCount = Integer.parseInt(fnThing.inviteesCounts);
            } catch (NumberFormatException e) {
                iInviteesCount = 0;
                e.printStackTrace();
            }
            thing._inviteesCount = iInviteesCount;
        }
        thing._switched = fnThing.switched;
        fnThing.online = thing._online;
        return thing;
    }

    public static ArrayList<FNThing> parseThingArray(Context context, ArrayList<Thing> thingsList) {
        ArrayList<FNThing> fnThingList = new ArrayList<>();

        if (thingsList == null) {
            return null;
        }
        for (int i = 0; i < thingsList.size(); i++) {
            Thing thing = thingsList.get(i);
            if (thing != null) {
                FNThing fnThing = parseThing(context, thing);
                fnThingList.add(fnThing);
            }
        }
        return fnThingList;
    }

    public String getThingId() {
        return thingId;
    }

    public void setThingId(String thingId) {
        this.thingId = thingId;
    }

    public String getProductId() {
        return productId;
    }

    public void setProductId(String productId) {
        this.productId = productId;
    }

    public String getProductSlug() {
        return productSlug;
    }

    public void setProductSlug(String productSlug) {
        this.productSlug = productSlug;
    }

    public String getIdentifier() {
        return identifier;
    }

    public void setIdentifier(String identifier) {
        this.identifier = identifier;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getFirmwareVer() {
        return firmwareVer;
    }

    public void setFirmwareVer(String firmwareVer) {
        this.firmwareVer = firmwareVer;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }

    public String getRingtoneId() {
        return ringtoneId;
    }

    public void setRingtoneId(String ringtone) {
        this.ringtoneId = ringtone;
    }

    public String getBatteryLevel() {
        return batteryLevel;
    }

    public void setBatteryLevel(String batteryLevel) {
        this.batteryLevel = batteryLevel;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getOrganizationId() {
        return organizationId;
    }

    public void setOrganizationId(String organizationId) {
        this.organizationId = organizationId;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getUserEmail() {
        return userEmail;
    }

    public void setUserEmail(String userEmail) {
        this.userEmail = userEmail;
    }

    public String getReadedAt() {
        return readedAt;
    }

    public void setReadedAt(String readedAt) {
        this.readedAt = readedAt;
    }

    public String getEventCounts() {
        return eventCounts;
    }

    public void setEventCounts(String eventCounts) {
        this.eventCounts = eventCounts;
    }

    public String getInviteesCounts() {
        return inviteesCounts;
    }

    public void setInviteesCounts(String inviteesCounts) {
        this.inviteesCounts = inviteesCounts;
    }

    public String getLastestEventTime() {
        return lastestEventTime;
    }

    public void setLastestEventTime(String lastestEventTime) {
        this.lastestEventTime = lastestEventTime;
    }

    public int getSwitched() {
        return switched;
    }

    public void setSwitched(int switched) {
        this.switched = switched;
    }

    public THINGSTATUS getOnline() {
        return this.online;
    }

    public void setOnline(THINGSTATUS online) {
        this.online = online;
    }
    public void setOnline(int online) {
    this.online = THINGSTATUS.setScore(online);
    }
}
