package com.fusionnextinc.doorbell;

import android.annotation.SuppressLint;

import androidx.lifecycle.GeneratedAdapter;
import androidx.lifecycle.LifecycleOwner;
import androidx.lifecycle.MethodCallsLogger;
import androidx.lifecycle.Lifecycle.Event;


@SuppressLint("RestrictedApi")
public class InAppUpdateManager_LifecycleAdapter implements GeneratedAdapter {
    final com.fusionnextinc.doorbell.InAppUpdateManager mReceiver;

    InAppUpdateManager_LifecycleAdapter(InAppUpdateManager receiver) {
        this.mReceiver = receiver;
    }

    public void callMethods(LifecycleOwner owner, Event event, boolean onAny, MethodCallsLogger logger) {
        boolean hasLogger = logger != null;
        if (!onAny) {
            if (event == Event.ON_RESUME) {
                if (!hasLogger || logger.approveCall("onResume", 1)) {
                    this.mReceiver.onResume();
                }

            } else if (event == Event.ON_DESTROY) {
                if (!hasLogger || logger.approveCall("onDestroy", 1)) {
                    this.mReceiver.onDestroy();
                }

            }
        }
    }
}

