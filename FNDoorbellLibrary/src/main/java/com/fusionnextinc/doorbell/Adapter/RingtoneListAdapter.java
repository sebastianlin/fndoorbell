package com.fusionnextinc.doorbell.Adapter;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.fusionnextinc.doorbell.MyApplication;
import com.fusionnextinc.doorbell.R;
import com.fusionnextinc.doorbell.manager.thingsManager.ThingsManager;
import com.fusionnextinc.doorbell.widget.FNLayoutUtil;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by Mike Chang on 2017/7/13
 */
public class RingtoneListAdapter extends BaseAdapter {
    private static final String TAG = "RingtoneListAdapter";

    private FNLayoutUtil layoutUtil;
    private Activity activity;
    private ArrayList<HashMap<String, Object>> ringtoneList;
    private int selectedId;

    public RingtoneListAdapter(Activity activity, ArrayList<HashMap<String, Object>> ringtoneList, int selectedId) {
        layoutUtil = new FNLayoutUtil(activity, MyApplication.LAYOUT_WIDTH, MyApplication.LAYOUT_HEIGHT, FNLayoutUtil.BASE_WIDTH);
        this.activity = activity;
        this.ringtoneList = ringtoneList;
        this.selectedId = selectedId;
    }

    @Override
    public int getCount() {
        return ringtoneList.size();
    }

    @Override
    public Object getItem(int position) {
        return ringtoneList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        final ViewHolder holder;
        final ViewGroup nullParent = null;

        if (convertView == null) {
            holder = new ViewHolder();
            convertView = LayoutInflater.from(activity).inflate(R.layout.adapter_ringtone_list, nullParent);
            layoutUtil.fitAllView(convertView);
            holder.txtRingtoneType = (TextView) convertView.findViewById(R.id.txt_ringtone_type);
            holder.imgRingtoneSelect = (ImageView) convertView.findViewById(R.id.img_ringtone_select);
            holder.imgRingtone = (ImageView) convertView.findViewById(R.id.img_ringtone);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }
        holder.txtRingtoneType.setText(ThingsManager.getInstance(activity).getRingtoneNameById(position));
        if (position == selectedId) {
            holder.imgRingtoneSelect.setImageDrawable(activity.getResources().getDrawable(R.drawable.checkbox_selected));
        } else {
            holder.imgRingtoneSelect.setImageDrawable(activity.getResources().getDrawable(R.drawable.checkbox_unselected));
        }

        if(position == ringtoneList.size()-1) {
            holder.imgRingtone.setImageDrawable(activity.getResources().getDrawable(R.drawable.ringtone_off));
        }

        return convertView;
    }

    private class ViewHolder {
        private TextView txtRingtoneType;
        private ImageView imgRingtoneSelect;
        private ImageView imgRingtone;
    }

    public void setSelectedRingtone(int ringtoneId) {
        selectedId = ringtoneId;
        notifyDataSetChanged();
    }

    public int getSelectedRingtone() {
        return selectedId;
    }
}
