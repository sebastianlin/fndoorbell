package com.fusionnext.cloud.datalink.http;

import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;

import com.fusionnext.cloud.utils.FNNetworkLog;

import org.json.JSONException;
import org.json.JSONObject;

import static com.fusionnext.cloud.datalink.http.HttpUtil.BC_HTTP_DOWNLOAD_FEEDBACK;


/**
 * Created by Mike Chang on 2016/2/22.
 */
public class HttpGet extends AsyncTask<HttpParam, HttpReturn, HttpReturn> {
    protected final static String _TAG = "HttpGet";

    private static final String SECTION_HTTP_ACCESS_TAG = "SECTION_HTTP_ACCESS_TAG";
    private static final String SECTION_HTTP_ACCESS_SUCCESSFUL = "SECTION_HTT_ACCESS_SUCCESSFUL";
    private static final String SECTION_HTTP_ACCESS_DATA = "SECTION_HTTP_ACCESS_DATA";

    public static final int _HTTP_T_GET_NO_KEEP_ALIVE_TYPE = 0xDF;
    public static final int _HTTP_T_GET_NORMAL_TYPE = 0xEF;
    public static final int _HTTP_T_GET_DOWNLOAD_FILE = 0xFF;

    private Context context = null;
    private int tag = -1;
    private OnParseJsonListener parseJsonListener = null;
    private HttpUtil.OnDownloadListener downloadListener = null;

    @Override
    protected HttpReturn doInBackground(HttpParam... params) {
        HttpUtil httpUtil = new HttpUtil(context);

        int segment = 0;
        while (segment < params.length) {
            HttpReturn result = new HttpReturn(true, "");

            int successfulCount = 0;
            for (int i = 0; params[segment] != null && params[segment].params != null &&
                    i < params[segment].params.size(); i++) {
                HttpParam.HttpSubParam subParam = params[segment].params.get(i);
                if (subParam == null || subParam.url.equals("")) continue;
                switch (tag) {
                    case _HTTP_T_GET_NO_KEEP_ALIVE_TYPE:
                    case _HTTP_T_GET_NORMAL_TYPE:
                        boolean isKeepAlive = tag != _HTTP_T_GET_NO_KEEP_ALIVE_TYPE;
                        result.returnString = httpUtil.getHttpJSON(subParam.url, subParam.accessToken, isKeepAlive);
                        result.result = (!result.returnString.equals(""));
                        if (!result.returnString.equals("")) {
                            JSONObject rltObject;
                            try {
                                rltObject = new JSONObject(result.returnString);
                                if (rltObject.has("error") && !rltObject.isNull("error")) {
                                    result.returnString = rltObject.toString();
                                    result.result = false;
                                }
                            } catch (JSONException e) {
                                result.result = false;
                                e.printStackTrace();
                            }
                        }
                        return result;
                    case _HTTP_T_GET_DOWNLOAD_FILE:
                    default:
                        FNNetworkLog.i(subParam.url, subParam.cacheName);
                        if (httpUtil.downFileToCache(subParam.url, subParam.cacheName, downloadListener)) {
                            successfulCount++;
                            //Public.Params.bitmapCache.putBitmap(context, subParam.cacheName);
                        }
                        break;
                }

                try {
                    Thread.sleep(50);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }

            if (successfulCount > 0) result.result = true;
            result.returnString = "";

            segment++;
            if (segment < params.length && params[segment] != null) {
                publishProgress(result);
            } else {
                return result;
            }

            try {
                Thread.sleep(50);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        return new HttpReturn(false, "");
    }

    @Override
    protected void onPreExecute() {

        super.onPreExecute();
    }

    @Override
    protected void onProgressUpdate(HttpReturn... values) {
        if (values == null || values[0] == null) return;
        if (context == null) return;

        Bundle bundle = new Bundle();
        bundle.putInt(SECTION_HTTP_ACCESS_TAG, this.tag);
        bundle.putBoolean(SECTION_HTTP_ACCESS_SUCCESSFUL, values[0].result);
        bundle.putString(SECTION_HTTP_ACCESS_DATA, values[0].returnString);

        Intent intent = new Intent(BC_HTTP_DOWNLOAD_FEEDBACK);
        intent.putExtras(bundle);
        context.sendBroadcast(intent);

        //super.onProgressUpdate(values);
    }

    @Override
    protected void onPostExecute(HttpReturn results) {
        if (this.parseJsonListener != null) {
            FNNetworkLog.d(_TAG, "result = " + results.result + ",  resultStr =  " + results.returnString);
            onParse(results.result, results.returnString);
        } else {
            if (results != null && context != null) {
                Bundle bundle = new Bundle();
                bundle.putInt(SECTION_HTTP_ACCESS_TAG, this.tag);
                bundle.putBoolean(SECTION_HTTP_ACCESS_SUCCESSFUL, results.result);
                bundle.putString(SECTION_HTTP_ACCESS_DATA, results.returnString);

                Intent intent = new Intent(BC_HTTP_DOWNLOAD_FEEDBACK);
                intent.putExtras(bundle);
                context.sendBroadcast(intent);
            }
        }

        super.onPostExecute(results);
    }

    public void setContext(Context context) {
        this.context = context;
    }

    public void setTag(int tag) {
        this.tag = tag;
    }

    public void setOnParseJsonListener(OnParseJsonListener parseJsonListener) {
        this.parseJsonListener = parseJsonListener;
    }

    public void setOnDownloadListener(HttpUtil.OnDownloadListener downloadListener) {
        this.downloadListener = downloadListener;
    }

    public interface OnParseJsonListener {
        void onParse(boolean result, String json);
    }

    private void onParse(boolean result, String json) {
        if (parseJsonListener != null) parseJsonListener.onParse(result, json);
    }

}
