package com.fusionnext.cloud.iot.objects;

import android.util.Log;

import com.fusionnext.cloud.datalink.json.FNJsonObject;
import com.fusionnext.cloud.utils.Times;

import org.json.JSONException;

import java.util.TimeZone;

/**
 * Created by Mike Chang on 2017/3/27
 */

public class Device {
    protected final static String _TAG = "Device";

    public String deviceId = "";
    public String clientId = "";
    public String userId = "";
    public String deviceIdentifer = "";
    public String deviceName = "";
    public String manufacturer = "";
    public String model = "";
    public String platform = "";
    public String platformVersion = "";
    public String pushToken = "";
    public String createAt = "";
    public String updatedAt = "";
    public String user = "";

    public Device() {
        deviceId = "";
        clientId = "";
        userId = "";
        deviceIdentifer = "";
        deviceName = "";
        manufacturer = "";
        userId = "";
        platform = "";
        platformVersion = "";
        pushToken = "";
        createAt = "";
        updatedAt = "";
        user = "";
    }

    public void clean() {
        deviceId = "";
        clientId = "";
        userId = "";
        deviceIdentifer = "";
        deviceName = "";
        manufacturer = "";
        userId = "";
        platform = "";
        platformVersion = "";
        pushToken = "";
        createAt = "";
        updatedAt = "";
        user = "";
    }

    public boolean getDataFromJSONObject(FNJsonObject object) {
        clean();
        if (object == null) {
            Log.e(_TAG, "Null JSON Object!");
            return false;
        }

        try {
            deviceId = object.getStringDefault("id");
            clientId = object.getStringDefault("client_id");
            userId = object.getStringDefault("user_id");
            deviceIdentifer = object.getStringDefault("device_identifier");
            deviceName = object.getStringDefault("device_name");
            manufacturer = object.getStringDefault("manufacturer");
            userId = object.getStringDefault("user_id");
            platform = object.getStringDefault("platform");
            platformVersion = object.getStringDefault("platform_ver");
            pushToken = object.getStringDefault("push_token");
            createAt = object.getStringDefault("created_at");
            createAt = Times.converTime(createAt, TimeZone.getDefault());
            updatedAt = object.getStringDefault("updated_at");
            updatedAt = Times.converTime(updatedAt, TimeZone.getDefault());
            user = object.getStringDefault("user");
        } catch (Exception e) {
            e.printStackTrace();
            Log.e(_TAG, "Error occur during parsing JSON object");
            return false;
        }
        return true;
    }

    public static Device parseDevice(String json) {
        try {
            FNJsonObject deviceObject = new FNJsonObject(json);

            Device device = new Device();
            device.getDataFromJSONObject(deviceObject);
            return device;
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return null;
    }
}
