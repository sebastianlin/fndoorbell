package com.fusionnext.cloud.iot.objects;

import android.util.Log;

import com.fusionnext.cloud.datalink.json.FNJsonObject;
import com.fusionnext.cloud.iot.Public;

/**
 * Created by Mike Chang on 2016/1/19.
 */
public class ViewableLink {

    protected final static String _TAG = "Viewable_links";
    protected final static String _JSON_TAG = "viewable_links";

    public String _small;
    public String _medium;
    public String _large;

    public ViewableLink() {
        _small = "";
        _medium = "";
        _large = "";
    }

    public void clean() {
        _small = "";
        _medium = "";
        _large = "";
    }

    public boolean getDataFromJSONObject(final FNJsonObject viewableLinkObj) {
        clean();
        if (viewableLinkObj == null) {
            Log.e(_TAG, "Null JSON Object!");
            return false;
        }

        try {
            FNJsonObject linkObj = viewableLinkObj.getJSONObject(_JSON_TAG);
            if (linkObj != null) {
                FNJsonObject smallPhotoObj = linkObj.getJSONObject("small");
                if (smallPhotoObj != null) {
                    _small = Public._HOST_V1() + smallPhotoObj.getStringDefault("href");
                }
                FNJsonObject mediumPhotoObj = linkObj.getJSONObject("medium");
                if (mediumPhotoObj != null) {
                    _medium = Public._HOST_V1() + mediumPhotoObj.getStringDefault("href");
                }
                FNJsonObject largePhotoObj = linkObj.getJSONObject("large");
                if (largePhotoObj != null) {
                    _large = Public._HOST_V1() + largePhotoObj.getStringDefault("href");
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
            Log.e(_TAG, "Error occur during parsing JSON object");
            return false;
        }
        return true;
    }
}
