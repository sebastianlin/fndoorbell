package com.fusionnextinc.doorbell.voice;

import android.media.AudioFormat;
import android.media.AudioRecord;
import android.media.MediaRecorder;

/**
 * Created by Mike Chang on 2019/4/10
 */
public class FNAudioRecorder {
    private static final String TAG = "FNAudioRecorder";
    private static final int RECORDER_SAMPLERATE = 8000;
    private static final int RECORDER_CHANNELS = AudioFormat.CHANNEL_IN_MONO;
    private static final int RECORDER_AUDIO_ENCODING = AudioFormat.ENCODING_PCM_16BIT;

    private AudioRecord mRecorder = null;
    private int mBufferSize = 640;
    private Thread mRecordingThread = null;
    private boolean mIsRecording = false;
    private OnFNAudioRecorderListener mFNAudioRecorderListener;

    protected void startRecord(OnFNAudioRecorderListener listener) {
        mFNAudioRecorderListener = listener;
//        mBufferSize = AudioRecord.getMinBufferSize
//                (RECORDER_SAMPLERATE, RECORDER_CHANNELS, RECORDER_AUDIO_ENCODING) * 3;

        mRecorder = new AudioRecord(MediaRecorder.AudioSource.MIC,
                RECORDER_SAMPLERATE,
                RECORDER_CHANNELS,
                RECORDER_AUDIO_ENCODING,
                mBufferSize);
        int i = mRecorder.getState();
        if (i == 1)
            mRecorder.startRecording();

        mIsRecording = true;

        mRecordingThread = new Thread(new Runnable() {
            @Override
            public void run() {
                byte data[] = new byte[mBufferSize];
                int read = 0;

                while (mIsRecording) {
                    read = mRecorder.read(data, 0, mBufferSize);

                    if (AudioRecord.ERROR_INVALID_OPERATION != read) {
                        if (mFNAudioRecorderListener != null) {
                            mFNAudioRecorderListener.onRecordData(data);
                        }
                    }
                }
            }
        }, "AudioRecorder Thread");

        mRecordingThread.start();
        if (mFNAudioRecorderListener != null) {
            mFNAudioRecorderListener.onStartRecord(this);
        }
    }

    protected void stopRecord() {
        if (null != mRecorder) {
            mIsRecording = false;

            int i = mRecorder.getState();
            if (i == 1)
                mRecorder.stop();
            mRecorder.release();

            mRecorder = null;
            mRecordingThread = null;
        }

        if (mFNAudioRecorderListener != null) {
            mFNAudioRecorderListener.onStopRecord(this);
        }
    }

    /**
     * 取得是否正在錄音
     *
     * @return 是否正在錄音
     */
    public boolean isRecording() {
        return mIsRecording;
    }

    /**
     * 取得錄音聲壓值
     *
     * @return 錄音聲壓值
     */
    public int getMaxAmplitude() {
        return mRecorder != null ? mRecorder.getSampleRate() : 0;
    }
}
