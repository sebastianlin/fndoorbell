package com.fusionnext.cloud.datalink.json;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.Iterator;

/**
 * Created by Mike Chang on 2016/2/22.
 */
public class FNJsonArray extends JSONArray {

    public FNJsonArray() {
        super();
    }

    public FNJsonArray(String json) throws JSONException {
        super(json);
    }

    private FNJsonObject castJsonObject(JSONObject _object) throws JSONException {
        if (_object == null || _object.keys() == null) return null;

        FNJsonObject mtJsonObject = new FNJsonObject();
        Iterator iterator = _object.keys();
        while (iterator.hasNext()) {
            String key = (String) iterator.next();
            Object jObject = _object.get(key);
            mtJsonObject.put(key, jObject);
        }

        return mtJsonObject;
    }

    @Override
    public FNJsonObject getJSONObject(int index) throws JSONException {
        return castJsonObject(super.getJSONObject(index));
    }
}
