package com.fusionnextinc.doorbell.manager.data;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.fusionnextinc.doorbell.objects.FNEvent;
import com.fusionnextinc.doorbell.objects.FNGroup;
import com.fusionnextinc.doorbell.objects.FNSchedule;
import com.fusionnextinc.doorbell.objects.FNThing;
import com.fusionnextinc.doorbell.objects.FNUserProfile;

import java.util.ArrayList;

/**
 * Created by Mike Chang on 2016/2/22.
 */
public class DataManager extends SQLiteOpenHelper {

    /* Grant the system GPS service to get location
       Use f_int_value type to return
       If the return value is -1, should ask user to use GPS service or not
       If the return value is 0, should disable the feature to get user's location
       If the return value is 1, application can get user's location */
    private final static int _PARAM_USE_EXTERNAL_SD_CACHE = 10100;
    private final static int _PARAM_FIREBASE_PUSH_TOKEN = 30100;

    private final static String NO_SEARCH_ID = "8888";

    private SQLiteDatabase sdb;
    //Database
    private final static int DB_VERSION = 1;
    private final static String DB_NAME = "com.fusionnextinc.doorbell.dat";
    private final static String synchronizedTagDataMgr = "synchronizedTagDataMgr";
    private volatile static DataManager instance;
    private static Context mContext;
    //Setting table
    private final String CREATE_SETTINGS_TABLE = "CREATE TABLE IF NOT EXISTS t_settings " +
            "(_id integer primary key autoincrement, " +
            "f_param integer, " +
            "f_str_value nvarchar(8192), " +
            "f_int_value integer)";

    //Thing table
    private final String CREATE_THING_TABLE = "CREATE TABLE IF NOT EXISTS t_thing" +
            "(thingId nvarchar(20) primary key COLLATE NOCASE, " +
            "productId nvarchar(20), " +
            "productSlug nvarchar(30), " +
            "identifier nvarchar(30), " +
            "name nvarchar(30), " +
            "firmwareVer nvarchar(30), " +
            "batteryLevel nvarchar(20), " +
            "ringtoneId nvarchar(20), " +
            "createdAt nvarchar(50), " +
            "updatedAt nvarchar(50), " +
            "readedAt nvarchar(50), " +
            "userId nvarchar(20), " +
            "userName nvarchar(20), " +
            "userEmail nvarchar(50), " +
            "eventCount nvarchar(20), " +
            "inviteesCount nvarchar(20), " +
            "lastEventTime nvarchar(50), " +
            "switched integer, " +
            "online integer, " +
            "organizationId nvarchar(20))";

    //Event table
    private final String CREATE_EVENT_TABLE = "CREATE TABLE IF NOT EXISTS t_event " +
            "(thingId nvarchar(20) COLLATE NOCASE, " +
            "eventId nvarchar(20), " +
            "name nvarchar(30), " +
            "payload nvarchar(200), " +
            "createdAt nvarchar(50), " +
            "updatedAt nvarchar(50)," +
            "primary key (thingId, eventId))";

    //User table
    private final String CREATE_USER_TABLE = "CREATE TABLE IF NOT EXISTS t_user" +
            "(userId nvarchar(20) primary key COLLATE NOCASE, " +
            "name nvarchar(20), " +
            "email nvarchar(50), " +
            "createdAt nvarchar(50), " +
            "updatedAt nvarchar(50), " +
            "organizationId nvarchar(20))";

    //Group table
    private final String CREATE_GROUP_TABLE = "CREATE TABLE IF NOT EXISTS t_group" +
            "(groupId integer primary key COLLATE NOCASE, " +
            "type nvarchar(20), " +
            "name nvarchar(50), " +
            "deviceCounts integer, " +
            "deviceOnOff integer)";

    //Schedule table
    private final String CREATE_SCHEDULE_TABLE = "CREATE TABLE IF NOT EXISTS t_schedule" +
            "(scheduleId integer COLLATE NOCASE, " +
            "thingId nvarchar(20), " +
            "weeks nvarchar(30), " +
            "hour integer, " +
            "min integer, " +
            "second integer, " +
            "deviceOnOff integer," +
            "switchOnOff integer," +
            "primary key (scheduleId, thingId))";

    //Activity list table
    private final String CREATE_DATA_LIST_TABLE = "CREATE TABLE IF NOT EXISTS t_data_list " +
            "(type integer, " +
            "searchId nvarchar(20) COLLATE NOCASE, " +
            "id nvarchar(20) COLLATE NOCASE, " +
            "item_index integer, " +
            "primary key (type, searchId, id))";


    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(CREATE_SETTINGS_TABLE);
        db.execSQL(CREATE_THING_TABLE);
        db.execSQL(CREATE_EVENT_TABLE);
        db.execSQL(CREATE_USER_TABLE);
        db.execSQL(CREATE_GROUP_TABLE);
        db.execSQL(CREATE_SCHEDULE_TABLE);
        db.execSQL(CREATE_DATA_LIST_TABLE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        //Used tables
        db.execSQL("DROP TABLE IF EXISTS t_settings");
        db.execSQL("DROP TABLE IF EXISTS t_thing");
        db.execSQL("DROP TABLE IF EXISTS t_event");
        db.execSQL("DROP TABLE IF EXISTS t_user");
        db.execSQL("DROP TABLE IF EXISTS t_group");
        db.execSQL("DROP TABLE IF EXISTS t_schedule");
        db.execSQL("DROP TABLE IF EXISTS t_data_list");
        onCreate(db);
    }


    public DataManager(Context context, String name,
                       SQLiteDatabase.CursorFactory factory, int version) {
        super(context, name, factory, version);
    }

    public static DataManager getInstance(Context context) {
        synchronized (synchronizedTagDataMgr) {
            mContext = context;
            if (instance == null) setDataMgr(DataManager.newData(context));
            return instance;
        }
    }

    public static void setDataMgr(DataManager dm) {
        synchronized (synchronizedTagDataMgr) {
            instance = dm;
        }
    }

    public static DataManager newData(Context context) {
        return new DataManager(context, DB_NAME, null, DB_VERSION);
    }

    private String getCursorString(Cursor cursor, String name) {
        if (cursor == null) return "";
        int index = cursor.getColumnIndex(name);
        if (index < 0) return "";
        return cursor.getString(index);
    }

    private int getCursorInt(Cursor cursor, String name, int defaultInt) {
        if (cursor == null) return defaultInt;
        int index = cursor.getColumnIndex(name);
        if (index < 0) return defaultInt;
        return cursor.getInt(index);
    }

    //Check the param setting is exist or not
    private boolean isParamExist(int param) {
        sdb = getReadableDatabase();
        String sql = "select _id from t_settings where f_param = " + param;
        Cursor cursor = null;
        try {
            int count;

            cursor = sdb.rawQuery(sql, null);
            count = cursor.getCount();

            return count > 0;
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            if (cursor != null) cursor.close();
        }

        return false;
    }

    //Get the param value for integer type
    private int getIntValue(int param, int defaultValue) {
        sdb = getReadableDatabase();
        String sql = "select f_param, f_int_value from t_settings where f_param = " + param;
        Cursor cursor = null;
        try {
            int ret = defaultValue;

            cursor = sdb.rawQuery(sql, null);
            if (cursor.getCount() > 0) {
                cursor.moveToFirst();
                ret = cursor.getInt(cursor.getColumnIndex("f_int_value"));
            }
            return ret;

        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            if (cursor != null) cursor.close();
        }

        return defaultValue;
    }

    //Save the param value for integer type
    private boolean setIntValue(int param, int value) {
        sdb = getWritableDatabase();
        String sql;
        Object[] data;
        try {
            if (isParamExist(param)) {
                sql = "update t_settings set f_int_value = ? where f_param = ?";
                data = new Object[]{value, param};
            } else {
                sql = "insert into t_settings (f_param, f_str_value, f_int_value) values(?, ?, ?)";
                data = new Object[]{param, "", value};
            }

            sdb.execSQL(sql, data);
            return true;
        } catch (Exception ex) {
            ex.printStackTrace();
        }

        return false;
    }

    //Get the param value for String type
    private String getStringValue(int param, String defaultValue) {
        sdb = getReadableDatabase();
        String sql = "select f_param, f_str_value from t_settings where f_param = " + param;
        Cursor cursor = null;
        try {
            String ret = defaultValue;

            cursor = sdb.rawQuery(sql, null);
            if (cursor.getCount() > 0) {
                cursor.moveToFirst();
                ret = cursor.getString(cursor.getColumnIndex("f_str_value"));
            }

            return ret;
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            if (cursor != null) cursor.close();
        }

        return defaultValue;
    }

    //Save the param value for integer type
    private boolean setStringValue(int param, String value) {
        sdb = getWritableDatabase();
        String sql;
        Object[] data;
        try {
            if (isParamExist(param)) {
                sql = "update t_settings set f_str_value = ? where f_param = ?";
                data = new Object[]{value, param};
            } else {
                sql = "insert into t_settings (f_param, f_str_value, f_int_value) values(?, ?, ?)";
                data = new Object[]{param, value, 0};
            }

            sdb.execSQL(sql, data);
            return true;
        } catch (Exception ex) {
            ex.printStackTrace();
        }

        return false;
    }

    protected FNThing loadThing(String thingId) {
        if (thingId == null || thingId.equals("")) return null;

        SQLiteDatabase sdb = getReadableDatabase();
        String sql = "select * from t_thing where thingId = '" + thingId + "'";
        Cursor cursor = null;
        try {
            cursor = sdb.rawQuery(sql, null);
            if (cursor.getCount() <= 0) return null;

            FNThing fnThing = new FNThing();
            cursor.moveToFirst();
            fnThing.setThingId(getCursorString(cursor, "thingId"));
            fnThing.setProductId(getCursorString(cursor, "productId"));
            fnThing.setProductSlug(getCursorString(cursor, "productSlug"));
            fnThing.setName(getCursorString(cursor, "name"));
            fnThing.setIdentifier(getCursorString(cursor, "identifier"));
            fnThing.setFirmwareVer(getCursorString(cursor, "firmwareVer"));
            fnThing.setBatteryLevel(getCursorString(cursor, "batteryLevel"));
            fnThing.setRingtoneId(getCursorString(cursor, "ringtoneId"));
            fnThing.setCreatedAt(getCursorString(cursor, "createdAt"));
            fnThing.setUpdatedAt(getCursorString(cursor, "updatedAt"));
            fnThing.setReadedAt(getCursorString(cursor, "readedAt"));
            fnThing.setOrganizationId(getCursorString(cursor, "organizationId"));
            fnThing.setUserId(getCursorString(cursor, "userId"));
            fnThing.setUserName(getCursorString(cursor, "userName"));
            fnThing.setUserEmail(getCursorString(cursor, "userEmail"));
            fnThing.setEventCounts(getCursorString(cursor, "eventCount"));
            fnThing.setInviteesCounts(getCursorString(cursor, "inviteesCount"));
            fnThing.setLastestEventTime(getCursorString(cursor, "lastEventTime"));
            fnThing.setSwitched(getCursorInt(cursor, "switched", 0));
            fnThing.setOnline(getCursorInt(cursor, "online", -1));
            return fnThing;
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            if (cursor != null) cursor.close();
        }

        return null;
    }

    protected boolean saveThing(FNThing fnThing) {
        //If the user is not exist, needn't save it, return true as successful
        if (fnThing == null || fnThing.getThingId() == null || fnThing.getThingId().equals(""))
            return true;

        SQLiteDatabase sdb = getWritableDatabase();
        try {
            String sql;
            Object[] data;
            FNThing oldFnThing = loadThing(fnThing.getThingId());
            if (oldFnThing != null) {
                String fwVersion, readedAt, batteryLevel, ringtoneId, eventCount;
                fwVersion = (fnThing.getFirmwareVer().length() == 0) ? oldFnThing.getFirmwareVer() : fnThing.getFirmwareVer();
                readedAt = (fnThing.getReadedAt().length() == 0) ? oldFnThing.getReadedAt() : fnThing.getReadedAt();
                batteryLevel = (fnThing.getBatteryLevel().length() == 0) ? oldFnThing.getBatteryLevel() : fnThing.getBatteryLevel();
                ringtoneId = (fnThing.getRingtoneId().length() == 0) ? oldFnThing.getRingtoneId() : fnThing.getRingtoneId();
                eventCount = (fnThing.getEventCounts().length() == 0) ? oldFnThing.getEventCounts() : fnThing.getEventCounts();
                sql = "update t_thing set productId = ?, productSlug = ?,  identifier = ?, name = ?,  firmwareVer = ?, batteryLevel = ?,  " +
                        "ringtoneId = ?, createdAt = ?, updatedAt = ?, readedAt = ?, userId = ?,  userName = ?, userEmail = ?, eventCount = ?, " +
                        "inviteesCount =?,  lastEventTime =?, organizationId = ?,  switched =?, online = ?  where thingId = ? ";
                data = new Object[]{fnThing.getProductId(), fnThing.getProductSlug(), fnThing.getIdentifier(), fnThing.getName(),
                        fwVersion, batteryLevel, ringtoneId, fnThing.getCreatedAt(), fnThing.getUpdatedAt(), readedAt, fnThing.getUserId(),
                        fnThing.getUserName(), fnThing.getUserEmail(), eventCount, fnThing.getInviteesCounts(), fnThing.getLastestEventTime(),
                        fnThing.getOrganizationId(), fnThing.getSwitched(), fnThing.getOnline().getScore(), fnThing.getThingId()};
            } else {
                sql = "insert into t_thing (thingId, productId, productSlug,  identifier, name, firmwareVer, batteryLevel,  ringtoneId, createdAt, " +
                        "updatedAt, readedAt, userId, userName, userEmail, eventCount, inviteesCount, lastEventTime, organizationId, switched, " +
                        "online) values(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
                data = new Object[]{fnThing.getThingId(), fnThing.getProductId(), fnThing.getProductSlug(), fnThing.getIdentifier(),
                        fnThing.getName(), fnThing.getFirmwareVer(), fnThing.getBatteryLevel(), fnThing.getRingtoneId(), fnThing.getCreatedAt(),
                        fnThing.getUpdatedAt(), fnThing.getReadedAt(), fnThing.getUserId(), fnThing.getUserName(), fnThing.getUserEmail(),
                        fnThing.getEventCounts(), fnThing.getInviteesCounts(), fnThing.getLastestEventTime(), fnThing.getOrganizationId(),
                        fnThing.getSwitched(), fnThing.getOnline().getScore()};
            }
            sdb.execSQL(sql, data);

            return true;
        } catch (Exception ex) {
            ex.printStackTrace();
        }

        return false;
    }

    public boolean deleteThing(String thingId) {
        SQLiteDatabase sdb = getWritableDatabase();
        try {
            String sql = "delete from t_thing where thingId = '" + thingId + "'";
            sdb.execSQL(sql);
            return true;
        } catch (Exception ex) {
            ex.printStackTrace();
        }

        return false;
    }

    protected FNEvent loadEvent(String thingId, String eventId) {
        if (thingId == null || thingId.equals("")) return null;

        SQLiteDatabase sdb = getReadableDatabase();
        String sql = "select * from t_event where thingId = '" + thingId + "' and eventId = '" + eventId + "'";
        Cursor cursor = null;
        try {
            cursor = sdb.rawQuery(sql, null);
            if (cursor.getCount() <= 0) return null;

            FNEvent fnEvent = new FNEvent();
            cursor.moveToFirst();
            fnEvent.setEventId(getCursorString(cursor, "eventId"));
            fnEvent.setThingId(getCursorString(cursor, "thingId"));
            fnEvent.setName(getCursorString(cursor, "name"));
            fnEvent.setPayload(getCursorString(cursor, "payload"));
            fnEvent.setCreatedAt(getCursorString(cursor, "createdAt"));
            fnEvent.setUpdatedAt(getCursorString(cursor, "updatedAt"));
            return fnEvent;
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            if (cursor != null) cursor.close();
        }

        return null;
    }

    public boolean saveEvent(FNEvent fnEvent) {
        //If the user is not exist, needn't save it, return true as successful
        if (fnEvent == null || fnEvent.getThingId() == null || fnEvent.getThingId().equals(""))
            return true;

        SQLiteDatabase sdb = getWritableDatabase();
        try {
            String sql;
            Object[] data;
            if (loadEvent(fnEvent.getThingId(), fnEvent.getEventId()) != null) {
                sql = "update t_event set payload = ?,  name = ?, createdAt = ?, updatedAt = ?  where thingId = ? and eventId = ?";
                data = new Object[]{fnEvent.getPayload(), fnEvent.getName(), fnEvent.getCreatedAt(), fnEvent.getUpdatedAt(), fnEvent.getThingId(), fnEvent.getEventId()};
            } else {
                sql = "insert into t_event (thingId, eventId, name, payload, createdAt, updatedAt) values(?,?,?,?,?,?)";
                data = new Object[]{fnEvent.getThingId(), fnEvent.getEventId(), fnEvent.getName(), fnEvent.getPayload(), fnEvent.getCreatedAt(), fnEvent.getUpdatedAt()};
            }
            sdb.execSQL(sql, data);

            return true;
        } catch (Exception ex) {
            ex.printStackTrace();
        }

        return false;
    }

    public boolean deleteEvent(String thingId, String eventId) {
        SQLiteDatabase sdb = getWritableDatabase();
        try {
            String sql = "delete from t_event where thingId = '" + thingId + "' and eventId = '" + eventId + "'";
            sdb.execSQL(sql);
            return true;
        } catch (Exception ex) {
            ex.printStackTrace();
        }

        return false;
    }

    protected FNUserProfile loadUser(String userId) {
        if (userId == null || userId.equals("")) return null;

        SQLiteDatabase sdb = getReadableDatabase();
        String sql = "select * from t_user where userId = '" + userId + "'";
        Cursor cursor = null;
        try {
            cursor = sdb.rawQuery(sql, null);
            if (cursor.getCount() <= 0) return null;

            FNUserProfile fnUserProfile = new FNUserProfile();
            cursor.moveToFirst();
            fnUserProfile.setUserId(getCursorString(cursor, "userId"));
            fnUserProfile.setOrganizationId(getCursorString(cursor, "organizationId"));
            fnUserProfile.setName(getCursorString(cursor, "name"));
            fnUserProfile.setEmail(getCursorString(cursor, "email"));
            fnUserProfile.setCreatedAt(getCursorString(cursor, "createdAt"));
            fnUserProfile.setUpdatedAt(getCursorString(cursor, "updatedAt"));
            return fnUserProfile;
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            if (cursor != null) cursor.close();
        }
        return null;
    }

    public boolean saveUser(FNUserProfile fnUserProfile) {
        //If the user is not exist, needn't save it, return true as successful
        if (fnUserProfile == null || fnUserProfile.getUserId() == null || fnUserProfile.getUserId().equals(""))
            return true;

        SQLiteDatabase sdb = getWritableDatabase();
        try {
            String sql;
            Object[] data;
            if (loadUser(fnUserProfile.getUserId()) != null) {
                sql = "update t_user set name = ?,  email = ?, createdAt = ?, updatedAt = ?, organizationId = ?  where userId = ?";
                data = new Object[]{fnUserProfile.getName(), fnUserProfile.getEmail(), fnUserProfile.getCreatedAt(), fnUserProfile.getUpdatedAt(), fnUserProfile.getOrganizationId(), fnUserProfile.getUserId()};
            } else {
                sql = "insert into t_user (userId, name, email, createdAt, updatedAt, organizationId) values(?,?,?,?,?,?)";
                data = new Object[]{fnUserProfile.getUserId(), fnUserProfile.getName(), fnUserProfile.getEmail(), fnUserProfile.getCreatedAt(), fnUserProfile.getUpdatedAt(), fnUserProfile.getOrganizationId()};
            }
            sdb.execSQL(sql, data);
            return true;
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return false;
    }

    public boolean deleteUser(String userId) {
        SQLiteDatabase sdb = getWritableDatabase();
        try {
            String sql = "delete from t_user where userId = '" + userId + "'";
            sdb.execSQL(sql);
            return true;
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return false;
    }

    protected FNGroup loadGroup(int groupId) {
        if (groupId < 0) return null;

        SQLiteDatabase sdb = getReadableDatabase();
        String sql = "select * from t_group where groupId = '" + groupId + "'";
        Cursor cursor = null;
        try {
            cursor = sdb.rawQuery(sql, null);
            if (cursor.getCount() <= 0) return null;

            cursor.moveToFirst();

            FNGroup fnGroup = new FNGroup(getCursorInt(cursor, "deviceOnOff", 0) == 0 ? false : true,
                    groupId,
                    getCursorString(cursor, "type"),
                    getCursorString(cursor, "name"),
                    getCursorInt(cursor, "deviceCounts", 0));
            return fnGroup;
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            if (cursor != null) cursor.close();
        }
        return null;
    }

    public boolean saveGroup(FNGroup fnGroup) {
        //If the user is not exist, needn't save it, return false as successful
        if (fnGroup == null || fnGroup.getGroupId() < 0)
            return false;

        SQLiteDatabase sdb = getWritableDatabase();
        try {
            String sql;
            Object[] data;
            if (loadGroup(fnGroup.getGroupId()) != null) {
                sql = "update t_group set type = ?, name = ?, deviceCounts = ?, deviceOnOff = ?  where groupId = ?";
                data = new Object[]{fnGroup.getGroupType(), fnGroup.getGroupName(), fnGroup.getDeviceCount(), fnGroup.isDeviceOn(), fnGroup.getGroupId()};
            } else {
                sql = "insert into t_group (groupId, type, name, deviceCounts, deviceOnOff) values(?,?,?,?,?)";
                data = new Object[]{fnGroup.getGroupId(), fnGroup.getGroupType(), fnGroup.getGroupName(), fnGroup.getDeviceCount(), fnGroup.isDeviceOn()};
            }
            sdb.execSQL(sql, data);
            return true;
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return false;
    }

    public boolean deleteGroup(int groupId) {
        SQLiteDatabase sdb = getWritableDatabase();
        try {
            String sql = "delete from t_group where groupId = '" + groupId + "'";
            sdb.execSQL(sql);
            return true;
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return false;
    }

    protected FNSchedule loadSchedule(int scheduleId, String thingId) {
        if (scheduleId < 0) return null;

        SQLiteDatabase sdb = getReadableDatabase();
        String sql = "select * from t_schedule where scheduleId = '" + scheduleId + "' and thingId = '" + thingId + "'";
        Cursor cursor = null;
        try {
            cursor = sdb.rawQuery(sql, null);
            if (cursor.getCount() <= 0) return null;

            cursor.moveToFirst();
            FNSchedule fnSchedule = new FNSchedule(scheduleId,
                    getCursorInt(cursor, "deviceOnOff", 0) == 0 ? false : true,
                    getCursorInt(cursor, "switchOnOff", 0) == 0 ? false : true,
                    getCursorInt(cursor, "hour", 0),
                    getCursorInt(cursor, "min", 0),
                    getCursorInt(cursor, "second", 0),
                    getCursorString(cursor, "weeks"));
            return fnSchedule;
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            if (cursor != null) cursor.close();
        }
        return null;
    }

    public boolean saveSchedule(FNSchedule fnSchedule, String thingId) {
        //If the user is not exist, needn't save it, return false as successful
        if (fnSchedule == null || fnSchedule.getScheduleId() < 0)
            return false;

        SQLiteDatabase sdb = getWritableDatabase();
        try {
            String sql;
            Object[] data;
            if (loadSchedule(fnSchedule.getScheduleId(), thingId) != null) {
                sql = "update t_schedule set hour = ?, min = ?, second = ?, deviceOnOff = ?, switchOnOff = ? , weeks = ? where scheduleId = ? and thingId = ?";
                data = new Object[]{fnSchedule.getHour(), fnSchedule.getMin(), fnSchedule.getSec(), fnSchedule.isDeviceOn(), fnSchedule.isSwitchOn(), fnSchedule.getStrWeeks(), fnSchedule.getScheduleId(), thingId};
            } else {
                sql = "insert into t_schedule (scheduleId, thingId, weeks, hour, min, second, deviceOnOff, switchOnOff) values(?,?,?,?,?,?,?,?)";
                data = new Object[]{fnSchedule.getScheduleId(), thingId, fnSchedule.getStrWeeks(), fnSchedule.getHour(), fnSchedule.getMin(), fnSchedule.getSec(), fnSchedule.isDeviceOn(), fnSchedule.isSwitchOn()};
            }
            sdb.execSQL(sql, data);
            return true;
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return false;
    }

    public boolean deleteSchedule(int scheduleId) {
        SQLiteDatabase sdb = getWritableDatabase();
        try {
            String sql = "delete from t_schedule where scheduleId = '" + scheduleId + "'";
            sdb.execSQL(sql);
            return true;
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return false;
    }

    private synchronized ArrayList<String> loadDataList(int type, String searchId) {
        SQLiteDatabase sdb = getReadableDatabase();
        Cursor cursor;
        ArrayList<String> list = new ArrayList<>();
        list.clear();
        try {
            String sql;
            if (searchId == null || searchId.trim().equals("") || searchId.equals(NO_SEARCH_ID)) {
                sql = "select * from t_data_list where type = " + String.valueOf(type) +
                        " order by item_index ASC";
            } else {
                sql = "select * from t_data_list where type = " + String.valueOf(type) + " and searchId = " + searchId +
                        " order by item_index ASC";
            }

            cursor = sdb.rawQuery(sql, null);
            if (cursor.getCount() > 0) {
                cursor.moveToFirst();
                while (true) {
                    list.add(cursor.getString(cursor.getColumnIndex("id")));

                    if (cursor.isLast()) break;
                    else cursor.moveToNext();
                }
            }
        } catch (Exception ex) {
            list.clear();
            ex.printStackTrace();
        }

        return list;
    }

    public synchronized boolean saveDataList(int type, String searchId, ArrayList<String> list) {
        if (list == null) list = new ArrayList<>();

        SQLiteDatabase sdb = getWritableDatabase();
        try {
            if (deleteDataList(type, searchId, null)) {
                for (int i = 0; i < list.size(); i++) {
                    String sql = "insert into t_data_list (type, searchId, id, item_index) values(?,?,?,?)";
                    Object[] data = new Object[]{type, searchId, list.get(i), i};
                    sdb.execSQL(sql, data);
                }
                return true;
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return false;
    }

    private boolean deleteDataList(int type, String searchId, String id) {
        SQLiteDatabase sdb = getWritableDatabase();
        try {
            String sql;
            Object[] data;
            if ((searchId == null || searchId.trim().equals("")) && (id == null || id.trim().equals(""))) {
                sql = "delete from t_data_list where type = ?";
                data = new Object[]{type};
            } else if (searchId != null && !searchId.trim().equals("") && id == null) {
                sql = "delete from t_data_list where type = ? and searchId = ?";
                data = new Object[]{type, searchId};
            } else {
                sql = "delete from t_data_list where type = ? and searchId = ? and id = ?";
                data = new Object[]{type, searchId, id};
            }
            sdb.execSQL(sql, data);
            return true;
        } catch (Exception ex) {
            ex.printStackTrace();
        }

        return false;
    }


    public synchronized void resetCacheData() {
        SQLiteDatabase sdb = getWritableDatabase();
        try {
            sdb.execSQL("delete from t_thing");
            sdb.execSQL("delete from t_event");
            sdb.execSQL("delete from t_user");
            sdb.execSQL("delete from t_group");
            sdb.execSQL("delete from t_schedule");
            sdb.execSQL("delete from t_data_list");
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    public synchronized boolean saveEventList(int type, String thingId, ArrayList<String> list) {
        return saveDataList(type, thingId, list);
    }

    public synchronized ArrayList<String> loadEventList(int type, String thingId) {
        return loadDataList(type, thingId);
    }

    public synchronized boolean saveThingList(int type, String groupId, ArrayList<String> list) {
        if (groupId == null) {
            return saveDataList(type, NO_SEARCH_ID, list);
        } else {
            return saveDataList(type, groupId, list);
        }
    }

    public synchronized ArrayList<String> loadThingList(int type, String groupId) {
        if (groupId == null) {
            return loadDataList(type, NO_SEARCH_ID);
        } else {
            return loadDataList(type, groupId);
        }
    }

    public synchronized boolean saveUserList(int type, String thingId, ArrayList<String> list) {
        return saveDataList(type, thingId, list);
    }

    public synchronized ArrayList<String> loadUserList(int type, String thingId) {
        return loadDataList(type, thingId);
    }

    public synchronized boolean saveGroupList(ArrayList<String> list) {
        return saveDataList(AppDataBase.GROUP_TYPE_NORMAL, NO_SEARCH_ID, list);
    }

    public synchronized boolean deleteGroupList(String groupId) {
        return deleteDataList(AppDataBase.GROUP_TYPE_NORMAL, NO_SEARCH_ID, groupId);
    }

    public synchronized ArrayList<String> loadGroupList() {
        return loadDataList(AppDataBase.GROUP_TYPE_NORMAL, NO_SEARCH_ID);
    }

    public synchronized boolean saveScheduleList(ArrayList<String> list, String thingId) {
        return saveDataList(AppDataBase.SCHEDULE_TYPE_NORMAL, thingId, list);
    }

    public synchronized boolean deleteScheduleList(String thingId, String scheduleId) {
        return deleteDataList(AppDataBase.SCHEDULE_TYPE_NORMAL, thingId, scheduleId);
    }

    public synchronized ArrayList<String> loadScheduleList(String thingId) {
        return loadDataList(AppDataBase.SCHEDULE_TYPE_NORMAL, thingId);
    }

    public boolean loadUseExternalSdCache(boolean defaultValue) {
        int defIntValue = defaultValue ? 1 : 0;
        return (getIntValue(_PARAM_USE_EXTERNAL_SD_CACHE, defIntValue) == 1);
    }

    public boolean saveFireBasePushToken(String pushToken) {
        return setStringValue(_PARAM_FIREBASE_PUSH_TOKEN, pushToken);
    }

    //Get device token
    public String loadFireBasePushToken() {
        return getStringValue(_PARAM_FIREBASE_PUSH_TOKEN, "");
    }
}
