package com.fusionnextinc.doorbell.voice;

import android.media.AudioFormat;
import android.media.AudioManager;
import android.media.AudioTrack;

/**
 * Created by Mike Chang on 2019/4/10
 */

public class FNAudioPlayer {
    private static final String TAG = "FNAudioPlayer";
    private static final int RECORDER_SAMPLERATE = 8000;
    private static final int RECORDER_CHANNELS = AudioFormat.CHANNEL_OUT_MONO;
    private static final int RECORDER_AUDIO_ENCODING = AudioFormat.ENCODING_PCM_16BIT;

    private AudioTrack mAudioTrack;

    /**
     * 初始化
     */
    private void init() {
        if (mAudioTrack == null) {
            mAudioTrack = new AudioTrack(AudioManager.STREAM_VOICE_CALL,
                    RECORDER_SAMPLERATE, RECORDER_CHANNELS,
                    RECORDER_AUDIO_ENCODING, 640,
                    AudioTrack.MODE_STREAM);
        }
    }

    /**
     * 播放
     *
     * @param data 聲音檔案路徑
     */
    public void play(byte[] data) {
        init();
        if (data == null || mAudioTrack == null) {
            return;
        }
        if (mAudioTrack.getPlayState() != AudioTrack.PLAYSTATE_PLAYING) {
            mAudioTrack.play();
        }
        if (mAudioTrack.getPlayState() == AudioTrack.PLAYSTATE_PLAYING) {
//            wipe(data, 0, data.length);
            mAudioTrack.write(data, 0, data.length);
        }
    }


    /**
     * 暫停
     */
    public void pause() {
        if (mAudioTrack == null) {
            return;
        }
        mAudioTrack.pause();
    }

    /**
     * 停止
     */
    public void stop() {
        if (mAudioTrack == null) {
            return;
        }
        mAudioTrack.stop();
        mAudioTrack.release();
        mAudioTrack = null;
    }

    /**
     * 銷毀
     */
    public void destroy() {
        if (mAudioTrack == null) {
            return;
        }
        mAudioTrack.stop();
        mAudioTrack.release();
        mAudioTrack = null;
    }

    /**
     * 消除噪音
     */
    private void wipe(byte[] lin, int off, int len) {
        int i, j;
        for (i = 0; i < len; i++) {
            j = lin[i + off];
            lin[i + off] = (byte) (j >> 2);
        }
    }
}
