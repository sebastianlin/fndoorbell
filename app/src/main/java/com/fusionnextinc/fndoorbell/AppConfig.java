package com.fusionnextinc.fndoorbell;

import com.fusionnextinc.doorbell.DefaultAppConfig;
import com.fusionnextinc.doorbell.utils.FNLog;

/**
 * Created by chocoyang on 2017/6/23.
 */
public class AppConfig {
    public static void initConfig() {
        FNLog.i("AppConfig","initConfig():" + BuildConfig.APP_SOLY);
        DefaultAppConfig.VERSION_NAME = BuildConfig.VERSION_NAME;
        DefaultAppConfig.FLAVOR = BuildConfig.FLAVOR;
        DefaultAppConfig.SHOW_LOG = BuildConfig.SHOW_LOG;
        DefaultAppConfig.APP_SOLY = BuildConfig.APP_SOLY;
        DefaultAppConfig.MQTT_ENABLE = BuildConfig.MQTT_ENABLE;
        DefaultAppConfig.BATTERY_LEVEL_BY_TEXT = BuildConfig.BATTERY_LEVEL_BY_TEXT;
    }
}
