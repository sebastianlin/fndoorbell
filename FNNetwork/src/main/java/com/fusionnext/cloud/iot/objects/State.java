package com.fusionnext.cloud.iot.objects;

import android.util.Log;

import com.fusionnext.cloud.datalink.json.FNJsonArray;
import com.fusionnext.cloud.datalink.json.FNJsonObject;
import com.fusionnext.cloud.utils.Times;

import org.json.JSONException;

import java.util.ArrayList;
import java.util.TimeZone;

/**
 * Created by Mike Chang on 2016/6/14
 */
public class State {

    protected final static String TAG = "State";

    public State() {
        _stateId = "";
        _thingId = "";
        _name = "";
        _value = "";
        _createdAt = "";
        _updatedAt = "";
    }

    public String _stateId;
    public String _thingId;
    public String _name;
    public String _value;
    public String _createdAt;  // user's created_at
    public String _updatedAt; // user's updated_at

    public void clean() {
        _stateId = "";
        _thingId = "";
        _name = "";
        _value = "";
        _createdAt = "";
        _updatedAt = "";
    }

    public boolean getDataFromJSONObject(FNJsonObject stateObj) {
        clean();
        if (stateObj == null) {
            Log.w(TAG, " No JSON information");
            return false;
        }
        try {
            _stateId = stateObj.getStringDefault("id");
            _thingId = stateObj.getStringDefault("thing_id");
            _name = stateObj.getStringDefault("name");
            _value = stateObj.getStringDefault("value");
            _createdAt = stateObj.getStringDefault("created_at");
            _createdAt = Times.converTime(_createdAt, TimeZone.getDefault());
            _updatedAt = stateObj.getStringDefault("updated_at");
            _updatedAt = Times.converTime(_updatedAt, TimeZone.getDefault());
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
        return true;
    }

    public static State parseState(String json) {
        try {
            FNJsonObject stateObject = new FNJsonObject(json);

            State state = new State();
            state.getDataFromJSONObject(stateObject);
            return state;
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static ArrayList<State> parseStateArray(String json) {
        try {
            ArrayList<State> stateList = new ArrayList<State>();
            FNJsonObject stateObject = new FNJsonObject(json);
            FNJsonArray stateArray = stateObject.getJSONArray("items");
            if (stateArray == null) {
                return null;
            }
            for (int i = 0; i < stateArray.length(); i++) {
                FNJsonObject object = stateArray.getJSONObject(i);
                State state = new State();
                state.getDataFromJSONObject(object);
                if (state == null) return null;
                stateList.add(state);
            }
            return stateList;
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return null;
    }
}
