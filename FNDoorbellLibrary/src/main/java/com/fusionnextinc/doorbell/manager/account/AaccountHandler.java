package com.fusionnextinc.doorbell.manager.account;

import com.fusionnextinc.doorbell.objects.FNError;

/**
 * Created by Mike Chang on 2017/6/3
 */
public abstract class AaccountHandler {

    public interface AccountListener {
        void onUserRegistered(AccountManager instance, boolean result, FNError error);

        void onUserLogined(AccountManager instance, boolean result, FNError error);

        void onGetUserInfo(AccountManager instance, boolean result, FNError error);

        void onUpdateUserInfo(AccountManager instance, boolean result, FNError error);

        void onUserLogout(AccountManager instance, boolean result, FNError error);

        void onResetPassword(AccountManager instance, boolean result, FNError error);

        void onSetPassword(AccountManager instance, boolean result, FNError error);
    }
}
