package com.fusionnextinc.doorbell;

/**
 * Created by chocoyang on 2017/6/23.
 */
public class DefaultAppConfig {
    public static String VERSION_NAME = "0.0";
    public static String FLAVOR = "fusionnext";
    public static boolean TRACK_LOG = false;
    public static boolean SHOW_LOG = false;
    public static boolean APP_SOLY = false;
    public static boolean MQTT_ENABLE = true;
    public static boolean BATTERY_LEVEL_BY_TEXT = false;
}
