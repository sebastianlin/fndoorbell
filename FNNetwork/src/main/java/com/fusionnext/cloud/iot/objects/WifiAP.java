package com.fusionnext.cloud.iot.objects;

import android.util.Log;

import com.fusionnext.cloud.datalink.json.FNJsonArray;
import com.fusionnext.cloud.datalink.json.FNJsonObject;

import org.json.JSONException;

import java.util.ArrayList;

/**
 * Created by Mike Chang on 2018/3/8
 */

public class WifiAP {
    protected final static String _TAG = "WifiAP";

    public String _ssid;
    public String _sec;
    public int _dbm;

    public WifiAP() {
        _ssid = "";
        _sec = "";
        _dbm = 0;
    }

    public void clean() {
        _ssid = "";
        _sec = "";
        _dbm = 0;
    }

    public boolean getDataFromJSONObject(FNJsonObject inviteObj) {
        clean();
        if (inviteObj == null) {
            Log.e(_TAG, "Null JSON Object!");
            return false;
        }

        try {
            //Get the id
            _ssid = inviteObj.getStringDefault("ssid");
            _sec = inviteObj.getStringDefault("sec");
            _dbm = inviteObj.getIntDefault("dbm");
        } catch (Exception e) {
            e.printStackTrace();
            Log.e(_TAG, "Error occur during parsing JSON object");
            return false;
        }
        return true;
    }

    public static ArrayList<WifiAP> parseWifiAPArray(String json) {
        try {
            ArrayList<WifiAP> wifiList = new ArrayList<WifiAP>();
            FNJsonObject wifiAPObject = new FNJsonObject(json);
            FNJsonArray wifiAPArray = wifiAPObject.getJSONArray("wifi");
            if (wifiAPArray == null) {
                return null;
            }
            for (int i = 0; i < wifiAPArray.length(); i++) {
                FNJsonObject object = wifiAPArray.getJSONObject(i);
                WifiAP wifiAP = new WifiAP();
                wifiAP.getDataFromJSONObject(object);
                if (wifiAP == null) return null;
                wifiList.add(wifiAP);
            }
            return wifiList;
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return null;
    }
}
