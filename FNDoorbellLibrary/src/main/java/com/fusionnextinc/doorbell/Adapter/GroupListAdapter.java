package com.fusionnextinc.doorbell.Adapter;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.fusionnextinc.doorbell.MyApplication;
import com.fusionnextinc.doorbell.R;
import com.fusionnextinc.doorbell.manager.data.AppDataBase;
import com.fusionnextinc.doorbell.manager.data.AppDataGroup;
import com.fusionnextinc.doorbell.manager.data.AppDataThing;
import com.fusionnextinc.doorbell.manager.data.ListMap;
import com.fusionnextinc.doorbell.manager.thingsManager.ThingsManager;
import com.fusionnextinc.doorbell.widget.FNLayoutUtil;
import com.fusionnextinc.doorbell.widget.FNProgressDialog;
import com.fusionnextinc.doorbell.widget.FNSwitch;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by Mike Chang on 2018/8/7
 */
public class GroupListAdapter extends BaseAdapter {
    private static final String TAG = "GroupListAdapter";

    private FNLayoutUtil mLayoutUtil;
    private Activity mActivity;
    private ListMap mGroupList = new ListMap();
    private boolean mIsLongPressed = false;
    // ????CheckBox?????
    private static HashMap<Integer, Boolean> mIsSelected = new HashMap<Integer, Boolean>();
    private FNProgressDialog progressBar;
    private static final int GROUP_THING_SWITCH_DURATION = 300;

    public GroupListAdapter(Activity activity, ListMap groupList) {
        mLayoutUtil = new FNLayoutUtil(activity, MyApplication.LAYOUT_WIDTH, MyApplication.LAYOUT_HEIGHT, FNLayoutUtil.BASE_WIDTH);
        this.mActivity = activity;
        this.mGroupList.addAll(groupList);
        initSelectedData();
    }

    @Override
    public int getCount() {
        return mGroupList.size();
    }

    @Override
    public Object getItem(int position) {
        return mGroupList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        final ViewHolder holder;
        final ViewGroup nullParent = null;

        if (convertView == null) {
            holder = new ViewHolder();
            convertView = LayoutInflater.from(mActivity).inflate(R.layout.adapter_group_list, nullParent);
            mLayoutUtil.fitAllView(convertView);
            holder.txtGroupName = convertView.findViewById(R.id.textName);
            holder.txtDeviceCount = convertView.findViewById(R.id.textDeviceCount);
            holder.imgGroup = convertView.findViewById(R.id.imageGroup);
            holder.swDeviceOnOff = convertView.findViewById(R.id.sw_deviceOnOff);
            holder.chkGroup = convertView.findViewById(R.id.img_group_select);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        final Map<String, Object> mapGroup = mGroupList.get(position);
        if (mapGroup.size() > 0) {
            String groupType = (String) mapGroup.get(AppDataGroup.TAG_GROUP_TYPE);
            final int groupId = (int) mapGroup.get(AppDataGroup.TAG_GROUP_ID);

            holder.txtGroupName.setText((String) mapGroup.get(AppDataGroup.TAG_GROUP_NAME));
            holder.txtDeviceCount.setText((int) mapGroup.get(AppDataGroup.TAG_GROUP_DEVICE_COUNT)
                    + " " + mActivity.getString(R.string.fn_title_tabbar_device));
            holder.imgGroup.setImageResource(ThingsManager.getInstance(mActivity).getGroupImageByType(groupType));

            holder.swDeviceOnOff.toggle();
            holder.swDeviceOnOff.setOnCheckedChangeListener(new FNSwitch.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(FNSwitch fnSwitch, final boolean isChecked) {
                    final ListMap groupOwnedThingsList = new ListMap();
                    final ListMap groupSharedThingsList = new ListMap();

                    groupOwnedThingsList.addAll(AppDataThing.getInstance(mActivity).getAdapterData(AppDataBase.THING_TYPE_THINGS_GROUP_OWNED, String.valueOf(groupId)));
                    groupSharedThingsList.addAll(AppDataThing.getInstance(mActivity).getAdapterData(AppDataBase.THING_TYPE_THINGS_GROUP_SHARED, String.valueOf(groupId)));
                    int deviceCounts = groupOwnedThingsList.size() + groupSharedThingsList.size();
                    if (deviceCounts > 0) {
                        startWait(deviceCounts * GROUP_THING_SWITCH_DURATION);
                        new Thread(new Runnable() {
                            public void run() {
                                for (int i = 0; i < groupOwnedThingsList.size(); i++) {
                                    String thingId = (String) groupOwnedThingsList.get(i).get(AppDataThing.TAG_THING_ID);
                                    ThingsManager.getInstance(mActivity).switchOnOffSmartPlug(thingId, isChecked);
                                }
                                for (int i = 0; i < groupSharedThingsList.size(); i++) {
                                    String thingId = (String) groupSharedThingsList.get(i).get(AppDataThing.TAG_THING_ID);
                                    ThingsManager.getInstance(mActivity).switchOnOffSmartPlug(thingId, isChecked);
                                }
                            }
                        }).start();
                    }
                }
            });

            handleGroupChkBox(position, holder, mapGroup);
        }
        return convertView;
    }

    private void handleGroupChkBox(final int position, ViewHolder holder, final Map<String, Object> mapGroup) {
        if (mIsLongPressed) {
            holder.chkGroup.setVisibility(View.VISIBLE);
            holder.swDeviceOnOff.setVisibility(View.GONE);
        } else {
            holder.chkGroup.setVisibility(View.GONE);
            holder.swDeviceOnOff.setVisibility(View.VISIBLE);
        }

        holder.chkGroup.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    mIsSelected.put(position, true);
                } else {
                    mIsSelected.put(position, false);
                }
            }
        });
        if (mIsSelected.size() > 0) {
            holder.chkGroup.setChecked(mIsSelected.get(position));
        }
    }

    public void setLongPressed(boolean isLongPressed) {
        mIsLongPressed = isLongPressed;
        notifyDataSetChanged();
    }

    private void initSelectedData() {
        mIsSelected.clear();
        for (int i = 0; i < mGroupList.size(); i++) {
            mIsSelected.put(i, false);
        }
    }


    public void setSelectChkBox(int position) {
        boolean isChecked = mIsSelected.get(position);
        mIsSelected.put(position, !isChecked);
        notifyDataSetChanged();
    }

    public ArrayList<Integer> getSelectedList() {
        Map<String, Object> mapGroup;
        ArrayList<Integer> selectedGroupList = new ArrayList<>();

        for (int i = 0; i < mIsSelected.size(); i++) {
            if (mIsSelected.get(i)) {
                mapGroup = mGroupList.get(i);
                selectedGroupList.add((int) mapGroup.get(AppDataGroup.TAG_GROUP_ID));
            }
        }
        return selectedGroupList;
    }

    public boolean isLongPressed() {
        return mIsLongPressed;
    }

    private class ViewHolder {
        private TextView txtGroupName, txtDeviceCount;
        private ImageView imgGroup;
        private FNSwitch swDeviceOnOff;
        private CheckBox chkGroup;
    }

    private void startWait(long durationMillis) {
        progressBar = new FNProgressDialog(mActivity)
                .show();
        progressBar.setProgressTitle(mActivity.getString(R.string.fn_msg_setting));
        progressBar.startAnimation(0, durationMillis);
    }
}
