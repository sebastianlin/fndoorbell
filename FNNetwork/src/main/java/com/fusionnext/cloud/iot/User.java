package com.fusionnext.cloud.iot;

import android.content.Context;

import com.fusionnext.cloud.datalink.http.HttpGet;
import com.fusionnext.cloud.datalink.http.HttpParam;
import com.fusionnext.cloud.datalink.http.HttpPost;
import com.fusionnext.cloud.datalink.http.HttpPut;
import com.fusionnext.cloud.datalink.json.FNJsonObject;
import com.fusionnext.cloud.iot.objects.ErrorObj;
import com.fusionnext.cloud.iot.objects.Thing;
import com.fusionnext.cloud.iot.objects.UserProfile;

import org.json.JSONException;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by Mike Chang on 2016/4/14
 */
public class User {
    protected final static String _TAG = "UserProfile";
    private Context context;
    private UserProfile userProfile = new UserProfile();

    public User(Context context) {
        this.context = context;
        setUserProfile(Public.getDataMgr(context).loadUserProfile());
    }

    public void setUserProfile(UserProfile profile) {
        if (userProfile == null) userProfile = new UserProfile();
        userProfile._userId = profile._userId;
        if (userProfile._organizationId != null)
            userProfile._organizationId = profile._organizationId;
        if (userProfile._name != null) userProfile._name = profile._name;
        if (userProfile._email != null) userProfile._email = profile._email;
        if (userProfile._avatar != null) userProfile._avatar = profile._avatar;
        if (userProfile._birthday != null) userProfile._birthday = profile._birthday;
        if (userProfile._gender != null) userProfile._gender = profile._gender;
        if (userProfile._locale != null) userProfile._locale = profile._locale;
    }

    public UserProfile getUserProfile() {
        if (userProfile == null) userProfile = new UserProfile();
        return userProfile;
    }

    public void createUser(UserProfile user, String password, final IOTHandler.OnUsersListener onUsersListener) {
        HttpPost.PostParam[] params = new HttpPost.PostParam[1];
        params[0] = new HttpPost.PostParam();
        params[0].accessToken = Public.getAuthorization(context).getClientAccessToken();
        params[0].url = HttpData.generateHttpPostUrl(HttpData._POST_TYPE_USER_CREATE_USER_BY_EMAIL);
        params[0].type = HttpData._POST_TYPE_USER_CREATE_USER_BY_EMAIL;
        params[0].param = new HashMap<>();

        //params[0].param.put("organization_id", user._organizationId);
        if (user._name != null && user._name.length() > 0) {
            params[0].param.put("name", user._name);
        }
        params[0].param.put("email", user._email);
        params[0].param.put("password", password);

        HttpPost post = new HttpPost();
        post.setContext(context);
        post.setOnParseJsonListener(new HttpPost.OnParseJsonListener() {
            @Override
            public void onParse(boolean returnResult, int postType, String id, String returnStr) {
                ErrorObj errorObj = null;

                if (returnStr != null) {
                    if (returnResult) {
                        parseUserProfile(returnStr);
                    } else {
                        errorObj = ErrorObj.parseCreateUserErrorObj(returnStr);
                    }
                }
                onUsersListener.onCreateUser(returnResult, errorObj);
            }
        });
        post.execute(params);
    }

    public void resendUser(UserProfile user, final IOTHandler.OnUsersListener onUsersListener) {
        HttpPost.PostParam[] params = new HttpPost.PostParam[1];
        params[0] = new HttpPost.PostParam();
        params[0].accessToken = Public.getAuthorization(context).getClientAccessToken();
        params[0].url = HttpData.generateHttpPostUrl(HttpData._POST_TYPE_USER_RESEND);
        params[0].type = HttpData._POST_TYPE_USER_RESEND;
        params[0].param = new HashMap<>();

        //params[0].param.put("organization_id", user._organizationId);
        if (user._name != null && user._name.length() > 0) {
            params[0].param.put("name", user._name);
        }
        params[0].param.put("email", user._email);


        HttpPost post = new HttpPost();
        post.setContext(context);
        post.setOnParseJsonListener(new HttpPost.OnParseJsonListener() {
            @Override
            public void onParse(boolean returnResult, int postType, String id, String returnStr) {
                ErrorObj errorObj = null;

                if (returnStr != null) {
                    if (returnResult) {
                        parseUserProfile(returnStr);
                    } else {
                        errorObj = ErrorObj.parseCreateUserErrorObj(returnStr);
                    }
                }
                onUsersListener.onRsendUser(returnResult, errorObj);
            }
        });
        post.execute(params);
    }

    public void getUser(String userId, final IOTHandler.OnUsersListener onUsersListener) {
        HttpParam.HttpSubParam param = new HttpParam.HttpSubParam();

        param.accessToken = Public.getAuthorization(context).getUserAccessToken();
        param.url = HttpData.generateHttpGetUrl(HttpData._GET_TYPE_USER_GET_USER, userId, null, HttpData._GET_TYPE_SORT_ORDER_DESC, null, null);
        param.cacheName = "";
        HttpParam[] params = new HttpParam[1];
        params[0] = new HttpParam();
        params[0].params.clear();
        params[0].params.add(param);
        HttpGet ha = new HttpGet();
        ha.setContext(context);
        ha.setTag(HttpGet._HTTP_T_GET_NORMAL_TYPE);
        ha.setOnParseJsonListener(new HttpGet.OnParseJsonListener() {
            public void onParse(boolean result, String returnStr) {
                ErrorObj errorObj = null;

                if (returnStr != null) {
                    if (result) {
                        parseUserProfile(returnStr);
                    } else {
                        errorObj = ErrorObj.parseErrorObj(returnStr);
                    }
                }
                onUsersListener.onGetUser(result, errorObj);
            }
        });
        ha.execute(params);
    }

    public void updateUser(final String name, final String oldPwd, String newPwd, final IOTHandler.OnUsersListener onUsersListener) {

        HttpPut.PutParam[] params = new HttpPut.PutParam[1];
        params[0] = new HttpPut.PutParam();
        params[0].accessToken = Public.getAuthorization(context).getUserAccessToken();
        params[0].url = HttpData.generateHttpPutUrl(HttpData._PUT_TYPE_USER_UPDATE_USER, Public.getAuthorization(context).getUserId());
        params[0].type = HttpData._PUT_TYPE_USER_UPDATE_USER;
        params[0].param = new HashMap<>();
        if (name != null) {
            params[0].param.put("name", name);
        }
        if (oldPwd != null && newPwd != null) {
            params[0].param.put("old_password", oldPwd);
            params[0].param.put("password", newPwd);
        }
        HttpPut put = new HttpPut();
        put.setHttpRequestType("PUT");
        put.setContext(context);
        put.setOnParseJsonListener(new HttpPut.OnParseJsonListener() {
            @Override
            public void onParse(boolean returnResult, int putType, String returnStr) {
                ErrorObj errorObj = null;

                if (returnStr != null) {
                    if (returnResult) {
                        parseUserProfile(returnStr);
                    } else {
                        errorObj = ErrorObj.parseErrorObj(returnStr);
                    }
                }
                onUsersListener.onUpdateUser(returnResult, errorObj);
            }
        });
        put.execute(params);
    }

    public void listAllThings(int order, String pageSize, String pageNum, final IOTHandler.OnUsersListener onUsersListener) {
        HttpParam.HttpSubParam param = new HttpParam.HttpSubParam();
        final String userId = Public.getAuthorization(context).getUserId();
        param.accessToken = Public.getAuthorization(context).getUserAccessToken();
        param.url = HttpData.generateHttpGetUrl(HttpData._GET_TYPE_USER_LIST_ALL_THINGS, userId, null, order, pageSize, pageNum);
        param.cacheName = "";
        HttpParam[] params = new HttpParam[1];
        params[0] = new HttpParam();
        params[0].params.clear();
        params[0].params.add(param);

        HttpGet httpGet = new HttpGet();
        httpGet.setContext(context);
        httpGet.setTag(HttpGet._HTTP_T_GET_NORMAL_TYPE);
        httpGet.setOnParseJsonListener(new HttpGet.OnParseJsonListener() {
            @Override
            public void onParse(boolean result, String returnStr) {
                ArrayList<Thing> ownedThingArrayList = null;
                ArrayList<Thing> sharedThingArrayList = null;
                ArrayList<Thing> invitedThingArrayList = null;

                ErrorObj errorObj = null;

                if (returnStr != null) {
                    if (result) {
                        try {
                            FNJsonObject thingObject = new FNJsonObject(returnStr);
                            String ownedThings = thingObject.getStringDefault("owned_things");
                            String sharedThings = thingObject.getStringDefault("shared_things");
                            String invitedThings = thingObject.getStringDefault("invited_things");

                            if (ownedThings != null && ownedThings.length() > 0) {
                                ownedThingArrayList = Thing.parseThingArray(ownedThings);
                            }
                            if (sharedThings != null && sharedThings.length() > 0) {
                                sharedThingArrayList = Thing.parseThingArray(sharedThings);
                            }
                            if (invitedThings != null && invitedThings.length() > 0) {
                                invitedThingArrayList = Thing.parseThingArray(invitedThings);
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    } else {
                        errorObj = ErrorObj.parseErrorObj(returnStr);
                    }
                }
                onUsersListener.onListAllThings(result, userId, ownedThingArrayList, sharedThingArrayList, invitedThingArrayList, errorObj);
            }
        });
        httpGet.execute(params);
    }

    public void listOwnedThings(int order, String pageSize, String pageNum, final IOTHandler.OnUsersListener onUsersListener) {
        HttpParam.HttpSubParam param = new HttpParam.HttpSubParam();
        final String userId = Public.getAuthorization(context).getUserId();
        param.accessToken = Public.getAuthorization(context).getUserAccessToken();
        param.url = HttpData.generateHttpGetUrl(HttpData._GET_TYPE_USER_LIST_OWNED_THINGS, userId, null, order, pageSize, pageNum);
        param.cacheName = "";
        HttpParam[] params = new HttpParam[1];
        params[0] = new HttpParam();
        params[0].params.clear();
        params[0].params.add(param);

        HttpGet httpGet = new HttpGet();
        httpGet.setContext(context);
        httpGet.setTag(HttpGet._HTTP_T_GET_NORMAL_TYPE);
        httpGet.setOnParseJsonListener(new HttpGet.OnParseJsonListener() {
            @Override
            public void onParse(boolean result, String returnStr) {
                ArrayList<Thing> thingArrayList = null;
                ErrorObj errorObj = null;

                if (returnStr != null) {
                    if (result) {
                        thingArrayList = Thing.parseThingArray(returnStr);
                    } else {
                        errorObj = ErrorObj.parseErrorObj(returnStr);
                    }
                }
                onUsersListener.onListOwnedThings(result, userId, thingArrayList, errorObj);
            }
        });
        httpGet.execute(params);
    }

    public void listInvitedThings(int order, String pageSize, String pageNum, final IOTHandler.OnUsersListener onUsersListener) {
        HttpParam.HttpSubParam param = new HttpParam.HttpSubParam();
        final String userId = Public.getAuthorization(context).getUserId();
        param.accessToken = Public.getAuthorization(context).getUserAccessToken();
        param.url = HttpData.generateHttpGetUrl(HttpData._GET_TYPE_USER_LIST_INVITED_THINGS, userId, null, order, pageSize, pageNum);
        param.cacheName = "";
        HttpParam[] params = new HttpParam[1];
        params[0] = new HttpParam();
        params[0].params.clear();
        params[0].params.add(param);

        HttpGet httpGet = new HttpGet();
        httpGet.setContext(context);
        httpGet.setTag(HttpGet._HTTP_T_GET_NORMAL_TYPE);
        httpGet.setOnParseJsonListener(new HttpGet.OnParseJsonListener() {
            @Override
            public void onParse(boolean result, String returnStr) {
                ArrayList<Thing> thingArrayList = null;
                ErrorObj errorObj = null;

                if (returnStr != null) {
                    if (result) {
                        thingArrayList = Thing.parseThingArray(returnStr);
                    } else {
                        errorObj = ErrorObj.parseErrorObj(returnStr);
                    }
                }
                onUsersListener.onListInvitedThings(result, userId, thingArrayList, errorObj);
            }
        });
        httpGet.execute(params);
    }

    public void listSharedThings(int order, String pageSize, String pageNum, final IOTHandler.OnUsersListener onUsersListener) {
        HttpParam.HttpSubParam param = new HttpParam.HttpSubParam();
        final String userId = Public.getAuthorization(context).getUserId();
        param.accessToken = Public.getAuthorization(context).getUserAccessToken();
        param.url = HttpData.generateHttpGetUrl(HttpData._GET_TYPE_USER_LIST_SHARED_THINGS, userId, null, order, pageSize, pageNum);
        param.cacheName = "";
        HttpParam[] params = new HttpParam[1];
        params[0] = new HttpParam();
        params[0].params.clear();
        params[0].params.add(param);

        HttpGet httpGet = new HttpGet();
        httpGet.setContext(context);
        httpGet.setTag(HttpGet._HTTP_T_GET_NORMAL_TYPE);
        httpGet.setOnParseJsonListener(new HttpGet.OnParseJsonListener() {
            @Override
            public void onParse(boolean result, String returnStr) {
                ArrayList<Thing> thingArrayList = null;
                ErrorObj errorObj = null;

                if (returnStr != null) {
                    if (result) {
                        thingArrayList = Thing.parseThingArray(returnStr);
                    } else {
                        errorObj = ErrorObj.parseErrorObj(returnStr);
                    }
                }
                onUsersListener.onListSharedThings(result, userId, thingArrayList, errorObj);
            }
        });
        httpGet.execute(params);
    }

    public void resetPassword(final String email, final IOTHandler.OnUsersListener onUsersListener) {
        HttpPost.PostParam[] params = new HttpPost.PostParam[1];
        params[0] = new HttpPost.PostParam();
        params[0].accessToken = Public.getAuthorization(context).getClientAccessToken();
        params[0].url = HttpData.generateHttpPostUrl(HttpData._POST_TYPE_USER_RESET_PASSWORD);
        params[0].type = HttpData._POST_TYPE_USER_RESET_PASSWORD;
        params[0].param = new HashMap<>();

        params[0].param.put("email", email);

        HttpPost post = new HttpPost();
        post.setContext(context);
        post.setOnParseJsonListener(new HttpPost.OnParseJsonListener() {
            @Override
            public void onParse(boolean returnResult, int postType, String id, String returnStr) {
                ErrorObj errorObj = null;

                if (returnStr != null && !returnResult) {
                    errorObj = ErrorObj.parseErrorObj(returnStr);
                }
                onUsersListener.onResetPassword(returnResult, errorObj);
            }
        });
        post.execute(params);
    }

    public void setPassword(final String email, final String password, final String code, final IOTHandler.OnUsersListener onUsersListener) {
        HttpPost.PostParam[] params = new HttpPost.PostParam[1];
        params[0] = new HttpPost.PostParam();
        params[0].accessToken = Public.getAuthorization(context).getClientAccessToken();
        params[0].url = HttpData.generateHttpPostUrl(HttpData._POST_TYPE_USER_SET_PASSWORD);
        params[0].type = HttpData._POST_TYPE_USER_SET_PASSWORD;
        params[0].param = new HashMap<>();
        params[0].param.put("email", email);
        params[0].param.put("new_password", password);
        params[0].param.put("reset_code", code);

        HttpPost post = new HttpPost();
        post.setContext(context);
        post.setOnParseJsonListener(new HttpPost.OnParseJsonListener() {
            @Override
            public void onParse(boolean returnResult, int postType, String id, String returnStr) {
                ErrorObj errorObj = null;

                if (returnStr != null && !returnResult) {
                    errorObj = ErrorObj.parseErrorObj(returnStr);
                }
                onUsersListener.onSetPassword(returnResult, errorObj);
            }
        });
        post.execute(params);
    }

    private boolean parseUserProfile(String json) {
        try {
            FNJsonObject profileObj = new FNJsonObject(json);
            userProfile.getDataFromJSONObject(profileObj);
            setUserProfile(userProfile);
            Public.getDataMgr(context).saveUserProfile(userProfile);
            return true;
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return false;
    }
}
