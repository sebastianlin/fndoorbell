package com.fusionnextinc.doorbell.objects;

import com.fusionnext.cloud.iot.objects.Event;

import java.util.ArrayList;

/**
 * Created by Mike Chang on 2017/6/8
 */

public class FNEvent {
    protected final static String TAG = "FNEvent";

    private String eventId;
    private String thingId;
    private String name;
    private String payload;
    private String createdAt;
    private String updatedAt;

    public FNEvent() {
        eventId = "";
        thingId = "";
        name = "";
        payload = "";
        createdAt = "";
        updatedAt = "";
    }

    public void clean() {
        eventId = "";
        thingId = "";
        name = "";
        payload = "";
        createdAt = "";
        updatedAt = "";
    }

    public static ArrayList<FNEvent> parseEventArray(ArrayList<Event> eventList) {
        ArrayList<FNEvent> fnEventList = new ArrayList<FNEvent>();

        if (eventList == null) {
            return null;
        }
        for (int i = 0; i < eventList.size(); i++) {
            Event event = eventList.get(i);
            FNEvent fnEvent = new FNEvent();
            fnEvent.eventId = event._eventId;
            fnEvent.thingId = event._thingId;
            fnEvent.name = event._name;
            fnEvent.payload = event._payload;
            fnEvent.createdAt = event._createdAt;
            fnEvent.updatedAt = event._updatedAt;
            fnEventList.add(fnEvent);
        }
        return fnEventList;
    }

    public String getEventId() {
        return eventId;
    }

    public void setEventId(String eventId) {
        this.eventId = eventId;
    }

    public String getThingId() {
        return thingId;
    }

    public void setThingId(String thingId) {
        this.thingId = thingId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPayload() {
        return payload;
    }

    public void setPayload(String payload) {
        this.payload = payload;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }

}
