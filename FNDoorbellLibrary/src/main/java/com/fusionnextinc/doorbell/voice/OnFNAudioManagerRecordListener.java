package com.fusionnextinc.doorbell.voice;

/**
 * Created by koukiyoshiyuu on 2018/3/8.
 */

public interface OnFNAudioManagerRecordListener {

    void onStartRecord(FNAudioManager voiceManager);

    void onRecordData(FNAudioManager voiceManager, byte[] data);

    void onStopRecord(FNAudioManager voiceManager);

    void onRecordCompleted(FNAudioManager voiceManager);

    void onRecordError(FNAudioManager voiceManager);

    void onRecordingTimeUpdated(long recordingTime);
}
