package com.fusionnext.cloud.fusionapi;

import android.content.Context;

import com.fusionnext.cloud.datalink.http.HttpGet;
import com.fusionnext.cloud.datalink.http.HttpParam;
import com.fusionnext.cloud.datalink.json.FNJsonObject;

import org.json.JSONException;

/**
 * Created by Mike Chang on 2017/3/7
 */
public class Clients {
    protected final static String _TAG = "Clients";

    public static class Client {
        public String clientId = "";
        public String clientName = "";
        public String clientPackageName = "";
        public boolean sdkAccessCamera = false;
        public String createAt = "";
        public String updatedAt = "";
    }

    private Context context;

    public Clients(Context context) {
        this.context = context;
    }

    public void getClient(String clientId, final FusionAPIHandler.OnFusionAPIListener onFusionAPIListener) {
        HttpParam.HttpSubParam param = new HttpParam.HttpSubParam();

        param.accessToken = Public.getAuthorization(context).getDeviceAccessToken();
        param.url = HttpData.generateHttpGetUrl(HttpData._GET_TYPE_GET_CLIENT, clientId);
        param.cacheName = "";
        HttpParam[] params = new HttpParam[1];
        params[0] = new HttpParam();
        params[0].params.clear();
        params[0].params.add(param);

        HttpGet httpGet = new HttpGet();
        httpGet.setContext(context);
        httpGet.setTag(HttpGet._HTTP_T_GET_NORMAL_TYPE);
        httpGet.setOnParseJsonListener(new HttpGet.OnParseJsonListener() {
            @Override
            public void onParse(boolean result, String returnStr) {
                if (returnStr != null) {
                    Client client = parseClient(returnStr);
                    onFusionAPIListener.onGetClient(result, client);
                }
            }
        });
        httpGet.execute(params);
    }

    public Client parseClient(String jsonStr) {
        if (jsonStr == null || jsonStr.equals("")) return null;
        Client client = new Client();

        try {
            FNJsonObject object = new FNJsonObject(jsonStr);

            client.clientId = object.getStringDefault("id");
            client.clientName = object.getStringDefault("client_name");
            client.clientPackageName = object.getStringDefault("client_package_name");
            client.sdkAccessCamera = object.getBooleanDefault("sdk_access_camera");
            client.createAt = object.getStringDefault("created_at");
            client.updatedAt = object.getStringDefault("updated_at");
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return client;
    }
}
