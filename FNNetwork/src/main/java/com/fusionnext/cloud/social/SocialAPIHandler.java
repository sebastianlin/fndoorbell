package com.fusionnext.cloud.social;


import com.fusionnext.cloud.social.objects.Comment;
import com.fusionnext.cloud.social.objects.FileObj;
import com.fusionnext.cloud.social.objects.Group;
import com.fusionnext.cloud.social.objects.Invitation;
import com.fusionnext.cloud.social.objects.Likes;
import com.fusionnext.cloud.social.objects.Message;
import com.fusionnext.cloud.social.objects.Photo;
import com.fusionnext.cloud.social.objects.Point;
import com.fusionnext.cloud.social.objects.Tag;
import com.fusionnext.cloud.social.objects.Track;
import com.fusionnext.cloud.social.objects.UserProfile;

import java.util.ArrayList;

/**
 * Created by Mike Chang on 2017/3/7
 */
public abstract class SocialAPIHandler {

    public interface OnAuthorizationListener {
        //void onGetClientToken(boolean result, Authorization.ClientToken clientToken);
        void onActivateAPI(boolean result);

        void onGetUserToken(boolean result, Authorization.UserToken userToken);
    }

    public interface OnCommentsListener {
        void onResult(boolean result, String id, int postType, String retStr);

        void onListComments(boolean result, String id, ArrayList<Comment> commentList);

        void onGetComment(boolean result, String id, Comment comment);

        void onListLikes(boolean result, String id, ArrayList<Likes> likeList);
    }

    public interface OnDevicesListener {
        void onResult(boolean result, int postType, String retStr);
    }

    public interface OnGroupsListener {
        void onResult(boolean result, String id, int postType, String retStr);

        void onCreateGroup(boolean result, Group group);

        void onGetGroup(boolean result, String id, Group group);

        void onListGroupInvitations(boolean result, String id, ArrayList<Invitation> invitedList);

        void onListGroupMembers(boolean result, String id, ArrayList<UserProfile> memberList);

        void onListGroupPhotos(boolean result, String id, ArrayList<Photo> photoList);

        void onListGroupMessages(boolean result, String id, ArrayList<Message> messageList);
    }

    public interface OnMessagesListener {
        void onResult(boolean result, int postType, String retStr);
    }

    public interface OnPhotosListener {
        void onResult(boolean result, String id, int postType, String retStr);

        void onListComments(boolean result, String id, ArrayList<Comment> commentList);

        void onListLikes(boolean result, String id, ArrayList<Likes> likeList);

        void onListTags(boolean result, String id, ArrayList<Tag> tagList);

        void onListAllPhotos(boolean result, ArrayList<Photo> photoList);

        void onGetPhoto(boolean result, String id, Photo photo);

        void onUpdatePhoto(boolean result, String id, Photo photo);

        void onSavePhoto(boolean result, String id, Photo photo);

    }

    public interface OnPointsListener {
        void onResult(boolean result, String id, int postType, String retStr);
    }

    public interface OnTagsListener {
        void onListTracks(boolean result, ArrayList<Track> trackList);

        void onListPhotos(boolean result, ArrayList<Photo> photoList);
    }

    public interface OnTracksListener {
        void onResult(boolean result, String id, int postType, String retStr);

        void onListTracks(boolean result, Tracks.ListTrackType trackType, ArrayList<Track> trackList);

        void onCreateTrack(boolean result, Track track);

        void onUpdateTrack(boolean result, String id, Track track);

        void onGetTrack(boolean result, String id, Track track);

        void onListPoints(boolean result, String id, ArrayList<Point> pointList);

        void onListPhotos(boolean result, String id, ArrayList<Photo> photoList);

        void onListComments(boolean result, String id, ArrayList<Comment> commentList);

        void onListLikes(boolean result, String id, ArrayList<Likes> likeList);

        void onListTags(boolean result, String id, ArrayList<Tag> tagList);
    }

    public interface OnUsersListener {
        void onResult(boolean result, String id, int postType, String retStr);

        void onGetUser(boolean result, String id, UserProfile userProfile);

        void onListUserFollowers(boolean result, String id, ArrayList<UserProfile> userProfileArrayList);

        void onListUserFollows(boolean result, String id, ArrayList<UserProfile> userProfileList);

        void onListUserTracks(boolean result, String id, ArrayList<Track> trackList);

        void onListUserGroups(boolean result, String id, ArrayList<Group> groupList);
    }

    public interface OnFilesListener {
        void onUploadFile(boolean result, String id, FileObj fileObj);
    }
}
