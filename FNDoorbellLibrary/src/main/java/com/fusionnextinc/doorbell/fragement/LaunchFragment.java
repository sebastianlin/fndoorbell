package com.fusionnextinc.doorbell.fragement;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.res.Configuration;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.PowerManager;
import android.provider.Settings;
import androidx.annotation.NonNull;
import androidx.core.app.ActivityCompat;
import androidx.core.content.PermissionChecker;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.fusionnextinc.doorbell.BuildConfig;
import com.fusionnextinc.doorbell.DefaultAppConfig;
import com.fusionnextinc.doorbell.MyApplication;
import com.fusionnextinc.doorbell.R;
import com.fusionnextinc.doorbell.manager.account.AccountManager;
import com.fusionnextinc.doorbell.manager.mqtt.MyMqtt;
import com.fusionnextinc.doorbell.utils.Utils;
import com.fusionnextinc.doorbell.widget.FNAlert.FNAlertDialog;
import com.fusionnextinc.doorbell.widget.FNLayoutUtil;

import java.util.ArrayList;

import static android.content.Context.POWER_SERVICE;

/**
 * Created by chocoyang on 2016/5/16.
 */
public class LaunchFragment extends FNFragment {
    private static final String TAG = LaunchFragment.class.getSimpleName();

    private static final int SPLASH_TIME_RELEASE = 3000;
    private static final int SPLASH_TIME_DEBUG = 1000;
    private static  boolean bActive = false;
    /**
     * MQTT配置参数
     **/
    private String protocol = "ssl";
    private String host = "broker.fusionnextinc.com";
    private String port = "8883";
    private String userID = "device";
    private String passWord = "";
    private String clientID = "";
    private static MyMqtt mMyMqtt;

    private FNLayoutUtil layoutUtil;
    private ImageView imgLogo;
    private AccountManager accountManager;

    // Constants
    private boolean mSkipLaunchWaiting;

    public static void openFragment(boolean clearOtherStack, boolean skipLaunchWaiting) {
        LaunchFragment launchFragment = new LaunchFragment();
        launchFragment.mSkipLaunchWaiting = skipLaunchWaiting;
        FNFragmentManager.getInstance().openFragment(launchFragment, clearOtherStack);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        layoutUtil = new FNLayoutUtil(getActivity(), MyApplication.LAYOUT_WIDTH, MyApplication.LAYOUT_HEIGHT, FNLayoutUtil.BASE_WIDTH);
        accountManager = AccountManager.getInstance(getActivity());
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        final ViewGroup nullParent = null;

        getActionBar().hide();
        View view = inflater.inflate(R.layout.fragment_launch, nullParent);
        imgLogo = (ImageView) view.findViewById(R.id.img_logo);
        TextView appVersion = (TextView) view.findViewById(R.id.text_app_version);
        layoutUtil.fitAllView(view);
        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;
        BitmapFactory.decodeResource(getResources(), R.drawable.logo, options);
        boolean isPortrait = options.outHeight >= options.outWidth;
        BitmapFactory.decodeResource(getResources(), R.drawable.logo_land, options);
        boolean isLandscape = options.outHeight <= options.outWidth;
        if (isPortrait && !isLandscape) {
            getActivity().setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        } else if (!isPortrait && isLandscape) {
            getActivity().setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
        } else {
            getActivity().setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_UNSPECIFIED);
        }
        imgLogo.setImageResource(getResources().getConfiguration().orientation == Configuration.ORIENTATION_PORTRAIT ? R.drawable.logo : R.drawable.logo_land);
        imgLogo.setVisibility(View.VISIBLE);
        appVersion.setText(getString(R.string.fn_text_app_version, Utils.getAppVerison(getActivity())));

        String[] requestPermissions = needRequestPermissions(getContext());
        if (requestPermissions.length != 0) {
            ActivityCompat.requestPermissions(getActivity(), requestPermissions, 0);
        } else {
            startNextPage();
        }
        bActive = true;
        return view;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        final String[] requestPermissions = needRequestPermissions(getContext());
        if (requestPermissions.length != 0) {
            FNAlertDialog dialog = new FNAlertDialog(getContext());
            dialog.setTitleText(getString(R.string.dialog_title_permission_denied));
            dialog.setContentText(getString(R.string.dialog_msg_permission_denied));
            dialog.setCancelable(false);
            dialog.setConfirmText(getString(R.string.btn_confirm));
            dialog.setConfirmClickListener(new FNAlertDialog.OnSweetClickListener() {
                @Override
                public void onClick(FNAlertDialog dialog) {
                    dialog.dismiss();
                    ActivityCompat.requestPermissions(getActivity(), requestPermissions, 0);
                }
            });
            dialog.setCancelText(getString(R.string.btn_exit));
            dialog.setCancelClickListener(new FNAlertDialog.OnSweetClickListener() {
                @Override
                public void onClick(FNAlertDialog dialog) {
                    dialog.dismiss();
                    getActivity().finish();
                }
            });
            dialog.show();
        } else {
            startNextPage();
        }
    }

    private void startNextPage() {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    ignoreBatteryOptimization(getActivity());
                }
                if (accountManager.isLogin()) {
                    if (DefaultAppConfig.APP_SOLY) {
                        HomeFragment.openFragment(true, ThingsFragment.THINGS_FRAGMENT_TYPE_NORMAL, null, HomeFragment.TabType.TAB_TYPE_DEVICE, mSkipLaunchWaiting);
                    } else {
                        ThingsFragment.openFragment(true, ThingsFragment.THINGS_FRAGMENT_TYPE_NORMAL, null);
                    }
                } else {
                    LoginFragment.openFragment(true);
                }
            }
        }, mSkipLaunchWaiting ? 0 : (BuildConfig.DEBUG ? SPLASH_TIME_DEBUG : SPLASH_TIME_RELEASE));
    }

    /**
     * 此處只能詢問使用 app 必要權限，其他權限請在使用時另外詢問
     *
     * @return 必要權限
     */
    private String[] needRequestPermissions(Context context) {
        ArrayList<String> failPermission = new ArrayList<>();
        if (PermissionChecker.checkSelfPermission(context, Manifest.permission.READ_EXTERNAL_STORAGE) != PermissionChecker.PERMISSION_GRANTED) {
            failPermission.add(Manifest.permission.READ_EXTERNAL_STORAGE);
        }
        if (PermissionChecker.checkSelfPermission(context, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PermissionChecker.PERMISSION_GRANTED) {
            failPermission.add(Manifest.permission.WRITE_EXTERNAL_STORAGE);
        }
        if (PermissionChecker.checkSelfPermission(context, Manifest.permission.INTERNET) != PermissionChecker.PERMISSION_GRANTED) {
            failPermission.add(Manifest.permission.INTERNET);
        }
        if (PermissionChecker.checkSelfPermission(context, Manifest.permission.ACCESS_NETWORK_STATE) != PermissionChecker.PERMISSION_GRANTED) {
            failPermission.add(Manifest.permission.ACCESS_NETWORK_STATE);
        }
        if (PermissionChecker.checkSelfPermission(context, Manifest.permission.ACCESS_WIFI_STATE) != PermissionChecker.PERMISSION_GRANTED) {
            failPermission.add(Manifest.permission.ACCESS_WIFI_STATE);
        }
        if (PermissionChecker.checkSelfPermission(context, Manifest.permission.ACCESS_FINE_LOCATION) != PermissionChecker.PERMISSION_GRANTED) {
            failPermission.add(Manifest.permission.ACCESS_FINE_LOCATION);
        }
        if (PermissionChecker.checkSelfPermission(context, Manifest.permission.RECORD_AUDIO) != PermissionChecker.PERMISSION_GRANTED) {
            failPermission.add(Manifest.permission.RECORD_AUDIO);
        }
        if (PermissionChecker.checkSelfPermission(context, Manifest.permission.MODIFY_AUDIO_SETTINGS) != PermissionChecker.PERMISSION_GRANTED) {
            failPermission.add(Manifest.permission.MODIFY_AUDIO_SETTINGS);
        }
        if (PermissionChecker.checkSelfPermission(context, Manifest.permission.DISABLE_KEYGUARD) != PermissionChecker.PERMISSION_GRANTED) {
            failPermission.add(Manifest.permission.DISABLE_KEYGUARD);
        }
        if (PermissionChecker.checkSelfPermission(context, Manifest.permission.WAKE_LOCK) != PermissionChecker.PERMISSION_GRANTED) {
            failPermission.add(Manifest.permission.WAKE_LOCK);
        }
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (PermissionChecker.checkSelfPermission(context, Manifest.permission.REQUEST_IGNORE_BATTERY_OPTIMIZATIONS) != PermissionChecker.PERMISSION_GRANTED) {
                failPermission.add(Manifest.permission.REQUEST_IGNORE_BATTERY_OPTIMIZATIONS);
            }
        }
        return failPermission.toArray(new String[failPermission.size()]);
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        imgLogo.setImageResource(getResources().getConfiguration().orientation == Configuration.ORIENTATION_PORTRAIT ? R.drawable.logo : R.drawable.logo_land);
    }

    /**
     * 忽略電池優化
     */
    public void ignoreBatteryOptimization(Activity activity) {

        if( activity == null )
            return;

        PowerManager powerManager = (PowerManager) activity.getSystemService(POWER_SERVICE);
        if( powerManager == null ) {
            return;
        }
        boolean hasIgnored = powerManager.isIgnoringBatteryOptimizations(activity.getPackageName());
        //  判斷當前APP是否有加入電池優化的白名單，如果沒有，彈出加入電池優化的白名單的設置對話框。
        if (!hasIgnored) {
            Intent intent = new Intent(Settings.ACTION_REQUEST_IGNORE_BATTERY_OPTIMIZATIONS);
            intent.setData(Uri.parse("package:" + activity.getPackageName()));
            startActivity(intent);
        }
    }
}
