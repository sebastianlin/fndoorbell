package com.fusionnextinc.doorbell.fragement;

import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.os.Handler;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bigkoo.pickerview.builder.TimePickerBuilder;
import com.bigkoo.pickerview.listener.OnTimeSelectListener;
import com.bigkoo.pickerview.view.TimePickerView;
import com.fusionnext.cloud.iot.objects.THINGSTATUS;
import com.fusionnext.cloud.utils.Times;
import com.fusionnextinc.doorbell.Adapter.ThingsListAdapter;
import com.fusionnextinc.doorbell.DefaultAppConfig;
import com.fusionnextinc.doorbell.MyApplication;
import com.fusionnextinc.doorbell.R;
import com.fusionnextinc.doorbell.manager.data.AppDataThing;
import com.fusionnextinc.doorbell.manager.mqtt.MqttManager;
import com.fusionnextinc.doorbell.manager.thingsManager.ThingsHandler;
import com.fusionnextinc.doorbell.manager.thingsManager.ThingsManager;
import com.fusionnextinc.doorbell.objects.FNError;
import com.fusionnextinc.doorbell.objects.FNEvent;
import com.fusionnextinc.doorbell.objects.FNFirmware;
import com.fusionnextinc.doorbell.objects.FNSchedule;
import com.fusionnextinc.doorbell.objects.FNThing;
import com.fusionnextinc.doorbell.objects.FNUserProfile;
import com.fusionnextinc.doorbell.utils.Utils;
import com.fusionnextinc.doorbell.widget.FNAlert.FNAlertDialog;
import com.fusionnextinc.doorbell.widget.FNDialog;
import com.fusionnextinc.doorbell.widget.FNLayoutUtil;
import com.fusionnextinc.doorbell.widget.FNProgressBarCustom;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

/**
 * Created by Mike Chang on 2018/5/4
 */
public class SmartPlugFragment extends FNFragment implements View.OnClickListener {
    private static final String TAG = SmartPlugFragment.class.getSimpleName();

    private FNLayoutUtil mLayoutUtil;
    private FNProgressBarCustom mProgressBar;
    private ImageView mIvPlugLogo, mIvPower, mIvSchedule, mIvTimer;
    private TextView mTextTimerCount, mTextOffline;
    private View mViewPlug;
    private ThingsManager mThingsManager;
    private FNThing mCurFNThing;
    private int mCurThingType;
    private String mCurProductId;
    private AppDataThing mAppDataThing;
    private Handler mHandler;
    private Long mTimerValue = 0l;
    private boolean isLocked = false;

    private enum SwitchStatus {SWITCH_STATUS_ON, SWITCH_STATUS_OFF, SWITCH_STATUS_OFFLINE}

    private SwitchStatus mSwitchStatus = SwitchStatus.SWITCH_STATUS_OFFLINE;

    public static void openFragment(boolean clearOtherStack, FNThing fnThing, int thingType) {
        SmartPlugFragment smartPlugFragment = new SmartPlugFragment();
        FNFragmentManager.getInstance().openFragment(smartPlugFragment, clearOtherStack);
        smartPlugFragment.mCurFNThing = fnThing;
        smartPlugFragment.mCurThingType = thingType;
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mLayoutUtil = new FNLayoutUtil(getActivity(), MyApplication.LAYOUT_WIDTH, MyApplication.LAYOUT_HEIGHT, FNLayoutUtil.BASE_WIDTH);
        mThingsManager = ThingsManager.getInstance(getActivity());
        mAppDataThing = AppDataThing.getInstance(getActivity());
        mCurProductId = mCurFNThing.getProductId();
        mHandler = new Handler();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        final ViewGroup nullParent = null;
        THINGSTATUS isOnline = mCurFNThing.getOnline() ;
        boolean isSwitchedOn = mCurFNThing.getSwitched() == 1 ? true : false;

        getActivity().setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        mCurFNThing = mAppDataThing.getThing(mCurFNThing.getThingId());
        getActionBar().resetAll();
        getActionBar().setTitle(mCurFNThing.getName(), Gravity.CENTER, null);
        getActionBar().setCoverMode(false);
        getActionBar().setStartImage(R.drawable.back, new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        if (mCurThingType == ThingsListAdapter.TYPE_OWNED_THINGS_LIST) {
            getActionBar().addEndImage(R.drawable.btn_thing_settings, onThingSettingsClickListener);
        } else if (mCurThingType == ThingsListAdapter.TYPE_SHARED_THINGS_LIST) {
            getActionBar().addEndImage(R.drawable.btn_thing_delete, onThingDeleteClickListener);
        }
        getActionBar().show();

        View view = inflater.inflate(R.layout.fragment_plug, nullParent);
        mViewPlug = (View) view.findViewById(R.id.fragment_plug);
        mIvPlugLogo = (ImageView) view.findViewById(R.id.img_plug_logo);
        mIvPower = (ImageView) view.findViewById(R.id.img_power);
        mIvSchedule = (ImageView) view.findViewById(R.id.img_schedule);
        mIvTimer = (ImageView) view.findViewById(R.id.img_timer);
        mTextTimerCount = (TextView) view.findViewById(R.id.text_timer_count);
        mTextOffline = (TextView) view.findViewById(R.id.text_offline);
        mLayoutUtil.fitAllView(view);
        mProgressBar = new FNProgressBarCustom(getActivity(), (ViewGroup) view.findViewById(R.id.fragment_plug));
        updateSwitchStatus(isOnline, isSwitchedOn);
        updateSwitchStatusView(mSwitchStatus);

        mIvPlugLogo.setOnClickListener(this);
        mIvPower.setOnClickListener(this);
        mIvSchedule.setOnClickListener(this);
        mIvTimer.setOnClickListener(this);
        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        if (DefaultAppConfig.MQTT_ENABLE) {
            if (!MqttManager.getInstance(getContext()).isConnected() && Utils.isNetworkConnected(getActivity())) {
                MqttManager.getInstance(getContext()).connect();
            }
            mThingsManager.registerMqttListener(mMqttListener);
        }
        mThingsManager.setThingsListener(mThingsListener);
        mThingsManager.getStatesList(mCurFNThing.getThingId(), ThingsManager.STATES_SWITCH_ONOFF);
    }

    @Override
    public void onPause() {
        super.onPause();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mThingsManager.unregisterMqttListener();
    }

    @Override
    public void onClick(View view) {
        if (Utils.isNetworkConnected(getActivity())) {
            if (view.getId() == R.id.img_plug_logo) {
                if (!mProgressBar.isShown()) {
                    isLocked = true;
                    if (mSwitchStatus.equals(SwitchStatus.SWITCH_STATUS_ON)) {
                        mThingsManager.switchOnOffSmartPlug(mCurFNThing.getThingId(), false);
                        startWait();
                    } else if (mSwitchStatus.equals(SwitchStatus.SWITCH_STATUS_OFF)) {
                        mThingsManager.switchOnOffSmartPlug(mCurFNThing.getThingId(), true);
                        startWait();
                    }
                }
            } else if (view.getId() == R.id.img_power) {
                if (!mProgressBar.isShown()) {
                    isLocked = true;
                    if (mSwitchStatus.equals(SwitchStatus.SWITCH_STATUS_ON)) {
                        mThingsManager.switchOnOffSmartPlug(mCurFNThing.getThingId(), false);
                        startWait();
                    } else if (mSwitchStatus.equals(SwitchStatus.SWITCH_STATUS_OFF)) {
                        mThingsManager.switchOnOffSmartPlug(mCurFNThing.getThingId(), true);
                        startWait();
                    }
                }
            } else if (view.getId() == R.id.img_schedule) {
                if (!mSwitchStatus.equals(SwitchStatus.SWITCH_STATUS_OFFLINE)) {
                    ScheduleListFragment.openFragment(false, mCurFNThing);
                }
            } else if (view.getId() == R.id.img_timer) {
                if (!mSwitchStatus.equals(SwitchStatus.SWITCH_STATUS_OFFLINE)) {
                    openTimerDialog();
                }
            }
        } else {
            FNDialog.showNoNetworkDialog(getActivity());
        }
    }

    private void startWait() {
        mProgressBar.show();
    }

    private void stopWait() {
        mProgressBar.hide();
    }

    private void updateSwitchStatusView(SwitchStatus switchStatus) {
        mSwitchStatus = switchStatus;
        if (switchStatus.equals(SwitchStatus.SWITCH_STATUS_ON)) {
            getActionBar().setBackgroundColor(getResources().getColor(R.color.plug_on_bg));
            mIvPlugLogo.setImageDrawable(getResources().getDrawable(R.drawable.image_com_plug_on));
            mViewPlug.setBackgroundColor(getResources().getColor(R.color.plug_on_bg));
            mViewPlug.setTag(R.color.plug_on_bg);
            mTextOffline.setVisibility(View.GONE);
        } else if (switchStatus.equals(SwitchStatus.SWITCH_STATUS_OFF)) {
            getActionBar().setBackgroundColor(getResources().getColor(R.color.plug_off_bg));
            mIvPlugLogo.setImageDrawable(getResources().getDrawable(R.drawable.image_com_plug_off));
            mViewPlug.setBackgroundColor(getResources().getColor(R.color.plug_off_bg));
            mViewPlug.setTag(R.color.plug_off_bg);
            mTextOffline.setVisibility(View.GONE);
        } else {
            getActionBar().setBackgroundColor(getResources().getColor(R.color.plug_off_bg));
            mIvPlugLogo.setImageDrawable(getResources().getDrawable(R.drawable.image_com_plug_offline));
            mViewPlug.setBackgroundColor(getResources().getColor(R.color.plug_off_bg));
            mViewPlug.setTag(R.color.plug_off_bg);
            mTextOffline.setVisibility(View.VISIBLE);
        }
    }

    final ThingsHandler.ThingsListener mThingsListener = new ThingsHandler.ThingsListener() {
        @Override
        public void onListUserOwnedThings(ThingsManager instance, boolean result, String userId, ArrayList<FNThing> thingArrayList, FNError error) {

        }

        @Override
        public void onListUserInvitedThings(ThingsManager instance, boolean result, String userId, ArrayList<FNThing> thingArrayList, FNError error) {

        }

        @Override
        public void onListUserSharedThings(ThingsManager instance, boolean result, String userId, ArrayList<FNThing> thingArrayList, FNError error) {

        }

        @Override
        public void onListUserAllThings(ThingsManager instance, boolean result, String userId, ArrayList<FNThing> ownedThingArrayList, ArrayList<FNThing> sharedThingArrayList, ArrayList<FNThing> invitedThingArrayList, FNError error) {

        }

        @Override
        public void onListThingEvents(ThingsManager instance, boolean result, String thingId, String eventName, ArrayList<FNEvent> eventList, FNError error) {

        }

        @Override
        public void onListThingEventsByTime(ThingsManager instance, boolean result, String thingId, String eventName, String startTime, String endTime, ArrayList<FNEvent> eventList, FNError error) {

        }

        @Override
        public void onCurBatteryLevel(ThingsManager instance, boolean result, String thingId, String batteryLevel, FNError error) {

        }

        @Override
        public void onCurFirmwareVersion(ThingsManager instance, boolean result, String thingId, String fwVersion, FNError error) {

        }

        @Override
        public void onCurSwitchedStatus(ThingsManager instance, boolean result, final String thingId, final boolean isSwitch, FNError error) {
            MyApplication.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    if (mCurFNThing.getThingId().equals(thingId) && isAdded()) {
                        THINGSTATUS isOnline = mCurFNThing.getOnline();
                        updateSwitchStatus(isOnline, isSwitch);
                        updateSwitchStatusView(mSwitchStatus);
                    }
                }
            });
        }

        @Override
        public void onNewFirmwareVersion(ThingsManager instance, boolean result, String thingId, FNFirmware fnFirmware) {

        }

        @Override
        public void onStartFwUpdate(ThingsManager instance, boolean result, String thingId, String fwId) {

        }

        @Override
        public void onCurRingTone(ThingsManager instance, boolean result, String thingId, String ringtoneId, FNError error) {

        }

        @Override
        public void onGetScheduleList(ThingsManager instance, boolean result, String thingId, ArrayList<FNSchedule> scheduleList, FNError error) {

        }

        @Override
        public void onListInvitedUsers(ThingsManager instance, boolean result, String thingId, ArrayList<FNUserProfile> userList, FNError error) {

        }

        @Override
        public void onListSharedUsers(ThingsManager instance, boolean result, String thingId, ArrayList<FNUserProfile> userList, FNError error) {

        }

        @Override
        public void onCreateClaimCode(ThingsManager instance, boolean result, String claimCode, FNError error) {

        }

        @Override
        public void onCreateThing(ThingsManager instance, boolean result, FNThing fnThing, FNError error) {

        }

        @Override
        public void onUpdateThing(ThingsManager instance, boolean result, FNThing fnThing, FNError error) {

        }

        @Override
        public void onDeleteThing(ThingsManager instance, final boolean result, final FNError error) {
            if (isAdded()) {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        stopWait();
                        if (result) {
                            FNDialog.showSuccessDialog(getActivity(), getString(R.string.fn_dialog_delete_doorbell), getString(R.string.fn_msg_delete_thing_successfully).replace("${Thing}", getActivity().getString(R.string.fn_title_thing_name)));
                            AppDataThing.getInstance(getActivity()).deleteThing(mCurFNThing.getThingId());
                            ThingsFragment thingsFragment = new ThingsFragment();

                            thingsFragment.setUpdateThing(true, null);
                            if (DefaultAppConfig.APP_SOLY) {
                                HomeFragment.openFragment(true, ThingsFragment.THINGS_FRAGMENT_TYPE_NORMAL, null, HomeFragment.TabType.TAB_TYPE_DEVICE, false);
                            } else {
                                thingsFragment.openFragment(true, ThingsFragment.THINGS_FRAGMENT_TYPE_NORMAL, null);
                            }
                        } else {
                            FNDialog.showErrorDialog(getActivity(), getString(R.string.fn_msg_delete_thing_failed).replace("${Thing}", getActivity().getString(R.string.fn_title_thing_name)), error);
                        }
                    }
                });
            }
        }

        @Override
        public void onGetThing(ThingsManager instance, boolean result, FNThing fnThing, FNError error) {

        }

        @Override
        public void onChangeRingtone(ThingsManager instance, boolean result, String thingId, int ringtoneId) {

        }

        @Override
        public void onSetTimer(ThingsManager instance, boolean result, String thingId, String value) {

        }

        @Override
        public void onSetSchedule(ThingsManager instance, boolean result, String thingId, String value) {

        }

        @Override
        public void onShareInvited(ThingsManager instance, boolean result, String thingId, FNError error) {

        }

        @Override
        public void onShareAccepted(ThingsManager instance, boolean result, String thingId, FNError error) {

        }

        @Override
        public void onShareRejected(ThingsManager instance, boolean result, String thingId, FNError error) {

        }

        @Override
        public void onSwitchOnOff(ThingsManager instance, boolean result, String thingId, boolean isTurnOn) {
            stopWait();
            if (isAdded()) {
                if (result) {
                    if (mCurFNThing.getThingId().equals(thingId)) {
                        updateSwitchStatus(THINGSTATUS.ONLINE, isTurnOn);
                        updateSwitchStatusView(mSwitchStatus);
                    }
                } else {
                    updateSwitchStatusView(isTurnOn ? SwitchStatus.SWITCH_STATUS_OFF : SwitchStatus.SWITCH_STATUS_ON);
                    new FNAlertDialog(getActivity(), FNAlertDialog.WARNING_TYPE)
                            .setTitleText(getActivity().getString(R.string.fn_tittle_smart_plug_plug))
                            .setContentText(getActivity().getString(R.string.fn_msg_smart_plug_switch_fail))
                            .setConfirmText(getActivity().getString(R.string.fn_dialog_ok))
                            .showCancelButton(false)
                            .setConfirmClickListener(new FNAlertDialog.OnSweetClickListener() {
                                @Override
                                public void onClick(FNAlertDialog sDialog) {
                                    sDialog.dismiss();
                                }
                            })
                            .show();
                }
            }
        }

        @Override
        public void onCloseCallSlot(ThingsManager instance, boolean result, FNError error) {

        }

        @Override
        public void onTakeCallSlot(ThingsManager instance, boolean result, String thingId, String callSlot, FNError error) {

        }
    };

    private View.OnClickListener onThingSettingsClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            ThingSettingsFragment.openFragment(false, mCurFNThing);
        }
    };

    private View.OnClickListener onThingDeleteClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            if (Utils.isNetworkConnected(getActivity())) {
                new FNAlertDialog(getActivity(), FNAlertDialog.WARNING_TYPE)
                        .setTitleText(getActivity().getString(R.string.fn_dialog_are_you_sure))
                        .setContentText(getActivity().getString(R.string.fn_dialog_delete_doorbell))
                        .setCancelText(getActivity().getString(R.string.fn_dialog_cancel))
                        .setConfirmText(getActivity().getString(R.string.fn_btn_delete))
                        .showCancelButton(true)
                        .setCancelClickListener(new FNAlertDialog.OnSweetClickListener() {
                            @Override
                            public void onClick(FNAlertDialog sDialog) {
                                sDialog.dismiss();
                            }
                        })
                        .setConfirmClickListener(new FNAlertDialog.OnSweetClickListener() {
                            @Override
                            public void onClick(FNAlertDialog sDialog) {
                                startWait();
                                mThingsManager.setThingsListener(mThingsListener);
                                mThingsManager.deleteThing(mCurFNThing.getThingId());
                                sDialog.dismiss();
                            }
                        })
                        .show();
            } else {
                FNDialog.showNoNetworkDialog(getActivity());
            }
        }
    };

    private void openTimerDialog() {
        Calendar selectedDate = Calendar.getInstance();
        selectedDate.set(Calendar.MILLISECOND, 0);
        selectedDate.set(Calendar.SECOND, 0);
        selectedDate.set(Calendar.MINUTE, 0);
        selectedDate.set(Calendar.HOUR_OF_DAY, 0);
        //时间选择器
        TimePickerView pvTime = new TimePickerBuilder(getActivity(), new OnTimeSelectListener() {
            @Override
            public void onTimeSelect(Date date, View v) {
                String timer = Times.getDateString(date, Times._TIME_FORMAT_HOUR_MIN_SEC);
                String[] time = timer.split(":");

                mTimerValue = (long) (Integer.valueOf(time[0]) * 60 * 60 + Integer.valueOf(time[1]) * 60 + Integer.valueOf(time[2]));
                Long timeStamp = System.currentTimeMillis() / 1000L + mTimerValue;
                boolean setDeviceOnOff = ((Integer) mViewPlug.getTag() == R.color.plug_on_bg) ? false : true;
                mThingsManager.setTimer(mCurFNThing.getThingId(), setDeviceOnOff, timeStamp);
                startTimer(mTimerValue);
            }
        }).setType(new boolean[]{false, false, false, true, true, true})
                .setDate(selectedDate)// 如果不设置的话，默认是系统时间*/
                .setLabel("", "", "", getString(R.string.fn_timer_picker_hour), getString(R.string.fn_timer_picker_min), getString(R.string.fn_timer_picker_sec))//默认设置为年月日时分秒
                .build();
        pvTime.show();
    }

    private Runnable runnable = new Runnable() {
        public void run() {
            mTimerValue = mTimerValue - 1;

            if (mTimerValue > 0) {
                mTextTimerCount.setVisibility(View.VISIBLE);
                mTextTimerCount.setText(Times.convertSecondToHHMMString(mTimerValue));
                mHandler.postDelayed(this, 1000);
            } else {
                mTextTimerCount.setText(Times.convertSecondToHHMMString(0));
                stopTimer();
            }
        }
    };

    private void startTimer(long secondTime) {
        mHandler.removeCallbacks(runnable);
        mHandler.postDelayed(runnable, 1000);
        mTextTimerCount.setVisibility(View.VISIBLE);
        mTextTimerCount.setText(Times.convertSecondToHHMMString(secondTime));
    }

    private void stopTimer() {
        mHandler.removeCallbacks(runnable);
        mTextTimerCount.setVisibility(View.GONE);
    }

    private ThingsHandler.mqttListener mMqttListener = new ThingsHandler.mqttListener() {
        @Override
        public void onMqttConnected() {

        }

        @Override
        public void onDeviceSwitchOn(ThingsManager instance, final String thingId, final boolean isTurnOn) {
            MyApplication.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    if (mCurFNThing.getThingId().equals(thingId) && !isLocked && isAdded()) {
                        updateSwitchStatus(THINGSTATUS.ONLINE, isTurnOn);
                        updateSwitchStatusView(mSwitchStatus);
                    }
                    isLocked = false;
                }
            });
        }

        @Override
        public void onDeviceOnLine(ThingsManager instance, final String thingId, final THINGSTATUS isOnline) {
            MyApplication.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    if (mCurFNThing.getThingId().equals(thingId) && isAdded()) {
                        boolean switchedOn = true;//mCurFNThing.getSwitched() == 1 ? true : false;
                        updateSwitchStatus(isOnline, switchedOn);
                        updateSwitchStatusView(mSwitchStatus);
                    }
                    stopWait();
                }
            });
        }
    };

    private void updateSwitchStatus(THINGSTATUS isOnline, boolean isSwitchedOn) {
        if (isOnline != THINGSTATUS.OFFLINE) {
            if (isSwitchedOn) {
                mSwitchStatus = SwitchStatus.SWITCH_STATUS_ON;
            } else {
                mSwitchStatus = SwitchStatus.SWITCH_STATUS_OFF;
            }
        } else {
            mSwitchStatus = SwitchStatus.SWITCH_STATUS_OFFLINE;
        }
    }
}
