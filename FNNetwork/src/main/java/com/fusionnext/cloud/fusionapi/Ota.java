package com.fusionnext.cloud.fusionapi;

import android.content.Context;
import android.util.Log;

import com.fusionnext.cloud.datalink.http.HttpGet;
import com.fusionnext.cloud.datalink.http.HttpParam;
import com.fusionnext.cloud.datalink.http.HttpUtil;
import com.fusionnext.cloud.datalink.json.FNJsonArray;
import com.fusionnext.cloud.datalink.json.FNJsonObject;
import com.fusionnext.cloud.fusionapi.objects.Firmware;
import com.fusionnext.cloud.fusionapi.objects.Software;

import org.json.JSONException;

public class Ota {
    protected final static String _TAG = "Ota";
    private Context context;
    private FusionAPIHandler.OnOtaHandlerListener otaHandlerListener;

    public Ota(Context context, FusionAPIHandler.OnOtaHandlerListener otaHandlerListener) {
        this.context = context;
        this.otaHandlerListener = otaHandlerListener;
    }

    public void getUpdatedFwByKey(String fwInfo) {
        HttpParam.HttpSubParam param = new HttpParam.HttpSubParam();

        param.accessToken = Public.getAuthorization(context).getDeviceAccessToken();
        param.url = HttpData.generateHttpGetUrl(HttpData._GET_TYPE_FW_SEARCH_UPDATE, fwInfo);
        param.cacheName = "";
        HttpParam[] params = new HttpParam[1];
        params[0] = new HttpParam();
        params[0].params.clear();
        params[0].params.add(param);
        HttpGet ha = new HttpGet();
        ha.setContext(context);
        ha.setTag(HttpGet._HTTP_T_GET_NORMAL_TYPE);
        ha.setOnParseJsonListener(new HttpGet.OnParseJsonListener() {
            public void onParse(boolean result, String json) {
                Firmware firmware = parseFirmwareArray(json);
                if (Ota.this.otaHandlerListener != null) {
                    Ota.this.otaHandlerListener.onNewFwInfo(result, firmware);
                }
            }
        });
        ha.execute(params);
    }

    public void getUpdatedFwById(String fwId) {
        HttpParam.HttpSubParam param = new HttpParam.HttpSubParam();
        param.accessToken = Public.getAuthorization(context).getDeviceAccessToken();
        param.url = HttpData.generateHttpGetUrl(HttpData._GET_TYPE_FW_GET_UPDATE, fwId);
        param.cacheName = "";
        HttpParam[] params = new HttpParam[1];
        params[0] = new HttpParam();
        params[0].params.clear();
        params[0].params.add(param);
        HttpGet ha = new HttpGet();
        ha.setContext(context);
        ha.setTag(HttpGet._HTTP_T_GET_NORMAL_TYPE);
        ha.setOnParseJsonListener(new HttpGet.OnParseJsonListener() {
            public void onParse(boolean result, String json) {
                Firmware firmware = parseFirmware(json);
                if (Ota.this.otaHandlerListener != null) {
                    Ota.this.otaHandlerListener.onNewFwInfo(result, firmware);
                }
            }
        });
        ha.execute(params);
    }

    private Firmware parseFirmwareArray(String json) {
        String var2 = "";
        Firmware firmware = null;
        if (json != null && !json.equals("")) {
            try {
                FNJsonObject jsonObject = new FNJsonObject(json);
                FNJsonArray jsonArray = jsonObject.getJSONArray("items");
                if (jsonArray != null) {
                    FNJsonObject jsonObj = jsonArray.getJSONObject(0);
                    firmware = new Firmware();
                    firmware.getDataFromJSONObject(jsonObj);
                }

                return firmware;
            } catch (JSONException e) {
                e.printStackTrace();
                return null;
            }
        } else {
            return null;
        }
    }

    private Firmware parseFirmware(String json) {
        String var2 = "";
        Firmware firmware = null;
        if (json != null && !json.equals("")) {
            try {
                FNJsonObject jsonObject = new FNJsonObject(json);
                if (jsonObject != null) {
                    firmware = new Firmware();
                    firmware.getDataFromJSONObject(jsonObject);
                }
                return firmware;
            } catch (JSONException e) {
                e.printStackTrace();
                return null;
            }
        } else {
            return null;
        }
    }

    public void getLastestApp() {
        HttpParam.HttpSubParam param = new HttpParam.HttpSubParam();

        param.accessToken = Public.getAuthorization(context).getDeviceAccessToken();
        param.url = HttpData.generateHttpGetUrl(HttpData._GET_TYPE_SW_LIST_UPDATES, Public.getAuthorization(context).getClientId());
        param.cacheName = "";
        HttpParam[] params = new HttpParam[1];
        params[0] = new HttpParam();
        params[0].params.clear();
        params[0].params.add(param);
        HttpGet ha = new HttpGet();
        ha.setContext(context);
        ha.setTag(HttpGet._HTTP_T_GET_NORMAL_TYPE);
        ha.setOnParseJsonListener(new HttpGet.OnParseJsonListener() {
            public void onParse(boolean result, String json) {
                Software software = Ota.this.parseSoftwareArray(json);
                if (Ota.this.otaHandlerListener != null) {
                    Ota.this.otaHandlerListener.onNewSwInfo(result, software);
                }
            }
        });
        ha.execute(params);
    }

    private Software parseSoftwareArray(String json) {
        String var2 = "";
        Software software = null;
        if (json != null && !json.equals("")) {
            try {
                FNJsonObject jsonObject = new FNJsonObject(json);
                FNJsonArray jsonArray = jsonObject.getJSONArray("items");
                if (jsonArray != null && jsonArray.length() > 0) {
                    FNJsonObject jsonObj = jsonArray.getJSONObject(0);
                    software = new Software();
                    software.getDataFromJSONObject(jsonObj);
                }

                return software;
            } catch (JSONException e) {
                e.printStackTrace();
                return null;
            }
        } else {
            return null;
        }
    }

    private Software parseSoftware(String json) {
        Software software = null;
        if (json != null && !json.equals("")) {
            try {
                FNJsonObject jsonObject = new FNJsonObject(json);
                software = new Software();
                software.getDataFromJSONObject(jsonObject);
                return software;
            } catch (JSONException e) {
                e.printStackTrace();
                return null;
            }
        } else {
            return null;
        }
    }

    public void downloadFile(String urlStr, String filename) {
        HttpParam.HttpSubParam param = new HttpParam.HttpSubParam();
        param.url = urlStr;
        param.cacheName = filename;
        HttpParam[] params = new HttpParam[1];
        params[0] = new HttpParam();
        params[0].params.clear();
        params[0].params.add(param);
        HttpGet ha = new HttpGet();
        ha.setContext(context);
        ha.setTag(HttpGet._HTTP_T_GET_DOWNLOAD_FILE);
        ha.setOnDownloadListener(new HttpUtil.OnDownloadListener() {
            @Override
            public void onStart() {
                if (Ota.this.otaHandlerListener != null) {
                    Ota.this.otaHandlerListener.onSrartDownload();
                }
            }

            @Override
            public void onStop() {
                if (Ota.this.otaHandlerListener != null) {
                    Ota.this.otaHandlerListener.onStopDownload();
                }
            }

            @Override
            public void onProgress(long progress) {
                Log.d(_TAG, "downloadFile: onProgress = " + progress);
                if (Ota.this.otaHandlerListener != null) {
                    Ota.this.otaHandlerListener.onDownloadProgress(progress);
                }
            }

            @Override
            public void onComplete() {
                if (Ota.this.otaHandlerListener != null) {
                    Ota.this.otaHandlerListener.onDownloadComplete();
                }
            }

            @Override
            public void onError() {
                if (Ota.this.otaHandlerListener != null) {
                    Ota.this.otaHandlerListener.onError();
                }
            }
        });
        ha.execute(params);
    }

    public void stopDownloadFile() {
        HttpUtil httpUtil = new HttpUtil(context);
    }
}
