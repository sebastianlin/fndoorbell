package com.fusionnext.cloud.iot;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.ConnectivityManager;
import android.net.Network;
import android.net.NetworkCapabilities;
import android.net.NetworkRequest;
import android.net.wifi.WifiManager;
import android.os.Build;

import com.fusionnext.cloud.datalink.http.HttpGet;
import com.fusionnext.cloud.datalink.http.HttpParam;
import com.fusionnext.cloud.datalink.json.FNJsonObject;
import com.fusionnext.cloud.iot.objects.ErrorObj;
import com.fusionnext.cloud.iot.objects.WifiAP;
import com.fusionnext.cloud.utils.FNWaitSignal;
import com.fusionnext.cloud.utils.FNWifiManager;

import org.json.JSONException;

import java.util.ArrayList;

/**
 * Created by Mike Chang on 2018/3/8
 */
public class ApMode {
    protected final static String _TAG = "ApMode";
    private Context context;
    private static final int WIFI_SCAN_INTERVAL = 5000;
    private Thread mWifiScanThread = null;
    private boolean mIsWifiScanThreadRunning = false;
    private static BroadcastReceiver wifiPriorityReceiver;

    public ApMode(Context context) {
        this.context = context;
        enableWifiPriority(context);
    }

    public void getWifiApList(final IOTHandler.OnAPModeListener onAPModeListener) {
        HttpParam.HttpSubParam param = new HttpParam.HttpSubParam();

        param.accessToken = null;
        param.url = HttpData.generatePtComHttpGetUrl(HttpData._GET_TYPE_PTCOM_LIST_WIFI_AP, null, null, null, null);
        param.cacheName = "";
        HttpParam[] params = new HttpParam[1];
        params[0] = new HttpParam();
        params[0].params.clear();
        params[0].params.add(param);

        HttpGet httpGet = new HttpGet();
        httpGet.setContext(context);
        httpGet.setTag(HttpGet._HTTP_T_GET_NO_KEEP_ALIVE_TYPE);
        httpGet.setOnParseJsonListener(new HttpGet.OnParseJsonListener() {
            @Override
            public void onParse(boolean result, String returnStr) {
                ArrayList<WifiAP> wifiAPList = null;
                ErrorObj errorObj = null;

                if (returnStr != null) {
                    if (result) {
                        wifiAPList = WifiAP.parseWifiAPArray(returnStr);
                    } else {
                        errorObj = ErrorObj.parseErrorObj(returnStr);
                    }
                }
                onAPModeListener.onListWifiAP(result, wifiAPList, errorObj);
            }
        });
        httpGet.execute(params);
    }

    public void registerDevice(String ssid, String password, String sec, String claimCode, final IOTHandler.OnAPModeListener onAPModeListener) {
        HttpParam.HttpSubParam param = new HttpParam.HttpSubParam();

        param.accessToken = null;
        param.url = HttpData.generatePtComHttpGetUrl(HttpData._GET_TYPE_PTCOM_REGISTER, ssid, password, sec, claimCode);
        param.cacheName = "";
        HttpParam[] params = new HttpParam[1];
        params[0] = new HttpParam();
        params[0].params.clear();
        params[0].params.add(param);

        HttpGet httpGet = new HttpGet();
        httpGet.setContext(context);
        httpGet.setTag(HttpGet._HTTP_T_GET_NO_KEEP_ALIVE_TYPE);
        httpGet.setOnParseJsonListener(new HttpGet.OnParseJsonListener() {
            @Override
            public void onParse(boolean result, String returnStr) {
                ErrorObj errorObj = null;
                String macAddr = null;
                if (returnStr != null) {
                    if (result) {
                        try {
                            FNJsonObject thingObject = new FNJsonObject(returnStr);
                            macAddr = thingObject.getStringDefault("serial");
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    } else {
                        errorObj = ErrorObj.parseErrorObj(returnStr);
                    }
                }
                onAPModeListener.onRegisterDevice(result, macAddr, errorObj);
            }
        });
        httpGet.execute(params);
    }

    public void changeSsid(String ssid, String password, String sec, final IOTHandler.OnAPModeListener onAPModeListener) {
        HttpParam.HttpSubParam param = new HttpParam.HttpSubParam();

        param.accessToken = null;
        param.url = HttpData.generatePtComHttpGetUrl(HttpData._GET_TYPE_PTCOM_CHANGE_SSID, ssid, password, sec, null);
        param.cacheName = "";
        HttpParam[] params = new HttpParam[1];
        params[0] = new HttpParam();
        params[0].params.clear();
        params[0].params.add(param);

        HttpGet httpGet = new HttpGet();
        httpGet.setContext(context);
        httpGet.setTag(HttpGet._HTTP_T_GET_NO_KEEP_ALIVE_TYPE);
        httpGet.setOnParseJsonListener(new HttpGet.OnParseJsonListener() {
            @Override
            public void onParse(boolean result, String returnStr) {
                ErrorObj errorObj = null;
                String macAddr = null;
                if (returnStr != null) {
                    if (result) {
                        try {
                            FNJsonObject thingObject = new FNJsonObject(returnStr);
                            macAddr = thingObject.getStringDefault("serial");
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    } else {
                        errorObj = ErrorObj.parseErrorObj(returnStr);
                    }
                }
                onAPModeListener.onChangeSsid(result, macAddr, errorObj);
            }
        });
        httpGet.execute(params);
    }

    public void stopWifiScan() {
        if (mWifiScanThread != null) {
            mIsWifiScanThreadRunning = false;
            mWifiScanThread.interrupt();
        }
    }

    public void startWifiScan(final IOTHandler.OnAPModeListener onAPModeListener) {
        if (mWifiScanThread == null) {
            mWifiScanThread = new Thread() {
                @Override
                public void run() {
                    while (mIsWifiScanThreadRunning) {
                        int result = -1;
                        getWifiApList(onAPModeListener);
                        try {
                            Thread.sleep(WIFI_SCAN_INTERVAL);
                        } catch (InterruptedException ignore) {
                        }
                    }
                    mWifiScanThread = null;
                }
            };
            mIsWifiScanThreadRunning = true;
            mWifiScanThread.start();
        }
    }

    private static synchronized void enableWifiPriority(Context context) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP && wifiPriorityReceiver == null) {
            final ConnectivityManager connectivityManager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
            final FNWaitSignal wait = new FNWaitSignal();
            NetworkRequest.Builder request = new NetworkRequest.Builder();
            request.addCapability(NetworkCapabilities.NET_CAPABILITY_NOT_RESTRICTED);
            request.addTransportType(NetworkCapabilities.TRANSPORT_WIFI);
            ConnectivityManager.NetworkCallback callback = new ConnectivityManager.NetworkCallback() {
                @Override
                public void onAvailable(Network network) {
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                            connectivityManager.bindProcessToNetwork(network);
                        } else if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                            ConnectivityManager.setProcessDefaultNetwork(network);
                        }
                        //connectivityManager.unregisterNetworkCallback(this);
                    }
                    wait.notifySuccess();
                }
            };
            connectivityManager.registerNetworkCallback(request.build(), callback);
            wait.waitNotify(1000);
            if (wait.isTimeout()) {
                connectivityManager.unregisterNetworkCallback(callback);
            } else {
                IntentFilter filter = new IntentFilter();
                filter.addAction(WifiManager.NETWORK_STATE_CHANGED_ACTION);
                wifiPriorityReceiver = new BroadcastReceiver() {
                    @Override
                    public void onReceive(Context context, Intent intent) {
                        String action = intent.getAction();
                        if (action != null && action.equals(WifiManager.NETWORK_STATE_CHANGED_ACTION)) {
                            FNWifiManager.getInstance(context).getGatewayIp();
                            if (FNWifiManager.getInstance(context).getGatewayIp() == null) {
                                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                                    connectivityManager.bindProcessToNetwork(null);
                                } else if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                                    ConnectivityManager.setProcessDefaultNetwork(null);
                                }
                                context.unregisterReceiver(this);
                                wifiPriorityReceiver = null;
                            }
                        }
                    }
                };
                context.registerReceiver(wifiPriorityReceiver, filter);
            }
        }
    }
}
