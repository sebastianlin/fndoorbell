package com.fusionnextinc.doorbell.fragement;

import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.ListView;

import com.fusionnextinc.doorbell.Adapter.RoomListAdapter;
import com.fusionnextinc.doorbell.MyApplication;
import com.fusionnextinc.doorbell.R;
import com.fusionnextinc.doorbell.manager.thingsManager.ThingsManager;
import com.fusionnextinc.doorbell.objects.FNThing;
import com.fusionnextinc.doorbell.widget.FNLayoutUtil;
import com.fusionnextinc.doorbell.widget.FNProgressBarCustom;

/**
 * Created by Mike Chang on 2017/6/22
 */
public class RoomListFragment extends FNFragment implements AbsListView.OnItemClickListener {
    private static final String TAG = RoomListFragment.class.getSimpleName();

    private FNLayoutUtil mLayoutUtil;
    private FNProgressBarCustom mProgressBar;
    private ThingsManager mThingsManager;
    private RoomListAdapter mRoomListAdapter = null;
    private ListView mRoomListView;

    public static void openFragment(boolean clearOtherStack, FNThing fnThing) {
        FNFragmentManager.getInstance().openFragment(new RoomListFragment(), clearOtherStack);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mLayoutUtil = new FNLayoutUtil(getActivity(), MyApplication.LAYOUT_WIDTH, MyApplication.LAYOUT_HEIGHT, FNLayoutUtil.BASE_WIDTH);
        mThingsManager = ThingsManager.getInstance(getActivity());
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        final ViewGroup nullParent = null;

        getActivity().setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        getActionBar().resetAll();
        getActionBar().setTitle(getString(R.string.fn_title_add_room), Gravity.CENTER, null);
        getActionBar().setCoverMode(false);
        getActionBar().setStartImage(R.drawable.back, new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        getActionBar().show();
        View view = inflater.inflate(R.layout.fragment_room_list, nullParent);
        mLayoutUtil.fitAllView(view);
        mRoomListView = (ListView) view.findViewById(R.id.room_list);
        mRoomListView.setOnItemClickListener(this);
        mProgressBar = new FNProgressBarCustom(getActivity(), (ViewGroup) view.findViewById(R.id.fragment_ringtone_list));
        initRoomList();
        //appDataThing.loadAllThingsList(true, true);
        return view;
    }


    private void startWait() {
        mProgressBar.show();
    }

    private void stopWait() {
        mProgressBar.hide();
    }

    private boolean initRoomList() {
        if (mRoomListView == null) return false;
        if (mRoomListAdapter == null) {
            //Setup the adapter
            mRoomListAdapter = new RoomListAdapter(getActivity(), mThingsManager.getRoomList());
        }
        mRoomListView.setAdapter(mRoomListAdapter);
        return true;
    }

    @Override
    public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
        RoomSettingFragment.openFragment(false, RoomSettingFragment.ROOMS_FRAGMENT_TYPE_CREATE, i);
    }

    @Override
    public void onBackPressed() {
        if (mProgressBar != null && !mProgressBar.isShown()) {
            super.onBackPressed(); // Comment this super call to avoid calling finish() or fragmentmanager's backstack pop operation.
        }
    }
}
