package com.fusionnextinc.doorbell.Adapter;

import android.app.Activity;
import android.content.Context;
import android.content.res.ColorStateList;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.LayerDrawable;
import android.os.Build;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.core.content.ContextCompat;

import com.fusionnext.cloud.iot.objects.THINGSTATUS;
import com.fusionnextinc.doorbell.DefaultAppConfig;
import com.fusionnextinc.doorbell.MyApplication;
import com.fusionnextinc.doorbell.R;
import com.fusionnextinc.doorbell.manager.data.AppDataThing;
import com.fusionnextinc.doorbell.manager.data.ListMap;
import com.fusionnextinc.doorbell.manager.thingsManager.ThingsManager;
import com.fusionnextinc.doorbell.utils.FNLog;
import com.fusionnextinc.doorbell.widget.FNHeaderListView.FNListViewAdapter;
import com.fusionnextinc.doorbell.widget.FNLayoutUtil;

import java.util.Map;

/**
 * Created by Mike Chang on 2017/6/23
 */
public class ThingsListAdapter extends FNListViewAdapter {
    private static final String TAG = "ThingsListAdapter";

    private final static int TYPE_DRAG_HINT = 0;
    public final static int TYPE_OWNED_THINGS_LIST = 1;
    public final static int TYPE_SHARED_THINGS_LIST = 2;
    public final static int TYPE_INVITATIONS_LIST = 3;
    private final static int SECTION_COUNTS = TYPE_INVITATIONS_LIST + 1;

    private final int COLOR_LEVEL1 = 30;
    private final int COLOR_LEVEL2 = 50;
    private final int MAX_EVENT_COUNT_NUMBER = 999;


    private Context context;
    private LayoutInflater mInflater;
    private FNLayoutUtil layoutUtil;
    private ListMap ownedlistMap = new ListMap();
    private ListMap sharedlistMap = new ListMap();
    private ListMap invitedlistMap = new ListMap();
    private int layoutID;

    public ThingsListAdapter(Context context, ListMap ownedlistMap, ListMap sharedlistMap, ListMap invitedlistMap, int layoutID) {
        this.context = context;
        this.mInflater = LayoutInflater.from(context);
        this.ownedlistMap.addAll(ownedlistMap);
        this.sharedlistMap.addAll(sharedlistMap);
        this.invitedlistMap.addAll(invitedlistMap);
        this.layoutID = layoutID;
    }

    private static class sectionHolder {
        //ID, its invisible
        private TextView textSection;
        private TextView textHint;
        private LinearLayout sectionLayout;
    }

    private static class ViewHolder {
        //ID, its invisible
        private TextView textId;
        private TextView textType;
        private LinearLayout bgLayout;
        private ImageView imgThing;
        private TextView textName;
        private TextView textSubtitle;
        private TextView textCounts;
        private ProgressBar batteryProgress;
        private TextView textProgress;
        private TextView textOnline;
    }

    public void updateAdapterData(int thingsType) {
        switch (thingsType) {
            case AppDataThing.THING_TYPE_THINGS_OWNED:
                ownedlistMap.clear();
                ownedlistMap.addAll(AppDataThing.getInstance(context).getAdapterData(thingsType));
                break;
            case AppDataThing.THING_TYPE_THINGS_SHARED:
                sharedlistMap.clear();
                sharedlistMap.addAll(AppDataThing.getInstance(context).getAdapterData(thingsType));
                break;
            case AppDataThing.THING_TYPE_THINGS_INVITED:
                invitedlistMap.clear();
                invitedlistMap.addAll(AppDataThing.getInstance(context).getAdapterData(thingsType));
                break;
            case AppDataThing.THING_TYPE_THINGS_ALL:
                ownedlistMap.clear();
                ownedlistMap.addAll(AppDataThing.getInstance(context).getAdapterData(AppDataThing.THING_TYPE_THINGS_OWNED));
                sharedlistMap.clear();
                sharedlistMap.addAll(AppDataThing.getInstance(context).getAdapterData(AppDataThing.THING_TYPE_THINGS_SHARED));
                invitedlistMap.clear();
                invitedlistMap.addAll(AppDataThing.getInstance(context).getAdapterData(AppDataThing.THING_TYPE_THINGS_INVITED));
                break;
        }
        notifyDataSetChanged();
    }

    public void updateAdapterDataById(String thingId) {
        if (updateMapDataById(ownedlistMap, thingId)) {
            notifyDataSetChanged();
            return;
        }
        if (updateMapDataById(sharedlistMap, thingId)) {
            notifyDataSetChanged();
            return;
        }
        notifyDataSetChanged();
    }

    private boolean updateMapDataById(ListMap listMap, String thingId) {
        for (int i = 0; i < listMap.size(); i++) {
            Map<String, Object> map = listMap.getList().get(i);
            Object object = map.get(AppDataThing.TAG_THING_ID);
            String id = null;
            if (object != null) {
                if (object instanceof Integer) id = Integer.toString((Integer) object);
                else if (object instanceof String) id = (String) object;
            }
            if (id != null && id.equals(thingId)) {
                Map<String, Object> newMap = AppDataThing.getInstance(context).getThingMapData(thingId);
                if (newMap != null) {
                    listMap.update(i, newMap);
                }
                return true;
            }
        }
        return false;
    }

    @Override
    public boolean isEnabled(int section, int position) {
        return true;
    }

    @Override
    public Object getItem(int section, int position) {
        if (section < 0 || position < 0) {
            FNLog.d(TAG, "section is " + section + ", position is " + position);
            return null;
        }
        switch (section) {
            case TYPE_OWNED_THINGS_LIST:
                return ownedlistMap.get(position);
            case TYPE_SHARED_THINGS_LIST:
                return sharedlistMap.get(position);
            case TYPE_INVITATIONS_LIST:
                return invitedlistMap.get(position);
        }
        return null;
    }

    @Override
    public long getItemId(int section, int position) {
        return position;
    }

    @Override
    public int getSectionCount() {
        return SECTION_COUNTS;
    }

    @Override
    public int getCountForSection(int section) {

        switch (section) {
            case TYPE_OWNED_THINGS_LIST:
                if (ownedlistMap.size() <= 0) {
                    return 1;
                } else {
                    return ownedlistMap.size();
                }
            case TYPE_SHARED_THINGS_LIST:
                if (sharedlistMap.size() <= 0) {
                    return 1;
                } else {
                    return sharedlistMap.size();
                }
            case TYPE_INVITATIONS_LIST:
                if (invitedlistMap.size() <= 0) {
                    return 1;
                } else {
                    return invitedlistMap.size();
                }
            default:
                return 0;
        }
    }

    @Override
    public View getItemView(int section, int position, View convertView, ViewGroup parent) {
        //Ready the ViewHolder
        Map<String, Object> map;
        ViewHolder holder;

        if (convertView == null) {
            holder = new ViewHolder();
            layoutUtil = new FNLayoutUtil((Activity) context, MyApplication.LAYOUT_WIDTH, MyApplication.LAYOUT_HEIGHT, FNLayoutUtil.BASE_WIDTH);
            convertView = mInflater.inflate(layoutID, null);
            holder.bgLayout = (LinearLayout) convertView.findViewById(R.id.adapter_things_list);
            layoutUtil.fitAllView(holder.bgLayout);
            holder.textId = (TextView) convertView.findViewById(R.id.textThingId);
            holder.textType = (TextView) convertView.findViewById(R.id.textThingType);
            holder.imgThing = (ImageView) convertView.findViewById(R.id.imageThing);
            holder.textName = (TextView) convertView.findViewById(R.id.textName);
            holder.textSubtitle = (TextView) convertView.findViewById(R.id.textSubtitle);
            holder.textCounts = (TextView) convertView.findViewById(R.id.textCounts);
            holder.batteryProgress = (ProgressBar) convertView.findViewById(R.id.batteryProgressbar);
            holder.textProgress = (TextView) convertView.findViewById(R.id.txtBatteryProgress);
            holder.textOnline = (TextView) convertView.findViewById(R.id.textOnline);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        switch (section) {
            case TYPE_OWNED_THINGS_LIST:
                if (ownedlistMap != null && ownedlistMap.size() > 0) {
                    map = ownedlistMap.get(position);
                    updateMyThingsView(holder, map, section);
                } else {
                    updateEmptyView(holder, section);
                }

                break;
            case TYPE_SHARED_THINGS_LIST:
                if (sharedlistMap != null && sharedlistMap.size() > 0) {
                    map = sharedlistMap.get(position);
                    updateSharedThingsView(holder, map, section);
                } else {
                    updateEmptyView(holder, section);
                }
                break;
            case TYPE_INVITATIONS_LIST:
                if (invitedlistMap != null && invitedlistMap.size() > 0) {
                    map = invitedlistMap.get(position);
                    updateInvitationsView(holder, map, section);
                } else {
                    updateEmptyView(holder, section);
                }
                break;
            default:
                break;
        }

        return convertView;
    }

    @Override
    public View getSectionHeaderView(int section, View convertView, ViewGroup parent) {
        sectionHolder holder;
        final ViewGroup nullParent = null;

        if (convertView == null) {
            holder = new sectionHolder();
            layoutUtil = new FNLayoutUtil((Activity) context, MyApplication.LAYOUT_WIDTH, MyApplication.LAYOUT_HEIGHT, FNLayoutUtil.BASE_WIDTH);
            convertView = mInflater.inflate(R.layout.thing_section_item, nullParent);
            holder.sectionLayout = (LinearLayout) convertView.findViewById(R.id.thing_section_item);
            layoutUtil.fitAllView(holder.sectionLayout);
            holder.textSection = (TextView) convertView.findViewById(R.id.textSection);
            holder.textHint = (TextView) convertView.findViewById(R.id.textHint);
            convertView.setTag(holder);
        } else {
            holder = (sectionHolder) convertView.getTag();
        }
        switch (section) {
            case TYPE_DRAG_HINT:
                holder.textHint.setVisibility(View.VISIBLE);
                holder.textHint.setText(context.getString(R.string.fn_title_drag_refresh));
                updateTextView(holder.textSection, null);
                break;
            case TYPE_OWNED_THINGS_LIST:
                holder.textHint.setVisibility(View.GONE);
                updateTextView(holder.textSection, context.getString(R.string.fn_title_main_my_things).replace("${Thing}", context.getString(R.string.fn_title_thing_name)));
                break;
            case TYPE_SHARED_THINGS_LIST:
                holder.textHint.setVisibility(View.GONE);
                updateTextView(holder.textSection, context.getString(R.string.fn_title_main_shared_things).replace("${Thing}", context.getString(R.string.fn_title_thing_name)));
                break;
            case TYPE_INVITATIONS_LIST:
                holder.textHint.setVisibility(View.GONE);
                updateTextView(holder.textSection, context.getString(R.string.fn_title_main_my_invitations));
                break;
            default:
                break;
        }
        return convertView;
    }

    private void updateTextView(TextView text, String strText) {
        if (text.getId() == R.id.textSection) {
            boolean hide = (strText == null || strText.trim().equals(""));
            text.setVisibility(hide ? View.GONE : View.VISIBLE);
        }
        text.setText(strText);
    }

    private void updateEmptyView(final ViewHolder holder, int section) {
        switch (section) {
            case TYPE_OWNED_THINGS_LIST:
                holder.textName.setText(context.getString(R.string.fn_text_my_doorbell_empty).replace("${Thing}", (context.getString(R.string.fn_title_thing_name))));
                break;
            case TYPE_SHARED_THINGS_LIST:
                holder.textName.setText(context.getString(R.string.fn_text_shared_doorbell_empty).replace("${Thing}", (context.getString(R.string.fn_title_thing_name))));
                break;
            case TYPE_INVITATIONS_LIST:
                holder.textName.setText(context.getString(R.string.fn_text_invitation_empty));
                break;
            default:
                break;
        }
        holder.textId.setText("");
        holder.textSubtitle.setVisibility(View.GONE);
        holder.imgThing.setVisibility(View.GONE);
        holder.batteryProgress.setVisibility(View.GONE);
        holder.textCounts.setVisibility(View.GONE);
        holder.textProgress.setVisibility(View.GONE);
        holder.textOnline.setVisibility(View.GONE);
    }

    private void updateMyThingsView(final ViewHolder holder, final Map<String, Object> map, final int thingType) {
        holder.textType.setText(String.valueOf(thingType));
        updateTextView(holder.textId, map, AppDataThing.TAG_THING_ID);
        updateTextView(holder.textName, map, AppDataThing.TAG_THING_NAME);
        holder.textSubtitle.setVisibility(View.GONE);
        if (DefaultAppConfig.BATTERY_LEVEL_BY_TEXT) {
            updateBatteryLevelByText(holder, map, AppDataThing.TAG_THING_BATTERY_LEVEL);
        } else {
            updateBatteryLevel(holder.batteryProgress, map, AppDataThing.TAG_THING_BATTERY_LEVEL);
        }
        updateEventCounts(holder.textCounts, map, AppDataThing.TAG_THING_EVENT_COUNTS);
        updateThingIcon(holder, map);
        updateThingStatus(holder, map);
    }

    private void updateSharedThingsView(final ViewHolder holder, final Map<String, Object> map, final int thingType) {
        holder.textType.setText(String.valueOf(thingType));
        updateTextView(holder.textId, map, AppDataThing.TAG_THING_ID);
        updateTextView(holder.textName, map, AppDataThing.TAG_THING_NAME);
        updateTextView(holder.textSubtitle, map, AppDataThing.TAG_THING_USER_EMAIL);
        if (DefaultAppConfig.BATTERY_LEVEL_BY_TEXT) {
            updateBatteryLevelByText(holder, map, AppDataThing.TAG_THING_BATTERY_LEVEL);
        } else {
            updateBatteryLevel(holder.batteryProgress, map, AppDataThing.TAG_THING_BATTERY_LEVEL);
        }
        updateEventCounts(holder.textCounts, map, AppDataThing.TAG_THING_EVENT_COUNTS);
        updateThingIcon(holder, map);
        updateThingStatus(holder, map);
    }

    private void updateInvitationsView(final ViewHolder holder, final Map<String, Object> map, final int thingType) {
        holder.textType.setText(String.valueOf(thingType));
        updateTextView(holder.textId, map, AppDataThing.TAG_THING_ID);
        updateTextView(holder.textName, map, AppDataThing.TAG_THING_NAME);
        updateTextView(holder.textSubtitle, map, AppDataThing.TAG_THING_USER_EMAIL);
        updateBatteryLevelByText(holder, map,AppDataThing.TAG_THING_BATTERY_LEVEL);
        updateThingIcon(holder, map);
        holder.textCounts.setVisibility(View.INVISIBLE);
        holder.textOnline.setVisibility(View.GONE);
    }

    private void updateTextView(TextView text, Map<String, Object> map, String tag) {
        String strText = "";
        Object object = map.get(tag);
        if (object != null) {
            if (object instanceof Integer) strText = Integer.toString((Integer) object);
            else if (object instanceof String) strText = (String) object;
        }

        if (text.getId() == R.id.textSubtitle || text.getId() == R.id.textName) {
            boolean hide = (strText == null || strText.trim().equals(""));
            text.setVisibility(hide ? View.GONE : View.VISIBLE);
        }
        if(strText.length() > 22 )
        {
            strText = strText.substring(0,22)+"...";
        }
        text.setText(strText);
    }

    private void updateEventCounts(TextView textView, final Map<String, Object> map, String tag) {
        String counts = (String) map.get(tag);
        String id = (String) map.get(AppDataThing.TAG_THING_PRODUCT_ID);
        if (id != null && id.length() > 0) {
            int productId = 0;
            try {
                productId = Integer.valueOf(id);
            } catch (NumberFormatException e) {
                e.printStackTrace();
            }
            if (productId == ThingsManager.PRODUCT_ID_DOORBELL_VOICE) {
                textView.setVisibility(View.GONE);
            } else {
                if (counts != null && counts.length() > 0) {
                    int iCounts = 0;
                    try {
                        iCounts = Integer.valueOf(counts);
                    } catch (NumberFormatException e) {
                        e.printStackTrace();
                    }
                    textView.setVisibility(iCounts > 0 ? View.VISIBLE : View.INVISIBLE);
                    if (iCounts > 0 && iCounts <= MAX_EVENT_COUNT_NUMBER) {
                        textView.setText(counts);
                    } else if (iCounts > MAX_EVENT_COUNT_NUMBER) {
                        String sCount = Integer.toString(MAX_EVENT_COUNT_NUMBER) + "+";
                        textView.setText(sCount);
                    }
                }
            }
        }
    }


    private void updateBatteryLevel(ProgressBar progressBar, final Map<String, Object> map, String tag) {
        String level = (String) map.get(tag);
        if (level != null && level.length() > 0) {
            int iLevel = Integer.valueOf(level);
            int newColor = context.getResources().getColor(R.color.battery_level_bg);

            try {
                if (iLevel <= COLOR_LEVEL1) {
                    float colorOffset = ((float) 100 / (float) (COLOR_LEVEL1 - 1)) / (float) 100;
                    newColor = interpolateColor(context.getResources().getColor(R.color.battery_level_low_start), context.getResources().getColor(R.color.battery_level_low_end), iLevel * colorOffset);
                } else if (iLevel > COLOR_LEVEL1 && iLevel <= COLOR_LEVEL2) {
                    float colorOffset = ((float) 100 / (float) (COLOR_LEVEL2 - COLOR_LEVEL1 - 1)) / (float) 100;
                    newColor = interpolateColor(context.getResources().getColor(R.color.battery_level_mid_start), context.getResources().getColor(R.color.battery_level_mid_end), iLevel * colorOffset);
                } else {
                    float colorOffset = ((float) 100 / (float) (100 - COLOR_LEVEL2 - 1)) / (float) 100;
                    newColor = interpolateColor(context.getResources().getColor(R.color.battery_level_high_start), context.getResources().getColor(R.color.battery_level_high_end), iLevel * colorOffset);
                }
            } catch (NumberFormatException e) {
                e.printStackTrace();
            }

            if (iLevel >= 0 && iLevel <= 100) {
                progressBar.setProgress(iLevel);
                if (Build.VERSION.SDK_INT < Build.VERSION_CODES.LOLLIPOP) {
                    LayerDrawable progressBarDrawable = (LayerDrawable) progressBar.getProgressDrawable();
                    Drawable backgroundDrawable = progressBarDrawable.getDrawable(0);
                    Drawable progressDrawable = progressBarDrawable.getDrawable(1);
                    Drawable progressDrawable2 = progressBarDrawable.getDrawable(2);

                    backgroundDrawable.setColorFilter(context.getResources().getColor(R.color.battery_level_bg), android.graphics.PorterDuff.Mode.SRC_IN);
                    progressDrawable.setColorFilter(newColor, android.graphics.PorterDuff.Mode.SRC_IN);
                    progressDrawable2.setColorFilter(newColor, android.graphics.PorterDuff.Mode.SRC_IN);
                } else {
                    progressBar.setProgressTintList(ColorStateList.valueOf(newColor));
                }
            } else {
                progressBar.setProgress(0);
            }
        }
    }

    private void updateBatteryLevelByText(final ViewHolder holder, final Map<String, Object> map, String tag) {
        String level = (String) map.get(tag);
        if (level != null && level.length() > 0) {
            int iLevel = Integer.valueOf(level);
            String id = (String) map.get(AppDataThing.TAG_THING_PRODUCT_ID);
            int newColor = context.getResources().getColor(R.color.battery_level_bg);

            try {
                if (iLevel <= COLOR_LEVEL1) {
                    newColor = context.getResources().getColor(R.color.battery_level_low_start);
                } else if (iLevel > COLOR_LEVEL1 && iLevel <= COLOR_LEVEL2) {
                    newColor = context.getResources().getColor(R.color.battery_level_mid_start);
                } else {

                    newColor = context.getResources().getColor(R.color.battery_level_high_start);
                }
            } catch (NumberFormatException e) {
                e.printStackTrace();
            }
            if (id != null && id.length() > 0)
            {
                int productId = 0;
                try {
                    productId = Integer.valueOf(id);
                } catch (NumberFormatException e) {
                    e.printStackTrace();
                }
                if (productId == ThingsManager.PRODUCT_ID_DOORBELL ||
                        productId == ThingsManager.PRODUCT_ID_DOORBELL_VOICE
                ) {

                    if (iLevel >= COLOR_LEVEL1 && iLevel <= 100) {
                        holder.textProgress.setText(Integer.toString(iLevel) + "%");
                        holder.textProgress.setTextColor(holder.textProgress.getResources().getColor(R.color.main_bg_level1));
                        //holder.textProgress.setTextColor(newColor);
                        //holder.imgThing.setColorFilter(newColor, PorterDuff.Mode.SRC_IN);
                    } else {
                        holder.textProgress.setText(Integer.toString(iLevel) + "%");
                        holder.textProgress.setTextColor(holder.textProgress.getResources().getColor(R.color.red));
                        //holder.textProgress.setTextColor(newColor);
                    }
                }
            }
        }
    }

    private void updateThingIcon(ViewHolder holder, final Map<String, Object> map) {
        ImageView image = holder.imgThing;
        ProgressBar progressBar = holder.batteryProgress;

        String id = (String) map.get(AppDataThing.TAG_THING_PRODUCT_ID);
        THINGSTATUS online = (THINGSTATUS) map.get(AppDataThing.TAG_THING_ONLINE);
        String battery_level = (String) map.get(AppDataThing.TAG_THING_BATTERY_LEVEL);
        int iLevel = 0;

        if (battery_level != null && battery_level.length() > 0) {
            iLevel = Integer.valueOf(battery_level);
        }

        if (id != null && id.length() > 0) {
            int productId = 0;
            try {
                productId = Integer.valueOf(id);
            } catch (NumberFormatException e) {
                e.printStackTrace();
            }
            if (productId == ThingsManager.PRODUCT_ID_DOORBELL) {
                image.setImageDrawable(context.getResources().getDrawable(R.drawable.thing_icon1));
            } else if (productId == ThingsManager.PRODUCT_ID_DOORBELL_VOICE) {
                if (iLevel < COLOR_LEVEL1) {
                    image.setImageDrawable(context.getResources().getDrawable(R.drawable.voice_bell_low));
                }else{
                    image.setImageDrawable(context.getResources().getDrawable(R.drawable.voice_bell));
                }
            }
            else if (productId == ThingsManager.PRODUCT_ID_OTC) {

                if (iLevel < COLOR_LEVEL1) {
                    image.setImageDrawable(context.getResources().getDrawable(R.drawable.otc_low));
                }else{
                    image.setImageDrawable(context.getResources().getDrawable(R.drawable.otc));
                }

            } else if (productId == ThingsManager.PRODUCT_ID_OTBED) {
                if (iLevel < COLOR_LEVEL1) {
                    image.setImageDrawable(context.getResources().getDrawable(R.drawable.otcbed_low));
                }else {
                    image.setImageDrawable(context.getResources().getDrawable(R.drawable.otcbed));
                }
            } else if (productId == ThingsManager.PRODUCT_ID_PLUG) {
                boolean isOnline = (boolean) map.get(AppDataThing.TAG_THING_ONLINE);
                if (isOnline) {
                    image.setImageDrawable(context.getResources().getDrawable(R.drawable.image_plug_logo));
                    holder.textName.setTextColor(context.getResources().getColor(R.color.things_title_text));
                } else {
                    image.setImageDrawable(context.getResources().getDrawable(R.drawable.image_plug_logo_offline));
                    holder.textName.setTextColor(context.getResources().getColor(R.color.things_title_text_disable));
                }
            } else {
                image.setImageDrawable(context.getResources().getDrawable(R.drawable.unknown_thing_icon));
            }
            if (productId == ThingsManager.PRODUCT_ID_DOORBELL && DefaultAppConfig.BATTERY_LEVEL_BY_TEXT) {
                progressBar.setVisibility(View.GONE);
                holder.textProgress.setVisibility(View.VISIBLE);
            }
            else if (productId == ThingsManager.PRODUCT_ID_DOORBELL_VOICE && DefaultAppConfig.BATTERY_LEVEL_BY_TEXT) {
                progressBar.setVisibility(View.GONE);
                holder.textProgress.setVisibility(View.VISIBLE);
            }
            else {
                progressBar.setVisibility(View.GONE);
                holder.textProgress.setVisibility(View.GONE);
            }
            image.setVisibility(View.VISIBLE);
        }
    }


     public void setThingIcon(ImageView image ,int productId)
    {

        if (productId != 0 ) {
            if (productId == ThingsManager.PRODUCT_ID_DOORBELL) {
                image.setImageDrawable(context.getResources().getDrawable(R.drawable.thing_icon1));
            } else if (productId == ThingsManager.PRODUCT_ID_DOORBELL_VOICE) {
                image.setImageDrawable(context.getResources().getDrawable(R.drawable.voice_bell));
            } else if (productId == ThingsManager.PRODUCT_ID_PLUG) {
                //boolean isOnline = (boolean) map.get(AppDataThing.TAG_THING_ONLINE);
                /*
                if (isOnline) {
                    image.setImageDrawable(context.getResources().getDrawable(R.drawable.image_plug_logo));
                    //holder.textName.setTextColor(context.getResources().getColor(R.color.things_title_text));
                } else {
                    image.setImageDrawable(context.getResources().getDrawable(R.drawable.image_plug_logo_offline));
                    //holder.textName.setTextColor(context.getResources().getColor(R.color.things_title_text_disable));
                }

                 */
            } else {
                image.setImageDrawable(context.getResources().getDrawable(R.drawable.unknown_thing_icon));
            }
            /*
            if (productId == ThingsManager.PRODUCT_ID_DOORBELL && !DefaultAppConfig.BATTERY_LEVEL_BY_TEXT) {
                progressBar.setVisibility(View.VISIBLE);
                holder.textProgress.setVisibility(View.GONE);
            } else if (DefaultAppConfig.BATTERY_LEVEL_BY_TEXT) {
                holder.textProgress.setVisibility(View.VISIBLE);
                progressBar.setVisibility(View.GONE);
            } else {
                progressBar.setVisibility(View.GONE);
                holder.textProgress.setVisibility(View.GONE);
            }

             */
            image.setVisibility(View.VISIBLE);
        }
    }

    private void updateThingStatus(ViewHolder holder, final Map<String, Object> map) {

        String id = (String) map.get(AppDataThing.TAG_THING_PRODUCT_ID);
        if (id != null && id.length() > 0) {
            int productId = 0;
            try {
                productId = Integer.valueOf(id);
            } catch (NumberFormatException e) {
                e.printStackTrace();
            }
            if (productId == ThingsManager.PRODUCT_ID_DOORBELL_VOICE
                    || productId == ThingsManager.PRODUCT_ID_OTC
                    || productId == ThingsManager.PRODUCT_ID_OTBED
            ) {
                holder.textOnline.setVisibility(View.VISIBLE);
                switch((THINGSTATUS) map.get(AppDataThing.TAG_THING_ONLINE))
                {
                    case ONLINE :
                        holder.textOnline.setText(holder.textOnline.getResources().getString(R.string.txt_thing_online));
                        holder.textOnline.setTextColor(holder.textOnline.getResources().getColor(R.color.main_bg_level1));
                        break;

                    case SLEEP:
                        holder.textOnline.setText(holder.textOnline.getResources().getString(R.string.txt_thing_sleep));
                        holder.textOnline.setTextColor(holder.textOnline.getResources().getColor(R.color.main_bg_level1));
                        break;

                    case OFFLINE:
                        holder.textOnline.setText(holder.textOnline.getResources().getString(R.string.txt_thing_offline));
                        holder.textOnline.setTextColor(holder.textOnline.getResources().getColor(R.color.red));
                        break;
                }
            }
            else {
                holder.textOnline.setVisibility(View.INVISIBLE);
            }
        }
    }

    private int interpolateColor(int startColor, int endColor, float position) {
        float[] hsvStartColor = new float[3];
        Color.colorToHSV(startColor, hsvStartColor);

        float[] hsvEndColor = new float[3];
        Color.colorToHSV(endColor, hsvEndColor);

        hsvEndColor[0] = interpolate(hsvStartColor[0], hsvEndColor[0], position);
        hsvEndColor[1] = interpolate(hsvStartColor[1], hsvEndColor[1], position);
        hsvEndColor[2] = interpolate(hsvStartColor[2], hsvEndColor[2], position);

        return Color.HSVToColor(hsvEndColor);
    }

    private float interpolate(float hsvStartColor, float hsvEndColor, float position) {
        return (hsvStartColor + ((hsvEndColor - hsvStartColor) * position));
    }
}


