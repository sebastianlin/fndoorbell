package com.fusionnextinc.doorbell.objects;

import com.fusionnext.cloud.iot.objects.ErrorObj;

/**
 * Created by Mike Chang on 2017/8/9
 */
public class FNError {
    protected final static String TAG = "FNError";

    public FNError() {
        message = "";
        code = "";
        errorCode = 0;
    }

    private String message;
    private String code;
    private int  errorCode;

    public void clean() {
        message = "";
        code = "";
        errorCode = 0;
    }

    public static FNError parseError(ErrorObj errorObj) {
        FNError fnError = new FNError();

        if (errorObj == null) {
            return null;
        }
        fnError.message = errorObj._message;
        fnError.code = errorObj._code;
        fnError.errorCode = errorObj._errorCode;
        return fnError;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public int getErrorCode() {
        return errorCode;
    }

    public void setErrorCode(int errorCode) {
        this.errorCode = errorCode;
    }

}
