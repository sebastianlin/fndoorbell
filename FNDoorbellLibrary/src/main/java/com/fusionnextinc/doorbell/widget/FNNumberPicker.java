package com.fusionnextinc.doorbell.widget;

import android.content.Context;
import android.graphics.Paint;
import androidx.core.content.ContextCompat;
import android.util.AttributeSet;
import android.util.TypedValue;
import android.view.View;
import android.widget.EditText;
import android.widget.NumberPicker;

import java.lang.reflect.Field;
import java.util.Objects;

/**
 * Created by chocoyang on 2015/11/6.
 */
public class FNNumberPicker extends NumberPicker {
    private static final String TAG = "FNNumberPicker";

    private EditText ed;

    public FNNumberPicker(Context context) {
        super(context);
        initView();
    }

    public FNNumberPicker(Context context, AttributeSet attrs) {
        super(context, attrs);
        initView();
    }

    public FNNumberPicker(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        initView();
    }

    private void initView() {
        int count = getChildCount();
        for (int i = 0; i < count; i++) {
            View view = getChildAt(i);
            if (view instanceof EditText)
                ed = (EditText) view;
        }
    }

    public boolean setDivider(int id) {
        try {
            Field field = NumberPicker.class.getDeclaredField("mSelectionDivider");
            field.setAccessible(true);
            field.set(this, ContextCompat.getDrawable(getContext(), id));
            return true;
        } catch (Exception ignore) {
        }
        return false;
    }

    public void setTextColor(int color) {
        try {
            Field field = NumberPicker.class.getDeclaredField("mSelectorWheelPaint");
            field.setAccessible(true);
            ((Paint) Objects.requireNonNull(field.get(this))).setColor(color);
            ed.setTextColor(color);
        } catch (Exception ignore) {
        }
    }

    public void setTextSize(float size) {
        try {
            Field field = NumberPicker.class.getDeclaredField("mSelectorWheelPaint");
            field.setAccessible(true);
            ((Paint) Objects.requireNonNull(field.get(this))).setTextSize(size);
            ed.setTextSize(TypedValue.COMPLEX_UNIT_PX, size);

        } catch (Exception ignore) {
        }

    }
}
