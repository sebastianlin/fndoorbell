package com.fusionnextinc.doorbell.objects;

import com.fusionnext.cloud.iot.objects.State;
import java.util.ArrayList;
/**
 * Created by Mike Chang on 2017/6/8
 */

public class FNState {
    protected final static String TAG = "FNState";

    private String stateId;
    private String thingId;
    private String name;
    private String value;
    private String createdAt;
    private String updatedAt;

    public FNState() {
        stateId = "";
        thingId = "";
        name = "";
        value = "";
        createdAt = "";
        updatedAt = "";
    }

    public void clean() {
        stateId = "";
        thingId = "";
        name = "";
        value = "";
        createdAt = "";
        updatedAt = "";
    }

    public static ArrayList<FNState> parseStateArray(ArrayList<State> stateList) {
        ArrayList<FNState> fnStateList = new ArrayList<>();

        if (stateList == null) {
            return null;
        }
        for (int i = 0; i < stateList.size(); i++) {
            State state = stateList.get(i);
            FNState fnState = new FNState();

            fnState.stateId = state._stateId;
            fnState.thingId = state._thingId;
            fnState.name = state._name;
            fnState.value = state._value;
            fnState.createdAt = state._createdAt;
            fnState.updatedAt = state._updatedAt;
            fnStateList.add(fnState);
        }
        return fnStateList;
    }
    public String getStateId() {
        return stateId;
    }

    public void setStateId(String stateId) {
        this.stateId = stateId;
    }

    public String getThingId() {
        return thingId;
    }

    public void setThingId(String thingId) {
        this.thingId = thingId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }
}
