package com.fusionnextinc.doorbell.objects;

import com.fusionnext.cloud.iot.objects.UserProfile;

import java.util.ArrayList;

/**
 * Created by Mike Chang on 2017/6/5
 */
public class FNUserProfile {

    protected final static String TAG = "UserProfile";

    public FNUserProfile() {
        userId = "";
        organizationId = "";
        socialProvider = "";
        socialId = "";
        name = "";
        email = "";
        avatar = "";
        birthday = "";
        gender = "";
        locale = "";
        createdAt = "";
        updatedAt = "";
    }

    private String userId; // user id
    private String organizationId; // user id
    private String socialProvider; // user's social_provider
    private String socialId; // user's social_id
    private String email; // user's name
    private String name; // user's name
    private String avatar; // user's photo
    private String birthday;  // user's gender
    private String gender;  // user's gender
    private String locale; // user's locale
    private String createdAt;  // user's created_at
    private String updatedAt; // user's updated_ato


    public void clean() {
        userId = "";
        organizationId = "";
        socialProvider = "";
        socialId = "";
        name = "";
        email = "";
        avatar = "";
        birthday = "";
        gender = "";
        locale = "";
        createdAt = "";
        updatedAt = "";
    }

    public static ArrayList<FNUserProfile> parseUserProfileArray(ArrayList<UserProfile> userList) {
        ArrayList<FNUserProfile> fnUserList = new ArrayList<FNUserProfile>();

        if (userList == null) {
            return null;
        }
        for (int i = 0; i < userList.size(); i++) {
            UserProfile userProfile = userList.get(i);
            FNUserProfile fnUserProfile = new FNUserProfile();

            fnUserProfile.userId = userProfile._userId;
            fnUserProfile.email = userProfile._email;
            fnUserProfile.name = userProfile._name;
            fnUserProfile.createdAt = userProfile._createdAt;
            fnUserProfile.updatedAt = userProfile._updatedAt;
            fnUserList.add(fnUserProfile);
        }
        return fnUserList;
    }
    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getOrganizationId() {
        return organizationId;
    }

    public void setOrganizationId(String organizationId) {
        this.organizationId = organizationId;
    }

    public String getSocialProvider() {
        return socialProvider;
    }

    public void setSocialProvider(String socialProvider) {
        this.socialProvider = socialProvider;
    }

    public String getSocialId() {
        return socialId;
    }

    public void setSocialId(String socialId) {
        this.socialId = socialId;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAvatar() {
        return avatar;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }

    public String getBirthday() {
        return birthday;
    }

    public void setBirthday(String birthday) {
        this.birthday = birthday;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getLocale() {
        return locale;
    }

    public void setLocale(String locale) {
        this.locale = locale;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }
}
