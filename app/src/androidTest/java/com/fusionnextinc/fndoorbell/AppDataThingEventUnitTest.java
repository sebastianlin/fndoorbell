package com.fusionnextinc.fndoorbell;

import android.app.Application;
import android.content.Context;
import androidx.test.platform.app.InstrumentationRegistry;
import android.test.ApplicationTestCase;

import com.fusionnextinc.doorbell.manager.data.AppDataThingEvent;


import java.util.concurrent.CountDownLatch;

/**
 * To work on unit tests, switch the Test Artifact in the Build Variants view.
 */
public class AppDataThingEventUnitTest extends ApplicationTestCase<Application> {
    protected final static String _TAG = "AppDatsThingEventUnitTest";
    Context context;
    private CountDownLatch latch;
    private AppDataThingEvent appDataThingEvent = null;
    private final String THING_ID = "2";
    public AppDataThingEventUnitTest() {
        super(Application.class);
    }

    public void setUp() throws Exception {
        super.setUp();
        context = getTestContext();
        appDataThingEvent = AppDataThingEvent.getInstance(context);
        assertNotNull(context);

    }

    public void testStartEventsListTask() {
        latch = new CountDownLatch(1); //創建CountDownLatch
        appDataThingEvent.startEventsListTask(AppDataThingEvent.THING_TYPE_EVENTS_ALL, THING_ID, false);
        try {
            latch.await();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }


    protected void tearDown() throws Exception {
        //super.tearDown();
    }

    /**
     * @return The {@link Context} of the test project.
     */
    private Context getTestContext() {
        try {
            return (Context) InstrumentationRegistry.getTargetContext();
        } catch (final Exception exception) {
            exception.printStackTrace();
            return null;
        }
    }


}