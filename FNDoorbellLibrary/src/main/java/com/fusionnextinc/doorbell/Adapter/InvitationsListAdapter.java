package com.fusionnextinc.doorbell.Adapter;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.fusionnextinc.doorbell.MyApplication;
import com.fusionnextinc.doorbell.R;
import com.fusionnextinc.doorbell.manager.data.AppDataUser;
import com.fusionnextinc.doorbell.manager.data.ListMap;
import com.fusionnextinc.doorbell.utils.FNLog;
import com.fusionnextinc.doorbell.widget.FNHeaderListView.FNListViewAdapter;
import com.fusionnextinc.doorbell.widget.FNLayoutUtil;

import java.util.Map;

/**
 * Created by Mike Chang on 2017/7/24
 */
public class InvitationsListAdapter extends FNListViewAdapter {
    private static final String TAG = "InvitationsListAdapter";
    public final static int TYPE_THING_SHARED_USER_LIST = 0;
    public final static int TYPE_THING_INVITED_USER_LIST = 1;
    private final static int SECTION_COUNTS = TYPE_THING_INVITED_USER_LIST + 1;

    private Context context;
    private LayoutInflater mInflater;
    private FNLayoutUtil layoutUtil;
    private ListMap sharedlistMap = new ListMap();
    private ListMap invitedlistMap = new ListMap();
    private int layoutID;

    public InvitationsListAdapter(Context context, ListMap sharedlistMap, ListMap invitedlistMap, int layoutID) {
        this.context = context;
        this.mInflater = LayoutInflater.from(context);
        this.sharedlistMap.addAll(sharedlistMap);
        this.invitedlistMap.addAll(invitedlistMap);
        this.layoutID = layoutID;
    }

    private static class sectionHolder {
        //ID, its invisible
        private TextView textSection;
        private LinearLayout sectionLayout;
    }

    private static class ViewHolder {
        //ID, its invisible
        private TextView textUserId;
        private TextView textUserType;
        private LinearLayout bgLayout;
        private TextView textUserName;
        private TextView textUserAccount;
    }

    public void updateAdapterData(int thingsType, String thingId) {
        switch (thingsType) {
            case AppDataUser.USER_TYPE_THING_SHARED:
                sharedlistMap.clear();
                sharedlistMap.addAll(AppDataUser.getInstance(context).getAdapterData(thingsType, thingId));
                break;
            case AppDataUser.USER_TYPE_THING_INVITED:
                invitedlistMap.clear();
                invitedlistMap.addAll(AppDataUser.getInstance(context).getAdapterData(thingsType, thingId));
                break;
        }
        notifyDataSetChanged();
    }

    @Override
    public boolean isEnabled(int section, int position) {
        switch (section) {
            case TYPE_THING_SHARED_USER_LIST:
                return sharedlistMap != null && sharedlistMap.size() > 0;
            case TYPE_THING_INVITED_USER_LIST:
                return invitedlistMap != null && invitedlistMap.size() > 0;
            default:
                return true;
        }
    }

    @Override
    public Object getItem(int section, int position) {
        if (section < 0 || position < 0) {
            FNLog.d(TAG, "section is " + section + ", position is " + position);
            return null;
        }
        switch (section) {
            case TYPE_THING_SHARED_USER_LIST:
                return sharedlistMap.get(position);
            case TYPE_THING_INVITED_USER_LIST:
                return invitedlistMap.get(position);
        }
        return null;
    }

    @Override
    public long getItemId(int section, int position) {
        return position;
    }

    @Override
    public int getSectionCount() {
        return SECTION_COUNTS;
    }

    @Override
    public int getCountForSection(int section) {

        switch (section) {
            case TYPE_THING_SHARED_USER_LIST:
                if (sharedlistMap.size() <= 0) {
                    return 1;
                } else {
                    return sharedlistMap.size();
                }
            case TYPE_THING_INVITED_USER_LIST:
                if (invitedlistMap.size() <= 0) {
                    return 1;
                } else {
                    return invitedlistMap.size();
                }
            default:
                return 0;
        }
    }

    @Override
    public View getItemView(int section, int position, View convertView, ViewGroup parent) {
        //Ready the ViewHolder
        Map<String, Object> map;
        ViewHolder holder;

        if (convertView == null) {
            holder = new ViewHolder();
            layoutUtil = new FNLayoutUtil((Activity) context, MyApplication.LAYOUT_WIDTH, MyApplication.LAYOUT_HEIGHT, FNLayoutUtil.BASE_WIDTH);
            convertView = mInflater.inflate(layoutID, null);
            holder.bgLayout = (LinearLayout) convertView.findViewById(R.id.adapter_invitations_list);
            layoutUtil.fitAllView(holder.bgLayout);
            holder.textUserId = (TextView) convertView.findViewById(R.id.textUserId);
            holder.textUserType = (TextView) convertView.findViewById(R.id.textUserType);
            holder.textUserName = (TextView) convertView.findViewById(R.id.textUserName);
            holder.textUserAccount = (TextView) convertView.findViewById(R.id.textUserAccount);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        switch (section) {
            case TYPE_THING_SHARED_USER_LIST:
                if (sharedlistMap != null && sharedlistMap.size() > 0) {
                    map = sharedlistMap.get(position);
                    updateSharedUserView(holder, map, section);
                } else {
                    updateEmptyView(holder, section);
                }
                break;
            case TYPE_THING_INVITED_USER_LIST:
                if (invitedlistMap != null && invitedlistMap.size() > 0) {
                    map = invitedlistMap.get(position);
                    updateInvitedUserView(holder, map, section);
                } else {
                    updateEmptyView(holder, section);
                }
                break;
            default:
                break;
        }
        return convertView;
    }

    @Override
    public View getSectionHeaderView(int section, View convertView, ViewGroup parent) {
        sectionHolder holder;
        final ViewGroup nullParent = null;

        if (convertView == null) {
            holder = new sectionHolder();
            layoutUtil = new FNLayoutUtil((Activity) context, MyApplication.LAYOUT_WIDTH, MyApplication.LAYOUT_HEIGHT, FNLayoutUtil.BASE_WIDTH);
            convertView = mInflater.inflate(R.layout.thing_section_item, nullParent);
            holder.sectionLayout = (LinearLayout) convertView.findViewById(R.id.thing_section_item);
            layoutUtil.fitAllView(holder.sectionLayout);
            holder.textSection = (TextView) convertView.findViewById(R.id.textSection);
            convertView.setTag(holder);
        } else {
            holder = (sectionHolder) convertView.getTag();
        }
        switch (section) {
            case TYPE_THING_SHARED_USER_LIST:
                updateTextView(holder.textSection, context.getString(R.string.fn_title_invitations_shared));
                break;
            case TYPE_THING_INVITED_USER_LIST:
                updateTextView(holder.textSection, context.getString(R.string.msg_section_setthing_howmany_invited));
                break;
            default:
                break;
        }
        return convertView;
    }

    private void updateTextView(TextView text, String strText) {
        if (text.getId() == R.id.textSection) {
            boolean hide = (strText == null || strText.trim().equals(""));
            text.setVisibility(hide ? View.GONE : View.VISIBLE);
        }
        text.setText(strText);
    }

    private void updateSharedUserView(final ViewHolder holder, final Map<String, Object> map, final int thingType) {
        holder.textUserType.setText(String.valueOf(thingType));
        updateTextView(holder.textUserId, map, AppDataUser.TAG_USER_ID);
        updateTextView(holder.textUserName, map, AppDataUser.TAG_USER_NAME);
        updateTextView(holder.textUserAccount, map, AppDataUser.TAG_USER_EMAIL);
    }

    private void updateInvitedUserView(final ViewHolder holder, final Map<String, Object> map, final int thingType) {
        holder.textUserType.setText(String.valueOf(thingType));
        updateTextView(holder.textUserId, map, AppDataUser.TAG_USER_ID);
        updateTextView(holder.textUserName, map, AppDataUser.TAG_USER_NAME);
        updateTextView(holder.textUserAccount, map, AppDataUser.TAG_USER_EMAIL);
    }

    private void updateTextView(TextView text, Map<String, Object> map, String tag) {
        String strText = "";
        Object object = map.get(tag);
        if (object != null) {
            if (object instanceof Integer) strText = Integer.toString((Integer) object);
            else if (object instanceof String) strText = (String) object;
        }

        if (text.getId() == R.id.textUserName || text.getId() == R.id.textUserAccount) {
            boolean hide = (strText == null || strText.trim().equals(""));
            text.setVisibility(hide ? View.GONE : View.VISIBLE);
        }
        text.setText(strText);
    }

    private void updateEmptyView(final ViewHolder holder, int section) {
        switch (section) {
            case TYPE_THING_SHARED_USER_LIST:
                holder.textUserName.setText(context.getString(R.string.fn_text_shared_user_empty));
                break;
            case TYPE_THING_INVITED_USER_LIST:
                holder.textUserName.setText(context.getString(R.string.fn_text_invited_user_empty));
                break;
            default:
                break;
        }
        holder.textUserId.setText("");
        holder.textUserAccount.setVisibility(View.GONE);
        holder.textUserType.setText(String.valueOf(section));
    }
}


