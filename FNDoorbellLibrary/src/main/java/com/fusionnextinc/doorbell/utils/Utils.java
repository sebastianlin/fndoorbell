package com.fusionnextinc.doorbell.utils;

import android.content.Context;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.os.Build;
import android.util.Base64;
import android.util.Log;
import android.widget.EditText;

import java.io.InputStream;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Utils {
    public static void CopyStream(InputStream is, OutputStream os)
    {
        final int buffer_size=1024;
        try
        {
            byte[] bytes=new byte[buffer_size];
            for(;;)
            {
              int count=is.read(bytes, 0, buffer_size);
              if(count==-1)
                  break;
              os.write(bytes, 0, count);
            }
        }
        catch(Exception ex){
            ex.printStackTrace();
        }
    }



    public static boolean isNetworkConnected(Context context){
        try{
            ConnectivityManager connectivity = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
            if(connectivity != null ){
                NetworkInfo info = connectivity.getActiveNetworkInfo();
                if( info != null && info.isConnected() ){
                    return true;
                }
            }

        }catch (Exception e){
            e.printStackTrace();
        }
        return false;
    }


    public static String getFileSizeAsString(long fileSize){
        DecimalFormat df = new DecimalFormat("#.00");
        String fileSizeString;
        if( fileSize == 0){
            fileSizeString = "0 Byte";
        }else if( fileSize < 1024 ){
            fileSizeString = df.format((double)fileSize) + "Bytes";
        }else if(fileSize < 1048576){
            fileSizeString = df.format((double) fileSize / 1024) + "KB";
        }else if(fileSize < 1073741824 ){
            fileSizeString = df.format((double) fileSize / 1048576 ) + "MB";
        }else{
            fileSizeString = df.format((double) fileSize / 1073741824 ) + "GB";
        }
        return fileSizeString;
    }

    public static boolean isValidEmail(String strEmail){
        try{
            String strPattern = "^[a-zA-Z][\\w\\.-]*[a-zA-Z0-9]@[a-zA-Z0-9][\\w\\.-]*[a-zA-Z0-9]\\.[a-zA-Z][a-zA-Z\\.]*[a-zA-Z]$";
            Pattern p = Pattern.compile(strPattern);
            Matcher m = p.matcher(strEmail);
            return m.matches();

        }catch( Exception e){
            e.printStackTrace();
            Log.e("Utils", "Failed to check string");
        }
        return false;
    }

    public static boolean isVideoFile(String strFileName){
        if(strFileName == null)
            return false;
         int typeIndex = strFileName.lastIndexOf(".");
        if (typeIndex == -1)
            return false;
        String fileType = strFileName.substring(typeIndex + 1).toLowerCase();
        if(fileType == null)
            return false;
        if (fileType.equals("mp4") || fileType.equals("m4v")
                || fileType.equals("3gp") || fileType.equals("3gpp")
                || fileType.equals("3g2") || fileType.equals("3gpp2")
                || fileType.equals("wmv") || fileType.equals("avi")) {
            return true;
        }
        return false;
    }


    public static String getMacAddress(Context context) {
        WifiManager wifiMan = (WifiManager) context.getApplicationContext().getSystemService(Context.WIFI_SERVICE);
        WifiInfo wifiInf = wifiMan.getConnectionInfo();
        return wifiInf.getMacAddress();
    }

    public static String getModelName() {
        String manufacturer = Build.MANUFACTURER;
        String model = Build.MODEL;
        if (model.startsWith(manufacturer)) {
            return capitalize(model);
        } else {
            return capitalize(manufacturer) + " " + model;
        }
    }


    private static String capitalize(String s) {
        if (s == null || s.length() == 0) {
            return "";
        }
        char first = s.charAt(0);
        if (Character.isUpperCase(first)) {
            return s;
        } else {
            return Character.toUpperCase(first) + s.substring(1);
        }
    }

    public static String getAPIVerison() {

        String version = "0.0";
        try {
            version = Build.VERSION.RELEASE;
        } catch (NumberFormatException e) {
            Log.e("myApp", "error retriving api version" + e.getMessage());
        }

        return version;
    }

    public static String getAppVerison(Context context) {
        String version = "0.0.1";
        try {
            PackageInfo pInfo = context.getPackageManager().getPackageInfo(context.getPackageName(), 0);
            version = pInfo.versionName;
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
        return version;
    }


    public static String getPackageName(Context context) {
        return context.getPackageName();
    }

    public static String getManufacturer(Context context) {
        String manufacturer = "none";
        try {
            manufacturer = Build.MANUFACTURER;
        } catch (NumberFormatException e) {
            Log.e("myApp", "error retriving manufacturer" + e.getMessage());
        }
        return manufacturer;
    }

    public static String getDeviceName(Context context) {
        String name = "none";
        try {
            name = Build.DEVICE;
        } catch (NumberFormatException e) {
            Log.e("myApp", "error retriving device name" + e.getMessage());
        }
        return name;
    }


    public static String decodeJWT(String token){
        String text = null;
        String[] separated = null;
        if (token != null) {
            separated = token.split("\\.");
        }
        if (separated != null && separated[1] != null) {
            byte[] data = Base64.decode(separated[1], Base64.DEFAULT);
            try {
                text = new String(data, "UTF-8");
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }
        }
        return text;
    }
    public static boolean emailIsValid(String email) {
        String expression = "^[\\w\\.-]+@([\\w\\-]+\\.)+[A-Z]{2,4}$";
        CharSequence inputStr = email.trim();

        Pattern pattern = Pattern.compile(expression, Pattern.CASE_INSENSITIVE);
        Matcher matcher = pattern.matcher(inputStr);
        return matcher.matches();
    }

    public static boolean emptyFieldsRemaining(EditText... ets) {
        for (EditText et : ets) {
            String input = et.getText().toString().trim();
            if (input.isEmpty()) {
                return true;
            }
        }
        return false;
    }

    public static boolean stringsNullOrEmpty(String... strings) {
        for(String string : strings){
            if(string == null || string.isEmpty()){
                return true;
            }
        }
        return false;
    }

    public static boolean birthdayIsValid(Date birthday){
        Calendar c = Calendar.getInstance();
        c.add(Calendar.YEAR, -13);
        return birthday.before(c.getTime());
    }

    public static boolean passwordIsValid(String password) {
        if (!password.toString().matches("^(?=.*\\d)(?=.*[A-Za-z]).{8,32}$")) {
            return false;
        }
        return true;
    }
}