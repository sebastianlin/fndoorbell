package com.fusionnextinc.doorbell.manager.thingsManager;

import com.fusionnext.cloud.iot.objects.THINGSTATUS;
import com.fusionnextinc.doorbell.objects.FNError;
import com.fusionnextinc.doorbell.objects.FNEvent;
import com.fusionnextinc.doorbell.objects.FNFirmware;
import com.fusionnextinc.doorbell.objects.FNSchedule;
import com.fusionnextinc.doorbell.objects.FNThing;
import com.fusionnextinc.doorbell.objects.FNUserProfile;

import java.util.ArrayList;

/**
 * Created by Mike Chang on 2017/6/3
 */
public abstract class ThingsHandler {

    public interface ThingsListener {
        void onListUserOwnedThings(ThingsManager instance, boolean result, String userId, ArrayList<FNThing> thingArrayList, FNError error);

        void onListUserInvitedThings(ThingsManager instance, boolean result, String userId, ArrayList<FNThing> thingArrayList, FNError error);

        void onListUserSharedThings(ThingsManager instance, boolean result, String userId, ArrayList<FNThing> thingArrayList, FNError error);

        void onListUserAllThings(ThingsManager instance, boolean result, String userId, ArrayList<FNThing> ownedThingArrayList, ArrayList<FNThing> sharedThingArrayList, ArrayList<FNThing> invitedThingArrayList, FNError error);

        void onListThingEvents(ThingsManager instance, boolean result, String thingId, String eventName, ArrayList<FNEvent> eventList, FNError error);

        void onListThingEventsByTime(ThingsManager instance, boolean result, String thingId, String eventName, String startTime, String endTime, ArrayList<FNEvent> eventList, FNError error);

        void onCurBatteryLevel(ThingsManager instance, boolean result, String thingId, String batteryLevel, FNError error);

        void onCurFirmwareVersion(ThingsManager instance, boolean result, String thingId, String fwVersion, FNError error);

        void onCurSwitchedStatus(ThingsManager instance, boolean result, String thingId, boolean isSwitch, FNError error);

        void onNewFirmwareVersion(ThingsManager instance, boolean result, String thingId, FNFirmware fnFirmware);

        void onStartFwUpdate(ThingsManager instance, boolean result, String thingId, String fwId);

        void onCurRingTone(ThingsManager instance, boolean result, String thingId, String ringtoneId, FNError error);

        void onGetScheduleList(ThingsManager instance, boolean result, String thingId, ArrayList<FNSchedule> scheduleList, FNError error);

        void onListInvitedUsers(ThingsManager instance, boolean result, String thingId, ArrayList<FNUserProfile> userList, FNError error);

        void onListSharedUsers(ThingsManager instance, boolean result, String thingId, ArrayList<FNUserProfile> userList, FNError error);

        void onCreateClaimCode(ThingsManager instance, boolean result, String claimCode, FNError error);

        void onCreateThing(ThingsManager instance, boolean result, FNThing fnThing, FNError error);

        void onUpdateThing(ThingsManager instance, boolean result, FNThing fnThing, FNError error);

        void onDeleteThing(ThingsManager instance, boolean result, FNError error);

        void onGetThing(ThingsManager instance, boolean result, FNThing fnThing, FNError error);

        void onChangeRingtone(ThingsManager instance, boolean result, String thingId, int ringtoneId);

        void onSetTimer(ThingsManager instance, boolean result, String thingId, String value);

        void onSetSchedule(ThingsManager instance, boolean result, String thingId, String value);

        void onShareInvited(ThingsManager instance, boolean result, String thingId, FNError error);

        void onShareAccepted(ThingsManager instance, boolean result, String thingId, FNError error);

        void onShareRejected(ThingsManager instance, boolean result, String thingId, FNError error);

        void onSwitchOnOff(ThingsManager instance, boolean result, String thingId, boolean isTurnOn);

        void onCloseCallSlot(ThingsManager instance, boolean result, FNError error);

        void onTakeCallSlot(ThingsManager instance, boolean result, String thingId, String callSlot, FNError error);
    }

    public interface mqttListener {
        void onMqttConnected();

        void onDeviceSwitchOn(ThingsManager instance, String thingId, boolean isTurnOn);

        void onDeviceOnLine(ThingsManager instance, String thingId, THINGSTATUS isOnline);
    }
}
