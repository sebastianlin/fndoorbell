package com.fusionnextinc.doorbell.fragement;

import android.graphics.Bitmap;
import android.os.Bundle;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.fusionnextinc.doorbell.MyApplication;
import com.fusionnextinc.doorbell.R;
import com.fusionnextinc.doorbell.manager.account.AccountManager;
import com.fusionnextinc.doorbell.manager.data.AppDataThing;
import com.fusionnextinc.doorbell.manager.qrCode.QRCodeGenerator;
import com.fusionnextinc.doorbell.manager.thingsManager.ThingsHandler;
import com.fusionnextinc.doorbell.manager.thingsManager.ThingsManager;
import com.fusionnextinc.doorbell.objects.FNError;
import com.fusionnextinc.doorbell.objects.FNEvent;
import com.fusionnextinc.doorbell.objects.FNFirmware;
import com.fusionnextinc.doorbell.objects.FNQRInfo;
import com.fusionnextinc.doorbell.objects.FNSchedule;
import com.fusionnextinc.doorbell.objects.FNThing;
import com.fusionnextinc.doorbell.objects.FNUserProfile;
import com.fusionnextinc.doorbell.widget.FNAlert.FNAlertDialog;
import com.fusionnextinc.doorbell.widget.FNDialog;
import com.fusionnextinc.doorbell.widget.FNLayoutUtil;
import com.google.zxing.WriterException;

import java.util.ArrayList;

/**
 * Created by Mike Chang on 2017/8/1
 */
public class MyQRCodeFragment extends FNFragment {
    private static final String TAG = MyQRCodeFragment.class.getSimpleName();
    private FNLayoutUtil layoutUtil;
    private QRCodeGenerator qrCodeGenerator;
    private AccountManager accountManager;
    public static final int MYQRCODE_FRAGMENT_TYPE_NORMAL = 0;
    public static final int MYQRCODE_FRAGMENT_TYPE_INVITATION = 1;
    private int myQrcodeFragmentType = MYQRCODE_FRAGMENT_TYPE_NORMAL;
    private String thingInfo = "";
    private ThingsManager thingsManager;

    public static void openFragment(boolean clearOtherStack) {
        MyQRCodeFragment myQrcodeFragment = new MyQRCodeFragment();
        FNFragmentManager.getInstance().openFragment(myQrcodeFragment, clearOtherStack);
    }

    public static void replaceFragment(int fragmentType, String info) {
        MyQRCodeFragment myQrcodeFragment = new MyQRCodeFragment();
        myQrcodeFragment.myQrcodeFragmentType = fragmentType;
        myQrcodeFragment.thingInfo = info;
        FNFragmentManager.getInstance().replaceFragment(myQrcodeFragment);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        layoutUtil = new FNLayoutUtil(getActivity(), MyApplication.LAYOUT_WIDTH, MyApplication.LAYOUT_HEIGHT, FNLayoutUtil.BASE_WIDTH);
        qrCodeGenerator = QRCodeGenerator.getInstance(getActivity());
        accountManager = AccountManager.getInstance(getActivity());
        thingsManager = ThingsManager.getInstance(getActivity());
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        final ViewGroup nullParent = null;
        String account = accountManager.getUserAccount();
        Bitmap bitmapQRcode;
        getActionBar().resetAll();
        getActionBar().setTitle(getString(R.string.fn_title_settings_my_qr_code), Gravity.CENTER, null);
        getActionBar().setCoverMode(false);
        getActionBar().setStartImage(R.drawable.back, new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        getActionBar().show();
        View view = inflater.inflate(R.layout.fragment_my_qrcode, nullParent);
        layoutUtil.fitAllView(view);
        ImageView imgQRCode = (ImageView) view.findViewById(R.id.img_qrcode);
        TextView txtHint = (TextView) view.findViewById(R.id.text_description);
        txtHint.setText(getString(R.string.fn_txt_my_qrcode_description).replace("${Thing}", getActivity().getString(R.string.fn_title_thing_name)));
        if (account != null && account.length() > 0) {
            FNQRInfo fnQRInfo = new FNQRInfo();
            try {
                fnQRInfo.setEmail(account);
                bitmapQRcode = qrCodeGenerator.genQRCode(fnQRInfo.getJsonString());
                if (bitmapQRcode != null) {
                    imgQRCode.setImageBitmap(bitmapQRcode);
                }
            } catch (WriterException e) {
                e.printStackTrace();
            }
        }

        if (myQrcodeFragmentType == MYQRCODE_FRAGMENT_TYPE_INVITATION) {
            showInvitationDialog(thingInfo);
        }
        return view;
    }

    private void showInvitationDialog(final String thingId) {
        FNThing fnThing;
        String user = getString(R.string.fn_text_none);
        String thing = getString(R.string.fn_text_none);

        if (thingId != null && thingId.length() > 0) {
            fnThing = AppDataThing.getInstance(getActivity()).getThing(thingId);
            if (fnThing != null) {
                if (fnThing.getUserName() != null) {
                    user = fnThing.getUserName();
                }
                if (fnThing.getName() != null) {
                    thing = fnThing.getName();
                }
            }
        }

        new FNAlertDialog(getActivity(), FNAlertDialog.WARNING_TYPE)
                .setTitleText(getActivity().getString(R.string.fn_dialog_are_you_sure))
                .setContentText(getActivity().getString(R.string.fn_dialog_invitations).replace("${USER}", user).replace("${ITEM}", thing))
                .setCancelText(getActivity().getString(R.string.fn_dialog_reject))
                .setConfirmText(getActivity().getString(R.string.fn_dialog_accept))
                .showCancelButton(true)
                .setCancelClickListener(new FNAlertDialog.OnSweetClickListener() {
                    @Override
                    public void onClick(FNAlertDialog sDialog) {
                        thingsManager.setThingsListener(thingsListener);
                        thingsManager.shareReject(thingId);
                        sDialog.dismiss();
                    }
                })
                .setConfirmClickListener(new FNAlertDialog.OnSweetClickListener() {
                    @Override
                    public void onClick(FNAlertDialog sDialog) {
                        thingsManager.setThingsListener(thingsListener);
                        thingsManager.shareAccept(thingId);
                        sDialog.dismiss();
                    }
                })
                .show();
        myQrcodeFragmentType = MYQRCODE_FRAGMENT_TYPE_NORMAL;
    }


    final ThingsHandler.ThingsListener thingsListener = new ThingsHandler.ThingsListener() {

        @Override
        public void onListUserOwnedThings(ThingsManager instance, boolean result, String userId, ArrayList<FNThing> thingArrayList, FNError error) {

        }

        @Override
        public void onListUserInvitedThings(ThingsManager instance, boolean result, String userId, ArrayList<FNThing> thingArrayList, FNError error) {

        }

        @Override
        public void onListUserSharedThings(ThingsManager instance, boolean result, String userId, ArrayList<FNThing> thingArrayList, FNError error) {

        }

        @Override
        public void onListUserAllThings(ThingsManager instance, boolean result, String userId, ArrayList<FNThing> ownedThingArrayList, ArrayList<FNThing> sharedThingArrayList, ArrayList<FNThing> invitedThingArrayList, FNError error) {

        }

        @Override
        public void onListThingEvents(ThingsManager instance, boolean result, final String thingId, String eventName, ArrayList<FNEvent> eventList, FNError error) {

        }

        @Override
        public void onListThingEventsByTime(ThingsManager instance, boolean result, final String thingId, String eventName, String startTime, String endTime, ArrayList<FNEvent> eventList, FNError error) {

        }

        @Override
        public void onCurBatteryLevel(ThingsManager instance, boolean result, String thingId, String batteryLevel, FNError error) {

        }

        @Override
        public void onCurFirmwareVersion(ThingsManager instance, boolean result, String thingId, String fwVersion, FNError error) {

        }

        @Override
        public void onCurSwitchedStatus(ThingsManager instance, boolean result, String thingId, boolean isSwitch, FNError error) {

        }

        @Override
        public void onNewFirmwareVersion(ThingsManager instance, boolean result, String thingId, FNFirmware fnFirmware) {

        }

        @Override
        public void onStartFwUpdate(ThingsManager instance, boolean result, String thingId, String fwId) {

        }

        @Override
        public void onCurRingTone(ThingsManager instance, boolean result, String thingId, String ringtoneId, FNError error) {

        }

        @Override
        public void onGetScheduleList(ThingsManager instance, boolean result, String thingId, ArrayList<FNSchedule> scheduleList, FNError error) {

        }

        @Override
        public void onListInvitedUsers(ThingsManager instance, boolean result, String thingId, ArrayList<FNUserProfile> userList, FNError error) {

        }

        @Override
        public void onListSharedUsers(ThingsManager instance, boolean result, String thingId, ArrayList<FNUserProfile> userList, FNError error) {

        }

        @Override
        public void onCreateClaimCode(ThingsManager instance, boolean result, String claimCode, FNError error) {

        }

        @Override
        public void onCreateThing(ThingsManager instance, boolean result, FNThing fnThing, FNError error) {

        }

        @Override
        public void onUpdateThing(ThingsManager instance, boolean result, FNThing fnThing, FNError error) {

        }

        @Override
        public void onDeleteThing(ThingsManager instance, boolean result, FNError error) {

        }

        @Override
        public void onGetThing(ThingsManager instance, boolean result, FNThing fnThing, FNError error) {

        }

        @Override
        public void onChangeRingtone(ThingsManager instance, boolean result, String thingId, int ringtoneId) {

        }

        @Override
        public void onSetTimer(ThingsManager instance, boolean result, String thingId, String value) {

        }

        @Override
        public void onSetSchedule(ThingsManager instance, boolean result, String thingId, String value) {

        }

        @Override
        public void onShareInvited(ThingsManager instance, boolean result, String thingId, FNError error) {

        }

        @Override
        public void onShareAccepted(ThingsManager instance, boolean result, String thingId, FNError error) {
            FNThing fnThing;
            String user = getString(R.string.fn_text_none);
            String thing = getString(R.string.fn_text_none);

            if (thingId != null && thingId.length() > 0) {
                fnThing = AppDataThing.getInstance(getActivity()).getThing(thingId);
                if (fnThing != null) {
                    if (fnThing.getUserName() != null) {
                        user = fnThing.getUserName();
                    }
                    if (fnThing.getName() != null) {
                        thing = fnThing.getName();
                    }
                }
            }

            if (isAdded()) {
                if (result) {
                    FNDialog.showSuccessDialog(getActivity(), getString(R.string.fn_dialog_invitations).replace("${USER}", user).replace("${ITEM}", thing), getString(R.string.fn_dialog_invitation_accepted));
                } else {
                    FNDialog.showErrorDialog(getActivity(), getString(R.string.fn_dialog_invitation_accept_failed), error);
                }
            }
        }

        @Override
        public void onShareRejected(ThingsManager instance, boolean result, String thingId, FNError error) {
            FNThing fnThing;
            String user = getString(R.string.fn_text_none);
            String thing = getString(R.string.fn_text_none);

            if (thingId != null && thingId.length() > 0) {
                fnThing = AppDataThing.getInstance(getActivity()).getThing(thingId);
                if (fnThing != null) {
                    user = fnThing.getUserName();
                    thing = fnThing.getName();
                }
            }

            if (result) {
                FNDialog.showSuccessDialog(getActivity(), getString(R.string.fn_dialog_invitations).replace("${USER}", user).replace("${ITEM}", thing), getString(R.string.fn_dialog_invitation_rejected));
            } else {
                FNDialog.showErrorDialog(getActivity(), getString(R.string.fn_dialog_invitation_reject_failed), error);
            }
        }

        @Override
        public void onSwitchOnOff(ThingsManager instance, boolean result, String thingId, boolean isTurnOn) {

        }

        @Override
        public void onCloseCallSlot(ThingsManager instance, boolean result, FNError error) {

        }

        @Override
        public void onTakeCallSlot(ThingsManager instance, boolean result, String thingId, String callSlot, FNError error) {

        }
    };

}
