package com.fusionnext.cloud.datalink.http;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;

import org.apache.http.Header;
import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.conn.ssl.AllowAllHostnameVerifier;
import org.apache.http.conn.ssl.SSLSocketFactory;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;
import org.apache.http.util.EntityUtils;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Locale;
import java.util.zip.GZIPInputStream;

/**
 * Created by Mike Chang on 2016/2/22.
 */
public class HttpUtil {
    public final static int _HTTP_CONNECT_TIMEOUT = 5000;
    public final static int _HTTP_READ_TIMEOUT = 5000;

    //Broadcast action definition
    public final static String BC_HTTP_UPLOAD_PERCENT = "BC_HTTP_UPLOAD_PERCENT";
    public final static String BC_HTTP_POST_FEEDBACK = "BC_HTTP_POST_FEEDBACK";
    public final static String BC_HTTP_DOWNLOAD_FEEDBACK = "BC_HTTP_DOWNLOAD_FEEDBACK";

    private Context context;

    public HttpUtil(Context c) {
        this.context = c;
    }

    private static boolean stopDownload = false;

    /**
     * 下载文件指定目录下
     *
     * @param urlStr   网络地址
     * @param filename 保存名称
     */
    public boolean downFileToCache(String urlStr, String filename, OnDownloadListener onDownloadListener) {
        stopDownload = false;
        String path = context.getFilesDir().getPath() + "/";
        String f_name = path + filename;
        long totalSize = 0;

        //if (Public.isFileExist(f_name)) return true;

        boolean ret = false;

        if ((urlStr == null || "".equals(urlStr))
                || (filename == null || "".equals(filename))) {
            if (onDownloadListener != null) {
                onDownloadListener.onError();
            }
            return false;
        }

        InputStream inputStream;
        try {
            URL url = new URL(urlStr);
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.setConnectTimeout(_HTTP_CONNECT_TIMEOUT);
            conn.setReadTimeout(_HTTP_READ_TIMEOUT);
            conn.setDoInput(true);
            conn.setRequestMethod("GET");
            conn.setRequestProperty("Content-Type", "text/html");
            conn.setRequestProperty("Accept-Charset", "utf-8");
            conn.setRequestProperty("contentType", "utf-8");
            totalSize = conn.getContentLength();
            inputStream = conn.getInputStream();
        } catch (IOException e) {
            e.printStackTrace();
            return false;
        }

        FileOutputStream outStream = null;
        try {
            /*
            openFileOutput()方法的第二参数用于指定操作模式，有四种模式，分别为：
                Context.MODE_PRIVATE    =  0
                Context.MODE_APPEND    =  32768
                Context.MODE_WORLD_READABLE =  1
                Context.MODE_WORLD_WRITEABLE =  2

            Context.MODE_PRIVATE：为默认操作模式，代表该文件是私有数据，只能被应用本身访问，
                在该模式下，写入的内容会覆盖原文件的内容
            Context.MODE_APPEND：模式会检查文件是否存在，存在就往文件追加内容，否则就创建新文件。
            Context.MODE_WORLD_READABLE和Context.MODE_WORLD_WRITEABLE
                用来控制其他应用是否有权限读写该文件。
                MODE_WORLD_READABLE：表示当前文件可以被其他应用读取；
                MODE_WORLD_WRITEABLE：表示当前文件可以被其他应用写入。
                如果希望文件被其他应用读和写，可以传入：
                Context.MODE_WORLD_READABLE + Context.MODE_WORLD_WRITEABLE
             */
            outStream = context.openFileOutput(filename, Context.MODE_PRIVATE);
            int temp;
            long readSize = 0;
            final byte[] data = new byte[1024 * 256];
            if (onDownloadListener != null) {
                onDownloadListener.onStart();
            }
            while (((temp = inputStream.read(data)) != -1) && !stopDownload) {
                readSize = readSize + temp;
                long progress = (readSize * 100 / totalSize);
                outStream.write(data, 0, temp);
                if (onDownloadListener != null) {
                    onDownloadListener.onProgress(progress);
                }
            }
            if (stopDownload) {
                if (onDownloadListener != null) {
                    onDownloadListener.onStop();
                }
            }
            ret = true;
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (outStream != null) {
                try {
                    outStream.flush();
                    outStream.close();
                    ret = true;
                    if (onDownloadListener != null && !stopDownload) {
                        onDownloadListener.onComplete();
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        return ret;
    }

    public String getHttpJSON(String url, String accessToken, boolean keepAlive) {
        HttpGet httpRequest = new HttpGet(url);
        HttpClient httpclient = null;

        SSLSocketFactory.getSocketFactory().setHostnameVerifier(new AllowAllHostnameVerifier());
        Locale locale = context.getResources().getConfiguration().locale;
        String localeStr = locale.getLanguage().toLowerCase() + "-" + locale.getCountry().toLowerCase();
        if (localeStr.equals("")) localeStr = "en";
        httpRequest.addHeader("Accept-Language", localeStr);
        httpRequest.addHeader("Accept-Encoding", "gzip,deflate");
        if (accessToken != null)
            httpRequest.addHeader("Authorization", accessToken);
        try {
            if (!keepAlive) {
                HttpParams httpParams = new BasicHttpParams();
                int timeoutConnection = 3000; //3 seconds
                HttpConnectionParams.setConnectionTimeout(httpParams, timeoutConnection);
                int timeoutSocket = 5000; //5 seconds;
                HttpConnectionParams.setSoTimeout(httpParams, timeoutSocket);
                httpclient = new DefaultHttpClient(httpParams);
            } else {
                //httpclient = new DefaultHttpClient();
                httpclient =  MySSLSocketFactory. createMyHttpClient();
            }
            HttpResponse httpResponse = httpclient.execute(httpRequest);

            if (httpResponse.getStatusLine().getStatusCode() == HttpStatus.SC_OK) {
                Header contentEncoding = httpResponse.getFirstHeader("Content-Encoding");
                if (contentEncoding != null && contentEncoding.getValue().equalsIgnoreCase("gzip")) {

                    InputStream is = new GZIPInputStream(httpResponse.getEntity().getContent());
                    BufferedReader br = new BufferedReader(new InputStreamReader(is));
                    String strLine = is.toString();
                    StringBuilder stringBuffer = new StringBuilder();
                    while ((strLine = br.readLine()) != null) {
                        stringBuffer.append(strLine);
                    }
                    is.close();
                    return stringBuffer.toString();
                }
                if (!keepAlive) {
                    httpclient.getConnectionManager().shutdown();
                }
                return EntityUtils.toString(httpResponse.getEntity(), "UTF-8");
            } else {
                if (!keepAlive) {
                    httpclient.getConnectionManager().shutdown();
                }
                return EntityUtils.toString(httpResponse.getEntity());
            }
        } catch (Exception e) {
            e.printStackTrace();
            System.out.println("==============e.printStackTrace() : "
                    + e.getMessage());
        }
        return "";
    }


    public interface OnDownloadListener {
        void onStart();

        void onStop();

        void onProgress(long progress);

        void onComplete();

        void onError();
    }

    public static void broadcastWithExtra(Context context, String action, Bundle extra) {
        Intent intent = new Intent(action);
        if (extra != null) intent.putExtras(extra);
        context.sendBroadcast(intent);
    }
}
