package com.fusionnext.cloud.fusionapi;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * Created by Mike Chang on 2016/2/22.
 */
public class DataManager extends SQLiteOpenHelper {
    //Device Token
    private final static int _PARAM_AUTH_DEVICE_ACCESS_TOKEN = 20230;
    private final static int _PARAM_AUTH_DEVICE_REFRESH_TOKEN = 20240;
    private final static int _PARAM_AUTH_DEVICE_EXPIRE_IN = 20250;
    private final static int _PARAM_AUTH_CLIENT_ID = 20260;

    //Permission
    private final static int _PARAM_AUTH_SDK_PERMISSION = 20500;
    private final static int _PARAM_AUTH_CAMERA_PERMISSION = 20510;
    private final static int _PARAM_AUTH_PLAYER_PERMISSION = 20520;

    //Device
    private final static int _PARAM_DEVICES_DEVICE_ID = 20300;
    private final static int _PARAM_DEVICES_DEVICE_CLIENT_ID = 20310;
    private final static int _PARAM_DEVICES_DEVICE_USER_ID = 20320;
    private final static int _PARAM_DEVICES_DEVICE_IDENTIFIER = 20330;
    private final static int _PARAM_DEVICES_DEVICE_NAME = 20340;
    private final static int _PARAM_DEVICES_DEVICE_MANUFACTURER = 20350;
    private final static int _PARAM_DEVICES_DEVICE_MODEL = 20360;
    private final static int _PARAM_DEVICES_DEVICE_PLATFORM = 20370;
    private final static int _PARAM_DEVICES_DEVICE_PLATFORM_VERISON = 20380;
    private final static int _PARAM_DEVICES_DEVICE_PUSH_TOKEN = 20390;
    private final static int _PARAM_DEVICES_DEVICE_CREATE_AT = 20400;
    private final static int _PARAM_DEVICES_DEVICE_UPDATE_AT = 20410;
    private final static int _PARAM_DEVICES_DEVICE_USER = 20420;

    private SQLiteDatabase sdb;

    //Setting table
    final String CREATE_SETTINGS_TABLE = "CREATE TABLE IF NOT EXISTS t_settings " +
            "(_id integer primary key autoincrement, " +
            "f_param integer, " +
            "f_str_value nvarchar(8192), " +
            "f_int_value integer)";


    public DataManager(Context context, String name,
                       SQLiteDatabase.CursorFactory factory, int version) {
        super(context, name, factory, version);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(CREATE_SETTINGS_TABLE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        //Used tables
        db.execSQL("DROP TABLE IF EXISTS t_settings");
        onCreate(db);
    }

    public static DataManager newData(Context context) {
        return new DataManager(context, Public.Defines._DB_NAME, null, Public.Defines._DB_VERSION);
    }

    private String getCursorString(Cursor cursor, String name) {
        if (cursor == null) return "";
        int index = cursor.getColumnIndex(name);
        if (index < 0) return "";
        return cursor.getString(index);
    }

    private int getCursorInt(Cursor cursor, String name, int defaultInt) {
        if (cursor == null) return defaultInt;
        int index = cursor.getColumnIndex(name);
        if (index < 0) return defaultInt;
        return cursor.getInt(index);
    }

    //Check the param setting is exist or not
    private boolean isParamExist(int param) {
        sdb = getReadableDatabase();
        String sql = "select _id from t_settings where f_param = " + param;
        Cursor cursor = null;
        try {
            int count;

            cursor = sdb.rawQuery(sql, null);
            count = cursor.getCount();

            return count > 0;
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            if (cursor != null) cursor.close();
        }

        return false;
    }

    //Get the param value for integer type
    private int getIntValue(int param, int defaultValue) {
        sdb = getReadableDatabase();
        String sql = "select f_param, f_int_value from t_settings where f_param = " + param;
        Cursor cursor = null;
        try {
            int ret = defaultValue;

            cursor = sdb.rawQuery(sql, null);
            if (cursor.getCount() > 0) {
                cursor.moveToFirst();
                ret = cursor.getInt(cursor.getColumnIndex("f_int_value"));
            }
            return ret;

        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            if (cursor != null) cursor.close();
        }

        return defaultValue;
    }

    //Save the param value for integer type
    private boolean setIntValue(int param, int value) {
        sdb = getWritableDatabase();
        String sql;
        Object[] data;
        try {
            if (isParamExist(param)) {
                sql = "update t_settings set f_int_value = ? where f_param = ?";
                data = new Object[]{value, param};
            } else {
                sql = "insert into t_settings (f_param, f_str_value, f_int_value) values(?, ?, ?)";
                data = new Object[]{param, "", value};
            }

            sdb.execSQL(sql, data);
            return true;
        } catch (Exception ex) {
            ex.printStackTrace();
        }

        return false;
    }

    //Get the param value for String type
    private String getStringValue(int param, String defaultValue) {
        sdb = getReadableDatabase();
        String sql = "select f_param, f_str_value from t_settings where f_param = " + param;
        Cursor cursor = null;
        try {
            String ret = defaultValue;

            cursor = sdb.rawQuery(sql, null);
            if (cursor.getCount() > 0) {
                cursor.moveToFirst();
                ret = cursor.getString(cursor.getColumnIndex("f_str_value"));
            }

            return ret;
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            if (cursor != null) cursor.close();
        }

        return defaultValue;
    }

    //Save the param value for integer type
    private boolean setStringValue(int param, String value) {
        sdb = getWritableDatabase();
        String sql;
        Object[] data;
        try {
            if (isParamExist(param)) {
                sql = "update t_settings set f_str_value = ? where f_param = ?";
                data = new Object[]{value, param};
            } else {
                sql = "insert into t_settings (f_param, f_str_value, f_int_value) values(?, ?, ?)";
                data = new Object[]{param, value, 0};
            }

            sdb.execSQL(sql, data);
            return true;
        } catch (Exception ex) {
            ex.printStackTrace();
        }

        return false;
    }

    public boolean saveDeviceToken(Authorization.DeviceToken deviceToken) {
        boolean b1 = setStringValue(_PARAM_AUTH_DEVICE_ACCESS_TOKEN, deviceToken.accessToken);
        boolean b2 = setStringValue(_PARAM_AUTH_CLIENT_ID, deviceToken.clientId);
        boolean b3 = setStringValue(_PARAM_AUTH_DEVICE_EXPIRE_IN, deviceToken.expireIn);
        boolean b4 = setStringValue(_PARAM_AUTH_DEVICE_REFRESH_TOKEN, deviceToken.refreshToken);

        return b1 && b2 && b3 && b4;
    }

    //Get device token
    public Authorization.DeviceToken loadDeviceToken() {
        Authorization.DeviceToken deviceToken = new Authorization.DeviceToken();

        deviceToken.accessToken = getStringValue(_PARAM_AUTH_DEVICE_ACCESS_TOKEN, "");
        deviceToken.clientId = getStringValue(_PARAM_AUTH_CLIENT_ID, "");
        deviceToken.expireIn = getStringValue(_PARAM_AUTH_DEVICE_EXPIRE_IN, "");
        deviceToken.refreshToken = getStringValue(_PARAM_AUTH_DEVICE_REFRESH_TOKEN, "");
        return deviceToken;
    }

    public boolean savePermission(Authorization.Permission permission) {
        boolean b1 = setIntValue(_PARAM_AUTH_SDK_PERMISSION, permission.sdkPermisiion);
        boolean b2 = setIntValue(_PARAM_AUTH_CAMERA_PERMISSION, permission.cameraPermisiion);
        boolean b3 = setIntValue(_PARAM_AUTH_PLAYER_PERMISSION, permission.playerPermisiion);

        return b1 && b2 && b3;
    }

    //Get device token
    public Authorization.Permission loadPermission() {
        Authorization.Permission permission = new Authorization.Permission();

        permission.sdkPermisiion = getIntValue(_PARAM_AUTH_SDK_PERMISSION, 0xFF);
        permission.cameraPermisiion = getIntValue(_PARAM_AUTH_CAMERA_PERMISSION, 0xFF);
        permission.playerPermisiion = getIntValue(_PARAM_AUTH_PLAYER_PERMISSION, 0xFF);
        return permission;
    }

    public boolean saveDevice(Devices.Device device) {
        boolean b1 = setStringValue(_PARAM_DEVICES_DEVICE_ID, device.deviceId);
        boolean b2 = setStringValue(_PARAM_DEVICES_DEVICE_CLIENT_ID, device.clientId);
        boolean b3 = setStringValue(_PARAM_DEVICES_DEVICE_USER_ID, device.userId);
        boolean b4 = setStringValue(_PARAM_DEVICES_DEVICE_IDENTIFIER, device.deviceIdentifer);
        boolean b5 = setStringValue(_PARAM_DEVICES_DEVICE_NAME, device.deviceName);
        boolean b6 = setStringValue(_PARAM_DEVICES_DEVICE_MANUFACTURER, device.manufacturer);
        boolean b7 = setStringValue(_PARAM_DEVICES_DEVICE_MODEL, device.model);
        boolean b8 = setStringValue(_PARAM_DEVICES_DEVICE_PLATFORM, device.platform);
        boolean b9 = setStringValue(_PARAM_DEVICES_DEVICE_PLATFORM_VERISON, device.platformVersion);
        boolean b10 = setStringValue(_PARAM_DEVICES_DEVICE_PUSH_TOKEN, device.pushToken);
        boolean b11 = setStringValue(_PARAM_DEVICES_DEVICE_CREATE_AT, device.createAt);
        boolean b12 = setStringValue(_PARAM_DEVICES_DEVICE_UPDATE_AT, device.updatedAt);
        boolean b13 = setStringValue(_PARAM_DEVICES_DEVICE_USER, device.user);
        return b1 && b2 && b3 && b4 && b5 && b6 && b7 && b8 && b9 && b10 && b11 && b12 && b13;
    }

    //Get device
    public Devices.Device loadDevice() {
        Devices.Device device = new Devices.Device();
        device.deviceId = getStringValue(_PARAM_DEVICES_DEVICE_ID, device.deviceId);
        device.clientId = getStringValue(_PARAM_DEVICES_DEVICE_CLIENT_ID, device.clientId);
        device.userId = getStringValue(_PARAM_DEVICES_DEVICE_USER_ID, device.userId);
        device.deviceIdentifer = getStringValue(_PARAM_DEVICES_DEVICE_IDENTIFIER, device.deviceIdentifer);
        device.deviceName = getStringValue(_PARAM_DEVICES_DEVICE_NAME, device.deviceName);
        device.manufacturer = getStringValue(_PARAM_DEVICES_DEVICE_MANUFACTURER, device.manufacturer);
        device.model = getStringValue(_PARAM_DEVICES_DEVICE_MODEL, device.model);
        device.platform = getStringValue(_PARAM_DEVICES_DEVICE_PLATFORM, device.platform);
        device.platformVersion = getStringValue(_PARAM_DEVICES_DEVICE_PLATFORM_VERISON, device.platformVersion);
        device.pushToken = getStringValue(_PARAM_DEVICES_DEVICE_PUSH_TOKEN, device.pushToken);
        device.createAt = getStringValue(_PARAM_DEVICES_DEVICE_CREATE_AT, device.createAt);
        device.updatedAt = getStringValue(_PARAM_DEVICES_DEVICE_UPDATE_AT, device.updatedAt);
        device.user = getStringValue(_PARAM_DEVICES_DEVICE_USER, device.user);
        return device;
    }
}
