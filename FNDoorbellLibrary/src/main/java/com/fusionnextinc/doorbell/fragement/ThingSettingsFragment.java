package com.fusionnextinc.doorbell.fragement;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.media.MediaPlayer;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.RemoteViews;
import android.widget.TextView;

import androidx.core.app.NotificationCompat;

import com.fusionnextinc.doorbell.DefaultAppConfig;
import com.fusionnextinc.doorbell.MainActivity;
import com.fusionnextinc.doorbell.MyApplication;
import com.fusionnextinc.doorbell.R;
import com.fusionnextinc.doorbell.manager.data.AppDataThing;
import com.fusionnextinc.doorbell.manager.smartConnection.SmartConnectionManager;
import com.fusionnextinc.doorbell.manager.thingsManager.ThingsHandler;
import com.fusionnextinc.doorbell.manager.thingsManager.ThingsManager;
import com.fusionnextinc.doorbell.objects.FNError;
import com.fusionnextinc.doorbell.objects.FNEvent;
import com.fusionnextinc.doorbell.objects.FNFirmware;
import com.fusionnextinc.doorbell.objects.FNSchedule;
import com.fusionnextinc.doorbell.objects.FNThing;
import com.fusionnextinc.doorbell.objects.FNUserProfile;
import com.fusionnextinc.doorbell.utils.Utils;
import com.fusionnextinc.doorbell.widget.FNAlert.FNAlertDialog;
import com.fusionnextinc.doorbell.widget.FNDialog;
import com.fusionnextinc.doorbell.widget.FNLayoutUtil;
import com.fusionnextinc.doorbell.widget.FNProgressBarCustom;

import java.util.ArrayList;
import java.util.Objects;

/**
 * Created by Mike Chang on 2017/7/7
 */
public class ThingSettingsFragment extends FNFragment implements View.OnClickListener {
    private static final String TAG = ThingSettingsFragment.class.getSimpleName();

    private FNLayoutUtil layoutUtil;
    private TextView nameStatus;
    private TextView soundStatus;
    private TextView ring_soundStatus;
    private TextView fwStatus;
    private TextView invitationsStatus;
    private FNProgressBarCustom progressBar;
    private ThingsManager thingsManager;
    private static FNThing curFNThing;
    private  MediaPlayer m_mediaPlayer = null;
    public static void openFragment(boolean clearOtherStack, FNThing fnThing) {
        curFNThing = fnThing;
        FNFragmentManager.getInstance().openFragment(new ThingSettingsFragment(), clearOtherStack);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        layoutUtil = new FNLayoutUtil(getActivity(), MyApplication.LAYOUT_WIDTH, MyApplication.LAYOUT_HEIGHT, FNLayoutUtil.BASE_WIDTH);
        thingsManager = ThingsManager.getInstance(getActivity());
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        final ViewGroup nullParent = null;

        getActivity().setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        getActionBar().resetAll();
        getActionBar().setTitle(getString(R.string.fn_tittle_settings_thing_settings).replace("${Thing}", getActivity().getString(R.string.fn_title_thing_name)), Gravity.CENTER, null);
        getActionBar().setCoverMode(false);
        getActionBar().setStartImage(R.drawable.back, new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        getActionBar().show();
        View view = inflater.inflate(R.layout.fragment_thing_settings, nullParent);
        layoutUtil.fitAllView(view);
        nameStatus = (TextView) view.findViewById(R.id.txt_name_status);
        soundStatus = (TextView) view.findViewById(R.id.txt_sound_status);
        ring_soundStatus= (TextView) view.findViewById(R.id.txt_ring_sound_status);
        fwStatus = (TextView) view.findViewById(R.id.txt_firmware_status);
        invitationsStatus = (TextView) view.findViewById(R.id.txt_invitations_status);
        TextView nameType = (TextView) view.findViewById(R.id.txt_name_type);
        nameType.setText(getActivity().getString(R.string.fn_tittle_settings_thing_name).replace("${Thing}", getActivity().getString(R.string.fn_title_thing_name)));
        TextView sharetype = (TextView) view.findViewById(R.id.txt_share_type);
        sharetype.setText(getActivity().getString(R.string.fn_tittle_settings_share).replace("${Thing}", getActivity().getString(R.string.fn_title_thing_name)));
        TextView deleteHint = (TextView) view.findViewById(R.id.txt_delete_hint);
        deleteHint.setText(getActivity().getString(R.string.fn_msg_settings_delete_thing).replace("${Thing}", getActivity().getString(R.string.fn_title_thing_name)));


        LinearLayout itemName = (LinearLayout) view.findViewById(R.id.thing_name_item);
        LinearLayout itemSound = (LinearLayout) view.findViewById(R.id.sound_item);
        LinearLayout itemRingSound = (LinearLayout) view.findViewById(R.id.ring_sound_item);
        LinearLayout itemFWUpdate = (LinearLayout) view.findViewById(R.id.firmware_item);
        LinearLayout itemInvitations = (LinearLayout) view.findViewById(R.id.invitations_item);
        LinearLayout itemShared = (LinearLayout) view.findViewById(R.id.share_item);
        LinearLayout itemChangeSsid = (LinearLayout) view.findViewById(R.id.change_ssid_item);
        Button btnDelete = (Button) view.findViewById(R.id.btn_delete);
        btnDelete.setText(getString(R.string.fn_btn_settings_delete_thing).replace("${Thing}", getActivity().getString(R.string.fn_title_thing_name)));
        progressBar = new FNProgressBarCustom(getActivity(), (ViewGroup) view.findViewById(R.id.fragment_thing_settings));

        if (Integer.valueOf(curFNThing.getProductId()) == ThingsManager.PRODUCT_ID_PLUG) {
            itemSound.setVisibility(View.GONE);
            itemRingSound.setVisibility(View.GONE);
            itemFWUpdate.setVisibility(View.GONE);
        } else {
            itemSound.setVisibility(View.VISIBLE);
            itemRingSound.setVisibility(View.VISIBLE);
            itemFWUpdate.setVisibility(View.VISIBLE);
        }
        invitationsStatus.setVisibility(View.GONE); //PTCom request to hide it.

        itemName.setOnClickListener(this);
        itemSound.setOnClickListener(this);
        itemRingSound.setOnClickListener(this);
        itemFWUpdate.setOnClickListener(this);
        itemInvitations.setOnClickListener(this);
        itemShared.setOnClickListener(this);
        itemChangeSsid.setOnClickListener(this);
        btnDelete.setOnClickListener(this);
        updateThingInfo();
        return view;
    }


    @Override
    public void onClick(View view) {
        if (view.getId() == R.id.btn_delete) {

            new FNAlertDialog(getActivity(), FNAlertDialog.WARNING_TYPE)
                    .setTitleText(getActivity().getString(R.string.fn_dialog_are_you_sure))
                    .setContentText(getActivity().getString(R.string.fn_dialog_delete_doorbell))
                    .setCancelText(getActivity().getString(R.string.fn_dialog_cancel))
                    .setConfirmText(getActivity().getString(R.string.fn_btn_delete))
                    .showCancelButton(true)
                    .setCancelClickListener(new FNAlertDialog.OnSweetClickListener() {
                        @Override
                        public void onClick(FNAlertDialog sDialog) {
                            sDialog.dismiss();
                        }
                    })
                    .setConfirmClickListener(new FNAlertDialog.OnSweetClickListener() {
                        @Override
                        public void onClick(final FNAlertDialog sDialog) {
                            if (Utils.isNetworkConnected(getActivity())) {
                                startWait();
                                sDialog.dismiss();
                                thingsManager.setThingsListener(thingsListener);
                                thingsManager.deleteThing(curFNThing.getThingId());
                            } else {
                                FNDialog.showNoNetworkDialog(getActivity());
                            }
                        }
                    })
                    .show();
        } else if (view.getId() == R.id.change_ssid_item) {
            SmartConnectionFragment.openFragment(requireActivity(), false, SmartConnectionManager.SMART_CONNECTION_CHANGE_SSID);
        } else if (view.getId() == R.id.ring_sound_item) {
            RingtoneListFragment.openFragment(false, curFNThing);
        } else if (view.getId() == R.id.firmware_item) {
            FirmwareVersionFragment.openFragment(false, curFNThing);
        } else if (view.getId() == R.id.thing_name_item) {
            ThingNameFragment.openFragment(false, curFNThing);
        } else if (view.getId() == R.id.share_item) {
            ShareThingFragment.openFragment(false, curFNThing);
        } else if (view.getId() == R.id.invitations_item) {
            InvitationsFragment.openFragment(false, curFNThing);
        } else if (view.getId() == R.id.sound_item) {

            if(m_mediaPlayer == null)
            {
                m_mediaPlayer = new MediaPlayer();

                try {
                    m_mediaPlayer = MediaPlayer.create(this.getContext(), R.raw.dream);

                    m_mediaPlayer.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
                        @Override
                        public void onCompletion(MediaPlayer mediaPlayer) {
                            m_mediaPlayer.release();
                            m_mediaPlayer = null;
                        }});
                    m_mediaPlayer.start();

                }catch (Exception ex) {
                    if(m_mediaPlayer!=null) {
                        m_mediaPlayer.release();// if error occurs, reinitialize the MediaPlayer
                        m_mediaPlayer = null; // ready to be garbage collected.
                    }

                }
            }


        }
    }



    private void startWait() {
        progressBar.show();
    }

    private void stopWait() {
        progressBar.hide();
    }

    @Override
    public void onBackPressed() {
        if (progressBar != null && !progressBar.isShown()) {
            super.onBackPressed(); // Comment this super call to avoid calling finish() or fragmentmanager's backstack pop operation.
        }
    }

    final ThingsHandler.ThingsListener thingsListener = new ThingsHandler.ThingsListener() {
        @Override
        public void onListUserOwnedThings(ThingsManager instance, boolean result, String userId, ArrayList<FNThing> thingArrayList, FNError error) {

        }

        @Override
        public void onListUserInvitedThings(ThingsManager instance, boolean result, String userId, ArrayList<FNThing> thingArrayList, FNError error) {

        }

        @Override
        public void onListUserSharedThings(ThingsManager instance, boolean result, String userId, ArrayList<FNThing> thingArrayList, FNError error) {

        }

        @Override
        public void onListUserAllThings(ThingsManager instance, boolean result, String userId, ArrayList<FNThing> ownedThingArrayList, ArrayList<FNThing> sharedThingArrayList, ArrayList<FNThing> invitedThingArrayList, FNError error) {

        }

        @Override
        public void onListThingEvents(ThingsManager instance, boolean result, String thingId, String eventName, ArrayList<FNEvent> eventList, FNError error) {

        }

        @Override
        public void onListThingEventsByTime(ThingsManager instance, boolean result, String thingId, String eventName, String startTime, String endTime, ArrayList<FNEvent> eventList, FNError error) {

        }

        @Override
        public void onCurBatteryLevel(ThingsManager instance, boolean result, String thingId, String batteryLevel, FNError error) {

        }

        @Override
        public void onCurFirmwareVersion(ThingsManager instance, boolean result, String thingId, String fwVersion, FNError error) {

        }

        @Override
        public void onCurSwitchedStatus(ThingsManager instance, boolean result, String thingId, boolean isSwitch, FNError error) {

        }

        @Override
        public void onNewFirmwareVersion(ThingsManager instance, boolean result, String thingId, FNFirmware fnFirmware) {

        }

        @Override
        public void onStartFwUpdate(ThingsManager instance, boolean result, String thingId, String fwId) {

        }

        @Override
        public void onCurRingTone(ThingsManager instance, boolean result, String thingId, String ringtoneId, FNError error) {

        }

        @Override
        public void onGetScheduleList(ThingsManager instance, boolean result, String thingId, ArrayList<FNSchedule> scheduleList, FNError error) {

        }

        @Override
        public void onListSharedUsers(ThingsManager instance, boolean result, String thingId, ArrayList<FNUserProfile> userList, FNError error) {

        }

        @Override
        public void onCreateClaimCode(ThingsManager instance, boolean result, String claimCode, FNError error) {

        }

        @Override
        public void onCreateThing(ThingsManager instance, boolean result, FNThing fnThing, FNError error) {

        }

        @Override
        public void onUpdateThing(ThingsManager instance, boolean result, FNThing fnThing, FNError error) {

        }

        @Override
        public void onChangeRingtone(ThingsManager instance, boolean result, String thingId, int ringtoneId) {

        }

        @Override
        public void onSetTimer(ThingsManager instance, boolean result, String thingId, String value) {

        }

        @Override
        public void onSetSchedule(ThingsManager instance, boolean result, String thingId, String value) {

        }

        @Override
        public void onShareInvited(ThingsManager instance, boolean result, String thingId, FNError error) {

        }

        @Override
        public void onShareAccepted(ThingsManager instance, boolean result, String thingId, FNError error) {

        }

        @Override
        public void onShareRejected(ThingsManager instance, boolean result, String thingId, FNError error) {

        }

        @Override
        public void onSwitchOnOff(ThingsManager instance, boolean result, String thingId, boolean isTurnOn) {

        }

        @Override
        public void onCloseCallSlot(ThingsManager instance, boolean result, FNError error) {

        }

        @Override
        public void onTakeCallSlot(ThingsManager instance, boolean result, String thingId, String callSlot, FNError error) {

        }

        @Override
        public void onListInvitedUsers(ThingsManager instance, boolean result, String thingId, ArrayList<FNUserProfile> userList, FNError error) {
            if (isAdded()) {
                if (userList != null && userList.size() >= 0) {
                    String size = String.valueOf(userList.size());
                    invitationsStatus.setText(getString(R.string.fn_text_invitations_invited_counts, size));
                } else {
                    invitationsStatus.setText(getString(R.string.fn_text_invitations_invited_counts, "0"));
                }
            }
        }

        @Override
        public void onDeleteThing(ThingsManager instance, final boolean result, final FNError error) {
            if (isAdded()) {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        stopWait();
                        if (result) {
                            FNDialog.showSuccessDialog(getActivity(), getString(R.string.fn_msg_delete_thing_successfully).replace("${Thing}", getActivity().getString(R.string.fn_title_thing_name)));
                            AppDataThing.getInstance(getActivity()).deleteThing(curFNThing.getThingId());
                            ThingsFragment thingsFragment = new ThingsFragment();

                            thingsFragment.setUpdateThing(true, null);
                            if (DefaultAppConfig.APP_SOLY) {
                                HomeFragment.openFragment(true, ThingsFragment.THINGS_FRAGMENT_TYPE_NORMAL, null, HomeFragment.TabType.TAB_TYPE_DEVICE, false);
                            } else {
                                thingsFragment.openFragment(true, ThingsFragment.THINGS_FRAGMENT_TYPE_NORMAL, null);
                            }
                        } else {
                            FNDialog.showErrorDialog(getActivity(), getString(R.string.fn_msg_delete_thing_failed).replace("${Thing}", getActivity().getString(R.string.fn_title_thing_name)), error);
                        }
                    }
                });
            }
        }

        @Override
        public void onGetThing(ThingsManager instance, boolean result, FNThing fnThing, FNError error) {

        }
    };

    private void updateThingInfo() {
        curFNThing = AppDataThing.getInstance(getActivity()).getThing(curFNThing.getThingId());
        if (curFNThing != null) {
            int ringtoneId = 0;
            if (curFNThing.getRingtoneId() != null && curFNThing.getRingtoneId().length() > 0) {
                try {
                    ringtoneId = Integer.parseInt(curFNThing.getRingtoneId());
                } catch (NumberFormatException e) {
                    ringtoneId = 0;
                    e.printStackTrace();
                }
            }
            //thingsManager.getInvitedUserList(curFNThing.getSsid(), HttpData._GET_TYPE_SORT_ORDER_DESC, null, null);
            //long invitedCounts = appDataUser.getInvitedUserCounts(curFNThing.getSsid());

            String size = curFNThing.getInviteesCounts();//String.valueOf(invitedCounts);
            invitationsStatus.setText(getString(R.string.fn_text_invitations_invited_counts, size));

            String ringtoneName = thingsManager.getRingtoneNameById(ringtoneId);
            nameStatus.setText(curFNThing.getName());
            fwStatus.setText(curFNThing.getFirmwareVer());
            soundStatus.setText(ringtoneName);
        }
    }
}
