package com.fusionnext.cloud.iot;

import com.fusionnext.cloud.iot.objects.Device;
import com.fusionnext.cloud.iot.objects.ErrorObj;
import com.fusionnext.cloud.iot.objects.Event;
import com.fusionnext.cloud.iot.objects.FileObj;
import com.fusionnext.cloud.iot.objects.Firmware;
import com.fusionnext.cloud.iot.objects.Setting;
import com.fusionnext.cloud.iot.objects.Software;
import com.fusionnext.cloud.iot.objects.State;
import com.fusionnext.cloud.iot.objects.Thing;
import com.fusionnext.cloud.iot.objects.UserProfile;
import com.fusionnext.cloud.iot.objects.WifiAP;

import java.util.ArrayList;

/**
 * Created by Mike Chang on 2017/3/7
 */
public abstract class IOTHandler {

    public interface OnAuthorizationListener {
        //void onGetClientToken(boolean result, Authorization.ClientToken clientToken);
        void onActivateAPI(boolean result, ErrorObj error);

        void onLogin(boolean result, ErrorObj error);

        void onLogout(boolean result, ErrorObj error);
    }

    public interface OnDevicesListener {
        void onResult(boolean result, String id, int postType, ErrorObj error);

        void onCreateDevice(boolean result, Device device, ErrorObj error);

        void onUpdateDevice(boolean result, Device device, ErrorObj error);

        void onDeleteDevice(boolean result, ErrorObj error);
    }

    public interface OnThingsListener {
        void onCreateClaimCode(boolean result, String code, ErrorObj error);

        void onCreateThing(boolean result, Thing thing, ErrorObj error);

        void onUpdateThing(boolean result, Thing thing, ErrorObj error);

        void onDeleteThing(boolean result, ErrorObj error);

        void onGetThing(boolean result, Thing thing, ErrorObj error);

        void onListStates(boolean result, String id, ArrayList<State> stateList, ErrorObj error);

        void onListSettings(boolean result, String settingName, String id, ArrayList<Setting> settingList, ErrorObj error);

        void onListEvents(boolean result, String thingId, String eventName, ArrayList<Event> eventList, ErrorObj error);

        void onListEventsByTime(boolean result, String thingId, String eventName, String startTime, String endTime, ArrayList<Event> eventList, int eventCount, ErrorObj error);

        void onListInvitedUsers(boolean result, String id, ArrayList<UserProfile> userList, ErrorObj error);

        void onListSharedUsers(boolean result, String id, ArrayList<UserProfile> userList, ErrorObj error);

        void onInvite(boolean result, String thingId, ErrorObj error);

        void onAccept(boolean result, String thingId, ErrorObj error);

        void onReject(boolean result, String thingId, ErrorObj error);

        void onCloseCallSlot(boolean result, ErrorObj error);

        void onTakeCallSlot(boolean result, String thingId, String callSlot, ErrorObj error);
    }


    public interface OnPushTokensListener {
        void onResult(boolean result, String id, int postType, ErrorObj error);
    }

    public interface OnUsersListener {
        void onResult(boolean result, String id, int postType, ErrorObj error);

        void onCreateUser(boolean result, ErrorObj error);
        void onRsendUser(boolean result, ErrorObj error);

        void onGetUser(boolean result, ErrorObj error);

        void onUpdateUser(boolean result, ErrorObj error);

        void onListAllThings(boolean result, String userId, ArrayList<Thing> ownedThingArrayList, ArrayList<Thing> sharedThingArrayList, ArrayList<Thing> invitedThingArrayList, ErrorObj error);

        void onListOwnedThings(boolean result, String userId, ArrayList<Thing> thingArrayList, ErrorObj error);

        void onListInvitedThings(boolean result, String userId, ArrayList<Thing> thingArrayList, ErrorObj error);

        void onListSharedThings(boolean result, String userId, ArrayList<Thing> thingArrayList, ErrorObj error);

        void onResetPassword(boolean result, ErrorObj error);

        void onSetPassword(boolean result, ErrorObj error);
    }

    public interface OnFilesListener {
        void onUploadFile(boolean result, String id, FileObj fileObj, ErrorObj error);
    }

    public interface OnSmartConnectionListener {
        void onThingDiscovered(boolean result, String value);
    }

    public interface OnSettingsListener {
        void onCreateSetting(boolean result, String settingType, Setting setting, ErrorObj error);
    }

    public interface OnOtaHandlerListener {
        void onNewFwInfo(boolean result, String thingId, Firmware firmware);

        void onNewSwInfo(boolean result, Software software);

        void onSrartDownload();

        void onStopDownload();

        void onDownloadProgress(long progress);

        void onDownloadComplete();

        void onError();

        void onStartFwUpdate(boolean result, String thingId, String fwId);
    }

    public interface OnAPModeListener {
        void onListWifiAP(boolean result, ArrayList<WifiAP> wifiAPList, ErrorObj error);

        void onRegisterDevice(boolean result, String macAddr, ErrorObj error);

        void onChangeSsid(boolean result, String macAddr, ErrorObj error);
    }

    public interface OnSmartPlugListener {
        void onSwitchOnOff(boolean result, String thingId, boolean isTurnOn);
    }
}
