package com.fusionnext.cloud.iot;

import android.content.Context;
import android.util.Log;

import com.fusionnext.cloud.datalink.http.HttpGet;
import com.fusionnext.cloud.datalink.http.HttpParam;
import com.fusionnext.cloud.datalink.http.HttpPost;
import com.fusionnext.cloud.datalink.http.HttpUtil;
import com.fusionnext.cloud.datalink.json.FNJsonArray;
import com.fusionnext.cloud.datalink.json.FNJsonObject;
import com.fusionnext.cloud.iot.objects.Firmware;
import com.fusionnext.cloud.iot.objects.Software;
import com.fusionnext.cloud.iot.objects.Thing;

import org.json.JSONException;

import java.util.HashMap;

public class Ota {
    protected final static String _TAG = "Ota";
    private Context mContext;
    private IOTHandler.OnOtaHandlerListener mOtaHandlerListener;

    public Ota(Context context, IOTHandler.OnOtaHandlerListener otaHandlerListener) {
        mContext = context;
        mOtaHandlerListener = otaHandlerListener;
    }

    public void getUpdatedFw(final Thing thing) {
        HttpParam.HttpSubParam param = new HttpParam.HttpSubParam();
        param.accessToken = Public.getAuthorization(mContext).getUserAccessToken();
        param.url = HttpData.generateHttpGetUrl(HttpData._GET_TYPE_FW_SEARCH_UPDATE, thing._productId, null, HttpData._GET_TYPE_SORT_ORDER_DESC, "1", "1");
        param.cacheName = "";
        HttpParam[] params = new HttpParam[1];
        params[0] = new HttpParam();
        params[0].params.clear();
        params[0].params.add(param);
        HttpGet ha = new HttpGet();
        ha.setContext(mContext);
        ha.setTag(HttpGet._HTTP_T_GET_NORMAL_TYPE);
        ha.setOnParseJsonListener(new HttpGet.OnParseJsonListener() {
            public void onParse(boolean result, String json) {
                Firmware firmware = parseFirmwareArray(json);
                if (mOtaHandlerListener != null) {
                    mOtaHandlerListener.onNewFwInfo(result, thing._thingId, firmware);
                }
            }
        });
        ha.execute(params);
    }

    public void getUpdatedFwById(String fwId) {
        HttpParam.HttpSubParam param = new HttpParam.HttpSubParam();
        param.accessToken = Public.getAuthorization(mContext).getUserAccessToken();
        param.url = HttpData.generateHttpGetUrl(HttpData._GET_TYPE_FW_GET_UPDATE, fwId, null, HttpData._GET_TYPE_SORT_ORDER_DESC, null, null);
        param.cacheName = "";
        HttpParam[] params = new HttpParam[1];
        params[0] = new HttpParam();
        params[0].params.clear();
        params[0].params.add(param);
        HttpGet ha = new HttpGet();
        ha.setContext(mContext);
        ha.setTag(HttpGet._HTTP_T_GET_NORMAL_TYPE);
        ha.setOnParseJsonListener(new HttpGet.OnParseJsonListener() {
            public void onParse(boolean result, String json) {
                Firmware firmware = parseFirmware(json);
                if (mOtaHandlerListener != null) {
                    mOtaHandlerListener.onNewFwInfo(result, null, firmware);
                }
            }
        });
        ha.execute(params);
    }

    private Firmware parseFirmwareArray(String json) {
        Firmware firmware = null;
        if (json != null && !json.equals("")) {
            try {
                FNJsonObject jsonObject = new FNJsonObject(json);
                FNJsonArray jsonArray = jsonObject.getJSONArray("items");
                if (jsonArray != null && jsonArray.length() > 0) {
                    FNJsonObject jsonObj = jsonArray.getJSONObject(0);
                    firmware = new Firmware();
                    firmware.getDataFromJSONObject(jsonObj);
                }

                return firmware;
            } catch (JSONException e) {
                e.printStackTrace();
                return null;
            }
        } else {
            return null;
        }
    }

    private Firmware parseFirmware(String json) {
        Firmware firmware;
        if (json != null && !json.equals("")) {
            try {
                FNJsonObject jsonObject = new FNJsonObject(json);
                firmware = new Firmware();
                firmware.getDataFromJSONObject(jsonObject);
                return firmware;
            } catch (JSONException e) {
                e.printStackTrace();
                return null;
            }
        } else {
            return null;
        }
    }

    public void getLastestApp() {
        HttpParam.HttpSubParam param = new HttpParam.HttpSubParam();

        param.accessToken = Public.getAuthorization(mContext).getClientAccessToken();
        param.url = HttpData.generateHttpGetUrl(HttpData._GET_TYPE_SW_LIST_UPDATES, Public.getAuthorization(mContext).getClientId(), null, HttpData._GET_TYPE_SORT_ORDER_DESC, null, null);
        param.cacheName = "";
        HttpParam[] params = new HttpParam[1];
        params[0] = new HttpParam();
        params[0].params.clear();
        params[0].params.add(param);
        HttpGet ha = new HttpGet();
        ha.setContext(mContext);
        ha.setTag(HttpGet._HTTP_T_GET_NORMAL_TYPE);
        ha.setOnParseJsonListener(new HttpGet.OnParseJsonListener() {
            public void onParse(boolean result, String json) {
                Software software = Ota.this.parseSoftwareArray(json);
                if (mOtaHandlerListener != null) {
                    mOtaHandlerListener.onNewSwInfo(result, software);
                }
            }
        });
        ha.execute(params);
    }

    private Software parseSoftwareArray(String json) {
        Software software = null;
        if (json != null && !json.equals("")) {
            try {
                FNJsonObject jsonObject = new FNJsonObject(json);
                FNJsonArray jsonArray = jsonObject.getJSONArray("items");
                if (jsonArray != null && jsonArray.length() > 0) {
                    FNJsonObject jsonObj = jsonArray.getJSONObject(0);
                    software = new Software();
                    software.getDataFromJSONObject(jsonObj);
                }

                return software;
            } catch (JSONException e) {
                e.printStackTrace();
                return null;
            }
        } else {
            return null;
        }
    }

    private Software parseSoftware(String json) {
        Software software;
        if (json != null && !json.equals("")) {
            try {
                FNJsonObject jsonObject = new FNJsonObject(json);
                software = new Software();
                software.getDataFromJSONObject(jsonObject);
                return software;
            } catch (JSONException e) {
                e.printStackTrace();
                return null;
            }
        } else {
            return null;
        }
    }

    public void downloadFile(String urlStr, String filename) {
        HttpParam.HttpSubParam param = new HttpParam.HttpSubParam();
        param.url = urlStr;
        param.cacheName = filename;
        HttpParam[] params = new HttpParam[1];
        params[0] = new HttpParam();
        params[0].params.clear();
        params[0].params.add(param);
        HttpGet ha = new HttpGet();
        ha.setContext(mContext);
        ha.setTag(HttpGet._HTTP_T_GET_DOWNLOAD_FILE);
        ha.setOnDownloadListener(new HttpUtil.OnDownloadListener() {
            @Override
            public void onStart() {
                if (mOtaHandlerListener != null) {
                    mOtaHandlerListener.onSrartDownload();
                }
            }

            @Override
            public void onStop() {
                if (mOtaHandlerListener != null) {
                    mOtaHandlerListener.onStopDownload();
                }
            }

            @Override
            public void onProgress(long progress) {
                Log.d(_TAG, "downloadFile: onProgress = " + progress);
                if (mOtaHandlerListener != null) {
                    mOtaHandlerListener.onDownloadProgress(progress);
                }
            }

            @Override
            public void onComplete() {
                if (mOtaHandlerListener != null) {
                    mOtaHandlerListener.onDownloadComplete();
                }
            }

            @Override
            public void onError() {
                if (mOtaHandlerListener != null) {
                    mOtaHandlerListener.onError();
                }
            }
        });
        ha.execute(params);
    }

    public void stopDownloadFile() {
        HttpUtil httpUtil = new HttpUtil(mContext);
    }

    public void startFwUpdate(final String thingId, final String fwId) {
        if (thingId == null || fwId == null) {
            return;
        }
        FNJsonObject payloadJsonObj = new FNJsonObject();
        try {
            payloadJsonObj.put("fw_update_id", fwId);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        HttpPost.PostParam[] params = new HttpPost.PostParam[1];
        params[0] = new HttpPost.PostParam();
        params[0].accessToken = Public.getAuthorization(mContext).getUserAccessToken();
        params[0].url = HttpData.generateHttpPostUrl(HttpData._POST_TYPE_FW_START_UPDATE);
        params[0].type = HttpData._POST_TYPE_FW_START_UPDATE;
        params[0].param = new HashMap<>();
        params[0].param.put("thing_id", thingId);
        params[0].param.put("name", "update_firmware");
        params[0].param.put("payload", payloadJsonObj.toString());

        HttpPost post = new HttpPost();
        post.setContext(mContext);
        post.setOnParseJsonListener(new HttpPost.OnParseJsonListener() {
            @Override
            public void onParse(boolean returnResult, int postType, String id, String returnStr) {
                mOtaHandlerListener.onStartFwUpdate(returnResult, thingId, fwId);
            }
        });
        post.execute(params);
    }
}
