package com.fusionnext.cloud.datalink.http;

import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;

import com.fusionnext.cloud.utils.FNNetworkLog;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Locale;
import java.util.Map;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLSession;

import static com.fusionnext.cloud.datalink.http.HttpUtil._HTTP_CONNECT_TIMEOUT;
import static com.fusionnext.cloud.datalink.http.HttpUtil._HTTP_READ_TIMEOUT;

/**
 * Created by Mike Chang on 2016/2/22.
 */

public class HttpPut extends AsyncTask<HttpPut.PutParam, Void, HttpPut.PutResult> {
    protected final static String _TAG = "HttpPut";

    private final static String SECTION_HTTP_POST_RESULT = "SECTION_HTTP_POST_RESULT";
    private final static String SECTION_HTTP_POST_DATA = "SECTION_HTTP_POST_DATA";
    private final static String SECTION_HTTP_POST_TYPE = "SECTION_HTTP_POST_TYPE";

    private final static int _PUT_TYPE_UNKNOWN = 0x80;

    private final static String CONTENT_TYPE_URL_ENCODED = "application/x-www-form-urlencoded";
    private final static String PARAM_LINK = "&";
    private final static String LINE_END = "\r\n";

    private Context context;
    //Default is post, can use "POST", "DELETE", or others
    private String HTTP_REQUEST_TYPE = "PUT";
    private OnParseJsonListener onParseJsonListener = null;

    public static class PutParam {
        public int type;
        public Map<Object, Object> param;
        public String accessToken = null;
        public URL url = null;
    }

    public static class PutResult {
        public String returnStr = "";
        public boolean returnResult;
        public int putType;

        private PutResult(int putType, boolean result, String retStr) {
            this.putType = putType;
            this.returnResult = result;
            this.returnStr = retStr;
        }
    }

    public void setHttpRequestType(String requestType) {
        this.HTTP_REQUEST_TYPE = requestType;
    }

    @Override
    protected PutResult doInBackground(PutParam... putParams) {
        URL url;
        PutResult result = new PutResult(_PUT_TYPE_UNKNOWN, false, "");
        if (putParams.length <= 0) return result;

        Map<Object, Object> param = putParams[0].param;
        result.putType = putParams[0].type;
        url = putParams[0].url;

        HttpURLConnection conn = null;
        DataOutputStream output = null;
        BufferedReader input = null;

        boolean error = false;
        try {
            FNNetworkLog.d(_TAG, url.toString());

            conn = (HttpURLConnection) url.openConnection();
            conn.setConnectTimeout(_HTTP_CONNECT_TIMEOUT);
            conn.setReadTimeout(_HTTP_READ_TIMEOUT);
            conn.setDoInput(true);        // ?�许输入
            boolean doOutput = HTTP_REQUEST_TYPE.equals("PUT");
            conn.setDoOutput(doOutput);        // ?�否?�许输出
            conn.setUseCaches(false);    // 不使?�Cache
            conn.setRequestMethod(HTTP_REQUEST_TYPE);
            conn.setRequestProperty("Connection", "keep-alive");
            Locale locale = context.getResources().getConfiguration().locale;
            String localeStr = locale.getLanguage().toLowerCase() + "-" + locale.getCountry().toLowerCase();
            if (localeStr.equals("")) localeStr = "en";
            conn.setRequestProperty("Accept-Language", localeStr);

            if (putParams[0].accessToken != null) {
                conn.setRequestProperty("Authorization",
                        putParams[0].accessToken);
            }
            if ("https".equalsIgnoreCase(url.getProtocol())){
                ((HttpsURLConnection) conn).setSSLSocketFactory(HttpPost.SSLSocketClient.getSSLSocketFactory());
                ((HttpsURLConnection) conn).setHostnameVerifier(HttpPost.SSLSocketClient.getHostnameVerifier());
            }


            conn.setRequestProperty("Content-Type", CONTENT_TYPE_URL_ENCODED);
            conn.setRequestProperty("Accept", "*/*");
            conn.connect();
            if (HTTP_REQUEST_TYPE.equals("PUT")) {
                output = new DataOutputStream(conn.getOutputStream());

                Map<Object, Object> newParam = new HashMap<>();
                if (param != null) {
                    Iterator iteratorKeys = param.keySet().iterator();
                    Iterator iteratorValues = param.values().iterator();
                    while (iteratorKeys.hasNext() && iteratorValues.hasNext()) {
                        Object key = iteratorKeys.next();
                        Object value = iteratorValues.next();
                        if (key != null && value != null) {
                            newParam.put(key, value);
                        }
                    }
                }
                addUrlEncodedText(newParam, output);    //Add Url encoded text
                output.flush();
            }

            int code = conn.getResponseCode();
            result.returnResult = (code == 200 || code == 201);
            if (result.returnResult) {
                input = new BufferedReader(new InputStreamReader(conn.getInputStream()));
            } else {
                input = new BufferedReader(new InputStreamReader(conn.getErrorStream()));
            }
            StringBuilder response = new StringBuilder();
            String oneLine;
            while ((oneLine = input.readLine()) != null) {
                response.append(oneLine).append(LINE_END);
            }
            String responseStr = response.toString();
            JSONObject rltObject = new JSONObject(responseStr);
            String message = responseStr;

            if (rltObject.has("error") && !rltObject.isNull("error")) {
                error = true;
                message = rltObject.toString();
            }
            /*if (error && errorObject.has("message") && !errorObject.isNull("message")) {
                message = errorObject.getString("message");
            }*/
            result.returnResult = result.returnResult && (!error);
            result.returnStr = message;
            FNNetworkLog.d(_TAG, "code = " + code + ",  result =  " + result.returnStr);
        } catch (IOException e) {
            e.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        } finally {
            result.returnResult = result.returnResult && (!error);

            try {
                if (output != null) {
                    output.close();
                }
                if (input != null) {
                    input.close();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }

            if (conn != null) {
                conn.disconnect();
            }
        }

        return result;
    }

    @Override
    protected void onPostExecute(PutResult putResult) {
        if (this.onParseJsonListener != null) {
            if (putResult.returnStr == null || putResult.returnStr.trim().equals("")) {
                return;
            }

            this.onParseJsonListener.onParse(putResult.returnResult, putResult.putType, putResult.returnStr);
        } else {
            if (putResult != null && context != null) {
                Bundle bundle = new Bundle();
                bundle.putBoolean(SECTION_HTTP_POST_RESULT, putResult.returnResult);
                bundle.putString(SECTION_HTTP_POST_DATA, putResult.returnStr);
                bundle.putInt(SECTION_HTTP_POST_TYPE, putResult.putType);

                HttpUtil.broadcastWithExtra(context, HttpUtil.BC_HTTP_POST_FEEDBACK, bundle);
            }
        }

        super.onPostExecute(putResult);
    }

    public void setContext(Context context) {
        this.context = context;
    }

    private void addUrlEncodedText(Map<Object, Object> params, DataOutputStream output) {
        StringBuilder sb = new StringBuilder();
        Iterator iteratorKeys = params.keySet().iterator();
        Iterator iteratorValues = params.values().iterator();
        try {
            while (iteratorKeys.hasNext() && iteratorValues.hasNext()) {
                Object key = iteratorKeys.next();
                Object value = iteratorValues.next();
                if (!sb.toString().equals("")) sb.append(PARAM_LINK);
                String encodeKey = URLEncoder.encode(key.toString(), "utf-8");
                String encodeValue = URLEncoder.encode(value.toString(), "utf-8");
                sb.append(encodeKey).append("=").append(encodeValue);
            }
            String sbStr = sb.toString();

            try {
                output.writeBytes(sbStr);
            } catch (IOException e) {
                e.printStackTrace();
            }

        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
    }

    public void setOnParseJsonListener(HttpPut.OnParseJsonListener onParseJsonListener) {
        this.onParseJsonListener = onParseJsonListener;
    }

    public interface OnParseJsonListener {
        void onParse(boolean returnResult, int putType, String returnStr);
    }
}
