package com.fusionnext.cloud.iot;

import android.content.Context;

import com.fusionnext.cloud.datalink.http.HttpPost;
import com.fusionnext.cloud.datalink.json.FNJsonObject;
import com.fusionnext.cloud.iot.objects.ErrorObj;
import com.fusionnext.cloud.iot.objects.FileObj;

import org.json.JSONException;

import java.io.File;
import java.util.HashMap;

/**
 * Created by Mike Chang on 2016/4/14
 */
public class Files {
    protected final static String _TAG = "Files";
    private Context context;

    public Files(Context context) {
        this.context = context;
    }

    public void uploadFile(File file, final IOTHandler.OnFilesListener onFilesListener) {
        if (file != null && file.exists()) {
            HttpPost.PostParam[] params = new HttpPost.PostParam[1];
            params[0] = new HttpPost.PostParam();
            params[0].accessToken = Public.getAuthorization(context).getUserAccessToken();
            params[0].url = HttpData.generateHttpPostUrl(HttpData._POST_TYPE_UPLOAD_FILE);
            params[0].type = HttpData._POST_TYPE_UPLOAD_FILE;
            params[0].uploadFile = true;
            params[0].param = new HashMap<>();
            params[0].param.put("file1", file.getAbsolutePath());

            HttpPost post = new HttpPost();
            post.setContext(context);
            post.setOnParseJsonListener(new HttpPost.OnParseJsonListener() {
                @Override
                public void onParse(boolean returnResult, int postType, String id, String returnStr) {
                    try {
                        FileObj fileObj = null;
                        ErrorObj errorObj = null;

                        if (returnStr != null) {
                            if (returnResult) {
                                FNJsonObject fileObject = new FNJsonObject(returnStr);
                                String fileString = fileObject.getString("file1");
                                fileObj = FileObj.parseFile(fileString);
                            } else {
                                errorObj = ErrorObj.parseErrorObj(returnStr);
                            }
                        }
                        onFilesListener.onUploadFile(returnResult, id, fileObj, errorObj);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            });
            post.execute(params);
        }
    }
}
