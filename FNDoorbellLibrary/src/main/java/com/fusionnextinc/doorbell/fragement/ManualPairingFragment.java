package com.fusionnextinc.doorbell.fragement;

import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.text.method.HideReturnsTransformationMethod;
import android.text.method.PasswordTransformationMethod;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;

import com.fusionnextinc.doorbell.Adapter.WifiAPListAdapter;
import com.fusionnextinc.doorbell.DefaultAppConfig;
import com.fusionnextinc.doorbell.MyApplication;
import com.fusionnextinc.doorbell.R;
import com.fusionnextinc.doorbell.manager.apMode.ApModeHandler;
import com.fusionnextinc.doorbell.manager.apMode.ApModeManager;
import com.fusionnextinc.doorbell.manager.smartConnection.SmartConnectionManager;
import com.fusionnextinc.doorbell.objects.FNError;
import com.fusionnextinc.doorbell.objects.FNWifiAp;
import com.fusionnextinc.doorbell.utils.Utils;
import com.fusionnextinc.doorbell.widget.FNAlert.FNAlertDialog;
import com.fusionnextinc.doorbell.widget.FNCustomDialog;
import com.fusionnextinc.doorbell.widget.FNDialog;
import com.fusionnextinc.doorbell.widget.FNLayoutUtil;
import com.fusionnextinc.doorbell.widget.FNProgressBarCustom;

import java.util.ArrayList;

/**
 * Created by Mike Chang on 2017/7/17
 */
public class ManualPairingFragment extends FNFragment implements View.OnClickListener, AdapterView.OnItemClickListener {
    private static final String TAG = ManualPairingFragment.class.getSimpleName();

    private FNLayoutUtil mLayoutUtil;
    private Button mBtnWifiConnect, mBtnCancel;
    private FNProgressBarCustom mProgressBar;
    private WifiAPListAdapter mWifiApListAdapter = null;
    private ListView mWifiAPListView;
    private View mWifiAP;
    //P2P Password
    private EditText mEditWifiPassword;
    private String mClaimCode;
    private ApModeManager mApModeManager;
    private FNWifiAp mSelectedFnWifiAp = null;
    private static int mSmartconnType = SmartConnectionManager.SMART_CONNECTION_CREATE_THING;

    public static void openFragment(boolean clearOtherStack, String claimCode, int type) {
        ManualPairingFragment manualPairingFragment = new ManualPairingFragment();
        manualPairingFragment.mClaimCode = claimCode;
        manualPairingFragment.mSmartconnType = type;
        FNFragmentManager.getInstance().openFragment(manualPairingFragment, clearOtherStack);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mLayoutUtil = new FNLayoutUtil(getActivity(), MyApplication.LAYOUT_WIDTH, MyApplication.LAYOUT_HEIGHT, FNLayoutUtil.BASE_WIDTH);
        mApModeManager = ApModeManager.getInstance(getActivity());
        mApModeManager.setApModeListener(onAPModeListener);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        final ViewGroup nullParent = null;

        getActivity().setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        getActionBar().resetAll();
        getActionBar().setTitle(getString(R.string.fn_title_manual_pairing), Gravity.CENTER, null);
        getActionBar().setCoverMode(false);
        getActionBar().setStartImage(R.drawable.back, new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        getActionBar().show();
        View view = inflater.inflate(R.layout.fragment_manual_pairing, nullParent);
        mLayoutUtil.fitAllView(view);

        TextView pairingStep1 = (TextView) view.findViewById(R.id.text_pairing_step1);
        TextView pairingStep2 = (TextView) view.findViewById(R.id.text_pairing_step2);
        TextView pairingStep3 = (TextView) view.findViewById(R.id.text_pairing_step3);
        TextView pairingStep4 = (TextView) view.findViewById(R.id.text_pairing_step4);
        mWifiAP = (View) view.findViewById(R.id.wifi_ap);
        mBtnWifiConnect = (Button) view.findViewById(R.id.btn_manual_priring);
        mBtnCancel = (Button) view.findViewById(R.id.btn_wifi_ap_cancecl);
        mProgressBar = new FNProgressBarCustom(getActivity(), (ViewGroup) view.findViewById(R.id.fragment_manual_pairing));
        mWifiAPListView = (ListView) view.findViewById(R.id.listview_wifi_ap);

        mBtnWifiConnect.setOnClickListener(this);
        mBtnCancel.setOnClickListener(this);
        mWifiAPListView.setOnItemClickListener(this);


        pairingStep1.setText(getString(R.string.fn_text_manual_pairing_step1));
        pairingStep2.setText(getString(R.string.fn_text_manual_pairing_step2));
        pairingStep3.setText(getString(R.string.fn_text_manual_pairing_step3).replace("${Thing}", getActivity().getString(R.string.fn_title_thing_name)));
        pairingStep4.setText(getString(R.string.fn_text_manual_pairing_step4).replace("${Thing}", getActivity().getString(R.string.fn_title_thing_name)));
        return view;
    }

    @Override
    public void onClick(View view) {
        if (Utils.isNetworkConnected(getActivity())) {
            if (view.getId() == R.id.btn_manual_priring) {
                mApModeManager.startScanWifiAp();
                mWifiAP.setVisibility(View.VISIBLE);
            } else if (view.getId() == R.id.btn_wifi_ap_cancecl) {
                mApModeManager.stopScanWifiAp();
                mWifiAP.setVisibility(View.GONE);
            }
        } else {
            FNDialog.showNoNetworkDialog(getActivity());
        }
    }

    private void startWait() {
        mProgressBar.show();
    }


    private void stopWait() {
        mProgressBar.hide();
        mBtnWifiConnect.setText(getString(R.string.fn_btn_smartconn_connect));
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        mApModeManager.stopScanWifiAp();
    }

    @Override
    public void onItemClick(AdapterView<?> adapterView, View view, final int i, long l) {
        mApModeManager.stopScanWifiAp();
        new FNAlertDialog(getActivity(), FNAlertDialog.WARNING_TYPE)
                .setTitleText(getActivity().getString(R.string.fn_title_manual_pairing))
                .setContentText(getActivity().getString(R.string.fn_msg_manual_pairing_wifi_select).replace("${Thing}", getActivity().getString(R.string.fn_title_thing_name)))
                .setConfirmText(getActivity().getString(R.string.fn_dialog_ok))
                .setCancelText(getActivity().getString(R.string.fn_dialog_cancel))
                .showCancelButton(true)
                .setConfirmClickListener(new FNAlertDialog.OnSweetClickListener() {
                    @Override
                    public void onClick(FNAlertDialog sDialog) {
                        sDialog.dismiss();
                        ArrayList<FNWifiAp> fnWifiApList = mWifiApListAdapter.getWifiApList();

                        if (fnWifiApList != null && i >= 0) {
                            mSelectedFnWifiAp = fnWifiApList.get(i);
                            showWifiPasswordDialog();
                        }
                    }
                })
                .setCancelClickListener(new FNAlertDialog.OnSweetClickListener() {
                    @Override
                    public void onClick(FNAlertDialog sDialog) {
                        sDialog.dismiss();
                        mApModeManager.startScanWifiAp();
                    }
                })
                .show();
    }

    private boolean initWifiApList(ArrayList<FNWifiAp> wifiApList) {
        if (mWifiAPListView == null || wifiApList == null) return false;
        if (mWifiApListAdapter == null) {
            //Setup the adapter
            mWifiApListAdapter = new WifiAPListAdapter(getActivity(), wifiApList);
        } else {
            mWifiApListAdapter.updateWifiApList(wifiApList);
        }
        mWifiAPListView.setAdapter(mWifiApListAdapter);
        return true;
    }

    private ApModeHandler.ApModeListener onAPModeListener = new ApModeHandler.ApModeListener() {
        @Override
        public void onListWifiAP(ApModeManager instance, boolean result, ArrayList<FNWifiAp> wifiApList, FNError error) {
            initWifiApList(wifiApList);
        }

        @Override
        public void onRegisterDevice(ApModeManager instance, boolean result, String macAddress, FNError error) {
            mWifiAP.setVisibility(View.GONE);
            new FNAlertDialog(getActivity(), FNAlertDialog.WARNING_TYPE)
                    .setTitleText(getActivity().getString(R.string.fn_title_manual_pairing))
                    .setContentText(getActivity().getString(R.string.fn_msg_manual_pairing_successfully).replace("${Thing}", getActivity().getString(R.string.fn_title_thing_name)))
                    .setConfirmText(getActivity().getString(R.string.fn_dialog_ok))
                    .showCancelButton(false)
                    .setConfirmClickListener(new FNAlertDialog.OnSweetClickListener() {
                        @Override
                        public void onClick(FNAlertDialog sDialog) {
                            sDialog.dismiss();
                            ThingsFragment thingsFragment = new ThingsFragment();

                            thingsFragment.setUpdateThing(true, null);
                            if (DefaultAppConfig.APP_SOLY) {
                                HomeFragment.openFragment(true, ThingsFragment.THINGS_FRAGMENT_TYPE_NORMAL, null, HomeFragment.TabType.TAB_TYPE_DEVICE, false);
                            } else {
                                thingsFragment.openFragment(true, ThingsFragment.THINGS_FRAGMENT_TYPE_NORMAL, null);
                            }
                        }
                    })
                    .show();

        }

        @Override
        public void onChangeSsid(ApModeManager instance, boolean result, String macAddress, FNError error) {
            mWifiAP.setVisibility(View.GONE);
            new FNAlertDialog(getActivity(), FNAlertDialog.WARNING_TYPE)
                    .setTitleText(getActivity().getString(R.string.fn_tittle_settings_change_ssid))
                    .setContentText(getActivity().getString(R.string.fn_msg_change_ssid_successfully).replace("${Thing}", getActivity().getString(R.string.fn_title_thing_name)))
                    .setConfirmText(getActivity().getString(R.string.fn_dialog_ok))
                    .showCancelButton(false)
                    .setConfirmClickListener(new FNAlertDialog.OnSweetClickListener() {
                        @Override
                        public void onClick(FNAlertDialog sDialog) {
                            sDialog.dismiss();
                        }
                    })
                    .show();
            onBackPressed();
        }
    };

    private void showWifiPasswordDialog() {
        final FNCustomDialog dialog = new FNCustomDialog(getActivity());

        View view = LayoutInflater.from(getActivity()).inflate(R.layout.dialog_wifi_password, null);
        mEditWifiPassword = (EditText) view.findViewById(R.id.ed_p2p_password);
        final TextView txtP2PPasswordHint = (TextView) view.findViewById(R.id.txt_p2p_password_hint);
        CheckBox chkShowP2PPassword = (CheckBox) view.findViewById(R.id.chk_show_p2p_password);

        if (chkShowP2PPassword.isChecked()) {
            mEditWifiPassword.setTransformationMethod(HideReturnsTransformationMethod.getInstance());
        } else {
            mEditWifiPassword.setTransformationMethod(PasswordTransformationMethod.getInstance());
        }

        boolean isP2PPasswordCheck = true;

        dialog.getButton(FNCustomDialog.BUTTON_NEGATIVE).setEnabled(isP2PPasswordCheck);

        chkShowP2PPassword.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    mEditWifiPassword.setTransformationMethod(HideReturnsTransformationMethod.getInstance());
                } else {
                    mEditWifiPassword.setTransformationMethod(PasswordTransformationMethod.getInstance());
                }
            }
        });

        mEditWifiPassword.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                boolean isShowMessage = false;
                dialog.getButton(FNCustomDialog.BUTTON_NEGATIVE).setEnabled(true);
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
        dialog.setView(view);
        dialog.show();
        dialog.setNegativeButton(getString(R.string.fn_dialog_cancel), new FNCustomDialog.OnClickListener() {
            @Override
            public void onClick(final FNCustomDialog dialog, int which) {

            }
        }, true);


        dialog.setPositiveButton(getString(R.string.fn_dialog_ok), new FNCustomDialog.OnClickListener() {
            @Override
            public void onClick(FNCustomDialog dialog, int which) {
                ArrayList<FNWifiAp> fnWifiApList = mWifiApListAdapter.getWifiApList();
                if (fnWifiApList != null && mSelectedFnWifiAp != null && mClaimCode != null) {
                    if (mSmartconnType == SmartConnectionManager.SMART_CONNECTION_CREATE_THING) {
                        mApModeManager.registerDevice(mSelectedFnWifiAp.getSsid(), mEditWifiPassword.getText().toString(), mSelectedFnWifiAp.getSec(), mClaimCode);
                    } else {
                        mApModeManager.changeSsid(mSelectedFnWifiAp.getSsid(), mEditWifiPassword.getText().toString(), mSelectedFnWifiAp.getSec());
                    }
                }
            }
        }, true);
    }

}
