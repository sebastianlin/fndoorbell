package com.fusionnext.cloud.social.objects;

import android.util.Log;

import com.fusionnext.cloud.datalink.json.FNJsonArray;
import com.fusionnext.cloud.datalink.json.FNJsonObject;

import org.json.JSONException;

import java.util.ArrayList;

/**
 * Created by Mike Chang on 2016/2/22.
 */

public class Comment {
    protected final static String _TAG = "Comment";
    protected final static String _JSON_TAG = "comment";

    public final static String COMMENTABLE_STRING_TYPE_PHOTO = "photo";
    public final static String COMMENTABLE_STRING_TYPE_TRACK = "track";

    public int _type;
    public String _commentId;
    public String _userId;
    public String _content;
    public int _commentableId;
    public String _commentableType;
    public String _createdAt;
    public String _updatedAt;

    public LikesMeta _likesMeta;
    public UserProfile _userProfile;


    public Comment() {
        _type = 0;
        _commentId = "";
        _userId = "";
        _content = "";
        _commentableId = 0;
        _commentableType = "";
        _createdAt = "";
        _updatedAt = "";
        _likesMeta = new LikesMeta();
        _userProfile = new UserProfile();
    }

    public void clean() {
        _type = 0;
        _commentId = "";
        _userId = "";
        _content = "";
        _commentableId = 0;
        _commentableType = "";
        _createdAt = "";
        _updatedAt = "";

        if (_likesMeta != null) {
            _likesMeta.clean();
        } else {
            _likesMeta = new LikesMeta();
        }
        if (_userProfile != null) {
            _userProfile.clean();
        } else {
            _userProfile = new UserProfile();
        }
    }

    public boolean getDataFromJSONObject(FNJsonObject commentObj) {
        clean();
        if (commentObj == null) {
            Log.e(_TAG, "Null JSON Object!");
            return false;
        }

        try {
            //Get the id
            _commentId = commentObj.getStringDefault("id");
            _userId = commentObj.getStringDefault("user_id");

            //Get the content
            _content = commentObj.getStringDefault("content");
            if (_content != null) _content = _content.trim();

            _content = commentObj.getStringDefault("content");

            _commentableId = commentObj.getIntDefault("commentable_id");
            _commentableType = commentObj.getStringDefault("commentable_type");
            _createdAt = commentObj.getStringDefault("created_at");
            _updatedAt = commentObj.getStringDefault("updated_at");

            FNJsonObject likesMetaObj = commentObj.getJSONObject("likesmeta");
            if (likesMetaObj == null) {
                Log.i(_TAG, "No likesmeta tag");
                return false;
            }
            //Get the user information
            _likesMeta.getDataFromJSONObject(likesMetaObj);

            FNJsonObject userObj = commentObj.getJSONObject("user");
            if (userObj == null) {
                Log.i(_TAG, "No user tag");
                return false;
            }
            //Get the user information
            _userProfile.getDataFromJSONObject(userObj);


        } catch (Exception e) {
            e.printStackTrace();
            Log.e(_TAG, "Error occur during parsing JSON object");
            return false;
        }
        return true;
    }

    public static Comment parseComment(String json) {
        try {
            FNJsonObject commentObject = new FNJsonObject(json);

            Comment comment = new Comment();
            comment.getDataFromJSONObject(commentObject);
            return comment;
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static ArrayList<Comment> parseCommentArray(String json) {
        try {
            ArrayList<Comment> commentList = new ArrayList<Comment>();
            FNJsonObject pointObject = new FNJsonObject(json);
            FNJsonArray commentsArray = pointObject.getJSONArray("items");
            if (commentsArray == null) {
                return null;
            }
            for (int i = 0; i < commentsArray.length(); i++) {
                FNJsonObject object = commentsArray.getJSONObject(i);
                Comment comment = new Comment();
                comment.getDataFromJSONObject(object);
                if (comment == null) return null;
                commentList.add(comment);
            }
            return commentList;
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return null;
    }
}
