package com.fusionnext.cloud.datalink.http;

import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;

import com.fusionnext.cloud.iot.Public;
import com.fusionnext.cloud.utils.FNNetworkLog;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.DataOutputStream;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.InetAddress;
import java.net.URL;
import java.net.URLEncoder;
import java.net.UnknownHostException;
import java.security.SecureRandom;
import java.security.cert.Certificate;
import java.security.cert.CertificateException;
import java.security.cert.CertificateParsingException;
import java.security.cert.X509Certificate;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLException;
import javax.net.ssl.SSLSession;
import javax.net.ssl.SSLSocketFactory;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;

import static com.fusionnext.cloud.datalink.http.HttpUtil.BC_HTTP_POST_FEEDBACK;

/**
 * Created by Mike Chang on 2016/2/22.
 */

public class HttpPost extends AsyncTask<HttpPost.PostParam, Void, HttpPost.PostResult> {
protected final static String _TAG = "HttpPost";

private final static String SECTION_HTTP_POST_RESULT = "SECTION_HTTP_POST_RESULT";
private final static String SECTION_HTTP_POST_DATA = "SECTION_HTTP_POST_DATA";
private final static String SECTION_HTTP_POST_TYPE = "SECTION_HTTP_POST_TYPE";
private final static String SECTION_HTTP_POST_ID = "SECTION_HTTP_POST_ID";
private final static String SECTION_HTTP_UPLOAD_PERCENT = "SECTION_HTTP_UPLOAD_PERCENT";

private final static int _POST_TYPE_UNKNOWN = 0x50;

private final static String CONTENT_TYPE_MULTI_PART_FORM_DATA = "multipart/form-data";
private final static String CONTENT_TYPE_URL_ENCODED = "application/x-www-form-urlencoded";
private final static String TWO_HYPHENS = "--";
private final static String PARAM_LINK = "&";
private final static String BOUNDARY = "****************fD4fH3gL0hK7aI6";  // ?�据?��?�?
private final static String LINE_END = "\r\n";

private Context context;
//Default is post, can use "POST", "DELETE", or others
private String HTTP_REQUEST_TYPE = "POST";

private OnParseJsonListener onParseJsonListener = null;
    public static String ServerName=null;

public static class PostParam {
    public int type;
    public URL url;
    public String id;
    public String accessToken = null;
    public boolean uploadFile = false;
    public Map<Object, Object> param;
}

    public static class SSLSocketClient {

        //獲取這個SSLSocketFactory
        public static SSLSocketFactory getSSLSocketFactory() {
            try {
                SSLContext sslContext = SSLContext.getInstance("SSL");
                sslContext.init(null, getTrustManager(), new SecureRandom());
                return sslContext.getSocketFactory();
            } catch (Exception e) {
                throw new RuntimeException(e);
            }
        }

        //獲取TrustManager
        private static TrustManager[] getTrustManager() {
            TrustManager[] trustAllCerts = new TrustManager[]{
                    new X509TrustManager() {
                        @Override
                        public void checkClientTrusted(X509Certificate[] chain, String authType) {
                        }

                        @Override
                        public void checkServerTrusted(X509Certificate[] chain, String authType) {
                        }

                        @Override
                        public X509Certificate[] getAcceptedIssuers() {
                            // return null; 或者
                            return new X509Certificate[]{}; // 空實現
                        }
                    }
            };
            return trustAllCerts;
        }

        //獲取HostnameVerifier
        public static HostnameVerifier getHostnameVerifier() {
            HostnameVerifier hostnameVerifier = new HostnameVerifier() {
                @Override
                public boolean verify(String s, SSLSession sslSession) {
                    // true表示信任所有域名
                    return true;
                }
            };
            return hostnameVerifier;
        }
    }

public static class PostResult {
    public String id = "";
    public String returnStr = "";
    public boolean returnResult;
    public int postType;


    private PostResult(int postType, boolean result, String retStr) {
        this.postType = postType;
        this.returnResult = result;
        this.returnStr = retStr;
    }
}

    public void setHttpRequestType(String requestType) {
        this.HTTP_REQUEST_TYPE = requestType;
    }


    /**
     * 覆盖java默认的证书验证
     */
    private static final TrustManager[] trustAllCerts = new TrustManager[]{new X509TrustManager() {
        public java.security.cert.X509Certificate[] getAcceptedIssuers() {
            return new java.security.cert.X509Certificate[]{};
        }

        public void checkClientTrusted(X509Certificate[] chain, String authType)
                throws CertificateException {
        }

        public void checkServerTrusted(X509Certificate[] chain, String authType)
                throws CertificateException {
        }
    }};

    /**
     * 信任所有
     * @param connection
     * @return
     */
    private static SSLSocketFactory trustAllHosts(HttpsURLConnection connection) {
        SSLSocketFactory oldFactory = connection.getSSLSocketFactory();
        try {
            SSLContext sc = SSLContext.getInstance("TLS");
            sc.init(null, trustAllCerts, new java.security.SecureRandom());
            SSLSocketFactory newFactory = sc.getSocketFactory();
            connection.setSSLSocketFactory(newFactory);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return oldFactory;
    }

    private static final int ALT_DNS_NAME = 2;
    private static final int ALT_IPA_NAME = 7;

    private List<String> getSubjectAltNames(X509Certificate certificate, int type) {
        List<String> result = new ArrayList<>();
        try {
            Collection<?> subjectAltNames = certificate.getSubjectAlternativeNames();
            if (subjectAltNames == null) {
                return Collections.emptyList();
            }
            for (Object subjectAltName : subjectAltNames) {
                List<?> entry = (List<?>) subjectAltName;
                if (entry == null || entry.size() < 2) {
                    continue;
                }
                Integer altNameType = (Integer) entry.get(0);
                if (altNameType == null) {
                    continue;
                }
                if (altNameType == type) {
                    String altName = (String) entry.get(1);
                    if (altName != null) {
                        result.add(altName);
                    }
                }
            }
            return result;
        } catch (CertificateParsingException e) {
            return Collections.emptyList();
        }
    }



    @Override
    protected PostResult doInBackground(PostParam... postParams) {
        URL url;

        PostResult result = new PostResult(_POST_TYPE_UNKNOWN, false, "");
        if (postParams.length <= 0) return result;

        Map<Object, Object> param = postParams[0].param;
        url = postParams[0].url;
        result.id = postParams[0].id;
        result.postType = postParams[0].type;

        HttpURLConnection conn = null;
        DataOutputStream output = null;
        BufferedReader input = null;

        boolean error = false;
        try {
            FNNetworkLog.d(_TAG, url.toString());
            conn = (HttpURLConnection) url.openConnection();
            conn.setConnectTimeout(HttpUtil._HTTP_CONNECT_TIMEOUT);
            conn.setReadTimeout(HttpUtil._HTTP_READ_TIMEOUT);
            conn.setDoInput(true);
            boolean doOutput = HTTP_REQUEST_TYPE.equals("POST");
            conn.setDoOutput(doOutput);
            conn.setUseCaches(false);
            conn.setRequestMethod(HTTP_REQUEST_TYPE);
            conn.setRequestProperty("Connection", "keep-alive");

            // 配置https的證書
            if ("https".equalsIgnoreCase(url.getProtocol())){
                ((HttpsURLConnection) conn).setSSLSocketFactory(SSLSocketClient.getSSLSocketFactory());
                ((HttpsURLConnection) conn).setHostnameVerifier(SSLSocketClient.getHostnameVerifier());
            }

            Locale locale = null;

            if (context != null) {
                locale = context.getResources().getConfiguration().locale;
            }
            String localeStr = null;
            if (locale != null) {
                localeStr = locale.getLanguage().toLowerCase() + "-" + locale.getCountry().toLowerCase();
            }
            if (localeStr == null || localeStr.equals("")) localeStr = "en";
            conn.setRequestProperty("Accept-Language", localeStr);

            if (postParams[0].accessToken != null) {
                //If login
                conn.setRequestProperty("Authorization",
                        postParams[0].accessToken);
            }

            if (postParams[0].uploadFile) {
                conn.setRequestProperty("Content-Type",
                        CONTENT_TYPE_MULTI_PART_FORM_DATA + "; boundary=" + BOUNDARY);
                conn.setRequestProperty("Accept", "application/image");
                ImageFile[] files = new ImageFile[param.size()];
                int k = 0;
                Iterator iteratorKeys = param.keySet().iterator();
                Iterator iteratorValues = param.values().iterator();
                while (iteratorKeys.hasNext() && iteratorValues.hasNext()) {
                    Object key = iteratorKeys.next();
                    Object value = iteratorValues.next();
                    String keyStr;
                    String valueStr;

                    if (key != null && value != null) {
                        keyStr = key.toString();
                        valueStr = value.toString();
                        conn.setRequestProperty(keyStr, valueStr);
                        files[k] = new ImageFile(valueStr, keyStr);
                        k++;
                    }
                }

                conn.connect();
                output = new DataOutputStream(conn.getOutputStream());
                addImageContent(files, output, true);
                output.writeBytes(TWO_HYPHENS + BOUNDARY + TWO_HYPHENS + LINE_END);// ?�据结�??��?
                output.flush();
            } else {
                conn.setRequestProperty("Content-Type", CONTENT_TYPE_URL_ENCODED);
                conn.setRequestProperty("Accept", "*/*");
                conn.connect();
                if (HTTP_REQUEST_TYPE.equals("POST")) {
                    output = new DataOutputStream(conn.getOutputStream());

                    Map<Object, Object> newParam = new HashMap<>();
                    if (param != null) {
                        Iterator iteratorKeys = param.keySet().iterator();
                        Iterator iteratorValues = param.values().iterator();
                        while (iteratorKeys.hasNext() && iteratorValues.hasNext()) {
                            Object key = iteratorKeys.next();
                            Object value = iteratorValues.next();
                            if (key != null && value != null) {
                                newParam.put(key, value);
                            }
                        }
                    }
                    addUrlEncodedText(newParam, output);    //Add Url encoded text
                    output.flush();
                }
            }

            int code = conn.getResponseCode();
            result.returnResult = (code == 200 || code == 201);
            if (result.returnResult) {
                input = new BufferedReader(new InputStreamReader(conn.getInputStream()));
            } else {
                input = new BufferedReader(new InputStreamReader(conn.getErrorStream()));
            }
            StringBuilder response = new StringBuilder();
            String oneLine;
            while ((oneLine = input.readLine()) != null) {
                response.append(oneLine).append(LINE_END);
            }
            String responseStr = response.toString();
            JSONObject rltObject = new JSONObject(responseStr);
            String message = responseStr;

            if (rltObject.has("error") && !rltObject.isNull("error")) {
                error = true;
                message = rltObject.toString();
                /*if (errorObject != null) {
                    if (error && errorObject.has("message") && !errorObject.isNull("message")) {
                        message = errorObject.getString("message");
                    }
                } else {
                    if (error && rltObject.has("error_description") && !rltObject.isNull("error_description")) {
                        message = rltObject.getString("error_description");
                    }*
                }*/
            }

            result.returnResult = result.returnResult && (!error);
            result.returnStr = message;

            FNNetworkLog.d(_TAG, "code = " + code + ",  result =  " + result.returnStr);
        } catch (JSONException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            result.returnResult = result.returnResult && (!error);

            try {
                if (output != null) {
                    output.close();
                }
                if (input != null) {
                    input.close();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }

            if (conn != null) {
                conn.disconnect();
            }
        }

        return result;
    }


//    @Override
    protected PostResult old_doInBackground(PostParam... postParams) {
        URL url;
        PostResult result = new PostResult(_POST_TYPE_UNKNOWN, false, "");
        if (postParams.length <= 0) return result;

        Map<Object, Object> param = postParams[0].param;
        url = postParams[0].url;
        result.id = postParams[0].id;
        result.postType = postParams[0].type;

        HttpURLConnection conn = null;
        DataOutputStream output = null;
        BufferedReader input = null;

        boolean error = false;
        SSLSocketFactory oldSocketFactory = null;
        HostnameVerifier oldHostnameVerifier = null;

        try {
            FNNetworkLog.d(_TAG, url.toString());
            conn = (HttpURLConnection) url.openConnection();
            conn.setConnectTimeout(HttpUtil._HTTP_CONNECT_TIMEOUT);
            conn.setReadTimeout(HttpUtil._HTTP_READ_TIMEOUT);
            conn.setDoInput(true);
            boolean doOutput = HTTP_REQUEST_TYPE.equals("POST");

            conn.setDoOutput(doOutput);
            conn.setUseCaches(false);
            conn.setRequestMethod(HTTP_REQUEST_TYPE);
            conn.setRequestProperty("Connection", "keep-alive");
           // conn.setRequestProperty("Connection", "close");

            final HttpsURLConnection https = (HttpsURLConnection) conn;

            oldSocketFactory = trustAllHosts(https);
            oldHostnameVerifier = https.getHostnameVerifier();


           // https.setHostnameVerifier(DO_NOT_VERIFY);
            https.setHostnameVerifier(new HostnameVerifier() {

                @Override
                public boolean verify(String hostname, SSLSession session) {
                    //return true;
                    HostnameVerifier hv =
                            HttpsURLConnection.getDefaultHostnameVerifier();
                    if( ServerName == null ) {
                        try {
                            Certificate[] certificates = session.getPeerCertificates();
                            List<String> altNames = getSubjectAltNames((X509Certificate) certificates[0], ALT_DNS_NAME);
                            for (int i = 0, size = altNames.size(); i < size; i++) {
                                if (ServerName == null)
                                    ServerName = altNames.get(i);
                            }
                           Public._HOST_SERVER = ServerName;
                           if( ServerName.equals("api.fusionnextinc.com"))
                                   Public._HOST_SERVER = Public._HOST_PRODUCTION_SERVER;

                        } catch (SSLException e) {
                            return false;
                        }
                        https.disconnect();
                        return true;
                    }
                    return true;
                }
            });
            Locale locale = null;

            if (context != null) {
                locale = context.getResources().getConfiguration().locale;
            }
            String localeStr = null;
            if (locale != null) {
                localeStr = locale.getLanguage().toLowerCase() + "-" + locale.getCountry().toLowerCase();
            }
            if (localeStr == null || localeStr.equals("")) localeStr = "en";
            conn.setRequestProperty("Accept-Language", localeStr);

            if (postParams[0].accessToken != null) {
                //If login
                conn.setRequestProperty("Authorization",
                        postParams[0].accessToken);
            }

            if (postParams[0].uploadFile) {
                conn.setRequestProperty("Content-Type",
                        CONTENT_TYPE_MULTI_PART_FORM_DATA + "; boundary=" + BOUNDARY);
                conn.setRequestProperty("Accept", "application/image");
                ImageFile[] files = new ImageFile[param.size()];
                int k = 0;
                Iterator iteratorKeys = param.keySet().iterator();
                Iterator iteratorValues = param.values().iterator();
                while (iteratorKeys.hasNext() && iteratorValues.hasNext()) {
                    Object key = iteratorKeys.next();
                    Object value = iteratorValues.next();
                    String keyStr;
                    String valueStr;

                    if (key != null && value != null) {
                        keyStr = key.toString();
                        valueStr = value.toString();
                        conn.setRequestProperty(keyStr, valueStr);
                        files[k] = new ImageFile(valueStr, keyStr);
                        k++;
                    }
                }

                conn.connect();
                output = new DataOutputStream(conn.getOutputStream());
                addImageContent(files, output, true);
                output.writeBytes(TWO_HYPHENS + BOUNDARY + TWO_HYPHENS + LINE_END);// ?�据结�??��?
                output.flush();
            } else {
                https.setRequestProperty("Content-Type", CONTENT_TYPE_URL_ENCODED);
                https.setRequestProperty("Accept", "*/*");

                https.connect();
                if (HTTP_REQUEST_TYPE.equals("POST")) {
                    output = new DataOutputStream(conn.getOutputStream());

                    Map<Object, Object> newParam = new HashMap<>();
                    if (param != null) {
                        Iterator iteratorKeys = param.keySet().iterator();
                        Iterator iteratorValues = param.values().iterator();
                        while (iteratorKeys.hasNext() && iteratorValues.hasNext()) {
                            Object key = iteratorKeys.next();
                            Object value = iteratorValues.next();
                            if (key != null && value != null) {
                                newParam.put(key, value);
                            }
                        }
                    }
                    addUrlEncodedText(newParam, output);    //Add Url encoded text
                    output.flush();
                }
            }

            int code = https.getResponseCode();
            result.returnResult = (code == 200 || code == 201);
            if (result.returnResult) {
                input = new BufferedReader(new InputStreamReader(conn.getInputStream()));
            } else {
                input = new BufferedReader(new InputStreamReader(conn.getErrorStream()));
            }
            StringBuilder response = new StringBuilder();
            String oneLine;
            while ((oneLine = input.readLine()) != null) {
                response.append(oneLine).append(LINE_END);
            }
            String responseStr = response.toString();
            JSONObject rltObject = new JSONObject(responseStr);
            String message = responseStr;

            if (rltObject.has("error") && !rltObject.isNull("error")) {
                error = true;
                message = rltObject.toString();
                /*if (errorObject != null) {
                    if (error && errorObject.has("message") && !errorObject.isNull("message")) {
                        message = errorObject.getString("message");
                    }
                } else {
                    if (error && rltObject.has("error_description") && !rltObject.isNull("error_description")) {
                        message = rltObject.getString("error_description");
                    }*
                }*/
            }

            result.returnResult = result.returnResult && (!error);
            result.returnStr = message;

            FNNetworkLog.d(_TAG, "code = " + code + ",  result =  " + result.returnStr);
        } catch (JSONException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            result.returnResult = result.returnResult && (!error);

            try {
                if (output != null) {
                    output.close();
                }
                if (input != null) {
                    input.close();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }

            if (conn != null) {
                conn.disconnect();
            }
        }

        return result;
    }

    @Override
    protected void onPostExecute(PostResult postResult) {
        if (this.onParseJsonListener != null) {
            if (postResult.returnStr == null || postResult.returnStr.trim().equals("")) {
                return;
            }
            this.onParseJsonListener.onParse(postResult.returnResult, postResult.postType, postResult.id, postResult.returnStr);
        } else {
            if (postResult != null && context != null) {
                Bundle bundle = new Bundle();
                bundle.putBoolean(SECTION_HTTP_POST_RESULT, postResult.returnResult);
                bundle.putString(SECTION_HTTP_POST_DATA, postResult.returnStr);
                bundle.putString(SECTION_HTTP_POST_ID, postResult.id);
                bundle.putInt(SECTION_HTTP_POST_TYPE, postResult.postType);

                HttpUtil.broadcastWithExtra(context, BC_HTTP_POST_FEEDBACK, bundle);
            }
        }
        super.onPostExecute(postResult);
    }

    public void setContext(Context context) {
        this.context = context;
    }

    private void addImageContent(ImageFile[] files, DataOutputStream output,
                                 boolean broadcastPercent) {
        for (int i = 0; i < files.length; i++) {
            //for(ImageFile file : files) {
            StringBuilder split = new StringBuilder();
            split.append(TWO_HYPHENS + BOUNDARY + LINE_END);
            split.append("Content-Disposition: form-data; name=\"")
                    .append(files[i].getFormName())
                    .append("\"; filename=\"")
                    .append(files[i].getFileName())
                    .append("\"").append(LINE_END);
            split.append("Content-Type: ").append(files[i].getContentType()).append(LINE_END);
            split.append(LINE_END);
            try {
                output.writeBytes(split.toString());
                output.write(files[i].getData(), 0, files[i].getData().length);
                output.writeBytes(LINE_END);
                if (broadcastPercent && (context != null)) {
                    Bundle bundle = new Bundle();
                    bundle.putInt(SECTION_HTTP_UPLOAD_PERCENT, 100 * (i + 1) / files.length);
                    HttpUtil.broadcastWithExtra(context, HttpUtil.BC_HTTP_UPLOAD_PERCENT, bundle);
                }
            } catch (IOException e) {
                throw new RuntimeException(e);
            }
        }
    }

    private void addUrlEncodedText(Map<Object, Object> params, DataOutputStream output) {
        StringBuilder sb = new StringBuilder();
        Iterator iteratorKeys = params.keySet().iterator();
        Iterator iteratorValues = params.values().iterator();
        try {
            while (iteratorKeys.hasNext() && iteratorValues.hasNext()) {
                Object key = iteratorKeys.next();
                Object value = iteratorValues.next();
                if (!sb.toString().equals("")) sb.append(PARAM_LINK);
                String encodeKey = URLEncoder.encode(key.toString(), "utf-8");
                String encodeValue = URLEncoder.encode(value.toString(), "utf-8");
                sb.append(encodeKey).append("=").append(encodeValue);
            }
            String sbStr = sb.toString();

            try {
                output.writeBytes(sbStr);
            } catch (IOException e) {
                e.printStackTrace();
            }

        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }

    }


private static class ImageFile {

    private String fullPath;
    private String filename;
    private String key;

    private ImageFile(String fullPath, String key) {
        this.fullPath = fullPath;
        this.key = key;

        this.filename = fullPath;//getFileNameByFullPath(this.fullPath);
    }

    private String getFormName() {
        return key;
    }

    private String getFileName() {
        return filename;
    }

    private String getContentType() {
        return HttpPost.getContentType(filename);
    }

    private byte[] getData() {

        try {
            FileInputStream fis = new FileInputStream(fullPath);
            ByteArrayOutputStream bos = new ByteArrayOutputStream();
            byte[] b = new byte[4096];
            int bytesRead;
            while ((bytesRead = fis.read(b)) != -1) {
                bos.write(b, 0, bytesRead);
            }
            return bos.toByteArray();
        } catch (FileNotFoundException ex) {
            ex.printStackTrace();
            return (new byte[0]);
        } catch (IOException e) {
            e.printStackTrace();
        }

        return (new byte[0]);
    }
}

    private static String getContentType(String filename) {
        String retStr;

        String extension = filename.substring(filename.indexOf('.'));
        extension = extension.toUpperCase();
        switch (extension) {
            case ".JPEG":
            case ".JPG":
                retStr = "image/jpeg";
                break;
            case ".PNG":
                retStr = "image/png";
                break;
            case ".GIF":
                retStr = "image/gif";
                break;
            default:
                retStr = "text/plain";      //default value
                break;
        }

        return retStr;
    }

    public void setOnParseJsonListener(HttpPost.OnParseJsonListener onParseJsonListener) {
        this.onParseJsonListener = onParseJsonListener;
    }

public interface OnParseJsonListener {
    void onParse(boolean returnResult, int postType, String id, String returnStr);
}
}