package com.fusionnextinc.fndoorbell;

import android.app.Application;
import android.content.Context;
import androidx.test.platform.app.InstrumentationRegistry;
import android.test.ApplicationTestCase;

import com.fusionnextinc.doorbell.manager.thingsManager.ThingsHandler;
import com.fusionnextinc.doorbell.manager.thingsManager.ThingsManager;
import com.fusionnextinc.doorbell.objects.FNError;
import com.fusionnextinc.doorbell.objects.FNEvent;
import com.fusionnextinc.doorbell.objects.FNFirmware;
import com.fusionnextinc.doorbell.objects.FNThing;
import com.fusionnextinc.doorbell.objects.FNUserProfile;
import com.fusionnextinc.doorbell.utils.FNLog;

import java.util.ArrayList;
import java.util.concurrent.CountDownLatch;

/**
 * To work on unit tests, switch the Test Artifact in the Build Variants view.
 */
public class ThingsManagerUnitTest extends ApplicationTestCase<Application> {
    protected final static String _TAG = "AccountUnitTest";
    Context context;
    private CountDownLatch latch;
    private static FNThing fnThing = null;
    private final String THING_ID = "25";

    public ThingsManagerUnitTest() {
        super(Application.class);
    }

    public void setUp() throws Exception {
        super.setUp();
        context = getTestContext();
        assertNotNull(context);

    }

    public void testCreateThing() {
        latch = new CountDownLatch(1); //創建CountDownLatch
        ThingsManager thingsManager = ThingsManager.getInstance(context);

        FNThing fnThing = new FNThing();
        fnThing.setProductSlug("iqgroup_doorbell");
        fnThing.setIdentifier("d2892d90-fc09-4e63-b112-18f5d533ab88");
        fnThing.setName("FN_IOT");
        fnThing.setFirmwareVer("1.0.0");

        thingsManager.setThingsListener(thingsListener);
        thingsManager.createThing(fnThing);
        try {
            latch.await();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public void testUpdateThing() {
        latch = new CountDownLatch(1); //創建CountDownLatch
        ThingsManager thingsManager = ThingsManager.getInstance(context);

        FNThing fnThing = new FNThing();
        fnThing.setProductSlug("iqgroup_doorbell");
        fnThing.setIdentifier("d2892d90-fc09-4e63-b112-18f5d533ab68");
        fnThing.setName("FN_IOT");
        thingsManager.setThingsListener(thingsListener);
        thingsManager.updateThing(THING_ID, fnThing);
        try {
            latch.await();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public void testGetOwnedThingList() {
        latch = new CountDownLatch(1); //創建CountDownLatch
        ThingsManager thingsManager = ThingsManager.getInstance(context);

        thingsManager.setThingsListener(thingsListener);
        //thingsManager.getOwnedThingsList(HttpData._GET_TYPE_SORT_ORDER_DESC, null, null);
        try {
            latch.await();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public void testGetInvitedThingList() {
        latch = new CountDownLatch(1); //創建CountDownLatch
        ThingsManager thingsManager = ThingsManager.getInstance(context);

        thingsManager.setThingsListener(thingsListener);
        //thingsManager.getInvitedThingsList(HttpData._GET_TYPE_SORT_ORDER_DESC, null, null);
        try {
            latch.await();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
    public void testGetSharedThingList() {
        latch = new CountDownLatch(1); //創建CountDownLatch
        ThingsManager thingsManager = ThingsManager.getInstance(context);

        thingsManager.setThingsListener(thingsListener);
        //thingsManager.getSharedThingsList(HttpData._GET_TYPE_SORT_ORDER_DESC, null, null);
        try {
            latch.await();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
    public void testGetStatesList() {
        latch = new CountDownLatch(1); //創建CountDownLatch
        ThingsManager thingsManager = ThingsManager.getInstance(context);

        thingsManager.setThingsListener(thingsListener);
        //thingsManager.getStatesList(THING_ID, null, HttpData._GET_TYPE_SORT_ORDER_DESC, null, null);
        try {
            latch.await();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public void testGetSettingsList() {
        latch = new CountDownLatch(1); //創建CountDownLatch
        ThingsManager thingsManager = ThingsManager.getInstance(context);

        thingsManager.setThingsListener(thingsListener);
        //thingsManager.getSettingsList(THING_ID, null, HttpData._GET_TYPE_SORT_ORDER_DESC, null, null);
        try {
            latch.await();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public void testGetEventList() {
        latch = new CountDownLatch(1); //創建CountDownLatch
        ThingsManager thingsManager = ThingsManager.getInstance(context);

        thingsManager.setThingsListener(thingsListener);
        //thingsManager.getEventsList(THING_ID, null, HttpData._GET_TYPE_SORT_ORDER_DESC, null, null);
        try {
            latch.await();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public void testGetInvitedUserList() {
        latch = new CountDownLatch(1); //創建CountDownLatch
        ThingsManager thingsManager = ThingsManager.getInstance(context);

        thingsManager.setThingsListener(thingsListener);
        //thingsManager.getInvitedUserList(THING_ID, HttpData._GET_TYPE_SORT_ORDER_DESC, null, null);
        try {
            latch.await();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public void testGetInvitedThingsList() {
        latch = new CountDownLatch(1); //創建CountDownLatch
        ThingsManager thingsManager = ThingsManager.getInstance(context);

        thingsManager.setThingsListener(thingsListener);
        //thingsManager.getInvitedThingsList(HttpData._GET_TYPE_SORT_ORDER_DESC, null, null);
        try {
            latch.await();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    protected void tearDown() throws Exception {
        //super.tearDown();
    }

    /**
     * @return The {@link Context} of the test project.
     */
    private Context getTestContext() {
        try {
            return (Context) InstrumentationRegistry.getTargetContext();
        } catch (final Exception exception) {
            exception.printStackTrace();
            return null;
        }
    }

    final ThingsHandler.ThingsListener thingsListener = new ThingsHandler.ThingsListener() {

        @Override
        public void onListUserOwnedThings(ThingsManager instance, boolean result, String userId, ArrayList<FNThing> thingArrayList, FNError error) {

        }

        @Override
        public void onListUserInvitedThings(ThingsManager instance, boolean result, String userId, ArrayList<FNThing> thingArrayList, FNError error) {

        }

        @Override
        public void onListUserSharedThings(ThingsManager instance, boolean result, String userId, ArrayList<FNThing> thingArrayList, FNError error) {

        }

        @Override
        public void onListUserAllThings(ThingsManager instance, boolean result, String userId, ArrayList<FNThing> ownedThingArrayList, ArrayList<FNThing> sharedThingArrayList, ArrayList<FNThing> invitedThingArrayList, FNError error) {

        }

        @Override
        public void onListThingEvents(ThingsManager instance, boolean result, String thingId, String eventName, ArrayList<FNEvent> eventList, FNError error) {
            latch.countDown();
            assertEquals(true, result);
        }

        @Override
        public void onListThingEventsByTime(ThingsManager instance, boolean result, String thingId, String eventName, String startTime, String endTime, ArrayList<FNEvent> eventList, FNError error) {
            latch.countDown();
            assertEquals(true, result);
        }

        @Override
        public void onCurBatteryLevel(ThingsManager instance, boolean result, String thingId, String batteryLevel, FNError error) {
            latch.countDown();
            assertEquals(true, result);
        }

        @Override
        public void onCurFirmwareVersion(ThingsManager instance, boolean result, String thingId, String fwVersion, FNError error) {
            latch.countDown();
            assertEquals(true, result);
        }

        @Override
        public void onNewFirmwareVersion(ThingsManager instance, boolean result, String thingId, FNFirmware fnFirmware) {

        }

        @Override
        public void onStartFwUpdate(ThingsManager instance, boolean result, String thingId, String fwId) {

        }

        @Override
        public void onCurRingTone(ThingsManager instance, boolean result, String thingId, String ringtoneId, FNError error) {
            latch.countDown();
            assertEquals(true, result);
        }

        @Override
        public void onListInvitedUsers(ThingsManager instance, boolean result, String thingId, ArrayList<FNUserProfile> userList, FNError error) {
            latch.countDown();
            assertEquals(true, result);
        }

        @Override
        public void onListSharedUsers(ThingsManager instance, boolean result, String thingId, ArrayList<FNUserProfile> userList, FNError error) {

        }

        @Override
        public void onCreateClaimCode(ThingsManager instance, boolean result, String claimCode, FNError error) {

        }

        @Override
        public void onCreateThing(ThingsManager instance, boolean result, FNThing fnThing, FNError error) {
            latch.countDown();
            assertNotNull(fnThing);
            ThingsManagerUnitTest.fnThing = fnThing;
            FNLog.d(_TAG, "FN Thing = " + fnThing.getName());
        }

        @Override
        public void onUpdateThing(ThingsManager instance, boolean result, FNThing fnThing, FNError error) {
            latch.countDown();
            assertNotNull(fnThing);

            latch = new CountDownLatch(1); //創建CountDownLatch
            ThingsManager thingsManager = ThingsManager.getInstance(context);

            thingsManager.setThingsListener(thingsListener);
            thingsManager.deleteThing(ThingsManagerUnitTest.fnThing.getThingId());
        }

        @Override
        public void onDeleteThing(ThingsManager instance, boolean result, FNError error) {
            latch.countDown();
            assertEquals(true, result);
        }

        @Override
        public void onGetThing(ThingsManager instance, boolean result, FNThing fnThing, FNError error) {

        }

        @Override
        public void onChangeRingtone(ThingsManager instance, boolean result, String thingId, int ringtoneId) {

        }

        @Override
        public void onSetTimer(ThingsManager instance, boolean result, String thingId, String value) {

        }

        @Override
        public void onSetSchedule(ThingsManager instance, boolean result, String thingId, String value) {

        }

        @Override
        public void onShareInvited(ThingsManager instance, boolean result, String thingId, FNError error) {

        }

        @Override
        public void onShareAccepted(ThingsManager instance, boolean result, String thingId, FNError error) {

        }

        @Override
        public void onShareRejected(ThingsManager instance, boolean result, String thingId, FNError error) {

        }

        @Override
        public void onSwitchOnOff(ThingsManager instance, boolean result, String thingId, boolean isTurnOn) {

        }
    };
}