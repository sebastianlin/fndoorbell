package com.fusionnextinc.doorbell.fragement;

import android.app.AppOpsManager;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.pm.ApplicationInfo;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.fusionnextinc.doorbell.DefaultAppConfig;
import com.fusionnextinc.doorbell.MyApplication;
import com.fusionnextinc.doorbell.R;
import com.fusionnextinc.doorbell.manager.account.AaccountHandler;
import com.fusionnextinc.doorbell.manager.account.AccountManager;
import com.fusionnextinc.doorbell.objects.FNError;
import com.fusionnextinc.doorbell.utils.Utils;
import com.fusionnextinc.doorbell.widget.FNAlert.FNAlertDialog;
import com.fusionnextinc.doorbell.widget.FNDialog;
import com.fusionnextinc.doorbell.widget.FNLayoutUtil;
import com.fusionnextinc.doorbell.widget.FNProgressBarCustom;
import com.fusionnextinc.doorbell.widget.FNSwitch;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

/**
 * Created by Mike Chang on 2017/7/7
 */
public class AccountSettingsFragment extends FNFragment implements View.OnClickListener {
    private static final String TAG = AccountSettingsFragment.class.getSimpleName();

    private FNLayoutUtil layoutUtil;
    private TextView mTxtAccountStatus, mTxtNameStatus, mTxtAbout;
    private LinearLayout mItemName, mItemChangePwd, mItemMyQRCode, mItemLegal, mItemPolicy, mItemProduct, mItemCommonProblem, mItemContact, mItemAbout;
    private FNSwitch swNotification;
    private Button btnLogout;
    private FNProgressBarCustom progressBar;
    private AccountManager accountManager;

    private String URL_LEGAL_SERVICE = "https://www.apexgaming.info/pages/%E6%9C%8D%E5%8B%99%E6%A2%9D%E6%AC%BE";
    private String URL_POLICY = "https://www.apexgaming.info/pages/%E9%9A%B1%E7%A7%81%E6%AC%8A%E6%94%BF%E7%AD%96";
    private String URL_PRODUCT_INTRODUCTIONS = "https://www.apexgaming.info/pages/smart-power-management-user-manual";
    private String URL_COMMON_PROBLEMS = "http://www.ptcom.com.tw/qa.html";
    private String URL_SUGGESTIONS_CONTACT_US = "https://www.apexgaming.info/pages/contact-us";
    private String URL_ABOUT = "http://www.ptcom.com.tw/about.html";

    public static void openFragment(boolean clearOtherStack) {
        FNFragmentManager.getInstance().openFragment(new AccountSettingsFragment(), clearOtherStack);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        layoutUtil = new FNLayoutUtil(getActivity(), MyApplication.LAYOUT_WIDTH, MyApplication.LAYOUT_HEIGHT, FNLayoutUtil.BASE_WIDTH);
        accountManager = AccountManager.getInstance(getActivity());
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        final ViewGroup nullParent = null;

        getActivity().setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        if (!DefaultAppConfig.APP_SOLY) {
            getActionBar().resetAll();
            getActionBar().setTitle(getString(R.string.fn_tittle_settings_account_settings), Gravity.CENTER, null);
            getActionBar().setStartImage(R.drawable.back, new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    onBackPressed();
                }
            });
            getActionBar().setCoverMode(false);
            getActionBar().show();
        }
        View view = inflater.inflate(R.layout.fragment_account_settings, nullParent);
        layoutUtil.fitAllView(view);

        mTxtAccountStatus = view.findViewById(R.id.txt_account_status);
        mTxtNameStatus = view.findViewById(R.id.txt_name_status);
        mTxtAbout = view.findViewById(R.id.txt_about);
        mItemName = view.findViewById(R.id.user_name_item);
        mItemChangePwd = view.findViewById(R.id.change_pwd_item);
        mItemMyQRCode = view.findViewById(R.id.qrcode_item);
        mItemLegal = view.findViewById(R.id.legal_item);
        mItemPolicy = view.findViewById(R.id.policy_item);
        mItemProduct = view.findViewById(R.id.product_item);
        mItemCommonProblem = view.findViewById(R.id.common_problem_item);
        mItemContact = view.findViewById(R.id.contact_item);
        mItemAbout = view.findViewById(R.id.about_item);
        swNotification = (FNSwitch) view.findViewById(R.id.sw_notifications);
        btnLogout = (Button) view.findViewById(R.id.btn_logout);
        progressBar = new FNProgressBarCustom(getActivity(), (ViewGroup) view.findViewById(R.id.fragment_account_settings));
        if (!DefaultAppConfig.APP_SOLY) {
            mItemLegal.setVisibility(View.GONE);
            mItemPolicy.setVisibility(View.GONE);
            mItemProduct.setVisibility(View.GONE);
            mItemContact.setVisibility(View.GONE);
        }
        mItemName.setOnClickListener(this);
        mItemChangePwd.setOnClickListener(this);
        mItemMyQRCode.setOnClickListener(this);
        mItemLegal.setOnClickListener(this);
        mItemPolicy.setOnClickListener(this);
        mItemProduct.setOnClickListener(this);
        mItemCommonProblem.setOnClickListener(this);
        mItemContact.setOnClickListener(this);
        mItemAbout.setOnClickListener(this);

        btnLogout.setOnClickListener(this);
        swNotification.setOnCheckedChangeListener(new FNSwitch.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(FNSwitch fnSwitch, boolean isChecked) {
                new FNAlertDialog(getActivity(), FNAlertDialog.WARNING_TYPE)
                        .setTitleText(getActivity().getString(R.string.fn_tittle_settings_notifications))
                        .setContentText(getActivity().getString(R.string.fn_msg_settings_notifications_description, getActivity().getString(R.string.app_name)))
                        .setCancelText(getActivity().getString(R.string.fn_dialog_cancel))
                        .setConfirmText(getActivity().getString(R.string.fn_dialog_ok))
                        .showCancelButton(true)
                        .setCancelClickListener(new FNAlertDialog.OnSweetClickListener() {
                            @Override
                            public void onClick(FNAlertDialog sDialog) {
                                sDialog.dismiss();
                                swNotification.setChecked(accountManager.isNotificationEnabled(getActivity()));
                            }
                        })
                        .setConfirmClickListener(new FNAlertDialog.OnSweetClickListener() {
                            @Override
                            public void onClick(FNAlertDialog sDialog) {
                                sDialog.dismiss();
                                goToSet();
                            }
                        })
                        .show();
            }
        });
        accountManager.setAccountListener(accountListener);
        accountManager.getUserInfo();
        updateUserInfo();
        return view;
    }

    @Override
    public void onClick(View view) {
        if (view.getId() == R.id.btn_logout) {
            new FNAlertDialog(getActivity(), FNAlertDialog.WARNING_TYPE)
                    .setTitleText(getActivity().getString(R.string.fn_dialog_are_you_sure))
                    .setContentText(getActivity().getString(R.string.fn_dialog_logout))
                    .setCancelText(getActivity().getString(R.string.fn_dialog_cancel))
                    .setConfirmText(getActivity().getString(R.string.fn_btn_settings_logout))
                    .showCancelButton(true)
                    .setCancelClickListener(new FNAlertDialog.OnSweetClickListener() {
                        @Override
                        public void onClick(FNAlertDialog sDialog) {
                            sDialog.dismiss();
                        }
                    })
                    .setConfirmClickListener(new FNAlertDialog.OnSweetClickListener() {
                        @Override
                        public void onClick(FNAlertDialog sDialog) {
                            if (Utils.isNetworkConnected(getActivity())) {
                                startWait();
                                accountManager.setAccountListener(accountListener);
                                accountManager.userLogout();
                                sDialog.dismiss();
                            } else {
                                FNDialog.showNoNetworkDialog(getActivity());
                            }
                        }
                    })
                    .show();
        } else if (view.getId() == R.id.change_pwd_item) {
            ChangePasswordFragment.openFragment(false);
        } else if (view.getId() == R.id.user_name_item) {
            UserNameFragment.openFragment(false);
        } else if (view.getId() == R.id.qrcode_item) {
            MyQRCodeFragment.openFragment(false);
        } else if (view.getId() == R.id.legal_item) {
            Intent intent = new Intent(Intent.ACTION_VIEW);
            Uri url = Uri.parse(URL_LEGAL_SERVICE);
            intent.setData(url);
            startActivity(intent);
        } else if (view.getId() == R.id.policy_item) {
            Intent intent = new Intent(Intent.ACTION_VIEW);
            Uri url = Uri.parse(URL_POLICY);
            intent.setData(url);
            startActivity(intent);
        } else if (view.getId() == R.id.product_item) {
            Intent intent = new Intent(Intent.ACTION_VIEW);
            Uri url = Uri.parse(URL_PRODUCT_INTRODUCTIONS);
            intent.setData(url);
            startActivity(intent);
        } else if (view.getId() == R.id.common_problem_item) {
            Intent intent = new Intent(Intent.ACTION_VIEW);
            Uri url = Uri.parse(URL_COMMON_PROBLEMS);
            intent.setData(url);
            startActivity(intent);
        } else if (view.getId() == R.id.contact_item) {
            Intent intent = new Intent(Intent.ACTION_VIEW);
            Uri url = Uri.parse(URL_SUGGESTIONS_CONTACT_US);
            intent.setData(url);
            startActivity(intent);
        } else if (view.getId() == R.id.about_item) {
            Intent intent = new Intent(Intent.ACTION_VIEW);
            Uri url = Uri.parse(URL_ABOUT);
            intent.setData(url);
            startActivity(intent);
        }
    }

    private void startWait() {
        progressBar.show();
    }


    private void stopWait() {
        progressBar.hide();
    }

    final AaccountHandler.AccountListener accountListener = new AaccountHandler.AccountListener() {
        @Override
        public void onUserRegistered(AccountManager instance, boolean result, FNError error) {

        }

        @Override
        public void onUserLogined(AccountManager instance, boolean result, FNError error) {

        }

        @Override
        public void onGetUserInfo(AccountManager instance, boolean result, FNError error) {
            if (isAdded()) {
                updateUserInfo();
            }
        }

        @Override
        public void onUpdateUserInfo(AccountManager instance, boolean result, FNError error) {

        }

        @Override
        public void onUserLogout(AccountManager instance, boolean result, FNError error) {
            if (isAdded()) {
                stopWait();
                if (result) {
                    FNDialog.showSuccessDialog(getActivity(), getString(R.string.fn_tittle_settings_account_settings), getString(R.string.fn_msg_logout_successfully));
                    LoginFragment.openFragment(true);
                } else {
                    FNDialog.showErrorDialog(getActivity(), getString(R.string.fn_msg_logout_failed), error);
                }
            }
        }

        @Override
        public void onResetPassword(AccountManager instance, boolean result, FNError error) {

        }

        @Override
        public void onSetPassword(AccountManager instance, boolean result, FNError error) {

        }
    };

    private void updateUserInfo() {
        String userName = accountManager.getUserName();

        mTxtAbout.setText(getString(R.string.fn_title_about_name).replace("${NAME}", getString(R.string.app_name)));
        mTxtAccountStatus.setText(accountManager.getUserAccount());
        if (userName.length() < 0 || userName.equals("")) {
            mTxtNameStatus.setText(getString(R.string.fn_text_none));
        } else {
            mTxtNameStatus.setText(userName);
        }
        swNotification.setChecked(accountManager.isNotificationEnabled(getActivity()));
    }

    @Override
    public void onBackPressed() {
        if (progressBar != null && !progressBar.isShown()) {
            super.onBackPressed();
        }
    }

    private void goToSet() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            // 運行系統在5.x環境使用
            // 進入設定系統應用許可權界面
            Intent intent = new Intent(Settings.ACTION_APPLICATION_SETTINGS);
            startActivity(intent);
        } else {
            // 進入設定系統應用許可權界面
            Intent intent = new Intent(Settings.ACTION_APPLICATION_SETTINGS);
            startActivity(intent);
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        swNotification.setChecked(accountManager.isNotificationEnabled(getActivity()));
    }
}