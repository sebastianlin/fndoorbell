package com.fusionnext.cloud.iot;

import android.content.Context;

import com.fusionnext.cloud.datalink.http.HttpPost;
import com.fusionnext.cloud.datalink.json.FNJsonObject;

import org.json.JSONException;

import java.util.HashMap;

/**
 * Created by Mike Chang on 2017/3/27
 */
public class SmartPlug {
    protected final static String _TAG = "SmartPlug";
    private Context mContext;

    public SmartPlug(Context context) {
        this.mContext = context;
    }

    public void switchOnOff(final String thingId, final boolean isTurnOn, final IOTHandler.OnSmartPlugListener onSmartPlugListener) {
        if (thingId == null) {
            return;
        }
        FNJsonObject payloadJsonObj = new FNJsonObject();
        FNJsonObject statesJsonObj = new FNJsonObject();
        try {
            if (isTurnOn) {
                payloadJsonObj.put("on", true);
            } else {
                payloadJsonObj.put("on", false);
            }
            statesJsonObj.put("states", payloadJsonObj);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        HttpPost.PostParam[] params = new HttpPost.PostParam[1];
        params[0] = new HttpPost.PostParam();
        params[0].accessToken = Public.getAuthorization(mContext).getUserAccessToken();
        params[0].url = HttpData.generateHttpPostUrl(HttpData._POST_TYPE_SMART_PLUG_SWITCH_ON_OFF);
        params[0].type = HttpData._POST_TYPE_SMART_PLUG_SWITCH_ON_OFF;
        params[0].param = new HashMap<>();
        params[0].param.put("thing_id", thingId);
        params[0].param.put("name", "switch");
        params[0].param.put("payload", statesJsonObj.toString());

        HttpPost post = new HttpPost();
        post.setContext(mContext);
        post.setOnParseJsonListener(new HttpPost.OnParseJsonListener() {
            @Override
            public void onParse(boolean returnResult, int postType, String id, String returnStr) {
                onSmartPlugListener.onSwitchOnOff(returnResult, thingId, isTurnOn);
            }
        });
        post.execute(params);
    }

}
