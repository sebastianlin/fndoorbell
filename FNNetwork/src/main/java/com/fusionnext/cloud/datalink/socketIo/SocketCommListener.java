package com.fusionnext.cloud.datalink.socketIo;

/**
 * Created by freddie on 2015/5/21.
 */
public interface SocketCommListener {
    void onJoin(int userId);

    void onConnect();

    void onDisconnect();

    void onLocation(int userId, String time, String latitude, String longitude);

    void onRecvPhoto(int userId, String photoId, String time, String latitude, String longitude, String fileName);

    void onMessage(int userId, String msg, String time);

    void onLeave(int userId);
}
