package com.fusionnextinc.fndoorbell;

import android.content.Context;
import androidx.test.platform.app.InstrumentationRegistry;


/**
 * Instrumentation test, which will execute on an Android device.
 *
 * @see <a href="http://d.android.com/tools/testing">Testing documentation</a>
 */

public class ExampleInstrumentedTest {
    /**
     * @return The {@link Context} of the test project.
     */
    private Context getTestContext() {
        try {
            return (Context) InstrumentationRegistry.getTargetContext();
        } catch (final Exception exception) {
            exception.printStackTrace();
            return null;
        }
    }
}
