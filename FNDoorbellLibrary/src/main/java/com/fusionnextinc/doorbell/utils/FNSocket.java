package com.fusionnextinc.doorbell.utils;

import android.util.Log;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.Socket;
import java.net.SocketException;

/**
 * Created by chocoyang on 2016/3/21.
 */
public class FNSocket {
    private static final String TAG = "FNSocket";

    private static final int SOCKET_INIT_BUFFER = 4096;

    public interface FNSocketDataListener {
        void onDataReceive(byte[] data, int offset, int byteCount);
    }

    public interface FNSocketStatusListener {
        void onDisconnected();
    }

    private FNSocketStatusListener mStatusListener;
    private FNSocketDataListener mDataListener;
    private Socket socket;
    private OutputStream output;
    private InputStream input;
    private int mPort;
    private boolean isConnected;

    public boolean openSocket(final String ip, final int port, int timeout) {
        final FNWaitSignal connectWait = new FNWaitSignal();
        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    socket = new Socket(ip, port);
                    if (!connectWait.isReturned()) {
                        output = socket.getOutputStream();
                        input = socket.getInputStream();
                        mPort = port;
                        isConnected = true;
                        startReceiveData();
                        connectWait.notifySuccess();
                    } else {
                        closeSocket();
                    }
                } catch (IOException ignore) {
                    closeSocket();
                    if (!connectWait.isReturned()) {
                        connectWait.notifyFail();
                    }
                }
            }
        }).start();
        connectWait.waitNotify(timeout);
        if (connectWait.isSuccess()) {
            return true;
        } else if (connectWait.isTimeout()) {
            Log.w(TAG, "failed to connect to " + ip + " (port " + port + ") timeout (" + timeout + " sec)");
            return false;
        } else {
            Log.w(TAG, "failed to connect to " + ip + " (port " + port + ")");
            return false;
        }
    }

    public void setStatusListener(FNSocketStatusListener statusListener) {
        mStatusListener = statusListener;
    }

    public void closeSocket() {
        closeSocket(false);
    }

    public int getPort() {
        return mPort;
    }

    public boolean sendDataToServer(byte[] buffer, int offset, int count) {
        if (output == null)
            return false;
        try {
            output.write(buffer, offset, count);
            output.flush();
            return true;
        } catch (IOException e) {
            e.printStackTrace();
            return false;
        }
    }

    public void receiveDataFromServer(FNSocketDataListener dataListener) {
        mDataListener = dataListener;
    }

    private void startReceiveData() {
        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    int autoBuffer;
                    byte[] buf = new byte[(autoBuffer = SOCKET_INIT_BUFFER)];
                    while (input != null) {
                        int len;
                        if ((len = input.read(buf, 0, buf.length)) > 0) {
                            if (mDataListener != null) {
                                mDataListener.onDataReceive(buf, 0, len);
                            }
                            if (len >= autoBuffer)
                                buf = new byte[(autoBuffer += SOCKET_INIT_BUFFER)];
                        } else {
                            closeSocket(isConnected);
                        }
                    }
                } catch (IOException e) {
                    if (e instanceof SocketException) {
                        closeSocket(isConnected);
                    } else {
                        e.printStackTrace();
                    }
                }
            }
        }).start();
    }

    private synchronized void closeSocket(boolean notifyListener) {
        isConnected = false;
        if (input != null) {
            try {
                input.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
            input = null;
        }
        if (output != null) {
            try {
                output.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
            output = null;
        }
        if (socket != null) {
            try {
                socket.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
            socket = null;
        }
        mPort = 0;
        if (notifyListener && mStatusListener != null)
            mStatusListener.onDisconnected();
    }
}
