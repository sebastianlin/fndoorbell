package com.fusionnextinc.doorbell.widget;

import android.content.Context;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.SoundEffectConstants;
import android.view.View;
import android.widget.Checkable;
import android.widget.LinearLayout;

import com.fusionnextinc.doorbell.R;
import com.fusionnextinc.doorbell.utils.ViewUtil;


/**
 * Created by chocoyang on 2016/4/21.
 */
public class FNSwitch extends LinearLayout implements View.OnTouchListener, Checkable {
    private static final String TAG = "FNSwitch";
    private static final float SCALE = 256 / 420f;
    private static final int MOVE_PRE_DURATION = 30;

    private static final int MOVE_LEFT = 0;
    private static final int MOVE_RIGHT = 1;

    private static final int TOUCH_MODE_IDLE = 0;
    private static final int TOUCH_MODE_DOWN = 1;
    private static final int TOUCH_MODE_DRAGGING = 2;

    private OnCheckedChangeListener listener;
    private View thumb;
    private boolean isChecked = false;
    private float cacheX, maxTranslationX;
    private int movePreX;
    private ViewUtil viewUtil;
    private int thumbMode = TOUCH_MODE_IDLE;

    public interface OnCheckedChangeListener {
        void onCheckedChanged(FNSwitch fnSwitch, boolean isChecked);
    }

    public FNSwitch(Context context) {
        super(context);
        init();
    }

    public FNSwitch(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    private void init() {
        setLayoutDirection(LAYOUT_DIRECTION_LTR);
        setFocusableInTouchMode(true);
        setOnTouchListener(this);
        thumb = new View(getContext());
        thumb.setBackgroundResource(R.drawable.switch_thumb);
        thumb.setOnTouchListener(this);
        addView(thumb);
    }

    private void refreshView() {
        setBackgroundResource(isChecked ? R.drawable.switch_bg_on : R.drawable.switch_bg_off);
        thumb.setTranslationX(isChecked ? maxTranslationX : 0);
    }

    public void setOnCheckedChangeListener(OnCheckedChangeListener listener) {
        this.listener = listener;
    }

    @Override
    public void setChecked(boolean checked) {
        setChecked(checked, false);
    }

    @Override
    public boolean isChecked() {
        return isChecked;
    }

    @Override
    public void toggle() {
        setChecked(!isChecked, true);
    }

    private void setChecked(boolean checked, boolean runAnim) {
        this.isChecked = checked;
        handler.removeMessages(MOVE_RIGHT);
        handler.removeMessages(MOVE_LEFT);
        if (runAnim) {
            handler.sendEmptyMessage(isChecked ? MOVE_RIGHT : MOVE_LEFT);
        } else {
            refreshView();
        }
    }

    private Handler handler = new Handler(Looper.getMainLooper()) {
        @Override
        public void handleMessage(Message msg) {
            float x = thumb.getTranslationX();
            switch (msg.what) {
                case MOVE_LEFT:
                    if (x > movePreX) {
                        thumb.setTranslationX(x - movePreX);
                        removeMessages(MOVE_RIGHT);
                        removeMessages(MOVE_LEFT);
                        sendEmptyMessageDelayed(MOVE_LEFT, MOVE_PRE_DURATION);
                    } else {
                        if (isChecked) {
                            isChecked = false;
                            playSoundEffect(SoundEffectConstants.CLICK);
                            if (listener != null)
                                listener.onCheckedChanged(FNSwitch.this, false);
                        }
                        refreshView();
                    }
                    break;
                case MOVE_RIGHT:
                    if (x < maxTranslationX - movePreX) {
                        thumb.setTranslationX(x + movePreX);
                        removeMessages(MOVE_RIGHT);
                        removeMessages(MOVE_LEFT);
                        sendEmptyMessageDelayed(MOVE_RIGHT, MOVE_PRE_DURATION);
                    } else {
                        if (!isChecked) {
                            isChecked = true;
                            playSoundEffect(SoundEffectConstants.CLICK);
                            if (listener != null)
                                listener.onCheckedChanged(FNSwitch.this, true);
                        }
                        refreshView();
                    }
                    break;
            }
        }
    };

    @Override
    public boolean onTouch(View v, MotionEvent event) {
        switch (event.getAction()) {
            case MotionEvent.ACTION_DOWN:
                viewUtil = new ViewUtil(v.getContext());
                viewUtil.setTouchEvent(event);
                if (v == thumb) {
                    thumbMode = TOUCH_MODE_DOWN;
                    cacheX = event.getRawX() - v.getTranslationX();
                }
                break;
            case MotionEvent.ACTION_MOVE:
                viewUtil.setTouchEvent(event);
                if (thumbMode == TOUCH_MODE_DOWN) {
                    getParent().requestDisallowInterceptTouchEvent(true);
                    thumbMode = TOUCH_MODE_DRAGGING;
                } else if (thumbMode == TOUCH_MODE_DRAGGING) {
                    float x = event.getRawX() - cacheX;
                    if (x < 0) {
                        v.setTranslationX(0);
                    } else if (x > maxTranslationX) {
                        v.setTranslationX(maxTranslationX);
                    } else {
                        v.setTranslationX(x);
                    }
                }
                break;
            case MotionEvent.ACTION_UP:
                int trackWidth = getWidth();
                int thumbWidth = thumb.getWidth();
                if (!viewUtil.isXMoved()) {
                    handler.sendEmptyMessage(thumb.getX() > (trackWidth / 2) - (thumbWidth / 2) ? MOVE_LEFT : MOVE_RIGHT);
                } else {
                    handler.sendEmptyMessage(thumb.getX() < (trackWidth / 2) - (thumbWidth / 2) ? MOVE_LEFT : MOVE_RIGHT);
                }
                thumbMode = TOUCH_MODE_IDLE;
                break;
        }
        return true;
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        if (MeasureSpec.getMode(widthMeasureSpec) == MeasureSpec.EXACTLY
                && MeasureSpec.getMode(heightMeasureSpec) == MeasureSpec.EXACTLY) {
            int width = MeasureSpec.getSize(widthMeasureSpec);
            int height = MeasureSpec.getSize(heightMeasureSpec);
            LayoutParams thumbParams = (LayoutParams) thumb.getLayoutParams();
            int thumbWidth = (int) (width * SCALE);
            if (thumbParams.height != height || thumbParams.width != thumbWidth) {
                thumbParams.height = height;
                thumbParams.width = thumbWidth;
                maxTranslationX = width - thumbWidth;
                movePreX = width / 4;
                thumb.setLayoutParams(thumbParams);
                refreshView();
            }
        }
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);
    }
}
