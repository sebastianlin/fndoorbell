package com.fusionnext.cloud.datalink.socketIo;


import android.content.Context;
import android.util.Log;

import com.fusionnext.cloud.fusionapi.Public;
import com.fusionnext.cloud.utils.Times;

import org.json.JSONException;
import org.json.JSONObject;

import java.net.URISyntaxException;
import java.util.ArrayList;

import io.socket.client.IO;
import io.socket.client.Socket;
import io.socket.emitter.Emitter;


public class SocketComm {
    private static final String TAG = "SocketComm";
    private static final String USER_ID = "user_id";
    private static final String CONTENT = "content";
    private static final String POINT = "point";
    private static final String TRACK_AT = "tracked_at";
    private static final String LATITUDE = "lat";
    private static final String LONGITUDE = "lng";
    private static final String LOCATION = "point";
    private static final String MESSAGE = "message";
    private static final String TIME = "time";
    private static final String PHOTO = "photo";
    private static final String FILENAME = "filename";
    private static final String CAPTURED_AT = "captured_at";
    private static final String ID = "id";

    private static final int SOCKETCOMM_JOIN = 0;
    private static final int SOCKETCOMM_CONNECT = 1;
    private static final int SOCKETCOMM_DISCONNECT = 2;
    private static final int SOCKETCOMM_LOCATION = 3;
    private static final int SOCKETCOMM_RECV_PHOTO = 4;
    private static final int SOCKETCOMM_MESSAGE = 5;
    private static final int SOCKETCOMM_LEAVE = 6;

    private static SocketComm instance;
    private Socket socket;
    private Context context;
    private final ArrayList<SocketCommListener> handlerArray = new ArrayList<SocketCommListener>();
    private static String curGroupId = null;

    public static SocketComm getInstance(Context context, String token, String groupId) {
        synchronized (SocketComm.class) {
            if (instance == null) {
                instance = new SocketComm(context.getApplicationContext(), token, groupId);
            }
        }
        return instance;
    }

    public SocketComm(Context context, String token, String groupId) {
        this.context = context;
        curGroupId = groupId;
        try {
            IO.Options opts = new IO.Options();
            opts.forceNew = true;
            opts.query = "token=" + token;//getCommToken();
            this.socket = IO.socket(Public.REAL_TIME_COMMUNICATION, opts);
            if (socket != null) {
                socket.on(Socket.EVENT_CONNECT, new Emitter.Listener() {

                    @Override
                    public void call(Object... args) {
                        Log.i(TAG, "socket is connected");
                        JSONObject obj = new JSONObject();
                        if (obj != null && curGroupId != null) {
                            try {
                                int groupId = Integer.parseInt(curGroupId);
                                obj.put("group_id", groupId);
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                            socket.emit("join", obj);
                            sendEventToUI(SOCKETCOMM_CONNECT, -1, null, null, null, null, null, null);
                        }
                    }

                }).on("join", new Emitter.Listener() {

                    @Override
                    public void call(Object... args) {
                        int userId = 0;
                        JSONObject obj = (JSONObject) args[0];
                        if (obj != null) {
                            try {
                                userId = obj.getInt(USER_ID);
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }

                            sendEventToUI(SOCKETCOMM_JOIN, userId, null, null, null, null, null, null);
                        }
                        Log.i(TAG, "join: " + userId);

                    }

                }).on("leave", new Emitter.Listener() {

                    @Override
                    public void call(Object... args) {

                        int userId = (int) args[0];

                        sendEventToUI(SOCKETCOMM_LEAVE, userId, null, null, null, null, null, null);
                        Log.i(TAG, "leave: " + userId);
                    }

                }).on(MESSAGE, new Emitter.Listener() {

                    @Override
                    public void call(Object... args) {
                        int userId = 0;
                        String msg = null;
                        String time = null;

                        JSONObject obj = (JSONObject) args[0];
                        if (obj != null) {
                            try {
                                userId = obj.getInt(USER_ID);
                                time = obj.getString(TIME);

                                JSONObject msgObj = obj.getJSONObject(MESSAGE);
                                if (msgObj != null) {
                                    msg = msgObj.getString(CONTENT);
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                            sendEventToUI(SOCKETCOMM_MESSAGE, userId, null, time, null, null, null, msg);
                        }
                        Log.i(TAG, "message: " + msg + ", user_id: " + userId);
                    }

                }).on(LOCATION, new Emitter.Listener() {

                    @Override
                    public void call(Object... args) {
                        int userId = 0;
                        String time = null;
                        String latitude = null;
                        String longitude = null;

                        JSONObject obj = (JSONObject) args[0];
                        if (obj != null) {
                            try {
                                userId = obj.getInt(USER_ID);
                                JSONObject pointObj = obj.getJSONObject(POINT);
                                if (pointObj != null) {
                                    time = pointObj.getString(TRACK_AT);
                                    latitude = pointObj.getString(LATITUDE);
                                    longitude = pointObj.getString(LONGITUDE);
                                }

                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                            sendEventToUI(SOCKETCOMM_LOCATION, userId, null, time, latitude, longitude, null, null);
                        }
                        Log.i(TAG, "location: latitude = " + latitude + ", longitude = " + longitude + ", at " + time);
                    }

                }).on(PHOTO, new Emitter.Listener() {

                    @Override
                    public void call(Object... args) {
                        int userId = 0;
                        String photoID = null;
                        String time = null;
                        String latitude = null;
                        String longitude = null;
                        String fileName = null;

                        JSONObject obj = (JSONObject) args[0];
                        if (obj != null) {
                            try {
                                userId = obj.getInt(USER_ID);
                                JSONObject photoObj = obj.getJSONObject(PHOTO);
                                if (photoObj != null) {
                                    time = photoObj.getString(CAPTURED_AT);
                                    latitude = photoObj.getString(LATITUDE);
                                    longitude = photoObj.getString(LONGITUDE);
                                    fileName = photoObj.getString(FILENAME);
                                    photoID = photoObj.getString(ID);
                                }

                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                            sendEventToUI(SOCKETCOMM_RECV_PHOTO, userId, photoID, time, latitude, longitude, fileName, null);
                        }
                        Log.i(TAG, "photo: latitude = " + latitude + ", longitude = " + longitude + ", at " + time);
                    }

                }).on(Socket.EVENT_DISCONNECT, new Emitter.Listener() {

                    @Override
                    public void call(Object... args) {
                        Log.i(TAG, "socket is disconnected");
                        sendEventToUI(SOCKETCOMM_DISCONNECT, -1, null, null, null, null, null, null);
                    }

                });
            }
        } catch (URISyntaxException e) {
            Log.i("TAG", "error connecting to socket");
            e.printStackTrace();
        }
    }

    public void connectSocketComm() {
        if (socket != null && curGroupId != null) {
            if (!socket.connected()) {
                socket.connect();
            }
        }
    }

    public void closeSocketComm() {
        if (socket != null) {
            socket.off();
            socket.disconnect();
            instance = null;
        }
    }

    public void sendLocation(String latitude, String longitude) {
        if (socket != null && latitude != null && longitude != null) {
            JSONObject obj = new JSONObject();
            JSONObject pointObj = null;
            if (obj != null) {
                try {
                    pointObj = new JSONObject();
                    if (pointObj != null) {
                        pointObj.put(LATITUDE, latitude);
                        pointObj.put(LONGITUDE, longitude);
                        pointObj.put(TRACK_AT, Times.nowTime(Times._TIME_FORMAT_STANDARD));
                    }
                    //obj.put(POINT, pointObj);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                socket.emit(LOCATION, pointObj);
            }
        }
    }

    public void sendMsg(String msg) {
        if (socket != null && msg != null) {
            JSONObject obj = new JSONObject();
            if (obj != null) {
                try {
                   /* JSONObject pointObj = new JSONObject();
                    if (pointObj != null) {
                        pointObj.put(LATITUDE, latitude);
                        pointObj.put(LONGITUDE, longitude);
                        pointObj.put(TIME, Times.nowTime(Times._TIME_FORMAT_STANDARD));
                    }*/
                    /*JSONObject msgObj = new JSONObject();
                    if (msgObj != null) {
                        msgObj.put(CONTENT, msg);
                    }
                                       obj.put(MESSAGE, msgObj);*/
                    obj.put(TIME, Times.nowTime(Times._TIME_FORMAT_STANDARD));
                    obj.put(CONTENT, msg);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                socket.emit(MESSAGE, obj);
            }
        }
    }

    public boolean sendPhoto(String id, String filename, String lat, String lng, String captured_at) {
        if (socket != null) {
            try {
                JSONObject jsonObject = new JSONObject();
                if (id != null)
                    jsonObject.put("id", id);
                if (filename != null)
                    jsonObject.put("filename", filename);
                if (lat != null)
                    jsonObject.put("lat", lat);
                if (lng != null)
                    jsonObject.put("lng", lng);
                if (captured_at != null)
                    jsonObject.put("captured_at", captured_at);
                socket.emit("photo", jsonObject);
                return true;
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return false;
    }

    public void addHandler(SocketCommListener handler) {
        if (handlerArray.contains(handler)) {
            return;
        }
        handlerArray.add(handler);
    }

    public void removeHandler(SocketCommListener handler) {
        handlerArray.remove(handler);
    }

    public ArrayList<SocketCommListener> getHandlerArray() {
        return handlerArray;
    }


    private void sendEventToUI(int type, int userId, String photoId, String time, String latitude, String longitude, String fileName, String msg) {
        ArrayList<SocketCommListener> handlerArray = getHandlerArray();

        if (handlerArray == null) {
            return;
        }

        for (int i = 0; i < handlerArray.size(); i++) {
            if (handlerArray.get(i) != null) {
                switch (type) {
                    case SOCKETCOMM_JOIN:
                        handlerArray.get(i).onJoin(userId);
                        break;
                    case SOCKETCOMM_CONNECT:
                        handlerArray.get(i).onConnect();
                        break;
                    case SOCKETCOMM_DISCONNECT:
                        handlerArray.get(i).onDisconnect();
                        break;
                    case SOCKETCOMM_LOCATION:
                        handlerArray.get(i).onLocation(userId, time, latitude, longitude);
                        break;
                    case SOCKETCOMM_RECV_PHOTO:
                        handlerArray.get(i).onRecvPhoto(userId, photoId, time, latitude, longitude, fileName);
                        break;
                    case SOCKETCOMM_MESSAGE:
                        handlerArray.get(i).onMessage(userId, msg, time);
                        break;
                    case SOCKETCOMM_LEAVE:
                        handlerArray.get(i).onLeave(userId);
                        break;
                    default:
                        break;
                }
            }
        }
    }

    public void setCurGroupId(String groupId) {
        curGroupId = groupId;
    }

}
