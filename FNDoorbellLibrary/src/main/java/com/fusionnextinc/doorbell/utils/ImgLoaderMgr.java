package com.fusionnextinc.doorbell.utils;

import android.content.Context;
import android.graphics.Bitmap;
import android.os.Environment;
import android.widget.ImageView;

import com.fusionnextinc.doorbell.MyApplication;
import com.fusionnextinc.doorbell.R;
import com.fusionnextinc.doorbell.manager.data.DataManager;
import com.nostra13.universalimageloader.cache.disc.impl.UnlimitedDiskCache;
import com.nostra13.universalimageloader.cache.disc.naming.Md5FileNameGenerator;
import com.nostra13.universalimageloader.cache.memory.impl.LruMemoryCache;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.assist.ImageScaleType;
import com.nostra13.universalimageloader.core.assist.QueueProcessingType;
import com.nostra13.universalimageloader.core.display.RoundedBitmapDisplayer;
import com.nostra13.universalimageloader.utils.MemoryCacheUtils;

import java.io.File;

/**
 * Created by Mike Chang on 2016/3/9.
 */
public class ImgLoaderMgr {
    private final static String synchronizedTagImageLoader = "synchronizedTagImageLoader";
    //ImageLoader
    private static ImageLoader imageLoader = null;

    public static ImageLoader getImageLoader(Context context) {
        return getInstance(context);
    }

    private static ImageLoader getInstance(Context context) {
        synchronized (synchronizedTagImageLoader) {
            context = context.getApplicationContext();
            if (imageLoader == null) {
                ImageLoaderConfiguration config = ImgLoaderMgr.initConfig(context);
                ImageLoader.getInstance().init(config);
                ImageLoader loader = ImageLoader.getInstance();
                setImageLoader(loader);
            }

            if (!imageLoader.isInited()) {
                ImageLoaderConfiguration config = ImgLoaderMgr.initConfig(context);
                ImageLoader.getInstance().init(config);
            }

            return imageLoader;
        }
    }

    private static void setImageLoader(ImageLoader loader) {
        synchronized (synchronizedTagImageLoader) {
            imageLoader = loader;
        }
    }

    public static File cacheDir(Context context) {
        File cacheDir;
        //Find the dir to save cached images
        boolean useExternalSdAsCache = DataManager.getInstance(context).loadUseExternalSdCache(false);
        if (useExternalSdAsCache) {
            if (Environment.getExternalStorageState().equals(Environment.MEDIA_MOUNTED))
                cacheDir = new File(Environment.getExternalStorageDirectory(), MyApplication.Defines._APP_SD_CACHE_DIR);
            else
                cacheDir = context.getCacheDir();
        } else {
            cacheDir = context.getCacheDir();
        }

        if (!cacheDir.exists()) cacheDir.mkdirs();

        return cacheDir;
    }

    public static ImageLoaderConfiguration initConfig(Context context) {
        // Create global configuration and initialize ImageLoader with this config
        return new ImageLoaderConfiguration.Builder(context)
                .memoryCacheExtraOptions(400, 600)      //If not set, default is device screen dimensions
                .diskCacheExtraOptions(400, 600, null)
                .threadPriority(Thread.NORM_PRIORITY - 1)
                .tasksProcessingOrder(QueueProcessingType.LIFO)
                .denyCacheImageMultipleSizesInMemory()
                .diskCacheFileNameGenerator(new Md5FileNameGenerator())
                .defaultDisplayImageOptions(initDisplay())
                .memoryCache(new LruMemoryCache(20 * 1024 * 1024))
                .memoryCacheSize(20 * 1024 * 1024)
                .diskCache(new UnlimitedDiskCache(cacheDir(context)))
                .diskCacheSize(128 * 1024 * 1024)
                .diskCacheFileCount(2000)
                .writeDebugLogs()
                .build();
    }

    public static DisplayImageOptions initDisplay() {
        return new DisplayImageOptions.Builder()
                .showImageOnLoading(R.drawable.default_image_background)
                .showImageOnFail(R.drawable.default_image_background)
                .showImageForEmptyUri(R.drawable.default_image_background)
                .delayBeforeLoading(0)
                .cacheInMemory(true)
                .cacheOnDisk(true)
                .considerExifParams(true)
                .imageScaleType(ImageScaleType.IN_SAMPLE_POWER_OF_2)
                .bitmapConfig(Bitmap.Config.RGB_565)
                .build();
    }

    public static void initImageLoader(Context context) {
        getImageLoader(context);
    }

    public static void dismissLoader() {
        ImageLoader loader = ImageLoader.getInstance();
        if (loader != null) {
            loader.clearMemoryCache();
            loader.destroy();
        }
    }

    public static void displayRoundImage(String uri, ImageView imageView, int radius) {

        DisplayImageOptions display = new DisplayImageOptions.Builder()
                .showImageOnLoading(R.drawable.default_image_background)
                .showImageOnFail(R.drawable.default_image_background)
                .showImageForEmptyUri(R.drawable.default_image_background)
                        //.delayBeforeLoading(0)
                .cacheInMemory(true)
                .cacheOnDisk(true)
                .considerExifParams(true)
                .imageScaleType(ImageScaleType.IN_SAMPLE_POWER_OF_2)
                .bitmapConfig(Bitmap.Config.ARGB_8888)
                .displayer(new RoundDisplayer(radius))
                .build();
        ImageLoader loader = ImageLoader.getInstance();
        if (loader != null) {
            loader.displayImage(uri, imageView, display);
        }
    }

    public static void displayShapeImage(String uri, ImageView imageView, int radius, boolean cache) {

        DisplayImageOptions display = new DisplayImageOptions.Builder()
                .showImageOnLoading(R.drawable.default_image_background)
                .showImageOnFail(R.drawable.default_image_background)
                .showImageForEmptyUri(R.drawable.default_image_background)
                        //.delayBeforeLoading(0)
                .cacheInMemory(cache)
                .cacheOnDisk(cache)
                .considerExifParams(true)
                .imageScaleType(ImageScaleType.IN_SAMPLE_POWER_OF_2)
                .bitmapConfig(Bitmap.Config.ARGB_8888)
                .displayer(new RoundedBitmapDisplayer(radius))
                .build();
        ImageLoader loader = ImageLoader.getInstance();
        if (!cache) {
            File imageFile = loader.getDiscCache().get(uri);
            if (imageFile.exists()) {
                imageFile.delete();
            }
            MemoryCacheUtils.removeFromCache(uri, loader.getMemoryCache());
        }
        if (loader != null) {
            loader.displayImage(uri, imageView, display);
        }
    }

    public static void displayShapeImage(String uri, ImageView imageView, int radius) {
        displayShapeImage(uri, imageView, radius, true);
    }
}

