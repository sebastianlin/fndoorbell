package com.fusionnextinc.doorbell.manager.account;

import android.app.AppOpsManager;
import android.app.NotificationManager;
import android.content.Context;
import android.content.pm.ApplicationInfo;
import android.os.Build;
import androidx.core.app.NotificationManagerCompat;

import com.fusionnext.cloud.iot.Authorization;
import com.fusionnext.cloud.iot.Devices;
import com.fusionnext.cloud.iot.IOTHandler;
import com.fusionnext.cloud.iot.PushTokens;
import com.fusionnext.cloud.iot.User;
import com.fusionnext.cloud.iot.objects.Device;
import com.fusionnext.cloud.iot.objects.ErrorObj;
import com.fusionnext.cloud.iot.objects.PushToken;
import com.fusionnext.cloud.iot.objects.Thing;
import com.fusionnext.cloud.iot.objects.UserProfile;
import com.fusionnextinc.doorbell.manager.data.DataManager;
import com.fusionnextinc.doorbell.objects.FNError;
import com.fusionnextinc.doorbell.objects.FNUserInfo;
import com.fusionnextinc.doorbell.utils.FNLog;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;

/**
 * Created by Mike Chang on 2017/6/5.
 */
public class AccountManager {
    private final static String TAG = "AccountManager";
    private final static String ORGANIZATION_ID = "1";
    private static AccountManager instance;
    private Authorization authorization;
    private User user;
    private PushTokens pushTokens;
    private FNUserInfo fnUserInfo;
    private AaccountHandler.AccountListener accountListener;
    private Context context;

    public static AccountManager getInstance(Context context) {
        if (instance == null) {
            synchronized (AccountManager.class) {
                if (instance == null) {
                    instance = new AccountManager(context);
                }
            }
        }
        return instance;
    }

    private AccountManager(Context context) {
        this.context = context;
        authorization = new Authorization(context);
        authorization.activiateAPI(onAuthorizationListener);
        updateUserInfo();
        pushTokens = new PushTokens(context);
    }

    private void updateUserInfo() {
        user = new User(context);
        fnUserInfo = new FNUserInfo();
        fnUserInfo.setUserAccount(user.getUserProfile()._email);
        fnUserInfo.setUserName(user.getUserProfile()._name);
    }

    public void setUserAccount(String account) {
        if (fnUserInfo != null) {
            fnUserInfo.setUserAccount(account);
        }
    }

    public String getUserAccount() {
        updateUserInfo();
        if (fnUserInfo != null) {
            return fnUserInfo.getUserAccount();
        }
        return null;
    }

    public void setUserPassword(String password) {
        if (fnUserInfo != null) {
            fnUserInfo.setUserPassword(password);
        }
    }

    public void setUserName(String name) {
        if (fnUserInfo != null) {
            fnUserInfo.setUserName(name);
        }
    }

    public String getUserName() {
        updateUserInfo();
        if (fnUserInfo != null) {
            return fnUserInfo.getUserName();
        }
        return null;
    }

    public void setFirebaseToken(String firebaseToken) {
        if (fnUserInfo != null) {
            fnUserInfo.setFirebaseToken(firebaseToken);
        }
    }

    public void setAccountListener(AaccountHandler.AccountListener accountListener) {
        this.accountListener = accountListener;
    }

    public boolean userLogin() {
        if (authorization != null && fnUserInfo != null) {
            PushToken pushToken = new PushToken();
            pushToken._pushToken = fnUserInfo.getFirebaseToken();
            pushToken._type = PushToken.LIKES_STRING_TYPE_FIREBASE;
            authorization.login(fnUserInfo.getUserAccount(), fnUserInfo.getUserPassword(), pushToken, onAuthorizationListener);
            return true;
        }
        return false;
    }

    public boolean userRegister() {
        if (user != null && fnUserInfo != null) {
            UserProfile userProfile = new UserProfile();

            userProfile._organizationId = ORGANIZATION_ID;
            userProfile._name = fnUserInfo.getUserName();
            userProfile._email = fnUserInfo.getUserAccount();
            user.createUser(userProfile, fnUserInfo.getUserPassword(), onUsersListener);
            return true;
        }
        return false;
    }


    public boolean userResend(String email ) {
        if (user != null && fnUserInfo != null) {
            UserProfile userProfile = new UserProfile();

            userProfile._organizationId = ORGANIZATION_ID;
            userProfile._name = fnUserInfo.getUserName();
            userProfile._email = email;
            user.resendUser(userProfile,onUsersListener);
            return true;
        }
        return false;
    }
    public boolean getUserInfo() {
        if (user != null && fnUserInfo != null) {
            updateUserInfo();
            user.getUser(user.getUserProfile()._userId, onUsersListener);
            return true;
        }
        return false;
    }

    public boolean updatePushTokn() {
        if (pushTokens != null) {
            PushToken pushToken = new PushToken();
            pushToken._pushToken = DataManager.getInstance(context).loadFireBasePushToken();
            pushToken._type = PushToken.LIKES_STRING_TYPE_FIREBASE;

            Devices devices = new Devices(context);
            devices.createDevice(pushToken, onDevicesListener);
            return true;
        }
        return false;
    }

    public boolean userLogout() {
        if (authorization != null && fnUserInfo != null) {
            authorization.logout(onAuthorizationListener);
            fnUserInfo.clean();
            return true;
        }
        return false;
    }

    public boolean resetUserPassword(String email) {
        if (user != null && fnUserInfo != null) {
            user.resetPassword(email, onUsersListener);
            fnUserInfo.clean();
            return true;
        }
        return false;
    }

    public boolean changUserPassword(String oldPassword, String newPassword) {
        if (user != null && fnUserInfo != null) {
            user.updateUser(null, oldPassword, newPassword, onUsersListener);
            return true;
        }
        return false;
    }

    public boolean changUserName(String name) {
        if (user != null && fnUserInfo != null) {
            user.updateUser(name, null, null, onUsersListener);
            return true;
        }
        return false;
    }

    public boolean setUserPassword(String account, String password, String code) {
        if (user != null && fnUserInfo != null) {
            user.setPassword(account, password, code, onUsersListener);
            fnUserInfo.clean();
            return true;
        }
        return false;
    }

    public boolean isLogin() {
        return authorization != null && authorization.isLogin();
    }

    private final IOTHandler.OnAuthorizationListener onAuthorizationListener = new IOTHandler.OnAuthorizationListener() {
        @Override
        public void onActivateAPI(boolean result, ErrorObj error) {
            FNLog.d(TAG, "IOT SDK is activated");
        }

        @Override
        public void onLogin(boolean result, ErrorObj error) {
            if (accountListener != null) {
                accountListener.onUserLogined(instance, result, FNError.parseError(error));
            }
        }

        @Override
        public void onLogout(boolean result, ErrorObj error) {
            if (result) {
                DataManager.getInstance(context).resetCacheData();
            }
            if (accountListener != null) {
                accountListener.onUserLogout(instance, result, FNError.parseError(error));
            }
        }
    };

    private final IOTHandler.OnUsersListener onUsersListener = new IOTHandler.OnUsersListener() {

        @Override
        public void onResult(boolean result, String id, int postType, ErrorObj error) {

        }

        @Override
        public void onRsendUser(boolean result, ErrorObj error) {

        }

        @Override
        public void onCreateUser(boolean result, ErrorObj error) {
            if (accountListener != null) {
                accountListener.onUserRegistered(instance, result, FNError.parseError(error));
            }
        }

        @Override
        public void onGetUser(boolean result, ErrorObj error) {
            if (result && fnUserInfo != null && user != null) {
                fnUserInfo.setUserAccount(user.getUserProfile()._email);
                fnUserInfo.setUserName(user.getUserProfile()._name);
            }
            if (accountListener != null) {
                accountListener.onGetUserInfo(instance, result, FNError.parseError(error));
            }
        }

        @Override
        public void onUpdateUser(boolean result, ErrorObj error) {
            if (result && fnUserInfo != null && user != null) {
                fnUserInfo.setUserAccount(user.getUserProfile()._email);
                fnUserInfo.setUserName(user.getUserProfile()._name);
            }
            if (accountListener != null) {
                accountListener.onUpdateUserInfo(instance, result, FNError.parseError(error));
            }
        }

        @Override
        public void onListAllThings(boolean b, String s, ArrayList<Thing> arrayList, ArrayList<Thing> arrayList1, ArrayList<Thing> arrayList2, ErrorObj errorObj) {

        }

        @Override
        public void onListOwnedThings(boolean result, String userId, ArrayList<Thing> arrayList, ErrorObj error) {

        }

        @Override
        public void onListInvitedThings(boolean result, String userId, ArrayList<Thing> arrayList, ErrorObj error) {

        }

        @Override
        public void onListSharedThings(boolean result, String userId, ArrayList<Thing> arrayList, ErrorObj error) {

        }

        @Override
        public void onResetPassword(boolean result, ErrorObj error) {
            if (accountListener != null) {
                accountListener.onResetPassword(instance, result, FNError.parseError(error));
            }
        }

        @Override
        public void onSetPassword(boolean result, ErrorObj error) {
            if (accountListener != null) {
                accountListener.onSetPassword(instance, result, FNError.parseError(error));
            }
        }
    };

    private final IOTHandler.OnDevicesListener onDevicesListener = new IOTHandler.OnDevicesListener() {

        @Override
        public void onResult(boolean result, String id, int postType, ErrorObj error) {

        }

        @Override
        public void onCreateDevice(boolean result, Device device, ErrorObj error) {

        }

        @Override
        public void onUpdateDevice(boolean result, Device device, ErrorObj error) {

        }

        @Override
        public void onDeleteDevice(boolean result, ErrorObj error) {

        }
    };

    public boolean isNotificationEnabled(Context context) {
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.KITKAT) {
            return NotificationManagerCompat.from(context).areNotificationsEnabled();
        }
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.O) {
            return isEnableV19(context);
        } else {
            return isEnableV26(context);
        }
    }

    private static boolean isEnableV19(Context context) {
        final String CHECK_OP_NO_THROW = "checkOpNoThrow";
        final String OP_POST_NOTIFICATION = "OP_POST_NOTIFICATION";
        AppOpsManager mAppOps = (AppOpsManager) context.getSystemService(Context.APP_OPS_SERVICE);
        ApplicationInfo appInfo = context.getApplicationInfo();
        String pkg = context.getApplicationContext().getPackageName();
        int uid = appInfo.uid;
        Class appOpsClass = null; /* Context.APP_OPS_MANAGER */
        try {
            appOpsClass = Class.forName(AppOpsManager.class.getName());
            Method checkOpNoThrowMethod = appOpsClass.getMethod(CHECK_OP_NO_THROW, Integer.TYPE, Integer.TYPE, String.class);
            Field opPostNotificationValue = appOpsClass.getDeclaredField(OP_POST_NOTIFICATION);
            int value = (int) opPostNotificationValue.get(Integer.class);
            return ((int) checkOpNoThrowMethod.invoke(mAppOps, value, uid, pkg) == AppOpsManager.MODE_ALLOWED);
        } catch (ClassNotFoundException e) {
        } catch (NoSuchMethodException e) {
        } catch (NoSuchFieldException e) {
        } catch (InvocationTargetException e) {
        } catch (IllegalAccessException e) {
        } catch (Exception e) {
        }
        return false;
    }

    private static boolean isEnableV26(Context context) {
        ApplicationInfo appInfo = context.getApplicationInfo();
        String pkg = context.getApplicationContext().getPackageName();
        int uid = appInfo.uid;
        try {
            NotificationManager notificationManager = (NotificationManager)
                    context.getSystemService(Context.NOTIFICATION_SERVICE);
            Method sServiceField = notificationManager.getClass().getDeclaredMethod("getService");
            sServiceField.setAccessible(true);
            Object sService = sServiceField.invoke(notificationManager);

            Method method = sService.getClass().getDeclaredMethod("areNotificationsEnabledForPackage"
                    , String.class, Integer.TYPE);
            method.setAccessible(true);
            return (boolean) method.invoke(sService, pkg, uid);
        } catch (Exception e) {
            return true;
        }
    }
}