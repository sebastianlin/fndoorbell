package com.iptnet.common.c2c;

import java.util.concurrent.CopyOnWriteArraySet;

import com.iptnet.c2c.C2CHandle;
import com.iptnet.c2c.C2CHandle.MediaStreamCallback;
import com.iptnet.c2c.C2CHandle.ProtocolMessageCallback;
import com.iptnet.c2c.C2CHandle.StreamCommandCallback;
import com.iptnet.c2c.C2CHandle.StreamStateCallback;
import com.iptnet.c2c.csbc.CSBCHandle;
import com.iptnet.c2c.csbc.CSBCHandle.InfoCB;
import com.iptnet.c2c.csbc.CSBCHandle.InfoCallback;
import com.iptnet.c2c.csbc.CSBCHandle.ServiceAddress;
import com.iptnet.c2c.csbc.CSBCHandle.ServiceAddressGroup;
import com.iptnet.common.Peer;

import android.os.Bundle;
import androidx.annotation.NonNull;
import android.util.Log;
import android.util.SparseArray;

/**
 * Use to handle easy C2C SDK.
 * @author HiKARi
 */
public class C2CProcess implements 
	ProtocolMessageCallback, StreamCommandCallback, StreamStateCallback, MediaStreamCallback, InfoCallback {
	private static final boolean DEBUG = true;
	private static final String TAG = C2CProcess.class.getSimpleName();
	private static C2CProcess mC2CProcess;
	
	private C2CHandle mC2CHandle;
	private boolean mSdkInit;
	private CopyOnWriteArraySet<ProtocolMessageCallback> mProtocolCommandCallbackSet;
	private MediaStreamCallback mMediaStreamCallback;
	private CopyOnWriteArraySet<StreamCommandCallback> mStreamCommandCallbackSet;
	private String mRegisterServer, mRegisterUid, mRegisterPassword;
	private CopyOnWriteArraySet<Object[]> mProtocolMessageCallbackSet;

	private static boolean DEBUG_SHOWN;

    public static final int P2P_STATUS_IDLE = 0;
    public static final int P2P_STATUS_ACCEPT = 1;
    public static final int P2P_STATUS_SETUP_DONE = 2;
    public static final int P2P_STATUS_DEVICE_CONNECTED = 3;
    public static final int P2P_STATUS_DEVICE_DISCONNECTED = 4;

    private static int mP2PServerStatus = P2P_STATUS_IDLE;

	// command
	private static class RespCmdParam {
		boolean submit = false;
		int lineId, event, subEvent;
		String peerId, msg;
	}
	
	public static class BaseInfo {

		int lineId;
		String peerId;
		
		BaseInfo(int lineId, String peerId) {
			this.lineId = lineId; this.peerId = peerId;
		}
		
		public int getLineId() {
			return lineId;
		}
		
		public String getPeerId() {
			return peerId;
		}
	}
	
	public static class DeviceInfo {

		BaseInfo base;
		int sequence, channel, capability, device_type;
		String tag, vendor, model, fwVer, libVer;
		
		DeviceInfo() {
	
		}
		
		public BaseInfo getBaseInfo() {
			return base;
		}
		
		public String getTag() {
			return tag;
		}
		
		public int getSequence() {
			return sequence;
		}
		
		public int getChannel() {
			return channel;
		}
		
		public int getCapability() {
			return capability;
		}
		
		public String getVendor() {
			return vendor;
		}
		
		public String getModel() {
			return model;
		}
		
		public String getFWVersion() {
			return fwVer;
		}
		
		public String getLIBVersion() {
			return libVer;
		}
		
		public int getDeviceType() {
			return device_type;
		}
	}
	
	public static class UpgradeFW {
		
		BaseInfo base;
		int sequence, channel, result, expires, code;
		String tag;
		
		UpgradeFW() {

		}
		
		public BaseInfo getBaseInfo() {
			return base;
		}
		
		public String getTag() {
			return tag;
		}
		
		public int getSequence() {
			return sequence;
		}
		
		public int getChannel() {
			return channel;
		}
		
		public int getResult() {
			return result;
		}
		
		public int getCode() {
			return code;
		}
		
		public int getExpires() {
			return expires;
		}
	}
	
	private RespCmdParam mFWUpgradeParam, mDevInfoParam;
	
	// CSBC
	private CSBCHandle mCSBCHandle;
	
	private static class CBInfoParam {
		public static class OTA {
			boolean submit;
			String host; int port;
		}
	}
	
	private CBInfoParam.OTA mCBInfoOtaParam;
	
	// OTA
//	private class OTAParam {
//		
//		String domain, account, version, module;
//	}
//
//	private OTAParam mOTAParam;
	
	public static class C2CException extends Exception {

		private static final long serialVersionUID = -6727373023646123941L;
		int mFailCode, mEventCode, mSubEventCode;
		
		public C2CException(String msg, int failCode, int eventCode, int subEventCode) {
			super(msg);
			mFailCode = failCode;
			mEventCode = eventCode;
			mSubEventCode = subEventCode;
		}
	}
	
	public static class C2CConnectException extends C2CException {

		private static final long serialVersionUID = -7752803469842679686L;

		public C2CConnectException(String msg, int failCode) {
			super(msg, failCode, Integer.MIN_VALUE, Integer.MIN_VALUE);
		}

		public int getFailCode() {
			return mFailCode;
		}
	}
	
	public static class C2CCommandException extends C2CException {

		private static final long serialVersionUID = 1446658263010717975L;

		public C2CCommandException(String msg, int eventCode, int subEventCode) {
			super(msg, Integer.MIN_VALUE, eventCode, subEventCode);
		}
		
		public int getEventCode() {
			return mEventCode;
		}
		
		public int getSubEventCode() {
			return mSubEventCode;
		}
	}
	
	public static class C2CParserException extends Exception {
		
		private static final long serialVersionUID = -7489619470840286702L;

		public C2CParserException(String msg) {
			super(msg);
		}
	}
	
	private C2CProcess() {
		mC2CHandle = C2CHandle.getInstance();
		mProtocolMessageCallbackSet = new CopyOnWriteArraySet<>();
		mProtocolCommandCallbackSet = new CopyOnWriteArraySet<>();
		mStreamCommandCallbackSet = new CopyOnWriteArraySet<>();
	}
	
	public static final C2CProcess getInstance() {
		if (mC2CProcess == null) {
			mC2CProcess = new C2CProcess();
		}
		return mC2CProcess;
	}
	
	public String getRegisterServer() {
		if (mRegisterServer == null) mRegisterServer = "";
		return mRegisterServer;
	}
	
	public C2CProcess setRegisterServer(String server) {
		if (server == null) server = "";
		mRegisterServer = server;
		return this;
	}
	
	public String getRegisterUid() {
		if (mRegisterUid == null) mRegisterUid = "";
		return mRegisterUid;
	}
	
	public C2CProcess setRegisterUid(String uid) {
		if (uid == null) uid = "";
		mRegisterUid = uid;
		return this;
	}
	
	public String getRegisterPassword() {
		if (mRegisterPassword == null) mRegisterPassword = "";
		return mRegisterPassword;
	}
	
	public C2CProcess setRegisterPassword(String password) {
		if (password == null) password = "";
		mRegisterPassword = password;
		return this;
	}
	
	public C2CProcess setRegisterData(String server, String uid, String password) {
		if (server == null) server = "";
		mRegisterServer = server;
		if (uid == null) uid = "";
		mRegisterUid = uid;
		if (password == null) password = "";
		mRegisterPassword = password;
		return this;
	}
	
	public int audioPutInData(int lineId, byte[] data, int dataLen, int timestamp, int frameSeq, int duration, int payloadType) {
		int ret = mC2CHandle.audioPutInData(lineId, data, dataLen, timestamp, frameSeq, duration, payloadType);
		return ret;
	}
	
	/**
	 * The method was deprecated!<p>
	 * The method that specified registration server address 'ntut.iptnet.net'
	 * @return
	 */
	@Deprecated
	public int initialize() {
		return initialize("ntut.iptnet.net");
	}
	
	public synchronized int initialize(String server) {
		if (!mSdkInit) {
			int ret = mC2CHandle.initialize(server,"","");
			if (ret < 0) {
				Log.e(TAG, "initialize fail (" + ret + ")");
			} else {
				Log.d(TAG, "initialize success");
				mC2CHandle.enableVersion2();
				mC2CHandle.setProtocolMessageCallback(this);
				mC2CHandle.setStreamCommandCallback(this);
				mC2CHandle.setMediaStreamCallback(this);
				mSdkInit = true;
				// initialize C2C module extension: CSBC
				/*
				if (mCSBCHandle == null) {
					try {
						Class.forName("com.iptnet.c2c.csbc.CSBCHandle");
						mC2CHandle.enableCSBCLib();
						mCSBCHandle = CSBCHandle.getInstance();
						mCSBCHandle.initialize(this);
						Log.i(TAG, "CSBC library initialized");
					} catch (ClassNotFoundException e) {
						Log.w(TAG, "not found CSBC library");
					} catch (RuntimeException e) {
						Log.w(TAG, "not found CSBC library");
					}
				}
				*/
			}
			return ret;
		} else {
			throw new IllegalStateException("not released");
		}
	}
	
	public synchronized void release() {
		if (mSdkInit) {
			mC2CHandle.setProtocolMessageCallback(null);
			mC2CHandle.setStreamCommandCallback(null);
			mC2CHandle.setMediaStreamCallback(null);
			mC2CHandle.deInitialize();
			mSdkInit = false;
			Log.d(TAG, "released");
			
			// release C2C moduel extension: CSBC
			if (mCSBCHandle != null) {
				mCSBCHandle.setInfoCallback(null);
				mCSBCHandle.deInitialize();
				mCSBCHandle = null;
				Log.i(TAG, "CSBC library released");
			}
			
		} else {
			throw new IllegalStateException("not initialized");
		}
	}

	/**
	 * C2C Service OTA Information<p>
	 * Blocking mode<br>
	 * Key: host[String], port[Integer]
	 */
	public Bundle doGetOtaInfo(String peerId) throws C2CCommandException {

		if (mCSBCHandle != null) {
			Bundle bundle = new Bundle();
			mCBInfoOtaParam = new CBInfoParam.OTA();
			mCBInfoOtaParam.submit = false;
			mCSBCHandle.getServiceInfo(CSBCHandle.SERVICE_OTA, peerId);
			synchronized (mCBInfoOtaParam) {
				try {
					if (!mCBInfoOtaParam.submit) {
						mCBInfoOtaParam.wait(30000);
						if (!mCBInfoOtaParam.submit) {
							// timeout
							String err = "FWUpgrade >> send command time out";
							w(err);
							throw new C2CCommandException(err, C2C_COMMAND_ERROR, C2C_TIMEOUT);
						}
					}
					String host = mCBInfoOtaParam.host;
					int port = mCBInfoOtaParam.port;
					bundle.putString("host", host);
					bundle.putInt("port", port);

				} catch (InterruptedException e) {
					// timeout
					String err = "FWUpgrade >> send command time out";
					w(err);
					throw new C2CCommandException(err, C2C_COMMAND_ERROR, C2C_TIMEOUT);
					
				} finally {
					mCBInfoOtaParam = null;
				}
			}
			return bundle;

		} else {
			String err = "CSBC library was not initial";
			e(err);
			throw new RuntimeException(err);
		}
	}
	
	/**
	 * C2C Protocol Command<p>
	 * Blocking mode<p>
	 */
	public UpgradeFW doFirmwareUpgrade(
		Peer peer,
		int sequence,	int channel,	String firmwareVersion,		String downloadUrl,
		String vendor,	String model,	String md5, 				long size)
		throws C2CConnectException, C2CCommandException, C2CParserException {
		
		// check parameters
		if (peer == null) throw new NullPointerException("peer is null");
		
		mFWUpgradeParam = new RespCmdParam();
		mFWUpgradeParam.submit = false;
		
		// pack command
		String peerId = peer.getPeerId();
		String acc = peer.getAccount();
		String pwd = peer.getPassword();
		StringBuilder msg = new StringBuilder();
		msg.append("UPGRADE_FW=");
		msg.append(String.valueOf(sequence) + ";");
		msg.append(String.valueOf(channel) + ";");
		msg.append(firmwareVersion + ";");
		msg.append(downloadUrl + ";");
		msg.append(vendor + ";");
		msg.append(model + ";");
		msg.append((md5 == null || md5.isEmpty() ? "0" : md5) + ";");
		msg.append(String.valueOf(size) + ";");
		d("FWUpgrade >> prepare send command");
		d("FWUpgrade >> peerId("+peerId+") acc("+acc+") pwd("+pwd+") msg("+msg+")");
		
		// send command to peer
		int ret = mC2CHandle.sendCommandByProtocol(peerId, acc, pwd, msg.toString());
		if (ret < 0) {
			String err = "FWUpgrade >> send protocol command fail ("+ret+")";
			w(err);
			throw new C2CConnectException(err, ret);
		} else {
			d("FWUpgrade >> send protocol command ("+ret+")");
		}

		// wait command response
		UpgradeFW fw = null;
		synchronized (mFWUpgradeParam) {
			try {
				// thread lock
				if (!mFWUpgradeParam.submit) {
					mFWUpgradeParam.wait(30000);
					if (!mFWUpgradeParam.submit) {
						String err = "FWUpgrade >> send command time out";
						w(err);
						throw new C2CCommandException(err, C2C_COMMAND_ERROR, C2C_TIMEOUT);
					}
				}
				
				// get response parameters
				int lineId = mFWUpgradeParam.lineId;
				int event = mFWUpgradeParam.event;
				if (C2C_COMMAND_ACK == event) {
					String command = mFWUpgradeParam.msg;
					String rspPeerId = mFWUpgradeParam.peerId;
					fw = parseUpgradeFWAck(command);
					fw.base = new BaseInfo(lineId, rspPeerId);
					d("FWUpgrade >> receive command ("+command+") from peerId("+rspPeerId+")");
					
				} else if (C2C_COMMAND_ERROR == event) {
					int sub = mFWUpgradeParam.subEvent;
					String err = "FWUpgrade >> receive command error, lineId("+lineId+") event("+event+") sub("+sub+")";
					d(err);
					throw new C2CCommandException(err, event, sub);
				}
				
			} catch (InterruptedException e) {
				// timeout
				String err = "FWUpgrade >> send command time out";
				w(err);
				throw new C2CCommandException(err, C2C_COMMAND_ERROR, C2C_TIMEOUT);
				
			} finally {
				mFWUpgradeParam = null;
			}	
		}		
		return fw;
	}
	
	/**
	 * [TYPE] C2C Protocol Command<p>
	 * Blocking mode<p>
	 */
	public DeviceInfo doGetDeviceInfo(Peer peer, int sequence, int channel)
		throws C2CConnectException, C2CCommandException, C2CParserException {
		
		// check parameters
		if (peer == null) throw new NullPointerException("peer is null");
		
		// initialize lock object
		mDevInfoParam = new RespCmdParam();
		mDevInfoParam.submit = false;
		
		// pack command
		String peerId = peer.getPeerId();
		String acc = peer.getAccount();
		String pwd = peer.getPassword();
		StringBuilder msg = new StringBuilder();
		msg.append("DEV_INFO=");
		msg.append(String.valueOf(sequence) + ";");
		msg.append(String.valueOf(channel) + ";");
		d("DeviceInfo >> prepare send command");
		d("DeviceInfo >> peerId("+peerId+") acc("+acc+") pwd("+pwd+") msg("+msg+")");
		
		// send command to peer
		int ret = mC2CHandle.sendCommandByProtocol(peerId, acc, pwd, msg.toString());
		if (ret < 0) {
			String err = "DeviceInfo >> send protocol command fail ("+ret+")";
			w(err);
			throw new C2CConnectException(err, ret);
		} else {
			d("DeviceInfo >> send protocol command ("+ret+")");
		}

		// wait command response
		DeviceInfo devInfo = null;
		synchronized (mDevInfoParam) {
			try {
				// thread lock
				if (!mDevInfoParam.submit) {
					mDevInfoParam.wait(30000);
					if (!mDevInfoParam.submit) {
						String err = "DeviceInfo >> send command time out";
						w(err);
						throw new C2CCommandException(err, C2C_COMMAND_ERROR, C2C_TIMEOUT);
					}
				}
				
				// get response parameters
				int lineId = mDevInfoParam.lineId;
				int event = mDevInfoParam.event;
				if (C2C_COMMAND_ACK == event) {
					String command = mDevInfoParam.msg;
					String rspPeerId = mDevInfoParam.peerId;
					devInfo = parseDeviceInfoAck(command);
					devInfo.base = new BaseInfo(lineId, rspPeerId);
					d("DeviceInfo >> receive command ("+command+") from peerId("+rspPeerId+")");
					
				} else if (C2C_COMMAND_ERROR == event) {
					int sub = mDevInfoParam.subEvent;
					String err = "DeviceInfo >> receive command error, lineId("+lineId+") event("+event+") sub("+sub+")";
					d(err);
					throw new C2CCommandException(err, event, sub);
				}
				
			} catch (InterruptedException e) {
				// timeout
				String err = "DeviceInfo >> send command time out";
				w(err);
				throw new C2CCommandException(err, C2C_COMMAND_ERROR, C2C_TIMEOUT);
				
			} finally {
				mDevInfoParam = null;
			}
		}
		
		return devInfo;		
	}
	
	public void showDebugMessage(boolean enabled) {
		mC2CHandle.showDebugMessage(enabled);
	}

	public int setFastConnection(boolean enabled) {
		return mC2CHandle.setFastConnection(enabled);
	}
	
	public int register() {
		String srv = getRegisterServer();
		String uid = getRegisterUid();
		String pwd = getRegisterPassword();
		int ret = mC2CHandle.startRegisterProcess(srv, uid, pwd);
		Log.d(TAG, "register (" + ret + ")");
		Log.d(TAG, ">> server = " + srv);
		Log.d(TAG, ">> uid = " + uid);
		Log.d(TAG, ">> password = " + pwd);
		return ret;
	}
	
	public int connect(String peerId, String account, String password) {
		int ret = mC2CHandle.startConnection(peerId, account, password);
		Log.d(TAG, "connect (" + ret + ")");
		Log.d(TAG, ">> peerId = " + peerId + ", acc = " + account + ", pwd = " + password);
		return ret;
	}
	
	public int terminate(int lineId) {
		int ret = mC2CHandle.terminateConnection(lineId);
		Log.d(TAG, "termiante (" + ret + ")");
		return ret;
	}
	
	public boolean isRegistrationDone() {
		boolean ret = mC2CHandle.isRegistrationDone();
		Log.d(TAG, "is registration done ("+ret+")");
		return ret;
	}
	
	public int sendRtpCommand(int lineId, String tag, String message) {
		int ret = mC2CHandle.sendCommandByRtp(lineId, tag, message, true);
		Log.d(TAG, "send command by RTP (" + ret + "), tag = " + tag + ", msg = " + message);
		return ret;
	}
	
	public int sendProtocolCommand(String peerId, String peerAcc, String peerPwd, String message) {
		int ret = mC2CHandle.sendCommandByProtocol(peerId, peerAcc, peerPwd, message);
		Log.d(TAG, "send command by Protocol (" + ret + "), peerId=" + peerId + ", acc=" + peerAcc + ", pwd=" + peerPwd + ", msg=" + message);
		return ret;
	}
	
	public int sendProtocolCommand(int lineId, String message) {
		int ret = mC2CHandle.sendCommandByProtocolViaConnection(lineId, message);
		Log.d(TAG, "send command by Protocol via connection (" + lineId + "), lineId=" + lineId + ", msg=" + message);
		return ret;
	}
	
	public String generateNewRegisterUid(String suffix, String domain) {
		String uid = mC2CHandle.getRegistrationUID(suffix, domain);
		if (uid == null) uid = "";
		return uid;
	}
	
	public String getRegisterPasswordByUid(String uid) {
		String pwd = mC2CHandle.getRegistrationPassword(uid);
		if (pwd == null) pwd = "";
		return pwd;
	}
	
	public int setNotification(@NonNull String notifyId, @NonNull String peerId, int code) {
		
		// check parameters
		if (notifyId == null) {
			throw new NullPointerException("notify ID is null");
		} else if (notifyId.isEmpty()) {
			throw new IllegalArgumentException("notify ID is not allow empty");
		}else if (peerId == null) {
			throw new NullPointerException("peer ID is null");
		} else if (peerId.isEmpty()) {
			throw new IllegalArgumentException("peer ID is not allow empty");
		} else if (code < 0) {
			throw new IllegalArgumentException("code is not be negative ("+code+")");
		}
		
		// set notification to server
		int ret = mC2CHandle.setNotification(notifyId, peerId, code);
		return ret;
	}
	
	public int setNotification(@NonNull String notifyId, @NonNull String peerId, int code,@NonNull String packageName) {
		
		// check parameters
		if (notifyId == null) {
			throw new NullPointerException("notify ID is null");
		} else if (notifyId.isEmpty()) {
			throw new IllegalArgumentException("notify ID is not allow empty");
		}else if (peerId == null) {
			throw new NullPointerException("peer ID is null");
		} else if (peerId.isEmpty()) {
			throw new IllegalArgumentException("peer ID is not allow empty");
		}else if (packageName == null) {
			throw new NullPointerException("packet name is null"); 
		}
		else if (code < 0) {
			throw new IllegalArgumentException("code is not be negative ("+code+")");
		}
		
		// set notification to server
		int ret = mC2CHandle.setNotification(notifyId, peerId, code,packageName);
		return ret;
	}	
	
	public int setC2CNotification(@NonNull String notifyId, @NonNull String peerId, int code,@NonNull String packageName) {
		
		// check parameters
		if (notifyId == null) {
			throw new NullPointerException("notify ID is null");
		} else if (notifyId.isEmpty()) {
			throw new IllegalArgumentException("notify ID is not allow empty");
		}else if (peerId == null) {
			throw new NullPointerException("peer ID is null");
		} else if (peerId.isEmpty()) {
			throw new IllegalArgumentException("peer ID is not allow empty");
		}else if (packageName == null) {
			throw new NullPointerException("packet name is null"); 
		}
		else if (code < 0) {
			throw new IllegalArgumentException("code is not be negative ("+code+")");
		}
		
		// set notification to server
		int ret = mC2CHandle.setC2CNotification(notifyId, peerId, code,packageName);
		return ret;
	}	
	
	public int queryTransmissionMode(int lineId) {
		int mode = mC2CHandle.queryTransmissionMode(lineId);
		return mode;
	}
	
	public void setMediaStreamCallback(MediaStreamCallback callback) {
		mMediaStreamCallback = callback;
	}

	public boolean addProtocolMessageCallback(ProtocolMessageCallback callback, int[] filter) throws NullPointerException {
		if (callback == null) throw new NullPointerException("callback is null");
		Object[] objs = new Object[2];
		objs[0] = callback;
		objs[1] = filter;
		return mProtocolMessageCallbackSet.add(objs);
	}
	
	public boolean removeProtocolMessageCallback(ProtocolMessageCallback callback) {
		for (Object[] objs : mProtocolMessageCallbackSet) {
			if (objs[0] == callback) {
				return mProtocolMessageCallbackSet.remove(objs);
			}
		}
		return false;
	}
	
	public boolean requestStreamCommandCallback(StreamCommandCallback callback) {
		if (callback == null) throw new NullPointerException("callback is null");
		
		for (StreamCommandCallback callbackobj : mStreamCommandCallbackSet) {
			if (callback == callbackobj)
				return false;
		}
		return mStreamCommandCallbackSet.add(callback);
	}
	
	public boolean abandonStreamCommandCallback(StreamCommandCallback callback) {
		if (callback == null) throw new NullPointerException("callback is null");
		
		for (StreamCommandCallback callbackobj : mStreamCommandCallbackSet) {
			if (callback == callbackobj)
			{
				return mStreamCommandCallbackSet.remove(callback);
			}
		}
		return false; 
	}
	
	public boolean requestProtocolCommandCallback(ProtocolMessageCallback callback) {
		if (callback == null) throw new NullPointerException("callback is null");
		
		for (ProtocolMessageCallback callbackobj : mProtocolCommandCallbackSet) {
			if (callback == callbackobj)
				return false;
		}
		return mProtocolCommandCallbackSet.add(callback);
	}
	
	public boolean abandonProtocolCommandCallback(ProtocolMessageCallback callback) {
		if (callback == null) throw new NullPointerException("callback is null");
		
		for (ProtocolMessageCallback callbackobj : mProtocolCommandCallbackSet) {
			if (callback == callbackobj)
			{
				return mProtocolCommandCallbackSet.remove(callback);
			}
		}
		return false; 
	}
	
	@Override
	public Object receiveProtocolMessage(int lineId, int mainEvent, int subEvent, Bundle params) {
		
		Log.d(TAG, "receive protocol message, lineId="+lineId+" message="+mainEvent+" sub="+subEvent);
		
		if (C2C_COMMAND_ACK == mainEvent || C2C_COMMAND_ERROR == mainEvent) {
			// check Firmware Upgrade command
			if (mFWUpgradeParam != null) {
				synchronized (mFWUpgradeParam) {			
					mFWUpgradeParam.lineId = lineId;
					mFWUpgradeParam.event = mainEvent;
					mFWUpgradeParam.subEvent = subEvent;
					mFWUpgradeParam.peerId = params.getString("peerId");
					mFWUpgradeParam.msg = params.getString("msg");
					mFWUpgradeParam.notifyAll();
					mFWUpgradeParam.submit = true;
				}
				return null;
			
			// check Device Information command
			} else if (mDevInfoParam != null) {
				synchronized (mDevInfoParam) {
					mDevInfoParam.lineId = lineId;
					mDevInfoParam.event = mainEvent;
					mDevInfoParam.subEvent = subEvent;
					mDevInfoParam.peerId = params.getString("peerId");
					mDevInfoParam.msg = params.getString("msg");
					mDevInfoParam.notifyAll();
					mDevInfoParam.submit = true;
				}
				return null;
			}
		}
		
		for (Object[] objs : mProtocolMessageCallbackSet) {
			ProtocolMessageCallback callback = (ProtocolMessageCallback) objs[0];
			if (objs[1] == null) {
				callback.receiveProtocolMessage(lineId, mainEvent, subEvent, params);
			} else {
				int[] filter = (int[]) objs[1];
				for (int f : filter) {
					if (f == mainEvent && f != C2C_COMMAND_MESSAGE && f != C2C_COMMAND_ACK && f != C2C_COMMAND_ERROR
						&& f != C2C_BYTE_MESSAGE && f != C2C_BYTE_ACK && f != C2C_BYTE_ERROR) {
						callback.receiveProtocolMessage(lineId, mainEvent, subEvent, params);
					}
				}
			}
		}
		
		for (ProtocolMessageCallback callbackobj : mProtocolCommandCallbackSet) {
			if (mainEvent == C2C_COMMAND_MESSAGE || mainEvent == C2C_COMMAND_ACK || mainEvent == C2C_COMMAND_ERROR ||
					mainEvent == C2C_BYTE_MESSAGE || mainEvent == C2C_BYTE_ACK || mainEvent == C2C_BYTE_ERROR)
			{
				callbackobj.receiveProtocolMessage(lineId, mainEvent, subEvent, params);
			}
		}
		
		return null;
	}
	
	@Override
	public void receiveVideoStream(int lineId, byte[] data, int dataLen, int payloadType, int timestamp, int frameRate, int frameType, int frameSeq) {
		if (mMediaStreamCallback != null)
			mMediaStreamCallback.receiveVideoStream(lineId, data, dataLen, payloadType, timestamp, frameRate, frameType, frameSeq);
	}

	@Override
	public void receiveAudioStream(int lineId, byte[] data, int dataLen, int payloadType, int timestamp, int frameSeq) {
		if (mMediaStreamCallback != null)
			mMediaStreamCallback.receiveAudioStream(lineId, data, dataLen, payloadType, timestamp, frameSeq);
	}

	@Override
	public void onStreamStart(int lineId) {
		
	}

	@Override
	public void onStreamStop(int lineId) {
		
	}

	@Override
	public String receiveCommandStream(int lineId, int type, String tag, String message, Bundle bundle) {
		if(DEBUG) Log.d(TAG, "receiveCommandStream msg = '"+message+"'");

		for (StreamCommandCallback callbackobj : mStreamCommandCallbackSet) {
			callbackobj.receiveCommandStream(lineId, type, tag, message,bundle);
		}
		return null;
	}

	/*
	 * COMMAND PROCESS
	 */
	private static DeviceInfo parseDeviceInfoAck(String ack) throws C2CParserException {
		
		DeviceInfo info = new DeviceInfo();
		
		// parse message
		// command format
		// DEV_INFO_ACK=[SEQ];[CH];[VENDOR];[DEV_MODEL];[FW_VER];[LIBNTIL_VER];[CAPABILITY];[DEVICETYPE];
		SparseArray<String> command = parseProtocolCommand(ack);
		
		// check tag
		String tag = command.get(0);
		if (!tag.equals("DEV_INFO_ACK")) {
			String err = "Parser >> command tag is not unrocognized, ack("+ack+")";
			e(err); throw new C2CParserException(err);
		}
		
		// check item size
		final int needItemSize = 3;
		int cmdItemSize = command.size();
		if (cmdItemSize <= needItemSize) {
			String err = "Parser >> command item size is not match. need size(" + needItemSize + "), ack(" + ack + " [" + cmdItemSize + "])";
			e(err); throw new C2CParserException(err);
		}

		// String to Integer
		try {
			/*1*/ info.sequence = Integer.parseInt(command.get(1));
			/*2*/ info.channel = Integer.parseInt(command.get(2));
			/*7*/ info.capability = Integer.parseInt(command.get(7), 16);

		} catch (NumberFormatException e) {
			String err = "Parser >> parse number format fail, seq("+command.get(1)+") ch("+command.get(2)+") capability("+command.get(7)+") device_type("+command.get(8);
			e(err); throw new C2CParserException(err);
		}

		// put parameter to bundle
		/*0*/ info.tag = tag;
		/*3*/ info.vendor = command.get(3);
		/*4*/ info.model = command.get(4);
		/*5*/ info.fwVer = command.get(5);
		/*6*/ info.libVer = command.get(6);
		
		/*8*/ if (command.get(8) != null )
			info.device_type = Integer.parseInt(command.get(8), 16);
		return info;
	}
	
	private static UpgradeFW parseUpgradeFWAck(String ack) throws C2CParserException {
		
		UpgradeFW fw = new UpgradeFW();
		
		// parse message
		// command format
		// UPGRADE_FW_ACK=[SEQ];[CH];[RESULT];[CODE];[EXPIRES];
		SparseArray<String> command = parseProtocolCommand(ack);
		
		// check tag
		String tag = command.get(0);
		if (!tag.equals("UPGRADE_FW_ACK")) {
			String err = "Parser >> command tag is not unrocognized, ack("+ack+")";
			e(err); throw new C2CParserException(err);
		}
		
		// check item size
		final int needItemSize = 3;
		int cmdItemSize = command.size();
		if (cmdItemSize <= needItemSize) {
			String err = "Parser >> command item size is not match. need size(" + needItemSize + "), ack(" + ack + " [" + cmdItemSize + "])";
			e(err); throw new C2CParserException(err);
		}
		
		// String to Integer
		try {
			/*1*/ fw.sequence = Integer.parseInt(command.get(1));
			/*2*/ fw.channel = Integer.parseInt(command.get(2));
			/*3*/ fw.result = Integer.parseInt(command.get(3));
			/*4*/ fw.code = Integer.parseInt(command.get(4));
			/*5*/ fw.expires = Integer.parseInt(command.get(5));

		} catch (NumberFormatException e) {
			String err = "Parser >> parse number format fail, seq("+command.get(1)+") ch("+command.get(2)+") result("+command.get(3)+")";
			err += " code("+command.get(4)+") expires("+command.get(5)+")";
			e(err); throw new C2CParserException(err);
		}
		
		// put parameter to bundle
		/*0*/ fw.tag = command.get(0);
		
		return fw;
	}
	
	private static SparseArray<String> parseProtocolCommand(String command) {

		SparseArray<String> parsed = new SparseArray<>(10);
		String tag = "";
		
		// get tag
		String[] splits = command.split("=");
		if (splits.length < 2) {
			parsed.put(0, tag);
			return parsed;
		}
		parsed.put(0, tag = splits[0]);
		
		// get value
		if (!tag.isEmpty()) {
			String[] values = splits[1].split(";");
			for (int i=1; i<values.length+1; i++) {
				parsed.put(i, values[i-1]);
			}
		}
		return parsed;
	}

	/*
	 * CSBC LIBRARY
	 */
	@Override
	public void onInfoCallback(InfoCB info) {
			
		// get service name
		String serviceName = info.getServiceName();		
		if ("ota".equals(serviceName) && mCBInfoOtaParam != null) {
			synchronized (mCBInfoOtaParam) {
				
				ServiceAddressGroup group = info.getAddress();
				int srvNum = group.getAddressNumber();
				if (srvNum > 0) {	
					// get OTA address and port
					ServiceAddress addr = group.getServiceAddress(0);
					String host = addr.getAddress();
					int port = addr.getPort();
					mCBInfoOtaParam.host = host;
					mCBInfoOtaParam.port = port;
					mCBInfoOtaParam.submit = true;
				} else {
					Log.e(TAG, "InfoCallback >> not get OTA address and port");
				}
				mCBInfoOtaParam.notifyAll();
			}
		}
	}

	private static void d(String msg) {
		if (DEBUG_SHOWN) Log.d(TAG, msg);
	}
	
	private static void w(String msg) {
		if (DEBUG_SHOWN) Log.w(TAG, msg);
	}
	
	private static void e(String msg) {
		if (DEBUG_SHOWN) Log.e(TAG, msg);
	}
	
	public void setLogMessageShown(boolean enabled) {
		DEBUG_SHOWN = enabled;
	}


    public static int getP2PServerStatus() {
        return mP2PServerStatus;
    }

    public static void setP2PServerStatus(int p2PServerStatus) {
        C2CProcess.mP2PServerStatus = p2PServerStatus;
    }
}
