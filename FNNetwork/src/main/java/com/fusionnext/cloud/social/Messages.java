package com.fusionnext.cloud.social;

import android.content.Context;

import com.fusionnext.cloud.datalink.http.HttpGet;
import com.fusionnext.cloud.datalink.http.HttpParam;

/**
 * Created by Mike Chang on 2017/3/23
 */
public class Messages {
    protected final static String _TAG = "Messages";
    private Context context;

    public Messages(Context context) {
        this.context = context;
    }

    public void getMessage(String messageId, final SocialAPIHandler.OnMessagesListener onMessagesListener) {
        HttpParam.HttpSubParam param = new HttpParam.HttpSubParam();

        param.accessToken = Public.getAuthorization(context).getUserAccessToken();
        param.url = HttpData.generateHttpGetUrl(HttpData._GET_TYPE_MESSAGES_GET_MESSAGE, messageId, 0, null, null);
        param.cacheName = "";
        HttpParam[] params = new HttpParam[1];
        params[0] = new HttpParam();
        params[0].params.clear();
        params[0].params.add(param);

        HttpGet httpGet = new HttpGet();
        httpGet.setContext(context);
        httpGet.setTag(HttpGet._HTTP_T_GET_NORMAL_TYPE);
        httpGet.setOnParseJsonListener(new HttpGet.OnParseJsonListener() {
            @Override
            public void onParse(boolean result, String returnStr) {
                //onSocialAPIListener.onGetComment(result, parseComment(returnStr));
            }
        });
        httpGet.execute(params);
    }
}
