package com.fusionnext.cloud.social;

import com.fusionnext.cloud.utils.FNNetworkLog;

import java.io.IOException;
import java.net.URL;

/**
 * Created by Mike Chang on 2016/2/22.
 */

public class HttpData {
    protected final static String _TAG = "HttpData";
    //HttpPost Type
    public final static int _POST_TYPE_UNKNOWN = 0x09;
    public final static int _POST_TYPE_UPLOAD_FILE = 0x10;
    public final static int _POST_TYPE_AUTH_GET_CLIENT_TOKEN = 0x11;
    public final static int _POST_TYPE_AUTH_REFRESH_CLIENT_TOKEN = 0x12;
    public final static int _POST_TYPE_AUTH_LOGIN_BY_EMAIL = 0x13;
    public final static int _POST_TYPE_USER_CREATE_USER_BY_EMAIL = 0x14;
    public final static int _POST_TYPE_USER_CREATE_USER_BY_FACEBOOK = 0x15;
    public final static int _POST_TYPE_TRACKS_CREATE_TRACK = 0x16;
    public final static int _POST_TYPE_TRACKS_LIKE = 0x17;
    /*COMMENTS*/
    public final static int _POST_TYPE_COMMENTS_ADD_COMMENT = 0x18;
    public final static int _POST_TYPE_COMMENTS_LIKE = 0x19;
    /*GROUP*/
    public final static int _POST_TYPE_GROUPS_CREATE_GROUP = 0x1A;
    public final static int _POST_TYPE_GROUPS_INVITE_INTO_GROUP = 0x1B;
    /*DEVICE*/
    public final static int _POST_TYPE_DEVICES_CREATE_DEVICE = 0x1C;
    /*POINT*/
    public final static int _POST_TYPE_POINTS_CREATE_POINTS = 0x1D;
    /*PHOTOS*/
    public final static int _POST_TYPE_PHOTOS_SAVE_PHOTO = 0x1E;
    public final static int _POST_TYPE_PHOTOS_LIKE = 0x1F;


    //HttpGet Type
    /*USER*/
    public final static int _GET_TYPE_USER_GET_USER = 0x20;
    public final static int _GET_TYPE_USER_LIST_FOLLOWERS = 0x21;
    public final static int _GET_TYPE_USER_LIST_FOLLOWS = 0x22;
    public final static int _GET_TYPE_USER_LIST_TRACKS = 0x23;
    public final static int _GET_TYPE_USER_LIST_INVITATIONS = 0x24;
    public final static int _GET_TYPE_USER_LIST_GROUPS = 0x25;
    /*TRACKS*/
    public final static int _GET_TYPE_TRACKS_LIST_ALL_TRACKS = 0x26;
    public final static int _GET_TYPE_TRACKS_LIST_TRACKS_FOLLOWED = 0x27;
    public final static int _GET_TYPE_TRACKS_LIST_TRACKS_FAVORITE = 0x28;
    public final static int _GET_TYPE_TRACKS_LIST_TRACKS_RECOMMEND = 0x29;
    public final static int _GET_TYPE_TRACKS_LIST_TRACKS_RANKING = 0x2A;
    public final static int _GET_TYPE_TRACKS_GET_TRACK = 0x2B;
    public final static int _GET_TYPE_TRACKS_LIST_POINTS = 0x2C;
    public final static int _GET_TYPE_TRACKS_LIST_PHOTOS = 0x2D;
    public final static int _GET_TYPE_TRACKS_LIST_COMMENTS = 0x2E;
    public final static int _GET_TYPE_TRACKS_LIST_LIKES = 0x2F;
    public final static int _GET_TYPE_TRACKS_LIST_TAGS = 0x30;

    /*COMMENTS*/
    public final static int _GET_TYPE_COMMENTS_GET_COMMENT = 0x31;
    public final static int _GET_TYPE_COMMENTS_LIST_LIKES = 0x32;
    /*GROUP*/
    public final static int _GET_TYPE_GROUPS_GET_GROUP = 0x33;
    public final static int _GET_TYPE_GROUPS_LIST_INVITATIONS = 0x34;
    public final static int _GET_TYPE_GROUPS_LIST_MEMBERS = 0x35;
    public final static int _GET_TYPE_GROUPS_LIST_MESSAGES = 0x36;
    public final static int _GET_TYPE_GROUPS_LIST_PHOTOS = 0x37;
    /*MESSAGES*/
    public final static int _GET_TYPE_MESSAGES_GET_MESSAGE = 0x38;
    /*PHOTOS*/
    public final static int _GET_TYPE_PHOTOS_LIST_ALL_PHOTOS = 0x39;
    public final static int _GET_TYPE_PHOTOS_GET_PHOTO = 0x3A;
    public final static int _GET_TYPE_PHOTOS_LIST_COMMENTS = 0x3B;
    public final static int _GET_TYPE_PHOTOS_LIST_LIKES = 0x3C;
    public final static int _GET_TYPE_PHOTOS_LIST_TAGS = 0x3D;
    /*TAGS*/
    public final static int _GET_TYPE_TAGS_LIST_TRACKS = 0x3E;
    public final static int _GET_TYPE_TAGS_LIST_PHOTOS = 0x3F;

    //HttpPut Type
    public final static int _PUT_TYPE_UNKNOWN = 0x40;
    public final static int _PUT_TYPE_USER_UPDATE_USER = 0x41;
    public final static int _PUT_TYPE_USER_FOLLOW_USER = 0x42;
    public final static int _PUT_TYPE_TRACKS_UPDATE_TRACK = 0x43;
    /*COMMENTS*/
    public final static int _PUT_TYPE_COMMENTS_UPDATE_COMMENT = 0x44;
    /*GROUP*/
    public final static int _PUT_TYPE_GROUPS_UPDATE_GROUP = 0x55;
    public final static int _PUT_TYPE_GROUPS_ACCEPT_INVITATION = 0x56;
    /*DEVICE*/
    public final static int _PUT_TYPE_DEVICES_UPDATE_DEVICE = 0x57;


    //HttpDelete Type
    public final static int _DEL_TYPE_UNKNOWN = 0x60;
    public final static int _DEL_TYPE_USER_UNFOLLOW_USER = 0x61;
    public final static int _DEL_TYPE_TRACKS_UNLIKE = 0x62;
    public final static int _DEL_TYPE_COMMENTS_UNLIKE = 0x63;
    public final static int _DEL_TYPE_GROUPS_DELETE_INVITATION = 0x64;
    public final static int _DEL_TYPE_GROUPS_LEAVE_GROUP = 0x65;
    public final static int _DEL_TYPE_PHOTOS_UNLIKE = 0x66;


    public final static int _GET_TYPE_SORT_ORDER_DESC = 0;
    public final static int _GET_TYPE_SORT_ORDER_ASC = 1;

    protected static URL generateHttpPostUrl(int postType) {
        return generateHttpPostUrl(postType, null);
    }

    protected static URL generateHttpPostUrl(int postType, String id) {
        boolean error = true;
        URL url = null;
        try {
            switch (postType) {
                case _POST_TYPE_UPLOAD_FILE:
                    url = new URL(Public._INTERFACE_POST_UPLOAD_FILE);
                    break;
                case _POST_TYPE_AUTH_GET_CLIENT_TOKEN:
                    url = new URL(Public._INTERFACE_POST_GET_CLIENT_TOKEN);
                    break;
                case _POST_TYPE_AUTH_REFRESH_CLIENT_TOKEN:
                    url = new URL(Public._INTERFACE_POST_REFRESH_DEVICE_TOKEN);
                    break;
                case _POST_TYPE_AUTH_LOGIN_BY_EMAIL:
                    url = new URL(Public._INTERFACE_POST_LOGIN_BY_EMAIL);
                    break;
                case _POST_TYPE_USER_CREATE_USER_BY_EMAIL:
                case _POST_TYPE_USER_CREATE_USER_BY_FACEBOOK:
                    url = new URL(Public._INTERFACE_POST_CREATE_USER);
                    break;
                case _POST_TYPE_TRACKS_CREATE_TRACK:
                    url = new URL(Public._INTERFACE_POST_CREATE_TRACK);
                    break;
                case _POST_TYPE_TRACKS_LIKE:
                    url = new URL(Public._INTERFACE_POST_LIKE_TRACK.replace("{track_id}", id));
                    break;
                case _POST_TYPE_COMMENTS_ADD_COMMENT:
                    url = new URL(Public._INTERFACE_POST_ADD_COMMENT);
                    break;
                case _POST_TYPE_COMMENTS_LIKE:
                    url = new URL(Public._INTERFACE_POST_LIKE_COMMENT.replace("{comment_id}", id));
                    break;
                case _POST_TYPE_GROUPS_CREATE_GROUP:
                    url = new URL(Public._INTERFACE_POST_CREATE_GROUP);
                    break;
                case _POST_TYPE_GROUPS_INVITE_INTO_GROUP:
                    url = new URL(Public._INTERFACE_POST_INVITE_INTO_GROUP.replace("{group_id}", id));
                    break;
                case _POST_TYPE_DEVICES_CREATE_DEVICE:
                    url = new URL(Public._INTERFACE_POST_CREATE_DEVICE);
                    break;
                case _POST_TYPE_POINTS_CREATE_POINTS:
                    url = new URL(Public._INTERFACE_POST_CREATE_POINTS);
                    break;
                case _POST_TYPE_PHOTOS_LIKE:
                    url = new URL(Public._INTERFACE_POST_LIKE_PHOTO.replace("{photo_id}", id));
                    break;
                case _POST_TYPE_PHOTOS_SAVE_PHOTO:
                    url = new URL(Public._INTERFACE_POST_SAVE_PHOTO);
                    break;
                default:
                    return url;
            }
            FNNetworkLog.d(_TAG, url.toString());
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
        }
        return url;
    }


    protected static String generateHttpGetUrl(int postType, int order, String pageSize, String pageNum) {
        return generateHttpGetUrl(postType, null, order, pageSize, pageNum);
    }

    protected static String generateHttpGetUrl(int getType, String id, int order, String pageSize, String pageNum) {
        boolean error = true;
        String url = null;

        switch (getType) {
            case _GET_TYPE_USER_GET_USER:
                url = Public._INTERFACE_GET_GET_USER.replace("{user_id}", id);
                break;
            case _GET_TYPE_USER_LIST_FOLLOWERS:
                url = Public._INTERFACE_GET_LIST_USER_FOLLOWERS.replace("{user_id}", id);
                url = genGetUrl(url, order, pageSize, pageNum);
                break;
            case _GET_TYPE_USER_LIST_FOLLOWS:
                url = Public._INTERFACE_GET_LIST_USER_FOLLOWS.replace("{user_id}", id);
                url = genGetUrl(url, order, pageSize, pageNum);
                break;
            case _GET_TYPE_USER_LIST_TRACKS:
                url = Public._INTERFACE_GET_LIST_USER_TRACKS.replace("{user_id}", id);
                url = genGetUrl(url, order, pageSize, pageNum);
                break;
            case _GET_TYPE_USER_LIST_INVITATIONS:
                url = Public._INTERFACE_GET_LIST_USER_INVITATIONS.replace("{user_id}", id);
                url = genGetUrl(url, order, pageSize, pageNum);
                break;
            case _GET_TYPE_USER_LIST_GROUPS:
                url = Public._INTERFACE_GET_LIST_USER_GROUPS.replace("{user_id}", id);
                url = genGetUrl(url, order, pageSize, pageNum);
                break;
            case _GET_TYPE_TRACKS_LIST_ALL_TRACKS:
                url = Public._INTERFACE_GET_LIST_ALL_TRACKS;
                url = genGetUrl(url, order, pageSize, pageNum);
                break;
            case _GET_TYPE_TRACKS_LIST_TRACKS_FOLLOWED:
                url = Public._INTERFACE_GET_LIST_FOLLOWED_TRACKS.replace("{user_id}", id);
                url = genGetUrl(url, order, pageSize, pageNum);
                break;
            case _GET_TYPE_TRACKS_LIST_TRACKS_FAVORITE:
                url = Public._INTERFACE_GET_LIST_FAVORITE_TRACKS;
                url = genGetUrl(url, order, pageSize, pageNum);
                break;
            case _GET_TYPE_TRACKS_LIST_TRACKS_RECOMMEND:
                url = Public._INTERFACE_GET_LIST_RECOMMENDED_TRACKS;
                url = genGetUrl(url, order, pageSize, pageNum);
                break;
            case _GET_TYPE_TRACKS_LIST_TRACKS_RANKING:
                url = Public._INTERFACE_GET_LIST_POPULAR_TRACKS;
                url = genGetUrl(url, order, pageSize, pageNum);
                break;
            case _GET_TYPE_TRACKS_GET_TRACK:
                url = Public._INTERFACE_GET_GET_TRACK.replace("{track_id}", id);
                break;
            case _GET_TYPE_TRACKS_LIST_POINTS:
                url = Public._INTERFACE_GET_LIST_TRACK_POINTS.replace("{track_id}", id);
                url = genGetUrl(url, order, pageSize, pageNum);
                break;
            case _GET_TYPE_TRACKS_LIST_PHOTOS:
                url = Public._INTERFACE_GET_LIST_TRACK_PHOTOS.replace("{track_id}", id);
                url = genGetUrl(url, order, pageSize, pageNum);
                break;
            case _GET_TYPE_TRACKS_LIST_COMMENTS:
                url = Public._INTERFACE_GET_LIST_TRACK_COMMENTS.replace("{track_id}", id);
                url = genGetUrl(url, order, pageSize, pageNum);
                break;
            case _GET_TYPE_TRACKS_LIST_LIKES:
                url = Public._INTERFACE_GET_LIST_TRACK_LIKES.replace("{track_id}", id);
                url = genGetUrl(url, order, pageSize, pageNum);
                break;
            case _GET_TYPE_TRACKS_LIST_TAGS:
                url = Public._INTERFACE_GET_LIST_TRACK_TAGS.replace("{track_id}", id);
                url = genGetUrl(url, order, pageSize, pageNum);
                break;
            case _GET_TYPE_COMMENTS_GET_COMMENT:
                url = Public._INTERFACE_GET_GET_COMMENT.replace("{comment_id}", id);
                break;
            case _GET_TYPE_COMMENTS_LIST_LIKES:
                url = Public._INTERFACE_GET_LIST_COMMENT_LIKES.replace("{comment_id}", id);
                url = genGetUrl(url, order, pageSize, pageNum);
                break;
            case _GET_TYPE_GROUPS_GET_GROUP:
                url = Public._INTERFACE_GET_GET_GROUP.replace("{group_id}", id);
                break;
            case _GET_TYPE_GROUPS_LIST_INVITATIONS:
                url = Public._INTERFACE_GET_LIST_GROUP_INVITATION.replace("{group_id}", id);
                url = genGetUrl(url, order, pageSize, pageNum);
                break;
            case _GET_TYPE_GROUPS_LIST_MEMBERS:
                url = Public._INTERFACE_GET_LIST_GROUP_MEMBERS.replace("{group_id}", id);
                url = genGetUrl(url, order, pageSize, pageNum);
                break;
            case _GET_TYPE_GROUPS_LIST_MESSAGES:
                url = Public._INTERFACE_GET_LIST_GROUP_MESSAGES.replace("{group_id}", id);
                url = genGetUrl(url, order, pageSize, pageNum);
                break;
            case _GET_TYPE_GROUPS_LIST_PHOTOS:
                url = Public._INTERFACE_GET_LIST_GROUP_PHOTOS.replace("{group_id}", id);
                url = genGetUrl(url, order, pageSize, pageNum);
                break;
            case _GET_TYPE_MESSAGES_GET_MESSAGE:
                url = Public._INTERFACE_GET_GET_MESSAGE.replace("{message_id}", id);
                break;

            case _GET_TYPE_PHOTOS_LIST_ALL_PHOTOS:
                url = Public._INTERFACE_GET_LIST_ALL_PHOTOS;
                url = genGetUrl(url, order, pageSize, pageNum);
                break;
            case _GET_TYPE_PHOTOS_GET_PHOTO:
                url = Public._INTERFACE_GET_GET_PHOTO.replace("{photo_id}", id);
                break;
            case _GET_TYPE_PHOTOS_LIST_COMMENTS:
                url = Public._INTERFACE_GET_LIST_PHOTO_COMMENTS.replace("{photo_id}", id);
                url = genGetUrl(url, order, pageSize, pageNum);
                break;
            case _GET_TYPE_PHOTOS_LIST_LIKES:
                url = Public._INTERFACE_GET_LIST_PHOTO_LIKES.replace("{photo_id}", id);
                url = genGetUrl(url, order, pageSize, pageNum);
                break;
            case _GET_TYPE_PHOTOS_LIST_TAGS:
                url = Public._INTERFACE_GET_LIST_PHOTO_TAGS.replace("{photo_id}", id);
                url = genGetUrl(url, order, pageSize, pageNum);
                break;
            case _GET_TYPE_TAGS_LIST_TRACKS:
                url = Public._INTERFACE_GET_LIST_TAGS_TRACKS.replace("{tag_name}", id);
                url = genGetUrl(url, order, pageSize, pageNum);
                break;
            case _GET_TYPE_TAGS_LIST_PHOTOS:
                url = Public._INTERFACE_GET_LIST_TAGS_PHOTOS.replace("{tag_name}", id);
                url = genGetUrl(url, order, pageSize, pageNum);
                break;
            default:
                return url;
        }
        FNNetworkLog.d(_TAG, url);

        return url;
    }

    private static String genGetUrl(String url, int order, String pageSize, String pageNum) {
        String sOrder = "desc";
        String output = null;
        if (order == _GET_TYPE_SORT_ORDER_ASC) {
            sOrder = "asc";
        } else {
            sOrder = "desc";
        }
        output = url + "order=" + sOrder;

        if (pageSize != null) {
            output = output + "&limit=" + pageSize;
        }
        if (pageNum != null) {
            output = output + "&page=" + pageNum;
        }
        return output;
    }

    protected static URL generateHttpPutUrl(int postType) {
        return generateHttpPutUrl(postType, null);
    }

    protected static URL generateHttpPutUrl(int putType, String id) {
        boolean error = true;
        URL url = null;
        try {
            switch (putType) {
                case _PUT_TYPE_USER_UPDATE_USER:
                    url = new URL(Public._INTERFACE_PUT_UPDATE_USER.replace("{user_id}", id));
                    break;
                case _PUT_TYPE_USER_FOLLOW_USER:
                    url = new URL(Public._INTERFACE_POST_FOLLOW_USER.replace("{user_id}", id));
                    break;
                case _PUT_TYPE_TRACKS_UPDATE_TRACK:
                    url = new URL(Public._INTERFACE_PUT_UPDATE_TRACK.replace("{track_id}", id));
                    break;
                case _PUT_TYPE_COMMENTS_UPDATE_COMMENT:
                    url = new URL(Public._INTERFACE_PUT_UPDATE_COMMENT.replace("{comment_id}", id));
                    break;
                case _PUT_TYPE_GROUPS_UPDATE_GROUP:
                    url = new URL(Public._INTERFACE_PUT_UPDATE_GROUP.replace("{group_id}", id));
                    break;
                case _PUT_TYPE_GROUPS_ACCEPT_INVITATION:
                    url = new URL(Public._INTERFACE_PUT_ACCEPT_INVITATION.replace("{group_id}", id));
                    break;
                case _PUT_TYPE_DEVICES_UPDATE_DEVICE:
                    url = new URL(Public._INTERFACE_PUT_UPDATE_DEVICE.replace("{device_id}", id));
                    break;
                default:
                    return url;
            }
            FNNetworkLog.d(_TAG, url.toString());
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
        }
        return url;
    }

    protected static URL generateHttpDeleteUrl(int deleteType, String id) {
        boolean error = true;
        URL url = null;
        try {
            switch (deleteType) {
                case _DEL_TYPE_USER_UNFOLLOW_USER:
                    url = new URL(Public._INTERFACE_DEL_UNFOLLOW_USER.replace("{user_id}", id));
                    break;
                case _DEL_TYPE_TRACKS_UNLIKE:
                    url = new URL(Public._INTERFACE_DEL_UNLIKE_TRACK.replace("{track_id}", id));
                    break;
                case _DEL_TYPE_COMMENTS_UNLIKE:
                    url = new URL(Public._INTERFACE_DEL_UNLIKE_COMMENT.replace("{comment_id}", id));
                    break;
                case _DEL_TYPE_GROUPS_DELETE_INVITATION:
                    url = new URL(Public._INTERFACE_DEL_DELETE_INVITATION.replace("{group_id}", id));
                    break;
                case _DEL_TYPE_GROUPS_LEAVE_GROUP:
                    url = new URL(Public._INTERFACE_DEL_LEAVE_GROUP.replace("{group_id}", id));
                    break;
                case _DEL_TYPE_PHOTOS_UNLIKE:
                    url = new URL(Public._INTERFACE_DEL_UNLIKE_PHOTO.replace("{group_id}", id));
                    break;
                default:
                    return url;
            }
            FNNetworkLog.d(_TAG, url.toString());
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
        }
        return url;
    }
}
