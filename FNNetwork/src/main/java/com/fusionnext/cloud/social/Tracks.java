package com.fusionnext.cloud.social;

import android.content.Context;

import com.fusionnext.cloud.datalink.http.HttpDelete;
import com.fusionnext.cloud.datalink.http.HttpGet;
import com.fusionnext.cloud.datalink.http.HttpParam;
import com.fusionnext.cloud.datalink.http.HttpPost;
import com.fusionnext.cloud.datalink.http.HttpPut;
import com.fusionnext.cloud.social.objects.Comment;
import com.fusionnext.cloud.social.objects.Likes;
import com.fusionnext.cloud.social.objects.Photo;
import com.fusionnext.cloud.social.objects.Point;
import com.fusionnext.cloud.social.objects.Tag;
import com.fusionnext.cloud.social.objects.Track;
import com.fusionnext.cloud.social.objects.UserProfile;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by Mike Chang on 2016/2/22.
 */
public class Tracks {
    protected final static String _TAG = "Tracks";
    private Context context;

    public enum ListTrackType {
        LIST_TRACK_TYPE_ALL, LIST_TRACK_TYPE_FOLLOWED, LIST_TRACK_TYPE_FAVORITE, LIST_TRACK_TYPE_RECOMMEND, LIST_TRACK_TYPE_RANKING
    }


    public Tracks(Context context) {
        this.context = context;
    }

    public void listTracks(final ListTrackType type, String userId, int order, String pageSize, String pageNum, final SocialAPIHandler.OnTracksListener onTracksListener) {
        HttpParam.HttpSubParam param = new HttpParam.HttpSubParam();

        param.accessToken = Public.getAuthorization(context).getUserAccessToken();

        switch (type) {
            case LIST_TRACK_TYPE_ALL:
                param.url = HttpData.generateHttpGetUrl(HttpData._GET_TYPE_TRACKS_LIST_ALL_TRACKS, order, pageSize, pageNum);
                break;
            case LIST_TRACK_TYPE_FOLLOWED:
                param.url = HttpData.generateHttpGetUrl(HttpData._GET_TYPE_TRACKS_LIST_TRACKS_FOLLOWED, userId, order, pageSize, pageNum);
                break;
            case LIST_TRACK_TYPE_FAVORITE:
                param.url = HttpData.generateHttpGetUrl(HttpData._GET_TYPE_TRACKS_LIST_TRACKS_FAVORITE, order, pageSize, pageNum);
                break;
            case LIST_TRACK_TYPE_RECOMMEND:
                param.url = HttpData.generateHttpGetUrl(HttpData._GET_TYPE_TRACKS_LIST_TRACKS_RECOMMEND, order, pageSize, pageNum);
                break;
            case LIST_TRACK_TYPE_RANKING:
                param.url = HttpData.generateHttpGetUrl(HttpData._GET_TYPE_TRACKS_LIST_TRACKS_RANKING, order, pageSize, pageNum);
                break;
            default:
                param.url = HttpData.generateHttpGetUrl(HttpData._GET_TYPE_TRACKS_LIST_ALL_TRACKS, order, pageSize, pageNum);
                break;
        }
        param.cacheName = "";
        HttpParam[] params = new HttpParam[1];
        params[0] = new HttpParam();
        params[0].params.clear();
        params[0].params.add(param);

        HttpGet httpGet = new HttpGet();
        httpGet.setContext(context);
        httpGet.setTag(HttpGet._HTTP_T_GET_NORMAL_TYPE);
        httpGet.setOnParseJsonListener(new HttpGet.OnParseJsonListener() {
            @Override
            public void onParse(boolean result, String returnStr) {
                if (type == ListTrackType.LIST_TRACK_TYPE_FOLLOWED) {
                    ArrayList<Track> trackList = new ArrayList<Track>();

                    ArrayList<UserProfile> userList = UserProfile.parseUserProfileArray(returnStr);
                    if (userList != null) {
                        for (int i = 0; i < userList.size(); i++) {
                            ArrayList<Track> track = userList.get(i)._trackList;
                            trackList.addAll(track);
                        }
                    }
                    onTracksListener.onListTracks(result, type, trackList);

                } else {
                    onTracksListener.onListTracks(result, type, Track.parseTrackArray(returnStr));
                }
            }
        });
        httpGet.execute(params);
    }

    public void createTrack(final Track track, final SocialAPIHandler.OnTracksListener onTracksListener) {
        if (track == null) {
            return;
        }
        HttpPost.PostParam[] params = new HttpPost.PostParam[1];
        params[0] = new HttpPost.PostParam();
        params[0].accessToken = Public.getAuthorization(context).getUserAccessToken();
        params[0].url = HttpData.generateHttpPostUrl(HttpData._POST_TYPE_TRACKS_CREATE_TRACK);
        params[0].type = HttpData._POST_TYPE_TRACKS_CREATE_TRACK;
        params[0].param = new HashMap<Object, Object>();
        params[0].param.put("title", track._title);
        params[0].param.put("description", track._description);
        params[0].param.put("place", track._place);
        params[0].param.put("cover", track._cover);
        params[0].param.put("days", track._days);
        params[0].param.put("privacy", track._privacy);

        HttpPost post = new HttpPost();
        post.setContext(context);
        post.setOnParseJsonListener(new HttpPost.OnParseJsonListener() {
            @Override
            public void onParse(boolean returnResult, int postType, String id, String returnStr) {
                onTracksListener.onCreateTrack(returnResult, Track.parseTrack(returnStr));
            }
        });
        post.execute(params);
    }

    public void updateTrack(final String trackId, Track track, final SocialAPIHandler.OnTracksListener onTracksListener) {
        if (track == null) {
            return;
        }

        HttpPut.PutParam[] params = new HttpPut.PutParam[1];
        params[0] = new HttpPut.PutParam();
        params[0].accessToken = Public.getAuthorization(context).getUserAccessToken();
        params[0].url = HttpData.generateHttpPutUrl(HttpData._PUT_TYPE_TRACKS_UPDATE_TRACK, trackId);
        params[0].type = HttpData._PUT_TYPE_TRACKS_UPDATE_TRACK;
        params[0].param = new HashMap<Object, Object>();
        params[0].param.put("title", track._title);
        params[0].param.put("description", track._description);
        params[0].param.put("place", track._place);
        params[0].param.put("cover", track._cover);
        params[0].param.put("days", track._days);
        params[0].param.put("privacy", track._privacy);

        HttpPut put = new HttpPut();
        put.setHttpRequestType("PUT");
        put.setOnParseJsonListener(new HttpPut.OnParseJsonListener() {
            @Override
            public void onParse(boolean returnResult, int putType, String returnStr) {
                onTracksListener.onUpdateTrack(returnResult, trackId, Track.parseTrack(returnStr));
            }
        });
        put.setContext(context);
        put.execute(params);
    }

    public void getTrack(final String trackId, final SocialAPIHandler.OnTracksListener onTracksListener) {
        HttpParam.HttpSubParam param = new HttpParam.HttpSubParam();

        param.accessToken = Public.getAuthorization(context).getUserAccessToken();
        param.url = HttpData.generateHttpGetUrl(HttpData._GET_TYPE_TRACKS_GET_TRACK, trackId, 0, null, null);
        param.cacheName = "";
        HttpParam[] params = new HttpParam[1];
        params[0] = new HttpParam();
        params[0].params.clear();
        params[0].params.add(param);

        HttpGet httpGet = new HttpGet();
        httpGet.setContext(context);
        httpGet.setTag(HttpGet._HTTP_T_GET_NORMAL_TYPE);
        httpGet.setOnParseJsonListener(new HttpGet.OnParseJsonListener() {
            @Override
            public void onParse(boolean result, String returnStr) {
                onTracksListener.onGetTrack(result, trackId, Track.parseTrack(returnStr));
            }
        });
        httpGet.execute(params);
    }

    public void listPoints(final String trackId, int order, String pageSize, String pageNum, final SocialAPIHandler.OnTracksListener onTracksListener) {
        HttpParam.HttpSubParam param = new HttpParam.HttpSubParam();

        param.accessToken = Public.getAuthorization(context).getUserAccessToken();
        param.url = HttpData.generateHttpGetUrl(HttpData._GET_TYPE_TRACKS_LIST_POINTS, trackId, order, pageSize, pageNum);
        param.cacheName = "";
        HttpParam[] params = new HttpParam[1];
        params[0] = new HttpParam();
        params[0].params.clear();
        params[0].params.add(param);

        HttpGet httpGet = new HttpGet();
        httpGet.setContext(context);
        httpGet.setTag(HttpGet._HTTP_T_GET_NORMAL_TYPE);
        httpGet.setOnParseJsonListener(new HttpGet.OnParseJsonListener() {
            @Override
            public void onParse(boolean result, String returnStr) {
                onTracksListener.onListPoints(result, trackId, Point.parsePointArray(returnStr));
            }
        });
        httpGet.execute(params);
    }

    public void listPhotos(final String trackId, int order, String pageSize, String pageNum, final SocialAPIHandler.OnTracksListener onTracksListener) {
        HttpParam.HttpSubParam param = new HttpParam.HttpSubParam();

        param.accessToken = Public.getAuthorization(context).getUserAccessToken();
        param.url = HttpData.generateHttpGetUrl(HttpData._GET_TYPE_TRACKS_LIST_PHOTOS, trackId, order, pageSize, pageNum);
        param.cacheName = "";
        HttpParam[] params = new HttpParam[1];
        params[0] = new HttpParam();
        params[0].params.clear();
        params[0].params.add(param);

        HttpGet httpGet = new HttpGet();
        httpGet.setContext(context);
        httpGet.setTag(HttpGet._HTTP_T_GET_NORMAL_TYPE);
        httpGet.setOnParseJsonListener(new HttpGet.OnParseJsonListener() {
            @Override
            public void onParse(boolean result, String returnStr) {
                onTracksListener.onListPhotos(result, trackId, Photo.parsePhotoArray(returnStr));
            }
        });
        httpGet.execute(params);
    }

    public void listComments(final String trackId, int order, String pageSize, String pageNum, final SocialAPIHandler.OnTracksListener onTracksListener) {
        HttpParam.HttpSubParam param = new HttpParam.HttpSubParam();

        param.accessToken = Public.getAuthorization(context).getUserAccessToken();
        param.url = HttpData.generateHttpGetUrl(HttpData._GET_TYPE_TRACKS_LIST_COMMENTS, trackId, order, pageSize, pageNum);
        param.cacheName = "";
        HttpParam[] params = new HttpParam[1];
        params[0] = new HttpParam();
        params[0].params.clear();
        params[0].params.add(param);

        HttpGet httpGet = new HttpGet();
        httpGet.setContext(context);
        httpGet.setTag(HttpGet._HTTP_T_GET_NORMAL_TYPE);
        httpGet.setOnParseJsonListener(new HttpGet.OnParseJsonListener() {
            @Override
            public void onParse(boolean result, String returnStr) {
                onTracksListener.onListComments(result, trackId, Comment.parseCommentArray(returnStr));
            }
        });
        httpGet.execute(params);
    }

    public void like(final String trackId, final SocialAPIHandler.OnTracksListener onTracksListener) {
        HttpPost.PostParam[] params = new HttpPost.PostParam[1];
        params[0] = new HttpPost.PostParam();
        params[0].accessToken = Public.getAuthorization(context).getUserAccessToken();
        params[0].url = HttpData.generateHttpPostUrl(HttpData._POST_TYPE_TRACKS_LIKE, trackId);
        params[0].type = HttpData._POST_TYPE_TRACKS_LIKE;

        HttpPost post = new HttpPost();
        post.setContext(context);
        post.setOnParseJsonListener(new HttpPost.OnParseJsonListener() {
            @Override
            public void onParse(boolean returnResult, int postType, String id, String returnStr) {
                onTracksListener.onResult(returnResult, trackId, postType, returnStr);
            }
        });
        post.execute(params);
    }

    public void unlike(final String trackId, final SocialAPIHandler.OnTracksListener onTracksListener) {
        HttpDelete.DelParam[] params = new HttpDelete.DelParam[1];
        params[0] = new HttpDelete.DelParam();
        params[0].accessToken = Public.getAuthorization(context).getUserAccessToken();
        params[0].url = HttpData.generateHttpDeleteUrl(HttpData._DEL_TYPE_TRACKS_UNLIKE, trackId);
        params[0].type = HttpData._DEL_TYPE_TRACKS_UNLIKE;
        params[0].param = new HashMap<Object, Object>();

        HttpDelete del = new HttpDelete();
        del.setContext(context);
        del.setOnParseJsonListener(new HttpDelete.OnParseJsonListener() {
            @Override
            public void onParse(boolean returnResult, int delType, String returnStr) {
                if (returnStr != null) {
                    onTracksListener.onResult(returnResult, trackId, delType, returnStr);
                }
            }
        });
        del.execute(params);
    }

    public void listLikes(final String trackId, int order, String pageSize, String pageNum, final SocialAPIHandler.OnTracksListener onTracksListener) {
        HttpParam.HttpSubParam param = new HttpParam.HttpSubParam();

        param.accessToken = Public.getAuthorization(context).getUserAccessToken();
        param.url = HttpData.generateHttpGetUrl(HttpData._GET_TYPE_TRACKS_LIST_LIKES, trackId, order, pageSize, pageNum);
        param.cacheName = "";
        HttpParam[] params = new HttpParam[1];
        params[0] = new HttpParam();
        params[0].params.clear();
        params[0].params.add(param);

        HttpGet httpGet = new HttpGet();
        httpGet.setContext(context);
        httpGet.setTag(HttpGet._HTTP_T_GET_NORMAL_TYPE);
        httpGet.setOnParseJsonListener(new HttpGet.OnParseJsonListener() {
            @Override
            public void onParse(boolean result, String returnStr) {
                onTracksListener.onListLikes(result, trackId, Likes.parseLikeArray(returnStr));
            }
        });
        httpGet.execute(params);
    }

    public void listTags(final String trackId, int order, String pageSize, String pageNum, final SocialAPIHandler.OnTracksListener onTracksListener) {
        HttpParam.HttpSubParam param = new HttpParam.HttpSubParam();

        param.accessToken = Public.getAuthorization(context).getUserAccessToken();
        param.url = HttpData.generateHttpGetUrl(HttpData._GET_TYPE_TRACKS_LIST_TAGS, trackId, order, pageSize, pageNum);
        param.cacheName = "";
        HttpParam[] params = new HttpParam[1];
        params[0] = new HttpParam();
        params[0].params.clear();
        params[0].params.add(param);

        HttpGet httpGet = new HttpGet();
        httpGet.setContext(context);
        httpGet.setTag(HttpGet._HTTP_T_GET_NORMAL_TYPE);
        httpGet.setOnParseJsonListener(new HttpGet.OnParseJsonListener() {
            @Override
            public void onParse(boolean result, String returnStr) {
                onTracksListener.onListTags(result, trackId, Tag.parseTagArray(returnStr));
            }
        });
        httpGet.execute(params);
    }
}
