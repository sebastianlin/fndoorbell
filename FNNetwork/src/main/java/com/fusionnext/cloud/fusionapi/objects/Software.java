//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.fusionnext.cloud.fusionapi.objects;

import android.util.Log;

import com.fusionnext.cloud.datalink.json.FNJsonObject;
import com.fusionnext.cloud.fusionapi.Public;

public class Software {
    public String _swId = "";
    public String _clientId = "";
    public String _version = "";
    public String _description = "";
    public String _filename = "";
    public String _downloadUrl = "";
    public String _storeUrl = "";
    public String _createdAt = "";
    public String _updatedAt = "";

    public Software() {
    }

    public void clean() {
        this._swId = "";
        this._clientId = "";
        this._version = "";
        this._description = "";
        this._filename = "";
        this._downloadUrl = "";
        this._storeUrl = "";
        this._createdAt = "";
        this._updatedAt = "";
    }

    public boolean getDataFromJSONObject(FNJsonObject softwareObj) {
        this.clean();
        if (softwareObj == null) {
            Log.w("Software", " No JSON information");
            return false;
        } else {
            try {
                this._swId = softwareObj.getStringDefault("id");
                this._clientId = softwareObj.getStringDefault("client_id");
                this._version = softwareObj.getStringDefault("version");
                this._description = softwareObj.getStringDefault("description");
                this._filename = softwareObj.getStringDefault("filename");
                this._downloadUrl = Public._HOST_V21 + softwareObj.getStringDefault("download_url");
                this._storeUrl = softwareObj.getStringDefault("store_url");
                this._createdAt = softwareObj.getStringDefault("created_at");
                this._updatedAt = softwareObj.getStringDefault("updated_at");
                return true;
            } catch (Exception e) {
                e.printStackTrace();
                return false;
            }
        }
    }
}
