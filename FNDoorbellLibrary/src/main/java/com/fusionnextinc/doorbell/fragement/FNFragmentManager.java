package com.fusionnextinc.doorbell.fragement;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import android.widget.Toast;

import com.fusionnextinc.doorbell.FNException;
import com.fusionnextinc.doorbell.MyApplication;
import com.fusionnextinc.doorbell.R;
import com.fusionnextinc.doorbell.widget.FNActionBar;

import java.util.ArrayList;

/**
 * Created by Mike Chang on 2017/6/21
 */
public class FNFragmentManager {
    private static final String TAG = FNFragmentManager.class.getSimpleName();
    private static FNFragmentManager sFragmentManager;

    private FragmentActivity mActivity;
    private FNActionBar mActionBar;
    private FragmentManager mFragmentManager;
    private boolean mCanFinish = false;
    private ArrayList<FNFragment> mFragmentList;

    public static FNFragmentManager getInstance() {
        synchronized (FNFragmentManager.class) {
            if (sFragmentManager == null) {
                sFragmentManager = new FNFragmentManager();
            }
        }
        return sFragmentManager;
    }

    public static boolean FindFragment(Class<? extends FNFragment> className) {
        synchronized (FNFragmentManager.class) {
            if (sFragmentManager == null) {
                sFragmentManager = new FNFragmentManager();
            }
        }
        return sFragmentManager.isFindFragment(className);
    }

    private FNFragmentManager() {

    }


    public void init(final FragmentActivity activity, FNActionBar actionBar) {
        mActivity = activity;
        mActionBar = actionBar;
        mFragmentManager = activity.getSupportFragmentManager();
        mFragmentList = new ArrayList<>();
    }

    public FNActionBar getActionBar() {
        return mActionBar;
    }

    public void onBackPressed() {
        if (mFragmentList.size() == 1) {
            if (mCanFinish) {
                mActivity.supportFinishAfterTransition();
            } else {
                mCanFinish = true;
                MyApplication.showToastOnUiThread(R.string.fn_msg_back_again, Toast.LENGTH_SHORT);
                MyApplication.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        mCanFinish = false;
                    }
                }, 2000);
            }
        } else {
            FragmentTransaction transaction = mFragmentManager.beginTransaction();
            FNFragment removeFragment = mFragmentList.remove(mFragmentList.size() - 1);
            transaction.remove(removeFragment);
            if (mFragmentList.size() > 0) {
                transaction.attach(mFragmentList.get(mFragmentList.size() - 1)).commit();
            }
        }
    }

    public void onBackPressed(boolean exitDirectly) {
        final boolean[] canFinish = {exitDirectly};

        if (mFragmentList.size() == 1) {
            if (canFinish[0]) {
                mActivity.supportFinishAfterTransition();
            } else {
                canFinish[0] = true;
                MyApplication.showToastOnUiThread(R.string.fn_msg_back_again, Toast.LENGTH_SHORT);
                MyApplication.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        canFinish[0] = false;
                    }
                }, 2000);
            }
        } else {
            FragmentTransaction transaction = mFragmentManager.beginTransaction();
            FNFragment removeFragment = mFragmentList.remove(mFragmentList.size() - 1);
            transaction.remove(removeFragment);
            if (mFragmentList.size() > 0) {
                transaction.attach(mFragmentList.get(mFragmentList.size() - 1)).commit();
            }
        }
    }

    public boolean callBackPressed() {
        for (Fragment fragment : mFragmentManager.getFragments()) {
            if (fragment != null && fragment instanceof FNFragment && fragment.isResumed()) {
                ((FNFragment) fragment).onBackPressed();
                return true;
            }
        }
        return false;
    }

    public boolean isResumed(Class<? extends FNFragment> className) {
        for (Fragment fragment : mFragmentManager.getFragments()) {
            if (fragment != null && fragment instanceof FNFragment && fragment.isResumed() && fragment.getClass().equals(className)) {
                return true;
            }
        }
        return false;
    }

    public boolean isFindFragment(Class<? extends FNFragment> className) {
        if (mFragmentManager == null)
            throw new FNException(TAG, "please call FNFragmentManager.init() first");

        for (Fragment fragment : mFragmentManager.getFragments()) {
            if (fragment != null && fragment instanceof FNFragment &&  fragment.getClass().equals(className)) {
                return true;
            }
        }
        return false;
    }


    public void openFragment(final FNFragment fragment, final Class<? extends FNFragment> clearStack) {
        if (mFragmentManager == null)
            throw new FNException(TAG, "please call FNFragmentManager.init() first");
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                // fix IllegalStateException
                mActivity.onStateNotSaved();
                FragmentTransaction transaction = mFragmentManager.beginTransaction();
                if (clearStack != null) {
                    for (int i = 0; i < mFragmentList.size(); i++) {
                        FNFragment removeFragment = mFragmentList.get(i);
                        if (removeFragment.getClass().getName().equals(clearStack.getName())) {
                            transaction.remove(removeFragment);
                            mFragmentList.remove(i);
                            i--;
                        }
                    }
                }
                if (mFragmentList.size() > 0) {
                    transaction.detach(mFragmentList.get(mFragmentList.size() - 1));
                }
                mFragmentList.add(fragment);
                transaction.add(R.id.fl_main, fragment).commit();
            }
        });
    }

    public void openFragment(final FNFragment fragment, final boolean clearOtherStack) {
        if (mFragmentManager == null)
            throw new FNException(TAG, "please call FNFragmentManager.init() first");
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                // fix IllegalStateException
                mActivity.onStateNotSaved();
                FragmentTransaction transaction = mFragmentManager.beginTransaction();
                if (clearOtherStack) {
                    for (FNFragment removeFragment : mFragmentList) {
                        transaction.remove(removeFragment);
                    }
                    mFragmentList.clear();
                } else if (mFragmentList.size() > 0) {
                    transaction.detach(mFragmentList.get(mFragmentList.size() - 1));
                }
                mFragmentList.add(fragment);
                transaction.add(R.id.fl_main, fragment).commit();
            }
        });
    }

    public void replaceFragment(final FNFragment fragment) {
        if (mFragmentManager == null)
            throw new FNException(TAG, "please call FNFragmentManager.init() first");
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if (mFragmentManager != null) {
                    FragmentTransaction transaction = mFragmentManager.beginTransaction();
                    Fragment currentFrag = mFragmentManager.findFragmentById(R.id.fl_main);

                    //Check if the new Fragment is the same
                    //If it is, don't add to the back stack
                    if (currentFrag != null && currentFrag.getClass().equals(fragment.getClass())) {
                        transaction.replace(R.id.fl_main, fragment).commit();
                    } else {
                        if (mFragmentList.size() > 0) {
                            transaction.detach(mFragmentList.get(mFragmentList.size() - 1));
                        }
                        mFragmentList.add(fragment);
                        transaction.add(R.id.fl_main, fragment).commit();
                    }
                }
            }
        });
    }

    public void onRequestPermissionsResult(final int requestCode, final String[] permissions, final int[] grantResults) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                for (Fragment fragment : mFragmentManager.getFragments()) {
                    if (fragment != null && fragment.isAdded()) {
                        fragment.onRequestPermissionsResult(requestCode, permissions, grantResults);
                    }
                }
            }
        });
    }

    private void runOnUiThread(Runnable action) {
        mActivity.runOnUiThread(action);
    }
}
