package com.fusionnextinc.doorbell.manager.thingsManager;

import android.content.Context;
import android.widget.Toast;

import com.fusionnext.cloud.datalink.json.FNJsonArray;
import com.fusionnext.cloud.datalink.json.FNJsonObject;
import com.fusionnext.cloud.iot.HttpData;
import com.fusionnext.cloud.iot.IOTHandler;
import com.fusionnext.cloud.iot.Ota;
import com.fusionnext.cloud.iot.Settings;
import com.fusionnext.cloud.iot.SmartPlug;
import com.fusionnext.cloud.iot.Things;
import com.fusionnext.cloud.iot.User;
import com.fusionnext.cloud.iot.objects.ErrorObj;
import com.fusionnext.cloud.iot.objects.Event;
import com.fusionnext.cloud.iot.objects.Firmware;
import com.fusionnext.cloud.iot.objects.Setting;
import com.fusionnext.cloud.iot.objects.Software;
import com.fusionnext.cloud.iot.objects.State;
import com.fusionnext.cloud.iot.objects.THINGSTATUS;
import com.fusionnext.cloud.iot.objects.Thing;
import com.fusionnext.cloud.iot.objects.UserProfile;
import com.fusionnext.cloud.utils.Times;
import com.fusionnextinc.doorbell.R;
import com.fusionnextinc.doorbell.manager.data.AppDataBase;
import com.fusionnextinc.doorbell.manager.data.AppDataSchedule;
import com.fusionnextinc.doorbell.manager.data.AppDataThing;
import com.fusionnextinc.doorbell.manager.data.DataManager;
import com.fusionnextinc.doorbell.manager.data.ListMap;
import com.fusionnextinc.doorbell.manager.mqtt.MqttListener;
import com.fusionnextinc.doorbell.manager.mqtt.MqttManager;
import com.fusionnextinc.doorbell.manager.mqtt.MyMqtt;
import com.fusionnextinc.doorbell.objects.FNError;
import com.fusionnextinc.doorbell.objects.FNEvent;
import com.fusionnextinc.doorbell.objects.FNFirmware;
import com.fusionnextinc.doorbell.objects.FNSchedule;
import com.fusionnextinc.doorbell.objects.FNState;
import com.fusionnextinc.doorbell.objects.FNThing;
import com.fusionnextinc.doorbell.objects.FNUserProfile;
import com.fusionnextinc.doorbell.utils.FNLog;

import org.json.JSONException;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by Mike Chang on 2017/6/8.
 */
public class ThingsManager {
    private final static String TAG = "ThingsManager";
    private volatile static ThingsManager instance;
    private Things things;
    private User user;
    private Settings settings;
    private Ota ota;
    private SmartPlug smartPlug;
    private Context context;
    private ThingsHandler.ThingsListener thingsListener;
    private ThingsHandler.mqttListener mThingMqttListener;
    private final String[] ringtonesFile = new String[]{"sound1.wav", "sound2.wav", "sound3.wav", "sound4.wav", "sound5.wav", "sound6.wav", "sound7.wav", "sound8.wav", "sound9.wav", "sound10.wav", "sound11.wav","bed_alert.wav", "silence.wav"};
    private final int[] ringtonesName = new int[]{R.string.fn_tittle_ringtone_name1, R.string.fn_tittle_ringtone_name2, R.string.fn_tittle_ringtone_name3, R.string.fn_tittle_ringtone_name4, R.string.fn_tittle_ringtone_name5
            , R.string.fn_tittle_ringtone_name6, R.string.fn_tittle_ringtone_name7, R.string.fn_tittle_ringtone_name8, R.string.fn_tittle_ringtone_name9, R.string.fn_tittle_ringtone_name10, R.string.fn_tittle_ringtone_name11,R.string.fn_txt_bed_alert, R.string.fn_txt_call_mute};
    private final int[] ringtonesRaw = new int[]{R.raw.sound1, R.raw.sound2, R.raw.sound3, R.raw.sound4, R.raw.sound5, R.raw.sound6, R.raw.sound7, R.raw.sound8, R.raw.sound9, R.raw.sound10, R.raw.sound11, R.raw.bed_alert,R.raw.silence};
    private ArrayList<HashMap<String, Object>> ringtoneList = new ArrayList<>();
    public final String SETTINGS_RINGTONE_SETTINGS = "ringtone";
    public final String SETTINGS_SMARTPLUG_TIMER = "timer";
    public final String SETTINGS_SMARTPLUG_SCHEDULE = "schedule";
    public static final String STATES_FIRMWARE_VERSION = "firmware_ver";
    public static final String STATES_BATTERY_LEVEL = "battery_level";
    public static final String STATES_SWITCH_ONOFF = "on";
    private final String EVENTS_PAGE_NUMBER = "50";
    private final String EVENTS_NAME_ALIVE = "alive";
    private static final String TAG_RINGTONES_FILE = "TAG_RINGTONES_FILE";
    private static final String TAG_RINGTONES_NAME = "TAG_RINGTONES_NAME";
    private static final String TAG_RINGTONES_RAW = "TAG_RINGTONES_RAW";
    /*Product ID*/
    public static final int PRODUCT_ID_DOORBELL = 1;
    public static final int PRODUCT_ID_DOORBELL_VOICE = 2;
    public static final int PRODUCT_ID_OTC = 10;
    public static final int PRODUCT_ID_OTBED = 12;
    public static final int PRODUCT_ID_PLUG = 6;

    private final int[] roomTypeString = new int[]{R.string.type_room_cutom, R.string.type_room_living_room, R.string.type_room_bedroom, R.string.type_room_dining_room, R.string.type_room_bathroom, R.string.type_room_kitchen, R.string.type_room_study};
    private final int[] roomNameString = new int[]{R.string.fn_title_room_cutom, R.string.fn_title_room_living_room, R.string.fn_title_room_bedroom, R.string.fn_title_room_dining_room, R.string.fn_title_room_bathroom, R.string.fn_title_room_kitchen, R.string.fn_title_room_study};
    private final int[] roomTypeImage = new int[]{R.drawable.ic_custom, R.drawable.ic_living, R.drawable.ic_bed, R.drawable.ic_dinning, R.drawable.ic_bath
            , R.drawable.ic_kitchen, R.drawable.ic_study};
    private final int[] groupTypeImage = new int[]{R.drawable.ic_create_custom, R.drawable.ic_create_living, R.drawable.ic_create_bed, R.drawable.ic_create_dinning, R.drawable.ic_create_bath
            , R.drawable.ic_create_kitchen, R.drawable.ic_create_study};
    private ArrayList<HashMap<String, Object>> roomList = new ArrayList<>();
    private static final String TAG_ROOM_TYPE = "TAG_ROOM_TYPE";
    private static final String TAG_ROOM_NAME = "TAG_ROOM_NAME";
    private static final String TAG_ROOM_IMAGE = "TAG_ROOM_IMAGE";
    private static final String TAG_GROUP_IMAGE = "TAG_GROUP_IMAGE";

    ArrayList<Thing> mOwnedThingArrayList = null;
    ArrayList<Thing> mSharedThingArrayList = null;

    public static ThingsManager getInstance(Context context) {
        if (instance == null) {
            synchronized (ThingsManager.class) {
                if (instance == null) {
                    instance = new ThingsManager(context);
                }
            }
        }
        return instance;
    }

    private ThingsManager(Context context) {
        things = new Things(context);
        user = new User(context);
        settings = new Settings(context);
        smartPlug = new SmartPlug(context);

        IOTHandler.OnOtaHandlerListener onOtaHandlerListener = new IOTHandler.OnOtaHandlerListener() {
            @Override
            public void onNewFwInfo(boolean result, String thingId, Firmware firmware) {
                if (thingsListener != null) {
                    FNFirmware fnFirmware = null;
                    if (firmware != null) {
                        fnFirmware = FNFirmware.parseFirmware(firmware);
                    }
                    thingsListener.onNewFirmwareVersion(instance, result, thingId, fnFirmware);
                }
            }

            @Override
            public void onNewSwInfo(boolean result, Software software) {

            }

            @Override
            public void onSrartDownload() {

            }

            @Override
            public void onStopDownload() {

            }

            @Override
            public void onDownloadProgress(long progress) {

            }

            @Override
            public void onDownloadComplete() {

            }

            @Override
            public void onError() {

            }

            @Override
            public void onStartFwUpdate(boolean result, String thingId, String fwId) {
                if (thingsListener != null) {
                    thingsListener.onStartFwUpdate(instance, result, thingId, fwId);
                }
            }
        };

        ota = new Ota(context, onOtaHandlerListener);
        this.context = context;
        generateRingtoneList();
        generateRoomList();
    }

    public void setThingsListener(ThingsHandler.ThingsListener thingsListener) {
        this.thingsListener = thingsListener;
    }

    public boolean getAllThingsList(int order, String pageSize, String pageNum) {
        if (user != null) {
            user.listAllThings(order, pageSize, pageNum, onUsersListener);
            return true;
        }
        return false;
    }

    public boolean getOwnedThingsList(int order, String pageSize, String pageNum) {
        if (user != null) {
            user.listOwnedThings(order, pageSize, pageNum, onUsersListener);
            return true;
        }
        return false;
    }

    public boolean getInvitedThingsList(int order, String pageSize, String pageNum) {
        if (user != null) {
            user.listInvitedThings(order, pageSize, pageNum, onUsersListener);
            return true;
        }
        return false;
    }

    public boolean getSharedThingsList(int order, String pageSize, String pageNum) {
        if (user != null) {
            user.listSharedThings(order, pageSize, pageNum, onUsersListener);
            return true;
        }
        return false;
    }

    public boolean getEventsList(final String thingId, String name, int order, String pageSize, String pageNum) {
        if (things != null) {
            things.listEvents(thingId, name, order, pageSize, pageNum, onThingsListener);
            return true;
        }
        return false;
    }

    public boolean getEventsList(final String thingId, String name, String startTime, String endTime, int order, String pageSize, String pageNum) {
        if (things != null) {
            things.listEvents(thingId, name, startTime, endTime, order, pageSize, pageNum, onThingsListener);
            return true;
        }
        return false;
    }

    public boolean getStatesList(final String thingId, String name) {
        if (things != null) {
            things.listStates(thingId, name, HttpData._GET_TYPE_SORT_ORDER_DESC, "1", "1", onThingsListener);
            return true;
        }
        return false;
    }

    public boolean getSettingsList(final String thingId, String name, int order, String pageSize, String pageNum) {
        if (things != null) {
            things.listSettings(thingId, name, order, pageSize, pageNum, onThingsListener);
            return true;
        }
        return false;
    }

    public boolean getInvitedUserList(final String thingId, int order, String pageSize, String pageNum) {
        if (things != null) {
            things.listInvitedUsers(thingId, order, pageSize, pageNum, onThingsListener);
            return true;
        }
        return false;
    }

    public boolean getSharedUserList(final String thingId, int order, String pageSize, String pageNum) {
        if (things != null) {
            things.listSharedUsers(thingId, order, pageSize, pageNum, onThingsListener);
            return true;
        }
        return false;
    }

    public boolean updateThing(final String thingId, FNThing fnThing) {
        if (things != null && fnThing != null && thingId != null) {
            things.updateThing(thingId, FNThing.parseFNThing(context, fnThing), onThingsListener);
            return true;
        }
        return false;
    }

    public boolean createClaimCode(final String thingName) {
        if (things != null && thingName != null) {
            things.createClaimCode(thingName, onThingsListener);
            return true;
        }
        return false;
    }

    public boolean createThing(final FNThing fnThing) {
        if (things != null && fnThing != null) {
            things.createThing(FNThing.parseFNThing(context, fnThing), onThingsListener);
            return true;
        }
        return false;
    }

    public boolean deleteThing(final String thingId) {
        if (thingId != null) {
            things.deleteThing(thingId, onThingsListener);
            return true;
        }
        return false;
    }

    public boolean takeCallSlot(String slotId) {
        if (slotId != null) {
            things.takeCallSlot(slotId, onThingsListener);
            return true;
        }
        return false;
    }

    public boolean closeCallSlot(String slotId) {
        if (slotId != null) {
            things.closeCallSlot(slotId, onThingsListener);
            return true;
        }
        return false;
    }

    public boolean shareInvite(final String thingId, final String email) {
        if (email != null && things != null) {
            things.invite(thingId, email, onThingsListener);
            return true;
        }
        return false;
    }

    public boolean shareAccept(final String thingId) {
        if (things != null) {
            things.accept(thingId, onThingsListener);
            return true;
        }
        return false;
    }

    public boolean shareReject(final String thingId) {
        if (things != null) {
            things.reject(thingId, null, onThingsListener);
            return true;
        }
        return false;
    }

    public boolean shareReject(final String thingId, String email) {
        if (things != null && email != null) {
            things.reject(thingId, email, onThingsListener);
            return true;
        }
        return false;
    }

    public boolean changeRingtone(final String thingId, final int ringtoneId) {
        if (thingId != null && ringtoneId >= 0) {
            FNJsonObject ringObj = new FNJsonObject();
            try {
                ringObj.put("ringtone", getRingtoneFileById(ringtoneId));
            } catch (JSONException e) {
                e.printStackTrace();
            }
            Setting setting = new Setting();
            setting._thingId = thingId;
            setting._value = ringObj.toString();
            setting._name = SETTINGS_RINGTONE_SETTINGS;
            settings.createSetting(setting, setting._name, onSettingsListener);
            return true;
        }
        return false;
    }

    public boolean setTimer(final String thingId, boolean deviceOn, long timeStamp) {
        if (thingId != null) {
            FNJsonObject timerObj = new FNJsonObject();
            try {
                timerObj.put("on", deviceOn);
                timerObj.put("ts", timeStamp);
            } catch (JSONException e) {
                e.printStackTrace();
            }

            Setting setting = new Setting();
            setting._thingId = thingId;
            setting._name = SETTINGS_SMARTPLUG_TIMER;
            setting._value = timerObj.toString();
            settings.createSetting(setting, setting._name, onSettingsListener);
            return true;
        }
        return false;
    }

    public boolean setSchedule(final String thingId, ArrayList<FNSchedule> scheduleList) {
        if (thingId != null) {
            FNJsonArray scheduleArray = new FNJsonArray();

            for (FNSchedule fnSchedule : scheduleList) {
                FNJsonObject scheduleObj = new FNJsonObject();
                try {
                    scheduleObj.put("schedule_id", fnSchedule.getScheduleId());
                    scheduleObj.put("active", fnSchedule.isSwitchOn());
                    scheduleObj.put("on", fnSchedule.isDeviceOn());
                    scheduleObj.put("h", fnSchedule.getUTCHour());
                    scheduleObj.put("m", fnSchedule.getUTCMin());
                    scheduleObj.put("s", fnSchedule.getSec());
                    scheduleObj.put("weekdays", fnSchedule.getWeeksJsonArray());
                    scheduleArray.put(scheduleObj);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            Setting setting = new Setting();
            setting._thingId = thingId;
            setting._value = scheduleArray.toString();
            setting._name = SETTINGS_SMARTPLUG_SCHEDULE;
            settings.createSetting(setting, setting._name, onSettingsListener);
            return true;
        }
        return false;
    }

    private final IOTHandler.OnUsersListener onUsersListener = new IOTHandler.OnUsersListener() {

        @Override
        public void onResult(boolean result, String id, int postType, ErrorObj error) {

        }
        @Override
        public void onRsendUser(boolean result, ErrorObj error) {

        }
        @Override
        public void onCreateUser(boolean result, ErrorObj error) {

        }

        @Override
        public void onGetUser(boolean b, ErrorObj error) {

        }

        @Override
        public void onUpdateUser(boolean b, ErrorObj error) {

        }

        @Override
        public void onListAllThings(boolean result, String userId, ArrayList<Thing> ownedThingArrayList, ArrayList<Thing> sharedThingArrayList, ArrayList<Thing> invitedThingArrayList, ErrorObj errorObj) {
            mOwnedThingArrayList = ownedThingArrayList;
            mSharedThingArrayList = sharedThingArrayList;
            if (thingsListener != null) {
                ArrayList<FNThing> fnOwnedThingsList = updateThingInfoToDB(AppDataThing.THING_TYPE_THINGS_OWNED, ownedThingArrayList);
                ArrayList<FNThing> fnSharedThingsList = updateThingInfoToDB(AppDataThing.THING_TYPE_THINGS_SHARED, sharedThingArrayList);
                ArrayList<FNThing> fnInvitedThingsList = updateThingInfoToDB(AppDataThing.THING_TYPE_THINGS_INVITED, invitedThingArrayList);
                thingsListener.onListUserAllThings(instance, result, userId, fnOwnedThingsList, fnSharedThingsList, fnInvitedThingsList, FNError.parseError(errorObj));
            }
        }

        @Override
        public void onListOwnedThings(boolean result, String userId, ArrayList<Thing> thingArrayList, ErrorObj error) {
            if (thingsListener != null) {
                ArrayList<FNThing> fnThingsList = updateThingInfoToDB(AppDataThing.THING_TYPE_THINGS_OWNED, thingArrayList);
                thingsListener.onListUserOwnedThings(instance, result, userId, fnThingsList, FNError.parseError(error));
            }
        }

        @Override
        public void onListInvitedThings(boolean result, String userId, ArrayList<Thing> thingArrayList, ErrorObj error) {
            if (thingsListener != null) {
                ArrayList<FNThing> fnThingsList = updateThingInfoToDB(AppDataThing.THING_TYPE_THINGS_INVITED, thingArrayList);
                thingsListener.onListUserInvitedThings(instance, result, userId, fnThingsList, FNError.parseError(error));
            }
        }

        @Override
        public void onListSharedThings(boolean result, String userId, ArrayList<Thing> thingArrayList, ErrorObj error) {
            if (thingsListener != null) {
                ArrayList<FNThing> fnThingsList = updateThingInfoToDB(AppDataThing.THING_TYPE_THINGS_SHARED, thingArrayList);
                thingsListener.onListUserSharedThings(instance, result, userId, fnThingsList, FNError.parseError(error));
            }
        }

        @Override
        public void onResetPassword(boolean result, ErrorObj error) {

        }

        @Override
        public void onSetPassword(boolean result, ErrorObj error) {

        }
    };

    private final IOTHandler.OnThingsListener onThingsListener = new IOTHandler.OnThingsListener() {
        @Override
        public void onCreateClaimCode(boolean result, String claimCode, ErrorObj error) {
            if (thingsListener != null) {
                thingsListener.onCreateClaimCode(instance, result, claimCode, FNError.parseError(error));
            }
        }

        @Override
        public void onCreateThing(boolean result, Thing thing, ErrorObj error) {
            if (thingsListener != null) {
                FNThing fnThing = null;
                if (thing != null && result) {
                    fnThing = FNThing.parseThing(context, thing);
                    if (fnThing != null) {
                        updateThingInfoToDBById(fnThing.getThingId(), fnThing);
                    }
                }
                thingsListener.onCreateThing(instance, result, fnThing, FNError.parseError(error));
            }
        }

        @Override
        public void onUpdateThing(boolean result, Thing thing, ErrorObj error) {
            if (thingsListener != null) {
                FNThing fnThing = null;
                if (thing != null && result) {
                    fnThing = FNThing.parseThing(context, thing);
                    if (fnThing != null) {
                        updateThingInfoToDBById(fnThing.getThingId(), fnThing);
                    }
                }
                thingsListener.onUpdateThing(instance, result, fnThing, FNError.parseError(error));
            }
        }

        @Override
        public void onDeleteThing(boolean result, ErrorObj error) {
            if (thingsListener != null) {
                thingsListener.onDeleteThing(instance, result, FNError.parseError(error));
            }
        }

        @Override
        public void onGetThing(boolean result, Thing thing, ErrorObj errorObj) {
            if (thingsListener != null) {
                FNThing fnThing = null;
                if (thing != null && result) {
                    fnThing = FNThing.parseThing(context, thing);
                    if (fnThing != null) {
                        updateThingInfoToDBById(fnThing.getThingId(), fnThing);
                    }
                    //getNewThingEvents(fnThing.getSsid());
                }
                thingsListener.onGetThing(instance, result, fnThing, FNError.parseError(errorObj));
            }
        }

        @Override
        public void onListStates(boolean result, String thingId, ArrayList<State> stateList, ErrorObj error) {
            ArrayList<FNState> fnStateList = null;

            if (stateList != null) {
                fnStateList = FNState.parseStateArray(stateList);
            }
            if (fnStateList != null && fnStateList.size() == 1) {
                if (fnStateList.get(0).getName().equals(STATES_BATTERY_LEVEL)) {
                    updateThingBatteryLevelToDB(thingId, fnStateList.get(0).getValue());
                    if (thingsListener != null) {
                        thingsListener.onCurBatteryLevel(instance, result, thingId, fnStateList.get(0).getValue(), FNError.parseError(error));
                    }
                } else if (fnStateList.get(0).getName().equals(STATES_FIRMWARE_VERSION)) {
                    updateThingFirmwareVersionToDB(thingId, fnStateList.get(0).getValue());
                    if (thingsListener != null) {
                        thingsListener.onCurFirmwareVersion(instance, result, thingId, fnStateList.get(0).getValue(), FNError.parseError(error));
                    }
                } else if (fnStateList.get(0).getName().equals(STATES_SWITCH_ONOFF)) {
                    int switchValue = Integer.valueOf(fnStateList.get(0).getValue());
                    boolean isSwitch = (switchValue == 1) ? true : false;

                    updateThingSwitchedToDB(thingId, Integer.valueOf(fnStateList.get(0).getValue()));
                    if (thingsListener != null) {
                        thingsListener.onCurSwitchedStatus(instance, result, thingId, isSwitch, FNError.parseError(error));
                    }
                }
            }
        }

        @Override
        public void onListSettings(boolean result, String settingName, String thingId, ArrayList<Setting> settingList, ErrorObj error) {
            String ringtoneId = null;

            if (settingList != null) {
                if (settingName.equals(SETTINGS_RINGTONE_SETTINGS)) {
                    if (settingList.size() > 0) {
                        for (int i = 0; i < ringtonesFile.length; i++) {
                            if (settingList.get(0)._value.equals(ringtonesFile[i])) {
                                ringtoneId = String.valueOf(i);
                                updateThingRingtoneToDB(thingId, ringtoneId);
                                break;
                            }
                        }
                    }
                    if (thingsListener != null) {
                        thingsListener.onCurRingTone(instance, result, thingId, ringtoneId, FNError.parseError(error));
                    }
                } else if (settingName.equals(SETTINGS_SMARTPLUG_SCHEDULE)) {
                    ArrayList<FNSchedule> scheduleList = null;
                    if (settingList.size() > 0) {
                        scheduleList = FNSchedule.parseScheduleArray(settingList.get(0)._value);
                        AppDataSchedule.getInstance(context).updateScheduleList(AppDataBase.SCHEDULE_TYPE_NORMAL, scheduleList, thingId);
                    }
                    if (thingsListener != null) {
                        thingsListener.onGetScheduleList(instance, result, thingId, scheduleList, FNError.parseError(error));
                    }
                }
            }
        }

        @Override
        public void onListEvents(final boolean result, final String thingId, final String eventName, final ArrayList<Event> eventList, final ErrorObj error) {
            if (thingsListener != null) {
                new Thread(new Runnable() {
                    public void run() {
                        ArrayList<FNEvent> fnEventList = null;
                        if (eventList != null) {
                            fnEventList = FNEvent.parseEventArray(eventList);
                        }
                        thingsListener.onListThingEvents(instance, result, thingId, eventName, fnEventList, FNError.parseError(error));
                    }
                }).start();
            }
        }

        @Override
        public void onListEventsByTime(final boolean result, final String thingId, final String eventName, final String startTime, final String endTime, final ArrayList<Event> eventList, int eventCount, final ErrorObj error) {
            if (result && eventList != null) {
                String count = "0";
                try {
                    count = String.valueOf(eventCount);
                } catch (NumberFormatException e) {
                    e.printStackTrace();
                }
                updateThingEventCountsToDB(thingId, count);
            }
            if (thingsListener != null) {
                new Thread(new Runnable() {
                    public void run() {
                        ArrayList<FNEvent> fnEventList = null;
                        if (eventList != null) {
                            fnEventList = FNEvent.parseEventArray(eventList);
                        }
                        for (FNEvent event : fnEventList) {
                            //Refresh the alive state of thing
                            if (event.getName().equals(EVENTS_NAME_ALIVE)) {
                                Date date = new Date();
                                date.setTime(System.currentTimeMillis() - (7*24 * 3600 * 1000));
                                String currentTime = Times.getDateString(date, Times._TIME_FORMAT_STANDARD);

                                //updateThingOnlineToDB(thingId, Times.compareTimes(event.getCreatedAt(), currentTime) ? THINGSTATUS.ONLINE : THINGSTATUS.OFFLINE);
                                break;
                            }
                        }
                        thingsListener.onListThingEventsByTime(instance, result, thingId, eventName, startTime, endTime, fnEventList, FNError.parseError(error));
                    }
                }).start();
            }
        }

        @Override
        public void onListInvitedUsers(boolean result, String thingId, ArrayList<UserProfile> userList, ErrorObj error) {
            if (result && userList != null) {
                String count = "0";
                try {
                    count = String.valueOf(userList.size());
                } catch (NumberFormatException e) {
                    e.printStackTrace();
                }
                updateThingInviteesCountsToDB(thingId, count);
            }
            if (thingsListener != null) {
                ArrayList<FNUserProfile> fnUserList = null;

                if (userList != null) {
                    fnUserList = FNUserProfile.parseUserProfileArray(userList);
                }
                thingsListener.onListInvitedUsers(instance, result, thingId, fnUserList, FNError.parseError(error));
            }
        }

        @Override
        public void onListSharedUsers(boolean result, String id, ArrayList<UserProfile> userList, ErrorObj error) {
            if (thingsListener != null) {
                ArrayList<FNUserProfile> fnUserList = null;

                if (userList != null) {
                    fnUserList = FNUserProfile.parseUserProfileArray(userList);
                }
                thingsListener.onListSharedUsers(instance, result, id, fnUserList, FNError.parseError(error));
            }
        }

        @Override
        public void onInvite(boolean result, String thingId, ErrorObj error) {
            if (thingsListener != null) {
                thingsListener.onShareInvited(instance, result, thingId, FNError.parseError(error));
            }
        }

        @Override
        public void onAccept(boolean result, String thingId, ErrorObj error) {
            if (thingsListener != null) {
                thingsListener.onShareAccepted(instance, result, thingId, FNError.parseError(error));
            }
        }

        @Override
        public void onReject(boolean result, String thingId, ErrorObj error) {
            if (thingsListener != null) {
                thingsListener.onShareRejected(instance, result, thingId, FNError.parseError(error));
            }
        }

        @Override
        public void onCloseCallSlot(boolean result, ErrorObj error) {
            if (thingsListener != null) {
                thingsListener.onCloseCallSlot(instance, result, FNError.parseError(error));
            }
        }

        @Override
        public void onTakeCallSlot(boolean result, String thingId, String callSlot, ErrorObj error) {
            if (thingsListener != null) {
                thingsListener.onTakeCallSlot(instance, result, thingId, callSlot, FNError.parseError(error));
            }
        }
    };

    private final IOTHandler.OnSettingsListener onSettingsListener = new IOTHandler.OnSettingsListener() {
        @Override
        public void onCreateSetting(boolean result, String settingType, Setting setting, ErrorObj error) {
            if (thingsListener != null) {
                if (settingType.equals(SETTINGS_RINGTONE_SETTINGS)) {
                    if (setting != null) {
                        FNJsonObject ringJsonObj = null;
                        String ringtone = "";
                        try {
                            ringJsonObj = new FNJsonObject(setting._value);
                            ringtone = ringJsonObj.getString("ringtone");
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        thingsListener.onChangeRingtone(instance, result, setting._thingId, getRingtoneIdByFile(ringtone));
                    } else {
                        thingsListener.onChangeRingtone(instance, result, null, 0);
                    }
                } else if (settingType.equals(SETTINGS_SMARTPLUG_TIMER)) {
                    if (setting != null) {
                        thingsListener.onSetTimer(instance, result, setting._thingId, setting._value);
                    } else {
                        thingsListener.onSetTimer(instance, result, null, null);
                    }
                } else if (settingType.equals(SETTINGS_SMARTPLUG_SCHEDULE)) {
                    if (setting != null) {
                        thingsListener.onSetSchedule(instance, result, setting._thingId, setting._value);
                    } else {
                        thingsListener.onSetSchedule(instance, result, null, null);
                    }
                }
            }
        }
    };

    private void generateRingtoneList() {
        for (int i = 0; i < ringtonesFile.length; i++) {
            HashMap<String, Object> item = new HashMap<>();
            item.put(TAG_RINGTONES_FILE, ringtonesFile[i]);
            item.put(TAG_RINGTONES_NAME, context.getString(ringtonesName[i]));
            item.put(TAG_RINGTONES_RAW, ringtonesRaw[i]);
            ringtoneList.add(item);
        }
    }

    private void generateRoomList() {
        for (int i = 0; i < roomTypeImage.length; i++) {
            HashMap<String, Object> item = new HashMap<>();
            item.put(TAG_ROOM_TYPE, context.getString(roomTypeString[i]));
            item.put(TAG_ROOM_NAME, context.getString(roomNameString[i]));
            item.put(TAG_ROOM_IMAGE, roomTypeImage[i]);
            item.put(TAG_GROUP_IMAGE, groupTypeImage[i]);
            roomList.add(item);
        }
    }

    public ArrayList<HashMap<String, Object>> getRingtoneList() {
        return ringtoneList;
    }

    public ArrayList<HashMap<String, Object>> getRoomList() {
        return roomList;
    }

    public String getRoomStringById(int roomId) {
        if (roomList != null && roomId >= 0 && roomId < roomList.size()) {
            return roomList.get(roomId).get(ThingsManager.TAG_ROOM_NAME).toString();
        }
        return null;
    }

    public String getRoomTypeById(int roomId) {
        if (roomList != null && roomId >= 0 && roomId < roomList.size()) {
            return roomList.get(roomId).get(ThingsManager.TAG_ROOM_TYPE).toString();
        }
        return null;
    }

    public int getRoomImageById(int roomId) {
        if (roomList != null && roomId >= 0 && roomId < roomList.size()) {
            return (int) roomList.get(roomId).get(ThingsManager.TAG_ROOM_IMAGE);
        }
        return -1;
    }

    public int getRoomImageByType(String groupType) {
        if (roomList != null && groupType != null) {
            for (int i = 0; i <= roomList.size(); i++) {
                if (groupType.equals(roomList.get(i).get(ThingsManager.TAG_ROOM_TYPE))) {
                    return (int) roomList.get(i).get(ThingsManager.TAG_ROOM_IMAGE);
                }
            }
        }
        return (int) roomList.get(0).get(ThingsManager.TAG_ROOM_IMAGE);
    }

    public int getGroupImageByType(String groupType) {
        if (roomList != null && groupType != null) {
            for (int i = 0; i <= roomList.size(); i++) {
                if (groupType.equals(roomList.get(i).get(ThingsManager.TAG_ROOM_TYPE))) {
                    return (int) roomList.get(i).get(ThingsManager.TAG_GROUP_IMAGE);
                }
            }
        }
        return (int) roomList.get(0).get(ThingsManager.TAG_GROUP_IMAGE);
    }


    public String getRingtoneFileById(int ringtoneId) {
        if (ringtoneList != null && ringtoneId >= 0 && ringtoneId < ringtoneList.size()) {
            return ringtoneList.get(ringtoneId).get(ThingsManager.TAG_RINGTONES_FILE).toString();
        }
        return null;
    }

    public String getRingtoneNameById(int ringtoneId) {
        if (ringtoneList != null && ringtoneId >= 0 && ringtoneId < ringtoneList.size()) {
            return ringtoneList.get(ringtoneId).get(ThingsManager.TAG_RINGTONES_NAME).toString();
        }
        return null;
    }

    public int getRingtoneRawById(int ringtoneId) {
        if (ringtoneList != null && ringtoneId >= 0 && ringtoneId < ringtoneList.size()) {
            return (int) ringtoneList.get(ringtoneId).get(ThingsManager.TAG_RINGTONES_RAW);
        }
        return -1;
    }

    public int getRingtoneRawByFile(String file) {
        int ringtoneId = 0;
        if (file != null) {
            ringtoneId = getRingtoneIdByFile(file);
        }
        if (ringtoneList != null && ringtoneId >= 0 && ringtoneId < ringtoneList.size()) {
            return (int) ringtoneList.get(ringtoneId).get(ThingsManager.TAG_RINGTONES_RAW);
        }
        return -1;
    }

    public int getRingtoneIdByFile(String file) {
        HashMap<String, Object> map;
        int ringtoneId = 0;
        if (ringtoneList != null) {
            for (int i = 0; i < ringtoneList.size(); i++) {
                map = ringtoneList.get(i);
                if (map.get(ThingsManager.TAG_RINGTONES_FILE).toString().contains(file)) {
                    ringtoneId = i;
                    break;
                }
            }
        }
        return ringtoneId;
    }

    public boolean startFirmwareUpdate(final String thingId, final String fwId) {
        if (ota != null && fwId != null) {
            ota.startFwUpdate(thingId, fwId);
            return true;
        }
        return false;
    }

    public boolean getNewFirmwareVersion(final FNThing fnThing) {
        if (ota != null && fnThing != null) {
            Thing thing = FNThing.parseFNThing(context, fnThing);
            if (thing != null) {
                ota.getUpdatedFw(thing);
            }
            return true;
        }
        return false;
    }

    private boolean getNewThingEvents(final String thingId) {
        if (thingId != null) {
            FNThing fnThing = AppDataThing.getInstance(context).getThing(thingId);
            if (fnThing != null) {
                String startTime = fnThing.getReadedAt();
                String lastestEventTime = fnThing.getLastestEventTime();
                Date date = new Date();
                date.setTime(System.currentTimeMillis() + (600 * 1000));
                String currentTime = Times.getDateString(date, Times._TIME_FORMAT_STANDARD);

                if (startTime != null && startTime.length() > 0) {
                    if (lastestEventTime != null && lastestEventTime.length() > 0 && Times.compareTimes(lastestEventTime, startTime)) {
                        startTime = Times.localToUTC(startTime, Times._TIME_FORMAT_STANDARD, Times._TIME_FORMAT_STANDARD);
                        currentTime = Times.localToUTC(currentTime, Times._TIME_FORMAT_STANDARD, Times._TIME_FORMAT_STANDARD);
                        getEventsList(fnThing.getThingId(), null, startTime, currentTime, HttpData._GET_TYPE_SORT_ORDER_DESC, EVENTS_PAGE_NUMBER, null);
                    }
                } else {
                    getEventsList(fnThing.getThingId(), null, null, null, HttpData._GET_TYPE_SORT_ORDER_DESC, EVENTS_PAGE_NUMBER, null);
                }
                return true;
            }
        }
        return false;
    }

    public boolean getThingInfo(String thingId) {
        if (thingId != null && thingId.length() > 0) {
            things.getThing(thingId, onThingsListener);
            return true;
            //getCurBatteryLevel(thingId);
            //getCurRingtone(thingId);
            //getCurFirmwareVersion(thingId);
        }
        return false;
    }

    private ArrayList<FNThing> updateThingInfoToDB(int type, ArrayList<Thing> thingArrayList) {
        ArrayList<FNThing> fnThingsList = null;
        ArrayList<FNThing> newFnThingsList = new ArrayList<>();
        if (thingArrayList != null) {
            fnThingsList = FNThing.parseThingArray(context, thingArrayList);
            //keep online value
            for (FNThing thing : fnThingsList) {
                FNThing oldThing = AppDataThing.getInstance(context).getThing(thing.getThingId());
                if (oldThing != null) {
//                    thing.setOnline(oldThing.getOnline());
                } else {
  //                  thing.setOnline(1);
                }
                newFnThingsList.add(thing);
            }
            AppDataThing.getInstance(context).updateThingsList(type, newFnThingsList);
            for (FNThing fnThing : newFnThingsList) {
                if (fnThing.getThingId() != null && fnThing.getThingId().length() > 0) {
                    getNewThingEvents(fnThing.getThingId());
                }
            }
        }
        return fnThingsList;
    }

    public boolean updateThingRingtoneToDB(String thingId, String ringtoneId) {
        FNThing fnThing = AppDataThing.getInstance(context).getThing(thingId);
        if (fnThing == null || ringtoneId == null) {
            return false;
        } else {
            fnThing.setRingtoneId(ringtoneId);
        }
        return AppDataThing.getInstance(context).saveThing(fnThing);
    }

    private boolean updateThingBatteryLevelToDB(String thingId, String batteryLevel) {
        FNThing fnThing = AppDataThing.getInstance(context).getThing(thingId);

        if (fnThing == null || batteryLevel == null) {
            return false;
        } else {
            fnThing.setBatteryLevel(batteryLevel);
        }
        return AppDataThing.getInstance(context).saveThing(fnThing);
    }

    private boolean updateThingFirmwareVersionToDB(String thingId, String fwVersion) {
        FNThing fnThing = AppDataThing.getInstance(context).getThing(thingId);

        if (fnThing == null || fwVersion == null) {
            return false;
        } else {
            fnThing.setFirmwareVer(fwVersion);
        }
        return AppDataThing.getInstance(context).saveThing(fnThing);
    }

    public boolean updateThingReadTimeToDB(String thingId, String time) {
        FNThing fnThing = AppDataThing.getInstance(context).getThing(thingId);

        if (fnThing == null || time == null) {
            return false;
        } else {
            fnThing.setReadedAt(time);
        }
        return AppDataThing.getInstance(context).saveThing(fnThing);
    }

    public boolean updateThingEventCountsToDB(String thingId, String counts) {
        FNThing fnThing = AppDataThing.getInstance(context).getThing(thingId);

        if (fnThing == null || counts == null) {
            return false;
        } else {
            fnThing.setEventCounts(counts);
        }
        return AppDataThing.getInstance(context).saveThing(fnThing);
    }

    public boolean updateThingInviteesCountsToDB(String thingId, String counts) {
        FNThing fnThing = AppDataThing.getInstance(context).getThing(thingId);

        if (fnThing == null || counts == null) {
            return false;
        } else {
            fnThing.setInviteesCounts(counts);
        }
        return AppDataThing.getInstance(context).saveThing(fnThing);
    }

    public boolean updateThingEventCountsAddOne(String thingId) {
        FNThing fnThing = AppDataThing.getInstance(context).getThing(thingId);

        if (fnThing == null) {
            return false;
        } else {
            int count;
            try {
                count = Integer.valueOf(fnThing.getEventCounts()) + 1;
                fnThing.setEventCounts(String.valueOf(count));
                return AppDataThing.getInstance(context).saveThing(fnThing);
            } catch (NumberFormatException e) {
                e.printStackTrace();
            }
        }
        return false;
    }

    private boolean updateThingInfoToDBById(String thingId, FNThing newFnThing) {
        FNThing fnThing = AppDataThing.getInstance(context).getThing(thingId);

        if (fnThing == null || newFnThing == null) {
            return false;
        } else {
            if (newFnThing.getProductId() != null && newFnThing.getProductId().length() > 0) {
                fnThing.setProductId(newFnThing.getProductId());
            }
            if (newFnThing.getProductSlug() != null && newFnThing.getProductSlug().length() > 0) {
                fnThing.setProductSlug(newFnThing.getProductSlug());
            }
            if (newFnThing.getIdentifier() != null && newFnThing.getIdentifier().length() > 0) {
                fnThing.setIdentifier(newFnThing.getIdentifier());
            }
            if (newFnThing.getName() != null && newFnThing.getName().length() > 0) {
                fnThing.setName(newFnThing.getName());
            }
            if (newFnThing.getFirmwareVer() != null && newFnThing.getFirmwareVer().length() > 0) {
                fnThing.setFirmwareVer(newFnThing.getFirmwareVer());
            }
            if (newFnThing.getBatteryLevel() != null && newFnThing.getBatteryLevel().length() > 0) {
                fnThing.setBatteryLevel(newFnThing.getBatteryLevel());
            }
            if (newFnThing.getInviteesCounts() != null && newFnThing.getInviteesCounts().length() > 0) {
                fnThing.setInviteesCounts(newFnThing.getInviteesCounts());
            }
            if (newFnThing.getCreatedAt() != null && newFnThing.getCreatedAt().length() > 0) {
                fnThing.setCreatedAt(newFnThing.getCreatedAt());
            }
            if (newFnThing.getUpdatedAt() != null && newFnThing.getUpdatedAt().length() > 0) {
                fnThing.setUpdatedAt(newFnThing.getUpdatedAt());
            }
            /*if (newFnThing.getRingtoneId() != null && newFnThing.getRingtoneId().length() > 0) {
                fnThing.setRingtoneId(newFnThing.getRingtoneId());
            }*/
            if (newFnThing.getLastestEventTime() != null && newFnThing.getLastestEventTime().length() > 0) {
                fnThing.setLastestEventTime(newFnThing.getLastestEventTime());
            }
        }
        return AppDataThing.getInstance(context).saveThing(fnThing);
    }

    public boolean switchOnOffSmartPlug(final String thingId, final boolean isTurnOn) {
        if (smartPlug != null) {
            smartPlug.switchOnOff(thingId, isTurnOn, onSmartPlugListener);
            return true;
        }
        return false;
    }

    private final IOTHandler.OnSmartPlugListener onSmartPlugListener = new IOTHandler.OnSmartPlugListener() {
        @Override
        public void onSwitchOnOff(boolean result, String thingId, boolean isTurnOn) {
            if (thingsListener != null) {
                thingsListener.onSwitchOnOff(instance, result, thingId, isTurnOn);
            }
        }
    };

    public void subscribeSwitchedTopic(int productId, String identifier) {
        if (productId == PRODUCT_ID_PLUG && identifier != null && identifier.length() > 0) {
            MqttManager.getInstance(context).subTopic(MyMqtt.TOPIC_SWITCH_ONOFF.replace("${thing_identifier}", identifier), 0);
        }
    }

    public void subscribeOnlineTopic(int productId, String identifier) {
        if (productId == PRODUCT_ID_PLUG && identifier != null && identifier.length() > 0) {
            MqttManager.getInstance(context).subTopic(MyMqtt.TOPIC_SWITCH_ONLINE.replace("${thing_identifier}", identifier), 0);
        }
    }

    public void registerMqttListener(ThingsHandler.mqttListener thingMqttListener) {
        mThingMqttListener = thingMqttListener;
        MqttManager.getInstance(context).addMqttListener(mMqttListener);
    }

    public void unregisterMqttListener() {
        mThingMqttListener = null;
        MqttManager.getInstance(context).removeMqttListener(mMqttListener);
    }

    private MqttListener mMqttListener = new MqttListener() {
        @Override
        public void onConnected() {
            Toast.makeText(context, "onConnected", Toast.LENGTH_SHORT).show();
            if (mThingMqttListener != null) {
                mThingMqttListener.onMqttConnected();
            }
        }

        @Override
        public void onFail() {
            Toast.makeText(context, "Connect fail", Toast.LENGTH_SHORT).show();
        }

        @Override
        public void onLost() {
            Toast.makeText(context, "Connect lost", Toast.LENGTH_SHORT).show();
        }

        @Override
        public void onReceive(String message) {
            FNLog.d(TAG, "onReceive" + message);
            try {
                FNJsonObject fnJsonObject = new FNJsonObject(message);
                String topic = fnJsonObject.getString("topic");
                if (topic != null) {
                    String[] topicArray = topic.split("/");
                    String payload = fnJsonObject.getString("payload").trim();
                    String thingId = getThingbyIdentifier(topicArray[2]);
                    if (topicArray != null && thingId != null) {
                        if (topicArray[topicArray.length - 1].equals("switched")) {
                            FNJsonObject payloadObj = new FNJsonObject(payload);
                            boolean isSwitchedOn = payloadObj.getJSONObject("states").getBoolean("on");
                            updateThingSwitchedToDB(thingId, isSwitchedOn ? 1 : 0);
                            if (mThingMqttListener != null) {
                                mThingMqttListener.onDeviceSwitchOn(instance, thingId, isSwitchedOn);
                            }
                        } else if (topicArray[topicArray.length - 1].equals("online")) {
                            THINGSTATUS isoffline = payload.equals("0") ? THINGSTATUS.OFFLINE : THINGSTATUS.SLEEP;
                            updateThingOnlineToDB(thingId, isoffline);
                            if (mThingMqttListener != null) {
                                mThingMqttListener.onDeviceOnLine(instance, thingId, isoffline);
                            }
                        }
                    }
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

        @Override
        public void onSendSucc() {

        }
    };

    private String getThingbyIdentifier(String identifier) {
        ArrayList<String> thingOwnedlist = DataManager.getInstance(context).loadThingList(AppDataThing.THING_TYPE_THINGS_OWNED, null);
        ArrayList<String> thingSharedlist = DataManager.getInstance(context).loadThingList(AppDataThing.THING_TYPE_THINGS_SHARED, null);

        if (thingOwnedlist != null && thingOwnedlist.size() > 0) {
            for (String thingId : thingOwnedlist) {
                Map<String, Object> map = AppDataThing.getInstance(context).getThingMapData(thingId);
                if (map != null) {
                    String id = (String) map.get(AppDataThing.TAG_THING_IDENTIFIER);
                    if (id.equals(identifier)) {
                        return thingId;
                    }
                }
            }
        }

        if (thingSharedlist != null && thingSharedlist.size() > 0) {
            for (String thingId : thingSharedlist) {
                Map<String, Object> map = AppDataThing.getInstance(context).getThingMapData(thingId);
                if (map != null) {
                    String id = (String) map.get(AppDataThing.TAG_THING_IDENTIFIER);
                    if (id.equals(identifier)) {
                        return thingId;
                    }
                }
            }
        }
        return null;
    }

    private boolean updateThingOnlineToDB(String thingId, THINGSTATUS online) {
        FNThing fnThing = AppDataThing.getInstance(context).getThing(thingId);

        if (fnThing == null) {
            return false;
        } else {
            fnThing.setOnline(online);
        }
        return AppDataThing.getInstance(context).saveThing(fnThing);
    }

    private boolean updateThingSwitchedToDB(String thingId, int switched) {
        FNThing fnThing = AppDataThing.getInstance(context).getThing(thingId);

        if (fnThing == null) {
            return false;
        } else {
            fnThing.setSwitched(switched);
        }
        return AppDataThing.getInstance(context).saveThing(fnThing);
    }

    public void subscribeTopic(ListMap ownedlistMap, ListMap sharedlistMap) {
        Map<String, Object> map;

        if (ownedlistMap != null) {
            for (int i = 0; i < ownedlistMap.size(); i++) {
                map = ownedlistMap.get(i);
                String productId = (String) map.get(AppDataThing.TAG_THING_PRODUCT_ID);
                String identifier = (String) map.get(AppDataThing.TAG_THING_IDENTIFIER);
                subscribeSwitchedTopic(Integer.valueOf(productId), identifier);
                subscribeOnlineTopic(Integer.valueOf(productId), identifier);
            }
        }

        if (sharedlistMap != null) {
            for (int i = 0; i < sharedlistMap.size(); i++) {
                map = sharedlistMap.get(i);
                String productId = (String) map.get(AppDataThing.TAG_THING_PRODUCT_ID);
                String identifier = (String) map.get(AppDataThing.TAG_THING_IDENTIFIER);
                subscribeSwitchedTopic(Integer.valueOf(productId), identifier);
                subscribeOnlineTopic(Integer.valueOf(productId), identifier);
            }
        }
    }
}