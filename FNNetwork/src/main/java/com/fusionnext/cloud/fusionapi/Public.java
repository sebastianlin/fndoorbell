package com.fusionnext.cloud.fusionapi;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;

import java.io.File;

public class Public {

    /* private synchronized static MyParameter getMyParam(Context context) {
         return (MyParameter) context.getApplicationContext();
     }*/
    static MyParameter instance = null;

    private synchronized static MyParameter getMyParam(Context context) {
        if (instance == null) {
            instance = new MyParameter();
        }
        return instance;
        //return (MyApplication) context.getApplicationContext();
    }

    public synchronized static DataManager getDataMgr(Context context) {
        return getMyParam(context).getDataMgr(context);
    }

    public synchronized static void setDataMgr(Context context, DataManager dm) {
        getMyParam(context).setDataMgr(dm);
    }

    public synchronized static Authorization getAuthorization(Context context) {
        return getMyParam(context).getAuthorization(context);
    }

    public synchronized static void setAuthorization(Context context, Authorization authorization) {
        getMyParam(context).setAuthorization(authorization);
    }

    //============================================================================
    //
    //Public Parameters for whole project to use
    //public static Account account;

    //Public definitions class
    public static class Defines {
        //Database
        public final static int _DB_VERSION = 28;
        public final static String _DB_NAME = "com.fusionnext.fusionapi.dat";
        public final static int _HTTP_CONNECT_TIMEOUT = 5000;
        public final static int _HTTP_READ_TIMEOUT = 5000;
    }
    //============================================================================

    //============================================================================
    //Interface url for Doweing
    //Beta server, internet
    public final static String _HOST_V1 = "https://testapi.fusionnextinc.com/v1";
    public final static String _HOST_V2 = "https://testapi.fusionnextinc.com/v2";
    public static final String _HOST_V21 = "https://testapi.fusionnextinc.com/v2.1";

    //Need authorization
    public final static String _INTERFACE_POST_GET_DEVICE_TOKEN = _HOST_V21 + "/auth";
    public final static String _INTERFACE_POST_REFRESH_DEVICE_TOKEN = _HOST_V21 + "/auth/refresh";
    public final static String _INTERFACE_POST_CREATE_CAMERA = _HOST_V21 + "/cameras";
    public final static String _INTERFACE_PUT_UPDATE_CAMERA = _HOST_V21 + "/cameras/{camera_id}";
    //Articles
    public final static String _INTERFACE_POST_CREATE_ARTICLE = _HOST_V21 + "/articles";
    public final static String _INTERFACE_PUT_EDIT_ARTICLE = _HOST_V21 + "/articles/{article_id}";
    public final static String _INTERFACE_GET_GET_ARTICLE = _HOST_V21 + "/articles/{article_id}";
    public final static String _INTERFACE_POST_COMMENT_ARTICLE = _HOST_V21 + "/articles/{article_id}/comments";
    public final static String _INTERFACE_POST_LIKE_ARTICLE = _HOST_V21 + "/articles/{article_id}/likes";
    public final static String _INTERFACE_GET_ARTICLE_COVER = _HOST_V21 + "/articles/{article_id}/cover.jpg?size=medium";
    //Client
    public final static String _INTERFACE_GET_GET_CLIENT = _HOST_V21 + "/clients/{client_id}";
    public final static String _INTERFACE_GET_LIST_PERMISSIONS = _HOST_V21 + "/clients/{client_id}/permissions";
    public final static String _INTERFACE_GET_LIST_ARTICLE = _HOST_V21 + "/clients/{client_id}/articles?sort=created_at&order=desc";
    //Devices
    public final static String _INTERFACE_POST_CREATE_DEVICE = _HOST_V21 + "/devices";
    public final static String _INTERFACE_PUT_UPDATE_DEVICE = _HOST_V21 + "/devices/{device_id}";
    //Files
    public final static String _INTERFACE_POST_UPLOAD_FILE = _HOST_V21 + "/files";

    public static final String _INTERFACE_SEARCH_FW_UPDATE_LIST = _HOST_V21 + "/fw_profiles/{fw_id}/fw_updates?sort=version&order=desc";
    public static final String _INTERFACE_GET_FW_UPDATE_LINK = _HOST_V21 + "/fw_updates/{fw_id}";
    public static final String _INTERFACE_GET_SW_UPDATE_LIST = _HOST_V21 + "/clients/{client_id}/sw_updates?sort=created_at&order=desc&limit=1";
    public static final String _INTERFACE_GET_SW_UPDATE_LINK = _HOST_V21 + "/sw_updates/{sw_id}";

    public static final String REAL_TIME_COMMUNICATION = "https://fusionnextinc.com:3001";
    //============================================================================
    //Broadcast action definition
    public static final String BC_HTTP_UPLOAD_PERCENT = "BC_HTTP_UPLOAD_PERCENT";
    public final static String BC_HTTP_POST_FEEDBACK = "BC_HTTP_POST_FEEDBACK";
    public static final String BC_HTTP_DOWNLOAD_FEEDBACK = "BC_HTTP_DOWNLOAD_FEEDBACK";

    //============================================================================
    public static void broadcastOnlyAction(Context context, String action) {
        broadcastWithExtra(context, action, null);
    }

    public static void broadcastWithExtra(Context context, String action, Bundle extra) {
        Intent intent = new Intent(action);
        if (extra != null) intent.putExtras(extra);
        context.sendBroadcast(intent);
    }

    public static boolean isFileExist(String _file) {
        if (_file == null || _file.equals("")) return false;
        File file = new File(_file);
        return file.exists();
    }
}
