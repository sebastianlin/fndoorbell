package com.fusionnext.cloud.social.objects;

import android.util.Log;

import com.fusionnext.cloud.datalink.json.FNJsonArray;
import com.fusionnext.cloud.datalink.json.FNJsonObject;

import org.json.JSONException;

import java.util.ArrayList;

/**
 * Created by Mike Chang on 2016/2/22.
 */
public class Likes {

    protected final static String TAG = "Likes";

    public final static String LIKES_STRING_TYPE_PHOTO = "photo";
    public final static String LIKES_STRING_TYPE_TRACK = "track";
    public final static String LIKES_STRING_TYPE_COMMENT = "comment";

    public final static int LIKES_TYPE_NONE = 0;
    public final static int LIKES_TYPE_PHOTO = 1;
    public final static int LIKES_TYPE_TRACK = 2;
    public final static int LIKES_TYPE_COMMENT = 3;

    public Likes() {
        _type = LIKES_TYPE_NONE;
        _likeId = "";
        _userId = "";
        _likeableId = 0;
        _likeableType = "";
        _createdAt = "";
        _updatedAt = "";
    }


    public int _type;
    public String _likeId;
    public String _userId;

    public int _likeableId;
    public String _likeableType;
    public String _createdAt;
    public String _updatedAt;

    public void clean() {
        _type = LIKES_TYPE_NONE;
        _likeId = "";
        _userId = "";
        _likeableId = 0;
        _likeableType = "";
        _createdAt = "";
        _updatedAt = "";
    }

    public boolean getDataFromJSONObject(FNJsonObject likeObj) {
        clean();
        if (likeObj == null) {
            Log.w(TAG, " No JSON information");
            return false;
        }
        try {
            _likeId = likeObj.getStringDefault("id");
            _userId = likeObj.getStringDefault("user_id");
            _likeableId = likeObj.getIntDefault("likeable_id");
            _likeableType = likeObj.getStringDefault("likeable_type");
            if (_likeableType.equals(LIKES_STRING_TYPE_PHOTO)) {
                _type = LIKES_TYPE_PHOTO;
            } else if (_likeableType.equals(LIKES_STRING_TYPE_TRACK)) {
                _type = LIKES_TYPE_TRACK;
            } else if (_likeableType.equals(LIKES_STRING_TYPE_COMMENT)) {
                _type = LIKES_TYPE_COMMENT;
            }
            _createdAt = likeObj.getStringDefault("created_at");
            _updatedAt = likeObj.getStringDefault("updated_at");
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
        return true;
    }

    public static ArrayList<Likes> parseLikeArray(String json) {
        try {
            ArrayList<Likes> likeList = new ArrayList<Likes>();
            FNJsonObject likeObject = new FNJsonObject(json);
            FNJsonArray likeArray = likeObject.getJSONArray("items");
            if (likeArray == null) {
                return null;
            }
            for (int i = 0; i < likeArray.length(); i++) {
                FNJsonObject object = likeArray.getJSONObject(i);
                Likes like = new Likes();
                like.getDataFromJSONObject(object);
                if (like == null) return null;
                likeList.add(like);
            }
            return likeList;
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return null;
    }
}
