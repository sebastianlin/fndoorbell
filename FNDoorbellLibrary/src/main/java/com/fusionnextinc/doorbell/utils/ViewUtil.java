package com.fusionnextinc.doorbell.utils;

import android.content.Context;
import android.os.Build;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewConfiguration;

import java.util.concurrent.atomic.AtomicInteger;

/**
 * Created by chocoyang on 2016/5/12.
 */
public class ViewUtil {
    private static final String TAG = "ViewUtil";

    private static final AtomicInteger sNextGeneratedId = new AtomicInteger(1);
    private int mTouchSlop;
    private float pressRawX, pressRawY;
    private boolean isXMoved, isYMoved, isMoveLeft, isMoveRight, isMoveTop, isMoveBottom;

    public static int generateViewId() {
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.JELLY_BEAN_MR1) {
            while (true) {
                final int result = sNextGeneratedId.get();
                // aapt-generated IDs have the high byte nonzero; clamp to the range under that.
                int newValue = result + 1;
                if (newValue > 0x00FFFFFF) newValue = 1; // Roll over to 1, not 0.
                if (sNextGeneratedId.compareAndSet(result, newValue))
                    return result;
            }
        } else {
            return View.generateViewId();
        }
    }

    public ViewUtil(Context context) {
        mTouchSlop = ViewConfiguration.get(context).getScaledTouchSlop();
    }

    public int getTouchSlop() {
        return mTouchSlop;
    }

    public void setTouchEvent(MotionEvent event) {
        switch (event.getAction()) {
            case MotionEvent.ACTION_DOWN:
                pressRawX = event.getRawX();
                pressRawY = event.getRawY();
                isXMoved = false;
                isYMoved = false;
                isMoveLeft = false;
                isMoveRight = false;
                isMoveTop = false;
                isMoveBottom = false;
                break;
            case MotionEvent.ACTION_MOVE:
                float rawX = event.getRawX();
                float rawY = event.getRawY();
                if (!isXMoved)
                    isXMoved = Math.abs(event.getRawX() - pressRawX) > mTouchSlop;
                if (!isYMoved)
                    isYMoved = Math.abs(event.getRawY() - pressRawY) > mTouchSlop;
                if (!isMoveLeft && (isMoveLeft = rawX < pressRawX)) {
                    isMoveRight = false;
                } else if (!isMoveRight && (isMoveRight = rawX > pressRawX)) {
                    isMoveLeft = false;
                }
                if (!isMoveTop && (isMoveTop = rawY < pressRawY)) {
                    isMoveBottom = false;
                } else if (!isMoveBottom && (isMoveBottom = rawY > pressRawY)) {
                    isMoveTop = false;
                }
                break;
            case MotionEvent.ACTION_UP:
                break;
        }
    }

    public boolean isMoved() {
        return isXMoved || isYMoved;
    }

    public boolean isXMoved() {
        return isXMoved;
    }

    public boolean isYMoved() {
        return isYMoved;
    }

    public boolean isMoveLeft() {
        return isMoveLeft;
    }

    public boolean isMoveRight() {
        return isMoveRight;
    }

    public boolean isMoveTop() {
        return isMoveTop;
    }

    public boolean isMoveBottom() {
        return isMoveBottom;
    }
}
