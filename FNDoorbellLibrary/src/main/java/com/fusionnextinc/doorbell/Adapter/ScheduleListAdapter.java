package com.fusionnextinc.doorbell.Adapter;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.TextView;

import com.fusionnextinc.doorbell.MyApplication;
import com.fusionnextinc.doorbell.R;
import com.fusionnextinc.doorbell.manager.data.AppDataBase;
import com.fusionnextinc.doorbell.manager.data.AppDataSchedule;
import com.fusionnextinc.doorbell.manager.data.ListMap;
import com.fusionnextinc.doorbell.manager.thingsManager.ThingsManager;
import com.fusionnextinc.doorbell.objects.FNSchedule;
import com.fusionnextinc.doorbell.widget.FNLayoutUtil;
import com.fusionnextinc.doorbell.widget.FNSwitch;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by Mike Chang on 2018/8/7
 */
public class    ScheduleListAdapter extends BaseAdapter {
    private static final String TAG = "ScheduleListAdapter";

    private FNLayoutUtil mLayoutUtil;
    private Activity mActivity;
    private String mThingId;
    private ListMap mScheduleList = new ListMap();
    private boolean mIsLongPressed = false;
    // 用来控制CheckBox的选中状况
    private static HashMap<Integer, Boolean> mIsSelected = new HashMap<Integer, Boolean>();
    private static HashMap<Integer, Boolean> mIsSwitched = new HashMap<Integer, Boolean>();

    public ScheduleListAdapter(Activity activity, String thingId, ListMap scheduleList) {
        mLayoutUtil = new FNLayoutUtil(activity, MyApplication.LAYOUT_WIDTH, MyApplication.LAYOUT_HEIGHT, FNLayoutUtil.BASE_WIDTH);
        this.mActivity = activity;
        this.mScheduleList.addAll(scheduleList);
        this.mThingId = thingId;
        initSelectedSwitchedData();
    }

    public void updateScheduleList(ListMap scheduleList) {
        mScheduleList.clear();
        mScheduleList.addAll(scheduleList);
        initSelectedSwitchedData();
    }

    public void setLongPressed(boolean isLongPressed) {
        mIsLongPressed = isLongPressed;
        notifyDataSetChanged();
    }

    public ArrayList<Integer> getSelectedScheduleList() {
        Map<String, Object> mapSchedule;
        ArrayList<Integer> selectedScheduleList = new ArrayList<>();

        for (int i = 0; i < mIsSelected.size(); i++) {
            if (mIsSelected.get(i)) {
                mapSchedule = mScheduleList.get(i);
                selectedScheduleList.add((int) mapSchedule.get(AppDataSchedule.TAG_SCHEDULE_ID));
            }
        }
        return selectedScheduleList;
    }

    public boolean isLongPressed() {
        return mIsLongPressed;
    }

    @Override
    public int getCount() {
        return mScheduleList.size();
    }

    @Override
    public Object getItem(int position) {
        return mScheduleList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        final ViewHolder holder;
        final ViewGroup nullParent = null;

        if (convertView == null) {
            holder = new ViewHolder();
            convertView = LayoutInflater.from(mActivity).inflate(R.layout.adapter_schedule_list, nullParent);
            mLayoutUtil.fitAllView(convertView);
            holder.textTime = convertView.findViewById(R.id.textTime);
            holder.textDeviceOnOff = convertView.findViewById(R.id.textDeviceOnOff);
            holder.textWeeks = convertView.findViewById(R.id.textWeeks);
            holder.swDeviceOnOff = convertView.findViewById(R.id.sw_deviceOnOff);
            holder.chkSchedule = convertView.findViewById(R.id.img_schedule_select);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }
        final Map<String, Object> mapSchedule = mScheduleList.get(position);
        if (mapSchedule != null && mapSchedule.size() > 0) {
            boolean isSwitched = mapSchedule.containsKey(AppDataSchedule.TAG_SCHEDULE_SWITCH_ONOFF)
                    ? (boolean) mapSchedule.get(AppDataSchedule.TAG_SCHEDULE_SWITCH_ONOFF) : true;

            holder.textTime.setText(String.format("%02d", (int) mapSchedule.get(AppDataSchedule.TAG_SCHEDULE_HOUR))
                    + ":" + String.format("%02d", (int) mapSchedule.get(AppDataSchedule.TAG_SCHEDULE_MIN)));

            isDeviceOnOff((boolean) mapSchedule.get(AppDataSchedule.TAG_SCHEDULE_DEVICE_ONOFF), holder);
            holder.swDeviceOnOff.setChecked(isSwitched);
            swtichDeviceOnOff(isSwitched, holder);
            mIsSwitched.put(position, isSwitched);

            String strWeek = (String) mapSchedule.get(AppDataSchedule.TAG_SCHEDULE_WEEKS);
            translateWeeks(strWeek, holder);
            holder.swDeviceOnOff.setOnCheckedChangeListener(new FNSwitch.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(FNSwitch fnSwitch, boolean isChecked) {
                    mIsSwitched.put(position, isChecked);
                    swtichDeviceOnOff(isChecked, holder);
                    new Thread(new Runnable() {
                        public void run() {
                            sendScheduleSettings();
                        }
                    }).start();

                }
            });
            scheduleChkBox(holder, position);
        }

        return convertView;
    }

    private class ViewHolder {
        private TextView textTime, textDeviceOnOff, textWeeks;
        private FNSwitch swDeviceOnOff;
        private CheckBox chkSchedule;
    }

    private void isDeviceOnOff(boolean isDeviceOn, ViewHolder holder) {
        if (isDeviceOn) {
            holder.textDeviceOnOff.setText(mActivity.getString(R.string.title_device_on));
        } else {
            holder.textDeviceOnOff.setText(mActivity.getString(R.string.title_device_off));
        }
        holder.textTime.setTextColor(mActivity.getResources().getColor(R.color.schedule_selected_text));
        holder.textDeviceOnOff.setTextColor(mActivity.getResources().getColor(R.color.schedule_selected_text));
        holder.textWeeks.setTextColor(mActivity.getResources().getColor(R.color.schedule_selected_text));
    }

    private void swtichDeviceOnOff(boolean isSwitchOn, ViewHolder holder) {
        if (isSwitchOn) {
            holder.textTime.setTextColor(mActivity.getResources().getColor(R.color.schedule_selected_text));
            holder.textDeviceOnOff.setTextColor(mActivity.getResources().getColor(R.color.schedule_selected_text));
            holder.textWeeks.setTextColor(mActivity.getResources().getColor(R.color.schedule_selected_text));
        } else {
            holder.textTime.setTextColor(mActivity.getResources().getColor(R.color.schedule_text));
            holder.textDeviceOnOff.setTextColor(mActivity.getResources().getColor(R.color.schedule_text));
            holder.textWeeks.setTextColor(mActivity.getResources().getColor(R.color.schedule_text));
        }
    }

    private void translateWeeks(String strWeeks, ViewHolder holder) {
        String[] weeks = null;
        String result = "";

        if (strWeeks.length() > 0) {
            weeks = strWeeks.split(",");
            if (weeks != null) {
                for (int i = 0; i < weeks.length; i++) {
                    String strWeek = "";

                    switch (Integer.valueOf(weeks[i])) {
                        case 7:
                            strWeek = mActivity.getString(R.string.title_sun);
                            break;
                        case 1:
                            strWeek = mActivity.getString(R.string.title_mon);
                            break;
                        case 2:
                            strWeek = mActivity.getString(R.string.title_tue);
                            break;
                        case 3:
                            strWeek = mActivity.getString(R.string.title_wed);
                            break;
                        case 4:
                            strWeek = mActivity.getString(R.string.title_thu);
                            break;
                        case 5:
                            strWeek = mActivity.getString(R.string.title_fri);
                            break;
                        case 6:
                            strWeek = mActivity.getString(R.string.title_sat);
                            break;
                        default:
                            break;
                    }
                    result = result + strWeek + "  ";
                }
            }
        }
        holder.textWeeks.setText(result);
    }

    private void initSelectedSwitchedData() {
        mIsSelected.clear();
        mIsSwitched.clear();
        for (int i = 0; i < mScheduleList.size(); i++) {
            final Map<String, Object> mapSchedule = mScheduleList.get(i);
            boolean isSwitched = mapSchedule.containsKey(AppDataSchedule.TAG_SCHEDULE_SWITCH_ONOFF)
                    ? (boolean) mapSchedule.get(AppDataSchedule.TAG_SCHEDULE_SWITCH_ONOFF) : true;

            mIsSelected.put(i, false);
            mIsSwitched.put(i, isSwitched);
        }
    }

    private void scheduleChkBox(final ViewHolder holder, final int position) {
        if (mIsLongPressed) {
            holder.chkSchedule.setVisibility(View.VISIBLE);
            holder.swDeviceOnOff.setVisibility(View.GONE);
        } else {
            holder.chkSchedule.setVisibility(View.GONE);
            holder.swDeviceOnOff.setVisibility(View.VISIBLE);
        }

        holder.chkSchedule.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    mIsSelected.put(position, true);
                } else {
                    mIsSelected.put(position, false);
                }
            }
        });

        if (mIsSelected.size() > 0) {
            holder.chkSchedule.setChecked(mIsSelected.get(position));
        }
    }

    public void setSelectChkBox(int position) {
        boolean isChecked = mIsSelected.get(position);
        mIsSelected.put(position, !isChecked);
        notifyDataSetChanged();
    }

    public void sendScheduleSettings() {
        ArrayList<FNSchedule> scheduleList = AppDataSchedule.getInstance(mActivity).loadScheduleList(AppDataBase.SCHEDULE_TYPE_NORMAL, mThingId);
        ArrayList<FNSchedule> newScheduleList = new ArrayList<>();

        for (int i = 0; i < mIsSwitched.size(); i++) {
            final Map<String, Object> map = mScheduleList.get(i);
            int scheduleId = (int) map.get(AppDataSchedule.TAG_SCHEDULE_ID);
            for (FNSchedule fnSchedule : scheduleList) {
                if (scheduleId == fnSchedule.getScheduleId()) {
                    fnSchedule.setSwitchOn(mIsSwitched.get(i));
                    newScheduleList.add(fnSchedule);
                    AppDataSchedule.getInstance(mActivity).updateSchedule(AppDataBase.SCHEDULE_TYPE_NORMAL, mThingId, fnSchedule, false);
                    break;
                }
            }
        }
        ThingsManager.getInstance(mActivity).setSchedule(mThingId, newScheduleList);
    }
}
