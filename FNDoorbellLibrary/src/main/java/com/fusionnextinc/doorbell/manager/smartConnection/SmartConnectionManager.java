package com.fusionnextinc.doorbell.manager.smartConnection;

import android.annotation.SuppressLint;
import android.content.Context;
import android.net.wifi.WifiManager;
import android.os.Build;

import androidx.annotation.RequiresApi;

import com.fusionnext.cloud.iot.IOTHandler;
import com.fusionnext.cloud.iot.Public;
import com.fusionnext.cloud.iot.SmartConnection;
import com.fusionnext.cloud.iot.objects.ErrorObj;
import com.fusionnextinc.doorbell.objects.FNThing;
import com.fusionnextinc.doorbell.utils.FNLog;
import com.fusionnextinc.doorbell.utils.FNSocket;

import org.json.JSONException;
import org.json.JSONObject;

import java.net.InetAddress;
import java.net.UnknownHostException;

/**
 * Created by Mike Chang on 2016/6/3
 */
public class SmartConnectionManager {
    protected final static String TAG = "SmartConnectionManager";
    private static int noifyPort = 11111;
    private static int notifyPeriod = 500;
    private static InetAddress inetAddress = null;
    private static SmartConnection smartConnection = null;
    private static SmartConnectionManager instance;
    private Thread mScanThread = null;
    private SmartConnectionHandler.SmartConnectionListener smartConnectionListener = null;

    WifiManager m_wifiManager;
    public final static int STATUS_SMART_CONNECTION_IDLE = 0;
    public final static int STATUS_SMART_CONNECTION_SEARCHING = 1;
    public final static int STATUS_SMART_CONNECTION_DISCOVRED = 2;
    public final static int STATUS_SMART_CONNECTION_REGISTER = 3;
    private static int status = STATUS_SMART_CONNECTION_IDLE;
    private final static String UDP_KEY_PRODUCT = "product";
    private final static String UDP_KEY_IDENTIFIER = "identifier";
    private final static String UDP_KEY_FW_VER = "fw_ver";
    private final static String UDP_KEY_RSSI = "rssi";
    private final static String UDP_KEY_REGISTERED = "registered";
    private final static String UDP_KEY_EVENT = "event";
    private final static String UDP_KEY_THING_IP = "thing_ip";
    private final static String UDP_MSG_OTA_START = "OTA_START";

    private final static String COMMAND_REGISTER_DEVICE = "device_register";
    private final static String COMMAND_CHANGE_SSID = "change_ssid";

    public final static int SMART_CONNECTION_CREATE_THING = 0;
    public final static int SMART_CONNECTION_CHANGE_SSID = 1;
    private static int mSmartconnType = SMART_CONNECTION_CREATE_THING;

    public FNSocket commandSocket = new FNSocket();

    private     String getIpAddress()
    {
        String  str_ipAddress ;
        int ipAddress = m_wifiManager.getConnectionInfo().getIpAddress();
        str_ipAddress = String.format("%d.%d.%d.%d",
                (ipAddress & 0xff),
                (ipAddress >> 8 & 0xff),
                (ipAddress >> 16 & 0xff),
                (ipAddress >> 24 & 0xff));
        try {
            inetAddress = InetAddress.getByName("0.0.0.0");
        } catch (UnknownHostException e) {
            e.printStackTrace();
        }
        return str_ipAddress;
    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    @SuppressLint("DefaultLocale")
    private SmartConnectionManager(Context context) {
        smartConnection = new SmartConnection();
        m_wifiManager = (WifiManager) context.getApplicationContext().getSystemService(Context.WIFI_SERVICE);

    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    public static SmartConnectionManager getInstance(Context context) {
        if (instance == null) {
            synchronized (SmartConnectionManager.class) {
                if (instance == null) {
                    instance = new SmartConnectionManager(context);
                }
            }
        }
        return instance;
    }

    public void setSmartConnectionListener(SmartConnectionHandler.SmartConnectionListener smartConnectionListener) {
        this.smartConnectionListener = smartConnectionListener;
    }


    public void start(final String ssid, final String password, final String custom, final int scanTimeout, final int smartconnType) {


        mSmartconnType = smartconnType;
        if (mScanThread == null) {
            Thread thread = new Thread(new Runnable() {
                @Override
                public synchronized void run() {
                    start(ssid, password, custom);
                    try {
                        Thread.sleep(scanTimeout);
                    } catch (InterruptedException ignore) {
                    }
                    if (smartConnectionListener != null && status == STATUS_SMART_CONNECTION_SEARCHING) {
                        smartConnectionListener.onThingNotFound(instance);
                    }
                    smartConnection.stop();
                    smartConnection.stopDiscovery();
                    stop();
                }
            });
            mScanThread = thread;
            thread.start();
        }
    }

    private int start(String ssid, String password, String ClaimCode) {
        int result = -1;

        if (ssid == null || password == null || ClaimCode == null || smartConnection == null) {
            return result;
        } else {
            String tlvData="";

            tlvData = Public.MakeClaimString(ClaimCode,getIpAddress());


            smartConnection.start(ssid, password, tlvData, onSmartConnectionHandlerListener);
            status = STATUS_SMART_CONNECTION_SEARCHING;
        }
        return result;
    }

    public int stop() {
        int result = -1;
        if (smartConnection != null) {
            status = STATUS_SMART_CONNECTION_IDLE;
            if (mScanThread != null) {
                mScanThread.interrupt();
                mScanThread = null;
            }
        }
        return result;
    }

    public int getSmartConnectionStatus() {
        return status;
    }


    private final IOTHandler.OnSmartConnectionListener onSmartConnectionListener = new IOTHandler.OnSmartConnectionListener() {
        @Override
        public void onThingDiscovered(boolean result, String info) {
            if (smartConnectionListener != null) {
                FNThing fnThing = null;
                boolean isThingOtaStart = false;
                int rssi = 0;
                String thingIp = null;

                if (result && info != null) {
                    try {
                        JSONObject jInfo = new JSONObject(info);
                        fnThing = new FNThing();
                        String product, identifier, fw_ver, event;
                        if ((product = jInfo.optString(UDP_KEY_PRODUCT, null)) != null) {
                            fnThing.setProductSlug(product);
                        }
                        if ((identifier = jInfo.optString(UDP_KEY_IDENTIFIER, null)) != null) {
                            fnThing.setIdentifier(identifier);
                        }
                        if ((fw_ver = jInfo.optString(UDP_KEY_FW_VER, null)) != null) {
                            fnThing.setFirmwareVer(fw_ver);
                        }
                        if ((event = jInfo.optString(UDP_KEY_EVENT, null)) != null && event.equals(UDP_MSG_OTA_START)) {
                            isThingOtaStart = true;
                        }
                        if ((event = jInfo.optString(UDP_KEY_EVENT, null)) != null && event.equals(UDP_MSG_OTA_START)) {
                            isThingOtaStart = true;
                        }
                        thingIp = jInfo.optString(UDP_KEY_THING_IP, null);
                        rssi = jInfo.optInt(UDP_KEY_RSSI, 0);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
                if (smartConnection != null) {
                    smartConnection.stop();
                }
                if (isThingOtaStart) {
                    smartConnectionListener.onThingOtaStart(instance, null);
                    stop();
                } else if (fnThing != null) {
                    assert smartConnection != null;
                    smartConnection.stopDiscovery();
                    stop();
                    smartConnectionListener.onThingFound(instance, fnThing, rssi);
                    status = STATUS_SMART_CONNECTION_DISCOVRED;
                    commandHandler(thingIp, fnThing, rssi);
                }
            }
        }
    };

    private final SmartConnection.OnSmartConnectionHandlerListener onSmartConnectionHandlerListener = new SmartConnection.OnSmartConnectionHandlerListener() {
        @Override
        public void onStart(boolean success) {
            smartConnection.stopDiscovery();
        }

        @Override
        public void onStop(boolean success) {
            smartConnection.startDiscovery(inetAddress, noifyPort, notifyPeriod, onSmartConnectionListener);
        }
    };

    public void updateFirmware(final String ssid, final String password, final String fwUrl, final int scanTimeout) {
        if (mScanThread == null) {
            Thread thread = new Thread(new Runnable() {
                @Override
                public void run() {
                    updateFirmware(ssid, password, fwUrl);
                    try {
                        Thread.sleep(scanTimeout);
                    } catch (InterruptedException ignore) {
                    }
                    if (smartConnectionListener != null && status == STATUS_SMART_CONNECTION_SEARCHING) {
                        smartConnectionListener.onThingNotFound(instance);
                    }
                    stop();
                }
            });
            mScanThread = thread;
            thread.start();
        }
    }

    private int updateFirmware(final String ssid, final String password, final String fwUrl) {
        int result = -1;

        if (ssid == null || password == null || fwUrl == null || smartConnection == null) {
            return result;
        } else {
            smartConnection.start(ssid, password, fwUrl, onSmartConnectionHandlerListener);
            status = STATUS_SMART_CONNECTION_SEARCHING;
        }
        return result;
    }

    private void receiveCommandFromServer(String command, FNThing fnThing, int rssi) {
        FNLog.d(TAG, "command: " + command);

        if (mSmartconnType == SMART_CONNECTION_CREATE_THING) {
            ErrorObj errorObj = ErrorObj.parseErrorObj(command);
            smartConnectionListener.onThingRegistered(instance, fnThing, errorObj, rssi);
            status = STATUS_SMART_CONNECTION_REGISTER;
        } else if (mSmartconnType == SMART_CONNECTION_CHANGE_SSID) {
            if (command.equals("ssid_changed")) {
                smartConnectionListener.onThingSsidChanged(instance, fnThing, rssi);
            }
        }
    }

    private void commandHandler(String thingIp, final FNThing fnThing, final int rssi) {
        if (thingIp != null) {
            if (commandSocket.openSocket(thingIp, 1000, 30000)) {
                byte[] data = mSmartconnType == SMART_CONNECTION_CHANGE_SSID ?
                        COMMAND_CHANGE_SSID.getBytes() : COMMAND_REGISTER_DEVICE.getBytes();
                final String[] tempResponse = {""};
                commandSocket.sendDataToServer(data, 0, data.length);
                commandSocket.receiveDataFromServer(new FNSocket.FNSocketDataListener() {
                    @Override
                    public void onDataReceive(byte[] data, int offset, int byteCount) {
                        tempResponse[0] += new String(data, offset, byteCount);
                        int count = 0;
                        char t = '{';
                        char b = '}';
                        char[] charArray = tempResponse[0].toCharArray();
                        int index = 0;
                        if (charArray[0] == '{') {
                            for (int i = 0; i < charArray.length; i++) {
                                char charData = charArray[i];
                                if (charData == t) {
                                    count++;
                                } else if (charData == b) {
                                    count--;
                                    if (count == 0) {
                                        receiveCommandFromServer(tempResponse[0].substring(0, i - index + 1), fnThing, rssi);
                                        tempResponse[0] = tempResponse[0].substring(i - index + 1, tempResponse[0].length());
                                        index = i + 1;
                                    }
                                }
                            }
                        } else {
                            receiveCommandFromServer(tempResponse[0], fnThing, rssi);
                        }
                    }
                });
            }
        }
    }
}
