package com.iptnet.common;

import android.annotation.SuppressLint;
import android.os.Parcel;
import android.os.Parcelable;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import android.util.SparseArray;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Locale;
import java.util.Set;

/**
 * Class : Peer<br/>
 * Basic from 'RemoteProfile 1.3', and change class name to 'Peer'<br/>
 * Supported 'Parcelable' and added friendly method to save thumbnail.
 * @author HiKARi
 * @since 2015/11/10
 * @version 1.7
 */
public class Peer implements Parcelable, Serializable, Cloneable, Comparable<Peer> {

	private static final long serialVersionUID = -215319727161412630L;
	private String peerId, account, password, description, targetSsid;
	private byte[] thumbnail;
	private SparseArray<Object> values = new SparseArray<>();

	long innerId = Integer.MIN_VALUE;

	public static final Creator<Peer> CREATOR = new Creator<Peer>() {
		
		@Override
		public Peer createFromParcel(Parcel source) { return new Peer(source); }
		
		@Override
		public Peer[] newArray(int size) { return new Peer[size]; }
	};

	@SuppressWarnings("unchecked")
	private Peer(Parcel p) {
		innerId = p.readLong();
		peerId = p.readString();
		account = p.readString();
		password = p.readString();
		description = p.readString();
		targetSsid = p.readString();
		final int thumbnailLen = p.readInt();
		if (thumbnailLen != 0) {
			thumbnail = new byte[thumbnailLen];
			p.readByteArray(thumbnail);
		}
		values = p.readSparseArray(Peer.class.getClassLoader());
	}

	/**
	 * The constructor for new instance the peer by empty parameter.
	 */
	public Peer() {
		peerId = "";
		account = "";
		password = "";
		description = "";
		targetSsid = "";
	}

	/**
	 * The constructor for new instance the peer by specified parameter.
	 * The parameter is be processed by {@link String#trim()}
	 * @param peerId The peer ID, such as 'xxx@yyy.iptnet.net'.
	 * @param account The peer account.
	 * @param password The peer password.
	 * @param description The peer description.
	 * @param targetSsid The peer SSID.
	 */
	public Peer(String peerId, String account, String password, String description, String targetSsid) {
		setPeerId(peerId);
		setAccount(account);
		setPassword(password);
		setDescription(description);
		setTargetSsid(targetSsid);
	}
	
	/**
	 * Getting peer unique identification, the ID format common is 'xxxxx'.
	 * @return The peer unique identification.
	 */
	public long getUniqueId() {
		return innerId;
	}

	/**
	 * Getting peer identification, the ID format common is 'xxx@yyy.iptnet.net'.
	 * @return The peer identification. If peer ID not setting, the peer ID is empty string.
	 * @see #setPeerId(String)
	 */
	public String getPeerId() {
		if (peerId == null)
			peerId = "";
		return peerId;
	}

	/**
	 * Setting peer identification, the ID format common is 'xxx@yyy.iptnet.net'.<br/>
	 * The parameter is be processed by {@link String#trim()}
	 * @param peerId The peer identification. If parameter is null, the peer Id is setting to empty string.
	 * @return The peer instance.
	 * @see #getPeerId
	 */
	public Peer setPeerId(String peerId) {
		if (peerId == null) peerId = "";
		peerId = peerId.trim();
		this.peerId = peerId;
		return this;
	}

	/**
	 * Getting peer account.
	 * @return The peer account. If peer account not setting, the peer account is empty string.
	 * @see #setAccount(String)
	 */
	public String getAccount() { return account; }

	/**
	 * Setting peer account.
 	 * The parameter is be processed by {@link String#trim()}
	 * @param account The peer instance.
	 * @return The peer instance.
	 * @see #getAccount()
	 */
	public Peer setAccount(String account) {
		if (account == null) account = "";
		account = account.trim();
		this.account = account;
		return this;
	}

	/**
	 * Getting peer password.
	 * @return The peer instance.
	 * @see #setPassword(String)
	 */
	public String getPassword() {
		return password;
	}

	/**
	 * Setting peer password.
	 * The parameter is be processed by {@link String#trim()}
	 * @param password The peer password.
	 * @return The peer instance.
	 * @see #getPassword()
	 */
	public Peer setPassword(String password) {
		if (password == null) password = "";
		password = password.trim();
		this.password = password;
		return this;
	}

	/**
	 * Getting peer description.
	 * @return The peer description.
	 * @see #setDescription(String)
	 */
	public String getDescription() { return description; }

	/**
	 * Setting peer description.
	 * The parameter is be processed by {@link String#trim()}
	 * @param description
	 * @return The peer instance.
	 * @see #getDescription()
	 */
	public Peer setDescription(String description) {
		if (description == null) description = "";
		description = description.trim();
		this.description = description;
		return this;
	}

	/**
	 * Getting peer SSID of Wi-Fi.
	 * @return The SSID of Wi-Fi.
	 * @see #setTargetSsid(String)
	 */
	public String getTargetSsid() {	return targetSsid; }

	/**
	 * Setting peer SSID of Wi-Fi.
	 * The parameter is be processed by {@link String#trim()}
	 * @param targetSsid The peer SSID of Wi-Fi.
	 * @return The peer instance.
	 * @see #getTargetSsid()
	 */
	public Peer setTargetSsid(String targetSsid) {
		if (targetSsid == null) targetSsid = "";
		targetSsid = targetSsid.trim();
		this.targetSsid = targetSsid;
		return this;
	}

	/**
	 * Getting byte array of thumbnail data.
	 * @return The byte array of thumbnail data.
	 * @see #setThumbnail(byte[])
	 */
	@Nullable
	public byte[] getThumbnail() { return thumbnail; }

	/**
	 * Setting byte array of thumbnail data.
	 * @param thumbnail The byte array of thumbnail data.
	 * @return The peer instance.
	 * @see #getThumbnail()
	 */
	public Peer setThumbnail(byte[] thumbnail) {
		this.thumbnail = thumbnail;
		return this;
	}

	/**
	 * Getting Integer object of specified key.
	 * @param key The specified key.
	 * @return The Integer object of specified key. The return null when the key is not mapping.
	 * @see #setValue(int, Integer)
	 * @see #getBooleanValue(int)
	 * @see #getStringValue(int)
	 */
	@Nullable
	public Integer getIntValue(int key) {
		if (values.indexOfKey(key) < 0) return null;
		Object value = values.get(key, null);
		if (value instanceof Integer)	return (Integer) value;
		else							return null;
	}

	/**
	 * Getting Boolean object of specified key.
	 * @param key The specified key.
	 * @return The Boolean object of specified key. The return null when the key is not mapping.
	 * @see #setValue(int, Boolean)
	 * @see #getIntValue(int)
	 * @see #getStringValue(int)
	 */
	@Nullable
	public Boolean getBooleanValue(int key) {
		if (values.indexOfKey(key) < 0) return null;
		Object value = values.get(key, null);
		if (value instanceof Boolean)	return (Boolean) value;
		else							return null;
	}

	/**
	 * Getting String object of specified key.
	 * @param key The specified key.
	 * @return The String object of specified key. The return null when the key is not mapping.
	 * @see #setValue(int, String)
	 * @see #getIntValue(int)
	 * @see #getBooleanValue(int)
	 */
	@Nullable
	public String getStringValue(int key) {
		if (values.indexOfKey(key) < 0) return null;
		Object value = values.get(key, null);
		if (value instanceof String)	return (String) value;
		else							return null;
	}

	/**
	 * Setting Integer object by specified key.
	 * @param key The specified key.
	 * @param value The integer value of Object.
	 * @return The peer instance.
	 * @see #getIntValue(int)
	 * @see #setValue(int, Boolean)
	 * @see #setValue(int, String)
	 */
	public Peer setValue(int key, Integer value) {
		values.put(key, value);
		return this;
	}

	/**
	 * Setting Boolean object by specified key.
	 * @param key The specified key.
	 * @param value The boolean value of Object.
	 * @return The peer instance.
	 * @see #getBooleanValue(int)
	 * @see #setValue(int, Integer)
	 * @see #setValue(int, String)
	 */
	public Peer setValue(int key, Boolean value) {
		values.put(key, value);
		return this;
	}

	/**
	 * Setting String object by specified key.
	 * @param key The specified key.
	 * @param value The String value of Object.
	 * @return The peer instance.
	 * @see #getStringValue(int)
	 * @see #setValue(int, Integer)
	 * @see #setValue(int, Boolean)
	 */
	public Peer setValue(int key, String value) {
		values.put(key, value);
		return this;
	}
	
	@Override
	public String toString() {
		String msg = "\n>> Class 'Peer' toString()";
		msg += "\n>> innerId = " + innerId;
		msg += "\n>> peerId = " + peerId + ", account = " + account + ", password = " + password;
		msg += "\n>> description = " + description + ", targetSsid = " + targetSsid;
		msg += ", thumbnail len = " + (thumbnail == null ? 0:thumbnail.length);
		for (int i=0; i<values.size(); i++) {
			int key = values.keyAt(i);
			Object value = values.get(key);
			msg += "\n>> key = " + key + ", value = " + String.valueOf(value);
		}
		return msg;
	}

	/**
	 * Getting the Hash code by the ASCII code sum of lower case of peer ID.
	 * @return The Hash code of the peer.
	 * @see #equals(Object)
	 */
	@Override
	public int hashCode() {
		if (peerId == null || peerId.isEmpty()) return Integer.MIN_VALUE;
		String peerId = this.peerId.toLowerCase(Locale.getDefault());
		int code = 0;
		for (byte b : peerId.getBytes()) {
			code += b;
		}
		return code;
	}

	/**
	 * Compare the ID of peer is equal.
	 * @param o The compared peer.
	 * @return True that the peer ID is same the compared, otherwise false.
	 */
	@Override
	public boolean equals(Object o) {
		if (o == null) return false;
		return o instanceof Peer && ((Peer) o).getPeerId().equalsIgnoreCase(this.peerId);
	}

	/**
	 * Copy the peer.
	 * @return The new instance peer by copied.
	 */
	@Override
	public Peer clone() {
		Peer peer = new Peer();
		peer.innerId = innerId;
		peer.peerId = peerId;
		peer.account = account;
		peer.password = password;
		peer.description = description;
		peer.targetSsid = targetSsid;
		if (thumbnail != null)
			peer.thumbnail = Arrays.copyOf(thumbnail, thumbnail.length);
		for (int i=0; i<values.size(); i++) {
			int key = values.keyAt(i);
			Object value = values.get(key);
			peer.values.put(key, value);
		}
		return peer;
	}

	@Override
	public int compareTo(@NonNull Peer another) {
		if (innerId > another.innerId)
			return 1;
		else if (innerId < another.innerId)
			return -1;
		else
			return 0;
	}

	@Override
	public int describeContents() {
		return 0;
	}

	@Override
	public void writeToParcel(Parcel dest, int flags) {
		dest.writeLong(innerId);
		dest.writeString(peerId);
		dest.writeString(account);
		dest.writeString(password);
		dest.writeString(description);
		dest.writeString(targetSsid);
		if (thumbnail == null)	dest.writeInt(0);
		else {
			dest.writeInt(thumbnail.length);
			dest.writeByteArray(thumbnail);
		}
		dest.writeSparseArray(values);
	}
	
	@SuppressWarnings("unchecked")
	private void readObject(ObjectInputStream stream) throws IOException, ClassNotFoundException {
		innerId = stream.readLong();
		peerId = stream.readUTF();
		account = stream.readUTF();
		password = stream.readUTF();
		description = stream.readUTF();
		targetSsid = stream.readUTF();
		ByteArrayOutputStream byteStream = new ByteArrayOutputStream();
		int b;
		while ((b = stream.read()) != -1)
			byteStream.write(b);
		thumbnail = byteStream.toByteArray();
		if (thumbnail.length == 1)
			thumbnail = null;
		values = new SparseArray<>();
		HashMap<Integer, Object> map = (HashMap<Integer, Object>) stream.readObject();
		Set<Integer> keySet = map.keySet();
		for (Integer key : keySet) {
			Object value = map.get(key);
			values.put(key, value);
		}
	}
	
	@SuppressLint("UseSparseArrays")
	private void writeObject(ObjectOutputStream stream) throws IOException {
		stream.writeLong(innerId);
		stream.writeUTF(peerId);
		stream.writeUTF(account);
		stream.writeUTF(password);
		stream.writeUTF(description);
		stream.writeUTF(targetSsid);
		if (thumbnail == null) {
			byte[] b = new byte[1];
			stream.write(b, 0, b.length);
		} else
			stream.write(thumbnail, 0, thumbnail.length);
		HashMap<Integer, Object> map = new HashMap<>(values.size());
		for (int i=0; i<values.size(); i++) {
			int key = values.keyAt(i);
			Object value;
			value = values.get(key);
			map.put(key, value);
		}
		stream.writeObject(map);
	}
}
