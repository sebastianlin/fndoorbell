package com.fusionnextinc.doorbell.manager.data;

import android.util.Log;

/**
 * Created by Mike Chang on 2014/6/26.
 */
public class QueryStatus {
    private final static String TAG = "QueryStatus";
    private String _lastId = "";
    private boolean _isQueryEnd = false;
    private int _msgPageSize = 30;
    private int _pageSize = 30;
    private boolean _isNewQuery = true;

    public void setPageSize(int size) {
        _pageSize = size;
    }

    public int getPageSize() {
        return _pageSize;
    }

    public int getMsgPageSize() {
        return _msgPageSize;
    }

    public void setQueryState(boolean newQuery) {
        _isNewQuery = newQuery;
        _isQueryEnd = false;
        if (newQuery) {
            _lastId = "";
        }
    }

    public boolean isNewQuery() {
        return _isNewQuery;
    }

    public String getLastItemId() {
        return _lastId;
    }

    public void setLastItemId(String idValue) {
        _lastId = idValue;
    }

    public boolean isQueryEnd() {
        return _isQueryEnd;
    }

    public void setQueryEnd(boolean isEnd) {
        _isQueryEnd = isEnd;
        if (_isQueryEnd)
            _lastId = "";
    }

    protected long cuclatePage() {
        long page = 1;
        if (!_isNewQuery && !getLastItemId().isEmpty()) {
            Log.d(TAG, " query next page from" + getLastItemId());
            long lastIdx = Long.parseLong(getLastItemId());

            if (lastIdx > 0) {
                page = lastIdx / getPageSize() + 1;
            }
        }
        return page;
    }
}
