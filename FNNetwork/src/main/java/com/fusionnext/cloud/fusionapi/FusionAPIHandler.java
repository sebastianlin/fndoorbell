package com.fusionnext.cloud.fusionapi;


import com.fusionnext.cloud.fusionapi.objects.Article;
import com.fusionnext.cloud.fusionapi.objects.Firmware;
import com.fusionnext.cloud.fusionapi.objects.Software;

/**
 * Created by Mike Chang on 2017/3/7
 */
public abstract class FusionAPIHandler {
    protected interface OnActivateAPIListener {
        void onResult(boolean result, int postType, String retStr);

        void onGetDeviceToken(boolean result, Authorization.DeviceToken deviceToken);

        void onRefreshDeviceToken(boolean result, Authorization.DeviceToken deviceToken);

        void onCreateDevice(boolean result, Devices.Device device);

        void onListPermission(boolean result, String[] permissions);
    }

    public interface OnFusionAPIListener {
        void onResult(boolean result, int postType, String retStr);

        void onActivateAPI(boolean result);

        void onRefreshDeviceToken(boolean result, Authorization.DeviceToken deviceToken);

        void onGetClient(boolean result, Clients.Client client);

        void onGetArticle(boolean result, Article article);

    }

    public interface OnOtaHandlerListener {
        void onNewFwInfo(boolean result, Firmware firmware);

        void onNewSwInfo(boolean result, Software software);

        void onSrartDownload();

        void onStopDownload();

        void onDownloadProgress(long progress);

        void onDownloadComplete();

        void onError();
    }
}
