package com.fusionnext.cloud.fusionapi;

import android.content.Context;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;

import com.fusionnext.cloud.datalink.http.HttpGet;
import com.fusionnext.cloud.datalink.http.HttpParam;
import com.fusionnext.cloud.datalink.http.HttpPost;
import com.fusionnext.cloud.datalink.json.FNJsonArray;
import com.fusionnext.cloud.datalink.json.FNJsonObject;
import com.fusionnext.cloud.utils.FNNetworkLog;
import com.fusionnext.cloud.utils.Utils;

import org.json.JSONException;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;

/**
 * Created by Mike Chang on 2016/2/22.
 */
public class Authorization {
    protected final static String _TAG = "Authorization";

    //SDK Permission
    public final static int SDK_PREMISSION_CAMERA = 1 << 0;
    public final static int SDK_PREMISSION_MAPKIT = 1 << 1;
    public final static int SDK_PREMISSION_PLAYER = 1 << 2;
    public final static int SDK_PREMISSION_NONE = 0;
    public final static int SDK_PREMISSION_ALL = SDK_PREMISSION_CAMERA & SDK_PREMISSION_MAPKIT & SDK_PREMISSION_PLAYER;
    //Camera Permission
    public final static int CAMERA_PREMISSION_AMBARELLA = 1 << 0;
    public final static int CAMERA_PREMISSION_NOVATEK = 1 << 1;
    public final static int CAMERA_PREMISSION_OTUS = 1 << 2;
    public final static int CAMERA_PREMISSION_AIT = 1 << 3;
    public final static int CAMERA_PREMISSION_ICATCH = 1 << 4;
    public final static int CAMERA_PREMISSION_NONE = 0;
    public final static int CAMERA_PREMISSION_ALL = CAMERA_PREMISSION_AMBARELLA & CAMERA_PREMISSION_NOVATEK & CAMERA_PREMISSION_OTUS & CAMERA_PREMISSION_AIT & CAMERA_PREMISSION_ICATCH;
    //Player Permission
    public final static int PLAYER_PREMISSION_GENERAL = 1 << 0;
    public final static int PLAYER_PREMISSION_VR = 1 << 1;
    public final static int PLAYER_PREMISSION_NONE = 0;
    public final static int PLAYER_PREMISSION_ALL = PLAYER_PREMISSION_GENERAL & PLAYER_PREMISSION_VR;

    public static class Permission {
        public int sdkPermisiion = 0xFF;
        public int cameraPermisiion = 0xFF;
        public int playerPermisiion = 0xFF;
    }

    public static class DeviceToken {
        public String accessToken = "";
        public String expireIn = "";
        public String refreshToken = "";
        public String clientId = "";
    }

    private Context context;
    private DeviceToken deviceToken = new DeviceToken();
    private Permission permission = new Permission();
    private FusionAPIHandler.OnFusionAPIListener onFusionAPIListener;

    public Authorization(Context context) {
        this.context = context;
        setDeviceToken(Public.getDataMgr(context).loadDeviceToken());
        setPermission(Public.getDataMgr(context).loadPermission());
    }

    public String getCommToken() {
        String token = "";
        if (deviceToken != null) {
            token = deviceToken.accessToken;
        }

        token = token.replace("Bearer", "").trim();
        return token;
    }

    public synchronized String getDeviceAccessToken() {
        setDeviceToken(Public.getDataMgr(context).loadDeviceToken());
        if (deviceToken != null) {
            if (!deviceToken.accessToken.equals("") && !deviceToken.refreshToken.equals("")) {
                refreshDeviceToken();
            }
            return deviceToken.accessToken;
        }
        return "";
    }


    public String getClientId() {
        setDeviceToken(Public.getDataMgr(context).loadDeviceToken());
        if (deviceToken != null) {
            return deviceToken.clientId;
        }
        return "";
    }

    protected Permission getPermission() {
        setPermission(Public.getDataMgr(context).loadPermission());
        return permission;
    }

    private void clearPermission() {
        setPermission(Public.getDataMgr(context).loadPermission());
        permission.cameraPermisiion = 0;
        permission.playerPermisiion = 0;
        permission.sdkPermisiion = 0;
    }

    private void setPermission(Permission permission) {
        if (permission == null) permission = new Permission();
        this.permission.sdkPermisiion = permission.sdkPermisiion;
        this.permission.cameraPermisiion = permission.cameraPermisiion;
        this.permission.playerPermisiion = permission.playerPermisiion;
    }

    private void setDeviceToken(DeviceToken token) {
        if (deviceToken == null) deviceToken = new DeviceToken();
        if (token.accessToken != null) deviceToken.accessToken = token.accessToken;
        if (token.clientId != null) deviceToken.clientId = token.clientId;
        if (token.expireIn != null) deviceToken.expireIn = token.expireIn;
        if (token.refreshToken != null) deviceToken.refreshToken = token.refreshToken;
    }

    private DeviceToken parseDeviceToken(String jsonStr) {
        if (jsonStr == null || jsonStr.equals("")) return null;

        try {
            FNJsonObject object = new FNJsonObject(jsonStr);
            SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");

            String token = object.getStringDefault("access_token");
            deviceToken.accessToken = "Bearer " + token;
            deviceToken.expireIn = object.getStringDefault("expires_in");
            Date curDate = new Date(System.currentTimeMillis() + Long.parseLong(deviceToken.expireIn) * 1000); // 獲取當前時間
            deviceToken.expireIn = formatter.format(curDate);
            deviceToken.refreshToken = object.getStringDefault("refresh_token");
            deviceToken.clientId = object.getStringDefault("client_id");
            Public.getDataMgr(context).saveDeviceToken(deviceToken);
            setDeviceToken(deviceToken);
            FNNetworkLog.i(_TAG, "Device token will be expired in " + deviceToken.expireIn);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return deviceToken;
    }

    public synchronized boolean refreshDeviceToken() {
        String refreshToken = deviceToken.refreshToken;
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
        long expireIn = 0;
        long curTime = System.currentTimeMillis();

        try {
            Date date = dateFormat.parse(deviceToken.expireIn);
            expireIn = date.getTime();
        } catch (ParseException e) {
            e.printStackTrace();
        }

        if (refreshToken.equals("")) {
            return false;
        }

        //don't need to refresh token from server, because the token is not expired yet.
        if (curTime < expireIn) {
            FNNetworkLog.i(_TAG, "Device token is not expired yet, expire time = " + deviceToken.expireIn);
            return true;
        }

        HttpPost.PostParam[] params = new HttpPost.PostParam[1];

        params[0] = new HttpPost.PostParam();
        params[0].url = HttpData.generateHttpPostUrl(HttpData._POST_TYPE_REFRESH_DEVICE_TOKEN);
        params[0].type = HttpData._POST_TYPE_REFRESH_DEVICE_TOKEN;
        params[0].param = new HashMap<Object, Object>();

        params[0].param.put("grant_type", "refresh_token");
        params[0].param.put("refresh_token", refreshToken);

        HttpPost post = new HttpPost();
        post.setContext(context);
        post.setOnParseJsonListener(new HttpPost.OnParseJsonListener() {
            @Override
            public void onParse(boolean returnResult, int postType, String id, String returnStr) {
                DeviceToken deviceToken = parseDeviceToken(returnStr);
            }
        });
        post.execute(params);
        return false;
    }

    public boolean regeistDevice(FusionAPIHandler.OnActivateAPIListener onActivateAPIListener) {
        boolean isRistered = false;
        if (deviceToken != null) {
            if (deviceToken.accessToken.equals("") || deviceToken.refreshToken.equals("")) {
                getDeviceToken(onActivateAPIListener);
            } else {
                isRistered = refreshDeviceToken();
            }
        }
        return isRistered;
    }

    private void getDeviceToken(final FusionAPIHandler.OnActivateAPIListener onActivateAPIListener) {
        String packageName = Utils.getPackageName(context);//"com.example.example";
        String identifier = null;

        try {
            ApplicationInfo info = context.getPackageManager().getApplicationInfo(Utils.getPackageName(context), PackageManager.GET_META_DATA);
            identifier = info.metaData.getString("com.fusionnext.sdk.fusion.API_KEY");
        } catch (Exception ignore) {
            //identifier = "103jUf92WgW9C11OlXSaOfZY8lYbctqe";
        }

        HttpPost.PostParam[] params = new HttpPost.PostParam[1];
        params[0] = new HttpPost.PostParam();
        params[0].url = HttpData.generateHttpPostUrl(HttpData._POST_TYPE_GET_DEVICE_TOKEN);
        params[0].type = HttpData._POST_TYPE_GET_DEVICE_TOKEN;
        params[0].param = new HashMap<Object, Object>();
        params[0].param.put("grant_type", "client_credentials");
        params[0].param.put("client_id", identifier);
        params[0].param.put("package_name", packageName);

        HttpPost post = new HttpPost();
        post.setContext(context);
        post.setOnParseJsonListener(new HttpPost.OnParseJsonListener() {
            @Override
            public void onParse(boolean returnResult, int postType, String id, String returnStr) {
                if (returnStr != null) {
                    DeviceToken deviceToken = parseDeviceToken(returnStr);
                }
                onActivateAPIListener.onGetDeviceToken(returnResult, deviceToken);
            }
        });
        post.execute(params);
    }

    private void listPermission(String clientId, final FusionAPIHandler.OnActivateAPIListener onActivateAPIListener) {
        HttpParam.HttpSubParam param = new HttpParam.HttpSubParam();

        param.accessToken = Public.getAuthorization(context).getDeviceAccessToken();
        param.url = HttpData.generateHttpGetUrl(HttpData._GET_TYPE_LIST_PERMISSION, clientId);
        param.cacheName = "";
        HttpParam[] params = new HttpParam[1];
        params[0] = new HttpParam();
        params[0].params.clear();
        params[0].params.add(param);

        HttpGet httpGet = new HttpGet();
        httpGet.setContext(context);
        httpGet.setTag(HttpGet._HTTP_T_GET_NORMAL_TYPE);
        httpGet.setOnParseJsonListener(new HttpGet.OnParseJsonListener() {
            @Override
            public void onParse(boolean result, String returnStr) {
                String[] permissions = new String[0];
                if (returnStr != null) {
                    permissions = parsePermissions(returnStr);
                }
                onActivateAPIListener.onListPermission(result, permissions);
            }
        });
        httpGet.execute(params);
    }

    private String[] parsePermissions(String jsonStr) {
        if (jsonStr == null || jsonStr.equals("")) return null;
        String[] permissions = null;
        try {
            FNJsonObject object = new FNJsonObject(jsonStr);
            FNJsonArray array = object.getJSONArray("items");
            if (array != null) {
                clearPermission();
                permissions = new String[array.length()];
                for (int j = 0; j < array.length(); j++) {
                    object = array.getJSONObject(j);
                    permissions[j] = object.getStringDefault("name");
                    if (permissions[j].equals("camera.access")) {
                        permission.sdkPermisiion = permission.sdkPermisiion | SDK_PREMISSION_CAMERA;
                    }
                }
            }
            Public.getDataMgr(context).savePermission(permission);
            setPermission(permission);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return permissions;
    }

    public boolean securityCheck() {
        String packageName = Utils.getPackageName(context);//"com.example.android";
        String key = Utils.md5(packageName + "_bmcjcd_android");
        String identifier = null;
        boolean result = false;
        try {
            ApplicationInfo info = context.getPackageManager().getApplicationInfo(Utils.getPackageName(context), PackageManager.GET_META_DATA);
            identifier = info.metaData.getString("com.fusionnext.sdk.fusion.API_KEY");//"fcd7o9w2ibcbbft7feuefeqab6qciez7";
            if (key != null && identifier != null && key.length() > 0 && identifier.length() > 0)
                for (int i = 0; i < key.length(); i++) {
                    if (((i + 1) % 2) == 0) {
                        if (key.charAt(i) == identifier.charAt(i)) {
                            result = true;
                        } else {
                            break;
                        }
                    }
                }
        } catch (Exception ignore) {

        }
        return result;
    }

    public void activiateAPI(Context context, final FusionAPIHandler.OnFusionAPIListener onFusionAPIListener) {
        this.onFusionAPIListener = onFusionAPIListener;
        if (securityCheck()) {
            getDeviceToken(onActivateAPIListener);
        } else {
            this.onFusionAPIListener.onActivateAPI(false);
        }
    }

    final FusionAPIHandler.OnActivateAPIListener onActivateAPIListener = new FusionAPIHandler.OnActivateAPIListener() {

        @Override
        public void onResult(boolean result, int postType, String retStr) {

        }

        @Override
        public void onGetDeviceToken(boolean result, Authorization.DeviceToken deviceToken) {
            listPermission(getClientId(), onActivateAPIListener);
        }

        @Override
        public void onRefreshDeviceToken(boolean result, Authorization.DeviceToken deviceToken) {

        }

        @Override
        public void onCreateDevice(boolean result, Devices.Device device) {
            if (onFusionAPIListener != null) {
                onFusionAPIListener.onActivateAPI(result);
            }
        }

        @Override
        public void onListPermission(boolean result, String[] permissions) {
            final Devices devices = new Devices(context);
            devices.createDevice(onActivateAPIListener);
        }
    };
}
