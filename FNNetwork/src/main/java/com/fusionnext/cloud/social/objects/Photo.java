package com.fusionnext.cloud.social.objects;

import android.util.Log;

import com.fusionnext.cloud.datalink.json.FNJsonArray;
import com.fusionnext.cloud.datalink.json.FNJsonObject;

import org.json.JSONException;

import java.util.ArrayList;

/**
 * Created by Mike Chang on 2016/2/22.
 */
public class Photo {
    protected final static String _TAG = "Photo";
    protected final static String _JSON_TAG = "photo";

    public Photo() {
        _type = PHOTO_TYPE_NONE;
        _photoId = "";
        _userId = "";
        _fileName = "";
        _description = "";
        _place = "";
        _lat = "";
        _lng = "";
        _capturedAt = "";
        _photoableId = "";
        _photoableType = "";
        _createdAt = "";
        _updatedAt = "";
        _commentsMeta = new CommentsMeta();
        _likesMeta = new LikesMeta();
        _viewableLinks = new ViewableLink();
        _userProfile = new UserProfile();
    }

    public final static String PHOTO_STRING_TYPE_GROUP = "group";
    public final static String COPHOTO_STRING_TYPE_TRACK = "track";

    public final static int PHOTO_TYPE_NONE = 0;
    public final static int PHOTO_TYPE_GROUP = 1;
    public final static int PHOTO_TYPE_TRACK = 2;

    public int _type;
    public String _photoId;
    public String _userId;
    public String _fileName;
    public String _description;
    public ViewableLink _viewableLinks;
    public String _place;
    public String _lat;
    public String _lng;
    public String _capturedAt;  // photo's captured_at
    public String _photoableId;  // photo's photoable_id
    public String _photoableType;  // photo's photoable_type
    public String _createdAt;  // photo's created_at
    public String _updatedAt; // photo's updated_ato
    public LikesMeta _likesMeta;
    public CommentsMeta _commentsMeta;
    public UserProfile _userProfile;

    public void clean() {
        _type = PHOTO_TYPE_NONE;
        _photoId = "";
        _userId = "";
        _fileName = "";
        _description = "";
        _place = "";
        _lat = "";
        _lng = "";
        _capturedAt = "";  // user's captured_at
        _photoableId = "";  // user's photoable_id
        _photoableType = "";  // user's photoable_type\
        _createdAt = "";
        _updatedAt = "";
        _viewableLinks.clean();
        _likesMeta.clean();
        _commentsMeta.clean();
        _userProfile.clean();
    }

    public boolean getDataFromJSONObject(FNJsonObject photoObj) {
        clean();
        if (photoObj == null) {
            Log.w(_TAG, " No JSON information");
            return false;
        }

        try {
            _photoId = photoObj.getStringDefault("id");
            _userId = photoObj.getStringDefault("user_id");
            _fileName = photoObj.getStringDefault("filename");
            _description = photoObj.getStringDefault("description");
            if (_fileName != null) _fileName = _fileName.trim();

            _viewableLinks.getDataFromJSONObject(photoObj);
            _place = photoObj.getStringDefault("place");

            _lat = photoObj.getStringDefault("lat");
            _lng = photoObj.getStringDefault("lng");
            _capturedAt = photoObj.getStringDefault("captured_at");
            _photoableId = photoObj.getStringDefault("photoable_id");
            _photoableType = photoObj.getStringDefault("photoable_type");
            if (_photoableType.equals(PHOTO_STRING_TYPE_GROUP)) {
                _type = PHOTO_TYPE_GROUP;
            } else if (_photoableType.equals(COPHOTO_STRING_TYPE_TRACK)) {
                _type = PHOTO_TYPE_TRACK;
            }
            _createdAt = photoObj.getStringDefault("created_at");
            _updatedAt = photoObj.getStringDefault("updated_at");

            FNJsonObject commentsMetaObj = photoObj.getJSONObject("commentsmeta");
            if (commentsMetaObj == null) {
                Log.i(_TAG, "No commentsmeta tag");
                return false;
            }
            _commentsMeta.getDataFromJSONObject(commentsMetaObj);

            FNJsonObject likesMetaObj = photoObj.getJSONObject("likesmeta");
            if (likesMetaObj == null) {
                Log.i(_TAG, "No likesmeta tag");
                return false;
            }
            _likesMeta.getDataFromJSONObject(likesMetaObj);

            FNJsonObject userObj = photoObj.getJSONObject("user");
            if (userObj == null) {
                Log.i(_TAG, "No user tag");
                return false;
            }
            //Get the user information
            _userProfile.getDataFromJSONObject(userObj);
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
        return true;
    }

    public static Photo parsePhoto(String json) {
        try {
            FNJsonObject photoObject = new FNJsonObject(json);

            Photo photo = new Photo();
            photo.getDataFromJSONObject(photoObject);
            return photo;
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static ArrayList<Photo> parsePhotoArray(String json) {
        try {
            ArrayList<Photo> photoList = new ArrayList<Photo>();
            FNJsonObject photoObject = new FNJsonObject(json);
            FNJsonArray photosArray = photoObject.getJSONArray("items");
            if (photosArray == null) {
                return null;
            }
            for (int i = 0; i < photosArray.length(); i++) {
                FNJsonObject object = photosArray.getJSONObject(i);
                Photo photo = new Photo();
                photo.getDataFromJSONObject(object);
                if (photo == null) return null;
                photoList.add(photo);
            }
            return photoList;
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return null;
    }
}
