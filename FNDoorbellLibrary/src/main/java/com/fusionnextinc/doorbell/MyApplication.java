package com.fusionnextinc.doorbell;

import android.app.Application;
import android.content.Context;
import android.os.Handler;

import androidx.multidex.MultiDexApplication;

import com.fusionnextinc.doorbell.info.SharedPreferencesInfo;
import com.fusionnextinc.doorbell.utils.FNLog;
import com.fusionnextinc.doorbell.widget.FNToast;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

/**
 * Created by Mike Chang on 2017/6/21
 */
public class MyApplication extends MultiDexApplication {
    private static final String TAG = "MyApplication";

    private static Application application;
    private static Context context;
    private static Thread mainThread;
    private static Handler mainThreadHandler;

    public static final int LAYOUT_WIDTH = 1080;
    public static final int LAYOUT_HEIGHT = 1920;

    private static boolean activityVisible;

    public static final String BROADCAST_ANSWER_EVENT = "BROADCAST.EVENT.ANSWER";
    public static final String BROADCAST_HANGUP_EVENT = "BROADCAST.EVENT.HANGUP";
    public static final String FCM_DATA_KEY = "FCM.DATA.KEY";
    public static final String CALL_RESPONSE_ACTION_KEY = "CALL.RESPONSE.ACTION.KEY";
    public static final String CALL_CANCEL_ACTION = "CALL.CANCEL.ACTION";
    public static final String CALL_RECEIVE_ACTION = "CALL.RECEIVE.ACTION";
    public static final String CALL_END_ACTION = "CALL.END.ACTION";

    //Public definitions class
    public static class Defines {
        //Application used
        public final static String _APP_SD_CACHE_DIR = "fndoorbell_cache";
    }

    @Override
    public void onCreate() {
        super.onCreate();
        // 初始化AppConfig
        try {
            Class<?> appConfigClass = Class.forName("com.fusionnextinc.fndoorbell.AppConfig");
            Method initConfig = appConfigClass.getMethod("initConfig");
            initConfig.invoke(null);
        } catch (NoSuchMethodException | ClassNotFoundException | IllegalAccessException | InvocationTargetException ignore) {
        }
        FNLog.SHOW_LOG = DefaultAppConfig.SHOW_LOG;
        if (DefaultAppConfig.TRACK_LOG) {
            FNLog.startWriteLog(this);
        }
        application = this;
        context = getApplicationContext();
        mainThread = Thread.currentThread();
        mainThreadHandler = new Handler();

        SharedPreferencesInfo.initDefaultSharedPreferences(this);
    }

    public static Context getAppContext() {
        return context;
    }

    public static Application getApplication() {
        return application;
    }

    public static void postOnUiThread(Runnable action) {
        mainThreadHandler.post(action);
    }

    public static void runOnUiThread(Runnable action) {
        runOnUiThread(action, 0);
    }

    public static void runOnUiThread(Runnable action, long delayMillis) {
        if (delayMillis != 0 || Thread.currentThread() != mainThread) {
            mainThreadHandler.postDelayed(action, delayMillis);
        } else {
            action.run();
        }
    }

    public static void showToastOnUiThread(final String msg, final int duration) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                FNToast.makeText(context, msg, duration).show();
            }
        });
    }

    public static void showToastOnUiThread(final int resId, final int duration) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                FNToast.makeText(context, context.getString(resId), duration).show();
            }
        });
    }

    public static void showToastOnUiThread(final int resId, final Integer errorCode, final int duration) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                FNToast.makeText(context, context.getString(resId) + (errorCode != null ? " (" + errorCode + ")" : ""), duration).show();
            }
        });
    }



    public static boolean isActivityVisible() {
        return activityVisible;
    }

    public static void activityResumed() {
        activityVisible = true;
    }

    public static void activityPaused() {
        activityVisible = false;
    }
}
