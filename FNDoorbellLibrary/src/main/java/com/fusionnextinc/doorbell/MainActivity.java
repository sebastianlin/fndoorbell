package com.fusionnextinc.doorbell;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.NotificationManager;
import android.app.admin.DevicePolicyManager;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.FragmentActivity;

import android.os.Handler;
import android.os.PowerManager;
import android.provider.Settings;
import android.text.TextUtils;
import android.util.Log;
import android.view.WindowManager;
import android.widget.ImageView;

import com.fusionnextinc.doorbell.fragement.CallFragment;
import com.fusionnextinc.doorbell.fragement.FNFragmentManager;
import com.fusionnextinc.doorbell.fragement.LaunchFragment;
import com.fusionnextinc.doorbell.info.SharedPreferencesInfo;
import com.fusionnextinc.doorbell.manager.mqtt.MqttManager;
import com.fusionnextinc.doorbell.manager.thingsManager.ThingsManager;
import com.fusionnextinc.doorbell.utils.FNLog;
import com.fusionnextinc.doorbell.widget.FNActionBar;
import com.fusionnextinc.doorbell.widget.FNLayoutUtil;



import com.iptnet.common.c2c.C2CProcess;

import java.lang.reflect.Method;
import java.util.logging.Logger;

import android.app.admin.DeviceAdminReceiver;

public class MainActivity extends AppCompatActivity  implements InAppUpdateManager.InAppUpdateHandler{
    private static final String TAG = "MainActivity";
    private static final String ENABLED_NOTIFICATION_LISTENERS = "enabled_notification_listeners";
    private static final String ACTION_NOTIFICATION_LISTENER_SETTINGS = "android.settings.ACTION_NOTIFICATION_LISTENER_SETTINGS";


    private ImageView interceptedNotificationImageView;
    private ImageChangeBroadcastReceiver imageChangeBroadcastReceiver;
    private AlertDialog enableNotificationListenerAlertDialog;
    private DevicePolicyManager mgr=null;
    private ComponentName cn=null;

    private static final int REQ_CODE_VERSION_UPDATE = 530;
    private InAppUpdateManager inAppUpdateManager;
    private Intent activityIntent = null;


    public static void waitNotifyEnd()
    {
        final Handler handler = new Handler();


        handler.post(new Runnable() {
            @Override
            public void run() {


                while (HeadsUpNotificationService.getFlage() == true)
                {
                    handler.postDelayed(this,500);
                }

            }

        });
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        FNLog.d(TAG, "onCreate");
        super.onCreate(savedInstanceState);

        if (savedInstanceState == null)
            activityIntent = getIntent();

        getWindow().addFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN |
                WindowManager.LayoutParams.FLAG_SHOW_WHEN_LOCKED //鎖屏顯示
                | WindowManager.LayoutParams.FLAG_DISMISS_KEYGUARD //解鎖
                | WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON //保持屏幕不息屏
                | WindowManager.LayoutParams.FLAG_TURN_SCREEN_ON);
        setContentView(R.layout.activity_main);
        FNLayoutUtil layoutUtil = new FNLayoutUtil(this, MyApplication.LAYOUT_WIDTH, MyApplication.LAYOUT_HEIGHT, FNLayoutUtil.BASE_WIDTH);
        layoutUtil.fitAllView(findViewById(R.id.activity_main));
        FNActionBar actionBar = (FNActionBar) findViewById(R.id.actionbar);
        actionBar.setMainLayout(findViewById(R.id.fl_main));
        FNFragmentManager.getInstance().init(this, actionBar);

        checkForUpdates();
/*

        // If the user did not turn the notification listener service on we prompt him to do so
        if(!isNotificationServiceEnabled()){
            enableNotificationListenerAlertDialog = buildNotificationServiceAlertDialog();
            enableNotificationListenerAlertDialog.show();
        }
*/
        /*
        // Finally we register a receiver to tell the MainActivity when a notification has been received
        imageChangeBroadcastReceiver = new ImageChangeBroadcastReceiver();
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction("com.github.chagall.notificationlistenerexample");
        registerReceiver(imageChangeBroadcastReceiver,intentFilter);
*/
        // 如果 Intent 帶有資訊，包含點擊 notification 進入，直接跳過 launch 等待時間，但還是必須進入 launch 頁面檢查權限
        boolean isNotificationClick = false;

        Bundle data = activityIntent.getExtras();

        if (data != null) {

            String type = data.getString("type");
            String thingId = data.getString(MyFirebaseMessagingService.THING_ID);
            String callSlotId = data.getString(MyFirebaseMessagingService.CALL_SLOT_ID);
            String thingName = data.getString(MyFirebaseMessagingService.THING_NAME);
            String sound = data.getString(MyFirebaseMessagingService.SOUND);
            String callname = data.getString(MyFirebaseMessagingService.CALLNAME_ID);
            String number = data.getString(MyFirebaseMessagingService.CALL_NUMBER_ID);
            String click = data.getString(MyFirebaseMessagingService.NOTIFY_CLICK);

            if (click.equals("null") == false) {


                if (type != null && type.equals("event")) {
                    String event = data.getString("event");
                    if (event != null && event.equals("call")) {
                        if (click.equals("action")) {
                            isNotificationClick = true;
                        }
                    }
                }

                cn = new ComponentName(this, AdminReceiver.class);
                mgr = (DevicePolicyManager) getSystemService(DEVICE_POLICY_SERVICE);


                Method ReflectScreenState;
                boolean isScreenOn = false;
                try {
                    ReflectScreenState = PowerManager.class.getMethod("isScreenOn", new Class[]{});
                    PowerManager pm = (PowerManager) getApplicationContext().getSystemService(Activity.POWER_SERVICE);
                    isScreenOn = (Boolean) ReflectScreenState.invoke(pm);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                PowerManager pm = (PowerManager) getSystemService(Context.POWER_SERVICE);
                boolean isInteractive = pm.isInteractive();

                //acquireWakeLock();
                //releaseWakeLock();
                String last_slot = SharedPreferencesInfo.getString(MyFirebaseMessagingService.CALL_NUMBER_ID, "");
                if (null != thingId && null != callSlotId && sound != null && thingName != null && null != number && !number.equals(last_slot)) {
                    ThingsManager.getInstance(MyApplication.getAppContext()).updateThingEventCountsAddOne(thingId);
                    if (C2CProcess.getInstance().getP2PServerStatus() != C2CProcess.P2P_STATUS_DEVICE_CONNECTED) {
                        FNLog.d(TAG, "onCreate in Call = " + getIntent().getExtras().getString(MyFirebaseMessagingService.CALLNAME_ID));
                        SharedPreferencesInfo.putString(MyFirebaseMessagingService.CALL_NUMBER_ID, number);
                        if (isInteractive) {
                            if (isInteractive)
                                isScreenOn = true;
                            else
                                isScreenOn = false;
                        }
                        if(MyFirebaseMessagingService.getFlage())
                        {
                            isScreenOn = true;
                        }
                        else
                        {
                            if( click.equals("action") )
                            {
                                isScreenOn = true;
                            }
                        }

                        CallFragment.openFragment(true, thingId, thingName, sound, callSlotId, isScreenOn);
                    }

                } else {
                    FNLog.d(TAG, "onCreate in Data");
                    LaunchFragment.openFragment(true, isNotificationClick);
                }
            }
        } else {
            FNLog.d(TAG, "onCreate not Data");
            LaunchFragment.openFragment(true, false);
        }

    }
    private PowerManager.WakeLock   wakeLock=null;
    private boolean hold=false;
    private boolean isInteractive = false;
    private void acquireWakeLock() {

        if (wakeLock ==null) {
            PowerManager pm = (PowerManager) getSystemService(Context.POWER_SERVICE);
            isInteractive = pm.isInteractive();
            wakeLock = pm.newWakeLock(PowerManager.PARTIAL_WAKE_LOCK, this.getClass().getCanonicalName());
            hold=wakeLock.isHeld();

            //wakeLock.acquire();
        }
    }


    private void releaseWakeLock() {
        hold=wakeLock.isHeld();
        if (wakeLock !=null&& wakeLock.isHeld()) {
            wakeLock.release();
            wakeLock =null;
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        FNLog.d(TAG, "onResumea");
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        MyApplication.activityResumed();
    }

    @Override
    protected void onPause() {
        getWindow().clearFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        super.onPause();
        MyApplication.activityPaused();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
 //       getIntent().putExtras(Bundle.EMPTY);
    }

    @Override
    protected void onSaveInstanceState(@NonNull Bundle outState) {
        // 解決當內存不足Activity被回收，Fragment不會被回收，導致Fragment重複add產生重疊問題
        // super.onSaveInstanceState(outState);
        super.onSaveInstanceState(outState);
    }

    @Override
    public void onBackPressed() {
        if (FNFragmentManager.getInstance().callBackPressed())
            return;
        super.onBackPressed();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        FNFragmentManager.getInstance().onRequestPermissionsResult(requestCode, permissions, grantResults);
    }

    private void checkForUpdates() {
        inAppUpdateManager = InAppUpdateManager.Builder(this, REQ_CODE_VERSION_UPDATE)
                .resumeUpdates(true) // Resume the update, if the update was stalled. Default is true
                .mode(Constants.UpdateMode.IMMEDIATE);

        inAppUpdateManager.checkForAppUpdate();
    }


    private void destroyMqtt() {
        MqttManager.getInstance(this).disConnect();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        if (requestCode == REQ_CODE_VERSION_UPDATE) {
            if (resultCode == Activity.RESULT_CANCELED) {
                // If the update is cancelled by the user,
                // you can request to start the update again.
                inAppUpdateManager.checkForAppUpdate();

                Log.d(TAG, "Update flow failed! Result code: " + resultCode);
            }
        }

        super.onActivityResult(requestCode, resultCode, data);
    }

    // InAppUpdateHandler implementation

    @Override
    public void onInAppUpdateError(int code, Throwable error) {
        /*
         * Called when some error occurred. See Constants class for more details
         */
        Log.d(TAG, "code: " + code, error);
    }

    @Override
    public void onInAppUpdateStatus(InAppUpdateStatus status) {

        /*
         * Called when the update status change occurred.


        progressBar.setVisibility(status.isDownloading() ? View.VISIBLE : View.GONE);

        tvVersionCode.setText(String.format("Available version code: %d", status.availableVersionCode()));
        tvUpdateAvailable.setText(String.format("Update available: %s", String.valueOf(status.isUpdateAvailable())));

        if (status.isDownloaded()) {
            updateButton.setText("Complete Update");
            updateButton.setOnClickListener(view -> inAppUpdateManager.completeUpdate());
        }

         */
    }


    /**
     * Change Intercepted Notification Image
     * Changes the MainActivity image based on which notification was intercepted
     * @param notificationCode The intercepted notification code
     */
    private void changeInterceptedNotificationImage(int notificationCode){
        switch(notificationCode){
            case MyNotificationListenerService.InterceptedNotificationCode.PTCOM_CODE:
                interceptedNotificationImageView.setImageResource(R.drawable.logo);
                break;

            case MyNotificationListenerService.InterceptedNotificationCode.OTHER_NOTIFICATIONS_CODE:
                interceptedNotificationImageView.setImageResource(R.drawable.logo);
                break;
        }
    }
    /**
     * Is Notification Service Enabled.
     * Verifies if the notification listener service is enabled.
     * Got it from: https://github.com/kpbird/NotificationListenerService-Example/blob/master/NLSExample/src/main/java/com/kpbird/nlsexample/NLService.java
     * @return True if enabled, false otherwise.
     */
    private boolean isNotificationServiceEnabled(){
        String pkgName = getPackageName();
        final String flat = Settings.Secure.getString(getContentResolver(),
                ENABLED_NOTIFICATION_LISTENERS);
        if (!TextUtils.isEmpty(flat)) {
            final String[] names = flat.split(":");
            for (int i = 0; i < names.length; i++) {
                final ComponentName cn = ComponentName.unflattenFromString(names[i]);
                if (cn != null) {
                    if (TextUtils.equals(pkgName, cn.getPackageName())) {
                        return true;
                    }
                }
            }
        }
        return false;
    }
    /**
     * Image Change Broadcast Receiver.
     * We use this Broadcast Receiver to notify the Main Activity when
     * a new notification has arrived, so it can properly change the
     * notification image
     * */
    public class ImageChangeBroadcastReceiver extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent intent) {
            int receivedNotificationCode = intent.getIntExtra("Notification Code",-1);
            changeInterceptedNotificationImage(receivedNotificationCode);
        }
    }
    private AlertDialog buildNotificationServiceAlertDialog(){
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
        alertDialogBuilder.setTitle(R.string.notification_listener_service);
        alertDialogBuilder.setMessage(R.string.notification_listener_service_explanation);
        alertDialogBuilder.setPositiveButton(R.string.yes,
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        startActivity(new Intent(ACTION_NOTIFICATION_LISTENER_SETTINGS));
                    }
                });
        alertDialogBuilder.setNegativeButton(R.string.no,
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        // If you choose to not enable the notification listener
                        // the app. will not work as expected
                    }
                });
        return(alertDialogBuilder.create());
    }

}
