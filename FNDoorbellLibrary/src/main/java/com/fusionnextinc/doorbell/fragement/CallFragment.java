package com.fusionnextinc.doorbell.fragement;

import android.annotation.SuppressLint;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.ActivityInfo;
import android.graphics.ColorMatrix;
import android.graphics.ColorMatrixColorFilter;
import android.media.AudioManager;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.DecelerateInterpolator;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.fusionnext.cloud.iot.Public;
import com.fusionnextinc.doorbell.HomeKeyReceiver;
import com.fusionnextinc.doorbell.MyApplication;
import com.fusionnextinc.doorbell.MyFirebaseMessagingService;
import com.fusionnextinc.doorbell.R;
import com.fusionnextinc.doorbell.manager.account.AaccountHandler;
import com.fusionnextinc.doorbell.manager.account.AccountManager;
import com.fusionnextinc.doorbell.manager.thingsManager.ThingsHandler;
import com.fusionnextinc.doorbell.manager.thingsManager.ThingsManager;
import com.fusionnextinc.doorbell.objects.FNError;
import com.fusionnextinc.doorbell.objects.FNEvent;
import com.fusionnextinc.doorbell.objects.FNFirmware;
import com.fusionnextinc.doorbell.objects.FNSchedule;
import com.fusionnextinc.doorbell.objects.FNThing;
import com.fusionnextinc.doorbell.objects.FNUserProfile;
import com.fusionnextinc.doorbell.utils.FNLog;
import com.fusionnextinc.doorbell.utils.TimesUtil;
import com.fusionnextinc.doorbell.voice.FNAudioManager;
import com.fusionnextinc.doorbell.voice.FNRingToneManager;
import com.fusionnextinc.doorbell.voice.OnFNAudioManagerRecordListener;
import com.fusionnextinc.doorbell.voice.OnFNRingToneListener;
import com.fusionnextinc.doorbell.widget.FNAlert.FNAlertDialog;
import com.fusionnextinc.doorbell.widget.FNDialog;
import com.fusionnextinc.doorbell.widget.FNLayoutUtil;
import com.fusionnextinc.doorbell.widget.FNToast;
import com.iptnet.c2c.C2CHandle;
import com.iptnet.common.c2c.C2CProcess;
import com.iptnet.common.c2c.MainService;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.concurrent.atomic.AtomicBoolean;

import static com.fusionnextinc.doorbell.R.string.fn_msg_error_system_busy;


/**
 * Created by Mike Chang on 2019/4/11
 */
public class CallFragment extends FNFragment implements View.OnClickListener {
    private static final String TAG = CallFragment.class.getSimpleName();
    private static final String otherEvent = "65537";
    private FNLayoutUtil mLayoutUtil;
    private ImageView mImgCallLogo, mImgReject, mImgAccept, mImgHangUp, mImgMute, mImgSpeaker,mBedDone;
    private TextView mTxtTitle, mTxtSubtitle;
    private String mThingId, mNotificationTitle, mNotificationSubtitle, mNotificationSound, mSlotId;
    private LinearLayout mLlCaling, mLlCalled,mLlBedup;
    private FNRingToneManager mFNRingToneManager = FNRingToneManager.getInstance();
    private final int ANIMATION_FLICK_INTERVAL = 500;
    private final int RINGTONE_PLAY_REPEAT_TIMES = 20;
    private final long RINGTONE_PLAY_DURATION = 30 * 1000;

    private ThingsManager mThingsManager;
    private FNAudioManager mAudioManager = FNAudioManager.getInstance();
    private C2CProcess mC2CProcess;
    private C2CRegisterEvent mC2CRegisterEvent;
    private int mLineID = -1;
    private boolean mIsFragmentDestoryed = false;
    private int mFrameSeq = 0;
    private boolean     mmInNotifyCall=false;
    private AccountManager mAccountManager;

    private  final AtomicBoolean mMuteLock = new AtomicBoolean(false);
    private  final AtomicBoolean mMuteEnable = new AtomicBoolean(false);


    public static void openFragment(String id, String title, String sound, String slotId) {
        CallFragment callFragment = new CallFragment();
        callFragment.mThingId = id;
        callFragment.mmInNotifyCall = false;
        callFragment.mNotificationTitle = title;
        callFragment.mNotificationSound = sound;
        callFragment.mSlotId = slotId;
        FNFragmentManager.getInstance().replaceFragment(callFragment);
    }

    public static void openFragment(boolean clearOtherStack, String id, String title, String sound, String slotId) {
        CallFragment callFragment = new CallFragment();
        callFragment.mThingId = id;
        callFragment.mmInNotifyCall = false;
        callFragment.mNotificationTitle = title;
        callFragment.mNotificationSound = sound;
        callFragment.mSlotId = slotId;
        FNFragmentManager.getInstance().openFragment(callFragment, clearOtherStack);
    }
    public static void openFragment(boolean clearOtherStack, String id, String title, String sound, String slotId,boolean inNotify ) {
        CallFragment callFragment = new CallFragment();
        callFragment.mThingId = id;
        callFragment.mmInNotifyCall = inNotify;
        callFragment.mNotificationTitle = title;
        callFragment.mNotificationSound = sound;
        callFragment.mSlotId = slotId;
        FNFragmentManager.getInstance().openFragment(callFragment, clearOtherStack);
    }


    private boolean isBedEvent()
    {
        return otherEvent.equals(mSlotId);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Public.getAuthorization(getContext()).getUserAccessToken();
        mAccountManager = AccountManager.getInstance(getActivity());
        mThingsManager = ThingsManager.getInstance(getActivity());
        mThingsManager.setThingsListener(mThingsListener);
        mLayoutUtil = new FNLayoutUtil(getActivity(), MyApplication.LAYOUT_WIDTH, MyApplication.LAYOUT_HEIGHT, FNLayoutUtil.BASE_WIDTH);
        getContext().startService(new Intent(getContext(), MainService.class));
        // get C2C process instance
        mC2CProcess = C2CProcess.getInstance();
        mC2CProcess.setMediaStreamCallback(new MediaReceivedCallback());
        mC2CProcess.showDebugMessage(true);
        mC2CProcess.addProtocolMessageCallback(mC2CRegisterEvent = new C2CRegisterEvent(), new int[]{
                C2CHandle.Message.C2C_REGISTERING, C2CHandle.Message.C2C_REGISTER_DONE, C2CHandle.Message.C2C_REGISTER_FAIL,
                C2CHandle.Message.C2C_SETUP_DONE, C2CHandle.Message.C2C_SETUP_ERROR, C2CHandle.Message.C2C_RECV_ALERT,
                C2CProcess.C2C_PACKET_LOSS, C2CProcess.C2C_CALL_TERMINATED,
                C2CHandle.Message.C2C_P2P_MODE, C2CHandle.Message.C2C_RELAY_MODE
        });
        mC2CProcess.requestProtocolCommandCallback(new C2CCommandCallBack());

        HomeKeyReceiver.unregisterHomeKeyReceiver(getActivity());
        HomeKeyReceiver.registerHomeKeyReceiver(getActivity(), new HomeKeyReceiver.HomeKeyListener() {
            @Override
            public void homeKey() {
                Log.i(TAG, "onStop():  mP2PServerStatus = " + mC2CProcess.getP2PServerStatus());
                if (mC2CProcess.getP2PServerStatus() == C2CProcess.P2P_STATUS_IDLE) {
                    hangUp();
                } else {
                    endCall();
                }
            }
        });
    }

    @SuppressLint("UseCompatLoadingForDrawables")
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        final ViewGroup nullParent = null;

        getActivity().getWindow().addFlags(WindowManager.LayoutParams.FLAG_SHOW_WHEN_LOCKED //鎖屏顯示
                | WindowManager.LayoutParams.FLAG_DISMISS_KEYGUARD //解鎖
                | WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON //保持屏幕不息屏
                | WindowManager.LayoutParams.FLAG_TURN_SCREEN_ON);//點亮屏幕
        getActivity().setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        getActionBar().hide();
        View view = inflater.inflate(R.layout.fragment_call, nullParent);
        mLayoutUtil.fitAllView(view);

        mLlCaling = view.findViewById(R.id.ll_calling);
        mLlCalled = view.findViewById(R.id.ll_called);
        mLlBedup = view.findViewById(R.id.ll_bedup);
        mImgCallLogo = view.findViewById(R.id.img_call_logo);
        if( isBedEvent() )
        {
            mNotificationSound = "bed_alert";
            mImgCallLogo.setImageDrawable(getResources().getDrawable(R.drawable.bed_logo));
        }

        mTxtTitle = view.findViewById(R.id.txt_call_title);
        mTxtSubtitle = view.findViewById(R.id.txt_call_subtitle);
        mImgReject = view.findViewById(R.id.img_reject);
        mImgAccept = view.findViewById(R.id.img_accept);
        mImgHangUp = view.findViewById(R.id.img_hang_up);
        mImgMute = view.findViewById(R.id.img_mute);
        mImgSpeaker = view.findViewById(R.id.img_speaker);
        mBedDone = view.findViewById(R.id.img_beddone);

        mImgMute.setTag(R.drawable.mute_disable);
        mImgHangUp.setTag(R.drawable.hang_up_disable);


        mImgSpeaker.setTag(R.drawable.speaker_off);

        if( isBedEvent() ) {
            mLlCaling.setVisibility(View.GONE);
            mLlCalled.setVisibility(View.GONE);
            mLlBedup.setVisibility(View.VISIBLE);
        }
        else {
            mLlCaling.setVisibility(View.VISIBLE);
            mLlCalled.setVisibility(View.GONE);
            mLlBedup.setVisibility(View.GONE);
        }

        mImgReject.setOnClickListener(this);
        mImgAccept.setOnClickListener(this);
        mImgHangUp.setOnClickListener(this);
        mImgMute.setOnClickListener(this);
        mImgSpeaker.setOnClickListener(this);
        mBedDone.setOnClickListener(this);

        ColorMatrix matrix = new ColorMatrix();
        matrix.setSaturation(0);//飽和度 0灰色 100過度彩色，50正常
        ColorMatrixColorFilter filter = new ColorMatrixColorFilter(matrix);
        mImgSpeaker.setColorFilter(filter);
        mImgSpeaker.setEnabled(false);

        mMuteLock.set(false);
        mMuteEnable.set(false);
        mImgMute.setImageResource(R.drawable.mute_disable);
        mImgHangUp.setImageResource(R.drawable.hang_up_disable);

        if (mNotificationTitle != null) {
            mTxtTitle.setText(mNotificationTitle);
            mNotificationSubtitle = getString(R.string.fn_btn_someone_knock).replace("${NAME}", getString(R.string.fn_title_thing_name));
        }

        if (mNotificationSubtitle != null) {
            mTxtSubtitle.setText(mNotificationSubtitle);
        }
        if(mmInNotifyCall == false) {
            mFNRingToneManager.playWithTime(getActivity(), mThingsManager.getRingtoneRawByFile(mNotificationSound), RINGTONE_PLAY_DURATION);
            mFNRingToneManager.setFNRingToneListener(new OnFNRingToneListener() {
                @Override
                public void OnCompletion() {
                    onBackPressed();
                }
            });
        }

        startAnimationFlick(mImgCallLogo);
/*
        new Thread(new Runnable() {
            public void run() {
                try {
                    Thread.sleep(5000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                hangUp_X();
            }
        }).start();
*/
        if(mmInNotifyCall == true )
        {
            mTxtSubtitle.setText(getString(R.string.msg_connecting));
            accept();
        }
        else
        {
            startAnimationFlick(mImgCallLogo);
        }

        return view;
    }

    @Override
    public void onClick(View view) {
        if (view.getId() == R.id.img_reject || view.getId() == R.id.img_beddone) {
            hangUp();
        } else if (view.getId() == R.id.img_accept) {

            mTxtSubtitle.setText(getString(R.string.msg_connecting));
            accept();
        } else if (view.getId() == R.id.img_hang_up) {
            if (mImgHangUp.getTag().equals(R.drawable.hang_up))
            {
                if (mC2CProcess.getP2PServerStatus() == C2CProcess.P2P_STATUS_DEVICE_CONNECTED)
                {
                    endCall();
                } else {
                    FNToast.makeText(getContext(), getContext().getString(R.string.error_system_busy), Toast.LENGTH_SHORT).show();
                }
            }
        } else if (view.getId() == R.id.img_mute) {
            if (mImgMute.getTag().equals(R.drawable.mute_off)) {
                mImgMute.setImageResource(R.drawable.mute_on);
                //Mute(true);
                mImgMute.setTag(R.drawable.mute_on);
                mMuteLock.set(true);
                //int ret = mC2CProcess.sendRtpCommand(mLineID, "PLAYBACK", "0");
            } else if(mImgMute.getTag().equals(R.drawable.mute_on)){
                mImgMute.setImageResource(R.drawable.mute_off);
                //Mute(false);
                mImgMute.setTag(R.drawable.mute_off);
                mMuteLock.set(false);
                //int ret = mC2CProcess.sendRtpCommand(mLineID, "PLAYBACK", "1");
            }
        } else if (view.getId() == R.id.img_speaker) {
            if (mImgSpeaker.getTag().equals(R.drawable.speaker_off)) {
                mImgSpeaker.setImageResource(R.drawable.speaker_on);
                mImgSpeaker.setTag(R.drawable.speaker_on);
               Mute(true);
            } else {
                mImgSpeaker.setImageResource(R.drawable.speaker_off);
                mImgSpeaker.setTag(R.drawable.speaker_off);
                Mute(false);
            }
        }
    }

    /**
     * 开启View闪烁效果
     */
    private void startAnimationFlick(View view) {
        if (null == view) {
            return;
        }
        Animation alphaAnimation = new AlphaAnimation(1, 0);
        alphaAnimation.setDuration(ANIMATION_FLICK_INTERVAL);
        alphaAnimation.setInterpolator(new DecelerateInterpolator());
        alphaAnimation.setRepeatCount(Animation.INFINITE);
        alphaAnimation.setRepeatMode(Animation.REVERSE);
        view.startAnimation(alphaAnimation);
    }

    /**
     * 取消View闪烁效果
     */
    private void stopAnimationFlick(View view) {
        if (null == view) {
            return;
        }
        view.clearAnimation();
    }

    @Override
    public void onBackPressed() {
        if (!mIsFragmentDestoryed) {
            super.onBackPressed(true);
            mFNRingToneManager.destory();
            ThingsFragment thingsFragment = new ThingsFragment();
            thingsFragment.setUpdateThing(true, mThingId);
            mIsFragmentDestoryed = true;
        }
    }

    private BroadcastReceiver mReceiver = new BroadcastReceiver() {

        @Override
        public void onReceive(Context context, Intent intent) {
            if (intent.getAction().equals(MyApplication.BROADCAST_ANSWER_EVENT)) {
                Log.i(TAG, " onReceive():  ");
                onBackPressed();
            }
        }
    };

    @Override
    public void onResume() {
        super.onResume();
        IntentFilter filter = new IntentFilter();
        filter.addAction(MyApplication.BROADCAST_ANSWER_EVENT);
        filter.addAction(MyApplication.BROADCAST_HANGUP_EVENT);

        getActivity().registerReceiver(mReceiver, filter);
    }

    @Override
    public void onPause() {
        super.onPause();
        if (mC2CProcess.getP2PServerStatus() == C2CProcess.P2P_STATUS_DEVICE_CONNECTED) {
            endCall();
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mFNRingToneManager.destory();
        // release the C2C module resourc
        mC2CProcess.setP2PServerStatus(C2CProcess.P2P_STATUS_IDLE);
        mC2CProcess.removeProtocolMessageCallback(mC2CRegisterEvent);
        HomeKeyReceiver.unregisterHomeKeyReceiver(getActivity());
        getActivity().unregisterReceiver(mReceiver);
        Log.e(TAG, "onDestroy");
    }

    private class C2CRegisterEvent implements C2CHandle.ProtocolMessageCallback {
        private Integer noticeLineId;
        private String noticePeerId;

        @Override
        public Object receiveProtocolMessage(int lineId, int message, int subEvent, Bundle bundle) {
            Log.i(TAG, "receiveProtocolMessage " + message);
            if (C2C_REGISTERING == message) {
                Log.i(TAG, "C2C_REGISTERING");
            }
            if (C2C_REGISTER_DONE == message) {
                Log.i(TAG, "C2C_REGISTER_DONE");
                mC2CProcess.setP2PServerStatus(C2CProcess.P2P_STATUS_SETUP_DONE);
            } else if (C2C_REGISTER_FAIL == message) {
                String msg;
                if (C2C_SRV_NO_RESP == subEvent) {

                } else if (C2C_SRV_DISCONNECT == subEvent) {

                } else if (C2C_SVR_FAIL == subEvent) {

                } else if (C2C_FORBIDDEN == subEvent) {

                } else if (C2C_UNAUTHORIZED == subEvent) {

                } else if (C2C_INVALID_ACCOUNT == subEvent) {

                } else {
                }
            } else if (C2C_SETUP_DONE == message) {


            } else if (C2C_SETUP_ERROR == message) {
                noticePeerId = null;
                Log.d(TAG, "setting notification fail event=" + subEvent + " peer=" + bundle.getString("peerId"));
            } else if (C2C_RECV_ALERT == message) {

            } else if (C2CProcess.C2C_PACKET_LOSS == message) {
                // terminated
                Log.i(TAG, "C2C_PACKET_LOSS");
                mC2CProcess.setP2PServerStatus(C2CProcess.P2P_STATUS_DEVICE_DISCONNECTED);
                endCall();
            } else if (C2CProcess.C2C_CALL_TERMINATED == message) {
                Log.d(TAG, "P2P Mode");
                mC2CProcess.setP2PServerStatus(C2CProcess.P2P_STATUS_DEVICE_DISCONNECTED);
                endCall();
            } else if (C2CProcess.C2C_P2P_MODE == message) {
                mC2CProcess.setP2PServerStatus(C2CProcess.P2P_STATUS_DEVICE_CONNECTED);
                String userName = mAccountManager.getUserName();
                JSONObject jsonObject = new JSONObject();
                try {
                    jsonObject.put("user_name", userName.length() < 0 || userName.equals("") ? getString(R.string.fn_text_none) : userName);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                int ret = mC2CProcess.sendRtpCommand(mLineID, "USERINFO", jsonObject.toString());
                sendAudioByP2P();
                Log.d(TAG, "P2P Mode:" + jsonObject.toString());
            } else if (C2CProcess.C2C_RELAY_MODE == message) {
                Log.d(TAG, "Relay Mode");
                mC2CProcess.setP2PServerStatus(C2CProcess.P2P_STATUS_DEVICE_CONNECTED);
                String userName = mAccountManager.getUserName();
                JSONObject jsonObject = new JSONObject();
                try {
                    jsonObject.put("user_name", userName.length() < 0 || userName.equals("") ? getString(R.string.fn_text_none) : userName);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                int ret = mC2CProcess.sendRtpCommand(mLineID, "USERINFO", jsonObject.toString());
                sendAudioByP2P();
            }
            return null;
        }
    }

    private class TerminateMessageCallback implements C2CHandle.ProtocolMessageCallback {

        boolean mPacketLoss;

        @Override
        public Object receiveProtocolMessage(int lineId, int message, int subEvent, Bundle params) {
            // packet loss event
            if (C2CProcess.C2C_PACKET_LOSS == message) {
                mPacketLoss = true;
                // terminated
            } else if (C2CProcess.C2C_CALL_TERMINATED == message) {
            }
            return null;
        }
    }

    private class MediaReceivedCallback implements C2CHandle.MediaStreamCallback {
        private boolean mNotSupported, mGetVideoPayloadType, mGetAudioPayloadType;

        @Override
        public void receiveVideoStream(int lineId, byte[] data, int dataLen, int payloadType, int timestamp, int frameRate, int frameType, int frameSeq) {
        }

        @Override
        public void receiveAudioStream(int lineId, byte[] data, int dataLen, int payloadType, int timestamp, int frameSeq) {
            FNLog.i(TAG, "Receive Audio Length " + dataLen);
           // if( !mMuteLock.get())
           // {
            if(mmInNotifyCall == true)
            {
                mAudioManager.play(data);
            }
            //}

        }

    }

    private class C2CCommandCallBack implements C2CHandle.ProtocolMessageCallback {
        @Override
        public Object receiveProtocolMessage(int lineId, int message, int subEvent, Bundle bundle) {
            if (C2CProcess.C2C_COMMAND_ACK == message) {
                FNLog.d(TAG, "C2C_COMMAND_ACK");
            } else if (C2CProcess.C2C_COMMAND_MESSAGE == message) {
                FNLog.d(TAG, "C2C_COMMAND_MESSAGE");
            }
            return null;
        }
    }

    final ThingsHandler.ThingsListener mThingsListener = new ThingsHandler.ThingsListener() {

        @Override
        public void onListUserOwnedThings(ThingsManager instance, boolean result, String userId, ArrayList<FNThing> thingArrayList, FNError error) {

        }

        @Override
        public void onListUserInvitedThings(ThingsManager instance, boolean result, String userId, ArrayList<FNThing> thingArrayList, FNError error) {

        }

        @Override
        public void onListUserSharedThings(ThingsManager instance, boolean result, String userId, ArrayList<FNThing> thingArrayList, FNError error) {

        }

        @Override
        public void onListUserAllThings(ThingsManager instance, boolean result, String userId, ArrayList<FNThing> ownedThingArrayList, ArrayList<FNThing> sharedThingArrayList, ArrayList<FNThing> invitedThingArrayList, FNError error) {

        }

        @Override
        public void onListThingEvents(ThingsManager instance, boolean result, String thingId, String eventName, ArrayList<FNEvent> eventList, FNError error) {

        }

        @Override
        public void onListThingEventsByTime(ThingsManager instance, boolean result, String thingId, String eventName, String startTime, String endTime, ArrayList<FNEvent> eventList, FNError error) {

        }

        @Override
        public void onCurBatteryLevel(ThingsManager instance, boolean result, String thingId, String batteryLevel, FNError error) {

        }

        @Override
        public void onCurFirmwareVersion(ThingsManager instance, boolean result, String thingId, String fwVersion, FNError error) {

        }

        @Override
        public void onCurSwitchedStatus(ThingsManager instance, boolean result, String thingId, boolean isSwitch, FNError error) {

        }

        @Override
        public void onNewFirmwareVersion(ThingsManager instance, boolean result, String thingId, FNFirmware fnFirmware) {

        }

        @Override
        public void onStartFwUpdate(ThingsManager instance, boolean result, String thingId, String fwId) {

        }

        @Override
        public void onCurRingTone(ThingsManager instance, boolean result, String thingId, String ringtoneId, FNError error) {

        }

        @Override
        public void onGetScheduleList(ThingsManager instance, boolean result, String thingId, ArrayList<FNSchedule> scheduleList, FNError error) {

        }

        @Override
        public void onListInvitedUsers(ThingsManager instance, boolean result, String thingId, ArrayList<FNUserProfile> userList, FNError error) {

        }

        @Override
        public void onListSharedUsers(ThingsManager instance, boolean result, String thingId, ArrayList<FNUserProfile> userList, FNError error) {

        }

        @Override
        public void onCreateClaimCode(ThingsManager instance, boolean result, String claimCode, FNError error) {

        }

        @Override
        public void onCreateThing(ThingsManager instance, boolean result, FNThing fnThing, FNError error) {

        }

        @Override
        public void onUpdateThing(ThingsManager instance, boolean result, FNThing fnThing, FNError error) {

        }

        @Override
        public void onDeleteThing(ThingsManager instance, boolean result, FNError error) {

        }

        @Override
        public void onGetThing(ThingsManager instance, boolean result, FNThing fnThing, FNError error) {

        }

        @Override
        public void onChangeRingtone(ThingsManager instance, boolean result, String thingId, int ringtoneId) {

        }

        @Override
        public void onSetTimer(ThingsManager instance, boolean result, String thingId, String value) {

        }

        @Override
        public void onSetSchedule(ThingsManager instance, boolean result, String thingId, String value) {

        }

        @Override
        public void onShareInvited(ThingsManager instance, boolean result, String thingId, FNError error) {

        }

        @Override
        public void onShareAccepted(ThingsManager instance, boolean result, String thingId, FNError error) {

        }

        @Override
        public void onShareRejected(ThingsManager instance, boolean result, String thingId, FNError error) {

        }

        @Override
        public void onSwitchOnOff(ThingsManager instance, boolean result, String thingId, boolean isTurnOn) {

        }

        @Override
        public void onCloseCallSlot(ThingsManager instance, boolean result, FNError error) {
            mFNRingToneManager.destory();
        }

        @Override
        public void onTakeCallSlot(ThingsManager instance, boolean result, String thingId, final String callSlot, final FNError error) {
            Log.i(TAG, "onTakeCallSlot (" + result + ", " + thingId + ", " + callSlot + ")");
            if (mC2CProcess.getP2PServerStatus() == C2CProcess.P2P_STATUS_SETUP_DONE && result) {
                mLineID = mC2CProcess.connect(callSlot, "root", "admin");
                Log.i(TAG, "mC2CProcess.connect : mLineID = " + mLineID);

                if (mLineID < 0) {
                    p2pRetry(callSlot, error);
                }
            } else if (result) {
                p2pRetry(callSlot, error);
            } else {
                if (isAdded()) {
                    FNDialog.showErrorDialog(getActivity(), getString(R.string.fn_msg_error_system_busy), error, FNAlertDialog -> {
                        endCall();
                        FNAlertDialog.dismiss();
                    });
                }
            }
        }
    };

    private void sendAudioByP2P() {
        if (mLineID >= 0) {
            setSpeakerphoneOn(false);

            //mImgMute.setTag(R.drawable.mute_on);
            if( mMuteEnable.compareAndSet(false,true))
            {
                mImgMute.postDelayed(new Runnable(){
                                @Override
                                public void run() {

                                    mImgMute.setImageResource(R.drawable.mute_off);
                                    mImgHangUp.setImageResource(R.drawable.hang_up);

                                    mImgMute.setTag(R.drawable.mute_off);
                                    mImgHangUp.setTag(R.drawable.hang_up);
                                }}
                        ,500);
            }

            mAudioManager.startRecord(new OnFNAudioManagerRecordListener() {
                @Override
                public void onStartRecord(FNAudioManager voiceManager) {
                    mFrameSeq = 0;
                }

                @Override
                public void onRecordData(FNAudioManager voiceManager, byte[] data) {
                    if( mMuteLock.get())
                    {
                        java.util.Arrays.fill(data, (byte) 0);
                    }
                    int ret = mC2CProcess.audioPutInData(mLineID, data, data.length, (int) System.currentTimeMillis(), mFrameSeq++, 0, C2CHandle.PayloadType.PCM_8K);
                    Log.i(TAG, "onRecordData: length  =  " + data.length);
                }

                @Override
                public void onStopRecord(FNAudioManager voiceManager) {

                }

                @Override
                public void onRecordCompleted(FNAudioManager voiceManager) {

                }

                @Override
                public void onRecordError(FNAudioManager voiceManager) {

                }

                @Override
                public void onRecordingTimeUpdated(long recordingTime) {
                    mTxtSubtitle.setText(TimesUtil.convertSecondToHHMMString(recordingTime));
                }
            });
            int ret = mC2CProcess.sendRtpCommand(mLineID, "PLAYBACK", "1");
        }
    }

    public  void Mute( boolean enable )
    {
        AudioManager audioManager = (AudioManager) getContext().getSystemService(Context.AUDIO_SERVICE);

        if (enable) {
            audioManager.setMicrophoneMute(true);
        } else {
            audioManager.setMicrophoneMute(false);
        }

    }


    private void setSpeakerphoneOn(boolean enable) {
        AudioManager audioManager = (AudioManager) getContext().getSystemService(Context.AUDIO_SERVICE);

        if (enable) {
            audioManager.setMode(AudioManager.MODE_IN_CALL);
            audioManager.setSpeakerphoneOn(true);
        } else {
            audioManager.setMode(AudioManager.MODE_IN_CALL);
            audioManager.setSpeakerphoneOn(false);
        }
    }

    private void endCall() {
        if( !isBedEvent() ) {
            if (mC2CProcess.getP2PServerStatus() != C2CProcess.P2P_STATUS_DEVICE_DISCONNECTED) {
                if (mLineID >= 0) {
                    mC2CProcess.terminate(mLineID);
                }
            }
            mAudioManager.stopRecord();
            mAudioManager.stop();
            mThingsManager.closeCallSlot(mSlotId);
        }
        onBackPressed();
        if( !isBedEvent() ){
            mC2CProcess.setP2PServerStatus(C2CProcess.P2P_STATUS_IDLE);
        }
    }
    private void hangUp_X() {
        if( !isBedEvent() ) {
            mThingsManager.closeCallSlot(mSlotId);
            mThingsManager.setThingsListener(mThingsListener);
            //stopAnimationFlick(mImgCallLogo);
        }
        mFNRingToneManager.destory();
        onBackPressed();
        if( !isBedEvent() ) {
            mC2CProcess.setP2PServerStatus(C2CProcess.P2P_STATUS_IDLE);
        }
    }
    private void hangUp() {
        if( !isBedEvent() )
        {
            mThingsManager.closeCallSlot(mSlotId);
            mThingsManager.setThingsListener(mThingsListener);
        }
        stopAnimationFlick(mImgCallLogo);
        mFNRingToneManager.destory();
        onBackPressed();
        if( !isBedEvent() ) {
            mC2CProcess.setP2PServerStatus(C2CProcess.P2P_STATUS_IDLE);
        }
    }

    private void accept() {

        mFNRingToneManager.destory();
        if( !isBedEvent() ) {
            mThingsManager.takeCallSlot(mSlotId);
            mThingsManager.setThingsListener(mThingsListener);
        }
        stopAnimationFlick(mImgCallLogo);
        if( !isBedEvent() ) {
            mLlCaling.setVisibility(View.GONE);
            mLlCalled.setVisibility(View.VISIBLE);
            mC2CProcess.setP2PServerStatus(C2CProcess.P2P_STATUS_ACCEPT);
        }
    }

    private void p2pRetry(final String callSlot, final FNError error) {
        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    Thread.sleep(1000);
                    mLineID = mC2CProcess.connect(callSlot, "root", "admin");
                    if (mLineID < 0) {
                        if (isAdded()) {
                            FNDialog.showErrorDialog(getActivity(), getString(R.string.fn_msg_error_system_busy), error, FNAlertDialog -> {
                                endCall();
                                FNAlertDialog.dismiss();
                            });
                        }
                    }
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }).start();
    }

    final private AaccountHandler.AccountListener mAccountListener = new AaccountHandler.AccountListener() {
        @Override
        public void onUserRegistered(AccountManager instance, boolean result, FNError error) {

        }

        @Override
        public void onUserLogined(AccountManager instance, boolean result, FNError error) {

        }

        @Override
        public void onGetUserInfo(AccountManager instance, boolean result, FNError error) {
            if (isAdded()) {
                int ret = mC2CProcess.sendRtpCommand(mLineID, "USERINFO", "0");
            }
        }

        @Override
        public void onUpdateUserInfo(AccountManager instance, boolean result, FNError error) {

        }

        @Override
        public void onUserLogout(AccountManager instance, boolean result, FNError error) {

        }

        @Override
        public void onResetPassword(AccountManager instance, boolean result, FNError error) {

        }

        @Override
        public void onSetPassword(AccountManager instance, boolean result, FNError error) {

        }
    };
}
