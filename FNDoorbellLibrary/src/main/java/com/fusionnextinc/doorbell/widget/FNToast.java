package com.fusionnextinc.doorbell.widget;

import android.content.Context;
import android.widget.Toast;

/**
 * Created by choco on 2015/1/29.
 */
public class FNToast {
    private static final String TAG = "FNToast";

    public static final int LENGTH_SHORT = Toast.LENGTH_SHORT;
    public static final int LENGTH_LONG = Toast.LENGTH_LONG;

    private static Toast toast;

    public static Toast makeText(Context context, CharSequence text, int duration) {
        if (toast == null) {
            toast = Toast.makeText(context, text, duration);
        } else {
            toast.setText(text);
        }
        return toast;
    }
}
