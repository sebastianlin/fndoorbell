package com.iptnet.common.c2c;

import com.iptnet.c2c.C2CHandle;
import com.iptnet.c2c.C2CHandle.ProtocolMessageCallback;


import android.annotation.SuppressLint;
import android.app.Notification;
import android.app.Service;
import android.content.Intent;
import android.content.pm.PackageManager.NameNotFoundException;
import android.os.Bundle;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.IBinder;
import android.os.Looper;
import android.os.Process;

import android.os.Message;
import android.os.PowerManager.WakeLock;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;
import android.util.Log;
import android.widget.Toast;

/**
 * The background service
 * @author HiKARi
 * @since 20160126A
 */
public class MainService extends Service {

	private static final String TAG = MainService.class.getSimpleName();
	
	public static final String ACTION_SERVICE_READY = "com.iptnet.jalacam.action.SERVICE_READY";
	
	private WakeLock mWakeLock;
	private C2CProcess mC2CProcess;
	private Notification.Builder mNotification;
	private C2CRegisterEvent mC2CRegisterEvent;
	private Thread mThdNotification;

	private Looper mServiceLooper;
	private ServiceHandler mServiceHandler;
	private final class ServiceHandler extends Handler {
		public ServiceHandler(Looper looper) {
			super(looper);
		}
		@Override
		public void handleMessage(Message msg) {
			// Normally we would do some work here, like download a file.
			// For our sample, we just sleep for 5 seconds.
			long endTime = System.currentTimeMillis() + 43200000;
			while (System.currentTimeMillis() < endTime) {
				synchronized (this) {
					try {
						wait(endTime - System.currentTimeMillis());
					} catch (Exception e) {
					}
				}
			}
			// Stop the service using the startId, so that we don't stop
			// the service in the middle of handling another job
			stopSelf(msg.arg1);
		}
	}


	public MainService() {
	}

	private class C2CRegisterEvent implements ProtocolMessageCallback {
		
		private Integer noticeLineId;
		private String noticePeerId;
		
		@Override
		public Object receiveProtocolMessage(int lineId, int message, int subEvent, Bundle bundle) {

			Log.i(TAG, "receiveProtocolMessage " + message);

			if (C2C_REGISTERING == message) {

				Log.i(TAG, "C2C_REGISTERING");

			} if (C2C_REGISTER_DONE == message) {

				Log.i(TAG, "C2C_REGISTER_DONE");

			} else if (C2C_REGISTER_FAIL == message) {

				String msg;
				if (C2C_SRV_NO_RESP == subEvent) {

					
				} else if (C2C_SRV_DISCONNECT == subEvent) {

					
				} else if (C2C_SVR_FAIL == subEvent) {

					
				} else if (C2C_FORBIDDEN == subEvent) {

					
				} else if (C2C_UNAUTHORIZED == subEvent) {

					
				} else if (C2C_INVALID_ACCOUNT == subEvent) {

					
				} else {

				}

			
			} else if (C2C_SETUP_DONE == message) {
				

			} else if (C2C_SETUP_ERROR == message) {
				noticePeerId = null;
				Log.d(TAG, "setting notification fail event="+subEvent+" peer="+bundle.getString("peerId"));
			}
			else if (C2C_RECV_ALERT == message)
			{
				

			}
			return null;
		}
	}
	
	@Override
	public IBinder onBind(Intent intent) {
		return null;
	}

	@SuppressLint("NewApi")
	@Override
	public void onCreate() {

		// get C2C module instance and initialize
		mC2CProcess = C2CProcess.getInstance();
		mC2CProcess.showDebugMessage(true);
		mC2CProcess.initialize();
		mC2CProcess.setFastConnection(true);
		mC2CProcess.addProtocolMessageCallback(mC2CRegisterEvent = new C2CRegisterEvent(), new int[] {
			C2CHandle.Message.C2C_REGISTERING, C2CHandle.Message.C2C_REGISTER_DONE, C2CHandle.Message.C2C_REGISTER_FAIL,
			C2CHandle.Message.C2C_SETUP_DONE, C2CHandle.Message.C2C_SETUP_ERROR,C2CHandle.Message.C2C_RECV_ALERT
		});	
		Log.e(TAG, "onCreate");

		// Start up the thread running the service.  Note that we create a
		// separate thread because the service normally runs in the process's
		// main thread, which we don't want to block.  We also make it
		// background priority so CPU-intensive work will not disrupt our UI.
		HandlerThread thread = new HandlerThread("ServiceStartArguments",
				Process.THREAD_PRIORITY_BACKGROUND);
		thread.start();

		// Get the HandlerThread's Looper and use it for our Handler
		mServiceLooper = thread.getLooper();
		mServiceHandler = new ServiceHandler(mServiceLooper);
	}

	@Override
	public int onStartCommand(Intent intent, int flags, int startId) {
 

		String srv = "barvotech.iptnet.net";
		
		// get build
		int build;
		try {
			build = getPackageManager().getPackageInfo(getPackageName(), 0).versionCode;
		} catch (NameNotFoundException e) {
			build = Integer.MIN_VALUE;
			Log.e(TAG, "get build fail");
		}

		// not found account and generate a new account
		String suffix = "mt7687";
		String domain = srv;
		String uid = mC2CProcess.generateNewRegisterUid(suffix, domain);
		String pwd = mC2CProcess.getRegisterPasswordByUid(uid);

		
		mC2CProcess.setRegisterData(srv, uid, pwd);
		mC2CProcess.register();

		// send broadcast
		LocalBroadcastManager.getInstance(this)
			.sendBroadcast(new Intent(ACTION_SERVICE_READY));


		Toast.makeText(this, "service starting", Toast.LENGTH_SHORT).show();

		// For each start request, send a message to start a job and deliver the
		// start ID so we know which request we're stopping when we finish the job
		Message msg = mServiceHandler.obtainMessage();
		msg.arg1 = startId;
		mServiceHandler.sendMessage(msg);

		return START_STICKY;
	}
	
	@Override
	public void onDestroy() {

		// release the C2C module resource
		mC2CProcess.removeProtocolMessageCallback(mC2CRegisterEvent);
		mC2CRegisterEvent = null;
		new Thread(new Runnable() {
			public void run() {
				if(mC2CProcess!=null)
					mC2CProcess.release();
				mC2CProcess = null;
		}}).start();
		
		// stop foreground service
		stopForeground(true);
		
		// stop keep CPU on
		if (mWakeLock != null) {
			mWakeLock.release();
			mWakeLock = null;
			Log.w(TAG, "WakeLock disabled");
		}


		Toast.makeText(this, "service done", Toast.LENGTH_SHORT).show();

		Log.e(TAG, "onDestroy");

	}
}
