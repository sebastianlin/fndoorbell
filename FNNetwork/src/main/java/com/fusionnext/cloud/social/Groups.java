package com.fusionnext.cloud.social;

import android.content.Context;

import com.fusionnext.cloud.datalink.http.HttpDelete;
import com.fusionnext.cloud.datalink.http.HttpGet;
import com.fusionnext.cloud.datalink.http.HttpParam;
import com.fusionnext.cloud.datalink.http.HttpPost;
import com.fusionnext.cloud.datalink.http.HttpPut;
import com.fusionnext.cloud.datalink.json.FNJsonArray;
import com.fusionnext.cloud.datalink.json.FNJsonObject;
import com.fusionnext.cloud.social.objects.Group;
import com.fusionnext.cloud.social.objects.Invitation;
import com.fusionnext.cloud.social.objects.Message;
import com.fusionnext.cloud.social.objects.Photo;
import com.fusionnext.cloud.social.objects.UserProfile;

import org.json.JSONException;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by Mike Chang on 2017/3/27
 */
public class Groups {
    protected final static String _TAG = "Groups";
    private Context context;

    public Groups(Context context) {
        this.context = context;
    }


    public void createGroup(Group group, final SocialAPIHandler.OnGroupsListener onGroupsListener) {
        if (group == null) {
            return;
        }
        HttpPost.PostParam[] params = new HttpPost.PostParam[1];
        params[0] = new HttpPost.PostParam();
        params[0].accessToken = Public.getAuthorization(context).getUserAccessToken();
        params[0].url = HttpData.generateHttpPostUrl(HttpData._POST_TYPE_GROUPS_CREATE_GROUP);
        params[0].type = HttpData._POST_TYPE_GROUPS_CREATE_GROUP;
        params[0].param = new HashMap<Object, Object>();
        params[0].param.put("title", group._title);
        params[0].param.put("description", group._description);
        params[0].param.put("destinations", group._destinations);
        params[0].param.put("privacy", group._privacy);
        params[0].param.put("expired_at", group._expired_at);

        HttpPost post = new HttpPost();
        post.setContext(context);
        post.setOnParseJsonListener(new HttpPost.OnParseJsonListener() {
            @Override
            public void onParse(boolean returnResult, int postType, String id, String returnStr) {
                onGroupsListener.onCreateGroup(returnResult, Group.parseGroup(returnStr));
            }
        });
        post.execute(params);
    }

    public void updateGroup(final String groupId, Group group, final SocialAPIHandler.OnGroupsListener onGroupsListener) {
        if (group == null) {
            return;
        }

        HttpPut.PutParam[] params = new HttpPut.PutParam[1];
        params[0] = new HttpPut.PutParam();
        params[0].accessToken = Public.getAuthorization(context).getUserAccessToken();
        params[0].url = HttpData.generateHttpPutUrl(HttpData._PUT_TYPE_GROUPS_UPDATE_GROUP, groupId);
        params[0].type = HttpData._PUT_TYPE_GROUPS_UPDATE_GROUP;
        params[0].param = new HashMap<Object, Object>();
        params[0].param.put("title", group._title);
        params[0].param.put("description", group._description);
        params[0].param.put("destinations", group._destinations);
        params[0].param.put("privacy", group._privacy);
        params[0].param.put("expired_at", group._expired_at);

        HttpPut put = new HttpPut();
        put.setHttpRequestType("PUT");
        put.setOnParseJsonListener(new HttpPut.OnParseJsonListener() {
            @Override
            public void onParse(boolean returnResult, int putType, String returnStr) {
                onGroupsListener.onResult(returnResult, groupId, putType, returnStr);
            }
        });
        put.setContext(context);
        put.execute(params);
    }

    public void getGroup(final String groupId, final SocialAPIHandler.OnGroupsListener onGroupsListener) {
        HttpParam.HttpSubParam param = new HttpParam.HttpSubParam();

        param.accessToken = Public.getAuthorization(context).getUserAccessToken();
        param.url = HttpData.generateHttpGetUrl(HttpData._GET_TYPE_GROUPS_GET_GROUP, groupId, 0, null, null);
        param.cacheName = "";
        HttpParam[] params = new HttpParam[1];
        params[0] = new HttpParam();
        params[0].params.clear();
        params[0].params.add(param);

        HttpGet httpGet = new HttpGet();
        httpGet.setContext(context);
        httpGet.setTag(HttpGet._HTTP_T_GET_NORMAL_TYPE);
        httpGet.setOnParseJsonListener(new HttpGet.OnParseJsonListener() {
            @Override
            public void onParse(boolean result, String returnStr) {
                onGroupsListener.onGetGroup(result, groupId, Group.parseGroup(returnStr));
            }
        });
        httpGet.execute(params);
    }

    public void inviteIntoGroup(final String groupId, String[] userId, final SocialAPIHandler.OnGroupsListener onGroupsListener) {
        String user = "";
        HttpPost.PostParam[] params = new HttpPost.PostParam[1];
        params[0] = new HttpPost.PostParam();
        params[0].accessToken = Public.getAuthorization(context).getUserAccessToken();
        params[0].url = HttpData.generateHttpPostUrl(HttpData._POST_TYPE_GROUPS_INVITE_INTO_GROUP, groupId);
        params[0].type = HttpData._POST_TYPE_GROUPS_INVITE_INTO_GROUP;
        params[0].param = new HashMap<Object, Object>();
        if (userId != null) {
            for (int i = 0; i < userId.length; i++) {
                user = user + userId[i];
                if (i + 1 == userId.length) {
                    break;
                }
                user = user + ",";
            }
        }
        params[0].param.put("user_id", user);

        HttpPost post = new HttpPost();
        post.setContext(context);
        post.setOnParseJsonListener(new HttpPost.OnParseJsonListener() {
            @Override
            public void onParse(boolean returnResult, int postType, String id, String returnStr) {
                onGroupsListener.onResult(returnResult, groupId, postType, returnStr);
            }
        });
        post.execute(params);
    }

    public void acceptInvitation(final String groupId, final SocialAPIHandler.OnGroupsListener onGroupsListener) {
        HttpPut.PutParam[] params = new HttpPut.PutParam[1];
        params[0] = new HttpPut.PutParam();
        params[0].accessToken = Public.getAuthorization(context).getUserAccessToken();
        params[0].url = HttpData.generateHttpPutUrl(HttpData._PUT_TYPE_GROUPS_UPDATE_GROUP, groupId);
        params[0].type = HttpData._PUT_TYPE_GROUPS_UPDATE_GROUP;

        HttpPut put = new HttpPut();
        put.setHttpRequestType("PUT");
        put.setOnParseJsonListener(new HttpPut.OnParseJsonListener() {
            @Override
            public void onParse(boolean returnResult, int putType, String returnStr) {
                onGroupsListener.onResult(returnResult, groupId, putType, returnStr);
            }
        });
        put.setContext(context);
        put.execute(params);
    }

    public void deleteInvitation(final String groupId, String[] userId, final SocialAPIHandler.OnGroupsListener onGroupsListener) {
        String user = "";
        HttpDelete.DelParam[] params = new HttpDelete.DelParam[1];
        params[0] = new HttpDelete.DelParam();
        params[0].accessToken = Public.getAuthorization(context).getUserAccessToken();
        params[0].url = HttpData.generateHttpDeleteUrl(HttpData._DEL_TYPE_GROUPS_DELETE_INVITATION, groupId);
        params[0].type = HttpData._DEL_TYPE_GROUPS_DELETE_INVITATION;
        params[0].param = new HashMap<Object, Object>();
        if (userId != null) {
            for (int i = 0; i < userId.length; i++) {
                user = user + userId[i];
                if (i + 1 == userId.length) {
                    break;
                }
                user = user + ",";
            }
        }
        params[0].param.put("user_id", user);
        HttpDelete del = new HttpDelete();
        del.setContext(context);
        del.setOnParseJsonListener(new HttpDelete.OnParseJsonListener() {
            @Override
            public void onParse(boolean returnResult, int delType, String returnStr) {
                if (returnStr != null) {
                    onGroupsListener.onResult(returnResult, groupId, delType, returnStr);
                }
            }
        });
        del.execute(params);
    }

    public void leaveGroup(final String groupId, String[] userId, final SocialAPIHandler.OnGroupsListener onGroupsListener) {
        String user = "";
        HttpDelete.DelParam[] params = new HttpDelete.DelParam[1];
        params[0] = new HttpDelete.DelParam();
        params[0].accessToken = Public.getAuthorization(context).getUserAccessToken();
        params[0].url = HttpData.generateHttpDeleteUrl(HttpData._DEL_TYPE_GROUPS_LEAVE_GROUP, groupId);
        params[0].type = HttpData._DEL_TYPE_GROUPS_LEAVE_GROUP;
        params[0].param = new HashMap<Object, Object>();
        if (userId != null) {
            for (int i = 0; i < userId.length; i++) {
                user = user + userId[i];
                if (i + 1 == userId.length) {
                    break;
                }
                user = user + ",";
            }
        }
        params[0].param.put("user_id", user);
        HttpDelete del = new HttpDelete();
        del.setContext(context);
        del.setOnParseJsonListener(new HttpDelete.OnParseJsonListener() {
            @Override
            public void onParse(boolean returnResult, int delType, String returnStr) {
                if (returnStr != null) {
                    onGroupsListener.onResult(returnResult, groupId, delType, returnStr);
                }
            }
        });
        del.execute(params);
    }

    public void listInvitations(final String groupId, int order, String pageSize, String pageNum, final SocialAPIHandler.OnGroupsListener onGroupsListener) {
        HttpParam.HttpSubParam param = new HttpParam.HttpSubParam();

        param.accessToken = Public.getAuthorization(context).getUserAccessToken();
        param.url = HttpData.generateHttpGetUrl(HttpData._GET_TYPE_GROUPS_LIST_INVITATIONS, groupId, order, pageSize, pageNum);
        param.cacheName = "";
        HttpParam[] params = new HttpParam[1];
        params[0] = new HttpParam();
        params[0].params.clear();
        params[0].params.add(param);

        HttpGet httpGet = new HttpGet();
        httpGet.setContext(context);
        httpGet.setTag(HttpGet._HTTP_T_GET_NORMAL_TYPE);
        httpGet.setOnParseJsonListener(new HttpGet.OnParseJsonListener() {
            @Override
            public void onParse(boolean result, String returnStr) {
                onGroupsListener.onListGroupInvitations(result, groupId, Invitation.parseInvitationArray(returnStr));
            }
        });
        httpGet.execute(params);
    }

    public void listPhotos(final String groupId, int order, String pageSize, String pageNum, final SocialAPIHandler.OnGroupsListener onGroupsListener) {
        HttpParam.HttpSubParam param = new HttpParam.HttpSubParam();

        param.accessToken = Public.getAuthorization(context).getUserAccessToken();
        param.url = HttpData.generateHttpGetUrl(HttpData._GET_TYPE_GROUPS_LIST_PHOTOS, groupId, order, pageSize, pageNum);
        param.cacheName = "";
        HttpParam[] params = new HttpParam[1];
        params[0] = new HttpParam();
        params[0].params.clear();
        params[0].params.add(param);

        HttpGet httpGet = new HttpGet();
        httpGet.setContext(context);
        httpGet.setTag(HttpGet._HTTP_T_GET_NORMAL_TYPE);
        httpGet.setOnParseJsonListener(new HttpGet.OnParseJsonListener() {
            @Override
            public void onParse(boolean result, String returnStr) {
                onGroupsListener.onListGroupPhotos(result, groupId, Photo.parsePhotoArray(returnStr));
            }
        });
        httpGet.execute(params);
    }

    public void listMembers(final String groupId, int order, String pageSize, String pageNum, final SocialAPIHandler.OnGroupsListener onGroupsListener) {
        HttpParam.HttpSubParam param = new HttpParam.HttpSubParam();

        param.accessToken = Public.getAuthorization(context).getUserAccessToken();
        param.url = HttpData.generateHttpGetUrl(HttpData._GET_TYPE_GROUPS_LIST_MEMBERS, groupId, order, pageSize, pageNum);
        param.cacheName = "";
        HttpParam[] params = new HttpParam[1];
        params[0] = new HttpParam();
        params[0].params.clear();
        params[0].params.add(param);

        HttpGet httpGet = new HttpGet();
        httpGet.setContext(context);
        httpGet.setTag(HttpGet._HTTP_T_GET_NORMAL_TYPE);
        httpGet.setOnParseJsonListener(new HttpGet.OnParseJsonListener() {
            @Override
            public void onParse(boolean result, String returnStr) {
                onGroupsListener.onListGroupMembers(result, groupId, parseMemberArray(returnStr));
            }
        });
        httpGet.execute(params);
    }

    public void listMessages(final String groupId, int order, String pageSize, String pageNum, final SocialAPIHandler.OnGroupsListener onGroupsListener) {
        HttpParam.HttpSubParam param = new HttpParam.HttpSubParam();

        param.accessToken = Public.getAuthorization(context).getUserAccessToken();
        param.url = HttpData.generateHttpGetUrl(HttpData._GET_TYPE_GROUPS_LIST_MESSAGES, groupId, order, pageSize, pageNum);
        param.cacheName = "";
        HttpParam[] params = new HttpParam[1];
        params[0] = new HttpParam();
        params[0].params.clear();
        params[0].params.add(param);

        HttpGet httpGet = new HttpGet();
        httpGet.setContext(context);
        httpGet.setTag(HttpGet._HTTP_T_GET_NORMAL_TYPE);
        httpGet.setOnParseJsonListener(new HttpGet.OnParseJsonListener() {
            @Override
            public void onParse(boolean result, String returnStr) {
                onGroupsListener.onListGroupMessages(result, groupId, Message.parseMessageArray(returnStr));
            }
        });
        httpGet.execute(params);
    }


    private ArrayList<UserProfile> parseMemberArray(String json) {
        try {
            ArrayList<UserProfile> memberList = new ArrayList<UserProfile>();
            FNJsonObject memberObject = new FNJsonObject(json);
            FNJsonArray memberArray = memberObject.getJSONArray("items");
            if (memberArray == null) {
                return null;
            }
            for (int i = 0; i < memberArray.length(); i++) {
                FNJsonObject object = memberArray.getJSONObject(i);
                UserProfile member = new UserProfile();
                member.getDataFromJSONObject(object);
                if (member == null) return null;
                memberList.add(member);
            }
            return memberList;
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return null;
    }
}
