package com.fusionnext.cloud.iot;

import android.content.Context;

import com.fusionnext.cloud.datalink.http.HttpPost;
import com.fusionnext.cloud.iot.objects.ErrorObj;
import com.fusionnext.cloud.iot.objects.Setting;

import java.util.HashMap;

/**
 * Created by Mike Chang on 2017/6/12
 */
public class Settings {
    protected final static String _TAG = "Settings";
    private Context context;

    public Settings(Context context) {
        this.context = context;
    }

    public void createSetting(final Setting setting, final String settingType, final IOTHandler.OnSettingsListener onSettingsListener) {
        if (setting == null) {
            return;
        }
        HttpPost.PostParam[] params = new HttpPost.PostParam[1];
        params[0] = new HttpPost.PostParam();
        params[0].accessToken = Public.getAuthorization(context).getUserAccessToken();
        params[0].url = HttpData.generateHttpPostUrl(HttpData._POST_TYPE_THINGS_CREATE_SETTINGS);
        params[0].type = HttpData._POST_TYPE_THINGS_CREATE_SETTINGS;
        params[0].param = new HashMap<>();
        params[0].param.put("thing_id", setting._thingId);
        params[0].param.put("name", setting._name);
        params[0].param.put("value", setting._value);

        HttpPost post = new HttpPost();
        post.setContext(context);
        post.setOnParseJsonListener(new HttpPost.OnParseJsonListener() {
            @Override
            public void onParse(boolean returnResult, int postType, String id, String returnStr) {
                Setting setting = null;
                ErrorObj errorObj = null;

                if (returnStr != null) {
                    if (returnResult) {
                        setting = Setting.parseSetting(returnStr);
                    } else {
                        errorObj = ErrorObj.parseErrorObj(returnStr);
                    }
                }
                onSettingsListener.onCreateSetting(returnResult, settingType, setting, errorObj);
            }
        });
        post.execute(params);
    }
}
