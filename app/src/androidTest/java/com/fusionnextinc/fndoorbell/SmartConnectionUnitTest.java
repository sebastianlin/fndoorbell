package com.fusionnextinc.fndoorbell;

import android.app.Application;
import android.content.Context;
import android.test.ApplicationTestCase;

import com.fusionnextinc.doorbell.manager.smartConnection.SmartConnectionHandler;
import com.fusionnextinc.doorbell.manager.smartConnection.SmartConnectionManager;
import com.fusionnextinc.doorbell.objects.FNThing;
import com.fusionnextinc.doorbell.utils.FNLog;

import java.util.concurrent.CountDownLatch;


/**
 * To work on unit tests, switch the Test Artifact in the Build Variants view.
 */
public class SmartConnectionUnitTest extends ApplicationTestCase<Application> {
    protected final static String _TAG = "SmartConnectionUnitTest";
    Context context;
    private CountDownLatch latch;

    public SmartConnectionUnitTest() {
        super(Application.class);
    }

    public void setUp() throws Exception {
        super.setUp();
        context = getTestContext();
        assertNotNull(context);
        SmartConnectionManager.getInstance().setSmartConnectionListener(smartConnectionListener);
    }

    public void testStartSmartConnection() {
        latch = new CountDownLatch(1); //創建CountDownLatch
        SmartConnectionManager.getInstance().start("FusionNextInc", "0287718518", null, 30000, 0);
        try {
            latch.await();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }


    protected void tearDown() throws Exception {
        //super.tearDown();
    }

    /**
     * @return The {@link Context} of the test project.
     */
    private Context getTestContext() {
        try {
            return (Context) InstrumentationRegistry.getTargetContext();
        } catch (final Exception exception) {
            exception.printStackTrace();
            return null;
        }
    }

    final SmartConnectionHandler.SmartConnectionListener  smartConnectionListener = new SmartConnectionHandler.SmartConnectionListener() {
        @Override
        public void onThingFound(SmartConnectionManager instance, FNThing fnThing) {
            SmartConnectionManager.getInstance().stop();
            latch.countDown();
            assertNotNull(fnThing);
            FNLog.d(_TAG, "FN Thing = " + fnThing.getName());
        }

        @Override
        public void onThingNotFound(SmartConnectionManager instance) {
            latch.countDown();
        }

        @Override
        public void onThingOtaStart(SmartConnectionManager instance, FNThing fnThing) {

        }

    };
}