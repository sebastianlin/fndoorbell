package com.fusionnext.cloud.iot.objects;

import android.util.Log;

import com.fusionnext.cloud.datalink.json.FNJsonArray;
import com.fusionnext.cloud.datalink.json.FNJsonObject;
import com.fusionnext.cloud.iot.Public;
import com.fusionnext.cloud.utils.Times;

import org.json.JSONException;

import java.util.ArrayList;
import java.util.TimeZone;

/**
 * Created by Mike Chang on 2016/2/22.
 */
public class UserProfile {

    protected final static String _JSON_TAG = "UserProfile";
    public UserProfile onUsersListener;

    public UserProfile() {
        _userId = "";
        _organizationId = "";
        _socialProvider = "";
        _socialId = "";
        _name = "";
        _email = "";
        _avatar = "";
        _birthday = "";
        _gender = "";
        _locale = "";
        _createdAt = "";
        _updatedAt = "";
    }

    public String _userId; // user id
    public String _organizationId; // user id
    public String _socialProvider; // user's social_provider
    public String _socialId; // user's social_id
    public String _email; // user's name
    public String _name; // user's name
    public String _avatar; // user's photo
    public String _birthday;  // user's gender
    public String _gender;  // user's gender
    public String _locale; // user's locale
    public String _createdAt;  // user's created_at
    public String _updatedAt; // user's updated_ato


    public void clean() {
        _userId = "";
        _organizationId = "";
        _socialProvider = "";
        _socialId = "";
        _name = "";
        _email = "";
        _avatar = "";
        _birthday = "";
        _gender = "";
        _locale = "";
        _createdAt = "";
        _updatedAt = "";
    }

    public boolean getDataFromJSONObject(FNJsonObject userObj) {
        clean();
        if (userObj == null) {
            Log.w(_JSON_TAG, " No JSON information");
            return false;
        }
        try {
            _userId = userObj.getStringDefault("id");
            _organizationId = userObj.getStringDefault("organization_id");
            _socialProvider = userObj.getStringDefault("social_provider");
            _socialId = userObj.getStringDefault("social_id");
            _name = userObj.getStringDefault("name");
            _email = userObj.getStringDefault("email");
            FNJsonObject avaterObj = userObj.getJSONObject("avatar");
            if (avaterObj != null) {
                _avatar = Public._HOST_V1() + avaterObj.getStringDefault("href");
            }

            _birthday = userObj.getStringDefault("birthday");
            _gender = userObj.getStringDefault("gender");
            _locale = userObj.getStringDefault("locale");
            _createdAt = userObj.getStringDefault("created_at");
            _createdAt = Times.converTime(_createdAt, TimeZone.getDefault());
            _updatedAt = userObj.getStringDefault("updated_at");
            _updatedAt = Times.converTime(_updatedAt, TimeZone.getDefault());
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
        return true;
    }

    public static ArrayList<UserProfile> parseUserProfileArray(String json) {
        String avatar;

        try {
            ArrayList<UserProfile> userList = new ArrayList<UserProfile>();
            FNJsonObject userObject = new FNJsonObject(json);
            FNJsonArray usersArray = userObject.getJSONArray("items");
            if (usersArray == null) {
                return null;
            }
            for (int i = 0; i < usersArray.length(); i++) {
                FNJsonObject object = usersArray.getJSONObject(i);
                UserProfile user = new UserProfile();
                user.getDataFromJSONObject(object);
                if (user == null) return null;
                userList.add(user);
            }
            return userList;
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return null;
    }

}
