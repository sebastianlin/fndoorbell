package com.fusionnextinc.doorbell;

import android.annotation.SuppressLint;
import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.media.AudioAttributes;
import android.media.AudioManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.IBinder;
import android.widget.RemoteViews;

import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.core.app.NotificationCompat;

import com.fusionnextinc.doorbell.utils.BooleanLock;
import com.fusionnextinc.doorbell.utils.FNLog;

import java.util.concurrent.atomic.AtomicBoolean;

public class HeadsUpNotificationService extends Service {

    public static HeadsUpNotificationService sInstance = null;
    NotificationManager manager = null;
    private int mLastId = 0;
    @Override
    public void onCreate() {
        super.onCreate();
        sInstance = this;
        manager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

    }
    public  void stopOnce() {




        manager.cancel(HeadsUpNotificationService.NOTIFY_ID);
        manager.cancel(HeadsUpNotificationService.SERVICE_NOTIFY_ID);
        manager.cancelAll();

        stopSelf(mLastId);
    }

    public static String CHANNEL_ID = "VoipChannel";
    public static String CHANNEL_NAME = "Voip Channel";

    public static String ACTION_STOP_SERVICE = "HeadsUp_STOP_SERVICE";
    public static   Integer NOTIFY_ID = 0;
    public static   Integer SERVICE_NOTIFY_ID = 120;

    public  static  AtomicBoolean s_flag = new AtomicBoolean(false);


    static public   boolean  getFlage()
    {
        if( s_flag.get() )
        {
            return true;
        }
        return false;
    }


    static public   void  cleanFlage()
    {
        s_flag.set(false);
    }

    static public   void  setFlage()
    {

        s_flag.set(true);
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    @SuppressLint("WrongConstant")
    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {


        //NotificationManager manager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        Intent openIntent = null;
        mLastId = startId;
        Bundle data = null;
        openIntent = new Intent(getApplicationContext(), MainActivity.class);
        openIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        openIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        openIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
        openIntent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
        //openIntent.putExtras(data);//將Bundle物件傳給intent


        if (intent != null && intent.getExtras() != null) {
            //data = intent.getBundleExtra(MyApplication.FCM_DATA_KEY);
            data = intent.getExtras();
        }
        try {
            Intent receiveCallAction = new Intent(getApplicationContext(), HeadsUpNotificationActionReceiver.class);
            receiveCallAction.putExtra(MyApplication.CALL_RESPONSE_ACTION_KEY, MyApplication.CALL_RECEIVE_ACTION);
            receiveCallAction.putExtra(MyApplication.FCM_DATA_KEY, data);
            receiveCallAction.putExtra(MyApplication.CALL_END_ACTION, "action");
            receiveCallAction.setAction("RECEIVE_CALL");

            Intent cancelCallAction = new Intent(getApplicationContext(), HeadsUpNotificationActionReceiver.class);
            cancelCallAction.putExtra(MyApplication.CALL_RESPONSE_ACTION_KEY, MyApplication.CALL_CANCEL_ACTION);
            cancelCallAction.putExtra(MyApplication.FCM_DATA_KEY, data);
            cancelCallAction.putExtra(MyApplication.CALL_END_ACTION, "cancel");
            cancelCallAction.setAction("CANCEL_CALL");


            Intent stopSelf = new Intent(this, HeadsUpNotificationService.class);
            stopSelf.setAction(this.ACTION_STOP_SERVICE);
            PendingIntent pStopSelf = PendingIntent.getService(this, 0, stopSelf,PendingIntent.FLAG_CANCEL_CURRENT);


            PendingIntent receiveCallPendingIntent = PendingIntent.getBroadcast(getApplicationContext(), 1200, receiveCallAction, PendingIntent.FLAG_UPDATE_CURRENT);
            PendingIntent cancelCallPendingIntent = PendingIntent.getBroadcast(getApplicationContext(), 1201, cancelCallAction, PendingIntent.FLAG_UPDATE_CURRENT);

            /*
                    NotificationChannel Channel=manager.getNotificationChannel(CHANNEL_ID);
                    while( (Channel=manager.getNotificationChannel(CHANNEL_ID)) != null)
                    {
                        manager.deleteNotificationChannel(CHANNEL_ID);
                        //manager.cancelAll();
                    }
             */

            createChannel();
            NotificationCompat.Builder notificationBuilder = null;
            RemoteViews notificationTitleLayout = new RemoteViews(getApplicationContext().getPackageName(),R.layout.heads_title_notification);
            RemoteViews notificationLayout = new RemoteViews(getApplicationContext().getPackageName(),R.layout.heads_up_notification);


            notificationTitleLayout.setOnClickPendingIntent(R.id.button_accept,receiveCallPendingIntent);
            notificationTitleLayout.setOnClickPendingIntent(R.id.button_decline,cancelCallPendingIntent);

            notificationLayout.setOnClickPendingIntent(R.id.button_accept,receiveCallPendingIntent);
            notificationLayout.setOnClickPendingIntent(R.id.button_decline,cancelCallPendingIntent);
            notificationLayout.setImageViewResource(R.id.icon_msg,R.drawable.call_logo);



            notificationTitleLayout.setOnClickPendingIntent(R.id.icon_msg,cancelCallPendingIntent);
            notificationTitleLayout.setOnClickPendingIntent(R.id.message_title,cancelCallPendingIntent);
            notificationTitleLayout.setOnClickPendingIntent(R.id.message_title,cancelCallPendingIntent);

            NotificationCompat.Builder notificationBuilder1 = null;

            if (data != null) {

                String type = data.getString("type");
                String thingId = data.getString(MyFirebaseMessagingService.THING_ID);
                String callSlotId = data.getString(MyFirebaseMessagingService.CALL_SLOT_ID);
                String thingName = data.getString(MyFirebaseMessagingService.THING_NAME);
                String sound = data.getString(MyFirebaseMessagingService.SOUND);
                String callname = data.getString(MyFirebaseMessagingService.CALLNAME_ID);
                String number = data.getString(MyFirebaseMessagingService.CALL_NUMBER_ID);

                if( callname == null || callname.length() == 0 ) {
                    callname = "語言通話來電中";
                }
                //notificationLayout.
                notificationLayout.setTextViewText(R.id.message_title,thingName);
                notificationLayout.setTextViewText(R.id.message_msg,callname);

                Uri defaultSoundUri = Uri.parse(ContentResolver.SCHEME_ANDROID_RESOURCE + "://"+ getApplicationContext().getPackageName() + "/" + R.raw.dream);

                FNLog.d("createChannel", "ring:" + defaultSoundUri.toString());
                //Notification.FLAG_INSISTENT
                notificationBuilder = new NotificationCompat.Builder(this, CHANNEL_ID)
                        .setDefaults(Notification.DEFAULT_ALL)
                        .setContentTitle(getString(R.string.fn_btn_someone_knock).replace("${NAME}", getString(R.string.fn_title_thing_name)))
                        .setSmallIcon(R.drawable.ic_logo)
                        .setSettingsText(thingName)
                        .setCustomHeadsUpContentView(notificationLayout)
                        .setPriority(NotificationCompat.PRIORITY_MAX)
                        .setCategory(NotificationCompat.CATEGORY_CALL)
                        .setVisibility(Notification.VISIBILITY_PUBLIC)
                        .setContentIntent(receiveCallPendingIntent)
                        .setAutoCancel(true)
                        .setOngoing(false)
                        .setWhen(System.currentTimeMillis())
                        .setSound(defaultSoundUri)
                        .setDeleteIntent(cancelCallPendingIntent)
                        .setFullScreenIntent(receiveCallPendingIntent, true);



                notificationBuilder1 = new NotificationCompat.Builder(this, CHANNEL_ID)
                        .setContentTitle(getString(R.string.fn_btn_someone_knock).replace("${NAME}", getString(R.string.fn_title_thing_name)))
                        .setSettingsText(thingName)
                        .setPriority(NotificationCompat.PRIORITY_MAX)
                        .setCategory(NotificationCompat.CATEGORY_CALL)
                        .setVisibility(Notification.VISIBILITY_PUBLIC)
                        .setContentIntent(receiveCallPendingIntent)
                        .setSmallIcon(R.drawable.ic_logo)
                        .setCustomHeadsUpContentView(notificationTitleLayout)
                        .setAutoCancel(true)
                        .setOngoing(false)
                        //.setSound(defaultSoundUri)
                        .setWhen(System.currentTimeMillis())
                        .setDeleteIntent(cancelCallPendingIntent);
//                        .setSound(defaultSoundUri)
                //                       .setFullScreenIntent(receiveCallPendingIntent, true);
            }
            Notification incomingCallNotification = null;
            if (notificationBuilder != null) {
                incomingCallNotification = notificationBuilder.build();
            }
            //NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);


            incomingCallNotification = notificationBuilder.build();
            incomingCallNotification.flags |= Notification.FLAG_INSISTENT;
            manager.notify("VOIPCALL",NOTIFY_ID,incomingCallNotification);
            startForeground(SERVICE_NOTIFY_ID,notificationBuilder1.build());

            //Thread.sleep(500);

            //Thread.sleep(500);

                    //incomingCallNotification);
        } catch (Exception e) {
            e.printStackTrace();
        }
        s_flag.set(true);
        return START_STICKY;
    }
    @RequiresApi(api = Build.VERSION_CODES.O)
    @Override
    public void onDestroy() {
        System.out.println("MyService onDestroy()：Called by the system to notify a Service that it is no longer used and is being removed. ");
        manager.cancelAll();
        stopSelf();

        s_flag.set(false);
        super.onDestroy();
    }

    @Override
    public void onTaskRemoved(Intent rootIntent) {

        super.onTaskRemoved(rootIntent);
    }
    /*
    Create noticiation channel if OS version is greater than or eqaul to Oreo
    */
    @SuppressLint("WrongConstant")
    public void createChannel() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {

            NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
//            Uri defaultSoundUri = Uri.parse(ContentResolver.SCHEME_ANDROID_RESOURCE + "://"+ getApplicationContext().getPackageName() + "/" + R.raw.dream);
            Uri defaultSoundUri = Uri.parse(ContentResolver.SCHEME_ANDROID_RESOURCE + "://"+ getApplicationContext().getPackageName() + "/" + R.raw.dream);

            FNLog.d("createChannel", "ring:" + defaultSoundUri.toString());
            NotificationChannel channel = new NotificationChannel(CHANNEL_ID, CHANNEL_NAME, NotificationManager.IMPORTANCE_HIGH);


                    //cancel(notificationManager.getNotificationChannel(CHANNEL_ID));
            //NotificationChannel old_channel=notificationManager.getNotificationChannel(CHANNEL_ID);
            channel.setBypassDnd(true);
            channel.setShowBadge(true);
            channel.setImportance(NotificationManager.IMPORTANCE_MAX);
            channel.setLockscreenVisibility(Notification.VISIBILITY_PUBLIC);
            channel.setDescription("Call Notifications");

            channel.setSound(defaultSoundUri,
                    new AudioAttributes.Builder().setContentType(AudioAttributes.CONTENT_TYPE_SONIFICATION)
                            .setLegacyStreamType(AudioManager.STREAM_RING)
                            .setUsage(AudioAttributes.USAGE_NOTIFICATION).build());

            notificationManager.createNotificationChannel(channel);



        }
    }
}


