package com.fusionnext.cloud.iot.objects;

public enum THINGSTATUS {
    EMPTY,
    OFFLINE,
    SLEEP,
    ONLINE,
    BUSY;

    public int getScore() {
        switch (this) {
            case OFFLINE:
                return 0;
            case SLEEP:
                return 1;
            case ONLINE:
                return 2;
            case BUSY:
                return 3;

        }
        return -1;
    }
    static   public THINGSTATUS setScore( int score )
    {

        switch (score) {
            case 0:
                return  THINGSTATUS.OFFLINE;

            case 1:
                return  THINGSTATUS.SLEEP;

            case 2:
                return  THINGSTATUS.ONLINE;

            case 3:
                return  THINGSTATUS.BUSY;



        }
        return  THINGSTATUS.EMPTY;
    }


    @Override
    public String toString() {
        switch (this) {
            case OFFLINE:
                return "offline: " + 0;
            case SLEEP:
                return "sleep: " + 1;
            case ONLINE:
                return "online: " + 2;
            case BUSY:
                return "busy: " + 3;
            default:
                return "defaul: " + -1;
        }
    }
}
