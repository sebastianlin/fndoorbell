package com.fusionnext.cloud.social.objects;

import android.util.Log;

import com.fusionnext.cloud.datalink.json.FNJsonObject;

/**
 * Created by Mike Chang on 2016/2/22.
 */
public class CommentsMeta {

    protected final static String _TAG = "Commentsmeta";

    public CommentsMeta() {
        _count = 0;
        _commented = 0;
    }

    public int _count;
    public int _commented;

    public void clean() {
        _count = 0;
        _commented = 0;
    }

    public boolean getDataFromJSONObject(FNJsonObject commentsMetaObj) {
        clean();
        if (commentsMetaObj == null) {
            Log.w(_TAG, " No JSON information");
            return false;
        }
        try {
            _count = commentsMetaObj.getIntDefault("count");
            _commented = commentsMetaObj.getBooleanDefault("is_commented") ? 1 : 0;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
        return true;
    }

}
