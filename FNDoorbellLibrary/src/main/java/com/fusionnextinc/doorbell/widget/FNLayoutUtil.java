package com.fusionnextinc.doorbell.widget;

import android.app.Activity;
import android.util.DisplayMetrics;
import android.util.TypedValue;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

/**
 * Created by choco on 2015/3/3.
 */
public class FNLayoutUtil {
    private static final String TAG = "FNLayoutUtil";
    public static final int BASE_WIDTH = 0;
    public static final int BASE_HEIGHT = 1;

    private Activity activity;
    private double proWidth, proHeight;
    private int baseMode;

    /**
     * 將元件與螢幕寬或高進行等比縮放, 元件須以px為單位
     *
     * @param activity             元件當前頁面之Activity
     * @param layout_width_pixels  Layout直版寬度
     * @param layout_height_pixels Layout直版高度
     * @param baseMode             FNLayoutUtil.BASE_WIDTH 以寬度比進行縮放, FNLayoutUtil.BASE_HEIGHT 以高度比進行縮放
     */
    public FNLayoutUtil(Activity activity, int layout_width_pixels, int layout_height_pixels, int baseMode) {
        this.activity = activity;
        this.baseMode = baseMode;
        DisplayMetrics metrics = new DisplayMetrics();
        activity.getWindowManager().getDefaultDisplay().getMetrics(metrics);
        if (metrics.widthPixels < metrics.heightPixels) {
            proWidth = metrics.widthPixels / (double) layout_width_pixels;
            proHeight = metrics.heightPixels / (double) layout_height_pixels;
        } else {
            proWidth = metrics.heightPixels / (double) layout_width_pixels;
            proHeight = metrics.widthPixels / (double) layout_height_pixels;
        }
    }

    /**
     * 透過此方法取得元件
     */
    public View findViewById(int id) {
        View view = activity.findViewById(id);
        return fitView(view);
    }

    /**
     * 透過此方法取得Inflater元件
     */
    public View findViewById(View child, int id) {
        View view = child.findViewById(id);
        return fitView(view);
    }

    /**
     * 透過此方法直接縮放view底下所有元件
     */
    public void fitAllView(View view) {
        if (view instanceof ViewGroup) {
            ViewGroup viewGroup = (ViewGroup) view;
            fitView(viewGroup);
            for (int i = 0; i < viewGroup.getChildCount(); i++) {
                fitAllView(viewGroup.getChildAt(i));
            }
        } else {
            fitView(view);
        }
    }

    /**
     * 透過此方法直接縮放元件
     */
    private View fitView(View view) {
        if (baseMode == BASE_WIDTH) {
            ViewGroup.MarginLayoutParams params = null;
            try {
                params = (ViewGroup.MarginLayoutParams) view.getLayoutParams();
            } catch (Exception e) {
            }
            if (params != null) {
                if (params.width > 0) {
                    params.width = limitMin(params.width * proWidth);
                }
                if (params.height > 0) {
                    params.height = limitMin(params.height * proWidth);
                }
                params.topMargin = limitMin(params.topMargin * proWidth);
                params.bottomMargin = limitMin(params.bottomMargin * proWidth);
                params.leftMargin = limitMin(params.leftMargin * proWidth);
                params.rightMargin = limitMin(params.rightMargin * proWidth);
                view.setLayoutParams(params);
            }
            view.setTranslationX(limitMin(view.getTranslationX() * proWidth));
            view.setTranslationY(limitMin(view.getTranslationY() * proWidth));
//            view.setMinimumHeight(limitMin(view.getMinimumHeight() * proWidth));
//            view.setMinimumWidth(limitMin(view.getMinimumWidth() * proWidth));
            view.setPadding(limitMin(view.getPaddingLeft() * proWidth), limitMin(view.getPaddingTop() * proWidth), limitMin(view.getPaddingRight() * proWidth), limitMin(view.getPaddingBottom() * proWidth));
            if (view instanceof TextView)
                ((TextView) view).setTextSize(TypedValue.COMPLEX_UNIT_PX, (float) (((TextView) view).getTextSize() * proWidth));
        } else if (baseMode == BASE_HEIGHT) {
            ViewGroup.MarginLayoutParams params = null;
            try {
                params = (ViewGroup.MarginLayoutParams) view.getLayoutParams();
            } catch (Exception e) {
            }
            if (params != null) {
                if (params.width > 0) {
                    params.width = limitMin(params.width * proHeight);
                }
                if (params.height > 0) {
                    params.height = limitMin(params.height * proHeight);
                }
                params.topMargin = limitMin(params.topMargin * proHeight);
                params.bottomMargin = limitMin(params.bottomMargin * proHeight);
                params.leftMargin = limitMin(params.leftMargin * proHeight);
                params.rightMargin = limitMin(params.rightMargin * proHeight);
                view.setLayoutParams(params);
            }
            view.setTranslationX(limitMin(view.getTranslationX() * proHeight));
            view.setTranslationY(limitMin(view.getTranslationY() * proHeight));
//            view.setMinimumHeight(limitMin(view.getMinimumHeight() * proHeight));
//            view.setMinimumWidth(limitMin(view.getMinimumWidth() * proHeight));
            view.setPadding(limitMin(view.getPaddingLeft() * proHeight), limitMin(view.getPaddingTop() * proHeight), limitMin(view.getPaddingRight() * proHeight), limitMin(view.getPaddingBottom() * proHeight));
            if (view instanceof TextView)
                ((TextView) view).setTextSize(TypedValue.COMPLEX_UNIT_PX, (float) (((TextView) view).getTextSize() * proHeight));
        }
        return view;
    }

    private int limitMin(double num) {
        if (num == 0) {
            return 0;
        } else if (num > 0 && num < 1) {
            return 1;
        } else if (num < 0 && num > -1) {
            return -1;
        } else {
            return (int) num;
        }
    }
}