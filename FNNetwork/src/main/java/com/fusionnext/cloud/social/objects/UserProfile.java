package com.fusionnext.cloud.social.objects;

import android.util.Log;

import com.fusionnext.cloud.datalink.json.FNJsonArray;
import com.fusionnext.cloud.datalink.json.FNJsonObject;
import com.fusionnext.cloud.social.Public;

import org.json.JSONException;

import java.util.ArrayList;

/**
 * Created by Mike Chang on 2016/2/22.
 */
public class UserProfile {

    protected final static String _JSON_TAG = "user";

    public UserProfile() {
        _userId = "";
        _socialProvider = "";
        _socialId = "";
        _name = "";
        _avatar = "";
        _birthday = "";
        _gender = "";
        _locale = "";
        _createdAt = "";
        _updatedAt = "";
        _devices = "";
        _trackList = new ArrayList<Track>();
        _follows = "";
        _followers = "";
        _followsCount = 0;
        _followersCount = 0;
        _tracksCount = 0;
        _isFollowing = 0;
        _isFollower = 0;
    }

    public String _userId; // user id
    public String _socialProvider; // user's social_provider
    public String _socialId; // user's social_id
    public String _name; // user's name
    public String _avatar; // user's photo
    public String _birthday;  // user's gender
    public String _gender;  // user's gender
    public String _locale; // user's locale
    public String _createdAt;  // user's created_at
    public String _updatedAt; // user's updated_ato
    public String _devices;
    public ArrayList<Track> _trackList;
    public String _follows;
    public String _followers;
    public int _followsCount;
    public int _followersCount;
    public int _tracksCount;
    public int _isFollowing; //The first one displays the number of users who this user is following. And is_following means "are you in this list or not".
    public int _isFollower; //The second one displays the number of users who is following this user (his followers). And is_followed means "are you in this list of not"

    public void clean() {
        _userId = "";
        _socialProvider = "";
        _socialId = "";
        _name = "";
        _avatar = "";
        _birthday = "";
        _gender = "";
        _locale = "";
        _createdAt = "";
        _updatedAt = "";
        _devices = "";
        if (_trackList != null) {
            _trackList.clear();
        }
        _follows = "";
        _followers = "";
        _followsCount = 0;
        _followersCount = 0;
        _tracksCount = 0;
        _isFollowing = 0;
        _isFollower = 0;
    }

    public boolean getDataFromJSONObject(FNJsonObject userObj) {
        clean();
        if (userObj == null) {
            Log.w(_JSON_TAG, " No JSON information");
            return false;
        }
        try {
            _userId = userObj.getStringDefault("id");
            _socialProvider = userObj.getStringDefault("social_provider");
            _socialId = userObj.getStringDefault("social_id");
            _name = userObj.getStringDefault("name");
            FNJsonObject avaterObj = userObj.getJSONObject("avatar");
            if (avaterObj != null) {
                _avatar = Public._HOST_V1 + avaterObj.getStringDefault("href");
            }

            _birthday = userObj.getStringDefault("birthday");
            _gender = userObj.getStringDefault("gender");
            _locale = userObj.getStringDefault("locale");
            _createdAt = userObj.getStringDefault("created_at");
            _updatedAt = userObj.getStringDefault("updated_at");

            FNJsonObject devicesObj = userObj.getJSONObject("devices");
            if (devicesObj != null) {
                _devices = Public._HOST_V1 + devicesObj.getStringDefault("href");
            }
            String tracksObj = userObj.getString("tracks");
            if (tracksObj != null) {
                _trackList = parseTrackArray(tracksObj);
            }
            FNJsonObject followsObj = userObj.getJSONObject("follows");
            if (followsObj != null) {
                _follows = Public._HOST_V1 + followsObj.getStringDefault("href");
            }
            FNJsonObject followersObj = userObj.getJSONObject("followers");
            if (followersObj != null) {
                _followers = Public._HOST_V1 + followersObj.getStringDefault("href");
            }

            FNJsonObject followsMetaObj = userObj.getJSONObject("followsmeta");
            if (followsMetaObj != null) {
                _followsCount = followsMetaObj.getInt("count");
                _isFollowing = followsMetaObj.getBooleanDefault("is_following") ? 1 : 0;
            }

            FNJsonObject followerMetaObj = userObj.getJSONObject("followersmeta");
            if (followerMetaObj != null) {
                _followersCount = followerMetaObj.getInt("count");
                _isFollower = followerMetaObj.getBooleanDefault("is_followed") ? 1 : 0;
            }

            FNJsonObject tracksMetaObj = userObj.getJSONObject("tracksmeta");
            if (tracksMetaObj != null) {
                _tracksCount = tracksMetaObj.getInt("count");
            }
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
        return true;
    }

    public static ArrayList<UserProfile> parseUserProfileArray(String json) {
        String avatar;

        try {
            ArrayList<UserProfile> userList = new ArrayList<UserProfile>();
            FNJsonObject userObject = new FNJsonObject(json);
            FNJsonArray usersArray = userObject.getJSONArray("items");
            if (usersArray == null) {
                return null;
            }
            for (int i = 0; i < usersArray.length(); i++) {
                FNJsonObject object = usersArray.getJSONObject(i);
                UserProfile user = new UserProfile();
                user.getDataFromJSONObject(object);
                if (user == null) return null;
                userList.add(user);
            }
            return userList;
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static ArrayList<Track> parseTrackArray(String json) {
        try {
            ArrayList<Track> trackList = new ArrayList<Track>();
            FNJsonObject trackObject = new FNJsonObject(json);
            FNJsonArray tracksArray = trackObject.getJSONArray("data");
            if (tracksArray == null) {
                return null;
            }
            for (int i = 0; i < tracksArray.length(); i++) {
                FNJsonObject object = tracksArray.getJSONObject(i);
                Track track = new Track();
                track.getDataFromJSONObject(object);
                if (track == null) return null;
                trackList.add(track);
            }
            return trackList;
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return null;
    }
}
