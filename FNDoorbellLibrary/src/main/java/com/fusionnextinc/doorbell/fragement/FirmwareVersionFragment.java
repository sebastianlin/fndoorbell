package com.fusionnextinc.doorbell.fragement;

import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.fusionnextinc.doorbell.MyApplication;
import com.fusionnextinc.doorbell.R;
import com.fusionnextinc.doorbell.manager.thingsManager.ThingsHandler;
import com.fusionnextinc.doorbell.manager.thingsManager.ThingsManager;
import com.fusionnextinc.doorbell.objects.FNError;
import com.fusionnextinc.doorbell.objects.FNEvent;
import com.fusionnextinc.doorbell.objects.FNFirmware;
import com.fusionnextinc.doorbell.objects.FNSchedule;
import com.fusionnextinc.doorbell.objects.FNThing;
import com.fusionnextinc.doorbell.objects.FNUserProfile;
import com.fusionnextinc.doorbell.utils.Utils;
import com.fusionnextinc.doorbell.widget.FNDialog;
import com.fusionnextinc.doorbell.widget.FNLayoutUtil;
import com.fusionnextinc.doorbell.widget.FNProgressBarCustom;

import java.util.ArrayList;

/**
 * Created by Mike Chang on 2017/7/17
 */
public class FirmwareVersionFragment extends FNFragment implements View.OnClickListener {
    private static final String TAG = FirmwareVersionFragment.class.getSimpleName();

    private FNLayoutUtil layoutUtil;
    private View viewInfo;
    private TextView txtFwVersion;
    private TextView txtFwDescription;
    private Button btnUpdate;
    private FNProgressBarCustom progressBar;
    private ThingsManager thingsManager;
    private static FNThing curFnThing;
    private static FNFirmware newFnFirmware;

    public static void openFragment(boolean clearOtherStack, FNThing fnThing) {
        FNFragmentManager.getInstance().openFragment(new FirmwareVersionFragment(), clearOtherStack);
        curFnThing = fnThing;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        layoutUtil = new FNLayoutUtil(getActivity(), MyApplication.LAYOUT_WIDTH, MyApplication.LAYOUT_HEIGHT, FNLayoutUtil.BASE_WIDTH);
        thingsManager = ThingsManager.getInstance(getActivity());
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        final ViewGroup nullParent = null;
        getActivity().setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        getActionBar().resetAll();
        getActionBar().setTitle(getString(R.string.fn_tittle_firmware_firmware_version), Gravity.CENTER, null);
        getActionBar().setCoverMode(false);
        getActionBar().setStartImage(R.drawable.back, new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        getActionBar().show();
        View view = inflater.inflate(R.layout.fragment_firmware_version, nullParent);
        layoutUtil.fitAllView(view);

        viewInfo = view.findViewById(R.id.firmware_info);
        txtFwVersion = (TextView) view.findViewById(R.id.text_firmware_version);
        txtFwDescription = (TextView) view.findViewById(R.id.text_firmware_description);
        btnUpdate = (Button) view.findViewById(R.id.btn_firmware_update);
        progressBar = new FNProgressBarCustom(getActivity(), (ViewGroup) view.findViewById(R.id.fragment_firmware_version));

        btnUpdate.setOnClickListener(this);

        txtFwDescription.setText(getString(R.string.fn_text_firmware_description, curFnThing.getName()));
        txtFwVersion.setText(getString(R.string.fn_title_firmware_version, curFnThing.getFirmwareVer()));
        if (Utils.isNetworkConnected(getActivity())) {
            thingsManager.setThingsListener(thingsListener);
            thingsManager.getNewFirmwareVersion(curFnThing);
            startWait();
        } else {
            FNDialog.showNoNetworkDialog(getActivity());
        }
        return view;
    }

    @Override
    public void onClick(View view) {
        if (view.getId() == R.id.btn_firmware_update) {
            FirmwareUpdateFragment.openFragment(false, curFnThing, newFnFirmware);
        }
    }

    private void startWait() {
        progressBar.show();
    }


    private void stopWait() {
        progressBar.hide();
    }

    final ThingsHandler.ThingsListener thingsListener = new ThingsHandler.ThingsListener() {
        @Override
        public void onListUserOwnedThings(ThingsManager instance, boolean result, String userId, ArrayList<FNThing> thingArrayList, FNError error) {

        }

        @Override
        public void onListUserInvitedThings(ThingsManager instance, boolean result, String userId, ArrayList<FNThing> thingArrayList, FNError error) {

        }

        @Override
        public void onListUserSharedThings(ThingsManager instance, boolean result, String userId, ArrayList<FNThing> thingArrayList, FNError error) {

        }

        @Override
        public void onListUserAllThings(ThingsManager instance, boolean result, String userId, ArrayList<FNThing> ownedThingArrayList, ArrayList<FNThing> sharedThingArrayList, ArrayList<FNThing> invitedThingArrayList, FNError error) {

        }

        @Override
        public void onListThingEvents(ThingsManager instance, boolean result, String thingId, String eventName, ArrayList<FNEvent> eventList, FNError error) {

        }

        @Override
        public void onListThingEventsByTime(ThingsManager instance, boolean result, String thingId, String eventName, String startTime, String endTime, ArrayList<FNEvent> eventList, FNError error) {

        }

        @Override
        public void onCurBatteryLevel(ThingsManager instance, boolean result, String thingId, String batteryLevel, FNError error) {

        }

        @Override
        public void onCurFirmwareVersion(ThingsManager instance, boolean result, String thingId, String fwVersion, FNError error) {

        }

        @Override
        public void onCurSwitchedStatus(ThingsManager instance, boolean result, String thingId, boolean isSwitch, FNError error) {

        }

        @Override
        public void onNewFirmwareVersion(ThingsManager instance, boolean result, String thingId, FNFirmware fnFirmware) {
            if (isAdded()) {
                if (thingId.equals(curFnThing.getThingId())) {
                    newFnFirmware = fnFirmware;
                    viewInfo.setVisibility(View.VISIBLE);
                    if (newFnFirmware != null) {
                        if (newFnFirmware.getVersion() != null) {
                            txtFwVersion.setText(getString(R.string.fn_title_firmware_version, newFnFirmware.getVersion()));
                            txtFwVersion.setVisibility(View.VISIBLE);
                        }
                        if (curFnThing.getFirmwareVer().equals(newFnFirmware.getVersion())) {
                            txtFwDescription.setVisibility(View.VISIBLE);
                            txtFwDescription.setText(getString(R.string.fn_dialog_firmware_up_to_date));
                            btnUpdate.setVisibility(View.GONE);
                        } else {
                            if (newFnFirmware.getDescription() != null) {
                                txtFwDescription.setVisibility(View.VISIBLE);
                                txtFwDescription.setText(newFnFirmware.getDescription());
                            }
                            btnUpdate.setVisibility(View.VISIBLE);
                        }
                    } else {
                        if (curFnThing.getFirmwareVer() != null) {
                            txtFwVersion.setText(getString(R.string.fn_title_firmware_version,curFnThing.getFirmwareVer()));
                            txtFwVersion.setVisibility(View.VISIBLE);
                            txtFwDescription.setVisibility(View.VISIBLE);
                            txtFwDescription.setText(getString(R.string.fn_dialog_firmware_up_to_date));
                            btnUpdate.setVisibility(View.GONE);
                        }
                    }

                }
                stopWait();
            }
        }

        @Override
        public void onStartFwUpdate(ThingsManager instance, boolean result, String thingId, String fwId) {

        }

        @Override
        public void onCurRingTone(ThingsManager instance, boolean result, String thingId, String ringtoneId, FNError error) {

        }

        @Override
        public void onGetScheduleList(ThingsManager instance, boolean result, String thingId, ArrayList<FNSchedule> scheduleList, FNError error) {

        }

        @Override
        public void onListInvitedUsers(ThingsManager instance, boolean result, String thingId, ArrayList<FNUserProfile> userList, FNError error) {

        }

        @Override
        public void onListSharedUsers(ThingsManager instance, boolean result, String thingId, ArrayList<FNUserProfile> userList, FNError error) {

        }

        @Override
        public void onCreateClaimCode(ThingsManager instance, boolean result, String claimCode, FNError error) {

        }

        @Override
        public void onCreateThing(ThingsManager instance, boolean result, FNThing fnThing, FNError error) {

        }

        @Override
        public void onUpdateThing(ThingsManager instance, boolean result, FNThing fnThing, FNError error) {

        }

        @Override
        public void onDeleteThing(ThingsManager instance, boolean result, FNError error) {

        }

        @Override
        public void onGetThing(ThingsManager instance, boolean result, FNThing fnThing, FNError error) {

        }

        @Override
        public void onChangeRingtone(ThingsManager instance, boolean result, String thingId, int ringtoneId) {

        }

        @Override
        public void onSetTimer(ThingsManager instance, boolean result, String thingId, String value) {

        }

        @Override
        public void onSetSchedule(ThingsManager instance, boolean result, String thingId, String value) {

        }

        @Override
        public void onShareInvited(ThingsManager instance, boolean result, String thingId, FNError error) {

        }

        @Override
        public void onShareAccepted(ThingsManager instance, boolean result, String thingId, FNError error) {

        }

        @Override
        public void onShareRejected(ThingsManager instance, boolean result, String thingId, FNError error) {

        }

        @Override
        public void onSwitchOnOff(ThingsManager instance, boolean result, String thingId, boolean isTurnOn) {

        }

        @Override
        public void onCloseCallSlot(ThingsManager instance, boolean result, FNError error) {

        }

        @Override
        public void onTakeCallSlot(ThingsManager instance, boolean result, String thingId, String callSlot, FNError error) {

        }
    };
}
