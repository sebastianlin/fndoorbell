package com.fusionnextinc.doorbell.voice;

import android.app.Activity;
import android.content.res.AssetFileDescriptor;
import android.media.MediaPlayer;

import com.fusionnext.cloud.iot.Public;
import com.fusionnextinc.doorbell.utils.FNLog;
import com.iptnet.common.c2c.C2CProcess;
import java.util.concurrent.atomic.AtomicInteger;
import java.io.IOException;

/**
 * Created by MikeChang on 2019/6/17
 */

public class FNRingToneManager {
    private static final String TAG = "FNRingToneManager";
    private static AtomicInteger mRingCount=null;
    private static FNRingToneManager sFNRingToneManager;
    private MediaPlayer mMediaPlayer;

    private OnFNRingToneListener mFNRingToneListener;

    private int mRingtoneRepeatTimes = 0;
    private Thread mRingToneThread = null;
    private static boolean isInterrupt = false;

    public static FNRingToneManager getInstance() {
        synchronized (FNRingToneManager.class) {
            if (sFNRingToneManager == null) {
                sFNRingToneManager = new FNRingToneManager();
            }
        }
        return sFNRingToneManager;
    }

    private FNRingToneManager() {
        mRingCount = new AtomicInteger(0);
        mMediaPlayer = new MediaPlayer();
    }
    public static void ContinueRing(  )
    {
        if ( mRingCount != null &&   C2CProcess.getInstance().getP2PServerStatus() != C2CProcess.P2P_STATUS_IDLE)
        {
            if( mRingCount.get() > 0) {
                mRingCount.set(30);
            }
            //    mFNRingToneManager.AddCallCount(RINGTONE_PLAY_DURATION);
        }
    }

    /**
     * 聲音播放器 - 播放
     *
     * @param ringtoneId
     */
    public synchronized void play(Activity activity, int ringtoneId, final int repeatTime) {
        AssetFileDescriptor afd = activity.getResources().openRawResourceFd(ringtoneId);
        if (afd != null) {
            try {
                if (mMediaPlayer == null) {
                    mMediaPlayer = new MediaPlayer();
                }
                mRingtoneRepeatTimes = 0;
                mMediaPlayer.reset();
                mMediaPlayer.setDataSource(afd.getFileDescriptor(), afd.getStartOffset(), afd.getLength());
                mMediaPlayer.prepare();
                mMediaPlayer.start();
                //監聽 播完執行--dream
                mMediaPlayer.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
                    @Override
                    public void onCompletion(MediaPlayer mp) {
                        FNLog.d(TAG, "onCompletion ");
                        if (mRingtoneRepeatTimes >= repeatTime - 1) {
                            if (mFNRingToneListener != null) {
                                mFNRingToneListener.OnCompletion();
                            }
                        } else if (mRingtoneRepeatTimes < repeatTime) {
                            mMediaPlayer.start();
                        }
                        mRingtoneRepeatTimes++;
                    }
                });
                afd.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }


    public synchronized void playWithTime(final Activity activity, final int ringtoneId, final long stopTime) {
        if (mRingToneThread == null) {
            Thread thread = new Thread(new Runnable() {
                @Override
                public synchronized void run() {
                    isInterrupt = false;
                    if(mRingCount == null )
                        mRingCount = new AtomicInteger(0);
                    mRingCount.addAndGet((int) (stopTime/1000));
                    play(activity, ringtoneId, 999);
                    try {
                        while( mRingCount != null && mRingCount.decrementAndGet()  > 0 ) {
                            Thread.sleep(1000);
                        }
                    } catch (InterruptedException ignore) {
                    }
                    stop();

                    if (mFNRingToneListener != null && !isInterrupt) {
                        mFNRingToneListener.OnCompletion();
                    }
                }
            });
            mRingToneThread = thread;
            thread.start();
        }
    }

    /**
     * 聲音播放器 - 暫停
     */
    public void pause() {
        mMediaPlayer.pause();
    }

    /**
     * 聲音播放器 - 停止
     */
    public synchronized void stop() {
        if (mMediaPlayer != null) {
            mMediaPlayer.stop();
            mMediaPlayer.release();
            mMediaPlayer = null;
            mRingCount = null;
        }
    }

    public synchronized void destory() {


        mRingCount = null;
        mFNRingToneListener = null;
        if (mRingToneThread != null) {
            mRingToneThread.interrupt();
            mRingToneThread = null;
            isInterrupt = true;
        }
    }

    public void setFNRingToneListener(OnFNRingToneListener listener) {
        mFNRingToneListener = listener;
    }
}
