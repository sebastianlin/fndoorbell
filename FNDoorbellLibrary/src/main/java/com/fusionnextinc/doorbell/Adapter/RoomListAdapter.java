package com.fusionnextinc.doorbell.Adapter;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.fusionnextinc.doorbell.MyApplication;
import com.fusionnextinc.doorbell.R;
import com.fusionnextinc.doorbell.manager.thingsManager.ThingsManager;
import com.fusionnextinc.doorbell.widget.FNLayoutUtil;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by Mike Chang on 2018/8/7
 */
public class RoomListAdapter extends BaseAdapter {
    private static final String TAG = "RoomListAdapter";

    private FNLayoutUtil layoutUtil;
    private Activity activity;
    private ArrayList<HashMap<String, Object>> roomList;

    public RoomListAdapter(Activity activity, ArrayList<HashMap<String, Object>> roomList) {
        layoutUtil = new FNLayoutUtil(activity, MyApplication.LAYOUT_WIDTH, MyApplication.LAYOUT_HEIGHT, FNLayoutUtil.BASE_WIDTH);
        this.activity = activity;
        this.roomList = roomList;
    }

    @Override
    public int getCount() {
        return roomList.size();
    }

    @Override
    public Object getItem(int position) {
        return roomList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        final ViewHolder holder;
        final ViewGroup nullParent = null;

        if (convertView == null) {
            holder = new ViewHolder();
            convertView = LayoutInflater.from(activity).inflate(R.layout.adapter_room_list, nullParent);
            layoutUtil.fitAllView(convertView);
            holder.txtRoomType = (TextView) convertView.findViewById(R.id.txt_room_type);
            holder.imgRoom = (ImageView) convertView.findViewById(R.id.img_room);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }
        holder.txtRoomType.setText(ThingsManager.getInstance(activity).getRoomStringById(position));
        holder.imgRoom.setImageResource(ThingsManager.getInstance(activity).getRoomImageById(position));
        return convertView;
    }

    private class ViewHolder {
        private TextView txtRoomType;
        private ImageView imgRoom;
    }
}
