//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//
package com.fusionnextinc.doorbell.objects;

import com.fusionnext.cloud.iot.objects.Firmware;

public class FNFirmware {
    private String fwId =  "";
    private String name =  "";
    private String version =  "";
    private String description = "";
    private String filename =  "";
    private String downloadUrl = "";
    private String createdAt =  "";
    private String updatedAt =  "";

    public FNFirmware() {
    }

    public void clean() {
        this.fwId = "";
        this.name = "";
        this.version = "";
        this.description = "";
        this.filename = "";
        this.downloadUrl = "";
        this.createdAt = "";
        this.updatedAt = "";
    }
    public static FNFirmware parseFirmware(Firmware firmware) {
        FNFirmware fnFirmware = new FNFirmware();

        if (firmware == null) {
            return null;
        }
        fnFirmware.fwId = firmware._fwId;
        fnFirmware.name = firmware._name;
        fnFirmware.version = firmware._version;
        fnFirmware.description = firmware._description;
        fnFirmware.filename = firmware._filename;
        fnFirmware.downloadUrl = firmware._downloadUrl;
        fnFirmware.createdAt = firmware._createdAt;
        fnFirmware.updatedAt = firmware._updatedAt;
        return fnFirmware;
    }

    public String getFwId() {
        return fwId;
    }

    public void setFwId(String fwId) {
        this.fwId = fwId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getFilename() {
        return filename;
    }

    public void setFilename(String filename) {
        this.filename = filename;
    }

    public String getDownloadUrl() {
        return downloadUrl;
    }

    public void setDownloadUrl(String downloadUrl) {
        this.downloadUrl = downloadUrl;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }
}
