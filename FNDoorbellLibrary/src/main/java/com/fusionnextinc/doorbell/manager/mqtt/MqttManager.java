package com.fusionnextinc.doorbell.manager.mqtt;

import android.content.Context;

import com.fusionnext.cloud.iot.Public;
import com.fusionnext.cloud.utils.Utils;

import java.util.ArrayList;
import java.util.List;

public class MqttManager implements MqttListener {

    private static MyMqtt mMyMqtt;
    private static List<MqttListener> mMqttListenerList = new ArrayList<>();
    private static final String TAG = "MqttService";

    /**
     * MQTT配置参数
     **/
    private String protocol = "ssl";
    private String host = "broker.fusionnextinc.com";
    private String port = "8883";
    private String userID = "device";
    private String passWord = "";
    private String clientID = "";
    private Context mContext;

    private static MqttManager instance;

    public static MqttManager getInstance(Context context) {
        if (instance == null) {
            synchronized (MqttManager.class) {
                if (instance == null) {
                    instance = new MqttManager(context);
                }
            }
        }
        return instance;
    }

    private MqttManager(Context context) {
        if (mMyMqtt == null) {
            mMyMqtt = new MyMqtt(context, this);
        }
        mContext = context;
    }

    public void connect() {
        if (Public.getAuthorization(mContext).isLogin()) {
            passWord = Public.getAuthorization(mContext).getUserAccessToken().replace("Bearer", "").trim();
            clientID = Utils.getUUID(mContext, null);
            mMyMqtt.setMqttSetting(protocol, host, port, userID, passWord, clientID);
            mMyMqtt.connectMqtt();
        }
    }

    public boolean isConnectionIdle() {
        return mMyMqtt.isConnectionIdle();
    }

    public boolean isConnected() {
        return mMyMqtt.isConnected();
    }

    /**
     * 断开Mqtt连接
     */
    public void disConnect() {
        mMyMqtt.disConnectMqtt();
    }

    public static void addMqttListener(MqttListener listener) {
        if (!mMqttListenerList.contains(listener)) {
            mMqttListenerList.add(listener);
        }
    }

    public static void removeMqttListener(MqttListener listener) {
        mMqttListenerList.remove(listener);
    }


    public void subTopic(String Topic, int Qos) {
        mMyMqtt.subTopic(Topic, Qos);
    }

    @Override
    public void onConnected() {
        for (MqttListener mqttListener : mMqttListenerList) {
            mqttListener.onConnected();
        }
    }

    @Override
    public void onFail() {
        for (MqttListener mqttListener : mMqttListenerList) {
            mqttListener.onFail();
        }
    }

    @Override
    public void onLost() {
//        new Thread(new Runnable() {
//            @Override
//            public void run() {
//                passWord = Public.getAuthorization(mContext).getUserAccessToken().replace("Bearer", "").trim();
//                mMyMqtt.setMqttSetting(protocol, host, port, userID, passWord, clientID);
//                mMyMqtt.connectMqtt();
//                try {
//                    Thread.sleep(3000);
//                } catch (InterruptedException e) {
//                    e.printStackTrace();
//                }
//            }
//        }).start();
        for (MqttListener mqttListener : mMqttListenerList) {
            mqttListener.onLost();
        }
    }

    @Override
    public void onReceive(String message) {
        for (MqttListener mqttListener : mMqttListenerList) {
            mqttListener.onReceive(message);
        }
    }

    @Override
    public void onSendSucc() {
        for (MqttListener mqttListener : mMqttListenerList) {
            mqttListener.onSendSucc();
        }
    }
}
