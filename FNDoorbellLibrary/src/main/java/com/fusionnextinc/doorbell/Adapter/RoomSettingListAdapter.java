package com.fusionnextinc.doorbell.Adapter;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.fusionnextinc.doorbell.MyApplication;
import com.fusionnextinc.doorbell.R;
import com.fusionnextinc.doorbell.manager.data.AppDataThing;
import com.fusionnextinc.doorbell.manager.data.ListMap;
import com.fusionnextinc.doorbell.manager.thingsManager.ThingsManager;
import com.fusionnextinc.doorbell.utils.FNLog;
import com.fusionnextinc.doorbell.widget.FNHeaderListView.FNListViewAdapter;
import com.fusionnextinc.doorbell.widget.FNLayoutUtil;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by Mike Chang on 2018/8/8
 */
public class RoomSettingListAdapter extends FNListViewAdapter {
    private static final String TAG = "RoomSettingListAdapter";

    private final static int TYPE_DRAG_HINT = 0;
    public final static int TYPE_OWNED_THINGS_LIST = 1;
    public final static int TYPE_SHARED_THINGS_LIST = 2;
    private final static int SECTION_COUNTS = TYPE_SHARED_THINGS_LIST + 1;

    private Context mContext;
    private LayoutInflater mInflater;
    private FNLayoutUtil mLayoutUtil;
    private ListMap mOwnedlistMap = new ListMap();
    private ListMap mSharedlistMap = new ListMap();
    private int mLayoutID;
    // 用来控制CheckBox的选中状况
    private static HashMap<Integer, Boolean> mIsOwnedSelected = new HashMap<Integer, Boolean>();
    private static HashMap<Integer, Boolean> mIsSharedSelected = new HashMap<Integer, Boolean>();

    public RoomSettingListAdapter(Context context, ListMap ownedlistMap, ListMap sharedlistMap, int layoutID) {
        this.mContext = context;
        this.mInflater = LayoutInflater.from(context);
        this.mOwnedlistMap.addAll(ownedlistMap);
        this.mSharedlistMap.addAll(sharedlistMap);
        this.mLayoutID = layoutID;
        for (int i = 0; i < ownedlistMap.size(); i++) {
            mIsOwnedSelected.put(i, false);
        }
        for (int i = 0; i < sharedlistMap.size(); i++) {
            mIsSharedSelected.put(i, false);
        }
    }

    private static class sectionHolder {
        //ID, its invisible
        private TextView textSection;
        private TextView textHint;
        private LinearLayout sectionLayout;
    }

    private static class ViewHolder {
        //ID, its invisible
        private TextView textId;
        private TextView textType;
        private LinearLayout bgLayout;
        private ImageView imgThing;
        private CheckBox imgSelectedThing;
        private TextView textName;
    }

    public void updateAdapterData(int thingsType) {
        switch (thingsType) {
            case AppDataThing.THING_TYPE_THINGS_OWNED:
                mOwnedlistMap.clear();
                mOwnedlistMap.addAll(AppDataThing.getInstance(mContext).getAdapterData(thingsType));
                break;
            case AppDataThing.THING_TYPE_THINGS_SHARED:
                mSharedlistMap.clear();
                mSharedlistMap.addAll(AppDataThing.getInstance(mContext).getAdapterData(thingsType));
                break;
            case AppDataThing.THING_TYPE_THINGS_ALL:
                mOwnedlistMap.clear();
                mOwnedlistMap.addAll(AppDataThing.getInstance(mContext).getAdapterData(AppDataThing.THING_TYPE_THINGS_OWNED));
                break;
        }
        notifyDataSetChanged();
    }

    public void updateAdapterDataById(String thingId) {
        if (updateMapDataById(mSharedlistMap, thingId)) {
            notifyDataSetChanged();
            return;
        }
        if (updateMapDataById(mOwnedlistMap, thingId)) {
            notifyDataSetChanged();
            return;
        }
        notifyDataSetChanged();
    }

    private boolean updateMapDataById(ListMap listMap, String thingId) {
        for (int i = 0; i < listMap.size(); i++) {
            Map<String, Object> map = listMap.getList().get(i);
            Object object = map.get(AppDataThing.TAG_THING_ID);
            String id = null;
            if (object != null) {
                if (object instanceof Integer) id = Integer.toString((Integer) object);
                else if (object instanceof String) id = (String) object;
            }
            if (id != null && id.equals(thingId)) {
                Map<String, Object> newMap = AppDataThing.getInstance(mContext).getThingMapData(thingId);
                if (newMap != null) {
                    listMap.update(i, newMap);
                }
                return true;
            }
        }
        return false;
    }

    @Override
    public boolean isEnabled(int section, int position) {
        return true;
    }

    @Override
    public Object getItem(int section, int position) {
        if (section < 0 || position < 0) {
            FNLog.d(TAG, "section is " + section + ", position is " + position);
            return null;
        }
        switch (section) {
            case TYPE_SHARED_THINGS_LIST:
                return mSharedlistMap.get(position);
            case TYPE_OWNED_THINGS_LIST:
                return mOwnedlistMap.get(position);
        }
        return null;
    }

    @Override
    public long getItemId(int section, int position) {
        return position;
    }

    @Override
    public int getSectionCount() {
        return SECTION_COUNTS;
    }

    @Override
    public int getCountForSection(int section) {

        switch (section) {
            case TYPE_SHARED_THINGS_LIST:
                if (mSharedlistMap.size() <= 0) {
                    return 1;
                } else {
                    return mSharedlistMap.size();
                }
            case TYPE_OWNED_THINGS_LIST:
                if (mOwnedlistMap.size() <= 0) {
                    return 1;
                } else {
                    return mOwnedlistMap.size();
                }
            default:
                return 0;
        }
    }

    @Override
    public View getItemView(final int section, final int position, View convertView, ViewGroup parent) {
        //Ready the ViewHolder
        Map<String, Object> map;
        ViewHolder holder;

        if (convertView == null) {
            holder = new ViewHolder();
            mLayoutUtil = new FNLayoutUtil((Activity) mContext, MyApplication.LAYOUT_WIDTH, MyApplication.LAYOUT_HEIGHT, FNLayoutUtil.BASE_WIDTH);
            convertView = mInflater.inflate(mLayoutID, null);
            holder.bgLayout = (LinearLayout) convertView.findViewById(R.id.adapter_room_thing_list);
            mLayoutUtil.fitAllView(holder.bgLayout);
            holder.textId = (TextView) convertView.findViewById(R.id.textThingId);
            holder.textType = (TextView) convertView.findViewById(R.id.textThingType);
            holder.imgThing = (ImageView) convertView.findViewById(R.id.imageThing);
            holder.textName = (TextView) convertView.findViewById(R.id.textName);
            holder.imgSelectedThing = (CheckBox) convertView.findViewById(R.id.img_room_select);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        holder.bgLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                switch (section) {
                    case TYPE_OWNED_THINGS_LIST:
                        setSelectOwnedChkBox(position);
                        break;
                    case TYPE_SHARED_THINGS_LIST:
                        setSelectSharedChkBox(position);
                        break;
                }
            }
        });

        holder.imgSelectedThing.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                switch (section) {
                    case TYPE_OWNED_THINGS_LIST:
                        if (mOwnedlistMap != null && mOwnedlistMap.size() > 0) {
                            mIsOwnedSelected.put(position, isChecked);
                        }
                        break;
                    case TYPE_SHARED_THINGS_LIST:
                        if (mSharedlistMap != null && mSharedlistMap.size() > 0) {
                            mIsSharedSelected.put(position, isChecked);
                        }
                        break;
                }
            }
        });

        switch (section) {
            case TYPE_OWNED_THINGS_LIST:
                if (mIsOwnedSelected.size() > 0) {
                    holder.imgSelectedThing.setChecked(mIsOwnedSelected.get(position));
                }
                break;
            case TYPE_SHARED_THINGS_LIST:
                if (mIsSharedSelected.size() > 0) {
                    holder.imgSelectedThing.setChecked(mIsSharedSelected.get(position));
                }
                break;
        }

        switch (section) {
            case TYPE_SHARED_THINGS_LIST:
                if (mSharedlistMap != null && mSharedlistMap.size() > 0) {
                    map = mSharedlistMap.get(position);
                    updateMyThingsView(holder, map, section);
                } else {
                    updateEmptyView(holder, section);
                }
                break;
            case TYPE_OWNED_THINGS_LIST:
                if (mOwnedlistMap != null && mOwnedlistMap.size() > 0) {
                    map = mOwnedlistMap.get(position);
                    updateMyThingsView(holder, map, section);
                } else {
                    updateEmptyView(holder, section);
                }
                break;
            default:
                break;
        }

        return convertView;
    }

    @Override
    public View getSectionHeaderView(int section, View convertView, ViewGroup parent) {
        sectionHolder holder;
        final ViewGroup nullParent = null;

        if (convertView == null) {
            holder = new sectionHolder();
            mLayoutUtil = new FNLayoutUtil((Activity) mContext, MyApplication.LAYOUT_WIDTH, MyApplication.LAYOUT_HEIGHT, FNLayoutUtil.BASE_WIDTH);
            convertView = mInflater.inflate(R.layout.roomsettings_section_item, nullParent);
            holder.sectionLayout = (LinearLayout) convertView.findViewById(R.id.roomsettings_section_item);
            mLayoutUtil.fitAllView(holder.sectionLayout);
            holder.textSection = (TextView) convertView.findViewById(R.id.textSection);
            holder.textHint = (TextView) convertView.findViewById(R.id.textHint);
            convertView.setTag(holder);
        } else {
            holder = (sectionHolder) convertView.getTag();
        }
        switch (section) {
            case TYPE_DRAG_HINT:
                holder.textHint.setVisibility(View.VISIBLE);
                holder.textHint.setText(mContext.getString(R.string.fn_title_drag_refresh));
                updateTextView(holder.textSection, null);
                break;
            case TYPE_OWNED_THINGS_LIST:
                holder.textHint.setVisibility(View.GONE);
                updateTextView(holder.textSection, mContext.getString(R.string.fn_title_main_my_things).replace("${Thing}", mContext.getString(R.string.fn_title_thing_name)));
                break;
            case TYPE_SHARED_THINGS_LIST:
                holder.textHint.setVisibility(View.GONE);
                updateTextView(holder.textSection, mContext.getString(R.string.fn_title_main_shared_things).replace("${Thing}", mContext.getString(R.string.fn_title_thing_name)));
                break;
            default:
                break;
        }
        return convertView;
    }

    private void updateTextView(TextView text, String strText) {
        if (text.getId() == R.id.textSection) {
            boolean hide = (strText == null || strText.trim().equals(""));
            text.setVisibility(hide ? View.GONE : View.VISIBLE);
        }
        text.setText(strText);
    }

    private void updateEmptyView(final ViewHolder holder, int section) {
        switch (section) {
            case TYPE_OWNED_THINGS_LIST:
                holder.textName.setText(mContext.getString(R.string.fn_text_my_doorbell_empty).replace("${Thing}", (mContext.getString(R.string.fn_title_thing_name))));
                holder.imgSelectedThing.setVisibility(View.GONE);
                break;
            case TYPE_SHARED_THINGS_LIST:
                holder.textName.setText(mContext.getString(R.string.fn_text_shared_doorbell_empty).replace("${Thing}", (mContext.getString(R.string.fn_title_thing_name))));
                holder.imgSelectedThing.setVisibility(View.GONE);
                break;
            default:
                break;
        }
        holder.textId.setText("");
        holder.imgThing.setVisibility(View.GONE);
    }

    private void updateMyThingsView(final ViewHolder holder, final Map<String, Object> map, final int thingType) {
        holder.textType.setText(String.valueOf(thingType));
        updateTextView(holder.textId, map, AppDataThing.TAG_THING_ID);
        updateTextView(holder.textName, map, AppDataThing.TAG_THING_NAME);
        updateThingIcon(holder, map, AppDataThing.TAG_THING_PRODUCT_ID);
        holder.imgSelectedThing.setVisibility(View.VISIBLE);
    }

    private void updateTextView(TextView text, Map<String, Object> map, String tag) {
        String strText = "";
        Object object = map.get(tag);
        if (object != null) {
            if (object instanceof Integer) strText = Integer.toString((Integer) object);
            else if (object instanceof String) strText = (String) object;
        }

        if (text.getId() == R.id.textSubtitle || text.getId() == R.id.textName) {
            boolean hide = (strText == null || strText.trim().equals(""));
            text.setVisibility(hide ? View.GONE : View.VISIBLE);
        }

        text.setText(strText);
    }

    private void updateThingIcon(ViewHolder holder, final Map<String, Object> map, String tag) {
        ImageView image = holder.imgThing;

        String id = (String) map.get(tag);
        if (id != null && id.length() > 0) {
            int productId = 0;
            try {
                productId = Integer.valueOf(id);
            } catch (NumberFormatException e) {
                e.printStackTrace();
            }
            if (productId == ThingsManager.PRODUCT_ID_DOORBELL || productId == ThingsManager.PRODUCT_ID_DOORBELL_VOICE) {
                image.setImageDrawable(mContext.getResources().getDrawable(R.drawable.thing_icon1));
            } else if (productId == ThingsManager.PRODUCT_ID_PLUG) {
                image.setImageDrawable(mContext.getResources().getDrawable(R.drawable.image_plug_logo));
            } else {
                image.setImageDrawable(mContext.getResources().getDrawable(R.drawable.unknown_thing_icon));
            }
            image.setVisibility(View.VISIBLE);
        }
    }

    public ArrayList<String> getSelectedOwnedThingList() {
        ArrayList<String> selectedThingList = new ArrayList<>();

        for (int i = 0; i < mIsOwnedSelected.size(); i++) {
            if (mIsOwnedSelected.get(i)) {
                if (mOwnedlistMap.get(i).containsKey(AppDataThing.TAG_THING_ID)) {
                    String ownedThingId = (String) mOwnedlistMap.get(i).get(AppDataThing.TAG_THING_ID);
                    selectedThingList.add(ownedThingId);
                }
            }
        }
        return selectedThingList;
    }

    public ArrayList<String> getSelectedSharedThingList() {
        ArrayList<String> selectedThingList = new ArrayList<>();

        for (int i = 0; i < mIsSharedSelected.size(); i++) {
            if (mIsSharedSelected.get(i)) {
                if (mSharedlistMap.get(i).containsKey(AppDataThing.TAG_THING_ID)) {
                    String sharedThingId = (String) mSharedlistMap.get(i).get(AppDataThing.TAG_THING_ID);
                    selectedThingList.add(sharedThingId);
                }
            }
        }
        return selectedThingList;
    }

    public void setSelectedOwnedThingList(String thingId) {
        for (int i = 0; i < mOwnedlistMap.size(); i++) {
            String ownedThingId = (String) mOwnedlistMap.get(i).get(AppDataThing.TAG_THING_ID);
            if (thingId.equals(ownedThingId)) {
                mIsOwnedSelected.put(i, true);
            }
        }
        notifyDataSetChanged();
    }

    public void setSelectedSharedThingList(String thingId) {
        for (int i = 0; i < mSharedlistMap.size(); i++) {
            String sharedThingId = (String) mSharedlistMap.get(i).get(AppDataThing.TAG_THING_ID);
            if (thingId.equals(sharedThingId)) {
                mIsSharedSelected.put(i, true);
            }
        }
        notifyDataSetChanged();
    }

    private void setSelectOwnedChkBox(int position) {
        if (mIsOwnedSelected.size() > 0) {
            boolean isChecked = mIsOwnedSelected.get(position);
            mIsOwnedSelected.put(position, !isChecked);
            notifyDataSetChanged();
        }
    }

    private void setSelectSharedChkBox(int position) {
        if (mIsSharedSelected.size() > 0) {
            boolean isChecked = mIsSharedSelected.get(position);
            mIsSharedSelected.put(position, !isChecked);
            notifyDataSetChanged();
        }
    }
}


