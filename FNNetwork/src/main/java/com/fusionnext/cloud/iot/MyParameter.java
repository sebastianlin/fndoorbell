package com.fusionnext.cloud.iot;

import android.app.Application;
import android.content.Context;
import android.util.Log;


/**
 * Created by Mike Chang  on 2015/7/27.
 */
public class MyParameter extends Application {
    private static final String TAG = "MyParameter";

    private final static String synchronizedTagDataMgr = "synchronizedTagDataMgr";
    private final static String synchronizedTagAuthorization = "synchronizedTagAuthorization";
    private final static String synchronizedTagPhotos = "synchronizedTagPhotos";
    private final static String synchronizedTagUser = "synchronizedTagUser";

    //Authorization
    private Authorization authorization;
    //Photos
    private Devices devices;
    //UserProfile
    private User user;
    //Database manager
    private DataManager dataMgr = null;

    private volatile static MyParameter myParameter = null;

    public static MyParameter newInstance() {
        if (myParameter == null) {
            synchronized (MyParameter.class) {
                if (myParameter == null) {
                    try {
                        myParameter = new MyParameter();
                    } catch (IllegalStateException e) {
                        Log.e(TAG, "error occurs when creating FN social network lib");
                    }
                }
            }
        }
        return myParameter;
    }

    public Authorization getAuthorization(Context context) {
        synchronized (synchronizedTagAuthorization) {
            if (authorization == null) setAuthorization(new Authorization(context));
            return authorization;
        }
    }

    public void setAuthorization(Authorization authorization) {
        synchronized (synchronizedTagAuthorization) {
            this.authorization = authorization;
        }
    }

    public Devices getDevices(Context context) {
        synchronized (synchronizedTagPhotos) {
            context = context.getApplicationContext();
            if (devices == null) setDevices(new Devices(context));
            return devices;
        }
    }

    public void setDevices(Devices devices) {
        synchronized (synchronizedTagPhotos) {
            this.devices = devices;
        }
    }

    public User getUser(Context context) {
        synchronized (synchronizedTagUser) {
            context = context.getApplicationContext();
            if (user == null) setUser(new User(context));
            return user;
        }
    }

    public void setUser(User user) {
        synchronized (synchronizedTagUser) {
            this.user = user;
        }
    }


    public DataManager getDataMgr(Context context) {
        synchronized (synchronizedTagDataMgr) {
            if (dataMgr == null) setDataMgr(DataManager.newData(context));
            return dataMgr;
        }
    }

    public void setDataMgr(DataManager dm) {
        synchronized (synchronizedTagDataMgr) {
            dataMgr = dm;
        }
    }
}