package com.fusionnext.cloud.iot;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;

import com.fusionnext.cloud.datalink.http.HttpPost;
import com.fusionnext.cloud.datalink.json.FNJsonObject;
import com.fusionnext.cloud.iot.objects.Device;
import com.fusionnext.cloud.iot.objects.ErrorObj;
import com.fusionnext.cloud.iot.objects.PushToken;
import com.fusionnext.cloud.iot.objects.UserProfile;
import com.fusionnext.cloud.utils.FNNetworkLog;
import com.fusionnext.cloud.utils.Utils;


import org.json.JSONException;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.concurrent.ExecutionException;

/**
 * Created by Mike Chang on 2016/2/22.
 */
public class Authorization {
    protected final static String _TAG = "Authorization";


    public static class Token {
        public String accessToken = "";
        public String expireIn = "";
        public String refreshToken = "";
        public String clientId = "";
        public long   currentTime=0;
        public long   expireTime=0;

        private  void set(Token token)
        {
            if (token.accessToken != null) accessToken = token.accessToken;
            if (token.clientId != null) clientId = token.clientId;
            if (token.expireIn != null) expireIn = token.expireIn;
            if (token.refreshToken != null) refreshToken = token.refreshToken;

            currentTime = token.currentTime;
            expireTime = token.expireTime;
        }

        public boolean parse(FNJsonObject object ) throws JSONException {

            if (object == null )
                return false;

            @SuppressLint("SimpleDateFormat") SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");

            String token = object.getStringDefault("access_token");
            accessToken = "Bearer " + token;
            expireIn = object.getStringDefault("expires_in");
            currentTime = System.currentTimeMillis();
            expireTime = currentTime + ((Long.parseLong(expireIn)-60)*1000);

            Date curDate = new Date(expireTime); // 獲取當前時間

            expireIn = formatter.format(curDate);
            refreshToken = object.getStringDefault("refresh_token");
            clientId = object.getStringDefault("client_id");
            return true;
        }
    }

    public static class ClientToken extends Token
    {
        public  void set(ClientToken token)
        {
            super.set(token);
        }

        public boolean parse(String jsonStr ) throws JSONException
        {
            if (jsonStr == null || jsonStr.equals("")) return false;

            FNJsonObject object = new FNJsonObject(jsonStr);
            if(super.parse(object)) {
                FNNetworkLog.i(_TAG, "Device token will be expired in " + expireIn);
                return true;
            }
            return false;
        }
    }

    public static class UserToken extends Token
    {
        public String userId = "";
        public String pushToken = "";

        public  void set(UserToken token)
        {
            super.set(token);
            if (userId != null) userId = token.userId;
            if (pushToken != null) pushToken = token.pushToken;
        }

        public boolean parse(String jsonStr ) throws JSONException {
            if (jsonStr == null || jsonStr.equals("")) return false;

            FNJsonObject object = new FNJsonObject(jsonStr);
            if(super.parse(object)) {
                userId = FNJsonObject.getJasonItems(object, "user_id");
                FNNetworkLog.i(_TAG, "UserProfile token will be expired in " + expireIn + ", UserId = " + userId);
                return true;
            }
            return false;
        }

    }

    private Context context;
    private ClientToken clientToken = new ClientToken();
    private UserToken userToken = new UserToken();

    private final static int REFRESH_TOKEN_TYPE_CLIENT = 0;
    private final static int REFRESH_TOKEN_TYPE_USER = 1;

    private IOTHandler.OnAuthorizationListener onAuthorizationListener;

    public interface onRefreshTokenListener {
        String onRefreshToken(boolean result, String accessToken);
    }

    public Authorization(Context context) {
        this.context = context;
        setClientToken(Public.getDataMgr(context).loadClientToken());
        setUserToken(Public.getDataMgr(context).loadUserToken());
    }

    public void activiateAPI(final IOTHandler.OnAuthorizationListener onAuthorizationListener) {
        String token = getClientAccessToken();
        if (token.equals("") || token.length() <= 0) {
            getClientCredentials(onAuthorizationListener);
        } else {
            if (onAuthorizationListener != null) {
                onAuthorizationListener.onActivateAPI(true, null);
            }
        }
        this.onAuthorizationListener = onAuthorizationListener;
    }

    public String getCommToken() {
        String token = "";

        if (clientToken != null) {
            token = clientToken.accessToken;
        }

        token = token.replace("Bearer", "").trim();
        return token;
    }

    protected synchronized String getClientAccessToken() {
        setClientToken(Public.getDataMgr(context).loadClientToken());
        if (clientToken != null) {
            if (!clientToken.accessToken.equals("") && !clientToken.refreshToken.equals("")) {

                refreshToken(REFRESH_TOKEN_TYPE_CLIENT, new onRefreshTokenListener() {
                    @Override
                    public String onRefreshToken(boolean result, String accessToken) {
                        FNNetworkLog.d(_TAG, "refresh accessToken  = " + accessToken);
                        return accessToken;
                    }
                });
            }
            return clientToken.accessToken;
        }
        return "";
    }

    public synchronized String getUserAccessToken() {
        setUserToken(Public.getDataMgr(context).loadUserToken());
        if (userToken != null) {
            if (!userToken.accessToken.equals("") && !userToken.refreshToken.equals("")) {
                refreshToken(REFRESH_TOKEN_TYPE_USER, new onRefreshTokenListener() {
                    @Override
                    public String onRefreshToken(boolean result, String accessToken) {
                        FNNetworkLog.d(_TAG, "refresh accessToken  = " + accessToken);
                        return accessToken;
                    }
                });
            }
            return userToken.accessToken;
        }
        return "";
    }

    public boolean isLogin() {
        boolean isLogin = false;
        FNNetworkLog.d(_TAG, "accessToken  = " + userToken.accessToken + ", refreshToken = " + userToken.refreshToken);
        if (userToken != null) {
            isLogin = !userToken.accessToken.equals("") && !userToken.refreshToken.equals("");
        }
        return isLogin;
    }

    public String getUserId() {
        if (userToken != null) {
            return userToken.userId;
        }
        return "";
    }

    public String getClientId() {
        if (clientToken != null) {
            return clientToken.clientId;
        }
        return "";
    }

    private void setClientToken(ClientToken token) {
        if (clientToken == null) clientToken = new ClientToken();
        clientToken.set(token);
    }

    private void setUserToken(UserToken token) {
        if (userToken == null) userToken = new UserToken();
        userToken.set(token);
    }

    private void getClientCredentials(final IOTHandler.OnAuthorizationListener onAuthorizationListener) {
        String packageName = Utils.getPackageName(context);
        String identifier;

        try {
            ApplicationInfo info = context.getPackageManager().getApplicationInfo(packageName, PackageManager.GET_META_DATA);
            identifier = info.metaData.getString("com.fusionnext.sdk.iot.API_KEY");
        } catch (Exception ignore) {
            identifier = null;
        }

        HttpPost.PostParam[] params = new HttpPost.PostParam[1];
        params[0] = new HttpPost.PostParam();
        params[0].url = HttpData.generateHttpPostUrl(HttpData._POST_TYPE_AUTH_GET_CLIENT_TOKEN);
        params[0].type = HttpData._POST_TYPE_AUTH_GET_CLIENT_TOKEN;
        params[0].param = new HashMap<>();
        params[0].param.put("grant_type", "client_credentials");
        params[0].param.put("client_id", identifier);

        HttpPost post = new HttpPost();
        post.setContext(context);
        post.setOnParseJsonListener(new HttpPost.OnParseJsonListener() {
            @Override
            public void onParse(boolean returnResult, int postType, String id, String returnStr) {
                ErrorObj errorObj = null;
                if (returnStr != null) {
                    if (returnResult) {
                        parseClientToken(returnStr);
                    } else {
                        errorObj = ErrorObj.parseErrorObj(returnStr);
                    }
                }
                if (onAuthorizationListener != null) {
                    onAuthorizationListener.onActivateAPI(returnResult, errorObj);
                }
            }
        });
        post.execute(params);
    }


    private ClientToken parseClientToken(String jsonStr) {
        if (jsonStr == null || jsonStr.equals("")) return null;

        try {
            clientToken.parse(jsonStr);
            setClientToken(clientToken);
            Public.getDataMgr(context).saveClientToken(clientToken);
            FNNetworkLog.i(_TAG, "Device token will be expired in " + clientToken.expireIn);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return clientToken;
    }

    private UserToken parseUserToken(String jsonStr) {

        if (jsonStr == null || jsonStr.equals("")) return null;

        try {
            userToken.parse(jsonStr);
            setUserToken(userToken);
            Public.getDataMgr(context).saveUserToken(userToken);
            FNNetworkLog.i(_TAG, "UserProfile token will be expired in " + userToken.expireIn + ", UserId = " + userToken.userId);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return userToken;
    }

    private synchronized void refreshToken(final int tokenType, final onRefreshTokenListener onRefreshTokenListener) {
        String refreshToken;
        String sDate;
        String identifier;
        Date    date;
        String packageName = Utils.getPackageName(context);

        long expireIn = 0 ,expireTime=0;
        long curTime = System.currentTimeMillis();

        try {
            ApplicationInfo info = context.getPackageManager().getApplicationInfo(packageName, PackageManager.GET_META_DATA);
            identifier = info.metaData.getString("com.fusionnext.sdk.iot.API_KEY");
        } catch (Exception ignore) {
            identifier = null;
        }

        if (tokenType == REFRESH_TOKEN_TYPE_CLIENT) {
            refreshToken = clientToken.refreshToken;
            sDate = clientToken.expireIn;
            expireTime = clientToken.expireTime;
        } else {
            refreshToken = userToken.refreshToken;
            sDate = userToken.expireIn;
            expireTime = userToken.expireTime;
        }
        Date curDate = new Date(curTime);
        if (sDate == null || sDate.equals("")) {
            return;
        }
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");

        try {
            date = dateFormat.parse(sDate);
            expireIn = date.getTime();
        } catch (ParseException e) {
            e.printStackTrace();
        }

        if (refreshToken.equals("")) {
            return;
        }

        //don't need to refresh token from server, because the token is not expired yet.
        //提前1分鐘refresh token

        if (expireTime != 0 && curTime < (expireTime ))
        {
            FNNetworkLog.i(_TAG, "Access token is not expired yet, expire time = " + sDate);
            return;
        }

        if (curTime < (expireIn - 60000)) {
            FNNetworkLog.i(_TAG, "Access token is not expired yet, expire time = " + sDate);
            return;
        }

        //setAccountType(accountInfo.accountType, true);
        HttpPost.PostParam[] params = new HttpPost.PostParam[1];

        params[0] = new HttpPost.PostParam();
        params[0].url = HttpData.generateHttpPostUrl(HttpData._POST_TYPE_AUTH_REFRESH_CLIENT_TOKEN);
        params[0].type = HttpData._POST_TYPE_AUTH_REFRESH_CLIENT_TOKEN;
        params[0].param = new HashMap<>();

        params[0].param.put("grant_type", "refresh_token");
        params[0].param.put("client_id", identifier);
        params[0].param.put("refresh_token", refreshToken);

        HttpPost post = new HttpPost();
        post.setContext(context);
        //HttpPost.PostResult result = post.httpPostSync(params);
        post.setOnParseJsonListener(new HttpPost.OnParseJsonListener() {
            @Override
            public void onParse(boolean returnResult, int postType, String id, String returnStr) {
                String token = null;
                if (returnResult) {
                    switch (tokenType) {
                        case REFRESH_TOKEN_TYPE_CLIENT:
                            if (returnStr != null) {
                                token = parseClientToken(returnStr).accessToken;
                            }
                            break;
                        case REFRESH_TOKEN_TYPE_USER:
                            if (returnStr != null) {
                                token = parseUserToken(returnStr).accessToken;
                            }
                            break;
                        default:
                            break;
                    }
                }
                onRefreshTokenListener.onRefreshToken(returnResult, token);
            }
        });
        try {
            post.execute(params).get();
        } catch (ExecutionException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public boolean login(String account, String password, final PushToken pushToken, final IOTHandler.OnAuthorizationListener onAuthorizationListener) {
        this.onAuthorizationListener = onAuthorizationListener;
        if (account != null && password != null) {
            getUserTokenByEmail(account, password, pushToken, onAuthorizationListener);
            return true;
        }
        return false;
    }

    public void logout(final IOTHandler.OnAuthorizationListener onAuthorizationListener) {
        this.onAuthorizationListener = onAuthorizationListener;
        Devices devices = new Devices(context);
        devices.deleteDevice(onDevicesListener);
        clearLoginInfo();
    }


    private void getUserTokenByEmail(final String account, String password, final PushToken pushToken, final IOTHandler.OnAuthorizationListener onAuthorizationListener) {
        String packageName = Utils.getPackageName(context);
        String identifier;

        if (pushToken != null && userToken != null) {
            userToken.pushToken = pushToken._pushToken;
        }

        try {
            ApplicationInfo info = context.getPackageManager().getApplicationInfo(packageName, PackageManager.GET_META_DATA);
            identifier = info.metaData.getString("com.fusionnext.sdk.iot.API_KEY");
        } catch (Exception ignore) {
            identifier = null;
        }

        HttpPost.PostParam[] params = new HttpPost.PostParam[1];
        params[0] = new HttpPost.PostParam();
        params[0].accessToken = Public.getAuthorization(context).getClientAccessToken();
        params[0].url = HttpData.generateHttpPostUrl(HttpData._POST_TYPE_AUTH_LOGIN_BY_EMAIL);
        params[0].type = HttpData._POST_TYPE_AUTH_LOGIN_BY_EMAIL;
        params[0].param = new HashMap<>();

        params[0].param.put("grant_type", "password");
        params[0].param.put("client_id", identifier);
        if (account != null && account.length() > 0) {
            params[0].param.put("username", account);
        }
        if (password != null && password.length() > 0) {
            params[0].param.put("password", password);
        }

        HttpPost post = new HttpPost();
        post.setContext(context);
        post.setOnParseJsonListener(new HttpPost.OnParseJsonListener() {
            @Override
            public void onParse(boolean returnResult, int postType, String id, String returnStr) {
                if (returnStr != null && returnResult) {
                    UserToken userToken = parseUserToken(returnStr);
                    UserProfile userProfile = Public.getDataMgr(context).loadUserProfile();
                    userProfile._userId = userToken.userId;
                    userProfile._email = account;
                    Public.getDataMgr(context).saveUserProfile(userProfile);
                    Devices devices = new Devices(context);
                    devices.createDevice(pushToken, onDevicesListener);
                } else {
                    if (onAuthorizationListener != null) {
                        ErrorObj errorObj = null;
                        if (returnStr != null) {
                            errorObj = ErrorObj.parseAuthErrorObj(returnStr);
                        }
                        onAuthorizationListener.onLogin(false, errorObj);
                    }
                }

            }
        });
        post.execute(params);
    }

    private void clearLoginInfo() {
        userToken.accessToken = "";
        userToken.expireIn = "";
        userToken.refreshToken = "";
        userToken.clientId = "";
        userToken.userId = "";

        Public.getDataMgr(context).saveUserToken(userToken);
    }

    private final IOTHandler.OnDevicesListener onDevicesListener = new IOTHandler.OnDevicesListener() {
        @Override
        public void onResult(boolean result, String id, int postType, ErrorObj error) {

        }

        @Override
        public void onCreateDevice(boolean result, Device device, ErrorObj error) {
            if (onAuthorizationListener != null) {
                onAuthorizationListener.onLogin(result, error);
            }
        }

        @Override
        public void onUpdateDevice(boolean result, Device device, ErrorObj error) {

        }

        @Override
        public void onDeleteDevice(boolean result, ErrorObj error) {
            if (onAuthorizationListener != null) {
                onAuthorizationListener.onLogout(result, error);
            }
        }
    };

    private final IOTHandler.OnPushTokensListener onPushTokensListener = new IOTHandler.OnPushTokensListener() {
        @Override
        public void onResult(boolean result, String id, int postType, ErrorObj error) {
            if (onAuthorizationListener != null) {
                onAuthorizationListener.onLogin(result, error);
            }
        }
    };
}
