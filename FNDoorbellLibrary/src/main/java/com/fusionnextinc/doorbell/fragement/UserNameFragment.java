package com.fusionnextinc.doorbell.fragement;

import android.content.Context;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;

import com.fusionnextinc.doorbell.MyApplication;
import com.fusionnextinc.doorbell.R;
import com.fusionnextinc.doorbell.manager.account.AaccountHandler;
import com.fusionnextinc.doorbell.manager.account.AccountManager;
import com.fusionnextinc.doorbell.objects.FNError;
import com.fusionnextinc.doorbell.utils.Utils;
import com.fusionnextinc.doorbell.widget.FNDialog;
import com.fusionnextinc.doorbell.widget.FNLayoutUtil;
import com.fusionnextinc.doorbell.widget.FNProgressBarCustom;

/**
 * Created by Mike Chang on 2017/6/22
 */
public class UserNameFragment extends FNFragment implements View.OnClickListener {
    private static final String TAG = UserNameFragment.class.getSimpleName();

    private FNLayoutUtil layoutUtil;
    private EditText editNewName;
    private FNProgressBarCustom progressBar;
    private AccountManager accountManager;

    public static void openFragment(boolean clearOtherStack) {
        FNFragmentManager.getInstance().openFragment(new UserNameFragment(), clearOtherStack);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        layoutUtil = new FNLayoutUtil(getActivity(), MyApplication.LAYOUT_WIDTH, MyApplication.LAYOUT_HEIGHT, FNLayoutUtil.BASE_WIDTH);
        accountManager = AccountManager.getInstance(getActivity());
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        final ViewGroup nullParent = null;

        getActivity().setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        getActionBar().resetAll();
        getActionBar().setTitle(getString(R.string.fn_tittle_settings_account_name), Gravity.CENTER, null);
        getActionBar().setCoverMode(false);
        getActionBar().setStartImage(R.drawable.back, new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        getActionBar().show();
        View view = inflater.inflate(R.layout.fragment_user_name, nullParent);
        layoutUtil.fitAllView(view);

        editNewName = (EditText) view.findViewById(R.id.et_user_name);
        Button btnDone = (Button) view.findViewById(R.id.btn_user_name_done);
        progressBar = new FNProgressBarCustom(getActivity(), (ViewGroup) view.findViewById(R.id.fragment_user_name));

        btnDone.setOnClickListener(this);
        return view;
    }

    @Override
    public void onClick(View view) {
        if (view.getId() == R.id.btn_user_name_done) {
            String name = editNewName.getText().toString().trim();

            if (!Utils.isNetworkConnected(getActivity())) {
                FNDialog.showNoNetworkDialog(getActivity());
                return;
            }
            // must be a check for not null, for empty field and for email validity(matchs email pattern)
            if (Utils.emptyFieldsRemaining(editNewName)) {
                FNDialog.showErrorDialog(getActivity(), getString(R.string.fn_tittle_settings_account_name), getString(R.string.fn_msg_login_fields_are_miss));
                return;
            }

            //mLogin.loginWithEmail(((EditText) findViewById(R.id.et_email)).getText().toString(),((EditText) findViewById(R.id.et_password)).getText().toString());
            if (name.length() > 0) {
                startWait();
                accountManager.setAccountListener(accountListener);
                accountManager.changUserName(name);
            }
        }
    }

    private void startWait() {
        progressBar.show();
    }


    private void stopWait() {
        progressBar.hide();
    }

    final AaccountHandler.AccountListener accountListener = new AaccountHandler.AccountListener() {
        @Override
        public void onUserRegistered(AccountManager instance, boolean result, FNError error) {

        }

        @Override
        public void onUserLogined(AccountManager instance, boolean result, FNError error) {

        }

        @Override
        public void onGetUserInfo(AccountManager instance, boolean result, FNError error) {

        }

        @Override
        public void onUserLogout(AccountManager instance, boolean result, FNError error) {

        }

        @Override
        public void onResetPassword(AccountManager instance, boolean result, FNError error) {

        }

        @Override
        public void onSetPassword(AccountManager instance, boolean result, FNError error) {

        }

        @Override
        public void onUpdateUserInfo(AccountManager instance, boolean result, FNError error) {
            if (isAdded()) {
                stopWait();
                closeKeyboard();
                if (result) {
                    FNDialog.showSuccessDialog(getActivity(), getString(R.string.fn_tittle_settings_account_name), getString(R.string.fn_msg_set_successfully));
                    onBackPressed();
                } else {
                    FNDialog.showErrorDialog(getActivity(), getString(R.string.fn_msg_set_failed), error);
                }
            }
        }
    };

    private void closeKeyboard() {
        if (getActivity().getCurrentFocus() != null && getActivity().getCurrentFocus().getWindowToken() != null) {
            InputMethodManager inputManager = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
            inputManager.hideSoftInputFromWindow(getActivity().getCurrentFocus().getWindowToken(), 0);
        }
    }
}
