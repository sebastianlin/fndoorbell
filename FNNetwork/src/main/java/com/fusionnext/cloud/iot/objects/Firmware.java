//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.fusionnext.cloud.iot.objects;

import android.util.Log;

import com.fusionnext.cloud.datalink.json.FNJsonObject;
import com.fusionnext.cloud.iot.Public;
import com.fusionnext.cloud.utils.Times;

import java.util.TimeZone;

public class Firmware {
    public String _fwId = "";
    public String _name = "";
    public String _version = "";
    public String _description = "";
    public String _filename = "";
    public String _downloadUrl = "";
    public String _createdAt = "";
    public String _updatedAt = "";

    public Firmware() {
    }

    public void clean() {
        this._fwId = "";
        this._name = "";
        this._version = "";
        this._description = "";
        this._filename = "";
        this._downloadUrl = "";
        this._createdAt = "";
        this._updatedAt = "";
    }

    public boolean getDataFromJSONObject(FNJsonObject firmwareObj) {
        this.clean();
        if (firmwareObj == null) {
            Log.w("Ota", " No JSON information");
            return false;
        } else {
            try {
                this._fwId = firmwareObj.getStringDefault("id");
                this._name = firmwareObj.getStringDefault("name");
                this._version = firmwareObj.getStringDefault("version");
                this._description = firmwareObj.getStringDefault("description");
                this._filename = firmwareObj.getStringDefault("filename");
                this._downloadUrl = Public._HOST_V1() + firmwareObj.getStringDefault("download_url");
                this._createdAt = firmwareObj.getStringDefault("created_at");
                _createdAt = Times.converTime(_createdAt, TimeZone.getDefault());
                this._updatedAt = firmwareObj.getStringDefault("updated_at");
                _updatedAt = Times.converTime(_updatedAt, TimeZone.getDefault());
                return true;
            } catch (Exception e) {
                e.printStackTrace();
                return false;
            }
        }
    }
}
