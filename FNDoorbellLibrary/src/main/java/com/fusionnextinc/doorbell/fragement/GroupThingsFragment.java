package com.fusionnextinc.doorbell.fragement;

import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;

import com.fusionnext.cloud.iot.objects.THINGSTATUS;
import com.fusionnextinc.doorbell.Adapter.GroupThingsListAdapter;
import com.fusionnextinc.doorbell.DefaultAppConfig;
import com.fusionnextinc.doorbell.MyApplication;
import com.fusionnextinc.doorbell.R;
import com.fusionnextinc.doorbell.manager.data.AppDataBase;
import com.fusionnextinc.doorbell.manager.data.AppDataGroup;
import com.fusionnextinc.doorbell.manager.data.AppDataThing;
import com.fusionnextinc.doorbell.manager.data.ListMap;
import com.fusionnextinc.doorbell.manager.thingsManager.ThingsHandler;
import com.fusionnextinc.doorbell.manager.thingsManager.ThingsManager;
import com.fusionnextinc.doorbell.objects.FNThing;
import com.fusionnextinc.doorbell.utils.FNLog;
import com.fusionnextinc.doorbell.widget.FNLayoutUtil;

/**
 * Created by Mike Chang on 2017/6/22
 */
public class GroupThingsFragment extends FNFragment implements View.OnClickListener, AbsListView.OnItemClickListener {
    private static final String TAG = GroupThingsFragment.class.getSimpleName();

    private FNLayoutUtil mLayoutUtil;
    private AppDataThing mAppDataThing;
    private GroupThingsListAdapter mThingsListAdapter = null;
    private ListView mThingsListView;
    private int mGroupId;
    private ThingsManager mThingsManager;

    public static void openFragment(boolean clearOtherStack, int groupId) {
        GroupThingsFragment thingsFragment = new GroupThingsFragment();
        thingsFragment.mGroupId = groupId;
        FNFragmentManager.getInstance().openFragment(thingsFragment, clearOtherStack);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mLayoutUtil = new FNLayoutUtil(getActivity(), MyApplication.LAYOUT_WIDTH, MyApplication.LAYOUT_HEIGHT, FNLayoutUtil.BASE_WIDTH);
        mAppDataThing = AppDataThing.getInstance(getActivity());
        mThingsManager = ThingsManager.getInstance(getActivity());
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        final ViewGroup nullParent = null;
        String groupName = (String) AppDataGroup.getInstance(getActivity()).getGroupData(mGroupId).get(AppDataGroup.TAG_GROUP_NAME);

        getActivity().setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        getActionBar().resetAll();
        getActionBar().setTitle(groupName, Gravity.CENTER, null);
        getActionBar().setCoverMode(false);
        getActionBar().setStartImage(R.drawable.back, new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        getActionBar().addEndImage(R.drawable.btn_add_thing, onAddThingClickListener);
        getActionBar().show();
        View view = inflater.inflate(R.layout.fragment_group_things, nullParent);
        mLayoutUtil.fitAllView(view);
        mThingsListView = view.findViewById(R.id.things_list);
        mThingsListView.setOnScrollListener(onScrollListener);
        mThingsListView.setOnItemClickListener(this);
        initThingsList();
        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        if (DefaultAppConfig.MQTT_ENABLE) {
            mThingsManager.registerMqttListener(mMqttListener);
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mThingsManager.unregisterMqttListener();
    }

    @Override
    public void onClick(View view) {

    }

    private View.OnClickListener onAddThingClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            RoomSettingFragment.openFragment(false, RoomSettingFragment.ROOMS_FRAGMENT_TYPE_EDIT, mGroupId);
        }
    };


    private boolean initThingsList() {
        if (mThingsListView == null) return false;
        ListMap groupOwnedThingsList = new ListMap();
        ListMap groupSharedThingsList = new ListMap();

        groupOwnedThingsList.addAll(mAppDataThing.getAdapterData(AppDataBase.THING_TYPE_THINGS_GROUP_OWNED, String.valueOf(mGroupId)));
        groupSharedThingsList.addAll(mAppDataThing.getAdapterData(AppDataBase.THING_TYPE_THINGS_GROUP_SHARED, String.valueOf(mGroupId)));
        //Setup the adapter
        mThingsListAdapter = new GroupThingsListAdapter(getActivity(), groupOwnedThingsList, groupSharedThingsList, R.layout.adapter_things_list);

        mThingsListView.setAdapter(mThingsListAdapter);
        //Ready the item click listener
        AdapterView.OnItemClickListener itemListener = new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                TextView textThingId = view.findViewById(R.id.textThingId);
                TextView textThingType = view.findViewById(R.id.textThingType);

                if (textThingId == null || textThingType == null || i < 0) {
                    FNLog.d(TAG, "textThingId == null || textThingType == null || i < 0");
                } else {
                    int thingType;
                    final String thingId = textThingId.getText().toString();

                    try {
                        thingType = Integer.valueOf(textThingType.getText().toString());
                    } catch (NumberFormatException e) {
                        thingType = -1;
                    }
                    if (thingId.length() > 0) {
                        FNThing fnThing = AppDataThing.getInstance(getActivity()).getThing(thingId);
                        int productId = 0;
                        try {
                            productId = Integer.valueOf(fnThing.getProductId());
                        } catch (NumberFormatException e) {
                            e.printStackTrace();
                        }
                        if (productId == ThingsManager.PRODUCT_ID_PLUG) {
                            SmartPlugFragment.openFragment(false, fnThing, thingType);
                        } else {
                            EventsFragment.openFragment(false, fnThing, thingType);
                        }
                    }
                }
            }
        };

        mThingsListView.setOnItemClickListener(itemListener);
        return true;
    }

    private void updateOnlineStatus() {
        if (mThingsListAdapter != null) {
            mThingsListAdapter.updateAdapterData(AppDataThing.THING_TYPE_THINGS_OWNED, mGroupId);
            mThingsListAdapter.updateAdapterData(AppDataThing.THING_TYPE_THINGS_SHARED, mGroupId);
        }
    }

    @Override
    public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {

    }

    private AbsListView.OnScrollListener onScrollListener = new AbsListView.OnScrollListener() {
        @Override
        public void onScrollStateChanged(AbsListView view, int scrollState) {

            switch (scrollState) {
                case SCROLL_STATE_IDLE:
                    break;
                case SCROLL_STATE_FLING:
                    break;
            }
        }

        @Override
        public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {

        }
    };

    private ThingsHandler.mqttListener mMqttListener = new ThingsHandler.mqttListener() {
        @Override
        public void onMqttConnected() {

        }

        @Override
        public void onDeviceSwitchOn(ThingsManager instance, String thingId, boolean isTurnOn) {

        }

        @Override
        public void onDeviceOnLine(ThingsManager instance, String thingId, THINGSTATUS isOnline) {
            updateOnlineStatus();
        }
    };
}
