package com.fusionnextinc.doorbell.fragement;

import android.content.pm.ActivityInfo;
import android.content.res.AssetFileDescriptor;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.DecelerateInterpolator;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.fusionnextinc.doorbell.MyApplication;
import com.fusionnextinc.doorbell.R;
import com.fusionnextinc.doorbell.manager.thingsManager.ThingsManager;
import com.fusionnextinc.doorbell.utils.FNLog;
import com.fusionnextinc.doorbell.widget.FNLayoutUtil;

import java.io.IOException;

/**
 * Created by Mike Chang on 2017/7/10
 */
public class NotificationFragment extends FNFragment implements View.OnClickListener {
    private static final String TAG = NotificationFragment.class.getSimpleName();

    private FNLayoutUtil layoutUtil;
    private RelativeLayout imgNotification;
    private static String thingId;
    private static String notificationTitle;
    private static String notificationSubtitle;
    private static String notificationSound;
    private static MediaPlayer mediaPlayer;
    private final int ANIMATION_FLICK_INTERVAL = 500;
    private static int ringtoneRepeatTimes = 0;
    private final int RINGTONE_PLAY_REPEAT_TIMES = 2;
    private ThingsManager thingsManager;

    public static void openFragment(String id, String title, String subtitle, String sound) {
        thingId = id;
        notificationTitle = title;
        notificationSubtitle = subtitle;
        notificationSound = sound;
        FNFragmentManager.getInstance().replaceFragment(new NotificationFragment());
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        thingsManager = ThingsManager.getInstance(getActivity());
        layoutUtil = new FNLayoutUtil(getActivity(), MyApplication.LAYOUT_WIDTH, MyApplication.LAYOUT_HEIGHT, FNLayoutUtil.BASE_WIDTH);

        if (mediaPlayer == null) {
            mediaPlayer = MediaPlayer.create(getActivity(), ThingsManager.getInstance(getActivity()).getRingtoneRawByFile(notificationSound));
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        final ViewGroup nullParent = null;

        getActivity().setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        getActionBar().hide();
        View view = inflater.inflate(R.layout.fragment_notification, nullParent);
        layoutUtil.fitAllView(view);

        imgNotification = (RelativeLayout) view.findViewById(R.id.img_notification);
        TextView txtTitle = (TextView) view.findViewById(R.id.txt_notifications_title);
        TextView txtSubtitle = (TextView) view.findViewById(R.id.txt_notifications_subtitle);
        LinearLayout viewNotification = (LinearLayout) view.findViewById(R.id.fragment_notification);

        viewNotification.setOnClickListener(this);

        if (notificationTitle != null) {
            txtTitle.setText(notificationTitle);
            notificationSubtitle = getString(R.string.fn_btn_someone_knock).replace("${NAME}",notificationTitle);
        }

        if (notificationSubtitle != null) {
            txtSubtitle.setText(notificationSubtitle);
        }

        AssetFileDescriptor afd = getActivity().getResources().openRawResourceFd(thingsManager.getRingtoneRawByFile(notificationSound));
        if (afd != null) {
            try {
                ringtoneRepeatTimes = 0;
                mediaPlayer.reset();
                mediaPlayer.setDataSource(afd.getFileDescriptor(), afd.getStartOffset(), afd.getLength());
                mediaPlayer.prepare();
                mediaPlayer.start();
                //監聽 播完執行--
                mediaPlayer.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
                    @Override
                    public void onCompletion(MediaPlayer mp) {
                        FNLog.d(TAG, "onCompletion ");
                        if (ringtoneRepeatTimes >= RINGTONE_PLAY_REPEAT_TIMES - 1) {
                            onBackPressed();
                        } else if (ringtoneRepeatTimes == 0){
                            mediaPlayer.start();
                        }
                        ringtoneRepeatTimes++;
                    }
                });
                afd.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        startAnimationFlick(imgNotification);
        return view;
    }

    @Override
    public void onClick(View view) {
        if (view.getId() == R.id.fragment_notification) {
            stopAnimationFlick(imgNotification);
            mediaPlayer.stop();
            onBackPressed();
        }
    }

    /**
     * 开启View闪烁效果
     */
    private void startAnimationFlick(View view) {
        if (null == view) {
            return;
        }
        Animation alphaAnimation = new AlphaAnimation(1, 0);
        alphaAnimation.setDuration(ANIMATION_FLICK_INTERVAL);
        alphaAnimation.setInterpolator(new DecelerateInterpolator());
        alphaAnimation.setRepeatCount(Animation.INFINITE);
        alphaAnimation.setRepeatMode(Animation.REVERSE);
        view.startAnimation(alphaAnimation);
    }

    /**
     * 取消View闪烁效果
     */
    private void stopAnimationFlick(View view) {
        if (null == view) {
            return;
        }
        view.clearAnimation();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        mediaPlayer.stop();
        ThingsFragment thingsFragment = new ThingsFragment();
        thingsFragment.setUpdateThing(true, thingId);
    }
}
