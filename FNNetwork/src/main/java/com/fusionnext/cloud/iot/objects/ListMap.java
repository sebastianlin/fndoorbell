package com.fusionnext.cloud.iot.objects;

import com.fusionnext.cloud.utils.Times;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * Created by Mike Chang on 2016/2/22.
 */
public class ListMap {

    private final ArrayList<Map<String, Object>> data = new ArrayList<Map<String, Object>>();

    public List<Map<String, Object>> getList() {
        synchronized (data) {
            return data;
        }
    }

    public Map<String, Object> get(int index) {
        synchronized (data) {
            if (index >= data.size()) return null;
            return data.get(index);
        }
    }

    public void update(int index, Map<String, Object> map) {
        synchronized (data) {
            if (index >= data.size()) return;
            data.set(index, map);
        }
    }

    public int size() {
        synchronized (data) {
            return data.size();
        }
    }

    public void clear() {
        synchronized (data) {
            data.clear();
        }
    }

    public void add(Map<String, Object> map) {
        synchronized (data) {
            data.add(map);
        }
    }

    public void addToHead(Map<String, Object> map) {
        synchronized (data) {
            data.add(0, map);
        }
    }

    public void addAll(ListMap map) {
        synchronized (data) {
            data.clear();
            if (map == null || map.size() <= 0) return;

            Collection<? extends Map<String, Object>> collection = map.data;
            data.addAll(collection);
        }
    }

    public void sort(final String key) {
        synchronized (data) {
            Collections.sort(data, new Comparator<Map<String, Object>>() {
                public int compare(Map<String, Object> o1, Map<String, Object> o2) {
                    return ((String) o1.get(key)).compareTo((String) o2.get(key));
                }
            });
        }
    }

    public void sortByTime(final String key) {
        synchronized (data) {
            Collections.sort(data, new Comparator<Map<String, Object>>() {
                public int compare(Map<String, Object> o1, Map<String, Object> o2) {
                    String sDate1 = (String) o1.get(key);
                    String sDate2 = (String) o2.get(key);
                    Date date1 = Times.parseTime(sDate1, Times._TIME_FORMAT_STANDARD);
                    Date date2 = Times.parseTime(sDate2, Times._TIME_FORMAT_STANDARD);

                    return date1.compareTo(date2);
                }
            });
        }
    }
}
