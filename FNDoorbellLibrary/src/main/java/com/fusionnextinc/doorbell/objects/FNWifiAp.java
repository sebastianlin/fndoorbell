package com.fusionnextinc.doorbell.objects;

import com.fusionnext.cloud.iot.objects.WifiAP;

import java.util.ArrayList;

/**
 * Created by Mike Chang on 2018/3/9
 */

public class FNWifiAp {
    protected final static String TAG = "FNWifiAp";
    private String mSsid;
    private String mSec;
    private int mDbm;

    public FNWifiAp() {
        mSsid = "";
        mSec = "";
        mDbm = 0;
    }

    public void clean() {
        mSsid = "";
        mSec = "";
        mDbm = 0;
    }

    public static ArrayList<FNWifiAp> parseWifiApArray(ArrayList<WifiAP> wifiApList) {
        ArrayList<FNWifiAp> fnWifiApList = new ArrayList<FNWifiAp>();

        if (wifiApList == null) {
            return null;
        }
        for (int i = 0; i < wifiApList.size(); i++) {
            WifiAP wifiAP = wifiApList.get(i);
            FNWifiAp fnWifiAp = new FNWifiAp();
            fnWifiAp.mSsid = wifiAP._ssid;
            fnWifiAp.mSec = wifiAP._sec;
            fnWifiAp.mDbm = wifiAP._dbm;

            fnWifiApList.add(fnWifiAp);
        }
        return fnWifiApList;
    }

    public String getSsid() {
        return mSsid;
    }

    public void setSsid(String mSsid) {
        this.mSsid = mSsid;
    }

    public String getSec() {
        return mSec;
    }

    public void setSec(String mSec) {
        this.mSec = mSec;
    }

    public int getDbm() {
        return mDbm;
    }

    public void setDbm(int mDbm) {
        this.mDbm = mDbm;
    }


}
