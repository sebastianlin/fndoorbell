package com.fusionnext.cloud.iot;

import android.content.Context;

import com.fusionnext.cloud.datalink.http.HttpDelete;
import com.fusionnext.cloud.datalink.http.HttpPost;
import com.fusionnext.cloud.datalink.http.HttpPut;
import com.fusionnext.cloud.iot.objects.Device;
import com.fusionnext.cloud.iot.objects.ErrorObj;
import com.fusionnext.cloud.iot.objects.PushToken;
import com.fusionnext.cloud.utils.Utils;

import java.util.HashMap;

/**
 * Created by Mike Chang on 2017/3/23
 */
public class Devices {
    protected final static String _TAG = "Devices";
    private Context context;
    private Device device = new Device();

    public Devices(Context context) {
        this.context = context;
        setDevice(Public.getDataMgr(context).loadDevice());
    }


    public void setDevice(Device device) {
        if (device == null) device = new Device();
        if (device.deviceId != null) this.device.deviceId = device.deviceId;
        if (device.clientId != null) this.device.clientId = device.clientId;
        if (device.userId != null) this.device.userId = device.userId;
        if (device.manufacturer != null) this.device.manufacturer = device.manufacturer;
        if (device.model != null) this.device.model = device.model;
        if (device.platform != null) this.device.platform = device.platform;
        if (device.platformVersion != null) this.device.platformVersion = device.platformVersion;
        if (device.createAt != null) this.device.createAt = device.createAt;
        if (device.updatedAt != null) this.device.updatedAt = device.updatedAt;
    }

    public void createDevice(PushToken pushToken, final IOTHandler.OnDevicesListener onDevicesListener) {
        String version = Utils.getAPIVerison();
        String model = Utils.getModelName();
        String manufacturer = Utils.getManufacturer(context);

        HttpPost.PostParam[] params = new HttpPost.PostParam[1];
        params[0] = new HttpPost.PostParam();
        params[0].accessToken = Public.getAuthorization(context).getUserAccessToken();
        params[0].url = HttpData.generateHttpPostUrl(HttpData._POST_TYPE_DEVICES_CREATE_DEVICE);
        params[0].type = HttpData._POST_TYPE_DEVICES_CREATE_DEVICE;
        params[0].param = new HashMap<>();
        params[0].param.put("uuid", Utils.getUUID(context, null));
        params[0].param.put("manufacturer", manufacturer);
        params[0].param.put("model", model);
        params[0].param.put("platform", "android");
        params[0].param.put("platform_ver", version);
        params[0].param.put("fcm_token", pushToken._pushToken);

        HttpPost post = new HttpPost();
        post.setContext(context);
        post.setOnParseJsonListener(new HttpPost.OnParseJsonListener() {
            @Override
            public void onParse(boolean returnResult, int postType, String id, String returnStr) {
                ErrorObj errorObj = null;
                if (returnStr != null) {
                    if (returnResult) {
                        Device device = Device.parseDevice(returnStr);
                        if (device != null) {
                            setDevice(device);
                            Public.getDataMgr(context).saveDevice(device);
                        }
                    } else {
                        errorObj = ErrorObj.parseErrorObj(returnStr);
                    }
                }
                onDevicesListener.onCreateDevice(returnResult, device, errorObj);
            }
        });
        post.execute(params);
    }

    protected void updateDevice(Device device, final IOTHandler.OnDevicesListener onDevicesListener) {
        if (device == null || device.deviceId == null) {
            return;
        }

        HttpPut.PutParam[] params = new HttpPut.PutParam[1];
        params[0] = new HttpPut.PutParam();
        params[0].accessToken = Public.getAuthorization(context).getClientAccessToken();
        params[0].url = HttpData.generateHttpPutUrl(HttpData._PUT_TYPE_DEVICES_UPDATE_DEVICE, device.deviceId);
        params[0].type = HttpData._PUT_TYPE_DEVICES_UPDATE_DEVICE;
        params[0].param = new HashMap<>();

        if (device.deviceName != null) {
            params[0].param.put("model", device.deviceName);
        }
        if (device.platformVersion != null) {
            params[0].param.put("platform_ver", device.platformVersion);
        }
        if (device.pushToken != null) {
            params[0].param.put("push_token", device.pushToken);
        }

        HttpPut put = new HttpPut();
        put.setHttpRequestType("PUT");
        put.setOnParseJsonListener(new HttpPut.OnParseJsonListener() {
            @Override
            public void onParse(boolean returnResult, int putType, String returnStr) {
                Device device = null;
                ErrorObj errorObj = null;

                if (returnStr != null) {
                    if (returnResult) {
                        device = Device.parseDevice(returnStr);
                    } else {
                        errorObj = ErrorObj.parseErrorObj(returnStr);
                    }
                }
                if (returnResult) {
                    Utils.resetUUID(context, null);
                }
                onDevicesListener.onUpdateDevice(returnResult, device, errorObj);
            }
        });
        put.setContext(context);
        put.execute(params);
    }

    protected void deleteDevice(final IOTHandler.OnDevicesListener onDevicesListener) {
        if (device.deviceId == null) {
            return;
        }

        HttpDelete.DelParam[] params = new HttpDelete.DelParam[1];
        params[0] = new HttpDelete.DelParam();
        params[0].accessToken = Public.getAuthorization(context).getUserAccessToken();
        params[0].url = HttpData.generateHttpDeleteUrl(HttpData._DEL_TYPE_THINGS_DELETE_DEVICE, device.deviceId);
        params[0].type = HttpData._DEL_TYPE_THINGS_DELETE_DEVICE;
        params[0].param = new HashMap<>();

        HttpDelete del = new HttpDelete();
        del.setContext(context);
        del.setOnParseJsonListener(new HttpDelete.OnParseJsonListener() {
            @Override
            public void onParse(boolean returnResult, int delType, String returnStr) {
                ErrorObj errorObj = null;

                if (returnStr != null && !returnResult) {
                    errorObj = ErrorObj.parseErrorObj(returnStr);
                }
                onDevicesListener.onDeleteDevice(returnResult, errorObj);
            }
        });
        del.execute(params);
    }

    public String getDeviceId() {
        setDevice(Public.getDataMgr(context).loadDevice());
        if (device != null) {
            return device.deviceId;
        }
        return "";
    }
}
