package com.fusionnextinc.doorbell.widget;

import android.content.Context;
import androidx.annotation.Nullable;
import android.util.AttributeSet;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.fusionnextinc.doorbell.R;

public class FNActionBar extends LinearLayout {
    private static final String TAG = FNActionBar.class.getSimpleName();

    private TextView mTxtFirst, mTxtSecond, mTxtThird, mTxtEnd, mTxtHint;
    private ImageView mImgStart, mImgEndFirst, mImgEndSecond, mImgEndThird;
    private View mMainLayout;

    private boolean mTitleCenterHorizontal;
    private boolean mCoverMode = true;

    public FNActionBar(Context context) {
        super(context);
        initView(LayoutInflater.from(context).inflate(R.layout.ui_actionbar, this, true));
    }

    public FNActionBar(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        initView(LayoutInflater.from(context).inflate(R.layout.ui_actionbar, this, true));
    }

    public FNActionBar(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        initView(LayoutInflater.from(context).inflate(R.layout.ui_actionbar, this, true));
    }

    private void initView(View view) {
        mTxtFirst = view.findViewById(R.id.txt_first);
        mTxtSecond = view.findViewById(R.id.txt_second);
        mTxtThird = view.findViewById(R.id.txt_third);
        mTxtEnd = view.findViewById(R.id.txt_end);
        mTxtHint = view.findViewById(R.id.txt_hint);
        mImgStart = view.findViewById(R.id.img_start);
        mImgEndFirst = view.findViewById(R.id.img_end_first);
        mImgEndSecond = view.findViewById(R.id.img_end_second);
        mImgEndThird = view.findViewById(R.id.img_end_third);
        resetAll();
    }

    /**
     * 必須設置 mainLayout，否則 {@link #setCoverMode(boolean)} 將不會生效
     *
     * @param mainLayout {@link FNActionBar} 下方之 layout
     */
    public void setMainLayout(View mainLayout) {
        mMainLayout = mainLayout;
        setCoverMode(mCoverMode);
    }

    /**
     * 重置 {@link FNActionBar} 內容
     */
    public void resetAll() {
        mTitleCenterHorizontal = false;
        mTxtFirst.setVisibility(GONE);
        mTxtSecond.setVisibility(GONE);
        mTxtThird.setVisibility(GONE);
        mTxtEnd.setVisibility(GONE);
        mTxtHint.setVisibility(GONE);
        mImgStart.setVisibility(GONE);
        mImgEndFirst.setVisibility(GONE);
        mImgEndSecond.setVisibility(GONE);
        mImgEndThird.setVisibility(GONE);
    }

    /**
     * 設置 {@link FNActionBar} Start 端圖示以及點擊監聽器
     *
     * @param resId    圖片資源 id
     * @param listener 點擊事件監聽器
     */
    public void setStartImage(int resId, OnClickListener listener) {
        mImgStart.setImageResource(resId);
        mImgStart.setOnClickListener(listener);
        mImgStart.setVisibility(VISIBLE);
        refreshAllView();
    }

    /**
     * 移除 {@link FNActionBar} Start 端圖示
     */
    public void removeStartImage() {
        mImgStart.setVisibility(GONE);
        refreshAllView();
    }

    /**
     * 增加 {@link FNActionBar} End 端圖示以及點擊事件監聽器
     *
     * @param resId    圖片資源 id
     * @param listener 點擊事件監聽器
     */
    public void addEndImage(int resId, OnClickListener listener) {
        if (mImgEndFirst.getVisibility() != VISIBLE) {
            mImgEndFirst.setImageResource(resId);
            mImgEndFirst.setOnClickListener(listener);
            mImgEndFirst.setVisibility(VISIBLE);
            refreshAllView();
        } else if (mImgEndSecond.getVisibility() != VISIBLE) {
            mImgEndSecond.setImageResource(resId);
            mImgEndSecond.setOnClickListener(listener);
            mImgEndSecond.setVisibility(VISIBLE);
        } else if (mImgEndThird.getVisibility() != VISIBLE) {
            mImgEndThird.setImageResource(resId);
            mImgEndThird.setOnClickListener(listener);
            mImgEndThird.setVisibility(VISIBLE);
        } else {
            Log.w(TAG, "Need implement ui_actionbar.xml if you want add more than 3 images.");
        }
    }

    /**
     * 移除所有 {@link FNActionBar} End 端圖示
     */
    public void removeAllEndImages() {
        mImgEndFirst.setVisibility(GONE);
        mImgEndSecond.setVisibility(GONE);
        mImgEndThird.setVisibility(GONE);
        refreshAllView();
    }

    /**
     * 設置 {@link FNActionBar} End 端文字以及點擊事件監聽器
     *
     * @param text     End 端文字
     * @param color    End 端文字顏色
     * @param listener 點擊事件監聽器
     */
    public void setEndText(String text, int color, OnClickListener listener) {
        mTxtEnd.setText(text);
        mTxtEnd.setTextColor(color);
        mTxtEnd.setOnClickListener(listener);
        mTxtEnd.setVisibility(VISIBLE);
        refreshAllView();
    }

    /**
     * 移除 {@link FNActionBar} End 端文字
     */
    public void removeEndText() {
        mTxtEnd.setVisibility(GONE);
        refreshAllView();
    }

    /**
     * 設置單行標題，並且使用預設文字顏色
     *
     * @param singleTitle 標題內容
     * @param gravity     對齊方向
     * @param listener    點擊事件監聽器
     */
    public void setTitle(String singleTitle, int gravity, OnClickListener listener) {
        setTitle(singleTitle, getResources().getColor(R.color.actionbar_title_msg), gravity, listener);
    }

    /**
     * 設置單行標題
     *
     * @param singleTitle 標題內容
     * @param color       文字顏色
     * @param gravity     對齊方向
     * @param listener    點擊事件監聽器
     */
    public void setTitle(String singleTitle, int color, int gravity, OnClickListener listener) {
        mTitleCenterHorizontal = (gravity & Gravity.CENTER_HORIZONTAL) == Gravity.CENTER_HORIZONTAL;
        mTxtSecond.setVisibility(GONE);
        mTxtThird.setVisibility(GONE);
        mTxtFirst.setText(singleTitle);
        mTxtFirst.setTextColor(color);
        mTxtFirst.setGravity(gravity);
        mTxtFirst.setOnClickListener(listener);
        mTxtFirst.setVisibility(VISIBLE);
        refreshAllView();
    }

    /**
     * 設置兩行標題，並且使用預設文字顏色
     *
     * @param firstTitle  第一行標題內容
     * @param secondTitle 第二行標題內容
     * @param gravity     對齊方向
     * @param listener    點擊事件監聽器
     */
    public void setTitle(String firstTitle, String secondTitle, int gravity, OnClickListener listener) {
        setTitle(firstTitle, secondTitle, getResources().getColor(R.color.actionbar_title_msg), gravity, listener);
    }

    /**
     * 設置兩行標題
     *
     * @param firstTitle  第一行標題內容
     * @param secondTitle 第二行標題內容
     * @param color       文字顏色
     * @param gravity     對齊方向
     * @param listener    點擊事件監聽器
     */
    public void setTitle(String firstTitle, String secondTitle, int color, int gravity, OnClickListener listener) {
        mTitleCenterHorizontal = (gravity & Gravity.CENTER_HORIZONTAL) == Gravity.CENTER_HORIZONTAL;
        mTxtFirst.setVisibility(GONE);
        mTxtSecond.setText(firstTitle);
        mTxtSecond.setTextColor(color);
        mTxtSecond.setGravity(gravity);
        mTxtSecond.setOnClickListener(listener);
        mTxtSecond.setVisibility(VISIBLE);
        mTxtThird.setText(secondTitle);
        mTxtThird.setTextColor(color);
        mTxtThird.setGravity(gravity);
        mTxtThird.setOnClickListener(listener);
        mTxtThird.setVisibility(VISIBLE);
        refreshAllView();
    }

    /**
     * 設置提示
     *
     * @param hint 提示內容
     */
    public void setHint(String hint) {
        mTxtHint.setText(hint);
        mTxtHint.setVisibility(VISIBLE);
    }

    /**
     * 移除提示
     */
    public void removeHint() {
        mTxtHint.setVisibility(GONE);
    }

    /**
     * 檢查 {@link FNActionBar} 是否顯示
     *
     * @return 是否顯示
     */
    public boolean isShowing() {
        return getVisibility() == VISIBLE;
    }

    /**
     * 顯示 {@link FNActionBar}
     */
    public void show() {
        setVisibility(VISIBLE);
    }

    /**
     * 隱藏 {@link FNActionBar}
     */
    public void hide() {
        setVisibility(GONE);
    }

    /**
     * 檢查 {@link FNActionBar} 是否蓋在 mainLayout 上方
     *
     * @return 是否蓋在 mainLayout 上方
     */
    public boolean isCoverMode() {
        return mCoverMode;
    }

    /**
     * 設置 {@link FNActionBar} 蓋在 mainLayout 上方，並且使用預設背景色
     *
     * @param isCoverMode 是否蓋在 mainLayout 上方
     */
    public void setCoverMode(boolean isCoverMode) {
        setCoverMode(isCoverMode, getResources().getColor(R.color.actionbar_bg));
    }

    /**
     * 設置 {@link FNActionBar} 蓋在 mainLayout 上方
     *
     * @param isCoverMode 是否蓋在 mainLayout 上方
     * @param color       背景顏色
     */
    public void setCoverMode(boolean isCoverMode, int color) {
        mCoverMode = isCoverMode;
        setBackgroundColor(color);
        if (mMainLayout != null) {
            RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams) mMainLayout.getLayoutParams();
            if (isCoverMode) {
                params.addRule(RelativeLayout.BELOW, 0);
                setAlpha(0.5f);
            } else {
                params.addRule(RelativeLayout.BELOW, R.id.actionbar);
                setAlpha(1.0f);
            }
            mMainLayout.setLayoutParams(params);
        }
    }

    /**
     * 解決 title 置中卻受兩旁 image 影響導致文字未正確置中問題
     */
    private void refreshAllView() {
        if (mTitleCenterHorizontal) {
            if (mImgStart.getVisibility() != VISIBLE) {
                mImgStart.setVisibility(mImgEndFirst.getVisibility() == VISIBLE || mTxtEnd.getVisibility() == VISIBLE ? INVISIBLE : GONE);
            }
            if (mImgEndFirst.getVisibility() != VISIBLE) {
                mImgEndFirst.setVisibility(mImgStart.getVisibility() == VISIBLE && mTxtEnd.getVisibility() == GONE ? INVISIBLE : GONE);
            }
        } else {
            if (mImgStart.getVisibility() == INVISIBLE) {
                mImgStart.setVisibility(GONE);
            }
            if (mImgEndFirst.getVisibility() == INVISIBLE) {
                mImgEndFirst.setVisibility(GONE);
            }
        }
    }
}
