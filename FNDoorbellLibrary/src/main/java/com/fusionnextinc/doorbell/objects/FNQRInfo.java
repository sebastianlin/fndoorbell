package com.fusionnextinc.doorbell.objects;

import android.util.Log;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by Mike Chang on 2017/8/8
 */
public class FNQRInfo {
    protected final static String TAG = "FNQRInfo";

    private final static String QRCODE_EMAIL = "email";

    public FNQRInfo() {
        email = "";
    }

    private String email;

    public void clean() {
        email = "";
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getJsonString() {
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put(QRCODE_EMAIL, email);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return jsonObject.toString();
    }

    public boolean getDataFromJSONObject(JSONObject qrCodeObj) {
        clean();
        if (qrCodeObj == null) {
            Log.w(TAG, " No JSON information");
            return false;
        }
        try {
            email = qrCodeObj.getString(QRCODE_EMAIL);
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
        return true;
    }

    public static FNQRInfo parseFNQRInfo(String json) {
        try {
            JSONObject fnQRInfoObject = new JSONObject(json);

            FNQRInfo fnQRInfo = new FNQRInfo();
            fnQRInfo.getDataFromJSONObject(fnQRInfoObject);
            return fnQRInfo;
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return null;
    }
}
