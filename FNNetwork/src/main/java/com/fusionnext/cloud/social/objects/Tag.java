package com.fusionnext.cloud.social.objects;

import android.util.Log;

import com.fusionnext.cloud.datalink.json.FNJsonArray;
import com.fusionnext.cloud.datalink.json.FNJsonObject;

import org.json.JSONException;

import java.util.ArrayList;

/**
 * Created by Mike Chang on 2017/3/22
 */
public class Tag {

    protected final static String TAG = "Tag";

    public final static String LIKES_STRING_TYPE_ID = "id";
    public final static String LIKES_STRING_TYPE_NAME = "name";
    public final static String LIKES_STRING_TYPE_CREATE_AT = "created_at";
    public final static String LIKES_STRING_TYPE_UPDATE_AT = "updated_at";

    public Tag() {
        _tagId = "";
        _name = "";
        _createdAt = "";
        _updatedAt = "";
    }

    public String _tagId;
    public String _name;
    public String _createdAt;
    public String _updatedAt;

    public void clean() {
        _tagId = "";
        _name = "";
        _createdAt = "";
        _updatedAt = "";
    }

    public boolean getDataFromJSONObject(FNJsonObject likeObj) {
        clean();
        if (likeObj == null) {
            Log.w(TAG, " No JSON information");
            return false;
        }
        try {
            _tagId = likeObj.getStringDefault(LIKES_STRING_TYPE_ID);
            _name = likeObj.getStringDefault(LIKES_STRING_TYPE_NAME);
            _createdAt = likeObj.getStringDefault(LIKES_STRING_TYPE_CREATE_AT);
            _updatedAt = likeObj.getStringDefault(LIKES_STRING_TYPE_UPDATE_AT);
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
        return true;
    }

    public static ArrayList<Tag> parseTagArray(String json) {
        try {
            ArrayList<Tag> tagList = new ArrayList<Tag>();
            FNJsonObject tagObject = new FNJsonObject(json);
            FNJsonArray tagsArray = tagObject.getJSONArray("items");
            if (tagsArray == null) {
                return null;
            }
            for (int i = 0; i < tagsArray.length(); i++) {
                FNJsonObject object = tagsArray.getJSONObject(i);
                Tag tag = new Tag();
                tag.getDataFromJSONObject(object);
                if (tag == null) return null;
                tagList.add(tag);
            }
            return tagList;
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return null;
    }
}
