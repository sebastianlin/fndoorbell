package com.fusionnextinc.doorbell.fragement;

import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.TextView;

import com.fusionnext.cloud.utils.Times;
import com.fusionnextinc.doorbell.Adapter.EventsListAdapter;
import com.fusionnextinc.doorbell.Adapter.ThingsListAdapter;
import com.fusionnextinc.doorbell.MyApplication;
import com.fusionnextinc.doorbell.R;
import com.fusionnextinc.doorbell.manager.data.AppDataHandler;
import com.fusionnextinc.doorbell.manager.data.AppDataThing;
import com.fusionnextinc.doorbell.manager.data.AppDataThingEvent;
import com.fusionnextinc.doorbell.manager.data.ListMap;
import com.fusionnextinc.doorbell.manager.thingsManager.ThingsHandler;
import com.fusionnextinc.doorbell.manager.thingsManager.ThingsManager;
import com.fusionnextinc.doorbell.objects.FNError;
import com.fusionnextinc.doorbell.objects.FNEvent;
import com.fusionnextinc.doorbell.objects.FNFirmware;
import com.fusionnextinc.doorbell.objects.FNSchedule;
import com.fusionnextinc.doorbell.objects.FNThing;
import com.fusionnextinc.doorbell.objects.FNUserProfile;
import com.fusionnextinc.doorbell.utils.FNLog;
import com.fusionnextinc.doorbell.utils.Utils;
import com.fusionnextinc.doorbell.widget.FNAlert.FNAlertDialog;
import com.fusionnextinc.doorbell.widget.FNDialog;
import com.fusionnextinc.doorbell.widget.FNHeaderListView.FNHeaderListView;
import com.fusionnextinc.doorbell.widget.FNLayoutUtil;
import com.fusionnextinc.doorbell.widget.FNProgressBarCustom;

import java.util.ArrayList;

/**
 * Created by Mike Chang on 2017/6/22
 */
public class EventsFragment extends FNFragment implements View.OnClickListener, AbsListView.OnItemClickListener {
    private static final String TAG = EventsFragment.class.getSimpleName();

    private FNLayoutUtil layoutUtil;
    private static FNProgressBarCustom progressBar;
    private ThingsManager thingsManager;
    private AppDataThingEvent appDataThingEvent;
    private AppDataThing appDataThing;
    private EventsListAdapter eventsListAdapter = null;
    private FNHeaderListView eventsListView;
    private TextView textEventEmpty;
    private static FNThing curFNThing;
    private static int curThingType;
    private static int THING_EVENT_TYPE = AppDataThingEvent.THING_TYPE_EVENTS_ALL;

    public static void openFragment(boolean clearOtherStack, FNThing fnThing, int thingType) {
        curFNThing = fnThing;
        curThingType = thingType;
        FNFragmentManager.getInstance().openFragment(new EventsFragment(), clearOtherStack);
        if (Integer.valueOf(curFNThing.getProductId()) == ThingsManager.PRODUCT_ID_DOORBELL) {
            THING_EVENT_TYPE = AppDataThingEvent.THING_TYPE_EVENTS_RING;
        } else if (Integer.valueOf(curFNThing.getProductId()) == ThingsManager.PRODUCT_ID_DOORBELL_VOICE) {
            THING_EVENT_TYPE = AppDataThingEvent.THING_TYPE_EVENTS_CALL;
        }
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        layoutUtil = new FNLayoutUtil(getActivity(), MyApplication.LAYOUT_WIDTH, MyApplication.LAYOUT_HEIGHT, FNLayoutUtil.BASE_WIDTH);
        thingsManager = ThingsManager.getInstance(getActivity());
        appDataThingEvent = AppDataThingEvent.getInstance(getActivity());
        appDataThingEvent.setAppDataThingEventListener(appDataThingEventListener);
        appDataThing = AppDataThing.getInstance(getActivity());
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        final ViewGroup nullParent = null;

        getActivity().setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        curFNThing = appDataThing.getThing(curFNThing.getThingId());
        getActionBar().resetAll();
        getActionBar().setTitle(curFNThing.getName(), Gravity.CENTER, null);
        getActionBar().setCoverMode(false);
        getActionBar().setStartImage(R.drawable.back, new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        if (curThingType == ThingsListAdapter.TYPE_OWNED_THINGS_LIST) {
            getActionBar().addEndImage(R.drawable.btn_thing_settings, onThingSettingsClickListener);
        } else if (curThingType == ThingsListAdapter.TYPE_SHARED_THINGS_LIST) {
            getActionBar().addEndImage(R.drawable.btn_thing_delete, onThingDeleteClickListener);
        }
        getActionBar().show();
        View view = inflater.inflate(R.layout.fragment_events, nullParent);
        layoutUtil.fitAllView(view);
        textEventEmpty = (TextView) view.findViewById(R.id.text_events_empty);
        eventsListView = (FNHeaderListView) view.findViewById(R.id.events_list);
        eventsListView.setOnScrollListener(onScrollListener);
        eventsListView.setOnItemClickListener(this);
        progressBar = new FNProgressBarCustom(getActivity(), (ViewGroup) view.findViewById(R.id.fragment_events));
        initEventsList(true);
        if (Utils.isNetworkConnected(getActivity())) {
            appDataThingEvent.loadEventsList(THING_EVENT_TYPE, curFNThing.getThingId(), true, true);
            thingsManager.updateThingReadTimeToDB(curFNThing.getThingId(), Times.nowTime(Times._TIME_FORMAT_STANDARD));
            thingsManager.updateThingEventCountsToDB(curFNThing.getThingId(), "0");
            startWait();
        } else {
            FNDialog.showNoNetworkDialog(getActivity());
        }
        return view;
    }

    @Override
    public void onClick(View view) {

    }

    private void startWait() {
        progressBar.show();
    }


    public static void stopWait() {
        if (progressBar != null) {
            progressBar.hide();
        }
    }

    private View.OnClickListener onThingSettingsClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            ThingSettingsFragment.openFragment(false, curFNThing);
        }
    };

    private View.OnClickListener onThingDeleteClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            if (Utils.isNetworkConnected(getActivity())) {
                new FNAlertDialog(getActivity(), FNAlertDialog.WARNING_TYPE)
                        .setTitleText(getActivity().getString(R.string.fn_dialog_are_you_sure))
                        .setContentText(getActivity().getString(ThingsListAdapter.TYPE_SHARED_THINGS_LIST != curThingType ? R.string.fn_dialog_delete_doorbell:R.string.fn_dialog_delete_share_doorbell))
                        .setCancelText(getActivity().getString(R.string.fn_dialog_cancel))
                        .setConfirmText(getActivity().getString(R.string.fn_btn_delete))
                        .showCancelButton(true)
                        .setCancelClickListener(new FNAlertDialog.OnSweetClickListener() {
                            @Override
                            public void onClick(FNAlertDialog sDialog) {
                                sDialog.dismiss();
                            }
                        })
                        .setConfirmClickListener(new FNAlertDialog.OnSweetClickListener() {
                            @Override
                            public void onClick(FNAlertDialog sDialog) {
                                startWait();
                                thingsManager.setThingsListener(thingsListener);
                                thingsManager.deleteThing(curFNThing.getThingId());
                                sDialog.dismiss();
                            }
                        })
                        .show();
            } else {
                FNDialog.showNoNetworkDialog(getActivity());
            }
        }
    };

    private boolean initEventsList(boolean isFootViewEnabled) {
        if (eventsListView == null) return false;

        if (eventsListAdapter == null) {
            ListMap thingEventsList = new ListMap();

            thingEventsList.addAll(appDataThingEvent.getAdapterData(THING_EVENT_TYPE, curFNThing.getThingId()));
            //Setup the adapter
            eventsListAdapter = new EventsListAdapter(getActivity(), thingEventsList, R.layout.adapter_events_list);
        }

        eventsListView.setAdapter(eventsListAdapter);
        eventsListView.setPullRefreshEnable(true);
        eventsListView.setPullLoadEnable(true);
        //Ready the item click listener
        AdapterView.OnItemClickListener itemListener = new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                //TextView textEventId = (TextView) view.findViewById(R.id.textEventId);
            }
        };
        eventsListView.setOnItemClickListener(itemListener);
        eventsListView.setPullListViewListener(new FNHeaderListView.IPullListViewListener() {

            @Override
            public void onRefresh() {
                if (Utils.isNetworkConnected(getActivity())) {
                    appDataThingEvent.refreshEventsListData(THING_EVENT_TYPE, curFNThing.getThingId());
                } else {
                    FNDialog.showNoNetworkDialog(getActivity());
                    eventsListView.setPullLoadEnable(false);
                    eventsListView.stopLoadMore();
                    eventsListView.stopRefresh();
                }
            }

            @Override
            public void onLoadMore() {
                FNLog.d("onLoadMoreData", "Click share footer bar");
                appDataThingEvent.getNextEventsPage(THING_EVENT_TYPE, curFNThing.getThingId());
            }
        });


        if (!isFootViewEnabled) {
            eventsListView.setPullLoadEnable(false);
        }

        return true;
    }

    @Override
    public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {

    }

    private AbsListView.OnScrollListener onScrollListener = new AbsListView.OnScrollListener() {
        @Override
        public void onScrollStateChanged(AbsListView view, int scrollState) {

            switch (scrollState) {
                case SCROLL_STATE_IDLE:
                    break;
                case SCROLL_STATE_FLING:
                    break;
            }
        }

        @Override
        public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {

        }
    };

    final AppDataHandler.AppDataThingEventListener appDataThingEventListener = new AppDataHandler.AppDataThingEventListener() {

        @Override
        public void onRefreshListAdapter(final int type, final String thingId, final ArrayList<FNEvent> eventList) {

            MyApplication.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    if (eventList != null && eventList.size() <= 0) {
                        textEventEmpty.setVisibility(View.VISIBLE);
                    } else {
                        textEventEmpty.setVisibility(View.GONE);
                    }

                    if (eventsListAdapter != null) {
                        eventsListAdapter.updateAdapterData(type, thingId);
                    }

                    if (eventsListView != null) {
                        boolean hasMoreData = appDataThingEvent.hasMoreData(type);
                        if (hasMoreData && eventsListAdapter != null && eventsListAdapter.getCount() == 0)
                            hasMoreData = false;
                        eventsListView.setPullLoadEnable(hasMoreData);
                        eventsListView.stopLoadMore();
                        eventsListView.stopRefresh();
                    }
                }
            });
        }
    };

    final ThingsHandler.ThingsListener thingsListener = new ThingsHandler.ThingsListener() {
        @Override
        public void onListUserOwnedThings(ThingsManager instance, boolean result, String userId, ArrayList<FNThing> thingArrayList, FNError error) {

        }

        @Override
        public void onListUserInvitedThings(ThingsManager instance, boolean result, String userId, ArrayList<FNThing> thingArrayList, FNError error) {

        }

        @Override
        public void onListUserSharedThings(ThingsManager instance, boolean result, String userId, ArrayList<FNThing> thingArrayList, FNError error) {

        }

        @Override
        public void onListUserAllThings(ThingsManager instance, boolean result, String userId, ArrayList<FNThing> ownedThingArrayList, ArrayList<FNThing> sharedThingArrayList, ArrayList<FNThing> invitedThingArrayList, FNError error) {

        }

        @Override
        public void onListThingEvents(ThingsManager instance, boolean result, String thingId, String eventName, ArrayList<FNEvent> eventList, FNError error) {

        }

        @Override
        public void onListThingEventsByTime(ThingsManager instance, boolean result, String thingId, String eventName, String startTime, String endTime, ArrayList<FNEvent> eventList, FNError error) {

        }

        @Override
        public void onCurBatteryLevel(ThingsManager instance, boolean result, String thingId, String batteryLevel, FNError error) {

        }

        @Override
        public void onCurFirmwareVersion(ThingsManager instance, boolean result, String thingId, String fwVersion, FNError error) {

        }

        @Override
        public void onCurSwitchedStatus(ThingsManager instance, boolean result, String thingId, boolean isSwitch, FNError error) {

        }

        @Override
        public void onNewFirmwareVersion(ThingsManager instance, boolean result, String thingId, FNFirmware fnFirmware) {

        }

        @Override
        public void onStartFwUpdate(ThingsManager instance, boolean result, String thingId, String fwId) {

        }

        @Override
        public void onCurRingTone(ThingsManager instance, boolean result, String thingId, String ringtoneId, FNError error) {

        }

        @Override
        public void onGetScheduleList(ThingsManager instance, boolean result, String thingId, ArrayList<FNSchedule> scheduleList, FNError error) {

        }

        @Override
        public void onListInvitedUsers(ThingsManager instance, boolean result, String thingId, ArrayList<FNUserProfile> userList, FNError error) {

        }

        @Override
        public void onListSharedUsers(ThingsManager instance, boolean result, String thingId, ArrayList<FNUserProfile> userList, FNError error) {

        }

        @Override
        public void onCreateClaimCode(ThingsManager instance, boolean result, String claimCode, FNError error) {

        }

        @Override
        public void onCreateThing(ThingsManager instance, boolean result, FNThing fnThing, FNError error) {

        }

        @Override
        public void onUpdateThing(ThingsManager instance, boolean result, FNThing fnThing, FNError error) {

        }

        @Override
        public void onDeleteThing(ThingsManager instance, boolean result, FNError error) {
            if (isAdded()) {
                stopWait();
                if (result) {
                    FNDialog.showSuccessDialog(getActivity(), curFNThing.getName(), getString((ThingsListAdapter.TYPE_SHARED_THINGS_LIST != curThingType?R.string.msg_delete_thing_successfully:R.string.msg_delete_share_thing_successfully)).replace("${Thing}", getActivity().getString(R.string.fn_title_thing_name)));
                    onBackPressed();
                    ThingsFragment thingsFragment = new ThingsFragment();
                    thingsFragment.setUpdateThing(true, null);
                } else {
                    FNDialog.showErrorDialog(getActivity(), getString(R.string.fn_msg_delete_thing_failed).replace("${Thing}", getActivity().getString(R.string.fn_title_thing_name)), error);
                }
            }
        }

        @Override
        public void onGetThing(ThingsManager instance, boolean result, FNThing fnThing, FNError error) {

        }

        @Override
        public void onChangeRingtone(ThingsManager instance, boolean result, String thingId, int ringtoneId) {

        }

        @Override
        public void onSetTimer(ThingsManager instance, boolean result, String thingId, String value) {

        }

        @Override
        public void onSetSchedule(ThingsManager instance, boolean result, String thingId, String value) {

        }

        @Override
        public void onShareInvited(ThingsManager instance, boolean result, String thingId, FNError error) {

        }

        @Override
        public void onShareAccepted(ThingsManager instance, boolean result, String thingId, FNError error) {

        }

        @Override
        public void onShareRejected(ThingsManager instance, boolean result, String thingId, FNError error) {

        }

        @Override
        public void onSwitchOnOff(ThingsManager instance, boolean result, String thingId, boolean isTurnOn) {

        }

        @Override
        public void onCloseCallSlot(ThingsManager instance, boolean result, FNError error) {

        }

        @Override
        public void onTakeCallSlot(ThingsManager instance, boolean result, String thingId, String callSlot, FNError error) {

        }
    };

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        eventsListAdapter = null;
    }

    @Override
    public void onBackPressed() {
        if (progressBar != null && !progressBar.isShown()) {
            super.onBackPressed(); // Comment this super call to avoid calling finish() or fragmentmanager's backstack pop operation.
            ThingsFragment thingsFragment = new ThingsFragment();
            thingsFragment.setUpdateThing(true, curFNThing.getThingId());
        }
    }
}
