package com.fusionnext.cloud.fusionapi;

import android.content.Context;

import com.fusionnext.cloud.datalink.http.HttpPost;
import com.fusionnext.cloud.utils.Utils;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;

/**
 * Created by Mike Chang on 2017/3/7
 */
public class Cameras {
    protected final static String _TAG = "Cameras";

    public static class Camera {
        //public String cameraId = "";
        //public String clientId = "";
        //public String deviceId = "";
        //public String cameraIdentifer = "";
        public String mac = "";
        public String manufacturer = "";
        public String model = "";
        public String platform = "";
        public String platformVersion = "";
        public String firmwareVersion = "";
        public HashMap<String, Object> info = new HashMap<String, Object>();
        //public String createAt = "";
        public String updatedAt = "";
    }

    private Context context;

    public Cameras(Context context) {
        this.context = context;
    }

    public void createCamera(Context context, Camera camera, final FusionAPIHandler.OnFusionAPIListener onFusionAPIListener) {
        if (camera == null) {
            return;
        }
        HttpPost.PostParam[] params = new HttpPost.PostParam[1];
        params[0] = new HttpPost.PostParam();
        params[0].accessToken = Public.getAuthorization(context).getDeviceAccessToken();
        params[0].url = HttpData.generateHttpPostUrl(HttpData._POST_TYPE_CREATE_CAMERA);
        params[0].type = HttpData._POST_TYPE_CREATE_CAMERA;

        params[0].param = new HashMap<Object, Object>();
        JSONObject infoObject = new JSONObject();
        for (String key : camera.info.keySet()) {
            try {
                infoObject.put(key, camera.info.get(key));
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

        params[0].param.put("identifier", Utils.getUUID(context, camera.mac));
        params[0].param.put("mac", camera.mac);
        params[0].param.put("manufacturer", camera.manufacturer);
        params[0].param.put("model", camera.model);
        params[0].param.put("platform", camera.platform);
        params[0].param.put("platform_ver", camera.platformVersion);
        params[0].param.put("firmware_ver", camera.firmwareVersion);
        params[0].param.put("info", infoObject.toString());

        HttpPost post = new HttpPost();
        post.setContext(context);
        post.setOnParseJsonListener(new HttpPost.OnParseJsonListener() {
            @Override
            public void onParse(boolean returnResult, int postType, String id, String returnStr) {
                onFusionAPIListener.onResult(returnResult, postType, returnStr);
            }
        });
        post.execute(params);
    }

    /*public void updateCamera(Context context, String cameraId, Camera camera, final FusionAPIHandler.OnFusionAPIListener onFusionAPIListener) {
        if (camera == null || cameraId == null) {
            return;
        }

        HttpPut.PutParam[] params = new HttpPut.PutParam[1];
        params[0] = new HttpPut.PutParam();
        params[0].accessToken = Public.getAuthorization(context).getDeviceAccessToken();
        params[0].url = HttpData.generateHttpPutUrl(HttpData._PUT_TYPE_UPDATE_CAMERA, cameraId);
        params[0].type = HttpData._PUT_TYPE_UPDATE_CAMERA;
        params[0].param = new HashMap<Object, Object>();

        if (camera.mac != null) {
            params[0].param.put("mac", camera.mac);
        }
        if (camera.firmwareVersion != null) {
            params[0].param.put("firmware_ver", camera.firmwareVersion);
        }
        if (camera.info != null) {
            params[0].param.put("info", camera.info);
        }

        HttpPut put = new HttpPut();
        put.setHttpRequestType("PUT");
        put.setOnParseJsonListener(new HttpPut.OnParseJsonListener() {
            @Override
            public void onParse(boolean returnResult, int putType, String returnStr) {
                onFusionAPIListener.onResult(returnResult, putType, returnStr);
            }
        });
        put.setContext(context);
        put.execute(params);
    }*/

}
