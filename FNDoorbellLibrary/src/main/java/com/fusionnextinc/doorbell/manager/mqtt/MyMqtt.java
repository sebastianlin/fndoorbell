package com.fusionnextinc.doorbell.manager.mqtt;

import android.content.Context;
import android.os.Handler;
import android.os.Message;
import android.util.Log;

import com.fusionnext.cloud.datalink.json.FNJsonObject;
import com.fusionnextinc.doorbell.BuildConfig;
import com.fusionnextinc.doorbell.utils.Utils;

import org.eclipse.paho.android.service.MqttAndroidClient;
import org.eclipse.paho.client.mqttv3.IMqttActionListener;
import org.eclipse.paho.client.mqttv3.IMqttDeliveryToken;
import org.eclipse.paho.client.mqttv3.IMqttToken;
import org.eclipse.paho.client.mqttv3.MqttCallbackExtended;
import org.eclipse.paho.client.mqttv3.MqttConnectOptions;
import org.eclipse.paho.client.mqttv3.MqttException;
import org.eclipse.paho.client.mqttv3.MqttMessage;

import java.util.UUID;

/**
 * Author：LvQingYang
 * Date：2017/8/29
 * Email：biloba12345@gamil.com
 * Github：https://github.com/biloba123
 * Info：MQTT操作类
 */

public class MyMqtt {
    private String TAG = "MyMqtt";
    /**
     * MQTT配置参数
     **/
    private static String protocol = "tcp";
    private static String host = "broker.fusionnextinc.com";
    private static String port = "1883";
    private static String userID = "";
    private static String passWord = "";
    private static String clientID = UUID.randomUUID().toString();

    public static final String TOPIC_SWITCH_ONOFF = "ptc/ptc-switch/${thing_identifier}/events/switched";
    public static final String TOPIC_SWITCH_ONLINE = "ptc/ptc-switch/${thing_identifier}/online";


    /**
     * MQTT状态信息
     **/
    private static final int MQTT_CONNECT_STATUS_IDLE = 0;
    private static final int MQTT_CONNECT_STATUS_CONNECTED = 1;
    private static final int MQTT_CONNECT_STATUS_CONNECTION_LOST = 2;
    private static final int MQTT_CONNECT_STATUS_CONNECTION_FAIL = 3;
    private static int isConnect = MQTT_CONNECT_STATUS_IDLE;
    /**
     * MQTT支持类
     **/
    private MqttAndroidClient mqttClient = null;

    private MqttListener mMqttListener;
    private Context mContext;

    private Handler mHandler = new Handler(new Handler.Callback() {
        @Override
        public boolean handleMessage(Message message) {
            switch (message.arg1) {
                case MqttTag.MQTT_STATE_CONNECTED:
                    if (BuildConfig.DEBUG) Log.d(TAG, "handleMessage: connected");
                    mMqttListener.onConnected();
                    break;
                case MqttTag.MQTT_STATE_FAIL:
//                    if (BuildConfig.DEBUG) Log.d(TAG, "handleMessage: fail");
                    if (Utils.isNetworkConnected(mContext)) {
                        reStartMqtt();
                    }
                    mMqttListener.onFail();
                    break;
                case MqttTag.MQTT_STATE_LOST:
                    if (BuildConfig.DEBUG) Log.d(TAG, "handleMessage: lost");
                    mMqttListener.onLost();
                    break;
                case MqttTag.MQTT_STATE_RECEIVE:
                    if (BuildConfig.DEBUG) Log.d(TAG, "handleMessage: receive");
                    mMqttListener.onReceive((String) message.obj);
                    break;
                case MqttTag.MQTT_STATE_SEND_SUCC:
                    if (BuildConfig.DEBUG) Log.d(TAG, "handleMessage: send");
                    mMqttListener.onSendSucc();
                    break;
            }
            return true;
        }
    });

    /**
     * 自带的监听类，判断Mqtt活动变化
     */
    private IMqttActionListener mIMqttActionListener = new IMqttActionListener() {
        @Override
        public void onSuccess(IMqttToken asyncActionToken) {
            isConnect = MQTT_CONNECT_STATUS_CONNECTED;
            Message msg = new Message();
            msg.arg1 = MqttTag.MQTT_STATE_CONNECTED;
            mHandler.sendMessage(msg);
        }

        @Override
        public void onFailure(IMqttToken asyncActionToken, Throwable exception) {
            isConnect = MQTT_CONNECT_STATUS_CONNECTION_FAIL;
            Message msg = new Message();
            msg.arg1 = MqttTag.MQTT_STATE_FAIL;
            mHandler.sendMessage(msg);
        }
    };

    /**
     * 自带的监听回传类
     */
    private MqttCallbackExtended mMqttCallback = new MqttCallbackExtended() {
        @Override
        public void connectComplete(boolean reconnect, String serverURI) {
            isConnect = MQTT_CONNECT_STATUS_CONNECTED;
            Message msg = new Message();
            msg.arg1 = MqttTag.MQTT_STATE_CONNECTED;
            mHandler.sendMessage(msg);
        }

        @Override
        public void connectionLost(Throwable cause) {
            isConnect = MQTT_CONNECT_STATUS_CONNECTION_LOST;
            Message msg = new Message();
            msg.arg1 = MqttTag.MQTT_STATE_LOST;
            mHandler.sendMessage(msg);
        }

        @Override
        public void messageArrived(String topic, MqttMessage message) throws Exception {
            Message msg = new Message();
            FNJsonObject jsonObject = new FNJsonObject();
            jsonObject.put("topic", topic);
            jsonObject.put("payload", new String(message.getPayload()));
            msg.arg1 = MqttTag.MQTT_STATE_RECEIVE;
            msg.obj = jsonObject.toString();
            mHandler.sendMessage(msg);
        }

        @Override
        public void deliveryComplete(IMqttDeliveryToken token) {
            Message msg = new Message();
            msg.arg1 = MqttTag.MQTT_STATE_SEND_SUCC;
            mHandler.sendMessage(msg);
        }
    };

    public MyMqtt(Context context, MqttListener lis) {
        mContext = context;
        mMqttListener = lis;
    }

    public static void setMqttSetting(String protocol, String host, String port, String userID, String passWord, String clientID) {
        MyMqtt.protocol = protocol;
        MyMqtt.host = host;
        MyMqtt.port = port;
        MyMqtt.userID = userID;
        MyMqtt.passWord = passWord;
        MyMqtt.clientID = clientID;
    }

    /**
     * 进行Mqtt连接
     */
    public void connectMqtt() {
        if (mqttClient == null) {
            mqttClient = new MqttAndroidClient(mContext, protocol + "://" + this.host + ":" + this.port, this.clientID);
            mqttClient.setCallback(mMqttCallback);
        }

        try {
            mqttClient.connect(getOptions(), null, mIMqttActionListener);
        } catch (MqttException e) {
            e.printStackTrace();
        }
    }

    /**
     * 断开Mqtt连接重新连接
     */
    public void reStartMqtt() {
        disConnectMqtt();
        connectMqtt();
    }

    /**
     * 断开Mqtt连接
     */
    public void disConnectMqtt() {
        if (mqttClient != null && mqttClient.isConnected()) {
            try {
                mqttClient.close();
                mqttClient.disconnect();
            } catch (MqttException e) {
                e.printStackTrace();
            }
            mqttClient = null;
        }
        isConnect = MQTT_CONNECT_STATUS_IDLE;
    }

    /**
     * 向Mqtt服务器发送数据
     */
    public void pubMsg(String Topic, String Msg, int Qos) {
        if (!isConnected()) {
            Log.d(TAG, "Mqtt连接未打开");
            return;
        }
        try {
            /** Topic,Msg,Qos,Retained**/
            mqttClient.publish(Topic, Msg.getBytes(), Qos, false);
        } catch (MqttException e) {
            e.printStackTrace();
        }
    }

    /**
     * 向Mqtt服务器发送数据
     */
    public void pubMsg(String Topic, byte[] Msg, int Qos) {
        if (!isConnected()) {
            Log.d(TAG, "Mqtt连接未打开");
            return;
        }
        try {
            /** Topic,Msg,Qos,Retained**/
            mqttClient.publish(Topic, Msg, Qos, false);
        } catch (MqttException e) {
            e.printStackTrace();
        }
    }

    /**
     * 向Mqtt服务器订阅某一个Topic
     */
    public void subTopic(String Topic, int Qos) {
        if (!isConnected()) {
            Log.d(TAG, "Mqtt连接未打开");
            return;
        }
        try {
            mqttClient.subscribe(Topic, Qos);
        } catch (MqttException e) {
            e.printStackTrace();
        }
    }

    /**
     * 设置Mqtt的连接信息
     */
    private MqttConnectOptions getOptions() {
        MqttConnectOptions options = new MqttConnectOptions();
        options.setMqttVersion(MqttConnectOptions.MQTT_VERSION_3_1_1);
        if (this.userID != null && this.userID.length() > 0 && this.passWord != null && this.passWord.length() > 0) {
            options.setUserName(this.userID);//设置服务器账号密码
            options.setPassword(this.passWord.toCharArray());
        }
        options.setCleanSession(false);//重连保持状态
        options.setAutomaticReconnect(true);
        options.setConnectionTimeout(10);//设置连接超时时间
        options.setKeepAliveInterval(10);//设置保持活动时间，超过时间没有消息收发将会触发ping消息确认
        return options;
    }

    public boolean isConnectionIdle() {
        return (isConnect == MQTT_CONNECT_STATUS_IDLE);
    }

    public boolean isConnected() {
        return (isConnect == MQTT_CONNECT_STATUS_CONNECTED);
    }
    public static String getClientID() {
        return clientID;
    }
}
