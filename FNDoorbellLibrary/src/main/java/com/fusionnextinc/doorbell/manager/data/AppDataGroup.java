package com.fusionnextinc.doorbell.manager.data;

import android.content.Context;

import com.fusionnextinc.doorbell.objects.FNGroup;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by Mike Chang on 2018/8/13
 */
public class AppDataGroup extends AppDataBase {
    protected final static String TAG = "AppDataGroup";
    public final static String TAG_GROUP_ID = "TAG_GROUP_ID";
    public final static String TAG_GROUP_TYPE = "TAG_GROUP_TYPE";
    public final static String TAG_GROUP_NAME = "TAG_GROUP_NAME";
    public final static String TAG_GROUP_DEVICE_COUNT = "TAG_GROUP_DEVICE_COUNT";
    public final static String TAG_GROUP_DEVICE_ONOFF = "TAG_GROUP_DEVICE_ONOFF";

    private volatile static AppDataGroup instance;
    private Context context;

    private AppDataGroup(Context context) {
        this.context = context;
    }

    public static AppDataGroup getInstance(Context context) {
        if (instance == null) {
            synchronized (AppDataGroup.class) {
                if (instance == null) {
                    instance = new AppDataGroup(context);
                }
            }
        }
        return instance;
    }

    @Override
    public ArrayList<String> getList(int tag) {
        return super.getList(tag);
    }

    @Override
    public void initList(boolean cleanIfExist) {
        super.initList(cleanIfExist);
    }

    // get the total numbers of saved share items
    public int genGroupId() {
        ArrayList<String> list = DataManager.getInstance(context).loadGroupList();
        if (list == null || list.size() == 0) return 0;
        Collections.sort(list, new Comparator<String>(){
            @Override
            public int compare(String o1, String o2) {
                return Integer.valueOf(o1).compareTo(Integer.valueOf(o2));
            }
        });
        return Integer.valueOf(list.get(list.size() - 1)) + 1;
    }

    public boolean deleteGroup(int type, String groupId) {
        if (DataManager.getInstance(context).deleteGroupList(groupId)) {
            DataManager.getInstance(context).deleteGroup(Integer.valueOf(groupId));
            ArrayList<String> list = DataManager.getInstance(context).loadGroupList();
            //Put this list to list cache
            updateList(type, list);
            return true;
        }
        return false;
    }

    //Ready the adapter data, this class will not save the adapter data
    public ListMap getAdapterData(int type) {
        ListMap adapter = new ListMap();
        ArrayList<String> list = DataManager.getInstance(context).loadGroupList();
        adapter.clear();
        if (list == null || list.size() <= 0) return adapter;

        //Put this list to list cache
        updateList(type, list);

        for (String str : list) {
            Map<String, Object> map = getGroupData(Integer.valueOf(str));
            if (map != null) {
                adapter.add(map);
            }
        }

        return adapter;
    }

    public Map<String, Object> getGroupData(int groupId) {
        FNGroup fnGroup = DataManager.getInstance(context).loadGroup(groupId);
        Map<String, Object> map = null;

        if (fnGroup != null) {
            map = new HashMap<String, Object>();
            map.put(TAG_GROUP_ID, fnGroup.getGroupId());
            map.put(TAG_GROUP_NAME, fnGroup.getGroupName());
            map.put(TAG_GROUP_TYPE, fnGroup.getGroupType());
            map.put(TAG_GROUP_DEVICE_COUNT, fnGroup.getDeviceCount());
            map.put(TAG_GROUP_DEVICE_ONOFF, fnGroup.isDeviceOn());
        }
        return map;
    }

    public boolean updateGroup(int type, FNGroup fnGroup, boolean isNewGroup) {
        ArrayList<String> list = new ArrayList<>();

        QueryStatus status = getQueryStatus(type);

        if (getList(type) != null) {
            list.addAll(getList(type));
        }

        if (fnGroup == null) return false;
        DataManager.getInstance(context).saveGroup(fnGroup);
        if (isNewGroup) {
            list.add(String.valueOf(fnGroup.getGroupId()));
        }
        if (status.isQueryEnd()) {
            status.setLastItemId("");
        } else {
            int lastIndex = list.size();
            if (lastIndex > 0) {
                status.setLastItemId(String.valueOf(lastIndex));
            }
        }
        updateQueryStatus(type, status);
        //Put this list to list cache
        updateList(type, list);
        //Save this list to database
        DataManager.getInstance(context).saveGroupList(getList(type));
        return true;
    }

    public boolean updateGroupList(int type, String groupId, ArrayList<FNGroup> groupList) {
        ArrayList<String> list = new ArrayList<>();

        list.clear();
        QueryStatus status = getQueryStatus(type);

        if (!status.isNewQuery()) {
            list.addAll(getList(type));
        }

        if (groupList == null) {
            status.setQueryEnd(true);
            status.setLastItemId("");
            updateQueryStatus(type, status);
            return false;
        }
        for (int i = 0; i < groupList.size(); i++) {
            FNGroup fnGroup = groupList.get(i);
            if (fnGroup == null) return false;
            DataManager.getInstance(context).saveGroup(fnGroup);
            list.add(String.valueOf(fnGroup.getGroupId()));
        }

        if (groupList.size() < status.getPageSize()) {
            status.setQueryEnd(true);
        }

        if (status.isQueryEnd()) {
            status.setLastItemId("");
        } else {
            int lastIndex = list.size();
            if (lastIndex > 0) {
                //status.setLastItemId( list.get(lastIndex - 1));
                status.setLastItemId(String.valueOf(lastIndex));
            }
        }
        updateQueryStatus(type, status);

        //Put this list to list cache
        updateList(type, list);
        //Save this list to database
        DataManager.getInstance(context).saveGroupList(getList(type));
        return true;
    }
}
