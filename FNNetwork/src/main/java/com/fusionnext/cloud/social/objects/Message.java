package com.fusionnext.cloud.social.objects;

import android.util.Log;

import com.fusionnext.cloud.datalink.json.FNJsonArray;
import com.fusionnext.cloud.datalink.json.FNJsonObject;

import org.json.JSONException;

import java.util.ArrayList;

/**
 * Created by Mike Chang on 2016/2/22.
 */
public class Message {
    protected final static String _TAG = "Message";

    public String _msgId;
    public String _userId;
    public String _groupId;
    public String _content;
    public String _createdAt;
    public String _updatedAt;
    public UserProfile _userProfile;

    public Message() {
        _msgId = "";
        _userId = "";
        _groupId = "";
        _content = "";
        _createdAt = "";
        _updatedAt = "";
        _userProfile = new UserProfile();
    }

    public void clean() {
        _msgId = "";
        _userId = "";
        _groupId = "";
        _content = "";
        _createdAt = "";
        _updatedAt = "";
        _userProfile.clean();
    }

    public boolean getDataFromJSONObject(FNJsonObject msgObj) {
        clean();
        if (msgObj == null) {
            Log.w(_TAG, " No JSON information");
            return false;
        }

        try {
            _msgId = msgObj.getStringDefault("id");
            _userId = msgObj.getStringDefault("user_id");
            _groupId = msgObj.getStringDefault("group_id");
            _content = msgObj.getStringDefault("content");
            _createdAt = msgObj.getStringDefault("created_at");
            _updatedAt = msgObj.getStringDefault("updated_at");

            FNJsonObject userObj = msgObj.getJSONObject("user");
            if (userObj == null) {
                Log.i(_TAG, "No user tag");
                return false;
            }
            //Get the user information
            _userProfile.getDataFromJSONObject(userObj);
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
        return true;
    }

    public static ArrayList<Message> parseMessageArray(String json) {
        try {
            ArrayList<Message> messageList = new ArrayList<Message>();
            FNJsonObject userObject = new FNJsonObject(json);
            FNJsonArray messagesArray = userObject.getJSONArray("items");
            if (messagesArray == null) {
                return null;
            }
            for (int i = 0; i < messagesArray.length(); i++) {
                FNJsonObject object = messagesArray.getJSONObject(i);
                Message message = new Message();
                message.getDataFromJSONObject(object);
                if (message == null) return null;
                messageList.add(message);
            }
            return messageList;
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return null;
    }
}
