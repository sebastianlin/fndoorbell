package com.fusionnextinc.doorbell.voice;

/**
 * Created by Mike Chang on 2019/4/10
 */

public interface OnFNAudioRecorderListener {

    void onStartRecord(FNAudioRecorder audioRecorder);

    void onRecordData(byte[] data);

    void onStopRecord(FNAudioRecorder audioRecorder);

    void onRecordError(FNAudioRecorder audioRecorder);

}