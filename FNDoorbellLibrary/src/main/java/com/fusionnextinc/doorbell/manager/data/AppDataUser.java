package com.fusionnextinc.doorbell.manager.data;

import android.content.Context;
import android.util.Log;
import android.widget.Toast;

import com.fusionnext.cloud.social.HttpData;
import com.fusionnext.cloud.utils.Utils;
import com.fusionnextinc.doorbell.R;
import com.fusionnextinc.doorbell.manager.thingsManager.ThingsHandler;
import com.fusionnextinc.doorbell.manager.thingsManager.ThingsManager;
import com.fusionnextinc.doorbell.objects.FNError;
import com.fusionnextinc.doorbell.objects.FNEvent;
import com.fusionnextinc.doorbell.objects.FNFirmware;
import com.fusionnextinc.doorbell.objects.FNSchedule;
import com.fusionnextinc.doorbell.objects.FNThing;
import com.fusionnextinc.doorbell.objects.FNUserProfile;
import com.fusionnextinc.doorbell.widget.FNToast;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by Mike Chang on 2017/7/24
 */
public class AppDataUser extends AppDataBase {
    protected final static String TAG = "AppDataUser";
    public final static String TAG_USER_ID = "TAG_USER_ID";
    public final static String TAG_USER_ORGANIZATION_ID = "TAG_USER_ORGANIZATION_ID";
    public final static String TAG_USER_NAME = "TAG_USER_NAME";
    public final static String TAG_USER_EMAIL = "TAG_USER_EMAIL";
    public final static String TAG_USER_CREATED_AT = "TAG_USER_CREATED_AT";
    public final static String TAG_USER_UPDATED_AT = "TAG_USER_UPDATED_AT";

    private volatile static AppDataUser instance;
    private Context context;
    private AppDataHandler.AppDataUserListener appDataUserListener = null;

    private AppDataUser(Context context) {
        this.context = context;
        ThingsManager.getInstance(context).setThingsListener(onThingsListener);
    }

    public static AppDataUser getInstance(Context context) {
        if (instance == null) {
            synchronized (AppDataUser.class) {
                if (instance == null) {
                    instance = new AppDataUser(context);
                }
            }
        }
        return instance;
    }

    @Override
    public ArrayList<String> getList(int tag) {
        return super.getList(tag);
    }

    @Override
    public void initList(boolean cleanIfExist) {
        super.initList(cleanIfExist);
    }

    // get the total numbers of saved share items
    public int getThingsSize(int type) {
        if (getList(type) == null) return 0;
        return getList(type).size();
    }

    public boolean deleteUser(int type, int index) {
        ArrayList<String> list = getList(type);
        if (list == null || list.size() <= 0) return false;
        if (index >= list.size()) return false;
        String id = list.get(index);
        if (id == null || id.endsWith("")) return false;

        return deleteUser(id);
    }

    public boolean deleteUser(String userId) {
        return DataManager.getInstance(context).deleteUser(userId);
    }

    //Ready the adapter data, this class will not save the adapter data
    public ListMap getAdapterData(int type, String thingId) {
        ListMap adapter = new ListMap();
        ArrayList<String> list = DataManager.getInstance(context).loadUserList(type, thingId);//getList(type);
        adapter.clear();
        if (list == null || list.size() <= 0) return adapter;

        for (String str : list) {
            Map<String, Object> map = getUserData(str);
            adapter.add(map);
        }

        return adapter;
    }

    private Map<String, Object> getUserData(String userId) {
        FNUserProfile fnUserProfile = DataManager.getInstance(context).loadUser(userId);
        Map<String, Object> map = new HashMap<String, Object>();

        if (fnUserProfile == null) {
            map.put(TAG_USER_NAME, "None");
        } else {
            map.put(TAG_USER_ID, fnUserProfile.getUserId());
            map.put(TAG_USER_ORGANIZATION_ID, fnUserProfile.getOrganizationId());
            map.put(TAG_USER_NAME, fnUserProfile.getName());
            map.put(TAG_USER_EMAIL, fnUserProfile.getEmail());
            map.put(TAG_USER_CREATED_AT, fnUserProfile.getCreatedAt());
            map.put(TAG_USER_UPDATED_AT, fnUserProfile.getUpdatedAt());
        }
        return map;
    }

    public void loadInvitationsList(String thingId, boolean onlineData, boolean onlineIfNoData) {
        loadUsersList(AppDataUser.USER_TYPE_THING_INVITED, thingId, onlineData, onlineIfNoData);
        loadUsersList(AppDataUser.USER_TYPE_THING_SHARED, thingId, onlineData, onlineIfNoData);
    }

    private void loadUsersList(int type, String thingId, boolean onlineData, boolean onlineIfNoData) {
        QueryStatus status = getQueryStatus(type);
        if (status == null) status = new QueryStatus();
        status.setQueryState(true);
        updateQueryStatus(type, status);

        if (onlineData) {
            startUsersListTask(type, thingId, true);
            return;
        }

        ArrayList<String> list = DataManager.getInstance(context).loadUserList(type, thingId);//getList(type);
        if ((list == null || list.size() <= 0) && onlineIfNoData) {
            if (!Utils.isNetworkConnected(context)) {
                Log.d(TAG, "Network is not available");
                return;
            }
            startUsersListTask(type, thingId, true);
            return;
        }

        if (!Utils.isNetworkConnected(context)) {
            Log.d(TAG, "Network is not available");
            return;
        }

        status.setQueryEnd(true);
        updateQueryStatus(type, status);
        refreshUsersAdapter(type, thingId);
    }

    public boolean hasMoreData(int tag) {
        QueryStatus status = getQueryStatus(tag);
        if (status == null) return false;
        return !status.isQueryEnd();
    }

    public boolean updateUser(int type, String thingId, FNUserProfile fnUserProfile) {
        ArrayList<String> list = new ArrayList<>();

        list.clear();
        QueryStatus status = getQueryStatus(type);
        if (!status.isNewQuery()) {
            list.addAll(getList(type));
        }

        if (fnUserProfile == null) return false;
        list.add(fnUserProfile.getUserId());

        if (status.isQueryEnd()) {
            status.setLastItemId("");
        } else {
            int lastIndex = list.size();
            if (lastIndex > 0) {
                status.setLastItemId(String.valueOf(lastIndex));
            }
        }
        updateQueryStatus(type, status);
        //Put this list to list cache
        updateList(type, list);
        //Save this list to database
        DataManager.getInstance(context).saveUserList(type, thingId, getList(type));
        return true;
    }

    public boolean updateUsersList(int type, String thingId, ArrayList<FNUserProfile> userList) {
        ArrayList<String> list = new ArrayList<>();

        list.clear();
        QueryStatus status = getQueryStatus(type);

        if (!status.isNewQuery()) {
            list.addAll(getList(type));
        }

        if (userList == null) {
            status.setQueryEnd(true);
            status.setLastItemId("");
            updateQueryStatus(type, status);
            return false;
        }
        for (int i = 0; i < userList.size(); i++) {
            FNUserProfile fnUserProfile = userList.get(i);
            if (fnUserProfile == null) return false;
            DataManager.getInstance(context).saveUser(fnUserProfile);
            list.add(fnUserProfile.getUserId());
        }

        if (userList.size() < status.getPageSize()) {
            status.setQueryEnd(true);
        }

        if (status.isQueryEnd()) {
            status.setLastItemId("");
        } else {
            int lastIndex = list.size();
            if (lastIndex > 0) {
                //status.setLastItemId( list.get(lastIndex - 1));
                status.setLastItemId(String.valueOf(lastIndex));
            }
        }
        updateQueryStatus(type, status);

        //Put this list to list cache
        updateList(type, list);
        //Save this list to database
        DataManager.getInstance(context).saveUserList(type, thingId, getList(type));
        return true;
    }

    public long getInvitedUserCounts(String thingId) {
        if (thingId != null) {
            return DataManager.getInstance(context).loadUserList(USER_TYPE_THING_INVITED, thingId).size();
        }
        return  0;
    }
    private void refreshUsersData(int type, final String thingId, boolean isNewQuery) {
        QueryStatus status = getQueryStatus(type);
        status.setQueryState(isNewQuery);
        startUsersListTask(type, thingId, isNewQuery);
    }

    public void getNextUsersPage(int type, final String thingId) {
        if (!Utils.isNetworkConnected(context)) {
            FNToast.makeText(context, context.getString(R.string.fn_msg_no_network), Toast.LENGTH_SHORT).show();
        } else {
            startUsersListTask(type, thingId, false);
        }
    }

    //Start an ASyncTask to get the share list from server
    private void startUsersListTask(final int type, final String thingId, boolean isNewQuery) {
        QueryStatus status = getQueryStatus(type);
        if (isNewQuery) {
            //Clear the loaded data
            clearList(type);
        }

        status.setQueryState(isNewQuery);
        updateQueryStatus(type, status);

        switch (type) {
            case USER_TYPE_THING_INVITED:
                ThingsManager.getInstance(context).setThingsListener(onThingsListener);
                ThingsManager.getInstance(context).getInvitedUserList(thingId, HttpData._GET_TYPE_SORT_ORDER_DESC, null, null);
                break;
            case USER_TYPE_THING_SHARED:
                ThingsManager.getInstance(context).setThingsListener(onThingsListener);
                ThingsManager.getInstance(context).getSharedUserList(thingId, HttpData._GET_TYPE_SORT_ORDER_DESC, null, null);
                break;
            default:
                break;
        }
    }

    private final ThingsHandler.ThingsListener onThingsListener = new ThingsHandler.ThingsListener() {

        @Override
        public void onListUserOwnedThings(ThingsManager instance, boolean result, String userId, ArrayList<FNThing> thingArrayList, FNError error) {

        }

        @Override
        public void onListUserInvitedThings(ThingsManager instance, boolean result, String userId, ArrayList<FNThing> thingArrayList, FNError error) {

        }

        @Override
        public void onListUserSharedThings(ThingsManager instance, boolean result, String userId, ArrayList<FNThing> thingArrayList, FNError error) {

        }

        @Override
        public void onListUserAllThings(ThingsManager instance, boolean result, String userId, ArrayList<FNThing> ownedThingArrayList, ArrayList<FNThing> sharedThingArrayList, ArrayList<FNThing> invitedThingArrayList, FNError error) {

        }

        @Override
        public void onListThingEvents(ThingsManager instance, boolean result, String thingId, String eventName, ArrayList<FNEvent> eventList, FNError error) {

        }

        @Override
        public void onListThingEventsByTime(ThingsManager instance, boolean result, String thingId, String eventName, String startTime, String endTime, ArrayList<FNEvent> eventList, FNError error) {

        }

        @Override
        public void onCurBatteryLevel(ThingsManager instance, boolean result, String thingId, String batteryLevel, FNError error) {

        }

        @Override
        public void onCurFirmwareVersion(ThingsManager instance, boolean result, String thingId, String fwVersion, FNError error) {

        }

        @Override
        public void onCurSwitchedStatus(ThingsManager instance, boolean result, String thingId, boolean isSwitch, FNError error) {

        }

        @Override
        public void onNewFirmwareVersion(ThingsManager instance, boolean result, String thingId, FNFirmware fnFirmware) {

        }

        @Override
        public void onStartFwUpdate(ThingsManager instance, boolean result, String thingId, String fwId) {

        }

        @Override
        public void onCurRingTone(ThingsManager instance, boolean result, String thingId, String ringtoneId, FNError error) {

        }

        @Override
        public void onGetScheduleList(ThingsManager instance, boolean result, String thingId, ArrayList<FNSchedule> scheduleList, FNError error) {

        }

        @Override
        public void onCreateThing(ThingsManager instance, boolean result, FNThing fnThing, FNError error) {

        }

        @Override
        public void onUpdateThing(ThingsManager instance, boolean result, FNThing fnThing, FNError error) {

        }

        @Override
        public void onDeleteThing(ThingsManager instance, boolean result, FNError error) {

        }

        @Override
        public void onGetThing(ThingsManager instance, boolean result, FNThing fnThing, FNError error) {

        }

        @Override
        public void onChangeRingtone(ThingsManager instance, boolean result, String thingId, int ringtoneId) {

        }

        @Override
        public void onSetTimer(ThingsManager instance, boolean result, String thingId, String value) {

        }

        @Override
        public void onSetSchedule(ThingsManager instance, boolean result, String thingId, String value) {

        }

        @Override
        public void onShareInvited(ThingsManager instance, boolean result, String thingId, FNError error) {

        }

        @Override
        public void onShareAccepted(ThingsManager instance, boolean result, String thingId, FNError error) {

        }

        @Override
        public void onShareRejected(ThingsManager instance, boolean result, String thingId, FNError error) {

        }

        @Override
        public void onSwitchOnOff(ThingsManager instance, boolean result, String thingId, boolean isTurnOn) {

        }

        @Override
        public void onCloseCallSlot(ThingsManager instance, boolean result, FNError error) {

        }

        @Override
        public void onTakeCallSlot(ThingsManager instance, boolean result, String thingId, String callSlot, FNError error) {

        }

        @Override
        public void onListInvitedUsers(ThingsManager instance, boolean result, String thingId, ArrayList<FNUserProfile> userList, FNError error) {
            if (result) {
                updateUsersList(USER_TYPE_THING_INVITED, thingId, userList);
                refreshUsersAdapter(USER_TYPE_THING_INVITED, thingId);
            }
        }

        @Override
        public void onListSharedUsers(ThingsManager instance, boolean result, String thingId, ArrayList<FNUserProfile> userList, FNError error) {
            if (result) {
                updateUsersList(USER_TYPE_THING_SHARED, thingId, userList);
                refreshUsersAdapter(USER_TYPE_THING_SHARED, thingId);
            }
        }

        @Override
        public void onCreateClaimCode(ThingsManager instance, boolean result, String claimCode, FNError error) {

        }
    };

    public void setAppDataUserListener(AppDataHandler.AppDataUserListener listener) {
        this.appDataUserListener = listener;
    }

    private void refreshUsersAdapter(int type, String thingId) {
        if (appDataUserListener != null) {
            appDataUserListener.onRefreshListAdapter(type, thingId);
        }
    }

    public void refreshUsersListData(String thingId) {
        refreshUsersData(AppDataUser.USER_TYPE_THING_INVITED, thingId, true);
        refreshUsersData(AppDataUser.USER_TYPE_THING_SHARED, thingId, true);
        /*if (appDataUserListener != null) {
            appDataUserListener.onRefreshListView();
        }*/
    }
}
