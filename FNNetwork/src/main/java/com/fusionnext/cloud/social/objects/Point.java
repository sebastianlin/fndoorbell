package com.fusionnext.cloud.social.objects;

import android.util.Log;

import com.fusionnext.cloud.datalink.json.FNJsonArray;
import com.fusionnext.cloud.datalink.json.FNJsonObject;
import com.fusionnext.cloud.social.Public;

import org.json.JSONException;

import java.util.ArrayList;

/**
 * Created by Mike Chang on 2016/1/19.
 */
public class Point {
    protected final static String _TAG = "Point";

    public Point() {
        _trackId = "";
        _pointId = "";
        _lat = "";
        _lng = "";
        _trackedAt = "";
        _createdAt = "";
        _updatedAt = "";
        _photo = "";
    }

    public String _trackId = "";
    public String _pointId = "";
    public String _lat;
    public String _lng;
    public String _trackedAt;
    public String _createdAt;
    public String _updatedAt;
    public String _photo;
    //public UserProfile _userProfile;

    public void clean() {
        _trackId = "";
        _pointId = "";
        _lat = "";
        _lng = "";
        _trackedAt = "";
        _createdAt = "";
        _updatedAt = "";
        _photo = "";
    }

    public boolean getDataFromJSONObject(FNJsonObject pointObj) {
        clean();
        if (pointObj == null) {
            Log.w(_TAG, " No JSON information");
            return false;
        }
        try {
            _trackId = pointObj.getStringDefault("track_id");
            _pointId = pointObj.getStringDefault("id");
            _lat = pointObj.getStringDefault("lat");
            _lng = pointObj.getStringDefault("lng");
            _trackedAt = pointObj.getStringDefault("tracked_at");
            _createdAt = pointObj.getStringDefault("created_at");
            _updatedAt = pointObj.getStringDefault("updated_at");

            FNJsonObject photoObj = pointObj.getJSONObject("photos");
            if (photoObj != null) {
                _photo = Public._HOST_V1 + photoObj.getStringDefault("href");
            }

        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
        return true;
    }

    public static ArrayList<Point> parsePointArray(String json) {
        try {
            ArrayList<Point> pointList = new ArrayList<Point>();
            FNJsonObject pointObject = new FNJsonObject(json);
            FNJsonArray pointsArray = pointObject.getJSONArray("items");
            if (pointsArray == null) {
                return null;
            }
            for (int i = 0; i < pointsArray.length(); i++) {
                FNJsonObject object = pointsArray.getJSONObject(i);
                Point point = new Point();
                point.getDataFromJSONObject(object);
                if (point == null) return null;
                pointList.add(point);
            }
            return pointList;
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return null;
    }
}
