package com.fusionnext.cloud.fusionapi;


import com.fusionnext.cloud.utils.FNNetworkLog;

import java.io.IOException;
import java.net.URL;

/**
 * Created by Mike Chang on 2016/2/22.
 */

public class HttpData {
    protected final static String _TAG = "HttpData";
    //HttpPost Type
    public final static int _POST_TYPE_UNKNOWN = 0x50;
    public final static int _POST_TYPE_GET_DEVICE_TOKEN = 0x51;
    public final static int _POST_TYPE_REFRESH_DEVICE_TOKEN = 0x52;
    public final static int _POST_TYPE_CREATE_CAMERA = 0x53;
    public final static int _POST_TYPE_CREATE_ARTICLE = 0x58;
    public final static int _POST_TYPE_COMMENT_ARTICLE = 0x59;
    public final static int _POST_TYPE_LIKE_ARTICLE = 0x5A;
    public final static int _POST_TYPE_CREATE_DEVICE = 0x5C;
    public final static int _POST_TYPE_UPDATE_DEVICE = 0x5B;
    public final static int _POST_TYPE_UPLOAD_FILE = 0x5D;

    //HttpGet Type
    public final static int _GET_TYPE_GET_CLIENT = 0x10;
    public final static int _GET_TYPE_LIST_PERMISSION = 0x11;
    public final static int _GET_TYPE_FW_SEARCH_UPDATE = 0x12;
    public final static int _GET_TYPE_FW_GET_UPDATE = 0x13;
    public final static int _GET_TYPE_SW_LIST_UPDATES = 0x14;
    public final static int _GET_TYPE_SW_GET_UPDATE = 0x15;
    //HttpPut Type
    public final static int _PUT_TYPE_UNKNOWN = 0x80;
    public final static int _PUT_TYPE_UPDATE_DEVICE = 0x81;
    public final static int _PUT_TYPE_UPDATE_CAMERA = 0x82;


    protected static URL generateHttpPostUrl(int postType) {
        boolean error = true;
        URL url = null;
        try {
            switch (postType) {
                case _POST_TYPE_GET_DEVICE_TOKEN:
                    url = new URL(Public._INTERFACE_POST_GET_DEVICE_TOKEN);
                    break;
                case _POST_TYPE_REFRESH_DEVICE_TOKEN:
                    url = new URL(Public._INTERFACE_POST_REFRESH_DEVICE_TOKEN);
                    break;
                case _POST_TYPE_CREATE_CAMERA:
                    url = new URL(Public._INTERFACE_POST_CREATE_CAMERA);
                    break;
                case _POST_TYPE_CREATE_ARTICLE:
                    url = new URL(Public._INTERFACE_POST_CREATE_ARTICLE);
                    break;
                case _POST_TYPE_COMMENT_ARTICLE:
                    url = new URL(Public._INTERFACE_POST_COMMENT_ARTICLE);
                    break;
                case _POST_TYPE_LIKE_ARTICLE:
                    url = new URL(Public._INTERFACE_POST_LIKE_ARTICLE);
                    break;
                case _POST_TYPE_CREATE_DEVICE:
                    url = new URL(Public._INTERFACE_POST_CREATE_DEVICE);
                    break;
                case _POST_TYPE_UPDATE_DEVICE:
                    url = new URL(Public._INTERFACE_PUT_UPDATE_DEVICE);
                    break;
                case _POST_TYPE_UPLOAD_FILE:
                    url = new URL(Public._INTERFACE_POST_UPLOAD_FILE);
                    break;
                default:
                    return url;
            }
            FNNetworkLog.d(_TAG, url.toString());
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
        }
        return url;
    }


    protected static String generateHttpGetUrl(int postType) {
        return generateHttpGetUrl(postType, null);
    }

    protected static String generateHttpGetUrl(int postType, String id) {
        boolean error = true;
        String url = null;

        switch (postType) {
            case _GET_TYPE_GET_CLIENT:
                url = Public._INTERFACE_GET_GET_CLIENT.replace("{client_id}", id);
                break;
            case _GET_TYPE_LIST_PERMISSION:
                url = Public._INTERFACE_GET_LIST_PERMISSIONS.replace("{client_id}", id);
                break;
            case _GET_TYPE_FW_SEARCH_UPDATE:
                url = Public._INTERFACE_SEARCH_FW_UPDATE_LIST.replace("{fw_id}", id);
                break;
            case _GET_TYPE_FW_GET_UPDATE:
                url = Public._INTERFACE_GET_FW_UPDATE_LINK.replace("{fw_id}", id);
                break;
            case _GET_TYPE_SW_LIST_UPDATES:
                url = Public._INTERFACE_GET_SW_UPDATE_LIST.replace("{client_id}", id);
                break;
            case _GET_TYPE_SW_GET_UPDATE:
                url = Public._INTERFACE_GET_SW_UPDATE_LINK.replace("{sw_id}", id);
                break;
            default:
                return url;
        }
        FNNetworkLog.d(_TAG, url);

        return url;
    }


    protected static URL generateHttpPutUrl(int postType) {
        return generateHttpPutUrl(postType, null);
    }

    protected static URL generateHttpPutUrl(int postType, String id) {
        boolean error = true;
        URL url = null;
        try {
            switch (postType) {
                case _PUT_TYPE_UPDATE_DEVICE:
                    url = new URL(Public._INTERFACE_PUT_UPDATE_DEVICE.replace("{device_id}", id));
                    break;
                case _PUT_TYPE_UPDATE_CAMERA:
                    url = new URL(Public._INTERFACE_PUT_UPDATE_CAMERA.replace("{camera_id}", id));
                    break;
                default:
                    return url;
            }
            FNNetworkLog.d(_TAG, url.toString());
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
        }
        return url;
    }
}
