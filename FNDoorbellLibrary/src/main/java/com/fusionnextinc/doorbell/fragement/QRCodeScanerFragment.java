package com.fusionnextinc.doorbell.fragement;

import android.Manifest;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.graphics.PointF;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.core.app.ActivityCompat;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.fusionnextinc.doorbell.MyApplication;
import com.fusionnextinc.doorbell.R;
import com.fusionnextinc.doorbell.manager.qrCode.PointsOverlayView;
import com.fusionnextinc.doorbell.manager.qrCode.qrcodeReaderView.QRCodeReaderView;
import com.fusionnextinc.doorbell.manager.thingsManager.ThingsHandler;
import com.fusionnextinc.doorbell.manager.thingsManager.ThingsManager;
import com.fusionnextinc.doorbell.objects.FNError;
import com.fusionnextinc.doorbell.objects.FNEvent;
import com.fusionnextinc.doorbell.objects.FNFirmware;
import com.fusionnextinc.doorbell.objects.FNQRInfo;
import com.fusionnextinc.doorbell.objects.FNSchedule;
import com.fusionnextinc.doorbell.objects.FNThing;
import com.fusionnextinc.doorbell.objects.FNUserProfile;
import com.fusionnextinc.doorbell.utils.FNLog;
import com.fusionnextinc.doorbell.utils.Utils;
import com.fusionnextinc.doorbell.widget.FNAlert.FNAlertDialog;
import com.fusionnextinc.doorbell.widget.FNDialog;
import com.fusionnextinc.doorbell.widget.FNLayoutUtil;
import com.fusionnextinc.doorbell.widget.FNProgressBarCustom;
import com.fusionnextinc.doorbell.widget.FNToast;

import java.util.ArrayList;

/**
 * Created by Mike Chang on 2017/8/1
 */
public class QRCodeScanerFragment extends FNFragment implements ActivityCompat.OnRequestPermissionsResultCallback, QRCodeReaderView.OnQRCodeReadListener {
    private static final String TAG = QRCodeScanerFragment.class.getSimpleName();
    private static final int MY_PERMISSION_REQUEST_CAMERA = 0;
    private FNLayoutUtil layoutUtil;
    private ThingsManager thingsManager;
    private QRCodeReaderView qrCodeReaderView;
    private PointsOverlayView pointsOverlayView;
    private FNProgressBarCustom progressBar;
    private static FNThing curFnThing;
    private boolean isQRCodelocked = false;

    public static void openFragment(boolean clearOtherStack, FNThing fnThing) {
        QRCodeScanerFragment launchFragment = new QRCodeScanerFragment();
        FNFragmentManager.getInstance().openFragment(launchFragment, clearOtherStack);
        curFnThing = fnThing;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        layoutUtil = new FNLayoutUtil(getActivity(), MyApplication.LAYOUT_WIDTH, MyApplication.LAYOUT_HEIGHT, FNLayoutUtil.BASE_WIDTH);
        thingsManager = ThingsManager.getInstance(getActivity());
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        final ViewGroup nullParent = null;
        getActionBar().resetAll();
        getActionBar().setTitle(getString(R.string.fn_btn_share_qrcode), Gravity.CENTER, null);
        getActionBar().setCoverMode(false);
        getActionBar().setStartImage(R.drawable.back, new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        getActionBar().show();
        View view = inflater.inflate(R.layout.fragment_qrcode_scanner, nullParent);
        layoutUtil.fitAllView(view);
        progressBar = new FNProgressBarCustom(getActivity(), (ViewGroup) view.findViewById(R.id.fragment_qrcode_scanner));

        if (ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.CAMERA)
                == PackageManager.PERMISSION_GRANTED) {
            initQRCodeReaderView(view);
        } else {
            requestCameraPermission();
        }
        return view;
    }

    @Override
    public void onResume() {
        super.onResume();

        if (qrCodeReaderView != null) {
            qrCodeReaderView.startCamera();
        }
    }

    @Override
    public void onPause() {
        super.onPause();

        if (qrCodeReaderView != null) {
            qrCodeReaderView.stopCamera();
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode != MY_PERMISSION_REQUEST_CAMERA) {
            return;
        }

        if (grantResults.length == 1 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
            FNToast.makeText(getActivity(), "Camera permission was granted.", FNToast.LENGTH_SHORT).show();
            initQRCodeReaderView(getView());
        } else {
            FNToast.makeText(getActivity(), "Camera permission request was denied.", FNToast.LENGTH_SHORT)
                    .show();
        }
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);

    }

    // Called when a QR is decoded
    // "text" : the text encoded in QR
    // "points" : points where QR control points are placed
    @Override
    public void onQRCodeRead(String text, PointF[] points) {
        String sharedEmail = text.trim();
        FNQRInfo fnQRInfo = FNQRInfo.parseFNQRInfo(sharedEmail);

        pointsOverlayView.setPoints(points);
        if (fnQRInfo != null && sharedEmail.length() > 0 && !isQRCodelocked) {
            if (Utils.isNetworkConnected(getActivity())) {
                isQRCodelocked = true;
                startWait();
                thingsManager.setThingsListener(thingsListener);
                thingsManager.shareInvite(curFnThing.getThingId(), fnQRInfo.getEmail());
            } else {
                FNDialog.showNoNetworkDialog(getActivity());
            }
        }
        FNLog.d(TAG, "qrcode text:" + sharedEmail + ", isQRCodelocked = " + isQRCodelocked);
    }

    private void requestCameraPermission() {
        if (ActivityCompat.shouldShowRequestPermissionRationale(getActivity(), Manifest.permission.CAMERA)) {
            new FNAlertDialog(getActivity(), FNAlertDialog.WARNING_TYPE)
                    .setTitleText(getActivity().getString(R.string.fn_dialog_are_you_sure))
                    .setContentText("Camera access is required to display the camera preview.")
                    .setConfirmText(getActivity().getString(R.string.fn_dialog_ok))
                    .setConfirmClickListener(new FNAlertDialog.OnSweetClickListener() {
                        @Override
                        public void onClick(FNAlertDialog sDialog) {
                            ActivityCompat.requestPermissions(getActivity(), new String[]{
                                    Manifest.permission.CAMERA
                            }, MY_PERMISSION_REQUEST_CAMERA);
                        }
                    })
                    .show();
        } else {
            new FNAlertDialog(getActivity(), FNAlertDialog.WARNING_TYPE)
                    .setTitleText(getActivity().getString(R.string.fn_dialog_are_you_sure))
                    .setContentText("Permission is not available. Requesting camera permission.")
                    .setConfirmText(getActivity().getString(R.string.fn_dialog_ok))
                    .setConfirmClickListener(new FNAlertDialog.OnSweetClickListener() {
                        @Override
                        public void onClick(FNAlertDialog sDialog) {
                            ActivityCompat.requestPermissions(getActivity(), new String[]{
                                    Manifest.permission.CAMERA
                            }, MY_PERMISSION_REQUEST_CAMERA);
                        }
                    })
                    .show();
        }
    }

    private void initQRCodeReaderView(View view) {
        qrCodeReaderView = (QRCodeReaderView) view.findViewById(R.id.qrdecoderview);
        pointsOverlayView = (PointsOverlayView) view.findViewById(R.id.points_overlay_view);
        qrCodeReaderView.setAutofocusInterval(2000L);
        qrCodeReaderView.setOnQRCodeReadListener(this);
        qrCodeReaderView.setBackCamera();
        qrCodeReaderView.startCamera();
    }

    private void startWait() {
        progressBar.show();
    }

    private void stopWait() {
        progressBar.hide();
    }

    final FNAlertDialog.OnSweetClickListener onSweetClickListener = new FNAlertDialog.OnSweetClickListener() {
        @Override
        public void onClick(FNAlertDialog sDialog) {
            isQRCodelocked = false;
            sDialog.dismiss();
            FNLog.d(TAG, "onSweetClickListener:  isQRCodelocked = " + isQRCodelocked);
        }
    };

    final ThingsHandler.ThingsListener thingsListener = new ThingsHandler.ThingsListener() {

        @Override
        public void onListUserOwnedThings(ThingsManager instance, boolean result, String userId, ArrayList<FNThing> thingArrayList, FNError error) {

        }

        @Override
        public void onListUserInvitedThings(ThingsManager instance, boolean result, String userId, ArrayList<FNThing> thingArrayList, FNError error) {

        }

        @Override
        public void onListUserSharedThings(ThingsManager instance, boolean result, String userId, ArrayList<FNThing> thingArrayList, FNError error) {

        }

        @Override
        public void onListUserAllThings(ThingsManager instance, boolean result, String userId, ArrayList<FNThing> ownedThingArrayList, ArrayList<FNThing> sharedThingArrayList, ArrayList<FNThing> invitedThingArrayList, FNError error) {

        }

        @Override
        public void onListThingEvents(ThingsManager instance, boolean result, String thingId, String eventName, ArrayList<FNEvent> eventList, FNError error) {

        }

        @Override
        public void onListThingEventsByTime(ThingsManager instance, boolean result, String thingId, String eventName, String startTime, String endTime, ArrayList<FNEvent> eventList, FNError error) {

        }

        @Override
        public void onCurBatteryLevel(ThingsManager instance, boolean result, String thingId, String batteryLevel, FNError error) {

        }

        @Override
        public void onCurFirmwareVersion(ThingsManager instance, boolean result, String thingId, String fwVersion, FNError error) {

        }

        @Override
        public void onCurSwitchedStatus(ThingsManager instance, boolean result, String thingId, boolean isSwitch, FNError error) {

        }

        @Override
        public void onNewFirmwareVersion(ThingsManager instance, boolean result, String thingId, FNFirmware fnFirmware) {

        }

        @Override
        public void onStartFwUpdate(ThingsManager instance, boolean result, String thingId, String fwId) {

        }

        @Override
        public void onCurRingTone(ThingsManager instance, boolean result, String thingId, String ringtoneId, FNError error) {

        }

        @Override
        public void onGetScheduleList(ThingsManager instance, boolean result, String thingId, ArrayList<FNSchedule> scheduleList, FNError error) {

        }

        @Override
        public void onListInvitedUsers(ThingsManager instance, boolean result, String thingId, ArrayList<FNUserProfile> userList, FNError error) {

        }

        @Override
        public void onListSharedUsers(ThingsManager instance, boolean result, String thingId, ArrayList<FNUserProfile> userList, FNError error) {

        }

        @Override
        public void onCreateClaimCode(ThingsManager instance, boolean result, String claimCode, FNError error) {

        }

        @Override
        public void onCreateThing(ThingsManager instance, boolean result, FNThing fnThing, FNError error) {

        }

        @Override
        public void onUpdateThing(ThingsManager instance, boolean result, FNThing fnThing, FNError error) {

        }

        @Override
        public void onDeleteThing(ThingsManager instance, boolean result, FNError error) {

        }

        @Override
        public void onGetThing(ThingsManager instance, boolean result, FNThing fnThing, FNError error) {

        }

        @Override
        public void onChangeRingtone(ThingsManager instance, boolean result, String thingId, int ringtoneId) {

        }

        @Override
        public void onSetTimer(ThingsManager instance, boolean result, String thingId, String value) {

        }

        @Override
        public void onSetSchedule(ThingsManager instance, boolean result, String thingId, String value) {

        }

        @Override
        public void onShareAccepted(ThingsManager instance, boolean result, String thingId, FNError error) {

        }

        @Override
        public void onShareRejected(ThingsManager instance, boolean result, String thingId, FNError error) {

        }

        @Override
        public void onSwitchOnOff(ThingsManager instance, boolean result, String thingId, boolean isTurnOn) {

        }

        @Override
        public void onCloseCallSlot(ThingsManager instance, boolean result, FNError error) {

        }

        @Override
        public void onTakeCallSlot(ThingsManager instance, boolean result, String thingId, String callSlot, FNError error) {

        }

        @Override
        public void onShareInvited(ThingsManager instance, boolean result, String thingId, FNError error) {
            if (isAdded()) {
                stopWait();
                if (result) {
                    isQRCodelocked = false;
                    FNDialog.showSuccessDialog(getActivity(), getString(R.string.fn_tittle_share_title), getString(R.string.fn_msg_share_successfully).replace("${Thing}", getActivity().getString(R.string.fn_title_thing_name)));
                    FNLog.d(TAG, "onShareInvited:  isQRCodelocked = " + isQRCodelocked);
                    onBackPressed();
                } else {
                    FNDialog.showErrorDialog(getActivity(), getString(R.string.fn_msg_share_failed).replace("${Thing}", getActivity().getString(R.string.fn_title_thing_name)), error, onSweetClickListener);
                }
            }
        }
    };
}
