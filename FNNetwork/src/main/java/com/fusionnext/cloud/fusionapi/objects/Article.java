package com.fusionnext.cloud.fusionapi.objects;

import android.util.Log;

import com.fusionnext.cloud.datalink.json.FNJsonObject;
import com.fusionnext.cloud.fusionapi.Public;

import org.json.JSONException;

/**
 * Created by Mike Chang on 2016/2/22.
 */
public class Article {
    protected final static String _TAG = "Track";

    public Article() {
        _articleId = "";
        _clientId = "";
        _location = "";
        _title = "";
        _cover = "";
        _link = "";
        _content = "";
        _createdAt = "";
        _updatedAt = "";
    }

    public String _articleId;
    public String _clientId;
    public String _location;
    public String _title;
    public String _cover;
    public String _link;
    public String _content;
    public String _createdAt;
    public String _updatedAt;

    public void clean() {
        _articleId = "";
        _clientId = "";
        _location = "";
        _title = "";
        _cover = "";
        _link = "";
        _content = "";  // user's captured_at
        _createdAt = "";
        _updatedAt = "";
    }

    public boolean getDataFromJSONObject(FNJsonObject articleObj) {
        clean();
        if (articleObj == null) {
            Log.w(_TAG, " No JSON information");
            return false;
        }

        try {
            _articleId = articleObj.getStringDefault("id");
            _clientId = articleObj.getStringDefault("client_id");
            _location = articleObj.getStringDefault("location");
            if (_location != null) _location = _location.trim();
            _title = articleObj.getStringDefault("title");
            _cover = Public._INTERFACE_GET_ARTICLE_COVER.replace("{article_id}", _articleId);//photoObj.getStringDefault("cover");

            _link = articleObj.getStringDefault("link");
            _content = articleObj.getStringDefault("content");
            _createdAt = articleObj.getStringDefault("created_at");
            _updatedAt = articleObj.getStringDefault("updated_at");
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
        return true;
    }

    public static Article parseArticle(String jsonStr) {
        if (jsonStr == null || jsonStr.equals("")) return null;
        Article article = new Article();

        try {
            FNJsonObject object = new FNJsonObject(jsonStr);
            article.getDataFromJSONObject(object);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return article;
    }
}
