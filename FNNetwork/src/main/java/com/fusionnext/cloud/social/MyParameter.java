package com.fusionnext.cloud.social;

import android.app.Application;
import android.content.Context;
import android.util.Log;


/**
 * Created by Mike Chang  on 2015/7/27.
 */
public class MyParameter extends Application {
    private static final String TAG = "MyParameter";

    private final static String synchronizedTagDataMgr = "synchronizedTagDataMgr";
    private final static String synchronizedTagAuthorization = "synchronizedTagAuthorization";
    private final static String synchronizedTagTrips = "synchronizedTagTrips";
    private final static String synchronizedTagPhotos = "synchronizedTagPhotos";
    private final static String synchronizedTagUser = "synchronizedTagUser";
    private final static String synchronizedTagGroup = "synchronizedTagGroup";

    //Authorization
    private Authorization authorization;
    //Tracks
    private Tracks tracks;
    //Photos
    private Photos photos;
    //UserProfile
    private User user;

    //UserProfile
    private Groups group;
    //Database manager
    private DataManager dataMgr = null;

    public static MyParameter newInstance() {
        MyParameter myParameter = null;
        try {
            myParameter = new MyParameter();
        } catch (IllegalStateException e) {
            Log.e(TAG, "error occurs when creating FN social network lib");
        }
        return myParameter;
    }

    public MyParameter() {

    }

    public Authorization getAuthorization(Context context) {
        synchronized (synchronizedTagAuthorization) {
            context = context;
            if (authorization == null) setAuthorization(new Authorization(context));
            return authorization;
        }
    }

    public void setAuthorization(Authorization authorization) {
        synchronized (synchronizedTagAuthorization) {
            this.authorization = authorization;
        }
    }

    public Tracks getTrips(Context context) {
        synchronized (synchronizedTagTrips) {
            context = context.getApplicationContext();
            if (tracks == null) setTracks(new Tracks(context));
            return tracks;
        }
    }

    public void setTracks(Tracks tracks) {
        synchronized (synchronizedTagTrips) {
            this.tracks = tracks;
        }
    }

    public Photos getPhotos(Context context) {
        synchronized (synchronizedTagPhotos) {
            context = context.getApplicationContext();
            if (photos == null) setPhotos(new Photos(context));
            return photos;
        }
    }

    public void setPhotos(Photos photos) {
        synchronized (synchronizedTagPhotos) {
            this.photos = photos;
        }
    }

    public User getUser(Context context) {
        synchronized (synchronizedTagUser) {
            context = context.getApplicationContext();
            if (user == null) setUser(new User(context));
            return user;
        }
    }

    public void setUser(User user) {
        synchronized (synchronizedTagUser) {
            this.user = user;
        }
    }

    public Groups getGroup(Context context) {
        synchronized (synchronizedTagGroup) {
            context = context.getApplicationContext();
            if (group == null) setGroup(new Groups(context));
            return group;
        }
    }

    public void setGroup(Groups group) {
        synchronized (synchronizedTagGroup) {
            this.group = group;
        }
    }


    public DataManager getDataMgr(Context context) {
        synchronized (synchronizedTagDataMgr) {
            context = context;
            if (dataMgr == null) setDataMgr(DataManager.newData(context));
            return dataMgr;
        }
    }

    public void setDataMgr(DataManager dm) {
        synchronized (synchronizedTagDataMgr) {
            dataMgr = dm;
        }
    }
}