package com.fusionnext.cloud.iot.objects;

import android.util.Log;

import com.fusionnext.cloud.datalink.json.FNJsonObject;

import org.json.JSONException;

/**
 * Created by Mike Chang on 2017/8/9
 */
public class ErrorObj {
    protected final static String _TAG = "ErrorObj";
    protected final static String _JSON_TAG = "error";
    //General Error
    public final static int ERROR_GENERAL_INSUFFICIENT_PERMISSIONS = 2000;
    public final static int ERROR_GENERAL_THING_NOT_EXIST = 2001;
    public final static int ERROR_GENERAL_NOT_THING_OWNER = 2002;
    public final static int ERROR_GENERAL_USER_NOT_EXIST = 2003;
    public final static int ERROR_GENERAL_USER_NOT_FOUND = 2004;
    public final static int ERROR_GENERAL_ACCOUNT_FORMAT_WRONG = 2005;
    public final static int ERROR_GENERAL_SORT_OPTION_INVALID = 2006;
    public final static int ERROR_GENERAL_ACCOUNT_ERROR = 2007;
    public final static int ERROR_GENERAL_UNKNOWN = 2008;
    //System Error
    public final static int ERROR_SYSTEM_INTERNET_NOT_AVALIABLE = 2100;
    public final static int ERROR_SYSTEM_REQUEST_TIMEOUT = 2101;
    //Regester Error
    public final static int ERROR_REGISTER_EMAIL_HAS_BEEN_TAKEN = 2200;
    public final static int ERROR_REGISTER_PASSWORD_FORMAT_WRONG = 2202;
    //Login Error
    public final static int ERROR_LOGIN_CREATE_DEVICE_FAIL = 2300;
    public final static int ERROR_LOGIN_IDENTIFIER_NOT_AVAILABLE = 2301;
    public final static int ERROR_LOGIN_PASSWORD_WRONG = 2302;
    public final static int ERROR_LOGIN_USER_ALREADY_LOGIN = 2303;
    public final static int ERROR_LOGIN_EMAIL_NOT_FOUND = 2304;
    public final static int ERROR_LOGIN_EMAIL_NOT_ACTIVATE = 2305;
    //Logout Error
    public final static int ERROR_LOGOUT_NO_USER_LOGIN = 2400;
    public final static int ERROR_LOGOUT_DEVICE_NOT_FOUND = 2401;
    //Set or Reset Password Error
    public final static int ERROR_CHANGE_PASSWORD_ACCOUNT_NOT_MATCH = 2500;
    public final static int ERROR_CHANGE_PASSWORD_TOKEN_INVALID = 2501;
    //User Profile  Error
    public final static int ERROR_USERPROFILE_OLD_PASSWORD_WRONG = 2600;
    public final static int ERROR_USERPROFILE_USER_NOT_LOGIN = 2601;
    //Creatre Thing  Error
    public final static int ERROR_CREATE_THING_ALREADY_EXIST = 2700;
    public final static int ERROR_CREATE_THING_UNKNOWN_PRODUCT = 2701;
    public final static int ERROR_CREATE_THING_IDENTIFIER_FORMAT_ERROR = 2702;
    //Update/Delete Thing  Error
    public final static int ERROR_UPDATE_THING_NOT_THING_OWNER = 2800;
    public final static int ERROR_DELETE_THING_NOT_THING_OWNER = 2801;
    //Invite User Error
    public final static int ERROR_INVITE_USER_NOT_THING_OWNER = 2900;
    public final static int ERROR_INVITE_USER_ALREADY_INVITED = 2901;
    public final static int ERROR_INVITE_USER_SELF_NOT_ALLOW = 2902;
    //Accept/Reject User Error
    public final static int ERROR_ACCEPT_REJECT_NOT_INVITED = 3000;
    public final static int ERROR_ACCEPT_REJECT_THING_NOT_EXIST = 3001;
    //List State Error
    public final static int ERROR_LIST_STATE_NOT_THING_OWNER = 3100;
    //List Setting Error
    public final static int ERROR_LIST_SETTING_NOT_THING_OWNER = 3200;
    //List Setting Error
    public final static int ERROR_LIST_ITEM_NOT_THING_OWNER = 3300;
    //Call slot Error
    public final static int ERROR_CALL_SLOT_HAS_ALREADY_BEEN_TAKEN = 4120;

    public String _message;
    public String _code;
    public int _errorCode;

    public ErrorObj() {
        _message = "";
        _code = "";
        _errorCode = ERROR_GENERAL_UNKNOWN;
    }

    public void clean() {
        _message = "";
        _code = "";
        _errorCode = ERROR_GENERAL_UNKNOWN;
    }

    private boolean getDataFromJSONObject(final FNJsonObject jsonObj) {
        clean();
        if (jsonObj == null) {
            Log.e(_TAG, "Null JSON Object!");
            return false;
        }

        try {
            FNJsonObject errObj = jsonObj.getJSONObject(_JSON_TAG);
            if (errObj != null) {
                _message = errObj.getStringDefault("message");
                _code = errObj.getStringDefault("code");
                _errorCode = transferErrorCode(_message);
                if (errObj.has("validation_errors")) {
                    String validationErrors = errObj.optString("validation_errors");
                    FNJsonObject jsonObject = new FNJsonObject(validationErrors);
                    String identifier = jsonObject.getString("identifier").replace("[", "").replace("]", "");
                    _errorCode = transferErrorCode(identifier);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
            Log.e(_TAG, "Error occur during parsing JSON object");
            return false;
        }
        return true;
    }

    private int transferErrorCode(String errorString) {
        if (errorString != null) {
            if (errorString.contains("Internet connection is not avaliable.")) {
                return ERROR_SYSTEM_INTERNET_NOT_AVALIABLE;
            } else if (errorString.contains("Request Timeout.")) {
                return ERROR_SYSTEM_REQUEST_TIMEOUT;
            } else if (errorString.contains("Unknown Error")) {
                return ERROR_GENERAL_UNKNOWN;
            } else if (errorString.contains("Insufficient permissions")) {
                return ERROR_GENERAL_INSUFFICIENT_PERMISSIONS;
            } else if (errorString.contains("username is not email format.")) {
                return ERROR_GENERAL_ACCOUNT_FORMAT_WRONG;
            } else if (errorString.contains("Please use correct email name")) {
                return ERROR_GENERAL_ACCOUNT_ERROR;
            } else if (errorString.contains("User Not Found, Please re-login")) {
                return ERROR_GENERAL_USER_NOT_FOUND;
            } else if (errorString.contains("Thing you used is not exist!")) {
                return ERROR_GENERAL_THING_NOT_EXIST;
            } else if (errorString.contains("Thing not exist, please check again")) {
                return ERROR_GENERAL_THING_NOT_EXIST;
            } else if (errorString.contains("User Not Exist, Please re-login")) {
                return ERROR_GENERAL_USER_NOT_EXIST;
            } else if (errorString.contains("User Not Exist, Please check again")) {
                return ERROR_GENERAL_USER_NOT_EXIST;
            } else if (errorString.contains("The sort option you provide is not allow")) {
                return ERROR_GENERAL_SORT_OPTION_INVALID;
            } else if (errorString.contains("You don't have permission to invite user")) {
                return ERROR_GENERAL_NOT_THING_OWNER;
            } else if (errorString.contains("Create Device Failure")) {
                return ERROR_LOGIN_CREATE_DEVICE_FAIL;
            } else if (errorString.contains("your Identifier is not avaliable")) {
                return ERROR_LOGIN_IDENTIFIER_NOT_AVAILABLE;
            } else if (errorString.contains("Wrong password.")) {
                return ERROR_LOGIN_PASSWORD_WRONG;
            } else if (errorString.contains("User already login")) {
                return ERROR_LOGIN_USER_ALREADY_LOGIN;
            } else if (errorString.contains("email not found")) {
                return ERROR_LOGIN_EMAIL_NOT_FOUND;
            } else if (errorString.contains("User with specified email doesn't exist.")) {
                return ERROR_LOGIN_EMAIL_NOT_FOUND;
            } else if (errorString.contains("User hasn't confirmed his email yet.")) {
                return ERROR_LOGIN_EMAIL_NOT_ACTIVATE;
            } else if (errorString.contains("email has already been taken.")) {
                return ERROR_REGISTER_EMAIL_HAS_BEEN_TAKEN;
            } else if (errorString.contains("The email has already been taken.")) {
                return ERROR_REGISTER_EMAIL_HAS_BEEN_TAKEN;
            } else if (errorString.contains("The password must be at least 6 characters.")) {
                return ERROR_REGISTER_PASSWORD_FORMAT_WRONG;
            } else if (errorString.contains("No User Login")) {
                return ERROR_LOGOUT_NO_USER_LOGIN;
            } else if (errorString.contains("Device is not save in storage")) {
                return ERROR_LOGOUT_DEVICE_NOT_FOUND;
            } else if (errorString.contains("E-mail Not Match with Token!")) {
                return ERROR_CHANGE_PASSWORD_ACCOUNT_NOT_MATCH;
            } else if (errorString.contains("Token Invalid")) {
                return ERROR_CHANGE_PASSWORD_TOKEN_INVALID;
            } else if (errorString.contains("E-mail Not Match with Token!")) {
                return ERROR_CHANGE_PASSWORD_ACCOUNT_NOT_MATCH;
            } else if (errorString.contains("Old Password is Wrong")) {
                return ERROR_USERPROFILE_OLD_PASSWORD_WRONG;
            } else if (errorString.contains("login required")) {
                return ERROR_USERPROFILE_USER_NOT_LOGIN;
            } else if (errorString.contains("The identifier has already been taken.") ||
                    errorString.contains("validation.unique")) {
                return ERROR_CREATE_THING_ALREADY_EXIST;
            } else if (errorString.contains("Product not support")) {
                return ERROR_CREATE_THING_UNKNOWN_PRODUCT;
            } else if (errorString.contains("The mac format is incorrect")) {
                return ERROR_CREATE_THING_IDENTIFIER_FORMAT_ERROR;
            } else if (errorString.contains("You can only Modify your own thing!")) {
                return ERROR_UPDATE_THING_NOT_THING_OWNER;
            } else if (errorString.contains("You can only delete your own thing!")) {
                return ERROR_DELETE_THING_NOT_THING_OWNER;
            } else if (errorString.contains("You don't have permission to invite user")) {
                return ERROR_INVITE_USER_NOT_THING_OWNER;
            } else if (errorString.contains("User is already invited.")) {
                return ERROR_INVITE_USER_ALREADY_INVITED;
            } else if (errorString.contains("You can't invite yourself.")) {
                return ERROR_INVITE_USER_SELF_NOT_ALLOW;
            } else if (errorString.contains("You are not invited by curren thing")) {
                return ERROR_ACCEPT_REJECT_NOT_INVITED;
            } else if (errorString.contains("Thing you query is not exist")) {
                return ERROR_ACCEPT_REJECT_THING_NOT_EXIST;
            } else if (errorString.contains("You don't have permission to get state")) {
                return ERROR_LIST_STATE_NOT_THING_OWNER;
            } else if (errorString.contains("You don't have permission to set setting")) {
                return ERROR_LIST_SETTING_NOT_THING_OWNER;
            } else if (errorString.contains("You don't have permission to Get list")) {
                return ERROR_LIST_ITEM_NOT_THING_OWNER;
            } else if (errorString.contains("Call slot has already been taken by another device")) {
                return ERROR_CALL_SLOT_HAS_ALREADY_BEEN_TAKEN;
            } else {
                return ERROR_GENERAL_UNKNOWN;
            }
        }
        return ERROR_GENERAL_UNKNOWN;
    }

    public static ErrorObj parseErrorObj(String json) {
        try {
            FNJsonObject errorObj = new FNJsonObject(json);

            ErrorObj error = new ErrorObj();
            error.getDataFromJSONObject(errorObj);
            return error;
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static ErrorObj parseAuthErrorObj(String json) {
        try {
            FNJsonObject errorObj = new FNJsonObject(json);

            ErrorObj error = new ErrorObj();
            error.getAuthDataFromJSONObject(errorObj);
            return error;
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static ErrorObj parseCreateUserErrorObj(String json) {
        try {
            FNJsonObject errorObj = new FNJsonObject(json);

            ErrorObj error = new ErrorObj();
            error.getCreateUserDataFromJSONObject(errorObj);
            return error;
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return null;
    }

    private boolean getAuthDataFromJSONObject(final FNJsonObject jsonObj) {
        clean();
        if (jsonObj == null) {
            Log.e(_TAG, "Null JSON Object!");
            return false;
        }

        try {
            if (jsonObj != null) {
                _message = jsonObj.getStringDefault("error_description");
                _code = jsonObj.getStringDefault("error");
                _errorCode = transferErrorCode(_message);
            }
        } catch (Exception e) {
            e.printStackTrace();
            Log.e(_TAG, "Error occur during parsing JSON object");
            return false;
        }
        return true;
    }

    private boolean getCreateUserDataFromJSONObject(final FNJsonObject jsonObj) {
        clean();
        if (jsonObj == null) {
            Log.e(_TAG, "Null JSON Object!");
            return false;
        }

        try {
            if (jsonObj != null) {
                FNJsonObject errorObj = jsonObj.getJSONObject("error");
                _code = errorObj.getStringDefault("code");

                FNJsonObject validationObj = errorObj.getJSONObject("validation_errors");
                _message = validationObj.getString("email");
                _errorCode = transferErrorCode(_message);
            }
        } catch (Exception e) {
            e.printStackTrace();
            Log.e(_TAG, "Error occur during parsing JSON object");
            return false;
        }
        return true;
    }
}
