package com.fusionnextinc.doorbell.manager.data;

import android.content.Context;
import android.util.Log;
import android.widget.Toast;

import com.fusionnext.cloud.social.HttpData;
import com.fusionnext.cloud.utils.Utils;
import com.fusionnextinc.doorbell.R;
import com.fusionnextinc.doorbell.manager.thingsManager.ThingsHandler;
import com.fusionnextinc.doorbell.manager.thingsManager.ThingsManager;
import com.fusionnextinc.doorbell.objects.FNError;
import com.fusionnextinc.doorbell.objects.FNEvent;
import com.fusionnextinc.doorbell.objects.FNFirmware;
import com.fusionnextinc.doorbell.objects.FNGroup;
import com.fusionnextinc.doorbell.objects.FNSchedule;
import com.fusionnextinc.doorbell.objects.FNThing;
import com.fusionnextinc.doorbell.objects.FNUserProfile;
import com.fusionnextinc.doorbell.widget.FNToast;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by Mike Chang on 2016/4/6
 */
public class AppDataThing extends AppDataBase {
    protected final static String TAG = "AppDataThing";
    public final static String TAG_THING_ID = "TAG_THING_ID";
    public final static String TAG_THING_PRODUCT_ID = "TAG_THING_PRODUCT_ID";
    public final static String TAG_THING_PRODUCT_SLUG = "TAG_THING_PRODUCT_SLUG";
    public final static String TAG_THING_IDENTIFIER = "TAG_THING_IDENTIFIER";
    public final static String TAG_THING_NAME = "TAG_THING_NAME";
    public final static String TAG_THING_FIRMWARE_VERSION = "TAG_THING_FIRMWARE_VERSION";
    public final static String TAG_THING_RING_TONE = "TAG_THING_RING_TONE";
    public final static String TAG_THING_BATTERY_LEVEL = "TAG_THING_BATTERY_LEVEL";
    public final static String TAG_THING_CREATED_AT = "TAG_THING_CREATED_AT";
    public final static String TAG_THING_UPDATED_AT = "TAG_THING_UPDATED_AT";
    public final static String TAG_THING_READED_AT = "TAG_THING_READED_AT";
    public final static String TAG_THING_USER_ID = "TAG_THING_USER_ID";
    public final static String TAG_THING_USER_NAME = "TAG_THING_USER_NAME";
    public final static String TAG_THING_USER_EMAIL = "TAG_THING_USER_EMAIL";
    public final static String TAG_THING_EVENT_COUNTS = "TAG_THING_EVENT_COUNTS";
    public final static String TAG_THING_ORGANIZATION_ID = "TAG_THING_ORGANIZATION_ID";
    public final static String TAG_THING_ONLINE = "TAG_THING_ONLINE";
    public final static String TAG_THING_SWITCHED = "TAG_THING_SWITCHED";

    private volatile static AppDataThing instance;
    private Context context;
    private AppDataHandler.AppDataThingListener appDataThingListener = null;

    private AppDataThing(Context context) {
        this.context = context;
        ThingsManager.getInstance(context).setThingsListener(onThingsListener);
    }

    public static AppDataThing getInstance(Context context) {
        if (instance == null) {
            synchronized (AppDataThing.class) {
                if (instance == null) {
                    instance = new AppDataThing(context);
                }
            }
        }
        return instance;
    }

    @Override
    public ArrayList<String> getList(int tag) {
        return super.getList(tag);
    }

    @Override
    public void initList(boolean cleanIfExist) {
        super.initList(cleanIfExist);
    }

    // get the total numbers of saved share items
    public int getThingsSize(int type) {
        if (getList(type) == null) return 0;
        return getList(type).size();
    }

    public boolean deleteThing(int type, int index, int thingType) {
        ArrayList<String> list = getList(type);
        if (list == null || list.size() <= 0) return false;
        if (index >= list.size()) return false;
        String id = list.get(index);
        if (id == null || id.endsWith("")) return false;

        return deleteThing(id);
    }

    public boolean deleteThing(String thingId) {
        return DataManager.getInstance(context).deleteThing(thingId);
    }

    //Ready the adapter data, this class will not save the adapter data
    public ListMap getAdapterData(int type) {
        return getAdapterData(type, null);
    }

    public ListMap getAdapterData(int type, String groupId) {
        ListMap adapter = new ListMap();
        ArrayList<String> list = DataManager.getInstance(context).loadThingList(type, groupId);//getList(type);
        boolean needUpdate = false;
        ArrayList<String> removeList = new ArrayList<>();
        adapter.clear();
        if (list == null || list.size() <= 0) return adapter;

        for (String str : list) {
            Map<String, Object> map = getThingMapData(str);
            if (map != null) {
                adapter.add(map);
            } else {
                needUpdate = true;
                removeList.add(str);
            }
        }

        for (String removeStr : removeList) {
            list.remove(removeStr);
        }

        if (needUpdate) {
            DataManager.getInstance(context).saveThingList(type, groupId, getList(type));
            updateList(type, list);
            if (groupId != null) {
                FNGroup fnGroup = DataManager.getInstance(context).loadGroup(Integer.valueOf(groupId));
                fnGroup.setDeviceCount(list.size());
                DataManager.getInstance(context).saveGroup(fnGroup);
            }
        }
        return adapter;
    }

    public Map<String, Object> getThingMapData(String thingId) {
        FNThing fnThing = DataManager.getInstance(context).loadThing(thingId);
        Map<String, Object> map = null;

        if (fnThing != null) {
            map = new HashMap<>();
            map.put(TAG_THING_ID, fnThing.getThingId());
            map.put(TAG_THING_PRODUCT_ID, fnThing.getProductId());
            map.put(TAG_THING_PRODUCT_SLUG, fnThing.getProductSlug());
            map.put(TAG_THING_IDENTIFIER, fnThing.getIdentifier());
            map.put(TAG_THING_NAME, fnThing.getName());
            map.put(TAG_THING_FIRMWARE_VERSION, fnThing.getFirmwareVer());
            map.put(TAG_THING_RING_TONE, fnThing.getRingtoneId());
            map.put(TAG_THING_BATTERY_LEVEL, fnThing.getBatteryLevel());
            map.put(TAG_THING_CREATED_AT, fnThing.getCreatedAt());
            map.put(TAG_THING_UPDATED_AT, fnThing.getUpdatedAt());
            map.put(TAG_THING_READED_AT, fnThing.getReadedAt());
            map.put(TAG_THING_USER_ID, fnThing.getUserId());
            map.put(TAG_THING_USER_NAME, fnThing.getUserName());
            map.put(TAG_THING_USER_EMAIL, fnThing.getUserEmail());
            map.put(TAG_THING_EVENT_COUNTS, fnThing.getEventCounts());
            map.put(TAG_THING_ORGANIZATION_ID, fnThing.getOrganizationId());
            map.put(TAG_THING_ONLINE, fnThing.getOnline() );
            map.put(TAG_THING_SWITCHED, fnThing.getSwitched() == 1 ? true : false);
        }
        return map;
    }

    public FNThing getThing(String thingId) {
        return DataManager.getInstance(context).loadThing(thingId);
    }

    public boolean saveThing(FNThing fnThing) {
        return DataManager.getInstance(context).saveThing(fnThing);
    }

    public void loadAllThingsList(boolean onlineData, boolean onlineIfNoData) {
        //loadThingsList(AppDataThing.THING_TYPE_THINGS_OWNED, onlineData, onlineIfNoData);
        //loadThingsList(AppDataThing.THING_TYPE_THINGS_SHARED, onlineData, onlineIfNoData);
        //loadThingsList(AppDataThing.THING_TYPE_THINGS_INVITED, onlineData, onlineIfNoData);
        loadThingsList(AppDataThing.THING_TYPE_THINGS_ALL, onlineData, onlineIfNoData);
    }

    private void loadThingsList(int type, boolean onlineData, boolean onlineIfNoData) {
        QueryStatus status = getQueryStatus(type);
        if (status == null) status = new QueryStatus();
        status.setQueryState(true);
        updateQueryStatus(type, status);

        if (onlineData) {
            startThingsListTask(type, true);
            return;
        }

        ArrayList<String> list = DataManager.getInstance(context).loadThingList(type, null);//getList(type);
        if ((list == null || list.size() <= 0) && onlineIfNoData) {
            if (!Utils.isNetworkConnected(context)) {
                Log.d(TAG, "Network is not available");
                return;
            }
            startThingsListTask(type, true);
            return;
        }

        if (!Utils.isNetworkConnected(context)) {
            Log.d(TAG, "Network is not available");
            return;
        }

        status.setQueryEnd(true);
        updateQueryStatus(type, status);
        refreshThingsAdapter(type);
    }

    public boolean hasMoreData(int tag) {
        QueryStatus status = getQueryStatus(tag);
        if (status == null) return false;
        return !status.isQueryEnd();
    }

    public boolean updateThing(int type, FNThing fnThing) {
        ArrayList<String> list = new ArrayList<>();

        list.clear();
        QueryStatus status = getQueryStatus(type);
        if (!status.isNewQuery()) {
            list.addAll(getList(type));
        }

        if (fnThing == null) return false;
        list.add(fnThing.getThingId());

        if (status.isQueryEnd()) {
            status.setLastItemId("");
        } else {
            int lastIndex = list.size();
            if (lastIndex > 0) {
                status.setLastItemId(String.valueOf(lastIndex));
            }
        }
        updateQueryStatus(type, status);
        //Put this list to list cache
        updateList(type, list);
        //Save this list to database
        DataManager.getInstance(context).saveThingList(type, null, getList(type));
        return true;
    }

    public boolean saveThingsList(int type, String groupId, ArrayList<String> list) {
        //Save this list to database
        DataManager.getInstance(context).saveThingList(type, groupId, list);
        return true;
    }

    public boolean updateThingsList(int type, ArrayList<FNThing> thingList) {
        updateThingsList(type, null, thingList);
        return true;
    }

    public boolean updateThingsList(int type, String groupId, ArrayList<FNThing> thingList) {
        ArrayList<String> list = new ArrayList<>();

        list.clear();
        QueryStatus status = getQueryStatus(type);

        if (!status.isNewQuery()) {
            list.addAll(getList(type));
        }

        if (thingList == null) {
            status.setQueryEnd(true);
            status.setLastItemId("");
            updateQueryStatus(type, status);
            return false;
        }
        for (int i = 0; i < thingList.size(); i++) {
            FNThing fnThing = thingList.get(i);
            if (fnThing == null) return false;
            DataManager.getInstance(context).saveThing(fnThing);
            list.add(fnThing.getThingId());
        }

        if (thingList.size() < status.getPageSize()) {
            status.setQueryEnd(true);
        }

        if (status.isQueryEnd()) {
            status.setLastItemId("");
        } else {
            int lastIndex = list.size();
            if (lastIndex > 0) {
                //status.setLastItemId( list.get(lastIndex - 1));
                status.setLastItemId(String.valueOf(lastIndex));
            }
        }
        updateQueryStatus(type, status);

        //Put this list to list cache
        updateList(type, list);
        //Save this list to database
        DataManager.getInstance(context).saveThingList(type, groupId, getList(type));
        return true;
    }

    private void refreshThingsData(int type, boolean isNewQuery) {
        QueryStatus status = getQueryStatus(type);
        status.setQueryState(isNewQuery);
        startThingsListTask(type, isNewQuery);
    }

    public void getNextThingsPage(int type) {
        if (!Utils.isNetworkConnected(context)) {
            FNToast.makeText(context, context.getString(R.string.fn_msg_no_network), Toast.LENGTH_SHORT).show();
        } else {
            startThingsListTask(type, false);
        }
    }

    //Start an ASyncTask to get the share list from server
    public void startThingsListTask(final int type, boolean isNewQuery) {
        QueryStatus status = getQueryStatus(type);
        if (isNewQuery) {
            //Clear the loaded data
            clearList(type);
        }

        status.setQueryState(isNewQuery);
        updateQueryStatus(type, status);

        switch (type) {
            case THING_TYPE_THINGS_OWNED:
                ThingsManager.getInstance(context).setThingsListener(onThingsListener);
                ThingsManager.getInstance(context).getOwnedThingsList(HttpData._GET_TYPE_SORT_ORDER_DESC, null, null);
                break;
            case THING_TYPE_THINGS_INVITED:
                ThingsManager.getInstance(context).setThingsListener(onThingsListener);
                ThingsManager.getInstance(context).getInvitedThingsList(HttpData._GET_TYPE_SORT_ORDER_DESC, null, null);
                break;
            case THING_TYPE_THINGS_SHARED:
                ThingsManager.getInstance(context).setThingsListener(onThingsListener);
                ThingsManager.getInstance(context).getSharedThingsList(HttpData._GET_TYPE_SORT_ORDER_DESC, null, null);
                break;
            case THING_TYPE_THINGS_ALL:
                ThingsManager.getInstance(context).setThingsListener(onThingsListener);
                ThingsManager.getInstance(context).getAllThingsList(HttpData._GET_TYPE_SORT_ORDER_DESC, null, null);
                break;
            default:
                break;
        }
    }

    public void updateThingInfo(String thingId) {
        ThingsManager.getInstance(context).setThingsListener(onThingsListener);
        ThingsManager.getInstance(context).getThingInfo(thingId);
    }

    private final ThingsHandler.ThingsListener onThingsListener = new ThingsHandler.ThingsListener() {
        @Override
        public void onListThingEvents(ThingsManager instance, boolean result, String thingId, String eventName, ArrayList<FNEvent> eventList, FNError error) {

        }

        @Override
        public void onCurFirmwareVersion(ThingsManager instance, boolean result, String thingId, String fwVersion, FNError error) {

        }

        @Override
        public void onCurSwitchedStatus(ThingsManager instance, boolean result, String thingId, boolean isSwitch, FNError error) {

        }

        @Override
        public void onNewFirmwareVersion(ThingsManager instance, boolean result, String thingId, FNFirmware fnFirmware) {

        }

        @Override
        public void onStartFwUpdate(ThingsManager instance, boolean result, String thingId, String fwId) {

        }

        @Override
        public void onCurRingTone(ThingsManager instance, boolean result, String thingId, String ringtoneId, FNError error) {

        }

        @Override
        public void onGetScheduleList(ThingsManager instance, boolean result, String thingId, ArrayList<FNSchedule> scheduleList, FNError error) {

        }

        @Override
        public void onListInvitedUsers(ThingsManager instance, boolean result, String thingId, ArrayList<FNUserProfile> userList, FNError error) {

        }

        @Override
        public void onListSharedUsers(ThingsManager instance, boolean result, String thingId, ArrayList<FNUserProfile> userList, FNError error) {

        }

        @Override
        public void onCreateClaimCode(ThingsManager instance, boolean result, String claimCode, FNError error) {

        }

        @Override
        public void onCreateThing(ThingsManager instance, boolean result, FNThing fnThing, FNError error) {

        }

        @Override
        public void onUpdateThing(ThingsManager instance, boolean result, FNThing fnThing, FNError error) {

        }

        @Override
        public void onDeleteThing(ThingsManager instance, boolean result, FNError error) {

        }

        @Override
        public void onGetThing(ThingsManager instance, boolean result, FNThing fnThing, FNError error) {
            if (result && appDataThingListener != null) {
                appDataThingListener.onRefreshBatteryLevel(fnThing.getThingId());
            }
        }

        @Override
        public void onChangeRingtone(ThingsManager instance, boolean result, String thingId, int ringtoneId) {

        }

        @Override
        public void onSetTimer(ThingsManager instance, boolean result, String thingId, String value) {

        }

        @Override
        public void onSetSchedule(ThingsManager instance, boolean result, String thingId, String value) {

        }

        @Override
        public void onShareInvited(ThingsManager instance, boolean result, String thingId, FNError error) {

        }

        @Override
        public void onShareAccepted(ThingsManager instance, boolean result, String thingId, FNError error) {

        }

        @Override
        public void onShareRejected(ThingsManager instance, boolean result, String thingId, FNError error) {

        }

        @Override
        public void onSwitchOnOff(ThingsManager instance, boolean result, String thingId, boolean isTurnOn) {

        }

        @Override
        public void onCloseCallSlot(ThingsManager instance, boolean result, FNError error) {

        }

        @Override
        public void onTakeCallSlot(ThingsManager instance, boolean result, String thingId, String callSlot, FNError error) {

        }

        @Override
        public void onListUserOwnedThings(ThingsManager instance, boolean result, String userId, ArrayList<FNThing> thingArrayList, FNError error) {
            if (result) {
                refreshThingsAdapter(THING_TYPE_THINGS_OWNED);
            }
        }

        @Override
        public void onListUserInvitedThings(ThingsManager instance, boolean result, String userId, ArrayList<FNThing> thingArrayList, FNError error) {
            if (result) {
                refreshThingsAdapter(THING_TYPE_THINGS_INVITED);
            }
        }

        @Override
        public void onListUserSharedThings(ThingsManager instance, boolean result, String userId, ArrayList<FNThing> thingArrayList, FNError error) {
            if (result) {
                refreshThingsAdapter(THING_TYPE_THINGS_SHARED);
            }
        }

        @Override
        public void onListUserAllThings(ThingsManager instance, boolean result, String userId, ArrayList<FNThing> ownedThingArrayList, ArrayList<FNThing> sharedThingArrayList, ArrayList<FNThing> invitedThingArrayList, FNError error) {
            if (result) {
                refreshThingsAdapter(THING_TYPE_THINGS_ALL);
//                ThingsManager.getInstance(context).subscribeTopic(getAdapterData(AppDataThing.THING_TYPE_THINGS_OWNED), getAdapterData(AppDataThing.THING_TYPE_THINGS_SHARED));
            }
        }

        @Override
        public void onListThingEventsByTime(ThingsManager instance, boolean result, String thingId, String eventName, String startTime, String endTime, ArrayList<FNEvent> eventList, FNError error) {
            if (result && appDataThingListener != null) {
                appDataThingListener.onRefreshEventCounts(thingId);
            }
        }

        @Override
        public void onCurBatteryLevel(ThingsManager instance, boolean result, String thingId, String batteryLevel, FNError error) {
            if (result && appDataThingListener != null) {
                appDataThingListener.onRefreshBatteryLevel(thingId);
            }
        }

    };


    private void refreshThingsAdapter(int type) {
        if (appDataThingListener != null) {
            appDataThingListener.onRefreshListAdapter(type);
        }
    }

    public void refreshThingsListData() {
        //refreshThingsData(AppDataThing.THING_TYPE_THINGS_OWNED, true);
        //refreshThingsData(AppDataThing.THING_TYPE_THINGS_SHARED, true);
        refreshThingsData(AppDataThing.THING_TYPE_THINGS_ALL, true);
    }

    public void setAppDataThingListener(AppDataHandler.AppDataThingListener listener) {
        this.appDataThingListener = listener;
    }
}
