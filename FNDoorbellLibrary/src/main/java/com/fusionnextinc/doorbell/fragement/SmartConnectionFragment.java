package com.fusionnextinc.doorbell.fragement;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.location.LocationManager;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.os.Build;
import android.os.Bundle;
import android.text.method.HideReturnsTransformationMethod;
import android.text.method.PasswordTransformationMethod;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.RequiresApi;
import androidx.annotation.StringRes;
import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.FragmentActivity;

import com.fusionnext.cloud.iot.objects.ErrorObj;
import com.fusionnextinc.doorbell.DefaultAppConfig;
import com.fusionnextinc.doorbell.MyApplication;
import com.fusionnextinc.doorbell.R;

import com.fusionnextinc.doorbell.manager.smartConnection.SmartConnectionHandler;
import com.fusionnextinc.doorbell.manager.smartConnection.SmartConnectionManager;
import com.fusionnextinc.doorbell.manager.thingsManager.ThingsHandler;
import com.fusionnextinc.doorbell.manager.thingsManager.ThingsManager;
import com.fusionnextinc.doorbell.objects.FNError;
import com.fusionnextinc.doorbell.objects.FNEvent;
import com.fusionnextinc.doorbell.objects.FNFirmware;
import com.fusionnextinc.doorbell.objects.FNSchedule;
import com.fusionnextinc.doorbell.objects.FNThing;
import com.fusionnextinc.doorbell.objects.FNUserProfile;
import com.fusionnextinc.doorbell.utils.FNLog;
import com.fusionnextinc.doorbell.utils.Utils;
import com.fusionnextinc.doorbell.widget.FNAlert.FNAlertDialog;
import com.fusionnextinc.doorbell.widget.FNDialog;
import com.fusionnextinc.doorbell.widget.FNLayoutUtil;
import com.fusionnextinc.doorbell.widget.FNProgressDialog;

import java.util.ArrayList;

import static androidx.core.content.ContextCompat.getSystemService;

/**
 * Created by Mike Chang on 2017/6/22
 */
public class SmartConnectionFragment extends FNFragment implements View.OnClickListener {
    private static final String TAG = SmartConnectionFragment.class.getSimpleName();

    private FNLayoutUtil layoutUtil;
    private EditText editSsid;
    private EditText editPassword;
    private EditText editName;
    private Button btnConnect, btnManualPair;
    private ImageView ivShowPwd;
    private FNProgressDialog progressBar;
    private SmartConnectionManager smartConnectionManager;
    private ThingsManager thingsManager;
    private static int SMARTCONN_TIMEOUT = 60000;
    private static int WIFI_RSSI_LIMITATION = -75;
    private String mClaimCode = null;
    private static int smartconnType;

    private enum PairingMode {
        PAIRING_MODE_AUTO, PAIRING_MODE_MANUAL
    }

    private PairingMode mPairingMode = PairingMode.PAIRING_MODE_AUTO;

    private static void showGPSDisabledAlertToUser(FragmentActivity activity){
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(activity);
        alertDialogBuilder.setMessage(activity.getString(R.string.need_gps))
                .setCancelable(false)
                .setPositiveButton(activity.getString(R.string.enable_gps),
                        new DialogInterface.OnClickListener(){
                            public void onClick(DialogInterface dialog, int id){
                                Intent callGPSSettingIntent = new Intent(
                                        android.provider.Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                                activity.startActivity(callGPSSettingIntent);
                            }
                        });
        alertDialogBuilder.setNegativeButton(activity.getString(R.string.btn_cancel),
                new DialogInterface.OnClickListener(){
                    public void onClick(DialogInterface dialog, int id){
                        dialog.cancel();
                    }
                });
        AlertDialog alert = alertDialogBuilder.create();
        alert.show();
    }

    public static void openFragment( boolean clearOtherStack, int type) {

            smartconnType = type;
            FNFragmentManager.getInstance().openFragment(new SmartConnectionFragment(), clearOtherStack);
    }

    public static void openFragment(FragmentActivity activity , boolean clearOtherStack, int type) {

        LocationManager locationManager = (LocationManager) activity.getSystemService(Context.LOCATION_SERVICE);

        if (locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)){
            openFragment(clearOtherStack,type);

        }else{
            showGPSDisabledAlertToUser(activity);
        }

    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        layoutUtil = new FNLayoutUtil(getActivity(), MyApplication.LAYOUT_WIDTH, MyApplication.LAYOUT_HEIGHT, FNLayoutUtil.BASE_WIDTH);
        smartConnectionManager = SmartConnectionManager.getInstance(getContext());
        thingsManager = ThingsManager.getInstance(getActivity());


    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        final ViewGroup nullParent = null;

        getActivity().setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        getActionBar().resetAll();
        getActionBar().setTitle(getString(R.string.fn_tittle_smartconn_add_things).replace("${Thing}", getActivity().getString(R.string.fn_title_thing_name)), Gravity.CENTER, null);
        getActionBar().setCoverMode(false);
        getActionBar().setStartImage(R.drawable.back, new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        getActionBar().addEndImage(R.drawable.btn_nav_info, onPairingInfoClickListener);
        getActionBar().show();
        View view = inflater.inflate(R.layout.fragment_smart_connection, nullParent);
        layoutUtil.fitAllView(view);

        editSsid = (EditText) view.findViewById(R.id.et_ssid);
        String ssid = getWifiName(getActivity());

        if (ssid != null) {
            ssid = ssid.replaceAll("\"", "");
            editSsid.setText(ssid);
        }
        editSsid.setEnabled(false);
        editPassword = (EditText) view.findViewById(R.id.et_password);
        TextView textName = (TextView) view.findViewById(R.id.text_name);
        editName = (EditText) view.findViewById(R.id.et_name);
        btnConnect = (Button) view.findViewById(R.id.btn_connect);
        btnManualPair = (Button) view.findViewById(R.id.btn_manual_pairing);
        btnManualPair.setEnabled(true);
        btnManualPair.setBackgroundResource(R.drawable.btn_shape);
        ivShowPwd = (ImageView) view.findViewById(R.id.btn_show_password);
        ivShowPwd.setTag(R.drawable.show_password_pressed);
        ivShowPwd.setImageDrawable(getResources().getDrawable(R.drawable.show_password_pressed));
        editPassword.setTransformationMethod(HideReturnsTransformationMethod.getInstance());
        btnConnect.setOnClickListener(this);
        btnManualPair.setOnClickListener(this);
        ivShowPwd.setOnClickListener(this);
        textName.setText(getActivity().getString(R.string.fn_tittle_smartconn_name).replace("${Thing}", getActivity().getString(R.string.fn_title_thing_name)));

        if (smartconnType == SmartConnectionManager.SMART_CONNECTION_CHANGE_SSID) {
            getActionBar().resetAll();
            getActionBar().setTitle(getString(R.string.fn_tittle_settings_change_ssid), Gravity.CENTER, null);
            getActionBar().setCoverMode(false);
            getActionBar().setStartImage(R.drawable.back, new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    onBackPressed();
                }
            });
            getActionBar().addEndImage(R.drawable.btn_nav_info, onPairingInfoClickListener);
            getActionBar().show();
            editPassword.setImeOptions(EditorInfo.IME_ACTION_DONE);
            textName.setVisibility(View.GONE);
            editName.setVisibility(View.GONE);
        }
        return view;
    }

    @Override
    public void onClick(View view) {
        if (Utils.isNetworkConnected(getActivity())) {
            WifiManager wifiManager = (WifiManager) getContext().getApplicationContext().getSystemService(Context.WIFI_SERVICE);
            WifiInfo wifiInfo = wifiManager.getConnectionInfo();
            FNLog.d(TAG, "Connect RSSI:" + wifiInfo.getRssi());

            if (view.getId() == R.id.btn_connect) {
                String name = editName.getText().toString().trim();
                if (name.length() == 0) {
                    name = getString(R.string.app_name);
                }
                // must be a check for not null, for empty field and for email validity(matchs email pattern)
                if (Utils.emptyFieldsRemaining(editSsid, editPassword)) {
                    FNDialog.showErrorDialog(getActivity(), getString(R.string.fn_dialog_are_you_sure), getString(R.string.fn_msg_login_fields_are_miss));
                    return;
                } else if (wifiInfo.getRssi() < WIFI_RSSI_LIMITATION) {
                    btnManualPair.setEnabled(true);
                    btnManualPair.setBackgroundResource(R.drawable.btn_shape);
                    showWifiSignalWeakDialog();
                    return;
                }
                mPairingMode = PairingMode.PAIRING_MODE_AUTO;
                startWait();
                thingsManager.setThingsListener(thingsListener);
                thingsManager.createClaimCode(name);
            } else if (view.getId() == R.id.btn_show_password) {
                if ((Integer) ivShowPwd.getTag() == R.drawable.show_password) {
                    ivShowPwd.setImageDrawable(getResources().getDrawable(R.drawable.show_password_pressed));
                    ivShowPwd.setTag(R.drawable.show_password_pressed);
                    editPassword.setTransformationMethod(HideReturnsTransformationMethod.getInstance());
                } else {
                    ivShowPwd.setImageDrawable(getResources().getDrawable(R.drawable.show_password));
                    ivShowPwd.setTag(R.drawable.show_password);
                    editPassword.setTransformationMethod(PasswordTransformationMethod.getInstance());
                }
            } else if (view.getId() == R.id.btn_manual_pairing) {
                String name = editName.getText().toString().trim();
                if (name.length() == 0) {
                    name = getString(R.string.app_name);
                }
                mPairingMode = PairingMode.PAIRING_MODE_MANUAL;
                thingsManager.setThingsListener(thingsListener);
                thingsManager.createClaimCode(name);
            }
        } else {
            FNDialog.showNoNetworkDialog(getActivity());
        }
    }

    private void closeKeyboard() {
        if (getActivity().getCurrentFocus() != null && getActivity().getCurrentFocus().getWindowToken() != null) {
            InputMethodManager inputManager = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
            inputManager.hideSoftInputFromWindow(getActivity().getCurrentFocus().getWindowToken(), 0);
        }
    }

    private void startWait() {
        if (progressBar == null) {
            progressBar = new FNProgressDialog(getContext())
                    .show();
            progressBar.startAnimation(0, SMARTCONN_TIMEOUT);
        }
    }

    private void stopWait() {
        if (progressBar != null) {
            progressBar.updateProgress(100);
            progressBar.stopAnimation();
            progressBar.dismiss();
            progressBar = null;
        }
        btnConnect.setText(getString(R.string.fn_btn_smartconn_connect));
    }

    @Override
    public void onPause() {
        super.onPause();
        stopWait();
    }

    final SmartConnectionHandler.SmartConnectionListener smartConnectionListener = new SmartConnectionHandler.SmartConnectionListener() {
        @Override
        public void onThingFound(SmartConnectionManager instance, final FNThing fnThing, final int rssi) {
            if (isAdded()) {
                MyApplication.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        if (smartconnType == SmartConnectionManager.SMART_CONNECTION_CREATE_THING) {
                            progressBar.setProgressTitle(rssi + " " + getString(R.string.fn_msg_smartconn_connecting));
                        } else if (smartconnType == SmartConnectionManager.SMART_CONNECTION_CHANGE_SSID) {
                            progressBar.setProgressTitle(rssi + " " + getString(R.string.fn_msg_smartconn_connecting));
                        }
                    }
                });
            }
        }

        @Override
        public void onThingRegistered(SmartConnectionManager instance, final FNThing fnThing, final ErrorObj error, int rssi) {
            MyApplication.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    if (error == null || error._code.length() == 0) {
                        if (isAdded()) {
                            closeKeyboard();
                            stopWait();
                            addThingDialog(true);
                            ThingsFragment thingsFragment = new ThingsFragment();

                            thingsFragment.setUpdateThing(true, null);

                            if (DefaultAppConfig.APP_SOLY) {
                                HomeFragment.openFragment(true, ThingsFragment.THINGS_FRAGMENT_TYPE_NORMAL, null, HomeFragment.TabType.TAB_TYPE_DEVICE, false);
                            } else {
                                ThingsFragment.openFragment(true, ThingsFragment.THINGS_FRAGMENT_TYPE_NORMAL, null);
                            }
                        }
                    } else {
                        String title = getString(R.string.fn_msg_smartconn_add_thing_failed).replace("${Thing}", getActivity().getString(R.string.fn_title_thing_name));
                        FNDialog.showErrorDialog(getContext(), title, FNError.parseError(error));
                        closeKeyboard();
                        stopWait();
                    }
                }
            });
        }

        @Override
        public void onThingNotFound(SmartConnectionManager instance) {
            if (isAdded()) {
                MyApplication.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        btnManualPair.setEnabled(true);
                        btnManualPair.setBackgroundColor(getContext().getResources().getColor(R.color.main_bg_level1));
                        showThingNotFoundDialog();
                        closeKeyboard();
                        stopWait();
                    }
                });
            }
        }

        @Override
        public void onThingOtaStart(SmartConnectionManager instance, FNThing fnThing) {

        }

        @Override
        public void onThingSsidChanged(SmartConnectionManager instance, FNThing fnThing, int rssi) {
            if (isAdded()) {
                MyApplication.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        if (smartconnType == SmartConnectionManager.SMART_CONNECTION_CHANGE_SSID) {
                            showChangeSsidDialog();
                            closeKeyboard();
                            stopWait();
                            onBackPressed();
                        }
                    }
                });
            }
        }
    };

    final ThingsHandler.ThingsListener thingsListener = new ThingsHandler.ThingsListener() {

        @Override
        public void onListUserOwnedThings(ThingsManager instance, boolean result, String userId, ArrayList<FNThing> thingArrayList, FNError error) {

        }

        @Override
        public void onListUserInvitedThings(ThingsManager instance, boolean result, String userId, ArrayList<FNThing> thingArrayList, FNError error) {

        }

        @Override
        public void onListUserSharedThings(ThingsManager instance, boolean result, String userId, ArrayList<FNThing> thingArrayList, FNError error) {

        }

        @Override
        public void onListUserAllThings(ThingsManager instance, boolean result, String userId, ArrayList<FNThing> ownedThingArrayList, ArrayList<FNThing> sharedThingArrayList, ArrayList<FNThing> invitedThingArrayList, FNError error) {

        }

        @Override
        public void onListThingEvents(ThingsManager instance, boolean result, String thingId, String eventName, ArrayList<FNEvent> eventList, FNError error) {

        }

        @Override
        public void onListThingEventsByTime(ThingsManager instance, boolean result, String thingId, String eventName, String startTime, String endTime, ArrayList<FNEvent> eventList, FNError error) {

        }

        @Override
        public void onCurBatteryLevel(ThingsManager instance, boolean result, String thingId, String batteryLevel, FNError error) {

        }

        @Override
        public void onCurFirmwareVersion(ThingsManager instance, boolean result, String thingId, String fwVersion, FNError error) {

        }

        @Override
        public void onCurSwitchedStatus(ThingsManager instance, boolean result, String thingId, boolean isSwitch, FNError error) {

        }

        @Override
        public void onNewFirmwareVersion(ThingsManager instance, boolean result, String thingId, FNFirmware fnFirmware) {

        }

        @Override
        public void onStartFwUpdate(ThingsManager instance, boolean result, String thingId, String fwId) {

        }

        @Override
        public void onCurRingTone(ThingsManager instance, boolean result, String thingId, String ringtoneId, FNError error) {

        }

        @Override
        public void onGetScheduleList(ThingsManager instance, boolean result, String thingId, ArrayList<FNSchedule> scheduleList, FNError error) {

        }

        @Override
        public void onListInvitedUsers(ThingsManager instance, boolean result, String thingId, ArrayList<FNUserProfile> userList, FNError error) {

        }

        @Override
        public void onListSharedUsers(ThingsManager instance, boolean result, String thingId, ArrayList<FNUserProfile> userList, FNError error) {

        }

        @Override
        public void onCreateClaimCode(ThingsManager instance, boolean result, String claimCode, FNError error) {
            String connectSsid = getWifiName(getActivity());
            String ssid = editSsid.getText().toString();
            String pwd = editPassword.getText().toString().trim();

            if (smartconnType == SmartConnectionManager.SMART_CONNECTION_CREATE_THING) {
                mClaimCode = claimCode;
            } else {
                mClaimCode = "";
            }
            if (mPairingMode.equals(PairingMode.PAIRING_MODE_MANUAL)) {
                stopWait();
                onBackPressed();
                ManualPairingFragment.openFragment(false, mClaimCode, smartconnType);
            } else {
                if (connectSsid != null) {
                    connectSsid = connectSsid.replaceAll("\"", "");
                }

                if (connectSsid != null && !connectSsid.equals(ssid)) {
                    FNDialog.showErrorDialog(getActivity(), getString(R.string.fn_dialog_are_you_sure), getString(R.string.fn_msg_smartconn_environment_warning));
                    return;
                }

                if (ssid.length() > 0) {
                    smartConnectionManager.setSmartConnectionListener(smartConnectionListener);
                    smartConnectionManager.start(ssid, pwd, mClaimCode, SMARTCONN_TIMEOUT, smartconnType);
                    btnConnect.setText(getString(R.string.fn_btn_smartconn_connecting));
                }
            }
        }

        @Override
        public void onUpdateThing(ThingsManager instance, boolean result, FNThing fnThing, FNError error) {

        }

        @Override
        public void onDeleteThing(ThingsManager instance, boolean result, FNError error) {

        }

        @Override
        public void onGetThing(ThingsManager instance, boolean result, FNThing fnThing, FNError error) {

        }

        @Override
        public void onChangeRingtone(ThingsManager instance, boolean result, String thingId, int ringtoneId) {

        }

        @Override
        public void onSetTimer(ThingsManager instance, boolean result, String thingId, String value) {

        }

        @Override
        public void onSetSchedule(ThingsManager instance, boolean result, String thingId, String value) {

        }

        @Override
        public void onShareInvited(ThingsManager instance, boolean result, String thingId, FNError error) {

        }

        @Override
        public void onShareAccepted(ThingsManager instance, boolean result, String thingId, FNError error) {

        }

        @Override
        public void onShareRejected(ThingsManager instance, boolean result, String thingId, FNError error) {

        }

        @Override
        public void onSwitchOnOff(ThingsManager instance, boolean result, String thingId, boolean isTurnOn) {

        }

        @Override
        public void onCloseCallSlot(ThingsManager instance, boolean result, FNError error) {

        }

        @Override
        public void onTakeCallSlot(ThingsManager instance, boolean result, String thingId, String callSlot, FNError error) {

        }

        @Override
        public void onCreateThing(ThingsManager instance, boolean result, FNThing fnThing, FNError error) {
            if (isAdded()) {
                closeKeyboard();
                stopWait();
                if (result) {
                    addThingDialog(true);
                    ThingsFragment thingsFragment = new ThingsFragment();

                    thingsFragment.setUpdateThing(true, null);
                    if (DefaultAppConfig.APP_SOLY) {
                        HomeFragment.openFragment(true, ThingsFragment.THINGS_FRAGMENT_TYPE_NORMAL, null, HomeFragment.TabType.TAB_TYPE_DEVICE, false);
                    } else {
                        thingsFragment.openFragment(true, ThingsFragment.THINGS_FRAGMENT_TYPE_NORMAL, null);
                    }
                } else {
                    FNDialog.showErrorDialog(getActivity(), getString(R.string.fn_tittle_smartconn_add_things).replace("${Thing}", getActivity().getString(R.string.fn_title_thing_name)), error);
                }
            }
        }
    };

    public String getWifiName(Context context) {
        WifiManager manager = (WifiManager) context.getApplicationContext().getSystemService(Context.WIFI_SERVICE);
        if (manager.isWifiEnabled()) {
            WifiInfo wifiInfo = manager.getConnectionInfo();
            if (wifiInfo != null) {
                return wifiInfo.getSSID();
            }
        }
        return null;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        smartConnectionManager.stop();
    }

    private void showWifiSignalWeakDialog() {
        String title = getActivity().getString(R.string.fn_tittle_smartconn_add_things).replace("${Thing}", getActivity().getString(R.string.fn_title_thing_name));

        if (smartconnType == SmartConnectionManager.SMART_CONNECTION_CREATE_THING) {
            title = getActivity().getString(R.string.fn_tittle_smartconn_add_things).replace("${Thing}", getActivity().getString(R.string.fn_title_thing_name));
        } else if (smartconnType == SmartConnectionManager.SMART_CONNECTION_CHANGE_SSID) {
            title = getActivity().getString(R.string.fn_tittle_settings_change_ssid);
        }
        new FNAlertDialog(getActivity(), FNAlertDialog.WARNING_TYPE)
                .setTitleText(title)
                .setContentText(getActivity().getString(R.string.fn_msg_smartconn_wifi_signal_is_weak).replace("${Thing}", getActivity().getString(R.string.fn_title_thing_name)))
                .setConfirmText(getActivity().getString(R.string.fn_dialog_ok))
                .showCancelButton(false)
                .setConfirmClickListener(new FNAlertDialog.OnSweetClickListener() {
                    @Override
                    public void onClick(FNAlertDialog sDialog) {
                        sDialog.dismiss();
//                        ManualPairingFragment.openFragment(false, mClaimCode, smartconnType);
                    }
                })
                .show();
    }

    private void showThingNotFoundDialog() {
        String title = getActivity().getString(R.string.fn_tittle_smartconn_add_things).replace("${Thing}", getActivity().getString(R.string.fn_title_thing_name));

        if (smartconnType == SmartConnectionManager.SMART_CONNECTION_CREATE_THING) {
            title = getActivity().getString(R.string.fn_tittle_smartconn_add_things).replace("${Thing}", getActivity().getString(R.string.fn_title_thing_name));
        } else if (smartconnType == SmartConnectionManager.SMART_CONNECTION_CHANGE_SSID) {
            title = getActivity().getString(R.string.fn_tittle_settings_change_ssid);
        }
        new FNAlertDialog(getActivity(), FNAlertDialog.WARNING_TYPE)
                .setTitleText(title)
                .setContentText(getActivity().getString(R.string.fn_msg_smartconn_not_found_thing).replace("${Thing}", getActivity().getString(R.string.fn_title_thing_name)))
                .setConfirmText(getActivity().getString(R.string.fn_dialog_ok))
                .showCancelButton(false)
                .setConfirmClickListener(new FNAlertDialog.OnSweetClickListener() {
                    @Override
                    public void onClick(FNAlertDialog sDialog) {
                        sDialog.dismiss();
//                        ManualPairingFragment.openFragment(false, mClaimCode, smartconnType);
                    }
                })
                .show();
    }

    private void showChangeSsidDialog() {
        new FNAlertDialog(getActivity(), FNAlertDialog.WARNING_TYPE)
                .setTitleText(getActivity().getString(R.string.fn_tittle_settings_change_ssid))
                .setContentText(getActivity().getString(R.string.fn_msg_change_ssid_successfully).replace("${Thing}", getActivity().getString(R.string.fn_title_thing_name)))
                .setConfirmText(getActivity().getString(R.string.fn_dialog_ok))
                .showCancelButton(false)
                .setConfirmClickListener(new FNAlertDialog.OnSweetClickListener() {
                    @Override
                    public void onClick(FNAlertDialog sDialog) {
                        sDialog.dismiss();
                    }
                })
                .show();
    }

    private void addThingDialog(boolean isSuccess) {
        String addThing;

        if (isSuccess) {
            addThing = getString(R.string.fn_msg_smartconn_add_thing_successfully).replace("${Thing}", getActivity().getString(R.string.fn_title_thing_name));
        } else {
            addThing = getString(R.string.fn_msg_smartconn_add_thing_failed).replace("${Thing}", getActivity().getString(R.string.fn_title_thing_name));
        }
        new FNAlertDialog(getActivity(), FNAlertDialog.WARNING_TYPE)
                .setTitleText(getActivity().getString(R.string.fn_tittle_smartconn_add_things).replace("${Thing}", getActivity().getString(R.string.fn_title_thing_name)))
                .setContentText(addThing)
                .setConfirmText(getActivity().getString(R.string.fn_dialog_ok))
                .showCancelButton(false)
                .setConfirmClickListener(new FNAlertDialog.OnSweetClickListener() {
                    @Override
                    public void onClick(FNAlertDialog sDialog) {
                        sDialog.dismiss();
                    }
                })
                .show();
    }

    private View.OnClickListener onPairingInfoClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            new FNAlertDialog(getActivity(), FNAlertDialog.WARNING_TYPE)
                    .setTitleText(getActivity().getString(R.string.fn_dialog_are_you_sure))
                    .setContentText(getActivity().getString(R.string.fn_text_auto_pairing_description))
                    .setConfirmText(getActivity().getString(R.string.fn_dialog_ok))
                    .showCancelButton(false)
                    .setConfirmClickListener(new FNAlertDialog.OnSweetClickListener() {
                        @Override
                        public void onClick(FNAlertDialog sDialog) {
                            sDialog.dismiss();
                        }
                    })
                    .show();
        }
    };
}
