package com.fusionnext.cloud.social;

import android.content.Context;

import com.fusionnext.cloud.datalink.http.HttpDelete;
import com.fusionnext.cloud.datalink.http.HttpGet;
import com.fusionnext.cloud.datalink.http.HttpParam;
import com.fusionnext.cloud.datalink.http.HttpPost;
import com.fusionnext.cloud.datalink.http.HttpPut;
import com.fusionnext.cloud.datalink.json.FNJsonObject;
import com.fusionnext.cloud.social.objects.Group;
import com.fusionnext.cloud.social.objects.Track;
import com.fusionnext.cloud.social.objects.UserProfile;

import org.json.JSONException;

import java.util.HashMap;

/**
 * Created by Mike Chang on 2016/4/14
 */
public class User {
    protected final static String _TAG = "UserProfile";
    private Context context;
    private UserProfile userProfile = new UserProfile();

    public User(Context context) {
        this.context = context;
        setUserProfile(Public.getDataMgr(context).loadUserProfile());
    }

    public void setUserProfile(UserProfile profile) {
        if (userProfile == null) userProfile = new UserProfile();
        userProfile._userId = profile._userId;
        if (userProfile._name != null) userProfile._name = profile._name;
        if (userProfile._avatar != null) userProfile._avatar = profile._avatar;
        if (userProfile._birthday != null) userProfile._birthday = profile._birthday;
        if (userProfile._gender != null) userProfile._gender = profile._gender;
        if (userProfile._locale != null) userProfile._locale = profile._locale;
    }

    public UserProfile getUserProfile() {
        if (userProfile == null) userProfile = new UserProfile();
        return userProfile;
    }

    public void createUserByEmail(String name, String email, String password, final SocialAPIHandler.OnUsersListener onUsersListener) {
        HttpPost.PostParam[] params = new HttpPost.PostParam[1];
        params[0] = new HttpPost.PostParam();
        params[0].accessToken = Public.getAuthorization(context).getClientAccessToken();
        params[0].url = HttpData.generateHttpPostUrl(HttpData._POST_TYPE_USER_CREATE_USER_BY_EMAIL);
        params[0].type = HttpData._POST_TYPE_USER_CREATE_USER_BY_EMAIL;
        params[0].param = new HashMap<Object, Object>();

        params[0].param.put("name", name);
        params[0].param.put("email", email);
        params[0].param.put("password", password);

        HttpPost post = new HttpPost();
        post.setContext(context);
        post.setOnParseJsonListener(new HttpPost.OnParseJsonListener() {
            @Override
            public void onParse(boolean returnResult, int postType, String id, String returnStr) {
                if (returnStr != null) {
                    parseUserProfile(returnStr);
                }
                onUsersListener.onResult(returnResult, null, postType, returnStr);
            }
        });
        post.execute(params);
    }

    public void createUserByFacebook(Context context, String token, final SocialAPIHandler.OnUsersListener onUsersListener) {
        HttpPost.PostParam[] params = new HttpPost.PostParam[1];
        params[0] = new HttpPost.PostParam();
        params[0].accessToken = Public.getAuthorization(context).getClientAccessToken();
        params[0].url = HttpData.generateHttpPostUrl(HttpData._POST_TYPE_USER_CREATE_USER_BY_FACEBOOK);
        params[0].type = HttpData._POST_TYPE_USER_CREATE_USER_BY_FACEBOOK;
        params[0].param = new HashMap<Object, Object>();

        params[0].param.put("social_provider", "facebook");
        params[0].param.put("social_token", token);

        HttpPost post = new HttpPost();
        post.setContext(context);
        post.setOnParseJsonListener(new HttpPost.OnParseJsonListener() {
            @Override
            public void onParse(boolean returnResult, int postType, String id, String returnStr) {
                onUsersListener.onResult(returnResult, null, postType, returnStr);
            }
        });
        post.execute(params);
    }


    public void updateUser(UserProfile profile, final String userId, String pwd, final SocialAPIHandler.OnUsersListener onUsersListener) {

        HttpPut.PutParam[] params = new HttpPut.PutParam[1];
        params[0] = new HttpPut.PutParam();
        params[0].accessToken = Public.getAuthorization(context).getUserAccessToken();
        params[0].url = HttpData.generateHttpPutUrl(HttpData._PUT_TYPE_USER_UPDATE_USER, userId);
        params[0].type = HttpData._PUT_TYPE_USER_UPDATE_USER;
        params[0].param = new HashMap<Object, Object>();

        if (profile != null) {
            params[0].param.put("avatar", profile._avatar);
            params[0].param.put("name", profile._name);
            params[0].param.put("birthday", profile._birthday);
            params[0].param.put("gender", profile._gender);
            params[0].param.put("locale", profile._locale);
        }

        if (pwd != null) {
            params[0].param.put("password", pwd);
        }

        setUserProfile(profile);
        HttpPut put = new HttpPut();

        put.setHttpRequestType("PUT");
        put.setContext(context);
        put.setOnParseJsonListener(new HttpPut.OnParseJsonListener() {
            @Override
            public void onParse(boolean returnResult, int putType, String returnStr) {
                onUsersListener.onResult(returnResult, userId, putType, returnStr);
            }
        });
        put.execute(params);
    }

    public void getUser(final String userId, final SocialAPIHandler.OnUsersListener onSocialAPIListener) {
        HttpParam.HttpSubParam param = new HttpParam.HttpSubParam();

        param.accessToken = Public.getAuthorization(context).getUserAccessToken();
        param.url = HttpData.generateHttpGetUrl(HttpData._GET_TYPE_USER_GET_USER, userId, 0, null, null);
        param.cacheName = "";
        HttpParam[] params = new HttpParam[1];
        params[0] = new HttpParam();
        params[0].params.clear();
        params[0].params.add(param);

        HttpGet httpGet = new HttpGet();
        httpGet.setContext(context);
        httpGet.setTag(HttpGet._HTTP_T_GET_NORMAL_TYPE);
        httpGet.setOnParseJsonListener(new HttpGet.OnParseJsonListener() {
            @Override
            public void onParse(boolean result, String returnStr) {
                if (returnStr != null) {
                    parseUserProfile(returnStr);
                }
                onSocialAPIListener.onGetUser(result, userId, userProfile);
            }
        });
        httpGet.execute(params);
    }

    public void follow(final String userId, final SocialAPIHandler.OnUsersListener onUsersListener) {
        HttpPost.PostParam[] params = new HttpPost.PostParam[1];
        params[0] = new HttpPost.PostParam();
        params[0].accessToken = Public.getAuthorization(context).getUserAccessToken();
        params[0].url = HttpData.generateHttpPutUrl(HttpData._PUT_TYPE_USER_FOLLOW_USER, userId);
        params[0].type = HttpData._PUT_TYPE_USER_FOLLOW_USER;
        params[0].param = new HashMap<Object, Object>();

        HttpPost post = new HttpPost();
        post.setContext(context);
        post.setOnParseJsonListener(new HttpPost.OnParseJsonListener() {
            @Override
            public void onParse(boolean returnResult, int postType, String id, String returnStr) {
                onUsersListener.onResult(returnResult, userId, postType, returnStr);
            }
        });
        post.execute(params);
    }

    public void unfollow(final String userId, final SocialAPIHandler.OnUsersListener onUsersListener) {
        HttpDelete.DelParam[] params = new HttpDelete.DelParam[1];
        params[0] = new HttpDelete.DelParam();
        params[0].accessToken = Public.getAuthorization(context).getUserAccessToken();
        params[0].url = HttpData.generateHttpDeleteUrl(HttpData._DEL_TYPE_USER_UNFOLLOW_USER, userId);
        params[0].type = HttpData._DEL_TYPE_USER_UNFOLLOW_USER;
        params[0].param = new HashMap<Object, Object>();

        HttpDelete del = new HttpDelete();
        del.setContext(context);
        del.setOnParseJsonListener(new HttpDelete.OnParseJsonListener() {
            @Override
            public void onParse(boolean returnResult, int delType, String returnStr) {
                if (returnStr != null) {
                    onUsersListener.onResult(returnResult, userId, delType, returnStr);
                }
            }
        });
        del.execute(params);
    }

    public void listFollowers(final String userId, int order, String pageSize, String pageNum, final SocialAPIHandler.OnUsersListener onUsersListener) {
        HttpParam.HttpSubParam param = new HttpParam.HttpSubParam();

        param.accessToken = Public.getAuthorization(context).getUserAccessToken();
        param.url = HttpData.generateHttpGetUrl(HttpData._GET_TYPE_USER_LIST_FOLLOWERS, userId, order, pageSize, pageNum);
        param.cacheName = "";
        HttpParam[] params = new HttpParam[1];
        params[0] = new HttpParam();
        params[0].params.clear();
        params[0].params.add(param);

        HttpGet httpGet = new HttpGet();
        httpGet.setContext(context);
        httpGet.setTag(HttpGet._HTTP_T_GET_NORMAL_TYPE);
        httpGet.setOnParseJsonListener(new HttpGet.OnParseJsonListener() {
            @Override
            public void onParse(boolean result, String returnStr) {
                onUsersListener.onListUserFollowers(result, userId, UserProfile.parseUserProfileArray(returnStr));
            }
        });
        httpGet.execute(params);
    }

    public void listFollows(final String userId, int order, String pageSize, String pageNum, final SocialAPIHandler.OnUsersListener onUsersListener) {
        HttpParam.HttpSubParam param = new HttpParam.HttpSubParam();

        param.accessToken = Public.getAuthorization(context).getUserAccessToken();
        param.url = HttpData.generateHttpGetUrl(HttpData._GET_TYPE_USER_LIST_FOLLOWS, userId, order, pageSize, pageNum);
        param.cacheName = "";
        HttpParam[] params = new HttpParam[1];
        params[0] = new HttpParam();
        params[0].params.clear();
        params[0].params.add(param);

        HttpGet httpGet = new HttpGet();
        httpGet.setContext(context);
        httpGet.setTag(HttpGet._HTTP_T_GET_NORMAL_TYPE);
        httpGet.setOnParseJsonListener(new HttpGet.OnParseJsonListener() {
            @Override
            public void onParse(boolean result, String returnStr) {
                onUsersListener.onListUserFollows(result, userId, UserProfile.parseUserProfileArray(returnStr));
            }
        });
        httpGet.execute(params);
    }

    public void listTracks(final String userId, int order, String pageSize, String pageNum, final SocialAPIHandler.OnUsersListener onUsersListener) {
        HttpParam.HttpSubParam param = new HttpParam.HttpSubParam();

        param.accessToken = Public.getAuthorization(context).getUserAccessToken();
        param.url = HttpData.generateHttpGetUrl(HttpData._GET_TYPE_USER_LIST_TRACKS, userId, order, pageSize, pageNum);
        param.cacheName = "";
        HttpParam[] params = new HttpParam[1];
        params[0] = new HttpParam();
        params[0].params.clear();
        params[0].params.add(param);

        HttpGet httpGet = new HttpGet();
        httpGet.setContext(context);
        httpGet.setTag(HttpGet._HTTP_T_GET_NORMAL_TYPE);
        httpGet.setOnParseJsonListener(new HttpGet.OnParseJsonListener() {
            @Override
            public void onParse(boolean result, String returnStr) {
                onUsersListener.onListUserTracks(result, userId, Track.parseTrackArray(returnStr));
            }
        });
        httpGet.execute(params);
    }

    public void listInvitations(final String userId, int order, String pageSize, String pageNum, final SocialAPIHandler.OnUsersListener onUsersListener) {
        HttpParam.HttpSubParam param = new HttpParam.HttpSubParam();

        param.accessToken = Public.getAuthorization(context).getUserAccessToken();
        param.url = HttpData.generateHttpGetUrl(HttpData._GET_TYPE_USER_LIST_INVITATIONS, userId, order, pageSize, pageNum);
        param.cacheName = "";
        HttpParam[] params = new HttpParam[1];
        params[0] = new HttpParam();
        params[0].params.clear();
        params[0].params.add(param);

        HttpGet httpGet = new HttpGet();
        httpGet.setContext(context);
        httpGet.setTag(HttpGet._HTTP_T_GET_NORMAL_TYPE);
        httpGet.setOnParseJsonListener(new HttpGet.OnParseJsonListener() {
            @Override
            public void onParse(boolean result, String returnStr) {
                onUsersListener.onListUserTracks(result, userId, Track.parseTrackArray(returnStr));
            }
        });
        httpGet.execute(params);
    }

    public void listGroups(final String userId, int order, String pageSize, String pageNum, final SocialAPIHandler.OnUsersListener onUsersListener) {
        HttpParam.HttpSubParam param = new HttpParam.HttpSubParam();

        param.accessToken = Public.getAuthorization(context).getUserAccessToken();
        param.url = HttpData.generateHttpGetUrl(HttpData._GET_TYPE_USER_LIST_GROUPS, userId, order, pageSize, pageNum);
        param.cacheName = "";
        HttpParam[] params = new HttpParam[1];
        params[0] = new HttpParam();
        params[0].params.clear();
        params[0].params.add(param);

        HttpGet httpGet = new HttpGet();
        httpGet.setContext(context);
        httpGet.setTag(HttpGet._HTTP_T_GET_NORMAL_TYPE);
        httpGet.setOnParseJsonListener(new HttpGet.OnParseJsonListener() {
            @Override
            public void onParse(boolean result, String returnStr) {
                onUsersListener.onListUserGroups(result, userId, Group.parseGroupArray(returnStr));
            }
        });
        httpGet.execute(params);
    }

    private boolean parseUserProfile(String json) {
        String avatar;

        try {
            FNJsonObject profileObj = new FNJsonObject(json);
            userProfile.getDataFromJSONObject(profileObj);
            setUserProfile(userProfile);
            Public.getDataMgr(context).saveUserProfile(userProfile);
            return true;
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return false;
    }
}
