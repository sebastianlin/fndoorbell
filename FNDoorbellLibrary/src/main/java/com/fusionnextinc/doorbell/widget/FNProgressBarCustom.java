package com.fusionnextinc.doorbell.widget;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.RelativeLayout.LayoutParams;

import com.fusionnextinc.doorbell.R;

public class FNProgressBarCustom {
	public static final int SIZE_DP = 35;

	private Context context;
	private ProgressBar progressBar;
	private RelativeLayout viewToShowIn;
	private ViewGroup container;
	private boolean isShown = false;

	private boolean inCenter = true;

	public FNProgressBarCustom(Context context) {
		this.context = context;
		createProgressBar();
		createViewToShowIn();
		viewToShowIn.addView(progressBar);
	}

	public FNProgressBarCustom(Context context, ViewGroup container){
		this(context);
		this.container = container;
	}

	private void createProgressBar() {
		progressBar = new ProgressBar(context);
		progressBar.setIndeterminate(true);
		int size = DimensionsHandler.convertDipToPixels(context, SIZE_DP);
		LayoutParams params = new LayoutParams(size, size);
		progressBar.setLayoutParams(createProgressBarParams());
	}
	
	public void setShowInCenter(boolean inCenter){
		this.inCenter = inCenter;
		progressBar.setLayoutParams(createProgressBarParams());
	}
	
	private LayoutParams createProgressBarParams(){
		int size = DimensionsHandler.convertDipToPixels(context, SIZE_DP);
		LayoutParams params = new LayoutParams(size, size);
		if(inCenter){
			params.addRule(RelativeLayout.CENTER_IN_PARENT);
		} else {
			params.addRule(RelativeLayout.CENTER_HORIZONTAL);
			params.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM);
			params.bottomMargin = DimensionsHandler.convertDipToPixels(context, 80);
		}
		
		return params;
	}

	private void createViewToShowIn() {
		viewToShowIn = new RelativeLayout(context);
		viewToShowIn.setId(R.id.progressbar);
		viewToShowIn.setClickable(true);
		ViewGroup.LayoutParams params = new ViewGroup.LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT);
		viewToShowIn.setLayoutParams(params);
	}
	
	public void setContainer(ViewGroup container){
		this.container = container;
	}
	
	public void show(){
		if(container == null || isShown)
			return;
		
		isShown = true;
		container.addView(viewToShowIn);
	}
	
	public void hide(){
		if(container == null || !isShown)
			return;
		
		isShown = false;
		View viewToRemove = container.findViewById(R.id.progressbar);
		if (viewToRemove!=null)
			container.removeView(viewToRemove);
	}
	
	public boolean isShown(){
		return isShown;
	}
	
	private static class DimensionsHandler {
		
		public static int convertDipToPixels(Context context, int dipNumber) {
			return (int) (context.getResources().getDisplayMetrics().density * dipNumber);
		}
	}

}
