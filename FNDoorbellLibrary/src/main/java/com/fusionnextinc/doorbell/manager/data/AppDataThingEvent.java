package com.fusionnextinc.doorbell.manager.data;

import android.content.Context;
import android.util.Log;

import com.fusionnext.cloud.social.HttpData;
import com.fusionnext.cloud.utils.Utils;
import com.fusionnextinc.doorbell.MyApplication;
import com.fusionnextinc.doorbell.fragement.EventsFragment;
import com.fusionnextinc.doorbell.manager.thingsManager.ThingsHandler;
import com.fusionnextinc.doorbell.manager.thingsManager.ThingsManager;
import com.fusionnextinc.doorbell.objects.FNError;
import com.fusionnextinc.doorbell.objects.FNEvent;
import com.fusionnextinc.doorbell.objects.FNFirmware;
import com.fusionnextinc.doorbell.objects.FNSchedule;
import com.fusionnextinc.doorbell.objects.FNThing;
import com.fusionnextinc.doorbell.objects.FNUserProfile;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by Mike Chang on 2016/4/6
 */
public class AppDataThingEvent extends AppDataBase {
    protected final static String TAG = "AppDataThingEvent";
    public final static String TAG_EVENT_THING_ID = "TAG_EVENT_THING_ID";
    public final static String TAG_EVENT_ID = "TAG_EVENT_ID";
    public final static String TAG_EVENT_NAME = "TAG_EVENT_NAME";
    public final static String TAG_EVENT_PAYLOAD = "TAG_EVENT_PAYLOAD";
    public final static String TAG_EVENT_CREATED_AT = "TAG_EVENT_CREATED_AT";
    public final static String TAG_EVENT_UPDATED_AT = "TAG_EVENT_UPDATED_AT";

    private final static String TYPE_THING_EVENT_RING = "ring";
    private final static String TYPE_THING_EVENT_CALL = "call,answer,hang_up";

    private volatile static AppDataThingEvent instance;
    private Context context;
    private AppDataHandler.AppDataThingEventListener appDataThingEventListener = null;

    public AppDataThingEvent(Context context) {
        this.context = context;
    }

    public static AppDataThingEvent getInstance(Context context) {
        if (instance == null) {
            synchronized (AppDataThingEvent.class) {
                if (instance == null) {
                    instance = new AppDataThingEvent(context);
                }
            }
        }
        return instance;
    }

    @Override
    public ArrayList<String> getList(int tag) {
        return super.getList(tag);
    }

    @Override
    public void initList(boolean cleanIfExist) {
        super.initList(cleanIfExist);
    }

    // get the total numbers of saved share items
    public int getEventsSize(int type) {
        if (getList(type) == null) return 0;
        return getList(type).size();
    }

    public boolean deleteEvent(int type, int index, String name) {
        ArrayList<String> list = getList(type);
        if (list == null || list.size() <= 0) return false;
        if (index >= list.size()) return false;
        String id = list.get(index);
        if (id == null || id.endsWith("")) return false;

        return deleteEvent(id, name);
    }

    public boolean deleteEvent(String thingId, String eventId) {
        return DataManager.getInstance(context).deleteEvent(thingId, eventId);
    }

    //Ready the adapter data, this class will not save the adapter data
    public ListMap getAdapterData(int type, String thingId) {
        ListMap adapter = new ListMap();
        ArrayList<String> list = DataManager.getInstance(context).loadEventList(type, thingId);//getList(type);
        adapter.clear();
        if (list == null || list.size() <= 0) return adapter;

        for (String str : list) {
            Map<String, Object> map = getEventData(thingId, str);
            adapter.add(map);
        }

        return adapter;
    }

    public Map<String, Object> getEventData(String thingId, String eventId) {

        FNEvent fnEvent = DataManager.getInstance(context).loadEvent(thingId, eventId);
        Map<String, Object> map = new HashMap<>();

        if (fnEvent == null) {
            map.put(TAG_EVENT_NAME, "None");
        } else {
            map.put(TAG_EVENT_THING_ID, fnEvent.getThingId());
            map.put(TAG_EVENT_ID, fnEvent.getEventId());
            map.put(TAG_EVENT_NAME, fnEvent.getName());
            map.put(TAG_EVENT_PAYLOAD, fnEvent.getPayload());
            map.put(TAG_EVENT_CREATED_AT, fnEvent.getCreatedAt());
            map.put(TAG_EVENT_UPDATED_AT, fnEvent.getUpdatedAt());
        }

        return map;
    }

    public void loadEventsList(int type, String thingId, boolean onlineData, boolean onlineIfNoData) {
        QueryStatus status = getQueryStatus(type);
        if (status == null) status = new QueryStatus();
        status.setQueryState(true);
        updateQueryStatus(type, status);

        if (onlineData) {
            startEventsListTask(type, thingId, true);
            return;
        }

        ArrayList<String> list = DataManager.getInstance(context).loadEventList(type, thingId);//getList(type);
        if ((list == null || list.size() <= 0) && onlineIfNoData) {
            if (!Utils.isNetworkConnected(context)) {
                Log.d(TAG, "Network is not available");
                return;
            }
            startEventsListTask(type, thingId, true);
            return;
        }

        if (!Utils.isNetworkConnected(context)) {
            Log.d(TAG, "Network is not available");
            return;
        }

        status.setQueryEnd(true);
        updateQueryStatus(type, status);
    }

    public boolean hasMoreData(int tag) {
        QueryStatus status = getQueryStatus(tag);
        if (status == null) return false;
        return !status.isQueryEnd();
    }

    private boolean updateEvent(int type, final String userId, FNEvent fnEvent) {
        ArrayList<String> list = new ArrayList<String>();

        list.clear();
        QueryStatus status = getQueryStatus(type);
        if (!status.isNewQuery()) {
            list.addAll(getList(type));
        }

        if (fnEvent == null) return false;
        list.add(fnEvent.getEventId());

        if (status.isQueryEnd()) {
            status.setLastItemId("");
        } else {
            int lastIndex = list.size();
            if (lastIndex > 0) {
                status.setLastItemId(String.valueOf(lastIndex));
            }
        }
        updateQueryStatus(type, status);
        //Put this list to list cache
        updateList(type, list);
        //Save this list to database
        DataManager.getInstance(context).saveEventList(type, userId, getList(type));
        return true;
    }

    private boolean updateEventList(int type, final String thingId, ArrayList<FNEvent> eventList) {
        ArrayList<String> list = new ArrayList<>();

        list.clear();
        QueryStatus status = getQueryStatus(type);

        if (!status.isNewQuery()) {
            list.addAll(getList(type));
        }

        if (eventList == null) {
            status.setQueryEnd(true);
            status.setLastItemId("");
            updateQueryStatus(type, status);
            return false;
        }
        for (int i = 0; i < eventList.size(); i++) {
            FNEvent fnEvent = eventList.get(i);
            if (fnEvent == null) return false;
            DataManager.getInstance(context).saveEvent(fnEvent);
            list.add(fnEvent.getEventId());
        }

        if (eventList.size() < status.getPageSize()) {
            status.setQueryEnd(true);
        }

        if (status.isQueryEnd()) {
            status.setLastItemId("");
        } else {
            int lastIndex = list.size();
            if (lastIndex > 0) {
                //status.setLastItemId( list.get(lastIndex - 1));
                status.setLastItemId(String.valueOf(lastIndex));
            }
        }
        updateQueryStatus(type, status);

        //Put this list to list cache
        updateList(type, list);
        //Save this list to database
        DataManager.getInstance(context).saveEventList(type, thingId, getList(type));
        return true;
    }

    public void refreshEventsData(int type, final String thingId, boolean isNewQuery) {
        QueryStatus status = getQueryStatus(type);
        status.setQueryState(isNewQuery);
        startEventsListTask(type, thingId, isNewQuery);
    }

    public void getNextEventsPage(int type, final String userId) {
        startEventsListTask(type, userId, false);
    }

    //Start an ASyncTask to get the share list from server
    public void startEventsListTask(final int type, final String thingId, boolean isNewQuery) {
        QueryStatus status = getQueryStatus(type);
        if (isNewQuery) {
            //Clear the loaded data
            clearList(type);
        }

        status.setQueryState(isNewQuery);
        updateQueryStatus(type, status);
        ThingsManager.getInstance(context).setThingsListener(onThingsListener);

        switch (type) {
            case THING_TYPE_EVENTS_ALL:
                ThingsManager.getInstance(context).getEventsList(thingId, null, HttpData._GET_TYPE_SORT_ORDER_DESC, Integer.toString(status.getPageSize()), String.valueOf(status.cuclatePage()));
                break;
            case THING_TYPE_EVENTS_RING:
                ThingsManager.getInstance(context).getEventsList(thingId, TYPE_THING_EVENT_RING, HttpData._GET_TYPE_SORT_ORDER_DESC, Integer.toString(status.getPageSize()), String.valueOf(status.cuclatePage()));
                break;
            case THING_TYPE_EVENTS_CALL:
                ThingsManager.getInstance(context).getEventsList(thingId, TYPE_THING_EVENT_CALL, HttpData._GET_TYPE_SORT_ORDER_DESC, Integer.toString(status.getPageSize()), String.valueOf(status.cuclatePage()));
                break;
            default:
                break;
        }
    }

    private final ThingsHandler.ThingsListener onThingsListener = new ThingsHandler.ThingsListener() {

        @Override
        public void onListUserOwnedThings(ThingsManager instance, boolean result, String userId, ArrayList<FNThing> thingArrayList, FNError error) {

        }

        @Override
        public void onListUserInvitedThings(ThingsManager instance, boolean result, String userId, ArrayList<FNThing> thingArrayList, FNError error) {

        }

        @Override
        public void onListUserSharedThings(ThingsManager instance, boolean result, String userId, ArrayList<FNThing> thingArrayList, FNError error) {

        }

        @Override
        public void onListUserAllThings(ThingsManager instance, boolean result, String userId, ArrayList<FNThing> ownedThingArrayList, ArrayList<FNThing> sharedThingArrayList, ArrayList<FNThing> invitedThingArrayList, FNError error) {

        }

        @Override
        public void onListThingEventsByTime(ThingsManager instance, boolean result, String thingId, String eventName, String startTime, String endTime, ArrayList<FNEvent> eventList, FNError error) {

        }

        @Override
        public void onCurBatteryLevel(ThingsManager instance, boolean result, String thingId, String batteryLevel, FNError error) {

        }

        @Override
        public void onCurFirmwareVersion(ThingsManager instance, boolean result, String thingId, String fwVersion, FNError error) {

        }

        @Override
        public void onCurSwitchedStatus(ThingsManager instance, boolean result, String thingId, boolean isSwitch, FNError error) {

        }

        @Override
        public void onNewFirmwareVersion(ThingsManager instance, boolean result, String thingId, FNFirmware fnFirmware) {

        }

        @Override
        public void onStartFwUpdate(ThingsManager instance, boolean result, String thingId, String fwId) {

        }

        @Override
        public void onCurRingTone(ThingsManager instance, boolean result, String thingId, String ringtoneId, FNError error) {

        }

        @Override
        public void onGetScheduleList(ThingsManager instance, boolean result, String thingId, ArrayList<FNSchedule> scheduleList, FNError error) {

        }

        @Override
        public void onListInvitedUsers(ThingsManager instance, boolean result, String thingId, ArrayList<FNUserProfile> userList, FNError error) {

        }

        @Override
        public void onListSharedUsers(ThingsManager instance, boolean result, String thingId, ArrayList<FNUserProfile> userList, FNError error) {

        }

        @Override
        public void onCreateClaimCode(ThingsManager instance, boolean result, String claimCode, FNError error) {

        }

        @Override
        public void onCreateThing(ThingsManager instance, boolean result, FNThing fnThing, FNError error) {

        }

        @Override
        public void onUpdateThing(ThingsManager instance, boolean result, FNThing fnThing, FNError error) {

        }

        @Override
        public void onDeleteThing(ThingsManager instance, boolean result, FNError error) {

        }

        @Override
        public void onGetThing(ThingsManager instance, boolean result, FNThing fnThing, FNError error) {

        }

        @Override
        public void onChangeRingtone(ThingsManager instance, boolean result, String thingId, int ringtoneId) {

        }

        @Override
        public void onSetTimer(ThingsManager instance, boolean result, String thingId, String value) {

        }

        @Override
        public void onSetSchedule(ThingsManager instance, boolean result, String thingId, String value) {

        }

        @Override
        public void onShareInvited(ThingsManager instance, boolean result, String thingId, FNError error) {

        }

        @Override
        public void onShareAccepted(ThingsManager instance, boolean result, String thingId, FNError error) {

        }

        @Override
        public void onShareRejected(ThingsManager instance, boolean result, String thingId, FNError error) {

        }

        @Override
        public void onSwitchOnOff(ThingsManager instance, boolean result, String thingId, boolean isTurnOn) {

        }

        @Override
        public void onCloseCallSlot(ThingsManager instance, boolean result, FNError error) {

        }

        @Override
        public void onTakeCallSlot(ThingsManager instance, boolean result, String thingId, String callSlot, FNError error) {

        }

        @Override
        public void onListThingEvents(ThingsManager instance, boolean result, final String thingId, final String thingType, final ArrayList<FNEvent> eventList, FNError error) {
            //thing type is null means get all types of events
            if (thingType == null) {
                updateEventList(THING_TYPE_EVENTS_ALL, thingId, eventList);
                refreshEventsAdapter(THING_TYPE_EVENTS_ALL, thingId, eventList);
            } else if (thingType.equals(TYPE_THING_EVENT_RING)) {
                updateEventList(THING_TYPE_EVENTS_RING, thingId, eventList);
                refreshEventsAdapter(THING_TYPE_EVENTS_RING, thingId, eventList);
            } else if (thingType.equals(TYPE_THING_EVENT_CALL)) {
                updateEventList(THING_TYPE_EVENTS_CALL, thingId, eventList);
                refreshEventsAdapter(THING_TYPE_EVENTS_CALL, thingId, eventList);
            }
            MyApplication.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    EventsFragment.stopWait();
                }
            });
        }
    };


    public void refreshEventsAdapter(int type, String thingId, ArrayList<FNEvent> eventList) {
        if (appDataThingEventListener != null) {
            appDataThingEventListener.onRefreshListAdapter(type, thingId, eventList);
        }
    }

    public void refreshEventsListData(int type, final String thingId) {
        /*if (appDataThingEventListener != null) {
            appDataThingEventListener.onRefreshListView();
        }*/
        refreshEventsData(type, thingId, true);
    }

    public void setAppDataThingEventListener(AppDataHandler.AppDataThingEventListener listener) {
        this.appDataThingEventListener = listener;
    }
}
