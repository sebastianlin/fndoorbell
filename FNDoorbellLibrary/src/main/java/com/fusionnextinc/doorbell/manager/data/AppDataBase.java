package com.fusionnextinc.doorbell.manager.data;


import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by Mike Chang on 2016/2/22.
 */
public class AppDataBase {
    //User Type
    public final static int THING_TYPE_EVENTS_ALL = 122;
    public final static int THING_TYPE_EVENTS_RING = 123;
    public final static int THING_TYPE_EVENTS_CALL = 124;
    public final static int THING_TYPE_THINGS_OWNED = 125;
    public final static int THING_TYPE_THINGS_INVITED = 126;
    public final static int THING_TYPE_THINGS_SHARED = 127;
    public final static int THING_TYPE_THINGS_GROUP_OWNED = 128;
    public final static int THING_TYPE_THINGS_GROUP_SHARED = 129;
    public final static int THING_TYPE_THINGS_ALL = 130;
    public final static int USER_TYPE_THING_INVITED = 131;
    public final static int USER_TYPE_THING_SHARED = 132;
    public final static int GROUP_TYPE_NORMAL = 133;
    public final static int SCHEDULE_TYPE_NORMAL = 134;

    public static class AppDataMap {
        public AppDataMap() {
            queryStatus = new QueryStatus();
            list = new ArrayList<>();
        }

        private QueryStatus queryStatus = null;
        private ArrayList<String> list = null;

        public QueryStatus getQueryStatus() {
            return queryStatus;
        }

        public ArrayList<String> getList() {
            return list;
        }
    }

    private HashMap<Integer, AppDataMap> listMap = null;

    protected void initList(boolean cleanIfExist) {
        if (listMap == null) {
            listMap = new HashMap<>();
            listMap.clear();
        }

        if (cleanIfExist) {
            listMap.clear();
        }
    }

    //Get a list based on the tag
    protected AppDataMap getMap(int tag) {
        initList(false);
        if (listMap.containsKey(tag)) return listMap.get(tag);
        return null;
    }

    protected ArrayList<String> getList(int tag) {
        AppDataMap map = getMap(tag);
        return map != null ? map.getList() : null;
    }

    protected QueryStatus getQueryStatus(int tag) {
        AppDataMap map = getMap(tag);
        return map != null ? map.getQueryStatus() : new QueryStatus();
    }

    protected void putMap(int tag, AppDataMap map) {
        initList(false);
        if (listMap.containsKey(tag)) listMap.remove(tag);
        listMap.put(tag, map);
    }

    protected void updateList(int tag, ArrayList<String> list) {
        AppDataMap map = getMap(tag);
        if (map == null) map = new AppDataMap();
        map.list.clear();
        map.list.addAll(list);
        putMap(tag, map);
    }

    protected void updateQueryStatus(int tag, QueryStatus status) {
        AppDataMap map = getMap(tag);
        if (map == null) map = new AppDataMap();
        map.queryStatus = status;
        putMap(tag, map);
    }

    protected void clearData(int tag) {
        initList(false);
        listMap.remove(tag);
    }

    protected void clearList(int tag) {
        initList(false);
        AppDataMap map = listMap.get(tag);
        if (map == null || map.list == null) return;
        map.list.clear();
    }
}
