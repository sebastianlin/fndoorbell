package com.fusionnextinc.doorbell.voice;

/**
 * Created by koukiyoshiyuu on 2018/3/8.
 */

public interface OnFNAudioManagerPlayerListener {

    void onPlaying(FNAudioManager voiceManager);

    void onPause(FNAudioManager voiceManager);

    void onStop(FNAudioManager voiceManager);

    void onCompletion(FNAudioManager voiceManager);

    void onError(FNAudioManager voiceManager);

    void onPlayingTimeUpdated(int recordingTime);
}
