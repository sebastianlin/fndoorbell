package com.fusionnext.cloud.datalink.json;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.Iterator;

/**
 * Created by Mike Chang on 2016/2/22.
 */
public class FNJsonObject extends JSONObject {

    public FNJsonObject() {
        super();
    }

    public FNJsonObject(String json) throws JSONException {
        super(json);
    }

    private FNJsonObject castJsonObject(JSONObject _object) throws JSONException {
        if (_object == null || _object.keys() == null) return null;

        FNJsonObject fnJsonObject = new FNJsonObject();
        Iterator iterator = _object.keys();
        while (iterator.hasNext()) {
            String key = (String) iterator.next();
            Object jObject = _object.get(key);
            fnJsonObject.put(key, jObject);
        }

        return fnJsonObject;
    }

    @Override
    public Object get(String name) throws JSONException {
        if (!has(name) || isNull(name)) return null;
        return super.get(name);
    }

    @Override
    public FNJsonArray getJSONArray(String name) throws JSONException {
        if (!has(name) || isNull(name)) return null;
        FNJsonArray mjArray = new FNJsonArray();
        JSONArray array = (JSONArray) get(name);
        for (int i = 0; i < array.length(); i++) {
            Object object = array.get(i);
            if (object instanceof JSONObject) {
                FNJsonObject mtJsonObject = castJsonObject((JSONObject) object);
                mjArray.put(mtJsonObject);
            }
        }

        return mjArray;
    }

    @Override
    public FNJsonObject getJSONObject(String name) throws JSONException {
        if (!has(name) || isNull(name)) return null;
        return castJsonObject(super.getJSONObject(name));
    }

    @Override
    public String getString(String name) throws JSONException {
        if (!has(name) || isNull(name)) return null;
        return super.getString(name);
    }

    public String getStringDefault(String name) throws JSONException {
        return getStringDefault(name, "");
    }

    public String getStringDefault(String name, String defaultValue) throws JSONException {
        if (!has(name) || isNull(name))
            return defaultValue;
        return getString(name);
    }

    @Override
    public boolean getBoolean(String name) throws JSONException {
        return super.getBoolean(name);
    }

    public boolean getBooleanDefault(String name, boolean defaultValue) throws JSONException {
        if (!has(name) || isNull(name)) return defaultValue;
        return getBoolean(name);
    }

    public boolean getBooleanDefault(String name) throws JSONException {
        return getBooleanDefault(name, false);
    }

    @Override
    public int getInt(String name) throws JSONException {
        return super.getInt(name);
    }

    public int getIntDefault(String name, int defaultValue) throws JSONException {
        if (!has(name) || isNull(name)) return defaultValue;
        return getInt(name);
    }

    public int getIntDefault(String name) throws JSONException {
        return getIntDefault(name, 0);
    }

    public static String getJasonItems(JSONObject obj, String itemName) {
        try {
            if (obj.has(itemName))
                return obj.getString(itemName);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return "";
    }

    public static boolean getJasonBooleanItems(JSONObject obj, String itemName) {
        return getJasonBooleanItems(obj, itemName, false);
    }

    public static boolean getJasonBooleanItems(JSONObject obj, String itemName, boolean defaultValue) {
        try {
            if (obj.has(itemName))
                return obj.getBoolean(itemName);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return defaultValue;
    }

    public static int getJasonIntItem(JSONObject obj, String itemName) {
        return getJasonIntItem(obj, itemName, 0);
    }

    public static int getJasonIntItem(JSONObject obj, String itemName, int defaultValue) {
        try {
            if (obj.has(itemName))
                return obj.getInt(itemName);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return defaultValue;
    }
}
