package com.fusionnext.cloud.utils;

/**
 * Created by chocoyang on 2016/7/12.
 */
public class FNWaitSignal {
    private final Object mSignal = new Object();

    private boolean mReturned = false;
    private boolean mSuccess = false;
    private boolean mTimeout = false;
    private boolean mResetNotify = false;
    private Object mTag;

    public void setTag(Object tag) {
        mTag = tag;
    }

    public Object getTag() {
        return mTag;
    }

    public void waitNotify() {
        synchronized (mSignal) {
            if (!mReturned) {
                try {
                    mSignal.wait();
                    mTimeout = !mReturned;
                    mReturned = true;
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    public void waitNotify(long millis) {
        synchronized (mSignal) {
            if (!mReturned) {
                try {
                    do {
                        mResetNotify = false;
                        mSignal.wait(millis);
                    } while (mResetNotify);
                    mTimeout = !mReturned;
                    mReturned = true;
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    public void resetWaitTime() {
        synchronized (mSignal) {
            mResetNotify = true;
            mSignal.notify();
        }
    }

    public boolean isSuccess() {
        return mSuccess;
    }

    public boolean isReturned() {
        return mReturned;
    }

    public boolean isTimeout() {
        return mTimeout;
    }

    public void notifySuccess() {
        synchronized (mSignal) {
            mSuccess = true;
            if (!mReturned) {
                mReturned = true;
                mSignal.notify();
            }
        }
    }

    public void notifyFail() {
        synchronized (mSignal) {
            mSuccess = false;
            if (!mReturned) {
                mReturned = true;
                mSignal.notify();
            }
        }
    }
}
