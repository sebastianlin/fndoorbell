package com.fusionnextinc.doorbell.manager.data;

import com.fusionnextinc.doorbell.objects.FNEvent;
import com.fusionnextinc.doorbell.objects.FNSchedule;

import java.util.ArrayList;

/**
 * Created by Mike Chang on 2017/7/24
 */
public abstract class AppDataHandler {

    public interface AppDataUserListener {
        void onRefreshListAdapter(int type, String thingId);
    }

    public interface AppDataThingListener {
        void onRefreshEventCounts(String thingId);

        void onRefreshBatteryLevel(String thingId);

        void onRefreshListAdapter(int type);
    }

    public interface AppDataThingEventListener {
        void onRefreshListAdapter(int type, String thingId, ArrayList<FNEvent> eventList);
    }
}
