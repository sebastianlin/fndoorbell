package com.fusionnextinc.doorbell.fragement;

import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.fusionnextinc.doorbell.MyApplication;
import com.fusionnextinc.doorbell.R;
import com.fusionnextinc.doorbell.manager.thingsManager.ThingsHandler;
import com.fusionnextinc.doorbell.manager.thingsManager.ThingsManager;
import com.fusionnextinc.doorbell.objects.FNError;
import com.fusionnextinc.doorbell.objects.FNEvent;
import com.fusionnextinc.doorbell.objects.FNFirmware;
import com.fusionnextinc.doorbell.objects.FNSchedule;
import com.fusionnextinc.doorbell.objects.FNThing;
import com.fusionnextinc.doorbell.objects.FNUserProfile;
import com.fusionnextinc.doorbell.utils.Utils;
import com.fusionnextinc.doorbell.widget.FNAlert.FNAlertDialog;
import com.fusionnextinc.doorbell.widget.FNDialog;
import com.fusionnextinc.doorbell.widget.FNLayoutUtil;
import com.fusionnextinc.doorbell.widget.FNProgressBarCustom;

import java.util.ArrayList;

/**
 * Created by Mike Chang on 2017/7/17
 */
public class FirmwareUpdateFragment extends FNFragment implements View.OnClickListener {
    private static final String TAG = FirmwareUpdateFragment.class.getSimpleName();

    private FNLayoutUtil mLayoutUtil;
    private Button mBtnFwConnect;
    private FNProgressBarCustom mProgressBar;
    private ThingsManager mThingsManager;
    private FNFirmware mNewFnFirmware;
    private FNThing mFnThing;

    public static void openFragment(boolean clearOtherStack, FNThing fnThing, FNFirmware fnFirmware) {
        FirmwareUpdateFragment firmwareUpdateFragment = new FirmwareUpdateFragment();
        FNFragmentManager.getInstance().openFragment(firmwareUpdateFragment, clearOtherStack);
        firmwareUpdateFragment.mNewFnFirmware = fnFirmware;
        firmwareUpdateFragment.mFnThing = fnThing;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mLayoutUtil = new FNLayoutUtil(getActivity(), MyApplication.LAYOUT_WIDTH, MyApplication.LAYOUT_HEIGHT, FNLayoutUtil.BASE_WIDTH);
        mThingsManager = ThingsManager.getInstance(getActivity());
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        final ViewGroup nullParent = null;

        getActivity().setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        getActionBar().resetAll();
        getActionBar().setTitle(getString(R.string.title_firmware_update), Gravity.CENTER, null);
        getActionBar().setCoverMode(false);
        getActionBar().setStartImage(R.drawable.back, new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        getActionBar().show();
        View view = inflater.inflate(R.layout.fragment_firmware_update, nullParent);
        mLayoutUtil.fitAllView(view);

        TextView fwStep1 = (TextView) view.findViewById(R.id.text_firmware_step1);
        TextView fwStep2 = (TextView) view.findViewById(R.id.text_firmware_step2);
        TextView fwStep3 = (TextView) view.findViewById(R.id.text_firmware_step3);
        mBtnFwConnect = (Button) view.findViewById(R.id.btn_fw_update);
        mProgressBar = new FNProgressBarCustom(getActivity(), (ViewGroup) view.findViewById(R.id.fragment_firmware_update));
        mBtnFwConnect.setOnClickListener(this);

        fwStep1.setText(getString(R.string.fn_text_firmware_update_step1));
        fwStep2.setText(getString(R.string.fn_text_firmware_update_step2).replace("${Thing}", getActivity().getString(R.string.fn_title_thing_name)));
        fwStep3.setText(getString(R.string.fn_text_firmware_update_step3).replace("${Thing}", getActivity().getString(R.string.fn_title_thing_name)));
        return view;
    }

    @Override
    public void onClick(View view) {
        if (Utils.isNetworkConnected(getActivity())) {
            if (view.getId() == R.id.btn_fw_update) {
                startWait();
                mThingsManager.setThingsListener(thingsListener);
                mThingsManager.startFirmwareUpdate(mFnThing.getThingId(), mNewFnFirmware.getFwId());
            }
        } else {
            FNDialog.showNoNetworkDialog(getActivity());
        }
    }

    private void startWait() {
        mProgressBar.show();
    }


    private void stopWait() {
        mProgressBar.hide();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
    }

    private void showUpdateErrorDialog() {
        new FNAlertDialog(getActivity(), FNAlertDialog.WARNING_TYPE)
                .setTitleText(getActivity().getString(R.string.title_firmware_update))
                .setContentText(getActivity().getString(R.string.fn_dialog_firmware_update_failed).replace("${Thing}", getActivity().getString(R.string.fn_title_thing_name)))
                .setConfirmText(getActivity().getString(R.string.fn_dialog_ok))
                .showCancelButton(false)
                .setConfirmClickListener(new FNAlertDialog.OnSweetClickListener() {
                    @Override
                    public void onClick(FNAlertDialog sDialog) {
                        sDialog.dismiss();
                    }
                })
                .show();
    }


    private void otaStartDialog(FNThing fnThing) {
        if (fnThing != null) {
            new FNAlertDialog(getActivity(), FNAlertDialog.WARNING_TYPE)
                    .setTitleText(getActivity().getString(R.string.title_firmware_update))
                    .setContentText(getActivity().getString(R.string.fn_dialog_firmware_start_update).replace("${Thing}", getActivity().getString(R.string.fn_title_thing_name))
                            .replace("${Firmware}", mNewFnFirmware.getVersion()))
                    .setConfirmText(getActivity().getString(R.string.fn_dialog_ok))
                    .showCancelButton(false)
                    .setConfirmClickListener(new FNAlertDialog.OnSweetClickListener() {
                        @Override
                        public void onClick(FNAlertDialog sDialog) {
                            sDialog.dismiss();
                        }
                    })
                    .show();
        }
    }


    final ThingsHandler.ThingsListener thingsListener = new ThingsHandler.ThingsListener() {
        @Override
        public void onListUserOwnedThings(ThingsManager instance, boolean result, String userId, ArrayList<FNThing> thingArrayList, FNError error) {

        }

        @Override
        public void onListUserInvitedThings(ThingsManager instance, boolean result, String userId, ArrayList<FNThing> thingArrayList, FNError error) {

        }

        @Override
        public void onListUserSharedThings(ThingsManager instance, boolean result, String userId, ArrayList<FNThing> thingArrayList, FNError error) {

        }

        @Override
        public void onListUserAllThings(ThingsManager instance, boolean result, String userId, ArrayList<FNThing> ownedThingArrayList, ArrayList<FNThing> sharedThingArrayList, ArrayList<FNThing> invitedThingArrayList, FNError error) {

        }

        @Override
        public void onListThingEvents(ThingsManager instance, boolean result, String thingId, String eventName, ArrayList<FNEvent> eventList, FNError error) {

        }

        @Override
        public void onListThingEventsByTime(ThingsManager instance, boolean result, String thingId, String eventName, String startTime, String endTime, ArrayList<FNEvent> eventList, FNError error) {

        }

        @Override
        public void onCurBatteryLevel(ThingsManager instance, boolean result, String thingId, String batteryLevel, FNError error) {

        }

        @Override
        public void onCurFirmwareVersion(ThingsManager instance, boolean result, String thingId, String fwVersion, FNError error) {

        }

        @Override
        public void onCurSwitchedStatus(ThingsManager instance, boolean result, String thingId, boolean isSwitch, FNError error) {

        }

        @Override
        public void onNewFirmwareVersion(ThingsManager instance, boolean result, String thingId, FNFirmware fnFirmware) {

        }

        @Override
        public void onStartFwUpdate(ThingsManager instance, boolean result, String thingId, String fwId) {
            stopWait();
            if (result) {
                otaStartDialog(mFnThing);
            } else {
                showUpdateErrorDialog();
            }
        }

        @Override
        public void onCurRingTone(ThingsManager instance, boolean result, String thingId, String ringtoneId, FNError error) {

        }

        @Override
        public void onGetScheduleList(ThingsManager instance, boolean result, String thingId, ArrayList<FNSchedule> scheduleList, FNError error) {

        }

        @Override
        public void onListInvitedUsers(ThingsManager instance, boolean result, String thingId, ArrayList<FNUserProfile> userList, FNError error) {

        }

        @Override
        public void onListSharedUsers(ThingsManager instance, boolean result, String thingId, ArrayList<FNUserProfile> userList, FNError error) {

        }

        @Override
        public void onCreateClaimCode(ThingsManager instance, boolean result, String claimCode, FNError error) {

        }

        @Override
        public void onCreateThing(ThingsManager instance, boolean result, FNThing fnThing, FNError error) {

        }

        @Override
        public void onUpdateThing(ThingsManager instance, boolean result, FNThing fnThing, FNError error) {

        }

        @Override
        public void onDeleteThing(ThingsManager instance, boolean result, FNError error) {

        }

        @Override
        public void onGetThing(ThingsManager instance, boolean result, FNThing fnThing, FNError error) {

        }

        @Override
        public void onChangeRingtone(ThingsManager instance, boolean result, String thingId, int ringtoneId) {

        }

        @Override
        public void onSetTimer(ThingsManager instance, boolean result, String thingId, String value) {

        }

        @Override
        public void onSetSchedule(ThingsManager instance, boolean result, String thingId, String value) {

        }

        @Override
        public void onShareInvited(ThingsManager instance, boolean result, String thingId, FNError error) {

        }

        @Override
        public void onShareAccepted(ThingsManager instance, boolean result, String thingId, FNError error) {

        }

        @Override
        public void onShareRejected(ThingsManager instance, boolean result, String thingId, FNError error) {

        }

        @Override
        public void onSwitchOnOff(ThingsManager instance, boolean result, String thingId, boolean isTurnOn) {

        }

        @Override
        public void onCloseCallSlot(ThingsManager instance, boolean result, FNError error) {

        }

        @Override
        public void onTakeCallSlot(ThingsManager instance, boolean result, String thingId, String callSlot, FNError error) {

        }
    };
}
