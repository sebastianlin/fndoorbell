package com.fusionnextinc.doorbell;

import static android.app.Notification.*;

import android.annotation.SuppressLint;
import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.service.notification.StatusBarNotification;
import android.widget.RemoteViews;

import androidx.annotation.RequiresApi;
import androidx.core.app.NotificationCompat;
import androidx.core.content.ContextCompat;

import com.fusionnext.cloud.utils.Times;
import com.fusionnextinc.doorbell.voice.FNRingToneManager;
import com.fusionnextinc.doorbell.fragement.CallFragment;
import com.fusionnextinc.doorbell.fragement.HomeFragment;
import com.fusionnextinc.doorbell.fragement.MyQRCodeFragment;
import com.fusionnextinc.doorbell.fragement.ThingsFragment;
import com.fusionnextinc.doorbell.info.SharedPreferencesInfo;
import com.fusionnextinc.doorbell.manager.account.AccountManager;
import com.fusionnextinc.doorbell.manager.data.AppDataThing;
import com.fusionnextinc.doorbell.manager.thingsManager.ThingsManager;
import com.fusionnextinc.doorbell.objects.FNThing;
import com.fusionnextinc.doorbell.utils.FNLog;
import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;
import com.iptnet.common.c2c.C2CProcess;

import java.util.Map;
import java.util.concurrent.atomic.AtomicBoolean;

import static com.fusionnextinc.doorbell.info.SharedPreferencesInfo.KEY_RINGING_THING_ID;

/**
 * Created by Mike Chang on 2017/7/7.
 */
@SuppressLint("MissingFirebaseInstanceTokenRefresh")
public class MyFirebaseMessagingService extends FirebaseMessagingService {

    public static final String TAG = "MyFirebaseMessagingService";
    public static final String EVENT_TYPE_OTA_SUCCESS = "ota_success";
    public static final String EVENT_TYPE_OTA_FAIL = "ota_fail";
    public static final String EVENT_TYPE_RING = "push";
    public static final String EVENT_TYPE_PUSH = "push";
    public static final String EVENT_TYPE_DISSCONNECTED = "disconnected";
    public static final String EVENT_TYPE_RECONNECTED = "reconnected";
    public static final String EVENT_TYPE_THING_CREATED = "thing_created";
    public static final String EVENT_TYPE_CALL = "call";
    public static final String EVENT_TYPE_ANSWER = "answer";
    public static final String EVENT_TYPE_HANGUP = "hang_up";
    public static final String MESSAGE_TYPE_EVENT = "event";
    public static final String MESSAGE_TYPE_INVITATION = "invitation";
    public static final String CALL_SLOT_ID = "call_slot_id";
    public static final String THING_ID = "thing_id";
    public static final String THING_NAME = "thing_name";
    public static final String INVITER_NAME = "inviter_name";
    public static final String SOUND = "sound";
    public static final String CALLNAME_ID = "callname";
    public static final String CALL_NUMBER_ID = "call_number";


    public static final String NOTIFY_CLICK = "notify_click";

    public  static AtomicBoolean s_flag = new AtomicBoolean(false);


    static public   boolean  getFlage()
    {
        if( s_flag.get() )
        {
            return true;
        }
        return false;
    }


    static public   void  cleanFlage()
    {
        s_flag.set(false);
    }

    static public   void  setFlage()
    {
        s_flag.set(true);
    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    public void onMessageReceived(RemoteMessage remoteMessage) {
        FNLog.d(TAG, "onMessageReceived:" + remoteMessage.getFrom()+"version:"+Build.VERSION.SDK_INT +":"+ Build.VERSION_CODES.P);
        NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        String NOTIFICATION_CHANNEL_ID = getString(R.string.default_notification_channel_id);
        AccountManager accountManager = AccountManager.getInstance(getApplicationContext());
        Map<String, String> map = remoteMessage.getData();
        final String type = map.get("type");
        String event = map.get(MESSAGE_TYPE_EVENT);
        if (type != null && type.equals(MESSAGE_TYPE_EVENT) &&
            event != null  &&  accountManager.isNotificationEnabled(getApplicationContext())
        )
        {
            assert notificationManager != null;
            NotificationChannel channel = notificationManager.getNotificationChannel(HeadsUpNotificationService.CHANNEL_ID);
            StatusBarNotification[] activeNotifications = notificationManager.getActiveNotifications();

            for(int i = 0; i < activeNotifications.length; i++)
            {
                String  s = activeNotifications[i].toString();

            }
            if( HeadsUpNotificationService.sInstance.getFlage() == true )
            {

                    //Intent intent = new Intent(this, HeadsUpNotificationService.class);
//                      HeadsUpNotificationService.sInstance.stopOnce();

                getApplicationContext().stopService(new Intent(getApplicationContext(), HeadsUpNotificationService.class));
                Intent it = new Intent(Intent.ACTION_CLOSE_SYSTEM_DIALOGS);
                getApplicationContext().sendBroadcast(it);


                while(HeadsUpNotificationService.sInstance.getFlage())
                {

                        try {
                                Thread.sleep(500);
                        } catch (InterruptedException e) {
                                e.printStackTrace();
                        }
                }


                NotificationChannel  channel2 = notificationManager.getNotificationChannel(HeadsUpNotificationService.CHANNEL_ID);
                MyFirebaseMessagingService.setFlage();

            }


            if ( event.equals(EVENT_TYPE_PUSH)) {
                sendNotificationMin(remoteMessage.getNotification().getTitle(), remoteMessage.getNotification().getBody());
            }
            else if ( event.equals(EVENT_TYPE_OTA_SUCCESS)) {
                //sendNotificationMin(remoteMessage.getNotification().getTitle(), remoteMessage.getNotification().getBody());
            }
            else if ( event.equals(EVENT_TYPE_OTA_FAIL)) {
                //sendNotificationMin(remoteMessage.getNotification().getTitle(), remoteMessage.getNotification().getBody());
            }
            else if ( event.equals(EVENT_TYPE_DISSCONNECTED)) {
                sendNotificationMin(remoteMessage.getNotification().getTitle(), remoteMessage.getNotification().getBody());
            }
            else if ( event.equals(EVENT_TYPE_RECONNECTED)) {
                sendNotificationMin(remoteMessage.getNotification().getTitle(), remoteMessage.getNotification().getBody());

            }
            else if ( event.equals(EVENT_TYPE_THING_CREATED)) {
                sendNotificationMin(remoteMessage.getNotification().getTitle(), remoteMessage.getNotification().getBody());

            }
/*
            else if ( event.equals(EVENT_TYPE_CALL)) {
                sendNotificationMax(remoteMessage.getNotification().getTitle(), remoteMessage.getNotification().getBody());
                return;
            }
*/

        }

        NotificationChannel notify = notificationManager.getNotificationChannel(HeadsUpNotificationService.NOTIFY_ID.toString());




        if (accountManager.isNotificationEnabled(getApplicationContext())) {
            if (MyApplication.isActivityVisible() ) {
                handleEvent(remoteMessage,notify != null);
            } else {
                if (type != null && type.equals(MESSAGE_TYPE_EVENT)) {
                    event = map.get(MESSAGE_TYPE_EVENT);
                    if (event != null && event.equals(EVENT_TYPE_CALL)) {
                        RemoteMessage.Notification notification = remoteMessage.getNotification();
                        final String callSlotId = map.get(CALL_SLOT_ID);
                        final String thingId = map.get(THING_ID);
                        final String thingName = map.get(THING_NAME);
                        final String callName = map.get(CALLNAME_ID);
                        final String Number = String.valueOf(System.currentTimeMillis());

                        FNLog.d(TAG, "Bundle");

                        Bundle bundle = new Bundle(); //new一個Bundle物件，並將要傳遞的資料傳入
                        bundle.putString(CALLNAME_ID, callName);

                        bundle.putString(CALL_SLOT_ID, callSlotId);
                        bundle.putString(THING_ID, thingId);
                        bundle.putString(THING_NAME, thingName);
                        bundle.putString(CALL_NUMBER_ID, Number);
                        bundle.putString(SOUND, notification.getSound());
                        bundle.putString(NOTIFY_CLICK, "null");

                        if (Build.VERSION.SDK_INT > Build.VERSION_CODES.P) {
                            Intent serviceIntent = new Intent(this, HeadsUpNotificationService.class);
                            serviceIntent.putExtra("inputExtra", "Foreground Service in Android");
                            serviceIntent.putExtras(bundle);//將Bundle物件傳給intent
                            serviceIntent.addFlags( Intent.FLAG_ACTIVITY_NEW_TASK);
                            serviceIntent.addFlags( Intent.FLAG_ACTIVITY_CLEAR_TOP);
                            ContextCompat.startForegroundService(this, serviceIntent);

                        } else {
                            Intent i = new Intent(getApplicationContext(), MainActivity.class);
                            i.putExtras(bundle);//將Bundle物件傳給intent
                            i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                            getApplicationContext().startActivity(i);
                        }
                    } else if (event != null && event.equals(EVENT_TYPE_HANGUP)) {
                        if( HeadsUpNotificationService.getFlage() == true ) {
                            NotificationManager manager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
                            manager.cancel(HeadsUpNotificationService.NOTIFY_ID);
                            manager.cancel(HeadsUpNotificationService.SERVICE_NOTIFY_ID);
                        }

                        getApplicationContext().stopService(new Intent(getApplicationContext(), HeadsUpNotificationService.class));
                        sendNotificationMin(remoteMessage.getNotification().getTitle(), remoteMessage.getNotification().getBody());
                    }
                    else if (event != null && event.equals(EVENT_TYPE_ANSWER) ) {
                        if( HeadsUpNotificationService.getFlage() == true ) {
                            NotificationManager manager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
                            manager.cancel(HeadsUpNotificationService.NOTIFY_ID);
                            manager.cancel(HeadsUpNotificationService.SERVICE_NOTIFY_ID);
                        }
                        getApplicationContext().stopService(new Intent(getApplicationContext(), HeadsUpNotificationService.class));
                        sendNotificationMin(remoteMessage.getNotification().getTitle(), remoteMessage.getNotification().getBody());
                    }
                }
                else {
                    sendNotificationMin(remoteMessage.getNotification().getTitle(), remoteMessage.getNotification().getBody());
                }
            }
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }


    @RequiresApi(api = Build.VERSION_CODES.O)
    private void sendNotification(String messageTitle, String messageBody,boolean bPriorityMAX )
    {


        Intent intent = new Intent(this, MainActivity.class);
        //intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);

        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK |Intent.FLAG_ACTIVITY_CLEAR_TASK);
        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0 /* request code */, intent, PendingIntent.FLAG_UPDATE_CURRENT);
        NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        long[] pattern = {500, 500, 500, 500, 500};


//        Uri defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        //Uri defaultSoundUri = Uri.parse(ContentResolver.SCHEME_ANDROID_RESOURCE + "://"+ getApplicationContext().getPackageName() + "/" + R.raw.dream);


        Uri defaultSoundUri = Uri.parse(ContentResolver.SCHEME_ANDROID_RESOURCE + "://"+ getApplicationContext().getPackageName() + "/" + R.raw.dream);

        String NOTIFICATION_CHANNEL_ID = getString(R.string.default_notification_channel_id);
        String NOTIFICATION_CHANNEL_NAME = getString(R.string.default_notification_channel_name);

        RemoteViews notificationTitleLayout = new RemoteViews(getApplicationContext().getPackageName(),R.layout.message_notification);
        notificationTitleLayout.setImageViewResource(R.id.icon_msg,R.drawable.call_logo);
        notificationTitleLayout.setTextViewText(R.id.message_title,messageTitle);
        notificationTitleLayout.setTextViewText(R.id.message_msg,messageBody);


        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {

            NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this,NOTIFICATION_CHANNEL_ID);
            notificationBuilder.setAutoCancel(true)
                    .setDefaults(Notification.DEFAULT_ALL)
                    .setWhen(System.currentTimeMillis())
                    .setSmallIcon(R.drawable.ic_logo)
                    .setCustomHeadsUpContentView(notificationTitleLayout)
                    //.setLargeIcon(BitmapFactory.decodeResource(getResources(), R.mipmap.ic_launcher))

                    //.setContentTitle(messageTitle)
                    //.setContentText(messageBody)
                    .setWhen(System.currentTimeMillis())
                    .setVibrate(pattern)
                    .setContentIntent(pendingIntent)
                    .setPriority(NotificationCompat.PRIORITY_DEFAULT)
                    .setSound(defaultSoundUri);

            NotificationChannel channel;
            if(bPriorityMAX == true) {
                notificationBuilder.setPriority(NotificationCompat.PRIORITY_MAX);
                channel = new NotificationChannel(NOTIFICATION_CHANNEL_ID,
                        NOTIFICATION_CHANNEL_NAME,
                        NotificationManager.IMPORTANCE_HIGH);


            }else
            {
                notificationBuilder.setPriority(NotificationCompat.PRIORITY_HIGH);
                channel = new NotificationChannel(NOTIFICATION_CHANNEL_ID,
                        NOTIFICATION_CHANNEL_NAME,
                        NotificationManager.IMPORTANCE_HIGH);

            }

            channel.enableVibration(true);
            channel.setBypassDnd(true);
            channel.setShowBadge(true);
            channel.setLockscreenVisibility(VISIBILITY_PUBLIC);
            channel.setDescription("Call Notifications");

            /*
            channel.setSound(defaultSoundUri,
            /
                    new AudioAttributes.Builder().setContentType(AudioAttributes.CONTENT_TYPE_SONIFICATION)
                            .setLegacyStreamType(AudioManager.STREAM_RING)
                            .setUsage(AudioAttributes.USAGE_NOTIFICATION).build());
*/

            notificationManager.createNotificationChannel(channel);
            notificationManager.notify(HeadsUpNotificationService.NOTIFY_ID, notificationBuilder.build());
        } else {
            NotificationCompat.Builder notificationBuilder = (NotificationCompat.Builder) new NotificationCompat.Builder(this)
                    .setSmallIcon(R.drawable.ic_logo)
                    .setContentTitle(messageTitle)
                    .setContentText(messageBody)
                    .setAutoCancel(true)
                    .setVibrate(pattern)
                    .setLights(Color.BLUE, 1, 1)
                    .setSound(defaultSoundUri)
                    .setContentIntent(pendingIntent);


            notificationManager.notify( /* ID of notification */HeadsUpNotificationService.NOTIFY_ID, notificationBuilder.build());
            new Thread(new Runnable() {
                @Override
                public void run() {
                    try {
                        Thread.sleep(30000);//五秒后悬挂式通知消失
                         //notificationManager.cancel(0);//按tag id 来清除消息
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            }).start();

        }
    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    private void sendNotificationMin(String messageTitle, String messageBody )
    {
        sendNotification(messageTitle,messageBody,false);
    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    private void sendNotificationMax(String messageTitle, String messageBody )
    {
        sendNotification(messageTitle,messageBody,true);
    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    private void handleEvent(RemoteMessage remoteMessage ,boolean inNotify ) {
        Map<String, String> map = remoteMessage.getData();
        RemoteMessage.Notification notification = remoteMessage.getNotification();
        String NOTIFICATION_CHANNEL_ID = getString(R.string.default_notification_channel_id);
        final String thingName = map.get(THING_NAME);
        final String inviterName = map.get(INVITER_NAME);
        final String type = map.get("type");
        final String thingId = map.get(THING_ID);

        if (notification.getSound() != null)
            FNLog.d(TAG, "sound:" + notification.getSound());

        if (inviterName != null)
            FNLog.d(TAG, "inviter_name:" + inviterName);
        if (thingName != null)
            FNLog.d(TAG, "thing_name:" + thingName);
        if (type != null)
            FNLog.d(TAG, "type:" + type);
        if (thingId != null)
            FNLog.d(TAG, "thing_id:" + thingId);

        if (type != null && type.equals(MESSAGE_TYPE_EVENT)) {
            String event = map.get(MESSAGE_TYPE_EVENT);

            if (event != null && event.equals(EVENT_TYPE_PUSH)) {
                //ThingsManager.getInstance(MyApplication.getAppContext()).updateThingEventCountsAddOne(thingId);
                //NotificationFragment.openFragment(thingId, thingName, notification.getBody(), notification.getSound());

                //sendNotification(remoteMessage.getNotification().getTitle(), remoteMessage.getNotification().getBody());
            } else if (event != null && event.equals(EVENT_TYPE_HANGUP)) {
                String ringingThingId = SharedPreferencesInfo.getString(KEY_RINGING_THING_ID, "");

                if (ringingThingId.equals(thingId)) {
                    sendNotificationMin(remoteMessage.getNotification().getTitle(), remoteMessage.getNotification().getBody());
                    Intent broadcast = new Intent();
                    broadcast.setAction(MyApplication.BROADCAST_HANGUP_EVENT);
                    MyApplication.getAppContext().sendBroadcast(broadcast);
                }

            } else if (event != null && event.equals(EVENT_TYPE_ANSWER)) {
                String ringingThingId = SharedPreferencesInfo.getString(KEY_RINGING_THING_ID, "");
                if (C2CProcess.getInstance().getP2PServerStatus() != C2CProcess.P2P_STATUS_DEVICE_CONNECTED) {
                    if (ringingThingId.equals(thingId)) {
                        sendNotificationMin(remoteMessage.getNotification().getTitle(), remoteMessage.getNotification().getBody());
                        Intent broadcast = new Intent();
                        broadcast.setAction(MyApplication.BROADCAST_ANSWER_EVENT);
                        MyApplication.getAppContext().sendBroadcast(broadcast);
                    }
                }
            } else if (event != null && event.equals(EVENT_TYPE_CALL)) {
                String callSlotId = map.get(CALL_SLOT_ID);

                FNLog.d(TAG, "call_slot_id:" + callSlotId);
                ThingsManager.getInstance(MyApplication.getAppContext()).updateThingEventCountsAddOne(thingId);
/*
        if (C2CProcess.getInstance().getP2PServerStatus() != C2CProcess.P2P_STATUS_DEVICE_CONNECTED &&
                        C2CProcess.getInstance().getP2PServerStatus() != C2CProcess.P2P_STATUS_ACCEPT &&
                        C2CProcess.getInstance().getP2PServerStatus() != C2CProcess.P2P_STATUS_SETUP_DONE) {

 */
                if (C2CProcess.getInstance().getP2PServerStatus() != C2CProcess.P2P_STATUS_DEVICE_CONNECTED &&
                        C2CProcess.getInstance().getP2PServerStatus() != C2CProcess.P2P_STATUS_ACCEPT  &&
                        C2CProcess.getInstance().getP2PServerStatus() != C2CProcess.P2P_STATUS_SETUP_DONE &&
                        inNotify == false
                ) {
                    SharedPreferencesInfo.putString(KEY_RINGING_THING_ID, thingId);
                    CallFragment.openFragment(thingId, thingName, notification.getSound(), callSlotId);
                }
                else
                {
                    FNRingToneManager.ContinueRing();
                }
            } else if (event != null && event.equals(EVENT_TYPE_OTA_SUCCESS)) {
                if (DefaultAppConfig.APP_SOLY) {
                    HomeFragment.openFragment(true, ThingsFragment.THINGS_FRAGMENT_TYPE_OTA_SUCCESS, thingName, HomeFragment.TabType.TAB_TYPE_DEVICE, false);
                } else {
                    ThingsFragment.openFragment(true, ThingsFragment.THINGS_FRAGMENT_TYPE_OTA_SUCCESS, thingName);
                }
            } else if (event != null && event.equals(EVENT_TYPE_OTA_FAIL)) {
                if (DefaultAppConfig.APP_SOLY) {
                    HomeFragment.openFragment(true, ThingsFragment.THINGS_FRAGMENT_TYPE_OTA_FAIL, thingName, HomeFragment.TabType.TAB_TYPE_DEVICE, false);
                } else {
                    ThingsFragment.openFragment(true, ThingsFragment.THINGS_FRAGMENT_TYPE_OTA_FAIL, thingName);
                }
            }
        } else if (type != null && type.equals(MESSAGE_TYPE_INVITATION)) {
            FNThing newFnThing = new FNThing();
            newFnThing.setName(thingName);
            newFnThing.setUserName(inviterName);
            newFnThing.setThingId(thingId);
            AppDataThing.getInstance(MyApplication.getAppContext()).saveThing(newFnThing);
            if (DefaultAppConfig.APP_SOLY) {
                MyQRCodeFragment.replaceFragment(MyQRCodeFragment.MYQRCODE_FRAGMENT_TYPE_INVITATION, thingId);
            } else {
                if (C2CProcess.getInstance().getP2PServerStatus() != C2CProcess.P2P_STATUS_DEVICE_CONNECTED) {
                    ThingsFragment.openFragment(true, ThingsFragment.THINGS_FRAGMENT_TYPE_INVITATION, thingId);
                }
            }
        }
    }
}