package com.fusionnextinc.doorbell.widget;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.res.Configuration;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.util.DisplayMetrics;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.NumberPicker;
import android.widget.SeekBar;
import android.widget.TextView;

import com.fusionnextinc.doorbell.MyApplication;
import com.fusionnextinc.doorbell.R;

import java.util.Calendar;

/**
 * Created by chocoyang on 2015/11/2.
 */
public class FNCustomDialog extends AlertDialog.Builder {
    private static final String TAG = "FNDialog";
    private static AlertDialog dialog;

    public static final int BUTTON_POSITIVE = -1;
    public static final int BUTTON_NEGATIVE = -2;
    public static final int BUTTON_NEUTRAL = -3;

    private static final int MAX_YEAR =  Calendar.getInstance().get(Calendar.YEAR);
    private static final int MIN_YEAR = 2017;

    private FNLayoutUtil layoutUtil;
    private View layout;
    private TextView title, msg;
    private LinearLayout llView, llDate, llTime;
    private FNNumberPicker npYear, npMonth, npDay, npHour, npMinute;
    private ListView lv;
    private FrameLayout view;
    private View divider_top, divider_bot, divider_btn1, divider_btn2;
    private Button btn1, btn2, btn3;
    private EditText itemEt;

    @Override
    public AlertDialog create() {
        if (isShowing()) {
            try {
                dialog.dismiss();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        dialog = super.create();
        return dialog;
    }

    public static void dismissAll() {
        if (isShowing()) {
            dialog.dismiss();
            dialog = null;
        }
    }

    public static boolean isShowing() {
        return dialog != null && dialog.isShowing();
    }

    public FNCustomDialog(Activity activity) {
        //super(activity, R.style.FNDialog);
        super(activity);
        layoutUtil = new FNLayoutUtil(activity, MyApplication.LAYOUT_WIDTH, MyApplication.LAYOUT_HEIGHT, FNLayoutUtil.BASE_WIDTH);
        WindowManager wm = (WindowManager) getContext().getSystemService(Context.WINDOW_SERVICE);
        final DisplayMetrics metrics = new DisplayMetrics();
        wm.getDefaultDisplay().getMetrics(metrics);
        LinearLayout main = new LinearLayout(activity) {
            @Override
            protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
                int maxHeight;
                if (getResources().getConfiguration().orientation == Configuration.ORIENTATION_PORTRAIT) {
                    maxHeight = (int) ((metrics.heightPixels > metrics.widthPixels ? metrics.heightPixels : metrics.widthPixels) * 0.85);
                } else {
                    maxHeight = (int) ((metrics.heightPixels < metrics.widthPixels ? metrics.heightPixels : metrics.widthPixels) * 0.85);
                }
                int height = MeasureSpec.getSize(heightMeasureSpec);
                if (height > maxHeight)
                    heightMeasureSpec = MeasureSpec.makeMeasureSpec(maxHeight, MeasureSpec.AT_MOST);
                super.onMeasure(widthMeasureSpec, heightMeasureSpec);
            }
        };
        layout = LayoutInflater.from(activity).inflate(R.layout.dialog_default, null);
        layoutUtil.fitAllView(layout);
        title = (TextView) layout.findViewById(R.id.message_title);
        msg = (TextView) layout.findViewById(R.id.msg);
        llView = (LinearLayout) layout.findViewById(R.id.ll_view);
        llDate = (LinearLayout) layout.findViewById(R.id.ll_date);
        llTime = (LinearLayout) layout.findViewById(R.id.ll_time);
        npYear = (FNNumberPicker) layout.findViewById(R.id.np_year);
        npMonth = (FNNumberPicker) layout.findViewById(R.id.np_month);
        npDay = (FNNumberPicker) layout.findViewById(R.id.np_day);
        npHour = (FNNumberPicker) layout.findViewById(R.id.np_hour);
        npMinute = (FNNumberPicker) layout.findViewById(R.id.np_minute);
        lv = (ListView) layout.findViewById(R.id.lv);
        view = (FrameLayout) layout.findViewById(R.id.view);
        divider_top = layout.findViewById(R.id.divider_top);
        divider_bot = layout.findViewById(R.id.divider_bot);
        divider_btn1 = layout.findViewById(R.id.divider_btn1);
        divider_btn2 = layout.findViewById(R.id.divider_btn2);
        btn1 = (Button) layout.findViewById(R.id.btn1);
        btn2 = (Button) layout.findViewById(R.id.btn2);
        btn3 = (Button) layout.findViewById(R.id.btn3);
        itemEt = (EditText) layout.findViewById(R.id.item_et);
        init();
        main.addView(layout);
        super.setView(main);
    }

    private void init() {
        title.setVisibility(View.GONE);
        llDate.setVisibility(View.GONE);
        llTime.setVisibility(View.GONE);
        msg.setVisibility(View.GONE);
        lv.setVisibility(View.GONE);
        view.setVisibility(View.GONE);
        divider_top.setVisibility(View.GONE);
        divider_bot.setVisibility(View.GONE);
        divider_btn1.setVisibility(View.GONE);
        divider_btn2.setVisibility(View.GONE);
        btn1.setVisibility(View.GONE);
        btn2.setVisibility(View.GONE);
        btn3.setVisibility(View.GONE);
    }

    public FNCustomDialog setCancelable(boolean cancelable) {
        super.setCancelable(cancelable);
        return this;
    }

    public FNCustomDialog setTitle(CharSequence title) {
        if (title != null) {
            this.title.setText(title);
            this.title.setVisibility(View.VISIBLE);
            divider_top.setVisibility(View.VISIBLE);
        } else {
            this.title.setVisibility(View.GONE);
            divider_top.setVisibility(View.GONE);
        }
        freshViewPadding();
        return this;
    }

    public CharSequence getTitle() {
        if (title != null)
            return title.getText();
        return "";
    }

    public FNCustomDialog setDatePicker(int year, int monthOfYear, int dayOfMonth, final OnDateChangedListener onDateChangedListener) {
        final Calendar calendar = Calendar.getInstance();
        calendar.set(year, monthOfYear, dayOfMonth);
        int divider = R.color.main_bg_level1;
        int color = itemEt.getCurrentTextColor();
        float size = itemEt.getTextSize();
        NumberPicker.OnValueChangeListener onValueChangeListener = new NumberPicker.OnValueChangeListener() {
            @Override
            public void onValueChange(NumberPicker picker, int oldVal, int newVal) {
                if (picker == npDay) {
                    int maxDayOfMonth = calendar.getActualMaximum(Calendar.DAY_OF_MONTH);
                    if (oldVal == maxDayOfMonth && newVal == 1) {
                        calendar.add(Calendar.DAY_OF_MONTH, 1);
                    } else if (oldVal == 1 && newVal == maxDayOfMonth) {
                        calendar.add(Calendar.DAY_OF_MONTH, -1);
                    } else {
                        calendar.add(Calendar.DAY_OF_MONTH, newVal - oldVal);
                    }
                    if (calendar.get(Calendar.YEAR) > MAX_YEAR) {
                        calendar.set(Calendar.YEAR, MIN_YEAR);
                    } else if (calendar.get(Calendar.YEAR) < MIN_YEAR) {
                        calendar.set(Calendar.YEAR, MAX_YEAR);
                    }
                } else if (picker == npMonth) {
                    if (oldVal == 12 && newVal == 1) {
                        calendar.add(Calendar.MONTH, 1);
                    } else if (oldVal == 1 && newVal == 12) {
                        calendar.add(Calendar.MONTH, -1);
                    } else {
                        calendar.add(Calendar.MONTH, newVal - oldVal);
                    }
                    if (calendar.get(Calendar.YEAR) > MAX_YEAR) {
                        calendar.set(Calendar.YEAR, MIN_YEAR);
                    } else if (calendar.get(Calendar.YEAR) < MIN_YEAR) {
                        calendar.set(Calendar.YEAR, MAX_YEAR);
                    }
                } else if (picker == npYear) {
                    calendar.set(Calendar.YEAR, newVal);
                }
                npDay.setMaxValue(calendar.getActualMaximum(Calendar.DAY_OF_MONTH));
                npDay.setMinValue(calendar.getActualMinimum(Calendar.DAY_OF_MONTH));
                npDay.setValue(calendar.get(Calendar.DAY_OF_MONTH));
                npMonth.setValue(calendar.get(Calendar.MONTH) + 1);
                npYear.setValue(calendar.get(Calendar.YEAR));
                if (onDateChangedListener != null)
                    onDateChangedListener.onDateChanged(FNCustomDialog.this, calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH), calendar.get(Calendar.DAY_OF_MONTH));
            }
        };
        npYear.setDivider(divider);
        npYear.setTextColor(color);
        npYear.setTextSize(size);
        npYear.setFormatter(new NumberPicker.Formatter() {
            @Override
            public String format(int value) {
                return String.format("%04d", value);
            }
        });
        npYear.setMaxValue(MAX_YEAR);
        npYear.setMinValue(MIN_YEAR);
        npYear.setValue(year);
        npYear.setOnValueChangedListener(onValueChangeListener);
        npMonth.setDivider(divider);
        npMonth.setTextColor(color);
        npMonth.setTextSize(size);
        npMonth.setFormatter(new NumberPicker.Formatter() {
            @Override
            public String format(int value) {
                return String.format("%02d", value);
            }
        });
        npMonth.setMaxValue(calendar.getActualMaximum(Calendar.MONTH) + 1);
        npMonth.setMinValue(calendar.getActualMinimum(Calendar.MONTH) + 1);
        npMonth.setValue(monthOfYear + 1);
        npMonth.setOnValueChangedListener(onValueChangeListener);
        npDay.setDivider(divider);
        npDay.setTextColor(color);
        npDay.setTextSize(size);
        npDay.setFormatter(new NumberPicker.Formatter() {
            @Override
            public String format(int value) {
                return String.format("%02d", value);
            }
        });
        npDay.setMaxValue(calendar.getActualMaximum(Calendar.DAY_OF_MONTH));
        npDay.setMinValue(calendar.getActualMinimum(Calendar.DAY_OF_MONTH));
        npDay.setValue(dayOfMonth);
        npDay.setOnValueChangedListener(onValueChangeListener);
        llDate.setVisibility(View.VISIBLE);
        return this;
    }

    public FNCustomDialog setTimePicker(int hour, final int minute, final OnTimeChangedListener onTimeChangedListener) {
        final Calendar calendar = Calendar.getInstance();
        calendar.set(Calendar.HOUR_OF_DAY, hour);
        calendar.set(Calendar.MINUTE, minute);
        int divider = R.color.transparent;
        int color = itemEt.getCurrentTextColor();
        float size = itemEt.getTextSize();
        NumberPicker.OnValueChangeListener onValueChangeListener = new NumberPicker.OnValueChangeListener() {
            @Override
            public void onValueChange(NumberPicker picker, int oldVal, int newVal) {
                if (picker == npMinute) {
                    if (oldVal == 59 && newVal == 0) {
                        calendar.add(Calendar.MINUTE, 1);
                    } else if (oldVal == 0 && newVal == 59) {
                        calendar.add(Calendar.MINUTE, -1);
                    } else {
                        calendar.add(Calendar.MINUTE, newVal - oldVal);
                    }
                } else if (picker == npHour) {
                    calendar.set(Calendar.HOUR_OF_DAY, newVal);
                }
                npMinute.setValue(calendar.get(Calendar.MINUTE));
                npHour.setValue(calendar.get(Calendar.HOUR_OF_DAY));
                if (onTimeChangedListener != null)
                    onTimeChangedListener.onTimeChanged(FNCustomDialog.this, calendar.get(Calendar.HOUR_OF_DAY), calendar.get(Calendar.MINUTE));
            }
        };
        npHour.setDivider(divider);
        npHour.setTextColor(color);
        npHour.setTextSize(size);
        npHour.setFormatter(new NumberPicker.Formatter() {
            @Override
            public String format(int value) {
                return String.format("%02d", value);
            }
        });
        npHour.setMaxValue(calendar.getActualMaximum(Calendar.HOUR_OF_DAY));
        npHour.setMinValue(calendar.getActualMinimum(Calendar.HOUR_OF_DAY));
        npHour.setValue(hour);
        npHour.setOnValueChangedListener(onValueChangeListener);
        npMinute.setDivider(divider);
        npMinute.setTextColor(color);
        npMinute.setTextSize(size);
        npMinute.setFormatter(new NumberPicker.Formatter() {
            @Override
            public String format(int value) {
                return String.format("%02d", value);
            }
        });
        npMinute.setMaxValue(calendar.getActualMaximum(Calendar.MINUTE));
        npMinute.setMinValue(calendar.getActualMinimum(Calendar.MINUTE));
        npMinute.setValue(minute);
        npMinute.setOnValueChangedListener(onValueChangeListener);
        llTime.setVisibility(View.VISIBLE);
        return this;
    }

    public FNCustomDialog setSingleChoiceItems(final CharSequence[] items, final int checkedItem, final OnClickListener listener, final boolean autoDismiss) {
        final boolean[] checkList = new boolean[items.length];
        if (checkedItem >= 0 && checkedItem < items.length)
            checkList[checkedItem] = true;
        lv.setAdapter(new BaseAdapter() {
            @Override
            public int getCount() {
                return items.length;
            }

            @Override
            public Object getItem(int position) {
                return items[position];
            }

            @Override
            public long getItemId(int position) {
                return position;
            }

            @Override
            public View getView(final int position, View convertView, ViewGroup parent) {
                final ItemHolder holder;
                if (convertView == null) {
                    holder = new ItemHolder();
                    convertView = LayoutInflater.from(getContext()).inflate(R.layout.adapter_selectitem, null);
                    layoutUtil.fitAllView(convertView);
                    holder.txt = (TextView) convertView.findViewById(R.id.txt);
                    holder.img = (ImageView) convertView.findViewById(R.id.img_thumb);
                    convertView.setTag(holder);
                } else {
                    holder = (ItemHolder) convertView.getTag();
                }
                holder.txt.setText(items[position]);
                holder.img.setImageResource(checkList[position] ? R.drawable.list_selected : R.drawable.list_unselected);
                convertView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        for (int i = 0; i < checkList.length; i++) {
                            checkList[i] = false;
                        }
                        checkList[position] = true;
                        notifyDataSetChanged();
                        if (listener != null)
                            listener.onClick(FNCustomDialog.this, position);
                        if (autoDismiss && isVisible())
                            dismiss();
                    }
                });
                return convertView;
            }
        });
        lv.setVisibility(View.VISIBLE);
        return this;
    }

    public FNCustomDialog setMultiChoiceItems(final CharSequence[] items, final boolean[] checkedItems, final OnMultiChoiceClickListener listener, final boolean autoDismiss) {
        lv.setAdapter(new BaseAdapter() {
            @Override
            public int getCount() {
                return items.length;
            }

            @Override
            public Object getItem(int position) {
                return items[position];
            }

            @Override
            public long getItemId(int position) {
                return position;
            }

            @Override
            public View getView(final int position, View convertView, ViewGroup parent) {
                final ItemHolder holder;
                if (convertView == null) {
                    holder = new ItemHolder();
                    convertView = LayoutInflater.from(getContext()).inflate(R.layout.adapter_selectitem, null);
                    layoutUtil.fitAllView(convertView);
                    holder.txt = (TextView) convertView.findViewById(R.id.txt);
                    holder.img = (ImageView) convertView.findViewById(R.id.img_thumb);
                    convertView.setTag(holder);
                } else {
                    holder = (ItemHolder) convertView.getTag();
                }
                holder.txt.setText(items[position]);
                holder.img.setImageResource(checkedItems[position] ? R.drawable.list_selected : R.drawable.list_unselected);
                convertView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        checkedItems[position] = !checkedItems[position];
                        holder.img.setImageResource(checkedItems[position] ? R.drawable.list_selected : R.drawable.list_unselected);
                        if (listener != null)
                            listener.onClick(FNCustomDialog.this, position, checkedItems[position]);
                        if (autoDismiss && isVisible())
                            dismiss();
                    }
                });
                return convertView;
            }
        });
        lv.setVisibility(View.VISIBLE);
        return this;
    }

    private class ItemHolder {
        private TextView txt;
        private ImageView img;
    }

    public FNCustomDialog setView(View view, SeekBar sb, ImageView thumb) {
        FNCustomDialog dialog = setView(view);
        sb.setThumb(new BitmapDrawable(getContext().getResources(), Bitmap.createScaledBitmap(((BitmapDrawable) thumb.getDrawable()).getBitmap(), thumb.getLayoutParams().width, thumb.getLayoutParams().height, true)));
        thumb.setVisibility(View.GONE);
        return dialog;
    }

    public FNCustomDialog setView(View view) {
        this.view.removeAllViews();
        if (view != null) {
            layoutUtil.fitAllView(view);
            this.view.addView(view);
            this.view.setVisibility(View.VISIBLE);
        } else {
            this.view.setVisibility(View.GONE);
        }
        return this;
    }

    public FNCustomDialog setMessage(CharSequence message, int gravity) {
        if (message != null) {
            msg.setText(message);
            msg.setVisibility(View.VISIBLE);
            msg.setGravity(gravity);
        } else {
            msg.setVisibility(View.GONE);
        }
        return this;
    }

    public FNCustomDialog setMessage(CharSequence message) {
        return setMessage(message, Gravity.START);
    }

    public FNCustomDialog setNegativeButton(CharSequence text, final OnClickListener listener, final boolean autoDismiss) {
        btn1.setText(text);
        if (listener != null) {
            btn1.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    listener.onClick(FNCustomDialog.this, BUTTON_NEGATIVE);
                    if (autoDismiss && isVisible())
                        dismiss();
                }
            });
        } else {
            btn1.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (autoDismiss && isVisible())
                        dismiss();
                }
            });
        }
        btn1.setVisibility(text != null ? View.VISIBLE : View.GONE);
        freshBtnDivider();
        return this;
    }

    public FNCustomDialog setNeutralButton(CharSequence text, final OnClickListener listener, final boolean autoDismiss) {
        btn2.setText(text);
        if (listener != null) {
            btn2.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    listener.onClick(FNCustomDialog.this, BUTTON_NEUTRAL);
                    if (autoDismiss && isVisible())
                        dismiss();
                }
            });
        } else {
            btn2.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (autoDismiss && isVisible())
                        dismiss();
                }
            });
        }
        btn2.setVisibility(text != null ? View.VISIBLE : View.GONE);
        freshBtnDivider();
        return this;
    }

    public FNCustomDialog setPositiveButton(CharSequence text, final OnClickListener listener, final boolean autoDismiss) {
        btn3.setText(text);
        if (listener != null) {
            btn3.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    listener.onClick(FNCustomDialog.this, BUTTON_POSITIVE);
                    if (autoDismiss && isVisible())
                        dismiss();
                }
            });
        } else {
            btn3.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (autoDismiss && isVisible())
                        dismiss();
                }
            });
        }
        btn3.setVisibility(text != null ? View.VISIBLE : View.GONE);
        freshBtnDivider();
        return this;
    }

    private void freshBtnDivider() {
        if (btn1.getVisibility() == View.VISIBLE && (btn2.getVisibility() == View.VISIBLE || btn3.getVisibility() == View.VISIBLE)) {
            divider_btn1.setVisibility(View.VISIBLE);
        } else {
            divider_btn1.setVisibility(View.GONE);
        }
        if (btn2.getVisibility() == View.VISIBLE && btn3.getVisibility() == View.VISIBLE) {
            divider_btn2.setVisibility(View.VISIBLE);
        } else {
            divider_btn2.setVisibility(View.GONE);
        }
        if (btn1.getVisibility() == View.VISIBLE || btn2.getVisibility() == View.VISIBLE || btn3.getVisibility() == View.VISIBLE) {
            divider_bot.setVisibility(View.VISIBLE);
        } else {
            divider_bot.setVisibility(View.GONE);
        }
        freshViewPadding();
    }

    private void freshViewPadding() {
        int top = (title.getVisibility() == View.VISIBLE ? title.getLayoutParams().height : 0) + (divider_top.getVisibility() == View.VISIBLE ? divider_top.getLayoutParams().height : 0);
        int bot = ((btn1.getVisibility() == View.VISIBLE || btn2.getVisibility() == View.VISIBLE || btn3.getVisibility() == View.VISIBLE) ? btn1.getLayoutParams().height : 0) + (divider_bot.getVisibility() == View.VISIBLE ? divider_bot.getLayoutParams().height : 0);
        ((ViewGroup.MarginLayoutParams) divider_top.getLayoutParams()).setMargins(0, 0, 0, -top);
        ((ViewGroup.MarginLayoutParams) divider_bot.getLayoutParams()).setMargins(0, -bot, 0, 0);
        llView.setPadding(0, top, 0, bot);
//        llView.setMinimumHeight(msg.getMinimumHeight() + top + bot);
    }

    public Button getButton(int position) {
        switch (position) {
            case BUTTON_NEGATIVE:
                return btn1;
            case BUTTON_NEUTRAL:
                return btn2;
            case BUTTON_POSITIVE:
                return btn3;
            default:
                return null;
        }
    }

    public boolean isVisible() {
        return layout != null && layout.isShown();
    }

    public void setViewBackground(int resId) {
        llView.setBackgroundResource(resId);
    }

    public void dismiss() {
        if (isVisible())
            dismissAll();
    }

    public interface OnClickListener {
        void onClick(FNCustomDialog dialog, int which);
    }

    public interface OnMultiChoiceClickListener {
        void onClick(FNCustomDialog dialog, int which, boolean isChecked);
    }

    public interface OnDateChangedListener {
        void onDateChanged(FNCustomDialog dialog, int year, int monthOfYear, int dayOfMonth);
    }

    public interface OnTimeChangedListener {
        void onTimeChanged(FNCustomDialog dialog, int hourOfDay, int minute);
    }
}
