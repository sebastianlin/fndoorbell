package com.fusionnextinc.doorbell;

/**
 * Created by chocoyang on 2016/3/31.
 */
public class FNException extends RuntimeException {
    public FNException(String tag, String detailMessage) {
        super(tag + ": " + detailMessage);
    }
}
