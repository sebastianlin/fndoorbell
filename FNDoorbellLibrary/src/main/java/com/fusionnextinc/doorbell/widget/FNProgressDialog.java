package com.fusionnextinc.doorbell.widget;

import android.app.Activity;
import android.content.Context;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import androidx.appcompat.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;

import com.fusionnextinc.doorbell.MyApplication;
import com.fusionnextinc.doorbell.R;

import az.plainpie.PieView;


public class FNProgressDialog {
    private static final String TAG = FNProgressDialog.class.getSimpleName();

    private Context mContext;
    private PieView mProgressPieView;
    private AlertDialog mDialog;
    private TextView mTextProgressTitle;
    // Utils
    private FNLayoutUtil mLayoutUtil;

    //Update upload progress
    private static final int HANDLER_UPDATE_PROGRESS = 0;
    private static final int MAX_UPDATE_PROGRESS_VALUE = 100;
    private int mCurProgress = 0;
    private long mProgress = -1;
    private long mDuration;

    public FNProgressDialog(Context context) {
        mContext = context;
    }

    public FNProgressDialog show() {
        View view = LayoutInflater.from(mContext).inflate(R.layout.view_progress_dialog, null);
        mLayoutUtil = new FNLayoutUtil((Activity) mContext, MyApplication.LAYOUT_WIDTH,
                MyApplication.LAYOUT_HEIGHT, FNLayoutUtil.BASE_WIDTH);
        mLayoutUtil.fitAllView(view);
        mProgressPieView = (PieView) view.findViewById(R.id.progress_dialog);
        mTextProgressTitle = (TextView) view.findViewById(R.id.text_progress);
        // Change the color fill of the bar representing the current percentage
        mProgressPieView.setPercentageBackgroundColor(mContext.getResources().getColor(R.color.progress_dialog_percentage));
        mProgressPieView.setInnerBackgroundColor(mContext.getResources().getColor(R.color.progress_dialog_percentage_bg));
        mProgressPieView.setPercentageTextSize(30);
        mProgressPieView.setPercentage(0);
        mProgressPieView.setMaxPercentage(100);
        mDialog = new AlertDialog.Builder(mContext)
                .setView(view)
                .setCancelable(false)
                .show();
        mDialog.getWindow().setBackgroundDrawableResource(R.color.transparent);
        return this;
    }

    public void setProgressTitle(int resid) {
        mTextProgressTitle.setText(resid);
    }

    public void setProgressTitle(String title) {
        mTextProgressTitle.setText(title);
    }

    public void updateProgress(final int progress) {
        MyApplication.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if (mProgressPieView != null) {
                    mProgressPieView.setPercentage(progress);
                }
            }
        });
    }

    public void startAnimation(int startProgress, long durationMillis) {
        if (mProgressPieView != null) {
            mCurProgress = startProgress + 1;
            mDuration = durationMillis;
            startUpdateUploadProgress();
        }
    }

    public void stopAnimation() {
        stopUpdateUploadProgress();
    }

    public void dismiss() {
        if (mDialog != null) {
            mDialog.dismiss();
            mDialog = null;
        }
    }

    private Handler handler = new Handler(Looper.getMainLooper()) {
        @Override
        public void handleMessage(Message msg) {
            int delayMillis = 20;

            switch (msg.what) {
                case HANDLER_UPDATE_PROGRESS:
                    if (mProgress > -1) {
                        MyApplication.runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                if (mProgressPieView != null) {
                                    mProgressPieView.setPercentage(mCurProgress +
                                            (int) ((MAX_UPDATE_PROGRESS_VALUE - mCurProgress) * mProgress / mDuration));
                                }
                            }
                        });
                        if (mProgress < mDuration) {
                            mProgress = mProgress + delayMillis;
                            sendEmptyMessageDelayed(HANDLER_UPDATE_PROGRESS, delayMillis);
                        } else {
                            mProgress = mDuration;
                            dismiss();
                        }
                    }
                    break;
            }
        }
    };

    private void startUpdateUploadProgress() {
        handler.removeMessages(HANDLER_UPDATE_PROGRESS);
        mProgress = 0;
        handler.sendEmptyMessage(HANDLER_UPDATE_PROGRESS);
    }

    private void stopUpdateUploadProgress() {
        handler.removeMessages(HANDLER_UPDATE_PROGRESS);
        mProgress = -1;
    }
}
