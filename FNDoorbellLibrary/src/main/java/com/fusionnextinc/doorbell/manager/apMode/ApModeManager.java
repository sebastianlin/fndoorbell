package com.fusionnextinc.doorbell.manager.apMode;

import android.content.Context;
import com.fusionnext.cloud.iot.ApMode;
import com.fusionnext.cloud.iot.IOTHandler;
import com.fusionnext.cloud.iot.objects.ErrorObj;
import com.fusionnext.cloud.iot.objects.WifiAP;
import com.fusionnextinc.doorbell.objects.FNError;
import com.fusionnextinc.doorbell.objects.FNWifiAp;
import com.fusionnext.cloud.iot.Public;
import java.util.ArrayList;

/**
 * Created by Mike Chang on 2018/3/12
 */
public class ApModeManager {
    protected final static String TAG = "ApModeManager";
    private static ApMode mApMode = null;
    private static ApModeManager mInstance;
    private ApModeHandler.ApModeListener mApModeListener = null;

    private ApModeManager(Context context) {
        mApMode = new ApMode(context);
    }

    public static ApModeManager getInstance(Context context) {
        if (mInstance == null) {
            synchronized (ApModeManager.class) {
                if (mInstance == null) {
                    mInstance = new ApModeManager(context);
                }
            }
        }
        return mInstance;
    }

    public void setApModeListener(ApModeHandler.ApModeListener apModeListener) {
        this.mApModeListener = apModeListener;
    }


    public void startScanWifiAp() {
        mApMode.startWifiScan(mWifiApListAdapter);
    }

    public void stopScanWifiAp() {
        mApMode.stopWifiScan();
    }

    public void registerDevice(String ssid, String password, String sec, String claimCode) {
        mApMode.registerDevice(ssid, password, sec,claimCode, mWifiApListAdapter);
    }

    public void changeSsid(String ssid, String password, String sec) {
        mApMode.changeSsid(ssid, password, sec, mWifiApListAdapter);
    }

    private IOTHandler.OnAPModeListener mWifiApListAdapter = new IOTHandler.OnAPModeListener() {
        @Override
        public void onListWifiAP(boolean result, ArrayList<WifiAP> wifiApList, ErrorObj error) {
            if (mApModeListener != null) {
                mApModeListener.onListWifiAP(mInstance, result, FNWifiAp.parseWifiApArray(wifiApList), FNError.parseError(error));
            }
        }

        @Override
        public void onRegisterDevice(boolean result, String macAddr, ErrorObj error) {
            if (mApModeListener != null) {
                mApModeListener.onRegisterDevice(mInstance, result, macAddr, FNError.parseError(error));
            }
        }

        @Override
        public void onChangeSsid(boolean result, String macAddr, ErrorObj error) {
            if (mApModeListener != null) {
                mApModeListener.onChangeSsid(mInstance, result, macAddr, FNError.parseError(error));
            }
        }
    };

}
