# Add project specific ProGuard rules here.
# By default, the flags in this file are appended to flags specified
# in /Users/chocoyang/Library/Android/sdk/tools/proguard/proguard-android.txt
# You can edit the include path and order by changing the proguardFiles
# directive in build.gradle.
#
# For more details, see
#   http://developer.android.com/guide/developing/tools/proguard.html

# Add any project specific keep options here:

# If your project uses WebView with JS, uncomment the following
# and specify the fully qualified class name to the JavaScript interface
# class:
#-keepclassmembers class fqcn.of.javascript.interface.for.webview {
#   public *;
#}
-keep class com.fusionnext.cloud.** {public *;}
-dontwarn io.socket.**
-keep class io.socket.** {public *; }
-keep interface io.socket.** {public *; }
-keep enum io.socket.** {public *;}

-dontwarn com.facebook.**
-keep class com.facebook.** {public *;}
-keep interface com.facebook.** {public *;}
-keep enum com.facebook.** {public *;}

-dontwarn com.mediatek.**
-keep class com.mediatek.** {public *;}
-keep interface com.mediatek.** {public *;}
-keep enum com.mediatek.** {public *;}

-dontwarn com.fusionnext.cloud.datalink.json.**
-keep class com.fusionnext.cloud.datalink.json.** {public *;}
-keep interface com.fusionnext.cloud.datalink.json.** {public *;}
-keep enum com.fusionnext.cloud.datalink.json.** {public *;}