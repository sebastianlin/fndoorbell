package com.fusionnextinc.doorbell.fragement;

import androidx.fragment.app.Fragment;

import com.fusionnextinc.doorbell.MyApplication;
import com.fusionnextinc.doorbell.widget.FNActionBar;

/**
 * Created by koukiyoshiyuu on 2017/11/13.
 */

public class FNFragment extends Fragment {
    private static final String TAG = FNFragment.class.getSimpleName();

    public FNActionBar getActionBar() {
        return FNFragmentManager.getInstance().getActionBar();
    }

    public void onBackPressed() {
        FNFragmentManager.getInstance().onBackPressed();
    }

    public void onBackPressed(boolean exitDirectly) {
        FNFragmentManager.getInstance().onBackPressed(exitDirectly);
    }

    public void runOnUiThread(Runnable action) {
        MyApplication.runOnUiThread(action);
    }

    public void runOnUiThread(Runnable action, long delayMillis) {
        MyApplication.runOnUiThread(action, delayMillis);
    }

}
