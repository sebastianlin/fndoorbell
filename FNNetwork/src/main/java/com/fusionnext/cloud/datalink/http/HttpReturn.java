package com.fusionnext.cloud.datalink.http;

/**
 * Created by Mike Chang on 2016/2/22.
 */
public class HttpReturn {

    public boolean result = false;
    //Currently, only be used for app to get json response from server
    //to get want list.
    public String returnString = "";

    public HttpReturn(boolean successful, String returnString) {
        super();
        this.result = successful;
        this.returnString = returnString;
    }
}
