package com.fusionnext.cloud.iot;

import com.fusionnext.cloud.utils.FNNetworkLog;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URL;
import java.net.URLEncoder;

/**
 * Created by Mike Chang on 2016/2/22.
 */

public class HttpData {
    protected final static String _TAG = "HttpData";
    //HttpPost Type
    public final static int _POST_TYPE_UNKNOWN = 0x09;
    public final static int _POST_TYPE_UPLOAD_FILE = 0x10;
    public final static int _POST_TYPE_AUTH_GET_CLIENT_TOKEN = 0x11;
    public final static int _POST_TYPE_AUTH_REFRESH_CLIENT_TOKEN = 0x12;
    public final static int _POST_TYPE_AUTH_LOGIN_BY_EMAIL = 0x13;
    public final static int _POST_TYPE_USER_CREATE_USER_BY_EMAIL = 0x14;
    public final static int _POST_TYPE_USER_CREATE_USER_BY_FACEBOOK = 0x15;
    public final static int _POST_TYPE_USER_RESET_PASSWORD = 0x16;
    public final static int _POST_TYPE_USER_SET_PASSWORD = 0x17;
    public final static int _POST_TYPE_USER_RESEND= 0x18;
    /*THING*/
    public final static int _POST_TYPE_THINGS_CREATE_THING = 0x1A;
    public final static int _POST_TYPE_THINGS_INVITE = 0x1B;
    public final static int _POST_TYPE_THINGS_CREATE_CLAIMCODE = 0x1C;
    /*DEVICE*/
    public final static int _POST_TYPE_DEVICES_CREATE_DEVICE = 0x1D;
    /*PUSHTOKEN*/
    public final static int _POST_TYPE_PUSHTOKENS_CREATE_PUSHTOKEN = 0x1E;
    /*SETTINGS*/
    public final static int _POST_TYPE_THINGS_CREATE_SETTINGS = 0x1F;
    /*Smart Plug*/
    public final static int _POST_TYPE_SMART_PLUG_SWITCH_ON_OFF = 0x21;
    /*OTA*/
    public final static int _POST_TYPE_FW_START_UPDATE = 0x21;
    //HttpGet Type
    /*USER*/
    public final static int _GET_TYPE_USER_LIST_OWNED_THINGS = 0x22;
    public final static int _GET_TYPE_USER_LIST_INVITED_THINGS = 0x23;
    public final static int _GET_TYPE_USER_LIST_SHARED_THINGS = 0x24;
    public final static int _GET_TYPE_USER_LIST_ALL_THINGS = 0x25;
    public final static int _GET_TYPE_USER_GET_USER = 0x26;
    /*THING*/
    public final static int _GET_TYPE_THINGS_LIST_STATES = 0x33;
    public final static int _GET_TYPE_THINGS_LIST_SETTINGS = 0x34;
    public final static int _GET_TYPE_THINGS_LIST_EVENTS = 0x35;
    public final static int _GET_TYPE_THINGS_LIST_EVENTS_BY_TIME = 0x36;
    public final static int _GET_TYPE_THINGS_LIST_INVITED_USERS = 0x37;
    public final static int _GET_TYPE_THINGS_LIST_SHARED_USERS = 0x38;
    public final static int _GET_TYPE_THINGS_GET_THING = 0x39;
    /*OTA*/
    public final static int _GET_TYPE_FW_SEARCH_UPDATE = 0x12;
    public final static int _GET_TYPE_FW_GET_UPDATE = 0x13;
    public final static int _GET_TYPE_SW_LIST_UPDATES = 0x14;
    public final static int _GET_TYPE_SW_GET_UPDATE = 0x15;
    /*PTCOM*/
    public final static int _GET_TYPE_PTCOM_LIST_WIFI_AP = 0x61;
    public final static int _GET_TYPE_PTCOM_REGISTER = 0x62;
    public final static int _GET_TYPE_PTCOM_CHANGE_SSID = 0x63;
    //HttpPut Type
    public final static int _PUT_TYPE_UNKNOWN = 0x40;
    public final static int _PUT_TYPE_USER_UPDATE_USER = 0x41;
    public final static int _PUT_TYPE_USER_FOLLOW_USER = 0x42;
    public final static int _PUT_TYPE_TRACKS_UPDATE_TRACK = 0x43;
    /*THING*/
    public final static int _PUT_TYPE_THINGS_UPDATE_THING = 0x55;
    public final static int _PUT_TYPE_THINGS_ACCEPT_INVITATION = 0x56;
    /*DEVICE*/
    public final static int _PUT_TYPE_DEVICES_UPDATE_DEVICE = 0x57;
    /*CALL SLOTS*/
    public final static int _PUT_TYPE_THINGS_TAKE_CALL_SLOT = 0x58;

    //HttpDelete Type
    public final static int _DEL_TYPE_UNKNOWN = 0x60;
    public final static int _DEL_TYPE_THINGS_DELETE_THING = 0x62;
    public final static int _DEL_TYPE_THINGS_DELETE_DEVICE = 0x63;
    public final static int _DEL_TYPE_THINGS_REJECT_INVITATION = 0x64;
    /*CALL SLOTS*/
    public final static int _DEL_TYPE_THINGS_CLOSE_CALL_SLOT = 0x65;

    public final static int _GET_TYPE_SORT_ORDER_DESC = 0;
    public final static int _GET_TYPE_SORT_ORDER_ASC = 1;

    protected static URL generateHttpPostUrl(int postType) {
        return generateHttpPostUrl(postType, null);
    }

    protected static URL generateHttpPostUrl(int postType, String id) {
        URL url = null;
        try {
            switch (postType) {
                case _POST_TYPE_UPLOAD_FILE:
                    url = new URL(Public._INTERFACE_POST_UPLOAD_FILE());
                    break;
                case _POST_TYPE_AUTH_GET_CLIENT_TOKEN:
                    url = new URL(Public._INTERFACE_POST_GET_CLIENT_TOKEN());
                    break;
                case _POST_TYPE_AUTH_REFRESH_CLIENT_TOKEN:
                    url = new URL(Public._INTERFACE_POST_REFRESH_DEVICE_TOKEN());
                    break;
                case _POST_TYPE_AUTH_LOGIN_BY_EMAIL:
                    url = new URL(Public._INTERFACE_POST_LOGIN_BY_EMAIL());
                    break;


                case _POST_TYPE_USER_CREATE_USER_BY_EMAIL:
                case _POST_TYPE_USER_CREATE_USER_BY_FACEBOOK:
                    url = new URL(Public._INTERFACE_POST_CREATE_USER());
                    break;

                case _POST_TYPE_USER_RESEND:
                    url = new URL(Public._INTERFACE_POST_RESEND());
                    break;
                case _POST_TYPE_THINGS_CREATE_THING:
                    url = new URL(Public._INTERFACE_POST_CREATE_THING());
                    break;
                case _POST_TYPE_THINGS_INVITE:
                    url = new URL(Public._INTERFACE_POST_THING_INVITE().replace("{thing_id}", id));
                    break;
                case _POST_TYPE_THINGS_CREATE_CLAIMCODE:
                    url = new URL(Public._INTERFACE_POST_CREATE_CLAIMCODE());
                    break;
                case _POST_TYPE_DEVICES_CREATE_DEVICE:
                    url = new URL(Public._INTERFACE_POST_CREATE_DEVICE());
                    break;
                case _POST_TYPE_PUSHTOKENS_CREATE_PUSHTOKEN:
                    url = new URL(Public._INTERFACE_POST_CREATE_PUSH_TOKEN());
                    break;
                case _POST_TYPE_THINGS_CREATE_SETTINGS:
                    url = new URL(Public._INTERFACE_POST_CREATE_SETTINGS());
                    break;
                case _POST_TYPE_FW_START_UPDATE:
                    url = new URL(Public._INTERFACE_POST_FW_START_UPDATE());
                    break;
                case _POST_TYPE_USER_RESET_PASSWORD:
                    url = new URL(Public._INTERFACE_POST_RESET_USER_PASSWORD());
                    break;
                case _POST_TYPE_USER_SET_PASSWORD:
                    url = new URL(Public._INTERFACE_POST_SET_USER_PASSWORD());
                    break;
                default:
                    return url;
            }
            FNNetworkLog.d(_TAG, url.toString());
        } catch (IOException e) {
            e.printStackTrace();
        }
        return url;
    }

    protected static String generateHttpGetUrl(int postType, int order, String pageSize, String pageNum) {
        return generateHttpGetUrl(postType, null, null, null, null, order, pageSize, pageNum);
    }

    protected static String generateHttpGetUrl(int postType, String id, String name, int order, String pageSize, String pageNum) {
        return generateHttpGetUrl(postType, id, name, null, null, order, pageSize, pageNum);
    }

    protected static String generateHttpGetUrl(int getType, String id, String name, String startTime, String endTime, int order, String pageSize, String pageNum) {
        String url = null;

        switch (getType) {
            case _GET_TYPE_USER_LIST_OWNED_THINGS:
                url = Public._INTERFACE_GET_LIST_USER_OWNED_THINGS().replace("{user_id}", id);
                url = genGetUrl(url, order, pageSize, pageNum);
                break;
            case _GET_TYPE_USER_LIST_INVITED_THINGS:
                url = Public._INTERFACE_GET_LIST_USER_INVITED_THINGS().replace("{user_id}", id);
                url = genGetUrl(url, order, pageSize, pageNum);
                break;
            case _GET_TYPE_USER_LIST_SHARED_THINGS:
                url = Public._INTERFACE_GET_LIST_USER_SHARED_THINGS().replace("{user_id}", id);
                url = genGetUrl(url, order, pageSize, pageNum);
                break;
            case _GET_TYPE_USER_LIST_ALL_THINGS:
                url = Public._INTERFACE_GET_LIST_USER_ALL_THINGS().replace("{user_id}", id);
                url = genGetUrl(url, order, pageSize, pageNum);
                break;
            case _GET_TYPE_USER_GET_USER:
                url = Public._INTERFACE_GET_GET_USER().replace("{user_id}", id);
                break;
            case _GET_TYPE_THINGS_LIST_STATES:
                url = Public._INTERFACE_GET_LIST_THING_STATES().replace("{thing_id}", id);
                if (name != null) {
                    url = url + "?name=" + name + "&";
                }
                url = genGetUrl(url, order, pageSize, pageNum);
                break;
            case _GET_TYPE_THINGS_LIST_SETTINGS:
                url = Public._INTERFACE_GET_LIST_THING_SETTINGS().replace("{thing_id}", id);
                if (name != null) {
                    url = url + "?name=" + name + "&";
                }
                url = genGetUrl(url, order, pageSize, pageNum);
                break;
            case _GET_TYPE_THINGS_LIST_EVENTS:
                url = Public._INTERFACE_GET_LIST_THING_EVENTS().replace("{thing_id}", id);
                if (name != null) {
                    url = url + "?name=" + name + "&";
                } else {
                    url = url + "?";
                }
                url = genGetUrl(url, order, pageSize, pageNum);
                break;
            case _GET_TYPE_THINGS_LIST_EVENTS_BY_TIME:
                url = Public._INTERFACE_GET_LIST_THING_EVENTS().replace("{thing_id}", id);
                if (name != null) {
                    url = url + "?name=" + name + "&";
                } else {
                    url = url + "?";
                }
                if (startTime != null) {
                    startTime = startTime.replaceAll(" ", "%20");
                    url = url + "start=" + startTime + "&";
                }
                if (endTime != null) {
                    endTime = endTime.replaceAll(" ", "%20");
                    url = url + "end=" + endTime + "&";
                }
                url = genGetUrl(url, order, pageSize, pageNum);
                break;
            case _GET_TYPE_THINGS_GET_THING:
                url = Public._INTERFACE_GET_GET_THING_INFO().replace("{thing_id}", id);
                break;
            case _GET_TYPE_THINGS_LIST_INVITED_USERS:
                url = Public._INTERFACE_GET_LIST_THING_INVITED_USERS().replace("{thing_id}", id);
                break;
            case _GET_TYPE_THINGS_LIST_SHARED_USERS:
                url = Public._INTERFACE_GET_LIST_THING_SHARED_USERS().replace("{thing_id}", id);
                break;
            case _GET_TYPE_FW_SEARCH_UPDATE:
                url = Public._INTERFACE_SEARCH_FW_UPDATE_LIST().replace("{product_id}", id);
                url = genGetUrl(url, order, pageSize, pageNum);
                break;
            case _GET_TYPE_FW_GET_UPDATE:
                url = Public._INTERFACE_GET_FW_UPDATE_LINK().replace("{fw_id}", id);
                break;
            case _GET_TYPE_SW_LIST_UPDATES:
                url = Public._INTERFACE_GET_SW_UPDATE_LIST().replace("{client_id}", id);
                break;
            case _GET_TYPE_SW_GET_UPDATE:
                url = Public._INTERFACE_GET_SW_UPDATE_LINK().replace("{client_id}", id);
                break;
            default:
                return url;
        }
        FNNetworkLog.d(_TAG, url);

        return url;
    }

    protected static String generatePtComHttpGetUrl(int getType, String ssid, String password, String sec, String claimCode) {
        String url = null;

        try {
            if (ssid != null) {
                ssid = URLEncoder.encode(ssid, "utf-8");
                ssid = ssid.replace("+", "%20");
            }
            if (password != null) {
                password = URLEncoder.encode(password, "utf-8");
                password = password.replace("+", "%20");
            }
            if (sec != null) {
                sec = URLEncoder.encode(sec, "utf-8");
                sec = sec.replace("+", "%20");
            }
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }


        switch (getType) {
            case _GET_TYPE_PTCOM_LIST_WIFI_AP:
                url = Public._PTCOM_INTERFACE_GET_WIFI_AP_LIST;
                break;

            case _GET_TYPE_PTCOM_REGISTER:
                url = Public._PTCOM_INTERFACE_GET_REGISTER.replace("{ssid}", ssid).
                        replace("{pwd}", password).replace("{sec}", sec).
                        replace("{claim_code}", claimCode).
                        replace("{ipaddr}", Public.ServerIP()).
                        replace("{mode}", Public.DeviceModeString()
                        );
                break;

            case _GET_TYPE_PTCOM_CHANGE_SSID:
                url = Public._PTCOM_INTERFACE_GET_CHANGE_SSID.replace("{ssid}", ssid).
                        replace("{pwd}", password).replace("{sec}", sec);
                break;
            default:
                break;
        }


        FNNetworkLog.d(_TAG, url);
        return url;
    }

    private static String genGetUrl(String url, int order, String pageSize, String pageNum) {
        String sOrder;
        String output;
        if (order == _GET_TYPE_SORT_ORDER_ASC) {
            sOrder = "asc";
        } else {
            sOrder = "desc";
        }
        output = url + "order=" + sOrder;

        if (pageSize != null) {
            output = output + "&limit=" + pageSize;
        }
        if (pageNum != null) {
            output = output + "&page=" + pageNum;
        }
        return output;
    }

    protected static URL generateHttpPutUrl(int postType) {
        return generateHttpPutUrl(postType, null);
    }

    protected static URL generateHttpPutUrl(int putType, String id) {
        URL url = null;
        try {
            switch (putType) {
                case _PUT_TYPE_USER_UPDATE_USER:
                    url = new URL(Public._INTERFACE_PUT_UPDATE_USER().replace("{user_id}", id));
                    break;

                case _PUT_TYPE_THINGS_UPDATE_THING:
                    url = new URL(Public._INTERFACE_PUT_UPDATE_THING().replace("{thing_id}", id));
                    break;
                case _PUT_TYPE_DEVICES_UPDATE_DEVICE:
                    url = new URL(Public._INTERFACE_PUT_UPDATE_DEVICE().replace("{device_id}", id));
                    break;
                case _PUT_TYPE_THINGS_ACCEPT_INVITATION:
                    url = new URL(Public._INTERFACE_PUT_THING_ACCEPT().replace("{thing_id}", id));
                    break;
                case _PUT_TYPE_THINGS_TAKE_CALL_SLOT:
                    url = new URL(Public._INTERFACE_PUT_THING_TAKE_CALL_SLOT().replace("{call_slot}", id));
                    break;
                default:
                    return url;
            }
            FNNetworkLog.d(_TAG, url.toString());
        } catch (IOException e) {
            e.printStackTrace();
        }
        return url;
    }

    protected static URL generateHttpDeleteUrl(int deleteType, String id) {
        URL url = null;
        try {
            switch (deleteType) {
                case _DEL_TYPE_THINGS_DELETE_THING:
                    url = new URL(Public._INTERFACE_DEL_DELETE_THING().replace("{thing_id}", id));
                    break;
                case _DEL_TYPE_THINGS_DELETE_DEVICE:
                    url = new URL(Public._INTERFACE_DEL_DELETE_DEVICE().replace("{device_id}", id));
                    break;
                case _DEL_TYPE_THINGS_REJECT_INVITATION:
                    url = new URL(Public._INTERFACE_DEL_THING_REJECT().replace("{thing_id}", id));
                    break;
                case _DEL_TYPE_THINGS_CLOSE_CALL_SLOT:
                    url = new URL(Public._INTERFACE_DEL_THING_CLOSE_CALL_SLOT().replace("{call_slot}", id));
                    break;
                default:
                    return url;
            }
            FNNetworkLog.d(_TAG, url.toString());
        } catch (IOException e) {
            e.printStackTrace();
        }
        return url;
    }
}
