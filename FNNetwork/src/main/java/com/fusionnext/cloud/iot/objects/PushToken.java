package com.fusionnext.cloud.iot.objects;

import android.util.Log;

import com.fusionnext.cloud.datalink.json.FNJsonObject;

/**
 * Created by Mike Chang on 2016/2/22.
 */
public class PushToken {

    protected final static String TAG = "PushToken";

    public final static String LIKES_STRING_TYPE_FIREBASE = "firebase";

    public PushToken() {
        _type = LIKES_STRING_TYPE_FIREBASE;
        _deviceId = "";
        _pushToken = "";
    }

    public String _type;
    private String _deviceId;
    public String _pushToken;

    public void clean() {
        _type = LIKES_STRING_TYPE_FIREBASE;
        _deviceId = "";
        _pushToken = "";
    }

    public boolean getDataFromJSONObject(FNJsonObject pushTokenObj) {
        clean();
        if (pushTokenObj == null) {
            Log.w(TAG, " No JSON information");
            return false;
        }
        try {
            _deviceId = pushTokenObj.getStringDefault("device_id");
            _type = pushTokenObj.getStringDefault("type");
            _pushToken = pushTokenObj.getStringDefault("push_token");
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
        return true;
    }
}
