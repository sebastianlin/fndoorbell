package com.fusionnext.cloud.social;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Environment;

import java.io.File;
import java.io.IOException;

public class Public {

    /* private synchronized static MyParameter getMyParam(Context context) {
         return (MyParameter) context.getApplicationContext();
     }*/
    static MyParameter instance = null;

    private synchronized static MyParameter getMyParam(Context context) {
        if (instance == null) {
            instance = new MyParameter();
        }
        return instance;
        //return (MyApplication) context.getApplicationContext();
    }

    public synchronized static Authorization getAuthorization(Context context) {
        return getMyParam(context).getAuthorization(context);
    }

    public synchronized static void setAuthorization(Context context, Authorization authorization) {
        getMyParam(context).setAuthorization(authorization);
    }


    public synchronized static Tracks getTrips(Context context) {
        return getMyParam(context).getTrips(context);
    }

    public synchronized static void setTrips(Context context, Tracks tracks) {
        getMyParam(context).setTracks(tracks);
    }

    public synchronized static Photos getPhotos(Context context) {
        return getMyParam(context).getPhotos(context);
    }

    public synchronized static void setPhotos(Context context, Photos photos) {
        getMyParam(context).setPhotos(photos);
    }

    public synchronized static User getUser(Context context) {
        return getMyParam(context).getUser(context);
    }

    public synchronized static void setUser(Context context, User user) {
        getMyParam(context).setUser(user);
    }

    public synchronized static Groups getGroup(Context context) {
        return getMyParam(context).getGroup(context);
    }

    public synchronized static void setGroup(Context context, Groups group) {
        getMyParam(context).setGroup(group);
    }

    public synchronized static DataManager getDataMgr(Context context) {
        return getMyParam(context).getDataMgr(context);
    }

    public synchronized static void setDataMgr(Context context, DataManager dm) {
        getMyParam(context).setDataMgr(dm);
    }

    public static void initCacheFolder() {
        //Init the cache folder to create the .nomedia file
        File sdFolder = Environment.getExternalStorageDirectory();
        File fileCache = new File(sdFolder, Public.Defines._APP_SD_CACHE_DIR);
        if (!fileCache.exists()) fileCache.mkdirs();

        File fileNoMedia = new File(fileCache.getAbsolutePath() +
                "/" + Public.Defines._APP_NO_MEDIA_FILE);
        try {
            fileNoMedia.createNewFile();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    //============================================================================
    //
    //Public Parameters for whole project to use
    //public static Account account;

    //Public definitions class
    public static class Defines {
        //Database
        public final static int _DB_VERSION = 28;
        public final static String _DB_NAME = "com.fusionnext.social.dat";

        //Application used
        public final static String _APP_SD_CACHE_DIR = "doweing_cache";
        public final static String _APP_UPLOAD_IMAGE_FILE = "upload_image.jpg";
        public final static String _APP_NO_MEDIA_FILE = ".nomedia";

        public final static int _MAX_VIDEO_CAPTURE_SECONDS = 10;
        public static final int CONTENT_MAX_COUNT = 150;
    }
    //============================================================================

    //============================================================================
    //Interface url for Doweing
    //Beta server, internet
    public final static String _HOST_V1 = "https://testsocial.fusionnextinc.com/v1";

    //Need authorization
    public final static String _INTERFACE_POST_GET_CLIENT_TOKEN = _HOST_V1 + "/auth";
    public final static String _INTERFACE_POST_REFRESH_DEVICE_TOKEN = _HOST_V1 + "/auth";
    public final static String _INTERFACE_POST_LOGIN_BY_EMAIL = _HOST_V1 + "/auth";
    //Users
    public final static String _INTERFACE_POST_CREATE_USER = _HOST_V1 + "/users";
    public final static String _INTERFACE_PUT_UPDATE_USER = _HOST_V1 + "/users/{user_id}";
    public final static String _INTERFACE_GET_GET_USER = _HOST_V1 + "/users/{user_id}?include=followsmeta,followersmeta,tracksmeta&";
    public final static String _INTERFACE_GET_SEARCH_USER = _HOST_V1 + "/users/search/";
    public final static String _INTERFACE_GET_SHOW_AVATAR = _HOST_V1 + "/users/{user_id}/avatar.jpg";
    public final static String _INTERFACE_POST_FOLLOW_USER = _HOST_V1 + "/users/{user_id}/followers";
    public final static String _INTERFACE_DEL_UNFOLLOW_USER = _HOST_V1 + "/users/{user_id}/followers";
    public final static String _INTERFACE_GET_LIST_USER_FOLLOWERS = _HOST_V1 + "/users/{user_id}/followers?include=followsmeta,followersmeta,tracksmeta&";
    public final static String _INTERFACE_GET_LIST_USER_FOLLOWS = _HOST_V1 + "/users/{user_id}/follows?include=followsmeta,followersmeta,tracksmeta&";
    public final static String _INTERFACE_GET_LIST_USER_TRACKS = _HOST_V1 + "/users/{user_id}/tracks?include=likes,likesmeta,comments,commentsmeta,user.followersmeta,user.followsmeta&";
    public final static String _INTERFACE_GET_LIST_USER_INVITATIONS = _HOST_V1 + "/users/{user_id}/groups?";
    public final static String _INTERFACE_GET_LIST_USER_GROUPS = _HOST_V1 + "/users/{user_id}/groups?";
    public final static String _INTERFACE_GET_LIST_USER_DEVICES = _HOST_V1 + "/users/{user_id}/devices?";
    //Articles
    public final static String _INTERFACE_POST_CREATE_ARTICLE = _HOST_V1 + "/articles";
    public final static String _INTERFACE_PUT_EDIT_ARTICLE = _HOST_V1 + "/articles/{article_id}";
    public final static String _INTERFACE_GET_GET_ARTICLE = _HOST_V1 + "/articles/{article_id}";
    public final static String _INTERFACE_POST_COMMENT_ARTICLE = _HOST_V1 + "/articles/{article_id}/comments";
    public final static String _INTERFACE_POST_LIKE_ARTICLE = _HOST_V1 + "/articles/{article_id}/likes";
    public final static String _INTERFACE_GET_ARTICLE_COVER = _HOST_V1 + "/articles/{article_id}/cover.jpg?size=medium";
    //Client
    public final static String _INTERFACE_GET_GET_CLIENT = _HOST_V1 + "/clients/{client_id}";
    public final static String _INTERFACE_GET_LIST_ARTICLE = _HOST_V1 + "/clients/{client_id}/articles?sort=created_at&";

    //Message
    public final static String _INTERFACE_GET_GET_MESSAGE = _HOST_V1 + "/messages/{message_id}";

    //Comments
    public final static String _INTERFACE_GET_GET_COMMENT = _HOST_V1 + "/comments/{comment_id}?include=user,likes,likesmeta&";
    public final static String _INTERFACE_PUT_UPDATE_COMMENT = _HOST_V1 + "/comments/{comment_id}";
    public final static String _INTERFACE_POST_LIKE_COMMENT = _HOST_V1 + "/comments/{comment_id}/likes";
    public final static String _INTERFACE_GET_LIST_COMMENT_LIKES = _HOST_V1 + "/comments/{comment_id}/likes?";
    public final static String _INTERFACE_POST_ADD_COMMENT = _HOST_V1 + "/comments";
    public final static String _INTERFACE_DEL_UNLIKE_COMMENT = _HOST_V1 + "/comments/{comment_id}/likes";
    //Devices
    public final static String _INTERFACE_POST_CREATE_DEVICE = _HOST_V1 + "/devices";
    public final static String _INTERFACE_PUT_UPDATE_DEVICE = _HOST_V1 + "/devices/{device_id}";
    //Files
    public final static String _INTERFACE_POST_UPLOAD_FILE = _HOST_V1 + "/files";
    //Points
    public final static String _INTERFACE_POST_CREATE_POINTS = _HOST_V1 + "/points";
    //group
    public final static String _INTERFACE_POST_CREATE_GROUP = _HOST_V1 + "/groups";
    public final static String _INTERFACE_PUT_UPDATE_GROUP = _HOST_V1 + "/groups/{group_id}";
    public final static String _INTERFACE_GET_GET_GROUP = _HOST_V1 + "/groups/{group_id}";
    public final static String _INTERFACE_POST_INVITE_INTO_GROUP = _HOST_V1 + "/groups/{group_id}/invitations";
    public final static String _INTERFACE_PUT_ACCEPT_INVITATION = _HOST_V1 + "/groups/{group_id}/invitations";
    public final static String _INTERFACE_DEL_DELETE_INVITATION = _HOST_V1 + "/groups/{group_id}/invitations";
    public final static String _INTERFACE_DEL_LEAVE_GROUP = _HOST_V1 + "/groups/{group_id}/users";
    public final static String _INTERFACE_GET_LIST_GROUP_INVITATION = _HOST_V1 + "/groups/{group_id}/invitations?";
    public final static String _INTERFACE_GET_LIST_GROUP_MEMBERS = _HOST_V1 + "/groups/{group_id}/users?";
    public final static String _INTERFACE_GET_LIST_GROUP_PHOTOS = _HOST_V1 + "/groups/{group_id}/photos?include=likes,user&";
    public final static String _INTERFACE_GET_LIST_GROUP_MESSAGES = _HOST_V1 + "/groups/{group_id}/messages?limit=10&order=desc&include=user&";

    //Likes
    public final static String _INTERFACE_DEL_UNLIKE_TRACK = _HOST_V1 + "/tracks/{track_id}/likes";
    public final static String _INTERFACE_DEL_UNLIKE_PHOTO = _HOST_V1 + "/photos/{photo_id}/likes";

    //Photos
    public final static String _INTERFACE_GET_LIST_ALL_PHOTOS = _HOST_V1 + "/photos?include=user,likes,likesmeta,comments,commentsmeta&";
    public final static String _INTERFACE_GET_GET_PHOTO = _HOST_V1 + "/photos/{photo_id}";
    public final static String _INTERFACE_POST_SAVE_PHOTO = _HOST_V1 + "/photos";
    public final static String _INTERFACE_GET_VIEW_PHOTO = _HOST_V1 + "/photos/{filename}?size=small";
    public final static String _INTERFACE_POST_PHOTO_COMMENT = _HOST_V1 + "/photos/{photo_id}/comments";
    public final static String _INTERFACE_GET_LIST_PHOTO_COMMENTS = _HOST_V1 + "/photos/{photo_id}/comments?include=likes,likesmeta,user&";
    public final static String _INTERFACE_POST_LIKE_PHOTO = _HOST_V1 + "/photos/{photo_id}/likes";
    public final static String _INTERFACE_GET_LIST_PHOTO_LIKES = _HOST_V1 + "/photos/{photo_id}/likes?";
    public final static String _INTERFACE_GET_LIST_PHOTO_TAGS = _HOST_V1 + "/photos/{photo_id}/tags?";
    //Track
    public final static String _INTERFACE_GET_LIST_ALL_TRACKS = _HOST_V1 + "/tracks?include=user,likes,likesmeta,comments,commentsmeta&";
    public final static String _INTERFACE_GET_LIST_RECOMMENDED_TRACKS = _HOST_V1 + "/tracks?recommended=1&include=user,likes,likesmeta,comments,commentsmeta&";
    public final static String _INTERFACE_GET_LIST_POPULAR_TRACKS = _HOST_V1 + "/tracks?popular=1&include=user,likes,likesmeta,comments,commentsmeta&";
    public final static String _INTERFACE_GET_LIST_FAVORITE_TRACKS = _HOST_V1 + "/tracks?liked=1&include=user,likes,likesmeta,comments,commentsmeta&";
    public final static String _INTERFACE_GET_LIST_FOLLOWED_TRACKS = _HOST_V1 + "/users/{user_id}/follows?include=tracks,tracks.likesmeta,tracks.commentsmeta&";
    public final static String _INTERFACE_POST_CREATE_TRACK = _HOST_V1 + "/tracks";
    public final static String _INTERFACE_PUT_UPDATE_TRACK = _HOST_V1 + "/tracks/{track_id}";
    public final static String _INTERFACE_GET_GET_TRACK = _HOST_V1 + "/tracks/{track_id}?include=likesmeta,commentsmeta";
    public final static String _INTERFACE_GET_GET_TRACK_COVER_PHOTO = _HOST_V1 + "/tracks/{track_id}/cover.jpg";
    public final static String _INTERFACE_POST_CREATE_TRACK_POINTS = _HOST_V1 + "/tracks/{track_id}/points";
    public final static String _INTERFACE_GET_LIST_TRACK_POINTS = _HOST_V1 + "/tracks/{track_id}/points?";
    public final static String _INTERFACE_GET_LIST_TRACK_PHOTOS = _HOST_V1 + "/tracks/{track_id}/photos?include=likesmeta,commentsmeta&";
    public final static String _INTERFACE_POST_COMMENT_TRACK = _HOST_V1 + "/tracks/{track_id}/comments";
    public final static String _INTERFACE_GET_LIST_TRACK_COMMENTS = _HOST_V1 + "/tracks/{track_id}/comments?include=likes,likesmeta,user&";
    public final static String _INTERFACE_POST_LIKE_TRACK = _HOST_V1 + "/tracks/{track_id}/likes";
    public final static String _INTERFACE_GET_LIST_TRACK_LIKES = _HOST_V1 + "/tracks/{track_id}/likes?";
    public final static String _INTERFACE_GET_LIST_TRACK_TAGS = _HOST_V1 + "/tracks/{track_id}/tags?";
    //Tag
    public final static String _INTERFACE_GET_LIST_TAGS_TRACKS = _HOST_V1 + "/tags/{tag_name}/tracks?";
    public final static String _INTERFACE_GET_LIST_TAGS_PHOTOS = _HOST_V1 + "/tags/{tag_name}/photos?";

    public static final String REAL_TIME_COMMUNICATION = "https://fusionnextinc.com:3001";
    //============================================================================

    //============================================================================
    //Activity request code definition
    //Common code should be large than 1000 and unit code should be less than 1000
    //Define this rule to keep the code will be sequential
    public final static int _LOAD_PROFILE_PAGE = 1001;
    public final static int _LOAD_MY_CONTENT = 1002;
    public final static int REQUEST_GOOGLE_ACCOUNT_PICKER = 1100;
    public final static int REQUEST_GOOGLE_AUTHORIZATION = 1101;
    public final static int LOAD_SYS_SETTING_LOCATION = 1300;
    //============================================================================

    //Google key
    public final static String GOOGLE_ACCOUNT_KEY = "GOOGLE_ACCOUNT_KEY";

    //Intent key
    public final static String INTENT_TAG_TRACK_ID = "INTENT_TAG_TRACK_ID";
    //============================================================================

    public static final String BC_REQUEST_REFRESH_GROUP_MSG_DATA = "BC_REQUEST_REFRESH_GROUP_MSG_DATA";
    public static final String BC_REFRESH_GROUP_MSG_ADAPTER = "BC_REFRESH_GROUP_MSG_ADAPTER";
    public static final String BC_REQUEST_REFRESH_GROUP_LIST_DATA = "BC_REQUEST_REFRESH_GROUP_LIST_DATA";
    public static final String BC_REFRESH_GROUP_LIST_ADAPTER = "BC_REFRESH_GROUP_LIST_ADAPTER";

    public static final String BC_REQUEST_REFRESH_TRIPS_DATA = "BC_REQUEST_REFRESH_TRIPS_DATA";
    public static final String BC_REFRESH_TRIPS_ADAPTER = "BC_REFRESH_TRIPS_ADAPTER";
    public static final String BC_REQUEST_REFRESH_PHOTOS_DATA = "BC_REQUEST_REFRESH_PHOTOS_DATA";
    public static final String BC_REFRESH_PHOTOS_ADAPTER = "BC_REFRESH_PHOTOS_ADAPTER";

    public static final String BC_REQUEST_REFRESH_ARTICLE_DATA = "BC_REQUEST_REFRESH_ARTICLE_DATA";
    public static final String BC_REFRESH_ARTICLE_ADAPTER = "BC_REFRESH_ARTICLE_ADAPTER";

    public static final String BC_REFRESH_TRIP_POINTS_ADAPTER = "BC_REFRESH_TRIP_POINTS_ADAPTER";

    public static final String BC_REFRESH_COMMENTS_ADAPTER = "BC_REFRESH_COMMENTS_ADAPTER";
    public static final String BC_REQUEST_REFRESH_COMMENTS_DATA = "BC_REQUEST_REFRESH_COMMENTS_DATA";

    public static final String BC_REFRESH_USERS_ADAPTER = "BC_REFRESH_USERS_ADAPTER";
    public static final String BC_REQUEST_REFRESH_USERS_DATA = "BC_REQUEST_REFRESH_USERS_DATA";
    //============================================================================
    //Shared Preference name
    public final static String SHARED_PREFERENCE_NAME = "com.fusionnext.doweing.sharedpreference";
    public final static String SHARED_PREFERENCE_FACEBOOK_ACCOUNT = "com..fusionnext.doweing.sharedpreference.facebook.account";

    //============================================================================
    public static void broadcastOnlyAction(Context context, String action) {
        broadcastWithExtra(context, action, null);
    }

    public static void broadcastWithExtra(Context context, String action, Bundle extra) {
        Intent intent = new Intent(action);
        if (extra != null) intent.putExtras(extra);
        context.sendBroadcast(intent);
    }

    public static boolean isFileExist(String _file) {
        if (_file == null || _file.equals("")) return false;
        File file = new File(_file);
        return file.exists();
    }

}
