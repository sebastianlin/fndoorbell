package com.fusionnextinc.fndoorbell;

import android.app.Application;
import android.content.Context;
import androidx.test.platform.app.InstrumentationRegistry;
import android.test.ApplicationTestCase;


import com.fusionnextinc.doorbell.manager.data.DataManager;
import com.fusionnextinc.doorbell.objects.FNEvent;
import com.fusionnextinc.doorbell.objects.FNThing;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.concurrent.CountDownLatch;

/**
 * To work on unit tests, switch the Test Artifact in the Build Variants view.
 */
public class DataManagerUnitTest extends ApplicationTestCase<Application> {
    protected final static String _TAG = "DataManagerUnitTest";
    Context context;
    private CountDownLatch latch;
    private DataManager dataManager = null;

    public DataManagerUnitTest() {
        super(Application.class);
    }

    public void setUp() throws Exception {
        super.setUp();
        context = getTestContext();
        dataManager = DataManager.getInstance(context);
        assertNotNull(context);

    }

    public void testSaveEvent() {
        Calendar c = Calendar.getInstance();
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String strDate = sdf.format(c.getTime());

        FNEvent fnEvent = new FNEvent();
        fnEvent.setEventId("1");
        fnEvent.setName("ring");
        fnEvent.setPayload("{test}");
        fnEvent.setThingId("1");
        fnEvent.setCreatedAt(strDate);
        fnEvent.setUpdatedAt(strDate);

        boolean result = dataManager.saveEvent(fnEvent);
        assertEquals(true, result);
    }

    public void testLoadEvent() {
        //FNEvent fnEvent = dataManager.loadEvent("1", "1");
        //assertNotNull(fnEvent);
    }

    public void testDeleteEvent() {
        boolean result = dataManager.deleteEvent("1", "1");
        assertEquals(true, result);
    }


    public void testSaveThing() {
        Calendar c = Calendar.getInstance();
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String strDate = sdf.format(c.getTime());

        FNThing fnThing = new FNThing();
        fnThing.setProductId("1");
        fnThing.setProductSlug("iqgroup_doorbell");
        fnThing.setName("Home");
        fnThing.setIdentifier("d2892d90-fc09-4e63-b111-18f5d584bb65");
        fnThing.setFirmwareVer("V0.0.1");
        fnThing.setThingId("1");
        fnThing.setCreatedAt(strDate);
        fnThing.setUpdatedAt(strDate);

        //boolean result = dataManager.saveThing(fnThing);
        //assertEquals(true, result);
    }

    public void testLoadThing() {
        //FNThing fnThing = dataManager.loadThing("1");
        //assertNotNull(fnThing);
    }

    public void testDeleteThing() {
        boolean result = dataManager.deleteThing("1");
        assertEquals(true, result);
    }

    protected void tearDown() throws Exception {
        super.tearDown();
    }

    /**
     * @return The {@link Context} of the test project.
     */
    private Context getTestContext() {
        try {
            return (Context) InstrumentationRegistry.getTargetContext();
        } catch (final Exception exception) {
            exception.printStackTrace();
            return null;
        }
    }
}