package com.fusionnextinc.doorbell.manager.apMode;

import com.fusionnextinc.doorbell.objects.FNError;
import com.fusionnextinc.doorbell.objects.FNWifiAp;

import java.util.ArrayList;

/**
 * Created by Mike Chang on 2017/6/3
 */
public abstract class ApModeHandler {

    public interface ApModeListener {
        void onListWifiAP(ApModeManager instance, boolean result, ArrayList<FNWifiAp> wifiApList, FNError error);

        void onRegisterDevice(ApModeManager instance, boolean result, String macAddress, FNError error);

        void onChangeSsid(ApModeManager instance, boolean result, String macAddress, FNError error);
    }
}
