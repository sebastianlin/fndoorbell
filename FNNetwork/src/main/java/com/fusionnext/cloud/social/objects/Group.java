package com.fusionnext.cloud.social.objects;

import android.util.Log;

import com.fusionnext.cloud.datalink.json.FNJsonArray;
import com.fusionnext.cloud.datalink.json.FNJsonObject;

import org.json.JSONException;

import java.util.ArrayList;

/**
 * Created by Mike Chang on 2016/5/4
 */
public class Group {
    protected final static String _TAG = "Group";

    public Group() {
        _groupId = "";
        _userId = "";
        _title = "";
        _description = "";
        _destinations = "";
        _privacy = "";
        _expired_at = "";
        _createdAt = "";
        _updatedAt = "";
    }

    public String _groupId = "";
    public String _userId = "";
    public String _title;
    public String _description;
    public String _destinations;
    public String _privacy;
    public String _expired_at;
    public String _createdAt;
    public String _updatedAt;

    public void clean() {
        _groupId = "";
        _userId = "";
        _title = "";
        _description = "";
        _destinations = "";
        _privacy = "";
        _expired_at = "";
        _createdAt = "";
        _updatedAt = "";
    }

    public boolean getDataFromJSONObject(FNJsonObject pointObj) {
        clean();
        if (pointObj == null) {
            Log.w(_TAG, " No JSON information");
            return false;
        }
        try {
            _userId = pointObj.getStringDefault("user_id");
            _groupId = pointObj.getStringDefault("id");
            _title = pointObj.getStringDefault("title");
            _description = pointObj.getStringDefault("description");
            _destinations = pointObj.getStringDefault("destinations");
            _privacy = pointObj.getStringDefault("privacy");
            _expired_at = pointObj.getStringDefault("expired_at");
            _createdAt = pointObj.getStringDefault("created_at");
            _updatedAt = pointObj.getStringDefault("updated_at");
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
        return true;
    }

    public static Group parseGroup(String json) {
        try {
            FNJsonObject trackObject = new FNJsonObject(json);

            Group group = new Group();
            group.getDataFromJSONObject(trackObject);
            return group;
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static ArrayList<Group> parseGroupArray(String json) {
        try {
            ArrayList<Group> groupList = new ArrayList<Group>();
            FNJsonObject userObject = new FNJsonObject(json);
            FNJsonArray groupsArray = userObject.getJSONArray("items");
            if (groupsArray == null) {
                return null;
            }
            for (int i = 0; i < groupsArray.length(); i++) {
                FNJsonObject object = groupsArray.getJSONObject(i);
                Group group = new Group();
                group.getDataFromJSONObject(object);
                if (group == null) return null;
                groupList.add(group);
            }
            return groupList;
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return null;
    }
}
