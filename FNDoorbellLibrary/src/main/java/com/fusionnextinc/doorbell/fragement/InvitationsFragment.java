package com.fusionnextinc.doorbell.fragement;

import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.TextView;

import com.fusionnext.cloud.utils.Utils;
import com.fusionnextinc.doorbell.Adapter.InvitationsListAdapter;
import com.fusionnextinc.doorbell.MyApplication;
import com.fusionnextinc.doorbell.R;
import com.fusionnextinc.doorbell.manager.data.AppDataBase;
import com.fusionnextinc.doorbell.manager.data.AppDataHandler;
import com.fusionnextinc.doorbell.manager.data.AppDataUser;
import com.fusionnextinc.doorbell.manager.data.ListMap;
import com.fusionnextinc.doorbell.manager.thingsManager.ThingsHandler;
import com.fusionnextinc.doorbell.manager.thingsManager.ThingsManager;
import com.fusionnextinc.doorbell.objects.FNError;
import com.fusionnextinc.doorbell.objects.FNEvent;
import com.fusionnextinc.doorbell.objects.FNFirmware;
import com.fusionnextinc.doorbell.objects.FNSchedule;
import com.fusionnextinc.doorbell.objects.FNThing;
import com.fusionnextinc.doorbell.objects.FNUserProfile;
import com.fusionnextinc.doorbell.utils.FNLog;
import com.fusionnextinc.doorbell.widget.FNAlert.FNAlertDialog;
import com.fusionnextinc.doorbell.widget.FNDialog;
import com.fusionnextinc.doorbell.widget.FNHeaderListView.FNHeaderListView;
import com.fusionnextinc.doorbell.widget.FNLayoutUtil;
import com.fusionnextinc.doorbell.widget.FNProgressBarCustom;

import java.util.ArrayList;

/**
 * Created by Mike Chang on 2017/6/22
 */
public class InvitationsFragment extends FNFragment implements View.OnClickListener, AbsListView.OnItemClickListener {
    private static final String TAG = InvitationsFragment.class.getSimpleName();

    private FNLayoutUtil layoutUtil;
    private FNProgressBarCustom progressBar;
    private ThingsManager thingsManager;
    private AppDataUser appDataUser;
    private InvitationsListAdapter invitationsListAdapter = null;
    private FNHeaderListView usersListView;
    private static FNThing curFNThing;

    public static void openFragment(boolean clearOtherStack, FNThing fnThing) {
        curFNThing = fnThing;
        FNFragmentManager.getInstance().openFragment(new InvitationsFragment(), clearOtherStack);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        layoutUtil = new FNLayoutUtil(getActivity(), MyApplication.LAYOUT_WIDTH, MyApplication.LAYOUT_HEIGHT, FNLayoutUtil.BASE_WIDTH);
        thingsManager = ThingsManager.getInstance(getActivity());
        appDataUser = AppDataUser.getInstance(getActivity());
        appDataUser.setAppDataUserListener(appDataUserListener);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        final ViewGroup nullParent = null;

        getActivity().setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        getActionBar().resetAll();
        getActionBar().setTitle(getString(R.string.fn_tittle_settings_invitations), Gravity.CENTER, null);
        getActionBar().setCoverMode(false);
        getActionBar().setStartImage(R.drawable.back, new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        getActionBar().show();
        View view = inflater.inflate(R.layout.fragment_invitations, nullParent);
        layoutUtil.fitAllView(view);
        usersListView = (FNHeaderListView) view.findViewById(R.id.invitations_list);
        usersListView.setOnScrollListener(onScrollListener);
        usersListView.setOnItemClickListener(this);
        progressBar = new FNProgressBarCustom(getActivity(), (ViewGroup) view.findViewById(R.id.fragment_invitations));
        initInvitationsList(curFNThing.getThingId());
        appDataUser.loadInvitationsList(curFNThing.getThingId(), true, true);
        return view;
    }

    @Override
    public void onClick(View view) {

    }

    private void startWait() {
        progressBar.show();
    }

    private void stopWait() {
        progressBar.hide();
    }

    private boolean initInvitationsList(String thingId) {
        if (usersListView == null) return false;

        if (invitationsListAdapter == null) {
            ListMap sharedUserList = new ListMap();
            ListMap invitedUserList = new ListMap();

            sharedUserList.addAll(appDataUser.getAdapterData(AppDataBase.USER_TYPE_THING_SHARED, thingId));
            invitedUserList.addAll(appDataUser.getAdapterData(AppDataBase.USER_TYPE_THING_INVITED, thingId));
            //Setup the adapter
            invitationsListAdapter = new InvitationsListAdapter(getActivity(), sharedUserList, invitedUserList, R.layout.adapter_invitations_list);
        }

        usersListView.setAdapter(invitationsListAdapter);
        usersListView.setPullRefreshEnable(true);
        usersListView.setPullLoadEnable(false);
        //Ready the item click listener
        AdapterView.OnItemClickListener itemListener = new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                TextView textUserType = (TextView) view.findViewById(R.id.textUserType);
                TextView textUserEmail = (TextView) view.findViewById(R.id.textUserAccount);

                if (textUserEmail == null || textUserType == null || i < 0) {
                    FNLog.d(TAG, "textUserEmail == null || textUserType == null || i < 0");
                } else {
                    int userType;
                    final String userEmail = textUserEmail.getText().toString();

                    try {
                        userType = Integer.valueOf(textUserType.getText().toString());
                    } catch (NumberFormatException e) {
                        userType = -1;
                    }

                    switch (userType) {
                        case InvitationsListAdapter.TYPE_THING_SHARED_USER_LIST:
                        case InvitationsListAdapter.TYPE_THING_INVITED_USER_LIST:
                            new FNAlertDialog(getActivity(), FNAlertDialog.WARNING_TYPE)
                                    .setTitleText(getActivity().getString(R.string.fn_dialog_are_you_sure))
                                    .setContentText(getActivity().getString(R.string.fn_dialog_delete_invitation))
                                    .setCancelText(getActivity().getString(R.string.fn_dialog_cancel))
                                    .setConfirmText(getActivity().getString(R.string.fn_btn_delete))
                                    .showCancelButton(true)
                                    .setCancelClickListener(new FNAlertDialog.OnSweetClickListener() {
                                        @Override
                                        public void onClick(FNAlertDialog sDialog) {
                                            sDialog.dismiss();
                                        }
                                    })
                                    .setConfirmClickListener(new FNAlertDialog.OnSweetClickListener() {
                                        @Override
                                        public void onClick(FNAlertDialog sDialog) {
                                            if (Utils.isNetworkConnected(getActivity())) {
                                                thingsManager.setThingsListener(thingsListener);
                                                thingsManager.shareReject(curFNThing.getThingId(), userEmail);
                                                startWait();
                                                sDialog.dismiss();
                                            } else {
                                                FNDialog.showNoNetworkDialog(getActivity());
                                            }
                                        }
                                    })
                                    .show();
                            break;
                        default:
                            break;
                    }
                }
            }
        };
        usersListView.setOnItemClickListener(itemListener);
        usersListView.setPullListViewListener(new FNHeaderListView.IPullListViewListener() {

            @Override
            public void onRefresh() {
                appDataUser.refreshUsersListData(curFNThing.getThingId());
            }

            @Override
            public void onLoadMore() {
                FNLog.d(TAG, "onLoadMoreData: Click share footer bar");
                //appDataThing.getNextThingsPage(type);
            }
        });
        return true;
    }

    @Override
    public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {

    }

    private AbsListView.OnScrollListener onScrollListener = new AbsListView.OnScrollListener() {
        @Override
        public void onScrollStateChanged(AbsListView view, int scrollState) {

            switch (scrollState) {
                case SCROLL_STATE_IDLE:
                    break;
                case SCROLL_STATE_FLING:
                    break;
            }
        }

        @Override
        public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {

        }
    };

    final ThingsHandler.ThingsListener thingsListener = new ThingsHandler.ThingsListener() {

        @Override
        public void onListUserOwnedThings(ThingsManager instance, boolean result, String userId, ArrayList<FNThing> thingArrayList, FNError error) {

        }

        @Override
        public void onListUserInvitedThings(ThingsManager instance, boolean result, String userId, ArrayList<FNThing> thingArrayList, FNError error) {

        }

        @Override
        public void onListUserSharedThings(ThingsManager instance, boolean result, String userId, ArrayList<FNThing> thingArrayList, FNError error) {

        }

        @Override
        public void onListUserAllThings(ThingsManager instance, boolean result, String userId, ArrayList<FNThing> ownedThingArrayList, ArrayList<FNThing> sharedThingArrayList, ArrayList<FNThing> invitedThingArrayList, FNError error) {

        }

        @Override
        public void onListThingEvents(ThingsManager instance, boolean result, String thingId, String eventName, ArrayList<FNEvent> eventList, FNError error) {

        }

        @Override
        public void onListThingEventsByTime(ThingsManager instance, boolean result, String thingId, String eventName, String startTime, String endTime, ArrayList<FNEvent> eventList, FNError error) {

        }

        @Override
        public void onCurBatteryLevel(ThingsManager instance, boolean result, String thingId, String batteryLevel, FNError error) {

        }

        @Override
        public void onCurFirmwareVersion(ThingsManager instance, boolean result, String thingId, String fwVersion, FNError error) {

        }

        @Override
        public void onCurSwitchedStatus(ThingsManager instance, boolean result, String thingId, boolean isSwitch, FNError error) {

        }

        @Override
        public void onNewFirmwareVersion(ThingsManager instance, boolean result, String thingId, FNFirmware fnFirmware) {

        }

        @Override
        public void onStartFwUpdate(ThingsManager instance, boolean result, String thingId, String fwId) {

        }

        @Override
        public void onCurRingTone(ThingsManager instance, boolean result, String thingId, String ringtoneId, FNError error) {

        }

        @Override
        public void onGetScheduleList(ThingsManager instance, boolean result, String thingId, ArrayList<FNSchedule> scheduleList, FNError error) {

        }

        @Override
        public void onListInvitedUsers(ThingsManager instance, boolean result, String thingId, ArrayList<FNUserProfile> userList, FNError error) {

        }

        @Override
        public void onListSharedUsers(ThingsManager instance, boolean result, String thingId, ArrayList<FNUserProfile> userList, FNError error) {

        }

        @Override
        public void onCreateClaimCode(ThingsManager instance, boolean result, String claimCode, FNError error) {

        }

        @Override
        public void onCreateThing(ThingsManager instance, boolean result, FNThing fnThing, FNError error) {

        }

        @Override
        public void onUpdateThing(ThingsManager instance, boolean result, FNThing fnThing, FNError error) {

        }

        @Override
        public void onDeleteThing(ThingsManager instance, boolean result, FNError error) {

        }

        @Override
        public void onGetThing(ThingsManager instance, boolean result, FNThing fnThing, FNError error) {

        }

        @Override
        public void onChangeRingtone(ThingsManager instance, boolean result, String thingId, int ringtoneId) {

        }

        @Override
        public void onSetTimer(ThingsManager instance, boolean result, String thingId, String value) {

        }

        @Override
        public void onSetSchedule(ThingsManager instance, boolean result, String thingId, String value) {

        }

        @Override
        public void onShareInvited(ThingsManager instance, boolean result, String thingId, FNError error) {

        }

        @Override
        public void onShareAccepted(ThingsManager instance, boolean result, String thingId, FNError error) {

        }

        @Override
        public void onShareRejected(ThingsManager instance, boolean result, String thingId, FNError error) {
            if (isAdded()) {
                stopWait();
                if (result) {
                    FNDialog.showSuccessDialog(getActivity(), getString(R.string.fn_dialog_delete_invitation), getString(R.string.fn_msg_delete_invitation_successfully).replace("${Thing}", getActivity().getString(R.string.fn_title_thing_name)));
                    appDataUser.refreshUsersListData(curFNThing.getThingId());
                } else {
                    FNDialog.showErrorDialog(getActivity(), getString(R.string.fn_msg_delete_invitation_failed).replace("${Thing}", getActivity().getString(R.string.fn_title_thing_name)), error);
                }
            }
        }

        @Override
        public void onSwitchOnOff(ThingsManager instance, boolean result, String thingId, boolean isTurnOn) {

        }

        @Override
        public void onCloseCallSlot(ThingsManager instance, boolean result, FNError error) {

        }

        @Override
        public void onTakeCallSlot(ThingsManager instance, boolean result, String thingId, String callSlot, FNError error) {

        }
    };

    final AppDataHandler.AppDataUserListener appDataUserListener = new AppDataHandler.AppDataUserListener() {
        @Override
        public void onRefreshListAdapter(int type, String thingId) {
            if (invitationsListAdapter != null) {
                invitationsListAdapter.updateAdapterData(type, thingId);
            }
            if (usersListView != null) {
                usersListView.setPullLoadEnable(false);
                usersListView.stopLoadMore();
                usersListView.stopRefresh();
            }
        }
    };
}
