package com.fusionnextinc.doorbell.manager.smartConnection;

import com.fusionnext.cloud.iot.objects.ErrorObj;
import com.fusionnextinc.doorbell.objects.FNThing;

/**
 * Created by Mike Chang on 2017/6/3
 */
public abstract class SmartConnectionHandler {

    public interface SmartConnectionListener {
        void onThingFound(SmartConnectionManager instance, FNThing fnThing, int rssi);

        void onThingRegistered(SmartConnectionManager instance, FNThing fnThing, ErrorObj error, int rssi);

        void onThingNotFound(SmartConnectionManager instance);

        void onThingOtaStart(SmartConnectionManager instance, FNThing fnThing);

        void onThingSsidChanged(SmartConnectionManager instance, FNThing fnThing, int rssi);
    }
}
