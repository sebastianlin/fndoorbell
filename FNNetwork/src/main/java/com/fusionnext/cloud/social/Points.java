package com.fusionnext.cloud.social;

import android.content.Context;

import com.fusionnext.cloud.datalink.http.HttpPost;
import com.fusionnext.cloud.social.objects.Point;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by Mike Chang on 2017/3/23
 */
public class Points {
    protected final static String _TAG = "Points";
    private Context context;

    public Points(Context context) {
        this.context = context;
    }


    public void createPoints(final String trackId, ArrayList<Point> pointList, final SocialAPIHandler.OnPointsListener onPointsListener) {
        HttpPost.PostParam[] params = new HttpPost.PostParam[1];
        params[0] = new HttpPost.PostParam();
        params[0].accessToken = Public.getAuthorization(context).getUserAccessToken();
        params[0].url = HttpData.generateHttpPostUrl(HttpData._POST_TYPE_POINTS_CREATE_POINTS);
        params[0].type = HttpData._POST_TYPE_POINTS_CREATE_POINTS;
        params[0].param = new HashMap<Object, Object>();
        params[0].param.put("track_id", trackId);
        if (pointList != null && pointList.size() > 0) {
            JSONObject jsonObject = new JSONObject();
            JSONArray terminalArray = new JSONArray();

            for (int i = 0; i < pointList.size(); i++) {
                JSONArray pointArray = null;
                Point point = pointList.get(i);
                try {
                    jsonObject.put("lat", point._lat);
                    jsonObject.put("lng", point._lng);
                    jsonObject.put("tracked_at", point._trackedAt);
                    pointArray = terminalArray.put(jsonObject);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                params[0].param.put("points", pointArray.toString());
            }
        }

        HttpPost post = new HttpPost();
        post.setContext(context);
        post.setOnParseJsonListener(new HttpPost.OnParseJsonListener() {
            @Override
            public void onParse(boolean returnResult, int postType, String id, String returnStr) {
                onPointsListener.onResult(returnResult, trackId, postType, returnStr);
            }
        });
        post.execute(params);
    }

}
