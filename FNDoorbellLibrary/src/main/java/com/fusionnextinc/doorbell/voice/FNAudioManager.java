package com.fusionnextinc.doorbell.voice;

import android.media.MediaPlayer;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;

/**
 * Created by koukiyoshiyuu on 2018/3/7.
 */

public class FNAudioManager {
    private static final String TAG = "FNVoiceManager";

    private static FNAudioManager sFNAudioManager;
    private FNAudioRecorder mFNAudioRecoder;
    private FNAudioPlayer mFNAudioPlayer;
    private OnFNAudioManagerRecordListener mFNVoiceManagerRecordListener;

    //Update Playing Time
    private static final int HANDLER_UPDATE_PLAYING_TIME = 0;
    private int mPlayingTime = -1;
    //Update Recording Time
    private static final int HANDLER_UPDATE_RECORD_TIME = 1;
    private static final int RECORDING_LENGTH_LIMITATION = 9999;
    private long mRecordTime = -1;


    public static FNAudioManager getInstance() {
        synchronized (FNAudioManager.class) {
            if (sFNAudioManager == null) {
                sFNAudioManager = new FNAudioManager();
            }
        }
        return sFNAudioManager;
    }

    private FNAudioManager() {
        mFNAudioRecoder = new FNAudioRecorder();
        mFNAudioPlayer = new FNAudioPlayer();
    }

    /**
     * 聲音錄製 - 開始錄音
     *
     * @param listener 錄音狀態Listener
     */
    public void startRecord(OnFNAudioManagerRecordListener listener) {
        mFNVoiceManagerRecordListener = listener;
        mFNAudioRecoder.startRecord(mFNVoiceRecorderListener);
    }

    /**
     * 聲音錄製 - 停止錄音
     */
    public void stopRecord() {
        if (mFNAudioRecoder.isRecording()) {
            mFNAudioRecoder.stopRecord();
        }
    }

    /**
     * 聲音錄製 - 取得錄音聲壓值
     *
     * @return 錄音聲壓值
     */
    public int getMaxAmplitude() {
        return mFNAudioRecoder.getMaxAmplitude();
    }

    /**
     * 聲音錄製 - 取得是否正在錄音
     *
     * @return 是否正在錄音
     */
    public boolean isRecording() {
        return mFNAudioRecoder.isRecording();
    }

    /**
     * 聲音播放器 - 播放
     *
     * @param data 聲音檔案路徑
     */
    public void play(byte[] data) {
        mFNAudioPlayer.play(data);
    }

    /**
     * 聲音播放器 - 暫停
     */
    public void pause() {
        mFNAudioPlayer.pause();
    }

    /**
     * 聲音播放器 - 停止
     */
    public void stop() {
        mFNAudioPlayer.stop();
    }

    /**
     * 聲音播放器 - 銷毀
     */
    public void destroy() {
        mFNAudioPlayer.destroy();
    }

    private OnFNAudioRecorderListener mFNVoiceRecorderListener = new OnFNAudioRecorderListener() {
        @Override
        public void onStartRecord(FNAudioRecorder audioRecorder) {
            if (mFNVoiceManagerRecordListener != null) {
                mFNVoiceManagerRecordListener.onStartRecord(FNAudioManager.this);
            }
            startUpdateRecordTime(0);
        }

        @Override
        public void onRecordData(byte[] data) {
            if (mFNVoiceManagerRecordListener != null) {
                mFNVoiceManagerRecordListener.onRecordData(FNAudioManager.this, data);
            }
        }

        @Override
        public void onStopRecord(FNAudioRecorder audioRecorder) {
            if (mFNVoiceManagerRecordListener != null) {
                if (mRecordTime >= RECORDING_LENGTH_LIMITATION + 1) {
                    mFNVoiceManagerRecordListener.onRecordCompleted(FNAudioManager.this);
                } else {
                    mFNVoiceManagerRecordListener.onStopRecord(FNAudioManager.this);
                }
            }
            stopUpdateRecordTime();
        }

        @Override
        public void onRecordError(FNAudioRecorder audioRecorder) {
            if (mFNVoiceManagerRecordListener != null) {
                mFNVoiceManagerRecordListener.onRecordError(FNAudioManager.this);
            }
            stopUpdateRecordTime();
        }
    };

    private Handler handler = new Handler(Looper.getMainLooper()) {
        @Override
        public void handleMessage(Message msg) {
            switch (msg.what) {
                case HANDLER_UPDATE_RECORD_TIME:
                    if (mRecordTime > -1) {
                        if (mFNVoiceManagerRecordListener != null) {
                            mFNVoiceManagerRecordListener.onRecordingTimeUpdated(mRecordTime);
                        }
                        sendEmptyMessageDelayed(HANDLER_UPDATE_RECORD_TIME, 1000);
                        mRecordTime++;
                        if (mRecordTime >= RECORDING_LENGTH_LIMITATION + 1) {
                            stopRecord();
                        }
                    }
                    break;
                case HANDLER_UPDATE_PLAYING_TIME:
                    if (mPlayingTime > -1) {
                        sendEmptyMessageDelayed(HANDLER_UPDATE_PLAYING_TIME, 1000);
                        mPlayingTime++;
                    }
                    break;
            }
        }
    };

    private void startUpdateRecordTime(int recordTime) {
        handler.removeMessages(HANDLER_UPDATE_RECORD_TIME);
        mRecordTime = recordTime;
        handler.sendEmptyMessage(HANDLER_UPDATE_RECORD_TIME);
    }

    private void stopUpdateRecordTime() {
        handler.removeMessages(HANDLER_UPDATE_RECORD_TIME);
        mRecordTime = -1;
        handler.sendEmptyMessage(HANDLER_UPDATE_RECORD_TIME);
    }

    private void startUpdatePlayingTime(int playingTime) {
        handler.removeMessages(HANDLER_UPDATE_PLAYING_TIME);
        mPlayingTime = playingTime;
        handler.sendEmptyMessage(HANDLER_UPDATE_PLAYING_TIME);
    }

    private void stopUpdatePlayingTime() {
        handler.removeMessages(HANDLER_UPDATE_PLAYING_TIME);
        mPlayingTime = -1;
        handler.sendEmptyMessage(HANDLER_UPDATE_PLAYING_TIME);
    }
}
