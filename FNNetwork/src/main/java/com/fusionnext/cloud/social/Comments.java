package com.fusionnext.cloud.social;

import android.content.Context;

import com.fusionnext.cloud.datalink.http.HttpDelete;
import com.fusionnext.cloud.datalink.http.HttpGet;
import com.fusionnext.cloud.datalink.http.HttpParam;
import com.fusionnext.cloud.datalink.http.HttpPost;
import com.fusionnext.cloud.datalink.http.HttpPut;
import com.fusionnext.cloud.social.objects.Comment;
import com.fusionnext.cloud.social.objects.Likes;

import java.util.HashMap;

/**
 * Created by Mike Chang on 2017/3/23
 */
public class Comments {
    protected final static String _TAG = "Comments";
    public final static int ADD_COMMENT_TYPE_TRACK = 0;
    public final static int ADD_COMMENT_TYPE_PHOTO = 1;
    public final static int ADD_COMMENT_TYPE_ARTICLE = 2;
    private Context context;

    public Comments(Context context) {
        this.context = context;
    }

    public void getComment(final String commentId, int order, String pageSize, String pageNum, final SocialAPIHandler.OnCommentsListener onCommentsListener) {
        HttpParam.HttpSubParam param = new HttpParam.HttpSubParam();

        param.accessToken = Public.getAuthorization(context).getUserAccessToken();
        param.url = HttpData.generateHttpGetUrl(HttpData._GET_TYPE_COMMENTS_GET_COMMENT, commentId, order, pageSize, pageNum);
        param.cacheName = "";
        HttpParam[] params = new HttpParam[1];
        params[0] = new HttpParam();
        params[0].params.clear();
        params[0].params.add(param);

        HttpGet httpGet = new HttpGet();
        httpGet.setContext(context);
        httpGet.setTag(HttpGet._HTTP_T_GET_NORMAL_TYPE);
        httpGet.setOnParseJsonListener(new HttpGet.OnParseJsonListener() {
            @Override
            public void onParse(boolean result, String returnStr) {
                onCommentsListener.onGetComment(result, commentId, Comment.parseComment(returnStr));
            }
        });
        httpGet.execute(params);
    }

    public void addComment(int type, String id, String content, final SocialAPIHandler.OnCommentsListener onCommentsListener) {
        HttpPost.PostParam[] params = new HttpPost.PostParam[1];
        params[0] = new HttpPost.PostParam();
        params[0].accessToken = Public.getAuthorization(context).getUserAccessToken();
        params[0].url = HttpData.generateHttpPostUrl(HttpData._POST_TYPE_COMMENTS_ADD_COMMENT);
        params[0].type = HttpData._POST_TYPE_COMMENTS_ADD_COMMENT;
        params[0].param = new HashMap<Object, Object>();

        params[0].param.put("content", content);
        switch (type) {
            case ADD_COMMENT_TYPE_TRACK:
                params[0].param.put("track_id", id);
                break;
            case ADD_COMMENT_TYPE_PHOTO:
                params[0].param.put("photo_id", id);
                break;
            case ADD_COMMENT_TYPE_ARTICLE:
                params[0].param.put("article_id", id);
                break;
            default:
                break;
        }

        HttpPost post = new HttpPost();
        post.setContext(context);
        post.setOnParseJsonListener(new HttpPost.OnParseJsonListener() {
            @Override
            public void onParse(boolean returnResult, int postType, String id, String returnStr) {
                onCommentsListener.onResult(returnResult, id, postType, returnStr);
            }
        });
        post.execute(params);
    }

    public void updateComment(final String commentId, String content, final SocialAPIHandler.OnCommentsListener onCommentsListener) {
        if (content == null) {
            return;
        }

        HttpPut.PutParam[] params = new HttpPut.PutParam[1];
        params[0] = new HttpPut.PutParam();
        params[0].accessToken = Public.getAuthorization(context).getUserAccessToken();
        params[0].url = HttpData.generateHttpPutUrl(HttpData._PUT_TYPE_COMMENTS_UPDATE_COMMENT, commentId);
        params[0].type = HttpData._PUT_TYPE_COMMENTS_UPDATE_COMMENT;
        params[0].param = new HashMap<Object, Object>();
        params[0].param.put("content", content);

        HttpPut put = new HttpPut();
        put.setHttpRequestType("PUT");
        put.setOnParseJsonListener(new HttpPut.OnParseJsonListener() {
            @Override
            public void onParse(boolean returnResult, int putType, String returnStr) {
                onCommentsListener.onResult(returnResult, commentId, putType, returnStr);
            }
        });
        put.setContext(context);
        put.execute(params);
    }

    public void like(final String commentId, final SocialAPIHandler.OnCommentsListener onCommentsListener) {
        HttpPost.PostParam[] params = new HttpPost.PostParam[1];
        params[0] = new HttpPost.PostParam();
        params[0].accessToken = Public.getAuthorization(context).getUserAccessToken();
        params[0].url = HttpData.generateHttpPostUrl(HttpData._POST_TYPE_COMMENTS_LIKE, commentId);
        params[0].type = HttpData._POST_TYPE_COMMENTS_LIKE;

        HttpPost post = new HttpPost();
        post.setContext(context);
        post.setOnParseJsonListener(new HttpPost.OnParseJsonListener() {
            @Override
            public void onParse(boolean returnResult, int postType, String id, String returnStr) {
                onCommentsListener.onResult(returnResult, commentId, postType, returnStr);
            }
        });
        post.execute(params);
    }

    public void unlike(final String commentId, final SocialAPIHandler.OnCommentsListener onCommentsListener) {
        HttpDelete.DelParam[] params = new HttpDelete.DelParam[1];
        params[0] = new HttpDelete.DelParam();
        params[0].accessToken = Public.getAuthorization(context).getUserAccessToken();
        params[0].url = HttpData.generateHttpDeleteUrl(HttpData._DEL_TYPE_COMMENTS_UNLIKE, commentId);
        params[0].type = HttpData._DEL_TYPE_COMMENTS_UNLIKE;
        params[0].param = new HashMap<Object, Object>();

        HttpDelete del = new HttpDelete();
        del.setContext(context);
        del.setOnParseJsonListener(new HttpDelete.OnParseJsonListener() {
            @Override
            public void onParse(boolean returnResult, int delType, String returnStr) {
                if (returnStr != null) {
                    onCommentsListener.onResult(returnResult, commentId, delType, returnStr);
                }
            }
        });
        del.execute(params);
    }

    public void listLikes(final String commentId, int order, String pageSize, String pageNum, final SocialAPIHandler.OnCommentsListener onCommentsListener) {
        HttpParam.HttpSubParam param = new HttpParam.HttpSubParam();

        param.accessToken = Public.getAuthorization(context).getUserAccessToken();
        param.url = HttpData.generateHttpGetUrl(HttpData._GET_TYPE_COMMENTS_LIST_LIKES, commentId, order, pageSize, pageNum);
        param.cacheName = "";
        HttpParam[] params = new HttpParam[1];
        params[0] = new HttpParam();
        params[0].params.clear();
        params[0].params.add(param);

        HttpGet httpGet = new HttpGet();
        httpGet.setContext(context);
        httpGet.setTag(HttpGet._HTTP_T_GET_NORMAL_TYPE);
        httpGet.setOnParseJsonListener(new HttpGet.OnParseJsonListener() {
            @Override
            public void onParse(boolean result, String returnStr) {
                onCommentsListener.onListLikes(result, commentId, Likes.parseLikeArray(returnStr));
            }
        });
        httpGet.execute(params);
    }
}
