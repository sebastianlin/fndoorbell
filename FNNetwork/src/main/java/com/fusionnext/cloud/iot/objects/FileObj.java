package com.fusionnext.cloud.iot.objects;

import android.util.Log;

import com.fusionnext.cloud.datalink.json.FNJsonObject;

import org.json.JSONException;

/**
 * Created by Mike Chang on 2017/3/22
 */
public class FileObj {

    protected final static String TAG = "FileObj";

    public final static String LIKES_STRING_TYPE_FILENAME = "filename";
    public final static String LIKES_STRING_TYPE_MIME = "mime";

    public FileObj() {
        _fileName = "";
        _mime = "";
    }

    public String _fileName;
    public String _mime;

    public void clean() {
        _fileName = "";
        _mime = "";
    }

    public boolean getDataFromJSONObject(FNJsonObject likeObj) {
        clean();
        if (likeObj == null) {
            Log.w(TAG, " No JSON information");
            return false;
        }
        try {
            _fileName = likeObj.getStringDefault(LIKES_STRING_TYPE_FILENAME);
            _mime = likeObj.getStringDefault(LIKES_STRING_TYPE_MIME);
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
        return true;
    }

    public static FileObj parseFile(String json) {
        try {
            FNJsonObject fileObject = new FNJsonObject(json);

            FileObj fileObj = new FileObj();
            fileObj.getDataFromJSONObject(fileObject);
            return fileObj;
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return null;
    }
}
