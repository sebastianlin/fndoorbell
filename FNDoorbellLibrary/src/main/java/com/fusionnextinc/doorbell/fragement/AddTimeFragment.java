package com.fusionnextinc.doorbell.fragement;

import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.bigkoo.pickerview.adapter.ArrayWheelAdapter;
import com.contrarywind.listener.OnItemSelectedListener;
import com.contrarywind.view.WheelView;
import com.fusionnextinc.doorbell.MyApplication;
import com.fusionnextinc.doorbell.R;
import com.fusionnextinc.doorbell.manager.data.AppDataBase;
import com.fusionnextinc.doorbell.manager.data.AppDataSchedule;
import com.fusionnextinc.doorbell.manager.thingsManager.ThingsHandler;
import com.fusionnextinc.doorbell.manager.thingsManager.ThingsManager;
import com.fusionnextinc.doorbell.objects.FNError;
import com.fusionnextinc.doorbell.objects.FNEvent;
import com.fusionnextinc.doorbell.objects.FNFirmware;
import com.fusionnextinc.doorbell.objects.FNSchedule;
import com.fusionnextinc.doorbell.objects.FNThing;
import com.fusionnextinc.doorbell.objects.FNUserProfile;
import com.fusionnextinc.doorbell.widget.FNAlert.FNAlertDialog;
import com.fusionnextinc.doorbell.widget.FNDialog;
import com.fusionnextinc.doorbell.widget.FNLayoutUtil;
import com.fusionnextinc.doorbell.widget.FNProgressBarCustom;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Mike Chang on 2018/8/8
 */
public class AddTimeFragment extends FNFragment implements View.OnClickListener {
    private static final int SCHEDULE_LIST_MAX_NUMBER = 10;

    private FNLayoutUtil mLayoutUtil;
    private WheelView mWheelViewPickerHour, mWheelViewPickerMin;
    private TextView mTextSun, mTextMon, mTextTue, mTextWed, mTextThu, mTextFri, mTextSat, mTextDeviceOn, mTextDeviceOff;
    private int mHour = 1;
    private int mMin = 0;
    private FNThing mCurFNThing;
    private FNSchedule mFNSchedule;
    private ThingsManager mThingsManager;
    private FNProgressBarCustom mProgressBar;
    private static boolean isSavingSchedule = false;

    public static void openFragment(boolean clearOtherStack, FNThing curFNThing, FNSchedule fnSchedule) {
        AddTimeFragment thingsFragment = new AddTimeFragment();
        thingsFragment.mCurFNThing = curFNThing;
        thingsFragment.mFNSchedule = fnSchedule;
        FNFragmentManager.getInstance().openFragment(thingsFragment, clearOtherStack);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mLayoutUtil = new FNLayoutUtil(getActivity(), MyApplication.LAYOUT_WIDTH, MyApplication.LAYOUT_HEIGHT, FNLayoutUtil.BASE_WIDTH);
        mThingsManager = ThingsManager.getInstance(getActivity());
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        final ViewGroup nullParent = null;

        getActivity().setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        getActionBar().resetAll();
        getActionBar().setTitle(getString(R.string.fn_title_add_schedule), Gravity.CENTER, null);
        getActionBar().setStartImage(R.drawable.back, new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        getActionBar().setEndText(getString(R.string.fn_btn_room_save), getResources().getColor(R.color.button_text_color), mSaveOnClickListener);
        getActionBar().setCoverMode(false);
        getActionBar().show();
        View view = inflater.inflate(R.layout.fragment_add_time, nullParent);
        mLayoutUtil.fitAllView(view);
        mWheelViewPickerHour = view.findViewById(R.id.wheelView_picker_hour);
        mWheelViewPickerMin = view.findViewById(R.id.wheelView_picker_min);
        mTextSun = view.findViewById(R.id.text_sun);
        mTextMon = view.findViewById(R.id.text_mon);
        mTextTue = view.findViewById(R.id.text_tue);
        mTextWed = view.findViewById(R.id.text_wed);
        mTextThu = view.findViewById(R.id.text_thu);
        mTextFri = view.findViewById(R.id.text_fri);
        mTextSat = view.findViewById(R.id.text_sat);
        mTextDeviceOff = view.findViewById(R.id.text_device_off);
        mTextDeviceOn = view.findViewById(R.id.text_device_on);
        mProgressBar = new FNProgressBarCustom(getActivity(), (ViewGroup) view.findViewById(R.id.fragment_add_time));

        mTextSun.setTag(R.drawable.week_circle_shape);
        mTextMon.setTag(R.drawable.week_circle_shape);
        mTextTue.setTag(R.drawable.week_circle_shape);
        mTextWed.setTag(R.drawable.week_circle_shape);
        mTextThu.setTag(R.drawable.week_circle_shape);
        mTextFri.setTag(R.drawable.week_circle_shape);
        mTextSat.setTag(R.drawable.week_circle_shape);
        mTextDeviceOn.setTag(R.drawable.device_onoff_btn_shape_pressed);
        mTextDeviceOff.setTag(R.drawable.device_onoff_btn_shape);

        if (mFNSchedule != null) {
            updateDeviceOnOff(mFNSchedule.isDeviceOn());
            for (int i = 0; i < mFNSchedule.getWeeks().size(); i++) {
                updateSelectedWeeks(mFNSchedule.getWeeks().get(i), true);
            }
            initTimePicker(mFNSchedule.getHour() - 1, mFNSchedule.getMin());
        } else {
            initTimePicker(0, 0);
        }
        mTextSun.setOnClickListener(this);
        mTextMon.setOnClickListener(this);
        mTextTue.setOnClickListener(this);
        mTextWed.setOnClickListener(this);
        mTextThu.setOnClickListener(this);
        mTextFri.setOnClickListener(this);
        mTextSat.setOnClickListener(this);
        mTextDeviceOn.setOnClickListener(this);
        mTextDeviceOff.setOnClickListener(this);
        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        mThingsManager.setThingsListener(mThingsListener);
    }

    @Override
    public void onBackPressed() {
        if (mProgressBar != null && !mProgressBar.isShown()) {
            super.onBackPressed(); // Comment this super call to avoid calling finish() or fragmentmanager's backstack pop operation.
        }
    }

    @Override
    public void onClick(View view) {
        if (view.getId() == R.id.text_sun) {
            if (mTextSun.getTag().equals(R.drawable.week_circle_shape)) {
                updateSelectedWeeks(7, true);
            } else {
                updateSelectedWeeks(7, false);
            }
        } else if (view.getId() == R.id.text_mon) {
            if (mTextMon.getTag().equals(R.drawable.week_circle_shape)) {
                updateSelectedWeeks(1, true);
            } else {
                updateSelectedWeeks(1, false);
            }
        } else if (view.getId() == R.id.text_tue) {
            if (mTextTue.getTag().equals(R.drawable.week_circle_shape)) {
                updateSelectedWeeks(2, true);
            } else {
                updateSelectedWeeks(2, false);
            }
        } else if (view.getId() == R.id.text_wed) {
            if (mTextWed.getTag().equals(R.drawable.week_circle_shape)) {
                updateSelectedWeeks(3, true);
            } else {
                updateSelectedWeeks(3, false);
            }
        } else if (view.getId() == R.id.text_thu) {
            if (mTextThu.getTag().equals(R.drawable.week_circle_shape)) {
                updateSelectedWeeks(4, true);
            } else {
                updateSelectedWeeks(4, false);
            }
        } else if (view.getId() == R.id.text_fri) {
            if (mTextFri.getTag().equals(R.drawable.week_circle_shape)) {
                updateSelectedWeeks(5, true);
            } else {
                updateSelectedWeeks(5, false);
            }
        } else if (view.getId() == R.id.text_sat) {
            if (mTextSat.getTag().equals(R.drawable.week_circle_shape)) {
                updateSelectedWeeks(6, true);
            } else {
                updateSelectedWeeks(6, false);
            }
        } else if (view.getId() == R.id.text_device_off) {
            updateDeviceOnOff(false);
        } else if (view.getId() == R.id.text_device_on) {
            updateDeviceOnOff(true);
        }
    }

    private void initTimePicker(int hour, int min) {
        final List<String> mHourOptionsItems = new ArrayList<>();
        mHour = hour + 1;
        mMin = min;
        for (int i = 1; i <= 24; i++) {
            mHourOptionsItems.add(String.format("%02d", i));
        }
        mWheelViewPickerHour.setCyclic(false);
        mWheelViewPickerHour.setDividerColor(getActivity().getResources().getColor(R.color.white));
        mWheelViewPickerHour.setCurrentItem(hour);
        mWheelViewPickerHour.setAdapter(new ArrayWheelAdapter(mHourOptionsItems));
        mWheelViewPickerHour.setOnItemSelectedListener(new OnItemSelectedListener() {
            @Override
            public void onItemSelected(int index) {
                mHour = Integer.valueOf(mHourOptionsItems.get(index));
            }
        });

        final List<String> mMinOptionsItems = new ArrayList<>();
        for (int i = 0; i < 60; i++) {
            mMinOptionsItems.add(String.format("%02d", i));
        }
        mWheelViewPickerMin.setCyclic(false);
        mWheelViewPickerMin.setDividerColor(getActivity().getResources().getColor(R.color.white));
        mWheelViewPickerMin.setCurrentItem(min);
        mWheelViewPickerMin.setAdapter(new ArrayWheelAdapter(mMinOptionsItems));
        mWheelViewPickerMin.setOnItemSelectedListener(new OnItemSelectedListener() {
            @Override
            public void onItemSelected(int index) {
                mMin = Integer.valueOf(mMinOptionsItems.get(index));
            }
        });
    }

    private FNSchedule createSchedule() {
        boolean isDeviceOn = mTextDeviceOn.getTag().equals(R.drawable.device_onoff_btn_shape_pressed) ? true : false;
        ArrayList<Integer> weekList = new ArrayList<>();

        if (mTextSun.getTag().equals(R.drawable.week_circle_shape_selected)) {
            weekList.add(7);
        }
        if (mTextMon.getTag().equals(R.drawable.week_circle_shape_selected)) {
            weekList.add(1);
        }
        if (mTextTue.getTag().equals(R.drawable.week_circle_shape_selected)) {
            weekList.add(2);
        }
        if (mTextWed.getTag().equals(R.drawable.week_circle_shape_selected)) {
            weekList.add(3);
        }
        if (mTextThu.getTag().equals(R.drawable.week_circle_shape_selected)) {
            weekList.add(4);
        }
        if (mTextFri.getTag().equals(R.drawable.week_circle_shape_selected)) {
            weekList.add(5);
        }
        if (mTextSat.getTag().equals(R.drawable.week_circle_shape_selected)) {
            weekList.add(6);
        }
        int scheduleId = mFNSchedule != null ? mFNSchedule.getScheduleId() : AppDataSchedule.getInstance(getActivity())
                .genScheduleId(mCurFNThing.getThingId());
        FNSchedule fnSchedule = new FNSchedule(scheduleId, isDeviceOn, true, mHour, mMin, 0, weekList);
        return fnSchedule;
    }

    private void updateSelectedWeeks(int week, boolean selected) {
        switch (week) {
            case 1:
                if (selected) {
                    mTextMon.setBackground(getActivity().getResources().getDrawable(R.drawable.week_circle_shape_selected));
                    mTextMon.setTextColor(getActivity().getResources().getColor(R.color.white));
                    mTextMon.setTag(R.drawable.week_circle_shape_selected);
                } else {
                    mTextMon.setBackground(getActivity().getResources().getDrawable(R.drawable.week_circle_shape));
                    mTextMon.setTextColor(getActivity().getResources().getColor(R.color.tab_bar_bg));
                    mTextMon.setTag(R.drawable.week_circle_shape);
                }
                break;
            case 2:
                if (selected) {
                    mTextTue.setBackground(getActivity().getResources().getDrawable(R.drawable.week_circle_shape_selected));
                    mTextTue.setTextColor(getActivity().getResources().getColor(R.color.white));
                    mTextTue.setTag(R.drawable.week_circle_shape_selected);
                } else {
                    mTextTue.setBackground(getActivity().getResources().getDrawable(R.drawable.week_circle_shape));
                    mTextTue.setTextColor(getActivity().getResources().getColor(R.color.tab_bar_bg));
                    mTextTue.setTag(R.drawable.week_circle_shape);
                }
                break;
            case 3:
                if (selected) {
                    mTextWed.setBackground(getActivity().getResources().getDrawable(R.drawable.week_circle_shape_selected));
                    mTextWed.setTextColor(getActivity().getResources().getColor(R.color.white));
                    mTextWed.setTag(R.drawable.week_circle_shape_selected);
                } else {
                    mTextWed.setBackground(getActivity().getResources().getDrawable(R.drawable.week_circle_shape));
                    mTextWed.setTextColor(getActivity().getResources().getColor(R.color.tab_bar_bg));
                    mTextWed.setTag(R.drawable.week_circle_shape);
                }
                break;
            case 4:
                if (selected) {
                    mTextThu.setBackground(getActivity().getResources().getDrawable(R.drawable.week_circle_shape_selected));
                    mTextThu.setTextColor(getActivity().getResources().getColor(R.color.white));
                    mTextThu.setTag(R.drawable.week_circle_shape_selected);
                } else {
                    mTextThu.setBackground(getActivity().getResources().getDrawable(R.drawable.week_circle_shape));
                    mTextThu.setTextColor(getActivity().getResources().getColor(R.color.tab_bar_bg));
                    mTextThu.setTag(R.drawable.week_circle_shape);
                }
                break;
            case 5:
                if (selected) {
                    mTextFri.setBackground(getActivity().getResources().getDrawable(R.drawable.week_circle_shape_selected));
                    mTextFri.setTextColor(getActivity().getResources().getColor(R.color.white));
                    mTextFri.setTag(R.drawable.week_circle_shape_selected);
                } else {
                    mTextFri.setBackground(getActivity().getResources().getDrawable(R.drawable.week_circle_shape));
                    mTextFri.setTextColor(getActivity().getResources().getColor(R.color.tab_bar_bg));
                    mTextFri.setTag(R.drawable.week_circle_shape);
                }
                break;
            case 6:
                if (selected) {
                    mTextSat.setBackground(getActivity().getResources().getDrawable(R.drawable.week_circle_shape_selected));
                    mTextSat.setTextColor(getActivity().getResources().getColor(R.color.white));
                    mTextSat.setTag(R.drawable.week_circle_shape_selected);
                } else {
                    mTextSat.setBackground(getActivity().getResources().getDrawable(R.drawable.week_circle_shape));
                    mTextSat.setTextColor(getActivity().getResources().getColor(R.color.tab_bar_bg));
                    mTextSat.setTag(R.drawable.week_circle_shape);
                }
                break;
            case 7:
                if (selected) {
                    mTextSun.setBackground(getActivity().getResources().getDrawable(R.drawable.week_circle_shape_selected));
                    mTextSun.setTextColor(getActivity().getResources().getColor(R.color.white));
                    mTextSun.setTag(R.drawable.week_circle_shape_selected);
                } else {
                    mTextSun.setBackground(getActivity().getResources().getDrawable(R.drawable.week_circle_shape));
                    mTextSun.setTextColor(getActivity().getResources().getColor(R.color.tab_bar_bg));
                    mTextSun.setTag(R.drawable.week_circle_shape);
                }
                break;
            default:
                break;
        }
    }

    private void updateDeviceOnOff(boolean turnOn) {
        if (turnOn) {
            if (mTextDeviceOn.getTag().equals(R.drawable.device_onoff_btn_shape)) {
                mTextDeviceOn.setBackground(getActivity().getResources().getDrawable(R.drawable.device_onoff_btn_shape_pressed));
                mTextDeviceOn.setTextColor(getActivity().getResources().getColor(R.color.white));
                mTextDeviceOn.setTag(R.drawable.device_onoff_btn_shape_pressed);
                mTextDeviceOff.setBackground(getActivity().getResources().getDrawable(R.drawable.device_onoff_btn_shape));
                mTextDeviceOff.setTextColor(getActivity().getResources().getColor(R.color.tab_bar_bg));
                mTextDeviceOff.setTag(R.drawable.device_onoff_btn_shape);
            } else {
                mTextDeviceOn.setBackground(getActivity().getResources().getDrawable(R.drawable.device_onoff_btn_shape));
                mTextDeviceOn.setTextColor(getActivity().getResources().getColor(R.color.tab_bar_bg));
                mTextDeviceOn.setTag(R.drawable.device_onoff_btn_shape);
                mTextDeviceOff.setBackground(getActivity().getResources().getDrawable(R.drawable.device_onoff_btn_shape_pressed));
                mTextDeviceOff.setTextColor(getActivity().getResources().getColor(R.color.white));
                mTextDeviceOff.setTag(R.drawable.device_onoff_btn_shape_pressed);
            }
        } else {
            if (mTextDeviceOff.getTag().equals(R.drawable.device_onoff_btn_shape)) {
                mTextDeviceOff.setBackground(getActivity().getResources().getDrawable(R.drawable.device_onoff_btn_shape_pressed));
                mTextDeviceOff.setTextColor(getActivity().getResources().getColor(R.color.white));
                mTextDeviceOff.setTag(R.drawable.device_onoff_btn_shape_pressed);
                mTextDeviceOn.setBackground(getActivity().getResources().getDrawable(R.drawable.device_onoff_btn_shape));
                mTextDeviceOn.setTextColor(getActivity().getResources().getColor(R.color.tab_bar_bg));
                mTextDeviceOn.setTag(R.drawable.device_onoff_btn_shape);
            } else {
                mTextDeviceOff.setBackground(getActivity().getResources().getDrawable(R.drawable.device_onoff_btn_shape));
                mTextDeviceOff.setTextColor(getActivity().getResources().getColor(R.color.tab_bar_bg));
                mTextDeviceOff.setTag(R.drawable.device_onoff_btn_shape);
                mTextDeviceOn.setBackground(getActivity().getResources().getDrawable(R.drawable.device_onoff_btn_shape_pressed));
                mTextDeviceOn.setTextColor(getActivity().getResources().getColor(R.color.white));
                mTextDeviceOn.setTag(R.drawable.device_onoff_btn_shape_pressed);
            }
        }
    }

    private void startWait() {
        mProgressBar.show();
    }

    private void stopWait() {
        mProgressBar.hide();
    }

    private View.OnClickListener mSaveOnClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            if (mProgressBar != null && !mProgressBar.isShown() && !isSavingSchedule) {
                isSavingSchedule = true;
                startWait();
                ArrayList<FNSchedule> scheduleList = AppDataSchedule.getInstance(getActivity()).loadScheduleList(AppDataBase.SCHEDULE_TYPE_NORMAL, mCurFNThing.getThingId());
                boolean isNewSchedule = mFNSchedule != null ? false : true;

                if (scheduleList.size() < SCHEDULE_LIST_MAX_NUMBER || !isNewSchedule) {
                    FNSchedule newSchedule = createSchedule();

                    boolean result = AppDataSchedule.getInstance(getActivity()).updateSchedule(AppDataBase.SCHEDULE_TYPE_NORMAL, mCurFNThing.getThingId(), newSchedule, isNewSchedule);
                    if (result) {
                        if (!isNewSchedule) {
                            for (FNSchedule schedule:scheduleList) {
                                if (schedule.getScheduleId() == newSchedule.getScheduleId()) {
                                    scheduleList.remove(schedule);
                                    break;
                                }
                            }
                        }
                        scheduleList.add(newSchedule);
                        mThingsManager.setSchedule(mCurFNThing.getThingId(), scheduleList);
                    } else {
                        isSavingSchedule = false;
                        stopWait();
                    }
                } else {
                    isSavingSchedule = false;
                    stopWait();
                    FNDialog.showErrorDialog(getActivity(), getString(R.string.title_add_schedule), getString(R.string.fn_msg_schedule_maximum_reached));
                }
            }
        }
    };

    final ThingsHandler.ThingsListener mThingsListener = new ThingsHandler.ThingsListener() {
        @Override
        public void onListUserOwnedThings(ThingsManager instance, boolean result, String userId, ArrayList<FNThing> thingArrayList, FNError error) {

        }

        @Override
        public void onListUserInvitedThings(ThingsManager instance, boolean result, String userId, ArrayList<FNThing> thingArrayList, FNError error) {

        }

        @Override
        public void onListUserSharedThings(ThingsManager instance, boolean result, String userId, ArrayList<FNThing> thingArrayList, FNError error) {

        }

        @Override
        public void onListUserAllThings(ThingsManager instance, boolean result, String userId, ArrayList<FNThing> ownedThingArrayList, ArrayList<FNThing> sharedThingArrayList, ArrayList<FNThing> invitedThingArrayList, FNError error) {

        }

        @Override
        public void onListThingEvents(ThingsManager instance, boolean result, String thingId, String eventName, ArrayList<FNEvent> eventList, FNError error) {

        }

        @Override
        public void onListThingEventsByTime(ThingsManager instance, boolean result, String thingId, String eventName, String startTime, String endTime, ArrayList<FNEvent> eventList, FNError error) {

        }

        @Override
        public void onCurBatteryLevel(ThingsManager instance, boolean result, String thingId, String batteryLevel, FNError error) {

        }

        @Override
        public void onCurFirmwareVersion(ThingsManager instance, boolean result, String thingId, String fwVersion, FNError error) {

        }

        @Override
        public void onCurSwitchedStatus(ThingsManager instance, boolean result, String thingId, boolean isSwitch, FNError error) {

        }

        @Override
        public void onNewFirmwareVersion(ThingsManager instance, boolean result, String thingId, FNFirmware fnFirmware) {

        }

        @Override
        public void onStartFwUpdate(ThingsManager instance, boolean result, String thingId, String fwId) {

        }

        @Override
        public void onCurRingTone(ThingsManager instance, boolean result, String thingId, String ringtoneId, FNError error) {

        }

        @Override
        public void onGetScheduleList(ThingsManager instance, boolean result, String thingId, ArrayList<FNSchedule> scheduleList, FNError error) {

        }

        @Override
        public void onListSharedUsers(ThingsManager instance, boolean result, String thingId, ArrayList<FNUserProfile> userList, FNError error) {

        }

        @Override
        public void onCreateClaimCode(ThingsManager instance, boolean result, String claimCode, FNError error) {

        }

        @Override
        public void onCreateThing(ThingsManager instance, boolean result, FNThing fnThing, FNError error) {

        }

        @Override
        public void onUpdateThing(ThingsManager instance, boolean result, FNThing fnThing, FNError error) {

        }

        @Override
        public void onChangeRingtone(ThingsManager instance, boolean result, String thingId, int ringtoneId) {

        }

        @Override
        public void onSetTimer(ThingsManager instance, boolean result, String thingId, String value) {

        }

        @Override
        public void onSetSchedule(ThingsManager instance, boolean result, String thingId, String value) {
            if (result) {
                FNDialog.showSuccessDialog(getActivity(), getString(R.string.title_add_schedule), getString(R.string.fn_msg_schedule_change_successfully), new FNAlertDialog.OnSweetClickListener() {
                    @Override
                    public void onClick(FNAlertDialog FNAlertDialog) {
                        FNAlertDialog.dismiss();
                        isSavingSchedule = false;
                        onBackPressed();
                    }
                });

            } else {
                isSavingSchedule = false;
                FNDialog.showErrorDialog(getActivity(), getString(R.string.title_add_schedule), getString(R.string.fn_msg_schedule_change_failed));
            }
            stopWait();
        }

        @Override
        public void onShareInvited(ThingsManager instance, boolean result, String thingId, FNError error) {

        }

        @Override
        public void onShareAccepted(ThingsManager instance, boolean result, String thingId, FNError error) {

        }

        @Override
        public void onShareRejected(ThingsManager instance, boolean result, String thingId, FNError error) {

        }

        @Override
        public void onSwitchOnOff(ThingsManager instance, boolean result, String thingId, boolean isTurnOn) {

        }

        @Override
        public void onCloseCallSlot(ThingsManager instance, boolean result, FNError error) {

        }

        @Override
        public void onTakeCallSlot(ThingsManager instance, boolean result, String thingId, String callSlot, FNError error) {

        }

        @Override
        public void onListInvitedUsers(ThingsManager instance, boolean result, String thingId, ArrayList<FNUserProfile> userList, FNError error) {

        }

        @Override
        public void onDeleteThing(ThingsManager instance, final boolean result, final FNError error) {

        }

        @Override
        public void onGetThing(ThingsManager instance, boolean result, FNThing fnThing, FNError error) {

        }
    };
}
