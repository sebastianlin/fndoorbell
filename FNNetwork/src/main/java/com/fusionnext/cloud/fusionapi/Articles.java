package com.fusionnext.cloud.fusionapi;

import android.content.Context;

import com.fusionnext.cloud.datalink.http.HttpGet;
import com.fusionnext.cloud.datalink.http.HttpParam;
import com.fusionnext.cloud.fusionapi.objects.Article;

/**
 * Created by Mike Chang on 2017/3/7
 */
public class Articles {
    protected final static String _TAG = "Articles";


    private Context context;

    public Articles(Context context) {
        this.context = context;
    }

    public void getArticle(final FusionAPIHandler.OnFusionAPIListener onFusionAPIListener) {
        HttpParam.HttpSubParam param = new HttpParam.HttpSubParam();

        param.accessToken = Public.getAuthorization(context).getDeviceAccessToken();
        param.url = HttpData.generateHttpGetUrl(HttpData._GET_TYPE_GET_CLIENT, Public.getAuthorization(context).getClientId());
        param.cacheName = "";
        HttpParam[] params = new HttpParam[1];
        params[0] = new HttpParam();
        params[0].params.clear();
        params[0].params.add(param);

        HttpGet httpGet = new HttpGet();
        httpGet.setContext(context);
        httpGet.setTag(HttpGet._HTTP_T_GET_NORMAL_TYPE);
        httpGet.setOnParseJsonListener(new HttpGet.OnParseJsonListener() {
            @Override
            public void onParse(boolean result, String returnStr) {
                if (returnStr != null) {
                    Article article = Article.parseArticle(returnStr);
                    onFusionAPIListener.onGetArticle(result, article);
                }
            }
        });
        httpGet.execute(params);
    }


}
