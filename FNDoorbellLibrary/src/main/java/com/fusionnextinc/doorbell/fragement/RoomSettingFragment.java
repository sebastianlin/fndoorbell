package com.fusionnextinc.doorbell.fragement;

import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.TextView;

import com.fusionnextinc.doorbell.Adapter.RoomSettingListAdapter;
import com.fusionnextinc.doorbell.MyApplication;
import com.fusionnextinc.doorbell.R;
import com.fusionnextinc.doorbell.manager.data.AppDataBase;
import com.fusionnextinc.doorbell.manager.data.AppDataGroup;
import com.fusionnextinc.doorbell.manager.data.AppDataHandler;
import com.fusionnextinc.doorbell.manager.data.AppDataThing;
import com.fusionnextinc.doorbell.manager.data.ListMap;
import com.fusionnextinc.doorbell.manager.thingsManager.ThingsHandler;
import com.fusionnextinc.doorbell.manager.thingsManager.ThingsManager;
import com.fusionnextinc.doorbell.objects.FNError;
import com.fusionnextinc.doorbell.objects.FNEvent;
import com.fusionnextinc.doorbell.objects.FNFirmware;
import com.fusionnextinc.doorbell.objects.FNGroup;
import com.fusionnextinc.doorbell.objects.FNSchedule;
import com.fusionnextinc.doorbell.objects.FNThing;
import com.fusionnextinc.doorbell.objects.FNUserProfile;
import com.fusionnextinc.doorbell.utils.FNLog;
import com.fusionnextinc.doorbell.utils.Utils;
import com.fusionnextinc.doorbell.widget.FNDialog;
import com.fusionnextinc.doorbell.widget.FNHeaderListView.FNHeaderListView;
import com.fusionnextinc.doorbell.widget.FNLayoutUtil;

import java.util.ArrayList;
import java.util.Map;

/**
 * Created by Mike Chang on 2018/8/8
 */
public class RoomSettingFragment extends FNFragment implements View.OnClickListener, AbsListView.OnItemClickListener {
    private static final String TAG = RoomSettingFragment.class.getSimpleName();

    private FNLayoutUtil mLayoutUtil;
    private ThingsManager mThingsManager;
    private AppDataThing mAppDataThing;
    private RoomSettingListAdapter mRoomSettingListAdapter = null;
    private FNHeaderListView mThingsListView;
    private ImageView mImageRoom;
    private TextView mTextViewRoom;
    private int mId;

    public static final int ROOMS_FRAGMENT_TYPE_CREATE = 0;
    public static final int ROOMS_FRAGMENT_TYPE_EDIT = 1;
    private int mRoomsFragmentType = ROOMS_FRAGMENT_TYPE_CREATE;

    public static void openFragment(boolean clearOtherStack, int type, int id) {
        RoomSettingFragment thingsFragment = new RoomSettingFragment();
        FNFragmentManager.getInstance().openFragment(thingsFragment, clearOtherStack);
        thingsFragment.mId = id;
        thingsFragment.mRoomsFragmentType = type;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mLayoutUtil = new FNLayoutUtil(getActivity(), MyApplication.LAYOUT_WIDTH, MyApplication.LAYOUT_HEIGHT, FNLayoutUtil.BASE_WIDTH);
        mThingsManager = ThingsManager.getInstance(getActivity());
        mAppDataThing = AppDataThing.getInstance(getActivity());
        mAppDataThing.setAppDataThingListener(appDataThingListener);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        final ViewGroup nullParent = null;

        getActivity().setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        getActionBar().resetAll();
        getActionBar().setTitle(getString(R.string.app_name), Gravity.CENTER, null);
        getActionBar().setStartImage(R.drawable.back, new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        getActionBar().setEndText(getString(R.string.fn_btn_room_save), getResources().getColor(R.color.button_text_color), new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int groupId;
                String groupType = null;
                boolean isNewGroup = true;
                if (mRoomsFragmentType == ROOMS_FRAGMENT_TYPE_CREATE) {
                    groupType = mThingsManager.getRoomTypeById(mId);
                    groupId = AppDataGroup.getInstance(getActivity()).genGroupId();
                    isNewGroup = true;
                } else {
                    Map<String, Object> mapGroup = AppDataGroup.getInstance(getActivity()).getGroupData(mId);
                    groupId = (int) mapGroup.get(AppDataGroup.TAG_GROUP_ID);
                    groupType = (String) mapGroup.get(AppDataGroup.TAG_GROUP_TYPE);
                    isNewGroup = false;
                }
                String groupName = mTextViewRoom.getText().length() > 0 ? mTextViewRoom.getText().toString() : mThingsManager.getRoomStringById(mId);
                int deviceCount = mRoomSettingListAdapter.getSelectedOwnedThingList().size() + mRoomSettingListAdapter.getSelectedSharedThingList().size();
                FNGroup fnGroup = new FNGroup(false, groupId, groupType, groupName, deviceCount);
                AppDataGroup.getInstance(getActivity()).updateGroup(AppDataBase.GROUP_TYPE_NORMAL, fnGroup, isNewGroup);
                AppDataThing.getInstance(getActivity()).saveThingsList(AppDataBase.THING_TYPE_THINGS_GROUP_OWNED, String.valueOf(groupId), mRoomSettingListAdapter.getSelectedOwnedThingList());
                AppDataThing.getInstance(getActivity()).saveThingsList(AppDataBase.THING_TYPE_THINGS_GROUP_SHARED, String.valueOf(groupId), mRoomSettingListAdapter.getSelectedSharedThingList());
                HomeFragment.openFragment(true, ThingsFragment.THINGS_FRAGMENT_TYPE_NORMAL, null, HomeFragment.TabType.TAB_TYPE_GROUP, false);
            }
        });
        getActionBar().setCoverMode(false);
        getActionBar().show();
        View view = inflater.inflate(R.layout.fragment_room_settings, nullParent);
        mLayoutUtil.fitAllView(view);
        mImageRoom = view.findViewById(R.id.img_room);
        mTextViewRoom = view.findViewById(R.id.txt_room_type);
        mThingsListView = view.findViewById(R.id.things_list);
        mThingsListView.setOnScrollListener(onScrollListener);
        mThingsListView.setOnItemClickListener(this);
        if (mRoomsFragmentType == ROOMS_FRAGMENT_TYPE_CREATE) {
            mImageRoom.setImageResource(mThingsManager.getRoomImageById(mId));
            mTextViewRoom.setHint(mThingsManager.getRoomStringById(mId));
        } else {
            Map<String, Object> mapGroup = AppDataGroup.getInstance(getActivity()).getGroupData(mId);
            String groupType = (String) mapGroup.get(AppDataGroup.TAG_GROUP_TYPE);
            mTextViewRoom.setText((String) mapGroup.get(AppDataGroup.TAG_GROUP_NAME));
            mImageRoom.setImageResource(ThingsManager.getInstance(getActivity()).getRoomImageByType(groupType));
        }
        initThingsList();
        return view;
    }

    @Override
    public void onClick(View view) {

    }

    private boolean initThingsList() {
        if (mThingsListView == null) return false;
        ListMap ownedThingsList = new ListMap();
        ListMap sharedThingsList = new ListMap();

        sharedThingsList.addAll(mAppDataThing.getAdapterData(AppDataBase.THING_TYPE_THINGS_SHARED));
        ownedThingsList.addAll(mAppDataThing.getAdapterData(AppDataBase.THING_TYPE_THINGS_OWNED));
        //Setup the adapter
        mRoomSettingListAdapter = new RoomSettingListAdapter(getActivity(), ownedThingsList, sharedThingsList, R.layout.adapter_room_thing_list);

        if (mRoomsFragmentType == ROOMS_FRAGMENT_TYPE_EDIT) {
            ListMap selectedOwnedThingList = new ListMap();
            ListMap selectedSharedThingList = new ListMap();

            selectedOwnedThingList.addAll(mAppDataThing.getAdapterData(AppDataBase.THING_TYPE_THINGS_GROUP_OWNED, String.valueOf(mId)));
            selectedSharedThingList.addAll(mAppDataThing.getAdapterData(AppDataBase.THING_TYPE_THINGS_GROUP_SHARED, String.valueOf(mId)));
            for (int i = 0; i < selectedOwnedThingList.size(); i++) {
                Map<String, Object> selectedThing = selectedOwnedThingList.get(i);
                mRoomSettingListAdapter.setSelectedOwnedThingList((String) selectedThing.get(AppDataThing.TAG_THING_ID));
            }
            for (int i = 0; i < selectedSharedThingList.size(); i++) {
                Map<String, Object> selectedThing = selectedSharedThingList.get(i);
                mRoomSettingListAdapter.setSelectedSharedThingList((String) selectedThing.get(AppDataThing.TAG_THING_ID));
            }
        }
        mThingsListView.setAdapter(mRoomSettingListAdapter);
        mThingsListView.setPullRefreshEnable(true);
        mThingsListView.setPullLoadEnable(false);
        mThingsListView.setOnItemClickListener(this);
        mThingsListView.setPullListViewListener(new FNHeaderListView.IPullListViewListener() {
            @Override
            public void onRefresh() {
                if (Utils.isNetworkConnected(getActivity())) {
                    mAppDataThing.refreshThingsListData();
                } else {
                    FNDialog.showNoNetworkDialog(getActivity());
                    if (mThingsListView != null) {
                        mThingsListView.setPullLoadEnable(false);
                        mThingsListView.stopLoadMore();
                        mThingsListView.stopRefresh();
                    }
                }
            }

            @Override
            public void onLoadMore() {
                FNLog.d(TAG, "onLoadMoreData: Click share footer bar");
            }
        });
        return true;
    }

    @Override
    public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {

    }

    private AbsListView.OnScrollListener onScrollListener = new AbsListView.OnScrollListener() {
        @Override
        public void onScrollStateChanged(AbsListView view, int scrollState) {

            switch (scrollState) {
                case SCROLL_STATE_IDLE:
                    break;
                case SCROLL_STATE_FLING:
                    break;
            }
        }

        @Override
        public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {

        }
    };

    final ThingsHandler.ThingsListener thingsListener = new ThingsHandler.ThingsListener() {

        @Override
        public void onListUserOwnedThings(ThingsManager instance, boolean result, String userId, ArrayList<FNThing> thingArrayList, FNError error) {

        }

        @Override
        public void onListUserInvitedThings(ThingsManager instance, boolean result, String userId, ArrayList<FNThing> thingArrayList, FNError error) {

        }

        @Override
        public void onListUserSharedThings(ThingsManager instance, boolean result, String userId, ArrayList<FNThing> thingArrayList, FNError error) {

        }

        @Override
        public void onListUserAllThings(ThingsManager instance, boolean result, String userId, ArrayList<FNThing> ownedThingArrayList, ArrayList<FNThing> sharedThingArrayList, ArrayList<FNThing> invitedThingArrayList, FNError error) {
        }

        @Override
        public void onListThingEvents(ThingsManager instance, boolean result, final String thingId, String eventName, ArrayList<FNEvent> eventList, FNError error) {

        }

        @Override
        public void onListThingEventsByTime(ThingsManager instance, boolean result, final String thingId, String eventName, String startTime, String endTime, ArrayList<FNEvent> eventList, FNError error) {

        }

        @Override
        public void onCurBatteryLevel(ThingsManager instance, boolean result, String thingId, String batteryLevel, FNError error) {

        }

        @Override
        public void onCurFirmwareVersion(ThingsManager instance, boolean result, String thingId, String fwVersion, FNError error) {

        }

        @Override
        public void onCurSwitchedStatus(ThingsManager instance, boolean result, String thingId, boolean isSwitch, FNError error) {

        }

        @Override
        public void onNewFirmwareVersion(ThingsManager instance, boolean result, String thingId, FNFirmware fnFirmware) {

        }

        @Override
        public void onStartFwUpdate(ThingsManager instance, boolean result, String thingId, String fwId) {

        }

        @Override
        public void onCurRingTone(ThingsManager instance, boolean result, String thingId, String ringtoneId, FNError error) {

        }

        @Override
        public void onGetScheduleList(ThingsManager instance, boolean result, String thingId, ArrayList<FNSchedule> scheduleList, FNError error) {

        }

        @Override
        public void onListInvitedUsers(ThingsManager instance, boolean result, String thingId, ArrayList<FNUserProfile> userList, FNError error) {

        }

        @Override
        public void onListSharedUsers(ThingsManager instance, boolean result, String thingId, ArrayList<FNUserProfile> userList, FNError error) {

        }

        @Override
        public void onCreateClaimCode(ThingsManager instance, boolean result, String claimCode, FNError error) {

        }

        @Override
        public void onCreateThing(ThingsManager instance, boolean result, FNThing fnThing, FNError error) {

        }

        @Override
        public void onUpdateThing(ThingsManager instance, boolean result, FNThing fnThing, FNError error) {

        }

        @Override
        public void onDeleteThing(ThingsManager instance, boolean result, FNError error) {

        }

        @Override
        public void onGetThing(ThingsManager instance, boolean result, FNThing fnThing, FNError error) {

        }

        @Override
        public void onChangeRingtone(ThingsManager instance, boolean result, String thingId, int ringtoneId) {

        }

        @Override
        public void onSetTimer(ThingsManager instance, boolean result, String thingId, String value) {

        }

        @Override
        public void onSetSchedule(ThingsManager instance, boolean result, String thingId, String value) {

        }

        @Override
        public void onShareInvited(ThingsManager instance, boolean result, String thingId, FNError error) {

        }

        @Override
        public void onShareAccepted(ThingsManager instance, boolean result, String thingId, FNError error) {

        }

        @Override
        public void onShareRejected(ThingsManager instance, boolean result, String thingId, FNError error) {

        }

        @Override
        public void onSwitchOnOff(ThingsManager instance, boolean result, String thingId, boolean isTurnOn) {

        }

        @Override
        public void onCloseCallSlot(ThingsManager instance, boolean result, FNError error) {

        }

        @Override
        public void onTakeCallSlot(ThingsManager instance, boolean result, String thingId, String callSlot, FNError error) {

        }
    };

    final AppDataHandler.AppDataThingListener appDataThingListener = new AppDataHandler.AppDataThingListener() {
        @Override
        public void onRefreshEventCounts(final String thingId) {
            MyApplication.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    if (mRoomSettingListAdapter != null) {
                        mRoomSettingListAdapter.updateAdapterDataById(thingId);
                    }
                }
            });
        }

        @Override
        public void onRefreshBatteryLevel(final String thingId) {
            MyApplication.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    if (mRoomSettingListAdapter != null) {
                        mRoomSettingListAdapter.updateAdapterDataById(thingId);
                    }
                }
            });
        }

        @Override
        public void onRefreshListAdapter(int type) {
            if (mRoomSettingListAdapter != null) {
                mRoomSettingListAdapter.updateAdapterData(type);
            }

            if (mThingsListView != null) {
                mThingsListView.setPullLoadEnable(false);
                mThingsListView.stopLoadMore();
                mThingsListView.stopRefresh();
            }
        }
    };
}
