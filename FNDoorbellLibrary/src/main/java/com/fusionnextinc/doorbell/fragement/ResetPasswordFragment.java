package com.fusionnextinc.doorbell.fragement;

import android.content.Context;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.text.method.HideReturnsTransformationMethod;
import android.text.method.PasswordTransformationMethod;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;

import com.fusionnextinc.doorbell.MyApplication;
import com.fusionnextinc.doorbell.R;
import com.fusionnextinc.doorbell.manager.account.AaccountHandler;
import com.fusionnextinc.doorbell.manager.account.AccountManager;
import com.fusionnextinc.doorbell.objects.FNError;
import com.fusionnextinc.doorbell.utils.Utils;
import com.fusionnextinc.doorbell.widget.FNDialog;
import com.fusionnextinc.doorbell.widget.FNLayoutUtil;
import com.fusionnextinc.doorbell.widget.FNProgressBarCustom;

/**
 * Created by Mike Chang on 2017/7/31
 */
public class ResetPasswordFragment extends FNFragment implements View.OnClickListener {
    private static final String TAG = ResetPasswordFragment.class.getSimpleName();

    private FNLayoutUtil layoutUtil;
    private EditText editAccount;
    private EditText editRecoveryCode;
    private EditText editNewPassword;
    private EditText editConfirmPassword;
    private ImageView ivShowNewPwd, ivShowConfirmPwd;
    private FNProgressBarCustom progressBar;
    private AccountManager accountManager;
    private static String accountEmail = "";

    public static void openFragment(boolean clearOtherStack, String email) {
        FNFragmentManager.getInstance().openFragment(new ResetPasswordFragment(), clearOtherStack);
        accountEmail = email;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        layoutUtil = new FNLayoutUtil(getActivity(), MyApplication.LAYOUT_WIDTH, MyApplication.LAYOUT_HEIGHT, FNLayoutUtil.BASE_WIDTH);
        accountManager = AccountManager.getInstance(getActivity());
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        final ViewGroup nullParent = null;

        getActivity().setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        getActionBar().resetAll();
        getActionBar().setTitle(getString(R.string.fn_title_reset_password), Gravity.CENTER, null);
        getActionBar().setCoverMode(false);
        getActionBar().setStartImage(R.drawable.back, new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        getActionBar().show();
        View view = inflater.inflate(R.layout.fragment_reset_password, nullParent);
        layoutUtil.fitAllView(view);

        editAccount = (EditText) view.findViewById(R.id.et_account);
        editNewPassword = (EditText) view.findViewById(R.id.et_new_password);
        editRecoveryCode = (EditText) view.findViewById(R.id.et_recovery_code);
        editConfirmPassword = (EditText) view.findViewById(R.id.et_confirm_password);
        ivShowNewPwd = (ImageView) view.findViewById(R.id.btn_show_new_password);
        ivShowNewPwd.setTag(R.drawable.show_password);
        ivShowNewPwd.setImageDrawable(getResources().getDrawable(R.drawable.show_password));
        ivShowConfirmPwd = (ImageView) view.findViewById(R.id.btn_show_confirm_password);
        ivShowConfirmPwd.setTag(R.drawable.show_password);
        ivShowConfirmPwd.setImageDrawable(getResources().getDrawable(R.drawable.show_password));
        Button btnDone = (Button) view.findViewById(R.id.btn_change_password_done);
        progressBar = new FNProgressBarCustom(getActivity(), (ViewGroup) view.findViewById(R.id.fragment_reset_password));

        editAccount.setText(accountEmail);
        btnDone.setOnClickListener(this);
        ivShowNewPwd.setOnClickListener(this);
        ivShowConfirmPwd.setOnClickListener(this);
        return view;
    }

    @Override
    public void onClick(View view) {
        if (Utils.isNetworkConnected(getActivity())) {
            if (view.getId() == R.id.btn_change_password_done) {
                String account = editAccount.getText().toString().trim();
                String newPassword = editNewPassword.getText().toString().trim();
                String recoveryCode = editRecoveryCode.getText().toString().trim();
                String confirmPassword = editConfirmPassword.getText().toString().trim();

                if (!Utils.isNetworkConnected(getActivity())) {
                    FNDialog.showNoNetworkDialog(getActivity());
                    return;
                }
                // must be a check for not null, for empty field and for email validity(matchs email pattern)
                if (Utils.emptyFieldsRemaining(editAccount, editRecoveryCode, editNewPassword, editConfirmPassword)) {
                    FNDialog.showErrorDialog(getActivity(), getString(R.string.fn_title_reset_password), getString(R.string.fn_msg_login_fields_are_miss));
                    return;
                }
                /*if (!Utils.passwordIsValid(newPassword)) {
                    FNDialog.showErrorDialog(getActivity(), getString(R.string.fn_title_reset_password), getString(R.string.fn_msg_login_email_is_invaild));
                    return;
                }*/

                if (!newPassword.equals(confirmPassword)) {
                    FNDialog.showErrorDialog(getActivity(), getString(R.string.fn_title_reset_password), getString(R.string.fn_msg_login_pwd_is_not_match));
                    return;
                }

                //mLogin.loginWithEmail(((EditText) findViewById(R.id.et_email)).getText().toString(),((EditText) findViewById(R.id.et_password)).getText().toString());
                if (newPassword.length() > 0 && recoveryCode.length() > 0) {
                    startWait();
                    accountManager.setAccountListener(accountListener);
                    accountManager.setUserPassword(account, newPassword, recoveryCode);
                }
            } else if (view.getId() == R.id.btn_show_new_password) {
                if ((Integer) ivShowNewPwd.getTag() == R.drawable.show_password) {
                    ivShowNewPwd.setImageDrawable(getResources().getDrawable(R.drawable.show_password_pressed));
                    ivShowNewPwd.setTag(R.drawable.show_password_pressed);
                    editNewPassword.setTransformationMethod(HideReturnsTransformationMethod.getInstance());
                } else {
                    ivShowNewPwd.setImageDrawable(getResources().getDrawable(R.drawable.show_password));
                    ivShowNewPwd.setTag(R.drawable.show_password);
                    editNewPassword.setTransformationMethod(PasswordTransformationMethod.getInstance());
                }
            } else if (view.getId() == R.id.btn_show_confirm_password) {
                if ((Integer) ivShowConfirmPwd.getTag() == R.drawable.show_password) {
                    ivShowConfirmPwd.setImageDrawable(getResources().getDrawable(R.drawable.show_password_pressed));
                    ivShowConfirmPwd.setTag(R.drawable.show_password_pressed);
                    editConfirmPassword.setTransformationMethod(HideReturnsTransformationMethod.getInstance());
                } else {
                    ivShowConfirmPwd.setImageDrawable(getResources().getDrawable(R.drawable.show_password));
                    ivShowConfirmPwd.setTag(R.drawable.show_password);
                    editConfirmPassword.setTransformationMethod(PasswordTransformationMethod.getInstance());
                }
            }
        } else {
            FNDialog.showNoNetworkDialog(getActivity());
        }
    }

    private void startWait() {
        progressBar.show();
    }


    private void stopWait() {
        progressBar.hide();
    }

    private void closeKeyboard() {
        if (getActivity().getCurrentFocus() != null && getActivity().getCurrentFocus().getWindowToken() != null) {
            InputMethodManager inputManager = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
            inputManager.hideSoftInputFromWindow(getActivity().getCurrentFocus().getWindowToken(), 0);
        }
    }

    final AaccountHandler.AccountListener accountListener = new AaccountHandler.AccountListener() {

        @Override
        public void onUserRegistered(AccountManager instance, boolean result, FNError error) {

        }

        @Override
        public void onUserLogined(AccountManager instance, boolean result, FNError error) {

        }

        @Override
        public void onGetUserInfo(AccountManager instance, boolean result, FNError error) {

        }

        @Override
        public void onUpdateUserInfo(AccountManager instance, boolean result, FNError error) {

        }

        @Override
        public void onUserLogout(AccountManager instance, boolean result, FNError error) {

        }

        @Override
        public void onResetPassword(AccountManager instance, boolean result, FNError error) {

        }

        @Override
        public void onSetPassword(AccountManager instance, boolean result, FNError error) {
            if (isAdded()) {
                stopWait();
                closeKeyboard();
                if (result) {
                    FNDialog.showSuccessDialog(getActivity(), getString(R.string.fn_title_reset_password), getString(R.string.fn_msg_set_successfully));
                    LoginFragment.openFragment(true);
                } else {
                    FNDialog.showErrorDialog(getActivity(), getString(R.string.fn_msg_set_failed), error);
                }
            }
        }
    };
}
