package com.fusionnextinc.doorbell.objects;

import com.fusionnext.cloud.iot.objects.Setting;

import java.util.ArrayList;

/**
 * Created by Mike Chang on 2017/6/8
 */

public class FNSetting {
    protected final static String TAG = "FNSetting";

    private String settingId;
    private String thingId;
    private String name;
    private String value;
    private String createdAt;
    private String updatedAt;

    public FNSetting() {
        settingId = "";
        thingId = "";
        name = "";
        value = "";
        createdAt = "";
        updatedAt = "";
    }

    public void clean() {
        settingId = "";
        thingId = "";
        name = "";
        value = "";
        createdAt = "";
        updatedAt = "";
    }

    public static ArrayList<FNSetting> parseSettingArray(ArrayList<Setting> settingList) {
        ArrayList<FNSetting> fnSettingList = new ArrayList<FNSetting>();

        if (settingList == null) {
            return null;
        }
        for (int i = 0; i < settingList.size(); i++) {
            Setting setting = settingList.get(i);
            FNSetting fnSetting = new FNSetting();

            fnSetting.settingId = setting._settingId;
            fnSetting.thingId = setting._thingId;
            fnSetting.name = setting._name;
            fnSetting.value = setting._value;
            fnSetting.createdAt = setting._createdAt;
            fnSetting.updatedAt = setting._updatedAt;
            fnSettingList.add(fnSetting);
        }
        return fnSettingList;
    }

    public String getSettingId() {
        return settingId;
    }

    public void setSettingId(String settingId) {
        this.settingId = settingId;
    }

    public String getThingId() {
        return thingId;
    }

    public void setThingId(String thingId) {
        this.thingId = thingId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }

}
