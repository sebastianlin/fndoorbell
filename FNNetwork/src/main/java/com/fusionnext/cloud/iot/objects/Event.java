package com.fusionnext.cloud.iot.objects;

import android.util.Log;

import com.fusionnext.cloud.datalink.json.FNJsonArray;
import com.fusionnext.cloud.datalink.json.FNJsonObject;
import com.fusionnext.cloud.utils.Times;

import org.json.JSONException;

import java.util.ArrayList;
import java.util.TimeZone;

/**
 * Created by Mike Chang on 2017/3/27
 */

public class Event {
    protected final static String _TAG = "Event";

    public String _eventId;
    public String _thingId;
    public String _name;
    public String _payload;
    public String _createdAt;
    public String _updatedAt;

    public Event() {
        _eventId = "";
        _thingId = "";
        _name = "";
        _payload = "";
        _createdAt = "";
        _updatedAt = "";
    }

    public void clean() {
        _eventId = "";
        _thingId = "";
        _name = "";
        _payload = "";
        _createdAt = "";
        _updatedAt = "";
    }

    public boolean getDataFromJSONObject(FNJsonObject inviteObj) {
        clean();
        if (inviteObj == null) {
            Log.e(_TAG, "Null JSON Object!");
            return false;
        }

        try {
            //Get the id
            _eventId = inviteObj.getStringDefault("id");
            _thingId = inviteObj.getStringDefault("thing_id");
            _name = inviteObj.getStringDefault("name");
            _payload = inviteObj.getStringDefault("payload");
            _createdAt = inviteObj.getStringDefault("created_at");
            _createdAt = Times.converTime(_createdAt, TimeZone.getDefault());
            _updatedAt = inviteObj.getStringDefault("updated_at");
            _updatedAt = Times.converTime(_updatedAt, TimeZone.getDefault());
        } catch (Exception e) {
            e.printStackTrace();
            Log.e(_TAG, "Error occur during parsing JSON object");
            return false;
        }
        return true;
    }

    public static ArrayList<Event> parseEventArray(String json) {
        try {
            ArrayList<Event> eventList = new ArrayList<Event>();
            FNJsonObject eventObject = new FNJsonObject(json);
            FNJsonArray eventArray = eventObject.getJSONArray("items");
            if (eventArray == null) {
                return null;
            }
            for (int i = 0; i < eventArray.length(); i++) {
                FNJsonObject object = eventArray.getJSONObject(i);
                Event event = new Event();
                event.getDataFromJSONObject(object);
                if (event == null) return null;
                eventList.add(event);
            }
            return eventList;
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static int getCountByEventArray(String json) {
        try {
            FNJsonObject eventObject = new FNJsonObject(json);
            int count = eventObject.getIntDefault("total", 0);
            return count;
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return 0;
    }
}
