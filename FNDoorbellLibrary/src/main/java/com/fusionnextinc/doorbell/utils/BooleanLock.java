package com.fusionnextinc.doorbell.utils;

import java.util.concurrent.atomic.AtomicBoolean;

public class BooleanLock {
    /**
     * 全局标志位
     */
    private final AtomicBoolean flag = new AtomicBoolean(false);

    /**
     * 存储数据副本
     */
    private final ThreadLocal<Boolean> threadLocal = ThreadLocal.withInitial(() -> false);

    /**
     * 尝试获取锁
     */
    public boolean tryLock() {
        boolean result = flag.compareAndSet(false, true);
        if (result) {
            threadLocal.set(true);
        }
        return result;
    }

    /**
     * 释放锁
     */
    public boolean unLock() {
        if (threadLocal.get()) {
            threadLocal.set(false);
            return flag.compareAndSet(true, false);
        }
        return false;
    }

}
