package com.fusionnextinc.doorbell.fragement;

import android.content.Context;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.net.Uri;
import android.os.Bundle;
import android.text.method.HideReturnsTransformationMethod;
import android.text.method.PasswordTransformationMethod;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.fusionnextinc.doorbell.DefaultAppConfig;
import com.fusionnextinc.doorbell.MyApplication;
import com.fusionnextinc.doorbell.R;
import com.fusionnextinc.doorbell.manager.account.AaccountHandler;
import com.fusionnextinc.doorbell.manager.account.AccountManager;
import com.fusionnextinc.doorbell.manager.data.DataManager;
import com.fusionnextinc.doorbell.objects.FNError;
import com.fusionnextinc.doorbell.utils.Utils;
import com.fusionnextinc.doorbell.widget.FNDialog;
import com.fusionnextinc.doorbell.widget.FNLayoutUtil;
import com.fusionnextinc.doorbell.widget.FNProgressBarCustom;

/**
 * Created by Mike Chang on 2017/6/21
 */
public class LoginFragment extends FNFragment implements View.OnClickListener {
    private static final String TAG = LoginFragment.class.getSimpleName();

    private FNLayoutUtil layoutUtil;
    private EditText editAccount, editPassword;
    private TextView mTextForgotPwd, mTextCreateOne, mTextPolicy, mTextService;
    private Button mBtnLogin;
    private ImageView ivShowPwd;
    private FNProgressBarCustom progressBar;
    private AccountManager accountManager;

    private String URL_LEGAL_SERVICE = "http://www.ptcom.com.tw/ott/serviceterms.html";
    private String URL_POLICY = "http://www.ptcom.com.tw/ott/privacy.html";

    public static void openFragment(boolean clearOtherStack) {
        FNFragmentManager.getInstance().openFragment(new LoginFragment(), clearOtherStack);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        layoutUtil = new FNLayoutUtil(getActivity(), MyApplication.LAYOUT_WIDTH, MyApplication.LAYOUT_HEIGHT, FNLayoutUtil.BASE_WIDTH);
        accountManager = AccountManager.getInstance(getActivity());
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        final ViewGroup nullParent = null;

        getActivity().setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        getActionBar().hide();
        View view = inflater.inflate(R.layout.fragment_login, nullParent);
        layoutUtil.fitAllView(view);

        editAccount = view.findViewById(R.id.et_account);
        editPassword = view.findViewById(R.id.et_password);
        mBtnLogin = view.findViewById(R.id.btn_login);
        mTextForgotPwd = view.findViewById(R.id.text_forgot_pwd);
        mTextCreateOne = view.findViewById(R.id.text_create_one);
        mTextPolicy = view.findViewById(R.id.text_policy);
        mTextService = view.findViewById(R.id.text_service);
        ivShowPwd = view.findViewById(R.id.btn_show_password);
        ivShowPwd.setTag(R.drawable.show_password);
        ivShowPwd.setImageDrawable(getResources().getDrawable(R.drawable.show_password));
        progressBar = new FNProgressBarCustom(getActivity(), (ViewGroup) view.findViewById(R.id.fragment_login));

        mBtnLogin.setOnClickListener(this);
        mTextForgotPwd.setOnClickListener(this);
        mTextCreateOne.setOnClickListener(this);
        mTextPolicy.setOnClickListener(this);
        mTextService.setOnClickListener(this);
        ivShowPwd.setOnClickListener(this);
        return view;
    }

    @Override
    public void onClick(View view) {
        if (view.getId() == R.id.btn_login) {
            String email = editAccount.getText().toString().trim();
            String pwd = editPassword.getText().toString().trim();

            if (!com.fusionnext.cloud.utils.Utils.isNetworkConnected(getActivity())) {
                FNDialog.showNoNetworkDialog(getActivity());
                return;
            }
            // must be a check for not null, for empty field and for email validity(matchs email pattern)
            if (Utils.emptyFieldsRemaining(editAccount, editPassword)) {
                FNDialog.showErrorDialog(getActivity(), getString(R.string.fn_msg_login_failed), getString(R.string.fn_msg_login_fields_are_miss));
                return;
            }

            if (!Utils.emailIsValid(email)) {
                FNDialog.showErrorDialog(getActivity(), getString(R.string.fn_msg_login_failed), getString(R.string.fn_msg_login_email_is_invaild));
                return;
            }

            //mLogin.loginWithEmail(((EditText) findViewById(R.id.et_email)).getText().toString(),((EditText) findViewById(R.id.et_password)).getText().toString());
            if (email.length() > 0 && pwd.length() > 0) {
                startWait();
                accountManager.setAccountListener(accountListener);
                accountManager.setUserAccount(email);
                accountManager.setUserPassword(pwd);
                accountManager.setFirebaseToken(DataManager.getInstance(getActivity()).loadFireBasePushToken());
                accountManager.userLogin();
            }
        } else if (view.getId() == R.id.text_create_one) {
            RegisterFragment.openFragment(false);
        } else if (view.getId() == R.id.text_forgot_pwd) {
            ForgotPwdFragment.openFragment(false);
        } else if (view.getId() == R.id.btn_show_password) {
            if ((Integer) ivShowPwd.getTag() == R.drawable.show_password) {
                ivShowPwd.setImageDrawable(getResources().getDrawable(R.drawable.show_password_pressed));
                ivShowPwd.setTag(R.drawable.show_password_pressed);
                editPassword.setTransformationMethod(HideReturnsTransformationMethod.getInstance());
            } else {
                ivShowPwd.setImageDrawable(getResources().getDrawable(R.drawable.show_password));
                ivShowPwd.setTag(R.drawable.show_password);
                editPassword.setTransformationMethod(PasswordTransformationMethod.getInstance());
            }
        } else if (view.getId() == R.id.text_policy) {
            Intent intent = new Intent(Intent.ACTION_VIEW);
            Uri url = Uri.parse(URL_POLICY);
            intent.setData(url);
            startActivity(intent);
        } else if (view.getId() == R.id.text_service) {
            Intent intent = new Intent(Intent.ACTION_VIEW);
            Uri url = Uri.parse(URL_LEGAL_SERVICE);
            intent.setData(url);
            startActivity(intent);
        }
    }

    private void closeKeyboard() {
        if (getActivity().getCurrentFocus() != null && getActivity().getCurrentFocus().getWindowToken() != null) {
            InputMethodManager inputManager = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
            inputManager.hideSoftInputFromWindow(getActivity().getCurrentFocus().getWindowToken(), 0);
        }
    }

    private void startWait() {
        progressBar.show();
    }


    private void stopWait() {
        progressBar.hide();
    }

    final AaccountHandler.AccountListener accountListener = new AaccountHandler.AccountListener() {

        @Override
        public void onUserRegistered(AccountManager instance, boolean result, FNError error) {

        }

        @Override
        public void onUpdateUserInfo(AccountManager instance, boolean result, FNError error) {

        }

        @Override
        public void onUserLogout(AccountManager instance, boolean result, FNError error) {

        }

        @Override
        public void onResetPassword(AccountManager instance, boolean result, FNError error) {

        }

        @Override
        public void onSetPassword(AccountManager instance, boolean result, FNError error) {

        }

        @Override
        public void onUserLogined(AccountManager instance, boolean result, FNError error) {
            if (isAdded()) {
                closeKeyboard();
                stopWait();
                if (result) {
                    FNDialog.showSuccessDialog(getActivity(), getString(R.string.fn_btn_login_login), getString(R.string.fn_msg_login_successfully));
                    accountManager.getUserInfo();
                } else {
                    FNDialog.showErrorDialog(getActivity(), getString(R.string.fn_msg_login_failed), error);
                }
            }
        }

        @Override
        public void onGetUserInfo(AccountManager instance, boolean result, FNError error) {
            if (isAdded()) {
                closeKeyboard();
                stopWait();
                ThingsFragment thingsFragment = new ThingsFragment();
                thingsFragment.setUpdateThing(true, null);
                if (DefaultAppConfig.APP_SOLY) {
                    HomeFragment.openFragment(true, ThingsFragment.THINGS_FRAGMENT_TYPE_NORMAL, null, HomeFragment.TabType.TAB_TYPE_DEVICE, false);
                } else {
                    ThingsFragment.openFragment(true, ThingsFragment.THINGS_FRAGMENT_TYPE_NORMAL, null);
                }
            }
        }

    };
}
