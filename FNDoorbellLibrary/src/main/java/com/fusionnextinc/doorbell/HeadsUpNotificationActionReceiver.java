package com.fusionnextinc.doorbell;

import android.app.NotificationManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;

import com.fusionnextinc.doorbell.utils.FNLog;


public class HeadsUpNotificationActionReceiver extends BroadcastReceiver {
    @Override
    public void onReceive(Context context, Intent intent) {
        if (intent != null && intent.getExtras() != null) {
            FNLog.d("HeadsUpNotificationActionReceiver", "onReceive: have data" );
            String action = intent.getStringExtra(MyApplication.CALL_RESPONSE_ACTION_KEY);
            String click = intent.getStringExtra(MyApplication.CALL_END_ACTION);
            Bundle data = intent.getBundleExtra(MyApplication.FCM_DATA_KEY);

            if (action != null) {
                performClickAction(context, action, data);
             }

            // Close the notification after the click action is performed.

            Intent it = new Intent(Intent.ACTION_CLOSE_SYSTEM_DIALOGS);
            context.sendBroadcast(it);
            context.stopService(new Intent(context, HeadsUpNotificationService.class));
        }
    }

    private void performClickAction(Context context, String action, Bundle data) {

        if( MyFirebaseMessagingService.getFlage() )
        {
            MyFirebaseMessagingService.cleanFlage();
            return;
        }
        if(data != null) {
            data.putString(MyFirebaseMessagingService.NOTIFY_CLICK, "null");
        }

        if (action.equals(MyApplication.CALL_RECEIVE_ACTION) && data != null) {
            Intent openIntent = null;

            openIntent = new Intent(context, MainActivity.class);
            openIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            openIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            openIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
            openIntent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
            data.putString(MyFirebaseMessagingService.NOTIFY_CLICK, "action");
            openIntent.putExtras(data);//將Bundle物件傳給intent

            context.startActivity(openIntent);
        } else //if (action.equals(MyApplication.CALL_CANCEL_ACTION))
        {
            if(action.equals(MyApplication.CALL_CANCEL_ACTION)) {
                data.putString(MyFirebaseMessagingService.NOTIFY_CLICK, "cancel");
            }
            context.stopService(new Intent(context, HeadsUpNotificationService.class));
            Intent it = new Intent(Intent.ACTION_CLOSE_SYSTEM_DIALOGS);
            context.sendBroadcast(it);
        }
    }
}
