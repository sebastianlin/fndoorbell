package com.fusionnextinc.doorbell.fragement;

import android.content.Context;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;

import com.fusionnextinc.doorbell.MyApplication;
import com.fusionnextinc.doorbell.R;
import com.fusionnextinc.doorbell.manager.account.AaccountHandler;
import com.fusionnextinc.doorbell.manager.account.AccountManager;
import com.fusionnextinc.doorbell.objects.FNError;
import com.fusionnextinc.doorbell.utils.Utils;
import com.fusionnextinc.doorbell.widget.FNDialog;
import com.fusionnextinc.doorbell.widget.FNLayoutUtil;
import com.fusionnextinc.doorbell.widget.FNProgressBarCustom;

/**
 * Created by Mike Chang on 2017/6/22
 */
public class ForgotPwdFragment extends FNFragment implements View.OnClickListener {
    private static final String TAG = ForgotPwdFragment.class.getSimpleName();

    private FNLayoutUtil layoutUtil;
    private EditText editAccount;
    private FNProgressBarCustom progressBar;
    private AccountManager accountManager;
    private String email = "";

    public static void openFragment(boolean clearOtherStack) {
        FNFragmentManager.getInstance().openFragment(new ForgotPwdFragment(), clearOtherStack);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        layoutUtil = new FNLayoutUtil(getActivity(), MyApplication.LAYOUT_WIDTH, MyApplication.LAYOUT_HEIGHT, FNLayoutUtil.BASE_WIDTH);
        accountManager = AccountManager.getInstance(getActivity());
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        final ViewGroup nullParent = null;

        getActivity().setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        getActionBar().resetAll();
        getActionBar().setTitle(getString(R.string.fn_title_forgot_password), Gravity.CENTER, null);
        getActionBar().setCoverMode(false);
        getActionBar().setStartImage(R.drawable.back, new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        getActionBar().show();
        View view = inflater.inflate(R.layout.fragment_forgot_password, nullParent);
        layoutUtil.fitAllView(view);

        editAccount = (EditText) view.findViewById(R.id.et_account);
        Button btnSend = (Button) view.findViewById(R.id.btn_send);
        progressBar = new FNProgressBarCustom(getActivity(), (ViewGroup) view.findViewById(R.id.fragment_forgot_password));

        btnSend.setOnClickListener(this);
        return view;
    }

    @Override
    public void onClick(View view) {
        if (Utils.isNetworkConnected(getActivity())) {
            if (view.getId() == R.id.btn_send) {
                email = editAccount.getText().toString().trim();

                // must be a check for not null, for empty field and for email validity(matchs email pattern)
                if (Utils.emptyFieldsRemaining(editAccount)) {
                    FNDialog.showErrorDialog(getActivity(), getString(R.string.fn_msg_forgot_password_failed), getString(R.string.fn_msg_login_fields_are_miss));
                    return;
                }

                if (!Utils.emailIsValid(email)) {
                    FNDialog.showErrorDialog(getActivity(), getString(R.string.fn_msg_forgot_password_failed), getString(R.string.fn_msg_login_email_is_invaild));
                    return;
                }

                if (email != null && email.length() > 0) {
                    startWait();
                    accountManager.setAccountListener(accountListener);
                    accountManager.resetUserPassword(email);
                }
            }
        } else {
            FNDialog.showNoNetworkDialog(getActivity());
        }
    }

    private void closeKeyboard() {
        if (getActivity().getCurrentFocus() != null && getActivity().getCurrentFocus().getWindowToken() != null) {
            InputMethodManager inputManager = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
            inputManager.hideSoftInputFromWindow(getActivity().getCurrentFocus().getWindowToken(), 0);
        }
    }

    private void startWait() {
        progressBar.show();
    }


    private void stopWait() {
        progressBar.hide();
    }

    final AaccountHandler.AccountListener accountListener = new AaccountHandler.AccountListener() {

        @Override
        public void onUserRegistered(AccountManager instance, boolean result, FNError error) {

        }

        @Override
        public void onUserLogined(AccountManager instance, boolean result, FNError error) {

        }

        @Override
        public void onGetUserInfo(AccountManager instance, boolean result, FNError error) {

        }

        @Override
        public void onUpdateUserInfo(AccountManager instance, boolean result, FNError error) {

        }

        @Override
        public void onUserLogout(AccountManager instance, boolean result, FNError error) {

        }

        @Override
        public void onSetPassword(AccountManager instance, boolean result, FNError error) {

        }

        @Override
        public void onResetPassword(AccountManager instance, boolean result, FNError error) {
            if (isAdded()) {
                closeKeyboard();
                stopWait();
                if (result) {
                    FNDialog.showSuccessDialog(getActivity(),  getString(R.string.fn_title_forgot_password), getString(R.string.fn_msg_forgot_password_successfully));
                    ResetPasswordFragment.openFragment(true, email);
                } else {
                    FNDialog.showErrorDialog(getActivity(), getString(R.string.fn_msg_forgot_password_failed), error);
                }
            }
        }
    };
}
