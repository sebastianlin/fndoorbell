package com.fusionnextinc.doorbell.objects;


import com.fusionnext.cloud.datalink.json.FNJsonArray;
import com.fusionnext.cloud.datalink.json.FNJsonObject;

import org.json.JSONArray;
import org.json.JSONException;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;

/**
 * Created by Mike Chang on 2018/8/10
 */

public class FNSchedule {
    protected final static String TAG = "FNSchedule";

    private int mScheduleId;
    private boolean mIsDeviceOn;
    private boolean mIsSwitchOn;
    private int mHour;
    private int mMin;
    private int mUTCHour;
    private int mUTCMin;
    private int mSec;
    private ArrayList<Integer> mWeeks;
    private String mStrWeeks;

    public FNSchedule(int sceduleId, boolean isDeviceOn, boolean isSwitchOn, int hour, int min, int sec, ArrayList<Integer> weeks) {
        mScheduleId = sceduleId;
        mIsDeviceOn = isDeviceOn;
        mIsSwitchOn = isSwitchOn;
        mHour = hour;
        mMin = min;
        mSec = sec;
        mWeeks = weeks;
        mStrWeeks = toStrWeeks(weeks);
    }

    public FNSchedule(int sceduleId, boolean isDeviceOn, boolean isSwitchOn, int hour, int min, int sec, String strWeeks) {
        mScheduleId = sceduleId;
        mIsDeviceOn = isDeviceOn;
        mIsSwitchOn = isSwitchOn;
        mHour = hour;
        mMin = min;
        mSec = sec;
        mWeeks = parseWeeks(strWeeks);
        mStrWeeks = strWeeks;
    }

    public void clean() {
        mScheduleId = 0;
        mIsDeviceOn = false;
        mIsSwitchOn = true;
        mHour = 0;
        mMin = 0;
        mSec = 0;
        mUTCHour = 0;
        mUTCMin = 0;
    }

    public int getScheduleId() {
        return mScheduleId;
    }

    public boolean isDeviceOn() {
        return mIsDeviceOn;
    }

    public boolean isSwitchOn() {
        return mIsSwitchOn;
    }

    public void setSwitchOn(boolean isSwitchOn) {
        mIsSwitchOn = isSwitchOn;
    }

    public int getHour() {
        return mHour;
    }

    public int getMin() {
        return mMin;
    }

    public int getSec() {
        return mSec;
    }

    public int getUTCHour() {
        Local2UTC();
        return mUTCHour;
    }

    public int getUTCMin() {
        Local2UTC();
        return mUTCMin;
    }

    public ArrayList<Integer> getWeeks() {
        return mWeeks;
    }

    public String getStrWeeks() {
        return mStrWeeks;
    }

    public JSONArray getWeeksJsonArray() {
        JSONArray weeksJsonArray = new JSONArray();

        if (mWeeks != null) {
            calculateUTCWeek();
            for (int week : mWeeks) {
                weeksJsonArray.put(week);
            }
        }
        return weeksJsonArray;
    }

    private static ArrayList<Integer> parseWeeks(String strWeeks) {
        ArrayList<Integer> weeks = new ArrayList<>();
        String[] week = null;

        if (strWeeks.length() > 0) {
            week = strWeeks.split(",");
            for (int i = 0; i < week.length; i++) {
                weeks.add(Integer.valueOf(week[i]));
            }
        }

        return weeks;
    }

    private String toStrWeeks(ArrayList<Integer> weeks) {
        String strWeeks = "";

        for (Integer week : weeks) {
            strWeeks = strWeeks + String.valueOf(week) + ",";
        }
        return strWeeks;
    }

    /**
     * 当地时间 ---> UTC时间
     *
     * @return
     */
    private void Local2UTC() {
        Calendar calendar = Calendar.getInstance();
        TimeZone timeZone = TimeZone.getDefault();
        SimpleDateFormat sdf = new SimpleDateFormat("HH:mm:ss");
        sdf.setTimeZone(TimeZone.getTimeZone("gmt"));
        String gmtTime = sdf.format(new Date(calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH) + 1, calendar.get(Calendar.DAY_OF_MONTH), mHour, mMin, 0));
        String[] utcTime = gmtTime.split(":");

        mUTCHour = Integer.valueOf(utcTime[0]);
        mUTCMin = Integer.valueOf(utcTime[1]);
        return;
    }

    private void calculateUTCWeek() {
        TimeZone timeZone = TimeZone.getDefault();

        int offsetFromUtc = (timeZone.getRawOffset() + timeZone.getDSTSavings()) / 1000;
        int utcTimeZone = (mMin + mHour * 60) - offsetFromUtc / 60;
        if (utcTimeZone < 0) {
            if (mWeeks != null) {
                int i = 0;
                for (int week : mWeeks) {
                    week = week - 1;
                    if (week == 0) {
                        week = 7;
                    }
                    mWeeks.set(i, week);
                    i++;
                }
            }
        } else if (utcTimeZone > 24 * 60) {
            if (mWeeks != null) {
                int i = 0;
                for (int week : mWeeks) {
                    week = week + 1;
                    if (week == 8) {
                        week = 1;
                    }
                    mWeeks.set(i, week);
                    i++;
                }
            }
        }
        return;
    }

    private static void calculateLocalWeek(int hour, int min, ArrayList<Integer> weeks) {
        TimeZone timeZone = TimeZone.getDefault();

        int offsetFromUtc = (timeZone.getRawOffset() + timeZone.getDSTSavings()) / 1000;
        int utcTimeZone = (min + hour * 60) - offsetFromUtc / 60;
        if (utcTimeZone < 0) {
            if (weeks != null) {
                int i = 0;
                for (int week : weeks) {
                    week = week + 1;
                    if (week == 8) {
                        week = 1;
                    }
                    weeks.set(i, week);
                    i++;
                }
            }
        } else if (utcTimeZone > 24 * 60) {
            if (weeks != null) {
                int i = 0;
                for (int week : weeks) {
                    week = week - 1;
                    if (week == 0) {
                        week = 7;
                    }
                    weeks.set(i, week);
                    i++;
                }
            }
        }
        return;
    }

    private static String UTC2Local(int hour, int min, int sec) {
        String dateToReturn = hour + ":" + min + ":" + sec;
        SimpleDateFormat sdf = new SimpleDateFormat("HH:mm:ss");
        sdf.setTimeZone(TimeZone.getTimeZone("UTC"));
        Date gmt = null;
        SimpleDateFormat sdfOutPutToSend = new SimpleDateFormat("HH:mm:ss");
        sdfOutPutToSend.setTimeZone(TimeZone.getDefault());

        try {
            gmt = sdf.parse(dateToReturn);
            dateToReturn = sdfOutPutToSend.format(gmt);

        } catch (ParseException e) {
            e.printStackTrace();
        }
        return dateToReturn;
    }

    public static ArrayList<FNSchedule> parseScheduleArray(String value) {
        ArrayList<FNSchedule> fnScheduleList = new ArrayList<>();
        if (value == null) {
            return null;
        }
        try {
            FNJsonArray fnJsonArray = new FNJsonArray(value);
            for (int i = 0; i < fnJsonArray.length(); i++) {
                FNJsonObject jsonObject = fnJsonArray.getJSONObject(i);
                int sceduleId = jsonObject.getIntDefault("schedule_id");
                boolean isDeviceOn = jsonObject.getBooleanDefault("on", true);
                boolean isSwitchOn = jsonObject.getBooleanDefault("active", true);
                int hour = jsonObject.getIntDefault("h", 12);
                int min = jsonObject.getIntDefault("m", 0);
                int sec = jsonObject.getIntDefault("s", 0);
                String gmtTime = UTC2Local(hour, min, sec);
                String[] localTime = gmtTime.split(":");

                String weekdays = jsonObject.getString("weekdays").replace("[", "").replace("]", "");
                ArrayList<Integer> weeks = parseWeeks(weekdays);
                calculateLocalWeek(Integer.valueOf(localTime[0]), Integer.valueOf(localTime[1]), weeks);
                FNSchedule fnSchedule = new FNSchedule(sceduleId, isDeviceOn, isSwitchOn, Integer.valueOf(localTime[0]), Integer.valueOf(localTime[1]), Integer.valueOf(localTime[2]), weeks);
                fnScheduleList.add(fnSchedule);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return fnScheduleList;
    }
}
