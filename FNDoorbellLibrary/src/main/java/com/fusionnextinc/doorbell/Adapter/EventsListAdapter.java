package com.fusionnextinc.doorbell.Adapter;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.fusionnext.cloud.datalink.json.FNJsonObject;
import com.fusionnext.cloud.utils.Times;
import com.fusionnextinc.doorbell.MyApplication;
import com.fusionnextinc.doorbell.R;
import com.fusionnextinc.doorbell.manager.data.AppDataThingEvent;
import com.fusionnextinc.doorbell.manager.data.ListMap;
import com.fusionnextinc.doorbell.widget.FNHeaderListView.FNListViewAdapter;
import com.fusionnextinc.doorbell.widget.FNLayoutUtil;

import org.json.JSONException;

import java.util.ArrayList;
import java.util.Map;
import java.util.TimeZone;

/**
 * Created by Mike Chang on 2017/7/11
 */
public class EventsListAdapter extends FNListViewAdapter {
    private static final String TAG = "EventsListAdapter";

    private Context context;
    private LayoutInflater mInflater;
    private FNLayoutUtil layoutUtil;
    private ListMap eventsListMap = new ListMap();
    private ArrayList<ListMap> eventslistMapArray = new ArrayList<>();
    private int layoutID;
    private final int EVENT_USERNAME_LENGTH_LITMIT = 10;

    public EventsListAdapter(Context context, ListMap listMap, int layoutID) {
        this.context = context;
        this.mInflater = LayoutInflater.from(context);
        this.eventsListMap.addAll(listMap);
        this.eventslistMapArray = toDateArray(eventsListMap);
        this.layoutID = layoutID;
    }

    private static class sectionHolder {
        //ID, its invisible
        private TextView textSection;
        private LinearLayout sectionLayout;
    }

    private static class ViewHolder {
        //ID, its invisible
        private TextView textId;
        private RelativeLayout bgLayout;
        //Trip information
        private TextView textDate;
        private TextView textName;
    }

    public void updateAdapterData(final int type, final String thingId) {
        eventsListMap.clear();
        eventsListMap.addAll(AppDataThingEvent.getInstance(context).getAdapterData(type, thingId));
        eventslistMapArray.clear();
        eventslistMapArray = toDateArray(eventsListMap);
        notifyDataSetChanged();
    }

    @Override
    public boolean isEnabled(int section, int position) {
        return true;
    }

    @Override
    public Object getItem(int section, int position) {
        if (section < 0 || position < 0 || eventsListMap == null || position >= eventsListMap.size() || eventsListMap.get(position) == null) {
            return null;
        }
        return eventsListMap.get(position);
    }

    @Override
    public long getItemId(int section, int position) {
        return position;
    }

    @Override
    public int getSectionCount() {
        if (eventslistMapArray == null) return 0;
        return eventslistMapArray.size();
    }

    @Override
    public int getCountForSection(int section) {
        if (eventslistMapArray == null) {
            return 0;
        } else {
            if (eventslistMapArray.get(section) != null) {
                return eventslistMapArray.get(section).size();
            }
        }
        return 0;
    }

    @Override
    public View getItemView(int section, int position, View convertView, ViewGroup parent) {
        //Ready the ViewHolder
        Map<String, Object> map = null;
        ViewHolder holder;

        if (convertView == null) {
            holder = new ViewHolder();
            layoutUtil = new FNLayoutUtil((Activity) context, MyApplication.LAYOUT_WIDTH, MyApplication.LAYOUT_HEIGHT, FNLayoutUtil.BASE_WIDTH);
            convertView = mInflater.inflate(layoutID, null);
            holder.bgLayout = convertView.findViewById(R.id.adapter_events_list);
            layoutUtil.fitAllView(holder.bgLayout);
            holder.textId = (TextView) convertView.findViewById(R.id.textEventId);
            holder.textDate = (TextView) convertView.findViewById(R.id.textEventTime);
            holder.textName = (TextView) convertView.findViewById(R.id.textEventName);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        if (eventslistMapArray != null) {
            ListMap listMap = eventslistMapArray.get(section);
            if (listMap != null) {
                map = listMap.get(position);
            }
        }
        updateEventsView(holder, map);
        return convertView;
    }

    @Override
    public View getSectionHeaderView(int section, View convertView, ViewGroup parent) {
        sectionHolder holder;
        final ViewGroup nullParent = null;
        if (convertView == null) {
            holder = new sectionHolder();
            layoutUtil = new FNLayoutUtil((Activity) context, MyApplication.LAYOUT_WIDTH, MyApplication.LAYOUT_HEIGHT, FNLayoutUtil.BASE_WIDTH);
            convertView = mInflater.inflate(R.layout.thing_section_item, nullParent);
            holder.sectionLayout = (LinearLayout) convertView.findViewById(R.id.thing_section_item);
            layoutUtil.fitAllView(holder.sectionLayout);
            holder.textSection = (TextView) convertView.findViewById(R.id.textSection);
            convertView.setTag(holder);
        } else {
            holder = (sectionHolder) convertView.getTag();
        }
        if (eventslistMapArray != null) {
            ListMap listMap = eventslistMapArray.get(section);
            if (listMap != null) {
                Map<String, Object> map = listMap.get(0);
                if (map != null) {


                    updateTextView(holder.textSection, map, AppDataThingEvent.TAG_EVENT_CREATED_AT);
                }
            }
        }
        return convertView;
    }

    private void updateEventsView(final ViewHolder holder, final Map<String, Object> map) {
        updateTextView(holder.textId, map, AppDataThingEvent.TAG_EVENT_ID);
        updateTextView(holder.textDate, map, AppDataThingEvent.TAG_EVENT_CREATED_AT);
        updateTextView(holder.textName, map, AppDataThingEvent.TAG_EVENT_NAME);
    }

    private void updateTextView(TextView text, Map<String, Object> map, String tag) {
        String strText = "";
        Object object = map.get(tag);
        if (object != null) {
            if (object instanceof Integer) strText = Integer.toString((Integer) object);
            else if (object instanceof String) strText = (String) object;
        }


        if (text.getId() == R.id.textSection) {
            strText = Times.formatTransfer(strText, Times._TIME_FORMAT_STANDARD, Times._TIME_FORMAT_STANDARD_DATE, TimeZone.getDefault());
        } else if (text.getId() == R.id.textEventTime) {
            strText = Times.formatTransfer(strText, Times._TIME_FORMAT_STANDARD, Times._TIME_FORMAT_HOUR_MIN_SEC, TimeZone.getDefault());
        } else if (text.getId() == R.id.textEventName) {
            if (strText.equals("call")) {
                strText = text.getContext().getString(R.string.txt_call_incoming_call);
                text.setTextColor(text.getContext().getResources().getColor(R.color.main_text_level1));
            } else if (strText.equals("hang_up")) {
                strText = text.getContext().getString(R.string.txt_call_hang_up);
                text.setTextColor(text.getContext().getResources().getColor(R.color.red));
            } else if (strText.equals("answer")) {
                Object payloadObject = map.get(AppDataThingEvent.TAG_EVENT_PAYLOAD);
                String payloadStr = null;
                String userName = null;
                if (payloadObject != null) {
                    if (payloadObject instanceof Integer)
                        payloadStr = Integer.toString((Integer) payloadObject);
                    else if (payloadObject instanceof String) payloadStr = (String) payloadObject;

                    if (payloadStr != null) {
                        try {
                            FNJsonObject jsonObject = new FNJsonObject(payloadStr);
                            userName = jsonObject.getStringDefault("user_name");
                            userName = userName.length() >= EVENT_USERNAME_LENGTH_LITMIT ?
                                    userName.substring(0, EVENT_USERNAME_LENGTH_LITMIT - 1) + "..." : userName;
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }
                strText = userName != null ? userName + " " + text.getContext().getString(R.string.txt_call_accepted) :
                        text.getContext().getString(R.string.txt_call_accepted);
                text.setTextColor(text.getContext().getResources().getColor(R.color.main_bg_level1));
            }
        }
        text.setText(strText);
    }

    private ArrayList<ListMap> toDateArray(ListMap listMap) {
        Map<String, Object> map;
        String date;
        String oldDate = "";
        ArrayList<ListMap> listMapArray = new ArrayList<>();
        // int whatDay = 1;
        ListMap tmpListMap = null;

        for (int i = 0; i < listMap.size(); i++) {
            map = listMap.get(i);
            Object object = map.get(AppDataThingEvent.TAG_EVENT_CREATED_AT);
            if (object != null) {
                if (object instanceof String) {
                    date = (String) object;
                    date = Times.formatTransfer(date, Times._TIME_FORMAT_STANDARD, Times._TIME_FORMAT_STANDARD_DATE, TimeZone.getDefault());
                    if (oldDate.length() <= 0) {
                        tmpListMap = new ListMap();
                        //map.put(_TAG_PHOTO_WHAT_DAY, context.getString(R.string.string_photo_day, whatDay));
                        tmpListMap.add(map);
                        if (i == listMap.size() - 1) {
                            listMapArray.add(tmpListMap);
                        }
                    } else if (date.equals(oldDate)) {
                        //map.put(_TAG_PHOTO_WHAT_DAY, context.getString(R.string.string_photo_day, whatDay));
                        if (tmpListMap != null) {
                            tmpListMap.add(map);
                            if (i == listMap.size() - 1) {
                                listMapArray.add(tmpListMap);
                            }
                        }
                    } else {
                        listMapArray.add(tmpListMap);
                        tmpListMap = new ListMap();
                        //whatDay = whatDay + Times.getDateDays(date, oldDate);
                        //map.put(_TAG_PHOTO_WHAT_DAY, context.getString(R.string.string_photo_day, whatDay));
                        tmpListMap.add(map);
                        if (i == listMap.size() - 1) {
                            listMapArray.add(tmpListMap);
                        }
                    }
                    oldDate = date;
                }
            }
        }
        return listMapArray;
    }

}


