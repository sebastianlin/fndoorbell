package com.fusionnext.cloud.iot;

import android.content.Context;

import com.fusionnext.cloud.datalink.http.HttpDelete;
import com.fusionnext.cloud.datalink.http.HttpGet;
import com.fusionnext.cloud.datalink.http.HttpParam;
import com.fusionnext.cloud.datalink.http.HttpPost;
import com.fusionnext.cloud.datalink.http.HttpPut;
import com.fusionnext.cloud.datalink.json.FNJsonObject;
import com.fusionnext.cloud.iot.objects.ErrorObj;
import com.fusionnext.cloud.iot.objects.Event;
import com.fusionnext.cloud.iot.objects.Setting;
import com.fusionnext.cloud.iot.objects.State;
import com.fusionnext.cloud.iot.objects.Thing;
import com.fusionnext.cloud.iot.objects.UserProfile;

import org.json.JSONException;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by Mike Chang on 2017/3/27
 */
public class Things {
    protected final static String _TAG = "Things";
    private Context context;

    public Things(Context context) {
        this.context = context;
    }

    public void createThing(Thing thing, final IOTHandler.OnThingsListener onThingsListener) {
        if (thing == null) {
            return;
        }
        if (thing._productId == null && thing._productSlug == null) {
            return;
        }
        HttpPost.PostParam[] params = new HttpPost.PostParam[1];
        params[0] = new HttpPost.PostParam();
        params[0].accessToken = Public.getAuthorization(context).getUserAccessToken();
        params[0].url = HttpData.generateHttpPostUrl(HttpData._POST_TYPE_THINGS_CREATE_THING);
        params[0].type = HttpData._POST_TYPE_THINGS_CREATE_THING;
        params[0].param = new HashMap<>();
        if (thing._productId != null) {
            params[0].param.put("product_id", thing._productId);
        }
        if (thing._productSlug != null) {
            params[0].param.put("product_id", thing._productSlug);
        }
        params[0].param.put("identifier", thing._identifier);
        params[0].param.put("name", thing._name);
        params[0].param.put("firmware_ver", thing._firmwareVer);

        HttpPost post = new HttpPost();
        post.setContext(context);
        post.setOnParseJsonListener(new HttpPost.OnParseJsonListener() {
            @Override
            public void onParse(boolean returnResult, int postType, String id, String returnStr) {
                Thing thing = null;
                ErrorObj errorObj = null;

                if (returnStr != null) {
                    if (returnResult) {
                        thing = Thing.parseThing(returnStr);
                    } else {
                        errorObj = ErrorObj.parseErrorObj(returnStr);
                    }
                }
                onThingsListener.onCreateThing(returnResult, thing, errorObj);
            }
        });
        post.execute(params);
    }

    public void updateThing(final String thingId, Thing thing, final IOTHandler.OnThingsListener onThingsListener) {
        if (thing == null) {
            return;
        }
        HttpPut.PutParam[] params = new HttpPut.PutParam[1];
        params[0] = new HttpPut.PutParam();
        params[0].accessToken = Public.getAuthorization(context).getUserAccessToken();
        params[0].url = HttpData.generateHttpPutUrl(HttpData._PUT_TYPE_THINGS_UPDATE_THING, thingId);
        params[0].type = HttpData._PUT_TYPE_THINGS_UPDATE_THING;
        params[0].param = new HashMap<>();
        params[0].param.put("name", thing._name);
        params[0].param.put("firmware_ver", thing._firmwareVer);

        HttpPut put = new HttpPut();
        put.setHttpRequestType("PUT");
        put.setOnParseJsonListener(new HttpPut.OnParseJsonListener() {
            @Override
            public void onParse(boolean returnResult, int putType, String returnStr) {
                Thing thing = null;
                ErrorObj errorObj = null;

                if (returnStr != null) {
                    if (returnResult) {
                        thing = Thing.parseThing(returnStr);
                    } else {
                        errorObj = ErrorObj.parseErrorObj(returnStr);
                    }
                }
                onThingsListener.onUpdateThing(returnResult, thing, errorObj);
            }
        });
        put.setContext(context);
        put.execute(params);
    }

    public void getThing(final String thingId, final IOTHandler.OnThingsListener onThingsListener) {
        HttpParam.HttpSubParam param = new HttpParam.HttpSubParam();

        param.accessToken = Public.getAuthorization(context).getUserAccessToken();
        param.url = HttpData.generateHttpGetUrl(HttpData._GET_TYPE_THINGS_GET_THING, thingId, null, HttpData._GET_TYPE_SORT_ORDER_DESC, null, null);
        param.cacheName = "";
        HttpParam[] params = new HttpParam[1];
        params[0] = new HttpParam();
        params[0].params.clear();
        params[0].params.add(param);

        HttpGet httpGet = new HttpGet();
        httpGet.setContext(context);
        httpGet.setTag(HttpGet._HTTP_T_GET_NORMAL_TYPE);
        httpGet.setOnParseJsonListener(new HttpGet.OnParseJsonListener() {
            @Override
            public void onParse(boolean result, String returnStr) {
                Thing thing = null;
                ErrorObj errorObj = null;

                if (returnStr != null) {
                    if (result) {
                        thing = Thing.parseThing(returnStr);
                    } else {
                        errorObj = ErrorObj.parseErrorObj(returnStr);
                    }
                }
                onThingsListener.onGetThing(result, thing, errorObj);
            }
        });
        httpGet.execute(params);
    }

    public void deleteThing(final String thingId, final IOTHandler.OnThingsListener onThingsListener) {
        HttpDelete.DelParam[] params = new HttpDelete.DelParam[1];
        params[0] = new HttpDelete.DelParam();
        params[0].accessToken = Public.getAuthorization(context).getUserAccessToken();
        params[0].url = HttpData.generateHttpDeleteUrl(HttpData._DEL_TYPE_THINGS_DELETE_THING, thingId);
        params[0].type = HttpData._DEL_TYPE_THINGS_DELETE_THING;
        params[0].param = new HashMap<>();

        HttpDelete del = new HttpDelete();
        del.setContext(context);
        del.setOnParseJsonListener(new HttpDelete.OnParseJsonListener() {
            @Override
            public void onParse(boolean returnResult, int delType, String returnStr) {
                ErrorObj errorObj = null;

                if (returnStr != null && !returnResult) {
                    errorObj = ErrorObj.parseErrorObj(returnStr);
                }
                onThingsListener.onDeleteThing(returnResult, errorObj);
            }
        });
        del.execute(params);
    }

    public void listEvents(final String thingId, final String name, int order, String pageSize, String pageNum, final IOTHandler.OnThingsListener onThingsListener) {
        HttpParam.HttpSubParam param = new HttpParam.HttpSubParam();

        param.accessToken = Public.getAuthorization(context).getUserAccessToken();
        param.url = HttpData.generateHttpGetUrl(HttpData._GET_TYPE_THINGS_LIST_EVENTS, thingId, name, order, pageSize, pageNum);
        param.cacheName = "";
        HttpParam[] params = new HttpParam[1];
        params[0] = new HttpParam();
        params[0].params.clear();
        params[0].params.add(param);

        HttpGet httpGet = new HttpGet();
        httpGet.setContext(context);
        httpGet.setTag(HttpGet._HTTP_T_GET_NORMAL_TYPE);
        httpGet.setOnParseJsonListener(new HttpGet.OnParseJsonListener() {
            @Override
            public void onParse(boolean result, String returnStr) {
                ArrayList<Event> eventList = null;
                ErrorObj errorObj = null;

                if (returnStr != null) {
                    if (result) {
                        eventList = Event.parseEventArray(returnStr);
                    } else {
                        errorObj = ErrorObj.parseErrorObj(returnStr);
                    }
                }
                onThingsListener.onListEvents(result, thingId, name, eventList, errorObj);
            }
        });
        httpGet.execute(params);
    }

    public void listEvents(final String thingId, final String name, final String startTime, final String endTime, int order, String pageSize, String pageNum, final IOTHandler.OnThingsListener onThingsListener) {
        HttpParam.HttpSubParam param = new HttpParam.HttpSubParam();

        param.accessToken = Public.getAuthorization(context).getUserAccessToken();
        param.url = HttpData.generateHttpGetUrl(HttpData._GET_TYPE_THINGS_LIST_EVENTS_BY_TIME, thingId, name, startTime, endTime, order, pageSize, pageNum);
        param.cacheName = "";
        HttpParam[] params = new HttpParam[1];
        params[0] = new HttpParam();
        params[0].params.clear();
        params[0].params.add(param);

        HttpGet httpGet = new HttpGet();
        httpGet.setContext(context);
        httpGet.setTag(HttpGet._HTTP_T_GET_NORMAL_TYPE);
        httpGet.setOnParseJsonListener(new HttpGet.OnParseJsonListener() {
            @Override
            public void onParse(boolean result, String returnStr) {
                ArrayList<Event> eventList = null;
                ErrorObj errorObj = null;
                int eventCount = 0;

                if (returnStr != null) {
                    if (result) {
                        eventList = Event.parseEventArray(returnStr);
                        eventCount = Event.getCountByEventArray(returnStr);
                    } else {
                        errorObj = ErrorObj.parseErrorObj(returnStr);
                    }
                }
                onThingsListener.onListEventsByTime(result, thingId, name, startTime, endTime, eventList, eventCount, errorObj);
            }
        });
        httpGet.execute(params);
    }

    public void invite(final String thingId, String account, final IOTHandler.OnThingsListener onThingsListener) {
        if (account == null || thingId == null) {
            return;
        }

        HttpPost.PostParam[] params = new HttpPost.PostParam[1];
        params[0] = new HttpPost.PostParam();
        params[0].accessToken = Public.getAuthorization(context).getUserAccessToken();
        params[0].url = HttpData.generateHttpPostUrl(HttpData._POST_TYPE_THINGS_INVITE, thingId);
        params[0].type = HttpData._POST_TYPE_THINGS_INVITE;
        params[0].param = new HashMap<>();
        params[0].param.put("email", account);

        HttpPost post = new HttpPost();
        post.setContext(context);
        post.setOnParseJsonListener(new HttpPost.OnParseJsonListener() {
            @Override
            public void onParse(boolean returnResult, int postType, String id, String returnStr) {
                ErrorObj errorObj = null;

                if (returnStr != null && !returnResult) {
                    errorObj = ErrorObj.parseErrorObj(returnStr);
                }
                onThingsListener.onInvite(returnResult, thingId, errorObj);
            }
        });
        post.execute(params);
    }

    public void accept(final String thingId, final IOTHandler.OnThingsListener onThingsListener) {
        if (thingId == null) {
            return;
        }
        HttpPut.PutParam[] params = new HttpPut.PutParam[1];
        params[0] = new HttpPut.PutParam();
        params[0].accessToken = Public.getAuthorization(context).getUserAccessToken();
        params[0].url = HttpData.generateHttpPutUrl(HttpData._PUT_TYPE_THINGS_ACCEPT_INVITATION, thingId);
        params[0].type = HttpData._PUT_TYPE_THINGS_ACCEPT_INVITATION;

        HttpPut put = new HttpPut();
        put.setHttpRequestType("PUT");
        put.setOnParseJsonListener(new HttpPut.OnParseJsonListener() {
            @Override
            public void onParse(boolean returnResult, int putType, String returnStr) {
                ErrorObj errorObj = null;

                if (returnStr != null) {
                    if (returnResult) {
                        Thing.parseThing(returnStr);
                    } else {
                        errorObj = ErrorObj.parseErrorObj(returnStr);
                    }
                }
                onThingsListener.onAccept(returnResult, thingId, errorObj);
            }
        });
        put.setContext(context);
        put.execute(params);
    }

    public void reject(final String thingId, String account, final IOTHandler.OnThingsListener onThingsListener) {
        if (thingId == null) {
            return;
        }
        HttpDelete.DelParam[] params = new HttpDelete.DelParam[1];
        params[0] = new HttpDelete.DelParam();
        params[0].accessToken = Public.getAuthorization(context).getUserAccessToken();
        params[0].url = HttpData.generateHttpDeleteUrl(HttpData._DEL_TYPE_THINGS_REJECT_INVITATION, thingId);
        params[0].type = HttpData._DEL_TYPE_THINGS_REJECT_INVITATION;
        params[0].param = new HashMap<>();
        if (account != null) {
            params[0].param.put("email", account);
        }

        HttpDelete del = new HttpDelete();
        del.setContext(context);
        del.setOnParseJsonListener(new HttpDelete.OnParseJsonListener() {
            @Override
            public void onParse(boolean returnResult, int delType, String returnStr) {
                ErrorObj errorObj = null;

                if (returnStr != null && !returnResult) {
                    errorObj = ErrorObj.parseErrorObj(returnStr);
                }
                onThingsListener.onReject(returnResult, thingId, errorObj);
            }
        });
        del.execute(params);
    }

    public void listStates(final String thingId, String name, int order, String pageSize, String pageNum, final IOTHandler.OnThingsListener onThingsListener) {
        HttpParam.HttpSubParam param = new HttpParam.HttpSubParam();

        param.accessToken = Public.getAuthorization(context).getUserAccessToken();
        param.url = HttpData.generateHttpGetUrl(HttpData._GET_TYPE_THINGS_LIST_STATES, thingId, name, order, pageSize, pageNum);
        param.cacheName = "";
        HttpParam[] params = new HttpParam[1];
        params[0] = new HttpParam();
        params[0].params.clear();
        params[0].params.add(param);

        HttpGet httpGet = new HttpGet();
        httpGet.setContext(context);
        httpGet.setTag(HttpGet._HTTP_T_GET_NORMAL_TYPE);
        httpGet.setOnParseJsonListener(new HttpGet.OnParseJsonListener() {
            @Override
            public void onParse(boolean result, String returnStr) {
                ArrayList<State> stateList = null;
                ErrorObj errorObj = null;

                if (returnStr != null) {
                    if (result) {
                        stateList = State.parseStateArray(returnStr);
                    } else {
                        errorObj = ErrorObj.parseErrorObj(returnStr);
                    }
                }
                onThingsListener.onListStates(result, thingId, stateList, errorObj);
            }
        });
        httpGet.execute(params);
    }

    public void listSettings(final String thingId, final String name, int order, String pageSize, String pageNum, final IOTHandler.OnThingsListener onThingsListener) {
        HttpParam.HttpSubParam param = new HttpParam.HttpSubParam();

        param.accessToken = Public.getAuthorization(context).getUserAccessToken();
        param.url = HttpData.generateHttpGetUrl(HttpData._GET_TYPE_THINGS_LIST_SETTINGS, thingId, name, order, pageSize, pageNum);
        param.cacheName = "";
        HttpParam[] params = new HttpParam[1];
        params[0] = new HttpParam();
        params[0].params.clear();
        params[0].params.add(param);

        HttpGet httpGet = new HttpGet();
        httpGet.setContext(context);
        httpGet.setTag(HttpGet._HTTP_T_GET_NORMAL_TYPE);
        httpGet.setOnParseJsonListener(new HttpGet.OnParseJsonListener() {
            @Override
            public void onParse(boolean result, String returnStr) {
                ArrayList<Setting> settingList = null;
                ErrorObj errorObj = null;

                if (returnStr != null) {
                    if (result) {
                        settingList = Setting.parseSettingArray(returnStr);
                    } else {
                        errorObj = ErrorObj.parseErrorObj(returnStr);
                    }
                }
                onThingsListener.onListSettings(result, name, thingId, settingList, errorObj);
            }
        });
        httpGet.execute(params);
    }

    public void listInvitedUsers(final String thingId, int order, String pageSize, String pageNum, final IOTHandler.OnThingsListener onThingsListener) {
        HttpParam.HttpSubParam param = new HttpParam.HttpSubParam();

        param.accessToken = Public.getAuthorization(context).getUserAccessToken();
        param.url = HttpData.generateHttpGetUrl(HttpData._GET_TYPE_THINGS_LIST_INVITED_USERS, thingId, null, order, pageSize, pageNum);
        param.cacheName = "";
        HttpParam[] params = new HttpParam[1];
        params[0] = new HttpParam();
        params[0].params.clear();
        params[0].params.add(param);

        HttpGet httpGet = new HttpGet();
        httpGet.setContext(context);
        httpGet.setTag(HttpGet._HTTP_T_GET_NORMAL_TYPE);
        httpGet.setOnParseJsonListener(new HttpGet.OnParseJsonListener() {
            @Override
            public void onParse(boolean result, String returnStr) {
                ArrayList<UserProfile> userList = null;
                ErrorObj errorObj = null;

                if (returnStr != null) {
                    if (result) {
                        userList = UserProfile.parseUserProfileArray(returnStr);
                    } else {
                        errorObj = ErrorObj.parseErrorObj(returnStr);
                    }
                }
                onThingsListener.onListInvitedUsers(result, thingId, userList, errorObj);
            }
        });
        httpGet.execute(params);
    }

    public void listSharedUsers(final String thingId, int order, String pageSize, String pageNum, final IOTHandler.OnThingsListener onThingsListener) {
        HttpParam.HttpSubParam param = new HttpParam.HttpSubParam();

        param.accessToken = Public.getAuthorization(context).getUserAccessToken();
        param.url = HttpData.generateHttpGetUrl(HttpData._GET_TYPE_THINGS_LIST_SHARED_USERS, thingId, null, order, pageSize, pageNum);
        param.cacheName = "";
        HttpParam[] params = new HttpParam[1];
        params[0] = new HttpParam();
        params[0].params.clear();
        params[0].params.add(param);

        HttpGet httpGet = new HttpGet();
        httpGet.setContext(context);
        httpGet.setTag(HttpGet._HTTP_T_GET_NORMAL_TYPE);
        httpGet.setOnParseJsonListener(new HttpGet.OnParseJsonListener() {
            @Override
            public void onParse(boolean result, String returnStr) {
                ArrayList<UserProfile> userList = null;
                ErrorObj errorObj = null;

                if (returnStr != null) {
                    if (result) {
                        userList = UserProfile.parseUserProfileArray(returnStr);
                    } else {
                        errorObj = ErrorObj.parseErrorObj(returnStr);
                    }
                }
                onThingsListener.onListSharedUsers(result, thingId, userList, errorObj);
            }
        });
        httpGet.execute(params);
    }

    public void createClaimCode(String name, final IOTHandler.OnThingsListener onThingsListener) {
        HttpPost.PostParam[] params = new HttpPost.PostParam[1];
        params[0] = new HttpPost.PostParam();
        params[0].accessToken = Public.getAuthorization(context).getUserAccessToken();
        params[0].url = HttpData.generateHttpPostUrl(HttpData._POST_TYPE_THINGS_CREATE_CLAIMCODE);
        params[0].type = HttpData._POST_TYPE_THINGS_CREATE_CLAIMCODE;
        params[0].param = new HashMap<>();

        if (name != null) {
            params[0].param.put("thing_name", name);
        }

        HttpPost post = new HttpPost();
        post.setContext(context);
        post.setOnParseJsonListener(new HttpPost.OnParseJsonListener() {
            @Override
            public void onParse(boolean returnResult, int postType, String id, String returnStr) {
                String claimCode = null;
                ErrorObj errorObj = null;

                if (returnStr != null) {
                    if (returnResult) {
                        try {
                            FNJsonObject thingObject = new FNJsonObject(returnStr);
                            claimCode = thingObject.getStringDefault("code");
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    } else {
                        errorObj = ErrorObj.parseErrorObj(returnStr);
                    }
                }
                onThingsListener.onCreateClaimCode(returnResult, claimCode, errorObj);
            }
        });
        post.execute(params);
    }

    public void takeCallSlot(String callSlot, final IOTHandler.OnThingsListener onThingsListener) {
        if (callSlot == null) {
            return;
        }
        HttpPut.PutParam[] params = new HttpPut.PutParam[1];
        params[0] = new HttpPut.PutParam();
        params[0].accessToken = Public.getAuthorization(context).getUserAccessToken();
        params[0].url = HttpData.generateHttpPutUrl(HttpData._PUT_TYPE_THINGS_TAKE_CALL_SLOT, callSlot);
        params[0].type = HttpData._PUT_TYPE_THINGS_TAKE_CALL_SLOT;
        params[0].param = new HashMap<>();
        params[0].param.put("device_id", Public.getDevices(context).getDeviceId());

        HttpPut put = new HttpPut();
        put.setHttpRequestType("PUT");
        put.setOnParseJsonListener(new HttpPut.OnParseJsonListener() {
            @Override
            public void onParse(boolean returnResult, int putType, String returnStr) {
                Thing thing = null;
                ErrorObj errorObj = null;
                String thingId = null;
                String callSlot = null;

                if (returnStr != null) {
                    if (returnResult) {
                        try {
                            FNJsonObject callSlotObject = new FNJsonObject(returnStr);
                            thingId = callSlotObject.getStringDefault("thing_id");
                            callSlot = callSlotObject.getStringDefault("identifier");
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    } else {
                        errorObj = ErrorObj.parseErrorObj(returnStr);
                    }
                }
                onThingsListener.onTakeCallSlot(returnResult, thingId, callSlot, errorObj);
            }
        });
        put.setContext(context);
        put.execute(params);
    }

    public void closeCallSlot(String callSlot, final IOTHandler.OnThingsListener onThingsListener) {
        HttpDelete.DelParam[] params = new HttpDelete.DelParam[1];
        params[0] = new HttpDelete.DelParam();
        params[0].accessToken = Public.getAuthorization(context).getUserAccessToken();
        params[0].url = HttpData.generateHttpDeleteUrl(HttpData._DEL_TYPE_THINGS_CLOSE_CALL_SLOT, callSlot);
        params[0].type = HttpData._DEL_TYPE_THINGS_CLOSE_CALL_SLOT;
        params[0].param = new HashMap<>();

        HttpDelete del = new HttpDelete();
        del.setContext(context);
        del.setOnParseJsonListener(new HttpDelete.OnParseJsonListener() {
            @Override
            public void onParse(boolean returnResult, int delType, String returnStr) {
                ErrorObj errorObj = null;

                if (returnStr != null && !returnResult) {
                    errorObj = ErrorObj.parseErrorObj(returnStr);
                }
                onThingsListener.onCloseCallSlot(returnResult, errorObj);
            }
        });
        del.execute(params);
    }
}
