package com.fusionnext.cloud.iot;

import com.fusionnext.cloud.utils.FNNetworkLog;
import com.mediatek.demo.smartconnection.JniLoader;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.SocketTimeoutException;

/**
 * Created by Mike Chang on 2016/5/26.
 */
public class SmartConnection {
    protected final static String _TAG = "SmartConnection";
    private JniLoader elian = null;
    private boolean isLibLoaded = false;
    private Thread notifyThread;
    private String tempResponse;
    private String mClaimCode;
    private DatagramSocket mSocket = null;

    //Update Battery Level
    private static final int SMART_CONNECTION_INTERVAL = 5000;
    private Thread mSmartConnectionThread = null;
    private boolean mIsSmartConnectionThreadRunning = false;
    private boolean mIsSmartConnectionEnable = false;

    public interface OnSmartConnectionHandlerListener {
        void onStart(boolean success);

        void onStop(boolean success);
    }

    public SmartConnection() {
        isLibLoaded = JniLoader.LoadLib();
        if (!isLibLoaded) {
            FNNetworkLog.e(_TAG, "can't load elianjni lib");
        }
        elian = new JniLoader();
    }

    private void startSmartConnection(final String ssid, final String password, final String ClaimCode) {
        int result = -1;
        mClaimCode = ClaimCode;

        result = elian.InitSmartConnection("", null, 0, 0, 1);
        elian.SetSendInterval(10, 10);
        FNNetworkLog.i(_TAG, "startSmartConnectionTimer:InitSmartConnection: " + result + ", custom = " + ClaimCode);
        result = elian.StartSmartConnection(ssid, password, ClaimCode);
        mIsSmartConnectionEnable = true;
        FNNetworkLog.i(_TAG, "startSmartConnectionTimer:StartSmartConnection: " + result);
    }


    private void stopSmartConnectionTimer() {
        if (mSmartConnectionThread != null) {
            mClaimCode = "";
            mIsSmartConnectionThreadRunning = false;
            mSmartConnectionThread.interrupt();
        }
    }

    private void startSmartConnectionTimer(final String ssid, final String password, final String ClaimCode, final OnSmartConnectionHandlerListener listener) {
        if (mSmartConnectionThread == null) {
            mSmartConnectionThread = new Thread() {
                @Override
                public void run() {
                    while (mIsSmartConnectionThreadRunning) {
                        int result = -1;
                        result = elian.InitSmartConnection("", null, 0, 0, 1);
                        FNNetworkLog.i(_TAG, "startSmartConnectionTimer:InitSmartConnection: " + result + ", custom = " + ClaimCode);
                        elian.SetSendInterval(10, 10);
                        result = elian.StartSmartConnection(ssid, password, ClaimCode);
                        mIsSmartConnectionEnable = true;
                        if (listener != null) {
                            listener.onStart(result < 0 ? false : true);
                        }
                        FNNetworkLog.i(_TAG, "startSmartConnectionTimer:StartSmartConnection: " + result);
                        try {
                            Thread.sleep(SMART_CONNECTION_INTERVAL);
                        } catch (InterruptedException ignore) {
                        }
                        if (mIsSmartConnectionEnable) {
                            result = elian.StopSmartConnection();
                            mIsSmartConnectionEnable = false;
                            if (listener != null) {
                                listener.onStop(result < 0 ? false : true);
                            }
                            FNNetworkLog.i(_TAG, "startSmartConnectionTimer:StopSmartConnection: " + result);
                        }

                        try {
                            Thread.sleep(SMART_CONNECTION_INTERVAL);
                        } catch (InterruptedException ignore) {
                        }
                    }
                    mSmartConnectionThread = null;
                }
            };
            mIsSmartConnectionThreadRunning = true;
            mSmartConnectionThread.start();
        }
    }


    public int start(String ssid, String password, String ClaimCode, final OnSmartConnectionHandlerListener listener) {
        int result = -1;

        if (ssid == null || password == null || ClaimCode == null) {
            return result;
        } else if (elian != null && isLibLoaded) {

            startSmartConnectionTimer(ssid, password, ClaimCode, listener);
        }
        return result;
    }


    public int stop() {
        int result = -1;

        if (elian != null && isLibLoaded) {
            FNNetworkLog.i(_TAG, "StopSmartConnection: " + result);
            stopSmartConnectionTimer();
            if (mIsSmartConnectionEnable) {
                result = elian.StopSmartConnection();
                mIsSmartConnectionEnable = false;
            } else {
                result = 0;
            }

        }
        return result;
    }

    public void startDiscovery(final InetAddress ipAddr, final int port, final int period,
                               final IOTHandler.OnSmartConnectionListener onSmartConnectionListener) {
        if (notifyThread == null) {
            tempResponse = null;
            Thread thread = new Thread(new Runnable() {
                @Override
                public void run() {
                    try {
                        mSocket = new DatagramSocket(port, ipAddr);
                        mSocket.setBroadcast(true);
                        mSocket.setSoTimeout(period);
                        byte[] buffer = new byte[4096];
                        DatagramPacket packet = new DatagramPacket(buffer, buffer.length);
                        while (notifyThread != null) {
                            synchronized (SmartConnection.class) {
                                packet.setLength(buffer.length);
                                try {
                                    mSocket.receive(packet);
                                    String response = new String(packet.getData(), packet.getOffset(), packet.getLength());
                                    FNNetworkLog.c(_TAG, "notification:  start");
                                    if (tempResponse == null || !tempResponse.equals(response) ) {
                                        FNNetworkLog.c(_TAG, "notification: " + response);

                                            tempResponse = response.trim();
                                            onSmartConnectionListener.onThingDiscovered(true, tempResponse);
                                            //notifyThread = null;
                                    }
                                } catch (SocketTimeoutException ignore) {

                                }
                            }
                        }
                    } catch (IOException e) {
                        e.printStackTrace();
                    } finally {
                        if (mSocket != null) {
                            mSocket.close();
                            mSocket = null;
                        }
                    }
                    notifyThread = null;
                }
            });
            notifyThread = thread;
            thread.start();
        }
    }

    public void stopDiscovery() {
        notifyThread = null;
        if (mSocket != null) {
            mSocket.close();
            mSocket = null;
        }
    }
}
