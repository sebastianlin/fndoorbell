package com.fusionnextinc.doorbell.fragement;

import android.Manifest;
import android.content.Context;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;

import com.fusionnextinc.doorbell.MyApplication;
import com.fusionnextinc.doorbell.R;
import com.fusionnextinc.doorbell.manager.thingsManager.ThingsHandler;
import com.fusionnextinc.doorbell.manager.thingsManager.ThingsManager;
import com.fusionnextinc.doorbell.objects.FNError;
import com.fusionnextinc.doorbell.objects.FNEvent;
import com.fusionnextinc.doorbell.objects.FNFirmware;
import com.fusionnextinc.doorbell.objects.FNSchedule;
import com.fusionnextinc.doorbell.objects.FNThing;
import com.fusionnextinc.doorbell.objects.FNUserProfile;
import com.fusionnextinc.doorbell.utils.Utils;
import com.fusionnextinc.doorbell.widget.FNDialog;
import com.fusionnextinc.doorbell.widget.FNLayoutUtil;
import com.fusionnextinc.doorbell.widget.FNProgressBarCustom;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Mike Chang on 2017/6/22
 */
public class ShareThingFragment extends FNFragment implements View.OnClickListener {
    private static final String TAG = ShareThingFragment.class.getSimpleName();

    private FNLayoutUtil layoutUtil;
    private EditText editSharedEmail;
    private FNProgressBarCustom progressBar;
    private ThingsManager thingsManager;
    private static FNThing curFnThing;

    public static void openFragment(boolean clearOtherStack, FNThing fnThing) {
        FNFragmentManager.getInstance().openFragment(new ShareThingFragment(), clearOtherStack);
        curFnThing = fnThing;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        layoutUtil = new FNLayoutUtil(getActivity(), MyApplication.LAYOUT_WIDTH, MyApplication.LAYOUT_HEIGHT, FNLayoutUtil.BASE_WIDTH);
        thingsManager = ThingsManager.getInstance(getActivity());
    }

    @Override
    public void onStart() {
        super.onStart();
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            int hasCameraPermission = getActivity().checkSelfPermission(Manifest.permission.CAMERA);

            List<String> permissions = new ArrayList<>();

            if (hasCameraPermission != PackageManager.PERMISSION_GRANTED) {
                permissions.add(Manifest.permission.CAMERA);
            }
            if (!permissions.isEmpty()) {
                requestPermissions(permissions.toArray(new String[permissions.size()]), 111);
            }
        }

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        final ViewGroup nullParent = null;

        getActivity().setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        getActionBar().resetAll();
        getActionBar().setTitle(getString(R.string.fn_tittle_settings_share).replace("${Thing}", getString(R.string.fn_title_thing_name)), Gravity.CENTER, null);
        getActionBar().setCoverMode(false);
        getActionBar().setStartImage(R.drawable.back, new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        getActionBar().show();
        View view = inflater.inflate(R.layout.fragment_share_doorbell, nullParent);
        layoutUtil.fitAllView(view);

        editSharedEmail = (EditText) view.findViewById(R.id.et_share_email);
        Button btnSend = (Button) view.findViewById(R.id.btn_share_send);
        Button btnQrcode = (Button) view.findViewById(R.id.btn_share_qrcode);
        progressBar = new FNProgressBarCustom(getActivity(), (ViewGroup) view.findViewById(R.id.fragment_share_doorbell));

        btnSend.setOnClickListener(this);
        btnQrcode.setOnClickListener(this);
        return view;
    }

    @Override
    public void onClick(View view) {
        if (view.getId() == R.id.btn_share_send) {
            String sharedEmail = editSharedEmail.getText().toString().trim();

            // must be a check for not null, for empty field and for email validity(matchs email pattern)
            if (Utils.emptyFieldsRemaining(editSharedEmail)) {
                FNDialog.showErrorDialog(getActivity(), getString(R.string.fn_msg_share_failed).replace("${Thing}",getActivity().getString(R.string.fn_title_thing_name)), getString(R.string.fn_msg_login_fields_are_miss));
                return;
            }

            //mLogin.loginWithEmail(((EditText) findViewById(R.id.et_email)).getText().toString(),((EditText) findViewById(R.id.et_password)).getText().toString());
            if (sharedEmail.length() > 0) {
                startWait();
                thingsManager.setThingsListener(thingsListener);
                thingsManager.shareInvite(curFnThing.getThingId(), sharedEmail);
            }
        } else if (view.getId() == R.id.btn_share_qrcode) {
            QRCodeScanerFragment.openFragment(false, curFnThing);
        }
    }

    private void startWait() {
        progressBar.show();
    }


    private void stopWait() {
        progressBar.hide();
    }

    private void closeKeyboard() {
        if (getActivity().getCurrentFocus() != null && getActivity().getCurrentFocus().getWindowToken() != null) {
            InputMethodManager inputManager = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
            inputManager.hideSoftInputFromWindow(getActivity().getCurrentFocus().getWindowToken(), 0);
        }
    }

    final ThingsHandler.ThingsListener thingsListener = new ThingsHandler.ThingsListener() {

        @Override
        public void onListUserOwnedThings(ThingsManager instance, boolean result, String userId, ArrayList<FNThing> thingArrayList, FNError error) {

        }

        @Override
        public void onListUserInvitedThings(ThingsManager instance, boolean result, String userId, ArrayList<FNThing> thingArrayList, FNError error) {

        }

        @Override
        public void onListUserSharedThings(ThingsManager instance, boolean result, String userId, ArrayList<FNThing> thingArrayList, FNError error) {

        }

        @Override
        public void onListUserAllThings(ThingsManager instance, boolean result, String userId, ArrayList<FNThing> ownedThingArrayList, ArrayList<FNThing> sharedThingArrayList, ArrayList<FNThing> invitedThingArrayList, FNError error) {

        }

        @Override
        public void onListThingEvents(ThingsManager instance, boolean result, String thingId, String eventName, ArrayList<FNEvent> eventList, FNError error) {

        }

        @Override
        public void onListThingEventsByTime(ThingsManager instance, boolean result, String thingId, String eventName, String startTime, String endTime, ArrayList<FNEvent> eventList, FNError error) {

        }

        @Override
        public void onCurBatteryLevel(ThingsManager instance, boolean result, String thingId, String batteryLevel, FNError error) {

        }

        @Override
        public void onCurFirmwareVersion(ThingsManager instance, boolean result, String thingId, String fwVersion, FNError error) {

        }

        @Override
        public void onCurSwitchedStatus(ThingsManager instance, boolean result, String thingId, boolean isSwitch, FNError error) {

        }

        @Override
        public void onNewFirmwareVersion(ThingsManager instance, boolean result, String thingId, FNFirmware fnFirmware) {

        }

        @Override
        public void onStartFwUpdate(ThingsManager instance, boolean result, String thingId, String fwId) {

        }

        @Override
        public void onCurRingTone(ThingsManager instance, boolean result, String thingId, String ringtoneId, FNError error) {

        }

        @Override
        public void onGetScheduleList(ThingsManager instance, boolean result, String thingId, ArrayList<FNSchedule> scheduleList, FNError error) {

        }

        @Override
        public void onListInvitedUsers(ThingsManager instance, boolean result, String thingId, ArrayList<FNUserProfile> userList, FNError error) {

        }

        @Override
        public void onListSharedUsers(ThingsManager instance, boolean result, String thingId, ArrayList<FNUserProfile> userList, FNError error) {

        }

        @Override
        public void onCreateClaimCode(ThingsManager instance, boolean result, String claimCode, FNError error) {

        }

        @Override
        public void onCreateThing(ThingsManager instance, boolean result, FNThing fnThing, FNError error) {

        }

        @Override
        public void onUpdateThing(ThingsManager instance, boolean result, FNThing fnThing, FNError error) {

        }

        @Override
        public void onDeleteThing(ThingsManager instance, boolean result, FNError error) {

        }

        @Override
        public void onGetThing(ThingsManager instance, boolean result, FNThing fnThing, FNError error) {

        }

        @Override
        public void onChangeRingtone(ThingsManager instance, boolean result, String thingId, int ringtoneId) {

        }

        @Override
        public void onSetTimer(ThingsManager instance, boolean result, String thingId, String value) {

        }

        @Override
        public void onSetSchedule(ThingsManager instance, boolean result, String thingId, String value) {

        }

        @Override
        public void onShareAccepted(ThingsManager instance, boolean result, String thingId, FNError error) {

        }

        @Override
        public void onShareRejected(ThingsManager instance, boolean result, String thingId, FNError error) {

        }

        @Override
        public void onSwitchOnOff(ThingsManager instance, boolean result, String thingId, boolean isTurnOn) {

        }

        @Override
        public void onCloseCallSlot(ThingsManager instance, boolean result, FNError error) {

        }

        @Override
        public void onTakeCallSlot(ThingsManager instance, boolean result, String thingId, String callSlot, FNError error) {

        }

        @Override
        public void onShareInvited(ThingsManager instance, boolean result, String thingId, FNError error) {
            if (isAdded()) {
                stopWait();
                closeKeyboard();
                if (result) {
                    FNDialog.showSuccessDialog(getActivity(), getString(R.string.fn_tittle_share_title), getString(R.string.fn_msg_share_successfully).replace("${Thing}",getActivity().getString(R.string.fn_title_thing_name)));
                    onBackPressed();
                } else {
                    FNDialog.showErrorDialog(getActivity(), getString(R.string.fn_msg_share_failed).replace("${Thing}",getActivity().getString(R.string.fn_title_thing_name)), error);
                }
            }
        }

    };
}
