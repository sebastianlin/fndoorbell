package com.fusionnext.cloud.fusionapi;

import android.app.Application;
import android.content.Context;
import android.util.Log;


/**
 * Created by Mike Chang  on 2015/7/27.
 */
public class MyParameter extends Application {
    private static final String TAG = "MyParameter";

    private final static String synchronizedTagDataMgr = "synchronizedTagDataMgr";
    private final static String synchronizedTagAccount = "synchronizedTagAccount";

    //Database manager
    private DataManager dataMgr = null;
    //Account
    private Authorization authorization;

    public static MyParameter newInstance() {
        MyParameter myParameter = null;
        try {
            myParameter = new MyParameter();
        } catch (IllegalStateException e) {
            Log.e(TAG, "error occurs when creating FN social network lib");
        }
        return myParameter;
    }

    public MyParameter() {

    }

    public DataManager getDataMgr(Context context) {
        synchronized (synchronizedTagDataMgr) {
            context = context;
            if (dataMgr == null) setDataMgr(DataManager.newData(context));
            return dataMgr;
        }
    }

    public void setDataMgr(DataManager dm) {
        synchronized (synchronizedTagDataMgr) {
            dataMgr = dm;
        }
    }

    public Authorization getAuthorization(Context context) {
        synchronized (synchronizedTagAccount) {
            context = context;
            if (authorization == null) setAuthorization(new Authorization(context));
            return authorization;
        }
    }

    public void setAuthorization(Authorization authorization) {
        synchronized (synchronizedTagAccount) {
            this.authorization = authorization;
        }
    }

}