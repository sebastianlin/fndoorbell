package com.fusionnext.cloud.social.objects;

import android.util.Log;

import com.fusionnext.cloud.datalink.json.FNJsonObject;

/**
 * Created by Mike Chang on 2016/2/22.
 */
public class LikesMeta {

    protected final static String _JSON_TAG = "likesmeta";

    public LikesMeta() {
        _count = 0;
        _liked = 0;
    }

    public int _count;
    public int _liked;

    public void clean() {
        _count = 0;
        _liked = 0;
    }

    public boolean getDataFromJSONObject(FNJsonObject likesMetaObj) {
        clean();
        if (likesMetaObj == null) {
            Log.w(_JSON_TAG, " No JSON information");
            return false;
        }
        try {
            _count = likesMetaObj.getIntDefault("count");
            _liked = likesMetaObj.getBooleanDefault("is_liked") ? 1 : 0;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
        return true;
    }

}
