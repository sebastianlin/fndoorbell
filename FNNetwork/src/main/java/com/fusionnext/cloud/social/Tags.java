package com.fusionnext.cloud.social;

import android.content.Context;

import com.fusionnext.cloud.datalink.http.HttpGet;
import com.fusionnext.cloud.datalink.http.HttpParam;
import com.fusionnext.cloud.social.objects.Photo;
import com.fusionnext.cloud.social.objects.Track;

/**
 * Created by Mike Chang on 2017/3/23
 */
public class Tags {
    protected final static String _TAG = "Tags";
    private Context context;

    public Tags(Context context) {
        this.context = context;
    }


    public void listTracks(String tag, int order, String pageSize, String pageNum, final SocialAPIHandler.OnTagsListener onTagsListener) {
        HttpParam.HttpSubParam param = new HttpParam.HttpSubParam();

        param.accessToken = Public.getAuthorization(context).getUserAccessToken();
        param.url = HttpData.generateHttpGetUrl(HttpData._GET_TYPE_TAGS_LIST_TRACKS, tag, order, pageSize, pageNum);
        param.cacheName = "";
        HttpParam[] params = new HttpParam[1];
        params[0] = new HttpParam();
        params[0].params.clear();
        params[0].params.add(param);

        HttpGet httpGet = new HttpGet();
        httpGet.setContext(context);
        httpGet.setTag(HttpGet._HTTP_T_GET_NORMAL_TYPE);
        httpGet.setOnParseJsonListener(new HttpGet.OnParseJsonListener() {
            @Override
            public void onParse(boolean result, String returnStr) {
                onTagsListener.onListTracks(result, Track.parseTrackArray(returnStr));
            }
        });
        httpGet.execute(params);
    }

    public void listPhotos(String tag, int order, String pageSize, String pageNum, final SocialAPIHandler.OnTagsListener onTagsListener) {
        HttpParam.HttpSubParam param = new HttpParam.HttpSubParam();

        param.accessToken = Public.getAuthorization(context).getUserAccessToken();
        param.url = HttpData.generateHttpGetUrl(HttpData._GET_TYPE_TAGS_LIST_PHOTOS, tag, order, pageSize, pageNum);
        param.cacheName = "";
        HttpParam[] params = new HttpParam[1];
        params[0] = new HttpParam();
        params[0].params.clear();
        params[0].params.add(param);

        HttpGet httpGet = new HttpGet();
        httpGet.setContext(context);
        httpGet.setTag(HttpGet._HTTP_T_GET_NORMAL_TYPE);
        httpGet.setOnParseJsonListener(new HttpGet.OnParseJsonListener() {
            @Override
            public void onParse(boolean result, String returnStr) {
                onTagsListener.onListPhotos(result, Photo.parsePhotoArray(returnStr));
            }
        });
        httpGet.execute(params);
    }
}
