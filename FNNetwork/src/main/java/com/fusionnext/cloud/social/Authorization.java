package com.fusionnext.cloud.social;

import android.content.Context;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;

import com.fusionnext.cloud.datalink.http.HttpPost;
import com.fusionnext.cloud.datalink.json.FNJsonObject;
import com.fusionnext.cloud.utils.FNNetworkLog;
import com.fusionnext.cloud.utils.Utils;

import org.json.JSONException;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;

/**
 * Created by Mike Chang on 2016/2/22.
 */
public class Authorization {
    protected final static String _TAG = "Authorization";

    public static class ClientToken {
        public String accessToken = "";
        public String expireIn = "";
        public String refreshToken = "";
        public String clientId = "";
    }

    public static class UserToken {
        public String accessToken = "";
        public String expireIn = "";
        public String refreshToken = "";
        public String clientId = "";
        public String userId = "";
    }

    private Context context;
    private ClientToken clientToken = new ClientToken();
    private UserToken userToken = new UserToken();

    public final static int REFRESH_TOKEN_TYPE_CLIENT = 0;
    public final static int REFRESH_TOKEN_TYPE_USER = 1;

    private SocialAPIHandler.OnAuthorizationListener onAuthorizationListener;

    public Authorization(Context context) {
        this.context = context;
        setClientToken(Public.getDataMgr(context).loadClientToken());
        setUserToken(Public.getDataMgr(context).loadUserToken());
    }

    public void activiateAPI(String clientSecret, final SocialAPIHandler.OnAuthorizationListener onAuthorizationListener) {
        getClientCredentials(clientSecret, onAuthorizationListener);
        this.onAuthorizationListener = onAuthorizationListener;
    }

    public String getCommToken() {
        String token = "";

        if (clientToken != null) {
            token = clientToken.accessToken;
        }

        token = token.replace("Bearer", "").trim();
        return token;
    }

    public synchronized String getClientAccessToken() {
        setClientToken(Public.getDataMgr(context).loadClientToken());
        if (clientToken != null) {
            if (!clientToken.accessToken.equals("") && !clientToken.refreshToken.equals("")) {
                refreshToken(REFRESH_TOKEN_TYPE_CLIENT);
            }
            return clientToken.accessToken;
        }
        return "";
    }

    public synchronized String getUserAccessToken() {
        setUserToken(Public.getDataMgr(context).loadUserToken());
        if (userToken != null) {
            if (!userToken.accessToken.equals("") && !userToken.refreshToken.equals("")) {
                refreshToken(REFRESH_TOKEN_TYPE_USER);
            }
            return userToken.accessToken;
        }
        return "";
    }

    public boolean isLogin() {
        boolean isLogin = false;
        FNNetworkLog.d(_TAG, "accessToken  = " + userToken.accessToken + ", refreshToken = " + userToken.refreshToken);
        if (userToken != null) {
            isLogin = !userToken.accessToken.equals("") && !userToken.refreshToken.equals("");
        }
        return isLogin;
    }

    public boolean clearTokenInfo() {
        UserToken token = new UserToken();

        setUserToken(token);
        Public.getDataMgr(context).saveUserToken(token);
        return true;
    }

    public String getUserId() {
        if (userToken != null) {
            return userToken.userId;
        }
        return "";
    }

    public String getClientId() {
        if (clientToken != null) {
            return clientToken.clientId;
        }
        return "";
    }

    private void setClientToken(ClientToken token) {
        if (clientToken == null) clientToken = new ClientToken();
        if (token.accessToken != null) clientToken.accessToken = token.accessToken;
        if (token.clientId != null) clientToken.clientId = token.clientId;
        if (token.expireIn != null) clientToken.expireIn = token.expireIn;
        if (token.refreshToken != null) clientToken.refreshToken = token.refreshToken;
    }

    private void setUserToken(UserToken token) {
        if (userToken == null) userToken = new UserToken();
        if (token.userId != null) userToken.userId = token.userId;
        if (token.accessToken != null) userToken.accessToken = token.accessToken;
        if (token.clientId != null) userToken.clientId = token.clientId;
        if (token.expireIn != null) userToken.expireIn = token.expireIn;
        if (token.refreshToken != null) userToken.refreshToken = token.refreshToken;
    }

    private void getClientCredentials(String clientSecret, final SocialAPIHandler.OnAuthorizationListener onAuthorizationListener) {
        String packageName = Utils.getPackageName(context);
        String identifier = null;

        try {
            ApplicationInfo info = context.getPackageManager().getApplicationInfo(packageName, PackageManager.GET_META_DATA);
            identifier = info.metaData.getString("com.fusionnext.sdk.social.API_KEY");
        } catch (Exception ignore) {
            identifier = null;
        }

        HttpPost.PostParam[] params = new HttpPost.PostParam[1];
        params[0] = new HttpPost.PostParam();
        params[0].url = HttpData.generateHttpPostUrl(HttpData._POST_TYPE_AUTH_GET_CLIENT_TOKEN);
        params[0].type = HttpData._POST_TYPE_AUTH_GET_CLIENT_TOKEN;
        params[0].param = new HashMap<Object, Object>();
        params[0].param.put("grant_type", "client_credentials");
        params[0].param.put("client_identifier", identifier);
        params[0].param.put("client_secret", clientSecret);

        HttpPost post = new HttpPost();
        post.setContext(context);
        post.setOnParseJsonListener(new HttpPost.OnParseJsonListener() {
            @Override
            public void onParse(boolean returnResult, int postType, String id, String returnStr) {
                if (returnStr != null) {
                    //Devices devices = new Devices(context);
                    //devices.createDevice(onDevicesListener);
                    ClientToken clientToken = parseClientToken(returnStr);
                    if (onAuthorizationListener != null) {
                        onAuthorizationListener.onActivateAPI(returnResult);
                    }
                }
            }
        });
        post.execute(params);
    }

    private ClientToken parseClientToken(String jsonStr) {
        if (jsonStr == null || jsonStr.equals("")) return null;

        try {
            FNJsonObject object = new FNJsonObject(jsonStr);
            SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");

            String token = object.getStringDefault("access_token");
            clientToken.accessToken = "Bearer " + token;
            clientToken.expireIn = object.getStringDefault("expires_in");
            Date curDate = new Date(System.currentTimeMillis() + Long.parseLong(clientToken.expireIn) * 1000); // 獲取當前時間
            clientToken.expireIn = formatter.format(curDate);
            clientToken.refreshToken = object.getStringDefault("refresh_token");
            clientToken.clientId = object.getStringDefault("client_id");
            setClientToken(clientToken);
            Public.getDataMgr(context).saveClientToken(clientToken);
            FNNetworkLog.i(_TAG, "Device token will be expired in " + clientToken.expireIn);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return clientToken;
    }

    private UserToken parseUserToken(String jsonStr) {
        String resultStr = "";
        if (jsonStr == null || jsonStr.equals("")) return null;

        try {
            FNJsonObject object = new FNJsonObject(jsonStr);
            SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");

            String token = FNJsonObject.getJasonItems(object, "access_token");
            userToken.accessToken = "Bearer " + token;
            userToken.expireIn = FNJsonObject.getJasonItems(object, "expires_in");
            Date curDate = new Date(System.currentTimeMillis() + Long.parseLong(userToken.expireIn) * 1000); // 獲取當前時間
            userToken.expireIn = formatter.format(curDate);

            userToken.refreshToken = FNJsonObject.getJasonItems(object, "refresh_token");
            userToken.clientId = FNJsonObject.getJasonItems(object, "client_id");
            userToken.userId = FNJsonObject.getJasonItems(object, "user_id");
            setUserToken(userToken);
            Public.getDataMgr(context).saveUserToken(userToken);


            FNNetworkLog.i(_TAG, "UserProfile token will be expired in " + userToken.expireIn);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return userToken;
    }

    private synchronized boolean refreshToken(final int tokenType) {
        String refreshToken = null;
        String sDate = null;
        if (tokenType == REFRESH_TOKEN_TYPE_CLIENT) {
            refreshToken = clientToken.refreshToken;
            sDate = clientToken.expireIn;
        } else {
            refreshToken = userToken.refreshToken;
            sDate = userToken.expireIn;
        }

        if (sDate == null || sDate.equals("")) {
            return false;
        }
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
        long expireIn = 0;
        long curTime = System.currentTimeMillis();

        try {
            Date date = dateFormat.parse(sDate);
            expireIn = date.getTime();
        } catch (ParseException e) {
            e.printStackTrace();
        }

        if (refreshToken.equals("")) {
            return false;
        }

        //don't need to refresh token from server, because the token is not expired yet.
        if (curTime < expireIn) {
            FNNetworkLog.i(_TAG, "Access token is not expired yet, expire time = " + clientToken.expireIn);
            return true;
        }

        //setAccountType(accountInfo.accountType, true);
        HttpPost.PostParam[] params = new HttpPost.PostParam[1];

        params[0] = new HttpPost.PostParam();
        params[0].url = HttpData.generateHttpPostUrl(HttpData._POST_TYPE_AUTH_REFRESH_CLIENT_TOKEN);
        params[0].type = HttpData._POST_TYPE_AUTH_REFRESH_CLIENT_TOKEN;
        params[0].param = new HashMap<Object, Object>();

        params[0].param.put("grant_type", "refresh_token");
        params[0].param.put("refresh_token", refreshToken);

        HttpPost post = new HttpPost();
        //HttpPost.PostResult result = post.httpPostSync(params);
        post.setContext(context);
        post.setOnParseJsonListener(new HttpPost.OnParseJsonListener() {
            @Override
            public void onParse(boolean returnResult, int postType, String id, String returnStr) {
                switch (tokenType) {
                    case REFRESH_TOKEN_TYPE_CLIENT:
                        if (returnStr != null) {
                            ClientToken clientToken = parseClientToken(returnStr);
                        }
                    case REFRESH_TOKEN_TYPE_USER:
                        if (returnStr != null) {
                            UserToken userToken = parseUserToken(returnStr);
                        }
                    default:
                        break;
                }
            }
        });
        post.execute(params);
        return false;
    }


    public boolean regeistDevice(String clientSecret, final SocialAPIHandler.OnAuthorizationListener onAuthorizationListener) {
        boolean isRistered = false;
        if (clientToken != null) {
            if (clientToken.accessToken.equals("") || clientToken.refreshToken.equals("")) {
                getClientCredentials(clientSecret, onAuthorizationListener);
            } else {
                isRistered = refreshToken(REFRESH_TOKEN_TYPE_USER);
            }
        }
        return isRistered;
    }


    public void getSocialTokenByEmail(String account, String password, final SocialAPIHandler.OnAuthorizationListener onAuthorizationListener) {
        String packageName = Utils.getPackageName(context);
        String identifier = null;

        try {
            ApplicationInfo info = context.getPackageManager().getApplicationInfo(packageName, PackageManager.GET_META_DATA);
            identifier = info.metaData.getString("com.fusionnext.sdk.social.API_KEY");
        } catch (Exception ignore) {
            identifier = null;
        }

        String deviceName = Utils.getDeviceName(context);
        HttpPost.PostParam[] params = new HttpPost.PostParam[1];
        params[0] = new HttpPost.PostParam();
        //params[0].accessToken = Public.getAuthorization(context).getClientAccessToken();
        params[0].url = HttpData.generateHttpPostUrl(HttpData._POST_TYPE_AUTH_LOGIN_BY_EMAIL);
        params[0].type = HttpData._POST_TYPE_AUTH_LOGIN_BY_EMAIL;
        params[0].param = new HashMap<Object, Object>();

        params[0].param.put("grant_type", "password");
        params[0].param.put("client_identifier", identifier);
        if (account != null && account.length() > 0) {
            params[0].param.put("email", account);
        }
        if (password != null && password.length() > 0) {
            params[0].param.put("password", password);
        }

        HttpPost post = new HttpPost();
        post.setContext(context);
        post.setOnParseJsonListener(new HttpPost.OnParseJsonListener() {
            @Override
            public void onParse(boolean returnResult, int postType, String id, String returnStr) {
                if (returnStr != null) {
                    UserToken userToken = parseUserToken(returnStr);
                }
                onAuthorizationListener.onGetUserToken(returnResult, userToken);
            }
        });
        post.execute(params);
    }

    public void clearLoginInfo() {
        userToken.accessToken = "";
        userToken.expireIn = "";
        userToken.refreshToken = "";
        userToken.clientId = "";
        userToken.userId = "";
    }

    final SocialAPIHandler.OnDevicesListener onDevicesListener = new SocialAPIHandler.OnDevicesListener() {
        @Override
        public void onResult(boolean result, int postType, String retStr) {
            if (onAuthorizationListener != null) {
                onAuthorizationListener.onActivateAPI(result);
            }
        }
    };
}
