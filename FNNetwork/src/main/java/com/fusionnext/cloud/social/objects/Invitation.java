package com.fusionnext.cloud.social.objects;

import android.util.Log;

import com.fusionnext.cloud.datalink.json.FNJsonArray;
import com.fusionnext.cloud.datalink.json.FNJsonObject;

import org.json.JSONException;

import java.util.ArrayList;

/**
 * Created by Mike Chang on 2017/3/27
 */

public class Invitation {
    protected final static String _TAG = "Invitation";

    public String _invitationId;
    public String _groupId;
    public String _invitedId;
    public String _inviterId;
    public String _createdAt;
    public String _updatedAt;

    public Invitation() {
        _invitationId = "";
        _groupId = "";
        _invitedId = "";
        _inviterId = "";
        _createdAt = "";
        _updatedAt = "";
    }

    public void clean() {
        _invitationId = "";
        _groupId = "";
        _invitedId = "";
        _inviterId = "";
        _createdAt = "";
        _updatedAt = "";
    }

    public boolean getDataFromJSONObject(FNJsonObject inviteObj) {
        clean();
        if (inviteObj == null) {
            Log.e(_TAG, "Null JSON Object!");
            return false;
        }

        try {
            //Get the id
            _invitationId = inviteObj.getStringDefault("id");
            _groupId = inviteObj.getStringDefault("group_id");
            _invitedId = inviteObj.getStringDefault("invited_id");
            _inviterId = inviteObj.getStringDefault("inviter_id");
            _createdAt = inviteObj.getStringDefault("created_at");
            _updatedAt = inviteObj.getStringDefault("updated_at");
        } catch (Exception e) {
            e.printStackTrace();
            Log.e(_TAG, "Error occur during parsing JSON object");
            return false;
        }
        return true;
    }

    public static ArrayList<Invitation> parseInvitationArray(String json) {
        try {
            ArrayList<Invitation> inviteList = new ArrayList<Invitation>();
            FNJsonObject inviteObject = new FNJsonObject(json);
            FNJsonArray inviteArray = inviteObject.getJSONArray("items");
            if (inviteArray == null) {
                return null;
            }
            for (int i = 0; i < inviteArray.length(); i++) {
                FNJsonObject object = inviteArray.getJSONObject(i);
                Invitation invitation = new Invitation();
                invitation.getDataFromJSONObject(object);
                if (invitation == null) return null;
                inviteList.add(invitation);
            }
            return inviteList;
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return null;
    }
}
