package com.fusionnextinc.doorbell.Adapter;

import android.app.Activity;
import android.content.Context;
import android.content.res.ColorStateList;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.LayerDrawable;
import android.os.Build;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.fusionnextinc.doorbell.DefaultAppConfig;
import com.fusionnextinc.doorbell.MyApplication;
import com.fusionnextinc.doorbell.R;
import com.fusionnextinc.doorbell.manager.data.AppDataBase;
import com.fusionnextinc.doorbell.manager.data.AppDataThing;
import com.fusionnextinc.doorbell.manager.data.ListMap;
import com.fusionnextinc.doorbell.manager.thingsManager.ThingsManager;
import com.fusionnextinc.doorbell.utils.FNLog;
import com.fusionnextinc.doorbell.widget.FNHeaderListView.FNListViewAdapter;
import com.fusionnextinc.doorbell.widget.FNLayoutUtil;

import java.util.Map;

/**
 * Created by Mike Chang on 2017/6/23
 */
public class GroupThingsListAdapter extends FNListViewAdapter {
    private static final String TAG = "GroupThingsListAdapter";

    private final static int TYPE_DRAG_HINT = 0;
    public final static int TYPE_OWNED_THINGS_LIST = 1;
    public final static int TYPE_SHARED_THINGS_LIST = 2;
    private final static int SECTION_COUNTS = TYPE_SHARED_THINGS_LIST + 1;

    private final int COLOR_LEVEL1 = 30;
    private final int COLOR_LEVEL2 = 50;
    private final int MAX_EVENT_COUNT_NUMBER = 999;

    private Context mContext;
    private LayoutInflater mInflater;
    private FNLayoutUtil mLayoutUtil;
    private ListMap mOwnedThinglistMap = new ListMap();
    private ListMap mSharedThinglistMap = new ListMap();
    private int mLayoutID;

    public GroupThingsListAdapter(Context context, ListMap ownedThinglistMap, ListMap sharedThinglistMap, int layoutID) {
        this.mContext = context;
        this.mInflater = LayoutInflater.from(context);
        this.mOwnedThinglistMap.addAll(ownedThinglistMap);
        this.mSharedThinglistMap.addAll(sharedThinglistMap);
        this.mLayoutID = layoutID;
    }

    private static class sectionHolder {
        //ID, its invisible
        private TextView textSection;
        private TextView textHint;
        private LinearLayout sectionLayout;
    }

    private static class ViewHolder {
        //ID, its invisible
        private TextView textId;
        private TextView textType;
        private LinearLayout bgLayout;
        private ImageView imgThing;
        private TextView textName;
        private TextView textSubtitle;
        private TextView textCounts;
        private ProgressBar batteryProgress;
    }

    @Override
    public boolean isEnabled(int section, int position) {
        return true;
    }

    @Override
    public Object getItem(int section, int position) {
        if (section < 0 || position < 0) {
            FNLog.d(TAG, "section is " + section + ", position is " + position);
            return null;
        }
        switch (section) {
            case TYPE_OWNED_THINGS_LIST:
                return mOwnedThinglistMap.get(position);
            case TYPE_SHARED_THINGS_LIST:
                return mSharedThinglistMap.get(position);
        }
        return null;
    }

    @Override
    public long getItemId(int section, int position) {
        return position;
    }

    @Override
    public int getSectionCount() {
        return SECTION_COUNTS;
    }

    @Override
    public int getCountForSection(int section) {

        switch (section) {
            case TYPE_OWNED_THINGS_LIST:
                if (mOwnedThinglistMap.size() <= 0) {
                    return 1;
                } else {
                    return mOwnedThinglistMap.size();
                }
            case TYPE_SHARED_THINGS_LIST:
                if (mSharedThinglistMap.size() <= 0) {
                    return 1;
                } else {
                    return mSharedThinglistMap.size();
                }
            default:
                return 0;
        }
    }

    @Override
    public View getItemView(int section, int position, View convertView, ViewGroup parent) {
        //Ready the ViewHolder
        Map<String, Object> map;
        ViewHolder holder;

        if (convertView == null) {
            holder = new ViewHolder();
            mLayoutUtil = new FNLayoutUtil((Activity) mContext, MyApplication.LAYOUT_WIDTH, MyApplication.LAYOUT_HEIGHT, FNLayoutUtil.BASE_WIDTH);
            convertView = mInflater.inflate(mLayoutID, null);
            holder.bgLayout = convertView.findViewById(R.id.adapter_things_list);
            mLayoutUtil.fitAllView(holder.bgLayout);
            holder.textId = convertView.findViewById(R.id.textThingId);
            holder.textType = convertView.findViewById(R.id.textThingType);
            holder.imgThing = convertView.findViewById(R.id.imageThing);
            holder.textName = convertView.findViewById(R.id.textName);
            holder.textSubtitle = convertView.findViewById(R.id.textSubtitle);
            holder.textCounts = convertView.findViewById(R.id.textCounts);
            holder.batteryProgress = convertView.findViewById(R.id.batteryProgressbar);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        switch (section) {
            case TYPE_OWNED_THINGS_LIST:
                if (mOwnedThinglistMap != null && mOwnedThinglistMap.size() > 0) {
                    map = mOwnedThinglistMap.get(position);
                    updateMyThingsView(holder, map, section);
                } else {
                    updateEmptyView(holder, section);
                }
                break;
            case TYPE_SHARED_THINGS_LIST:
                if (mSharedThinglistMap != null && mSharedThinglistMap.size() > 0) {
                    map = mSharedThinglistMap.get(position);
                    updateMyThingsView(holder, map, section);
                } else {
                    updateEmptyView(holder, section);
                }
                break;
            default:
                break;
        }

        return convertView;
    }

    @Override
    public View getSectionHeaderView(int section, View convertView, ViewGroup parent) {
        sectionHolder holder;
        final ViewGroup nullParent = null;

        if (convertView == null) {
            holder = new sectionHolder();
            mLayoutUtil = new FNLayoutUtil((Activity) mContext, MyApplication.LAYOUT_WIDTH, MyApplication.LAYOUT_HEIGHT, FNLayoutUtil.BASE_WIDTH);
            convertView = mInflater.inflate(R.layout.thing_section_item, nullParent);
            holder.sectionLayout = convertView.findViewById(R.id.thing_section_item);
            mLayoutUtil.fitAllView(holder.sectionLayout);
            holder.textSection = convertView.findViewById(R.id.textSection);
            holder.textHint = convertView.findViewById(R.id.textHint);
            convertView.setTag(holder);
        } else {
            holder = (sectionHolder) convertView.getTag();
        }
        switch (section) {
            case TYPE_DRAG_HINT:
                holder.textHint.setVisibility(View.VISIBLE);
                holder.textHint.setText(mContext.getString(R.string.fn_title_drag_refresh));
                updateTextView(holder.textSection, null);
                break;
            case TYPE_OWNED_THINGS_LIST:
                holder.textHint.setVisibility(View.GONE);
                updateTextView(holder.textSection, mContext.getString(R.string.fn_title_main_my_things).replace("${Thing}", mContext.getString(R.string.fn_title_thing_name)));
                break;
            case TYPE_SHARED_THINGS_LIST:
                holder.textHint.setVisibility(View.GONE);
                updateTextView(holder.textSection, mContext.getString(R.string.fn_title_main_shared_things).replace("${Thing}", mContext.getString(R.string.fn_title_thing_name)));
                break;
            default:
                break;
        }
        return convertView;
    }

    public void updateAdapterData(int thingsType, int groupId) {
        switch (thingsType) {
            case AppDataThing.THING_TYPE_THINGS_OWNED:
                mOwnedThinglistMap.clear();
                mOwnedThinglistMap.addAll(AppDataThing.getInstance(mContext).getAdapterData(AppDataBase.THING_TYPE_THINGS_GROUP_OWNED, String.valueOf(groupId)));
                break;
            case AppDataThing.THING_TYPE_THINGS_SHARED:
                mSharedThinglistMap.clear();
                mSharedThinglistMap.addAll(AppDataThing.getInstance(mContext).getAdapterData(AppDataBase.THING_TYPE_THINGS_GROUP_SHARED, String.valueOf(groupId)));
                break;
        }
        notifyDataSetChanged();
    }

    private void updateTextView(TextView text, String strText) {
        if (text.getId() == R.id.textSection) {
            boolean hide = (strText == null || strText.trim().equals(""));
            text.setVisibility(hide ? View.GONE : View.VISIBLE);
        }
        text.setText(strText);
    }

    private void updateEmptyView(final ViewHolder holder, int section) {
        switch (section) {
            case TYPE_OWNED_THINGS_LIST:
                holder.textName.setText(mContext.getString(R.string.fn_text_my_doorbell_empty).replace("${Thing}", (mContext.getString(R.string.fn_title_thing_name))));
                break;
            case TYPE_SHARED_THINGS_LIST:
                holder.textName.setText(mContext.getString(R.string.fn_text_shared_doorbell_empty).replace("${Thing}", (mContext.getString(R.string.fn_title_thing_name))));
                break;
            default:
                break;
        }
        holder.textId.setText("");
        holder.textSubtitle.setVisibility(View.GONE);
        holder.imgThing.setVisibility(View.GONE);
        holder.batteryProgress.setVisibility(View.GONE);
        holder.textCounts.setVisibility(View.GONE);
    }

    private void updateMyThingsView(final ViewHolder holder, final Map<String, Object> map, final int thingType) {
        holder.textType.setText(String.valueOf(thingType));
        updateTextView(holder.textId, map, AppDataThing.TAG_THING_ID);
        updateTextView(holder.textName, map, AppDataThing.TAG_THING_NAME);
        holder.textSubtitle.setVisibility(View.GONE);
        updateBatteryLevel(holder.batteryProgress, map, AppDataThing.TAG_THING_BATTERY_LEVEL);
        updateEventCounts(holder.textCounts, map, AppDataThing.TAG_THING_EVENT_COUNTS);
        updateThingIcon(holder, map, AppDataThing.TAG_THING_PRODUCT_ID);
    }

    private void updateTextView(TextView text, Map<String, Object> map, String tag) {
        String strText = "";
        Object object = map.get(tag);
        if (object != null) {
            if (object instanceof Integer) strText = Integer.toString((Integer) object);
            else if (object instanceof String) strText = (String) object;
        }

        if (text.getId() == R.id.textSubtitle || text.getId() == R.id.textName) {
            boolean hide = (strText == null || strText.trim().equals(""));
            text.setVisibility(hide ? View.GONE : View.VISIBLE);
        }

        text.setText(strText);
    }

    private void updateEventCounts(TextView textView, final Map<String, Object> map, String tag) {
        String counts = (String) map.get(tag);
        if (counts != null && counts.length() > 0) {
            int iCounts = 0;
            try {
                iCounts = Integer.valueOf(counts);
            } catch (NumberFormatException e) {
                e.printStackTrace();
            }
            textView.setVisibility(iCounts > 0 ? View.VISIBLE : View.GONE);
            if (iCounts > 0 && iCounts <= MAX_EVENT_COUNT_NUMBER) {
                textView.setText(counts);
            } else if (iCounts > MAX_EVENT_COUNT_NUMBER) {
                String sCount = Integer.toString(MAX_EVENT_COUNT_NUMBER) + "+";
                textView.setText(sCount);
            }
        }
    }

    private void updateBatteryLevel(ProgressBar progressBar, final Map<String, Object> map, String tag) {
        String level = (String) map.get(tag);
        if (level != null && level.length() > 0) {
            int iLevel = 0;
            int newColor = mContext.getResources().getColor(R.color.battery_level_bg);

            try {
                iLevel = Integer.valueOf(level);
                if (iLevel <= COLOR_LEVEL1) {
                    float colorOffset = ((float) 100 / (float) (COLOR_LEVEL1 - 1)) / (float) 100;
                    newColor = interpolateColor(mContext.getResources().getColor(R.color.battery_level_low_start), mContext.getResources().getColor(R.color.battery_level_low_end), iLevel * colorOffset);
                } else if (iLevel > COLOR_LEVEL1 && iLevel <= COLOR_LEVEL2) {
                    float colorOffset = ((float) 100 / (float) (COLOR_LEVEL2 - COLOR_LEVEL1 - 1)) / (float) 100;
                    newColor = interpolateColor(mContext.getResources().getColor(R.color.battery_level_mid_start), mContext.getResources().getColor(R.color.battery_level_mid_end), iLevel * colorOffset);
                } else {
                    float colorOffset = ((float) 100 / (float) (100 - COLOR_LEVEL2 - 1)) / (float) 100;
                    newColor = interpolateColor(mContext.getResources().getColor(R.color.battery_level_high_start), mContext.getResources().getColor(R.color.battery_level_high_end), iLevel * colorOffset);
                }
            } catch (NumberFormatException e) {
                e.printStackTrace();
            }

            if (iLevel >= 0 && iLevel <= 100) {
                progressBar.setProgress(iLevel);
                if (Build.VERSION.SDK_INT < Build.VERSION_CODES.LOLLIPOP) {
                    LayerDrawable progressBarDrawable = (LayerDrawable) progressBar.getProgressDrawable();
                    Drawable backgroundDrawable = progressBarDrawable.getDrawable(0);
                    Drawable progressDrawable = progressBarDrawable.getDrawable(1);
                    Drawable progressDrawable2 = progressBarDrawable.getDrawable(2);

                    backgroundDrawable.setColorFilter(mContext.getResources().getColor(R.color.battery_level_bg), android.graphics.PorterDuff.Mode.SRC_IN);
                    progressDrawable.setColorFilter(newColor, android.graphics.PorterDuff.Mode.SRC_IN);
                    progressDrawable2.setColorFilter(newColor, android.graphics.PorterDuff.Mode.SRC_IN);
                } else {
                    progressBar.setProgressTintList(ColorStateList.valueOf(newColor));
                }
            } else {
                progressBar.setProgress(0);
            }
        }
    }

    private void updateThingIcon(ViewHolder holder, final Map<String, Object> map, String tag) {
        ImageView image = holder.imgThing;
        ProgressBar progressBar = holder.batteryProgress;

        String id = (String) map.get(tag);
        if (id != null && id.length() > 0) {
            int productId = 0;
            try {
                productId = Integer.valueOf(id);
            } catch (NumberFormatException e) {
                e.printStackTrace();
            }
            if (productId == ThingsManager.PRODUCT_ID_DOORBELL || productId == ThingsManager.PRODUCT_ID_DOORBELL_VOICE) {
                image.setImageDrawable(mContext.getResources().getDrawable(R.drawable.thing_icon1));
            } else if (productId == ThingsManager.PRODUCT_ID_PLUG) {
                boolean isOnline = (boolean) map.get(AppDataThing.TAG_THING_ONLINE);
                if (isOnline) {
                    image.setImageDrawable(mContext.getResources().getDrawable(R.drawable.image_plug_logo));
                    holder.textName.setTextColor(mContext.getResources().getColor(R.color.things_title_text));
                } else {
                    image.setImageDrawable(mContext.getResources().getDrawable(R.drawable.image_plug_logo_offline));
                    holder.textName.setTextColor(mContext.getResources().getColor(R.color.things_title_text_disable));
                }
            } else {
                image.setImageDrawable(mContext.getResources().getDrawable(R.drawable.unknown_thing_icon));
            }
            if (DefaultAppConfig.APP_SOLY || DefaultAppConfig.BATTERY_LEVEL_BY_TEXT) {
                progressBar.setVisibility(View.GONE);
            } else {
                progressBar.setVisibility(View.VISIBLE);
            }
            image.setVisibility(View.VISIBLE);
        }
    }

    private int interpolateColor(int startColor, int endColor, float position) {
        float[] hsvStartColor = new float[3];
        Color.colorToHSV(startColor, hsvStartColor);

        float[] hsvEndColor = new float[3];
        Color.colorToHSV(endColor, hsvEndColor);

        hsvEndColor[0] = interpolate(hsvStartColor[0], hsvEndColor[0], position);
        hsvEndColor[1] = interpolate(hsvStartColor[1], hsvEndColor[1], position);
        hsvEndColor[2] = interpolate(hsvStartColor[2], hsvEndColor[2], position);

        return Color.HSVToColor(hsvEndColor);
    }

    private float interpolate(float hsvStartColor, float hsvEndColor, float position) {
        return (hsvStartColor + ((hsvEndColor - hsvStartColor) * position));
    }
}


