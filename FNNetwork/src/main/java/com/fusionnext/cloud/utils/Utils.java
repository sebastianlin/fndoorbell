package com.fusionnext.cloud.utils;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.os.Build;
import android.preference.PreferenceManager;
import android.util.Base64;
import android.util.Log;
import android.widget.EditText;

import java.io.InputStream;
import java.io.OutputStream;
import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.text.DecimalFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.UUID;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Utils {
    public static void CopyStream(InputStream is, OutputStream os) {
        final int buffer_size = 1024;
        try {
            byte[] bytes = new byte[buffer_size];
            for (; ; ) {
                int count = is.read(bytes, 0, buffer_size);
                if (count == -1)
                    break;
                os.write(bytes, 0, count);
            }
        } catch (Exception ex) {
        }
    }


    public static boolean isNetworkConnected(Context context) {
        try {
            ConnectivityManager connectivity = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
            if (connectivity != null) {
                NetworkInfo info = connectivity.getActiveNetworkInfo();
                if (info != null && info.isConnected()) {
                    return true;
                }
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }


    public static String getFileSizeAsString(long fileSize) {
        DecimalFormat df = new DecimalFormat("#.00");
        String fileSizeString = "";
        if (fileSize == 0) {
            fileSizeString = "0 Byte";
        } else if (fileSize < 1024) {
            fileSizeString = df.format((double) fileSize) + "Bytes";
        } else if (fileSize < 1048576) {
            fileSizeString = df.format((double) fileSize / 1024) + "KB";
        } else if (fileSize < 1073741824) {
            fileSizeString = df.format((double) fileSize / 1048576) + "MB";
        } else {
            fileSizeString = df.format((double) fileSize / 1073741824) + "GB";
        }
        return fileSizeString;
    }

    public static boolean isValidEmail(String strEmail) {
        try {
            String strPattern = "^[a-zA-Z][\\w\\.-]*[a-zA-Z0-9]@[a-zA-Z0-9][\\w\\.-]*[a-zA-Z0-9]\\.[a-zA-Z][a-zA-Z\\.]*[a-zA-Z]$";
            Pattern p = Pattern.compile(strPattern);
            Matcher m = p.matcher(strEmail);
            return m.matches();

        } catch (Exception e) {
            e.printStackTrace();
            Log.e("Utils", "Failed to check string");
        }
        return false;
    }

    public static boolean isVideoFile(String strFileName) {
        if (strFileName == null)
            return false;
        int typeIndex = strFileName.lastIndexOf(".");
        if (typeIndex == -1)
            return false;
        String fileType = strFileName.substring(typeIndex + 1).toLowerCase();
        if (fileType == null)
            return false;
        return fileType.equals("mp4") || fileType.equals("m4v")
                || fileType.equals("3gp") || fileType.equals("3gpp")
                || fileType.equals("3g2") || fileType.equals("3gpp2")
                || fileType.equals("wmv") || fileType.equals("avi");
    }

    public static String getMacAddress(Context context) {
        WifiManager wifiMan = (WifiManager) context.getSystemService(Context.WIFI_SERVICE);
        WifiInfo wifiInf = wifiMan.getConnectionInfo();
        return wifiInf.getMacAddress();
    }

    public static String getModelName() {
        String manufacturer = Build.MANUFACTURER;
        String model = Build.MODEL;
        if (model.startsWith(manufacturer)) {
            return capitalize(model);
        } else {
            return capitalize(manufacturer) + " " + model;
        }
    }


    private static String capitalize(String s) {
        if (s == null || s.length() == 0) {
            return "";
        }
        char first = s.charAt(0);
        if (Character.isUpperCase(first)) {
            return s;
        } else {
            return Character.toUpperCase(first) + s.substring(1);
        }
    }

    public static String getAPIVerison() {

        String version = "0.0";
        try {
            StringBuilder strBuild = new StringBuilder();
            strBuild.append(Build.VERSION.RELEASE);
            version = strBuild.toString();
        } catch (NumberFormatException e) {
            Log.e("myApp", "error retriving api version" + e.getMessage());
        }

        return version;
    }

    public static String getPackageName(Context context) {
        String name = context.getPackageName();
        return name;
    }

    public static String getManufacturer(Context context) {
        String manufacturer = "none";
        try {
            StringBuilder strBuild = new StringBuilder();
            strBuild.append(Build.MANUFACTURER);
            manufacturer = strBuild.toString();
        } catch (NumberFormatException e) {
            Log.e("myApp", "error retriving manufacturer" + e.getMessage());
        }
        return manufacturer;
    }

    public static String getDeviceName(Context context) {
        String name = "none";
        try {
            StringBuilder strBuild = new StringBuilder();
            strBuild.append(Build.DEVICE);
            name = strBuild.toString();
        } catch (NumberFormatException e) {
            Log.e("myApp", "error retriving device name" + e.getMessage());
        }
        return name;
    }


    public static String decodeJWT(String token) {
        String text = null;
        String[] separated = null;
        if (token != null) {
            separated = token.split("\\.");
        }
        if (separated[1] != null) {
            byte[] data = Base64.decode(separated[1], Base64.DEFAULT);
            text = new String(data, StandardCharsets.UTF_8);
        }
        return text;
    }

    public static boolean emailIsValid(String email) {
        String expression = "^[\\w\\.-]+@([\\w\\-]+\\.)+[A-Z]{2,4}$";
        CharSequence inputStr = email.trim();

        Pattern pattern = Pattern.compile(expression, Pattern.CASE_INSENSITIVE);
        Matcher matcher = pattern.matcher(inputStr);
        return matcher.matches();
    }

    public static boolean emptyFieldsRemaining(EditText... ets) {
        for (EditText et : ets) {
            String input = et.getText().toString().trim();
            if (input == null || input.isEmpty()) {
                return true;
            }
        }
        return false;
    }

    public static boolean stringsNullOrEmpty(String... strings) {
        for (String string : strings) {
            if (string == null || string.isEmpty()) {
                return true;
            }
        }
        return false;
    }

    public static boolean birthdayIsValid(Date birthday) {
        Calendar c = Calendar.getInstance();
        c.add(Calendar.YEAR, -13);
        return birthday.before(c.getTime());
    }

    public static String getUUID(Context context, String msg) {
        SharedPreferences settings = PreferenceManager.getDefaultSharedPreferences(context);
        String key = msg == null ? "UUID" : ("UUID_" + msg);

        settings.edit().remove(key);
        String uuid = settings.getString(key, null);
        if (uuid == null) {
            uuid = UUID.randomUUID().toString();
            settings.edit().putString(key, uuid).apply();
        }
        return uuid;
    }

    public static void resetUUID(Context context, String msg) {
        SharedPreferences settings = PreferenceManager.getDefaultSharedPreferences(context);
        String key = msg == null ? "UUID" : ("UUID_" + msg);
        settings.edit().remove(key);
    }

    public static String md5(String toEncrypt) {
        try {
            final MessageDigest digest = MessageDigest.getInstance("md5");
            digest.update(toEncrypt.getBytes());
            final byte[] bytes = digest.digest();
            final StringBuilder sb = new StringBuilder();
            for (int i = 0; i < bytes.length; i++) {
                sb.append(String.format("%02X", bytes[i]));
            }
            return sb.toString().toLowerCase();
        } catch (Exception exc) {
            return ""; // Impossibru!
        }
    }
}