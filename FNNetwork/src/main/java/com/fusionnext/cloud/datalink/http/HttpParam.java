package com.fusionnext.cloud.datalink.http;

import java.util.ArrayList;

/**
 * Created by Mike Chang on 2016/2/22.
 */
public class HttpParam {

    public static class HttpSubParam {
        public String url = null;
        public String cacheName = "";
        public String accessToken = null;
    }

    public ArrayList<HttpSubParam> params = new ArrayList<>();

}
