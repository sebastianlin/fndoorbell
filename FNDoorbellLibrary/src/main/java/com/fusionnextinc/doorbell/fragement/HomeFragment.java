package com.fusionnextinc.doorbell.fragement;

import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import com.google.android.material.tabs.TabLayout;
import androidx.fragment.app.Fragment;
import androidx.viewpager.widget.ViewPager;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.fusionnextinc.doorbell.Adapter.HomeFragmentAdapter;
import com.fusionnextinc.doorbell.MyApplication;
import com.fusionnextinc.doorbell.R;
import com.fusionnextinc.doorbell.manager.account.AccountManager;
import com.fusionnextinc.doorbell.manager.smartConnection.SmartConnectionManager;
import com.fusionnextinc.doorbell.widget.FNActionBar;
import com.fusionnextinc.doorbell.widget.FNLayoutUtil;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

/**
 * Created by MikeChang on 2018/6/7
 */
public class HomeFragment extends FNFragment implements
        ViewPager.OnPageChangeListener {
    private static final String TAG = HomeFragment.class.getSimpleName();
    // Utils
    private FNLayoutUtil mLayoutUtil;

    private String thingInfo = "";

    //Tab
    private TabLayout mTabLayout;
    private LayoutInflater mLayoutInflater;
    private int mIvIdleArray[] = {R.drawable.icon_tabbar_thing, R.drawable.icon_tabbar_group, R.drawable.icon_tabbar_percenter};
    private int mIvSelectedArray[] = {R.drawable.icon_tabbar_thing_selected, R.drawable.icon_tabbar_group_selected, R.drawable.icon_tabbar_percenter_selected};
    private int mTextViewArray[] = {R.string.fn_title_tabbar_device, R.string.fn_title_tabbar_group, R.string.fn_title_tabbar_me};
    private List<Fragment> mFragmentList = new ArrayList<Fragment>();
    private ViewPager mViewPager;
    private HomeFragmentAdapter mHomeFragmentAdapter;

    public enum TabType {TAB_TYPE_DEVICE, TAB_TYPE_GROUP, TAB_TYPE_ME}

    private TabType mTabType = TabType.TAB_TYPE_DEVICE;

    // Constants
    private boolean mCheckActivityIntent = false;

    public static void openFragment(boolean clearOtherStack, int fragmentType, String info, TabType tabType, boolean checkActivityIntent) {
        HomeFragment fragment = new HomeFragment();
        fragment.thingInfo = info;
        fragment.mTabType = tabType;
        fragment.mCheckActivityIntent = checkActivityIntent;
        FNFragmentManager.getInstance().openFragment(fragment, clearOtherStack);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mLayoutUtil = new FNLayoutUtil(getActivity(), MyApplication.LAYOUT_WIDTH, MyApplication.LAYOUT_HEIGHT, FNLayoutUtil.BASE_WIDTH);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        if (getActivity().getRequestedOrientation() != ActivityInfo.SCREEN_ORIENTATION_PORTRAIT) {
            getActivity().setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        }

        View view = inflater.inflate(R.layout.fragment_home, container, false);
        mLayoutUtil.fitAllView(view);
        initView(view);//初始化控件
        initPage();//初始化页面
        setTabIcon();
        setSelectTab();
        checkActivityIntent();
        AccountManager.getInstance(getContext()).updatePushTokn();
        return view;
    }

    //    控件初始化控件
    private void initView(View view) {
        mViewPager = view.findViewById(R.id.pager);
        mTabLayout = view.findViewById(R.id.tabs);
        mViewPager.addOnPageChangeListener(this);//设置页面切换时的监听器
        mLayoutInflater = LayoutInflater.from(MyApplication.getAppContext());//加载布局管理器
    }

    /*初始化Fragment*/
    private void initPage() {
        ThingsFragment thingsFragment = new ThingsFragment();
        AccountSettingsFragment accountSettingsFragment = new AccountSettingsFragment();
        GroupsFragment groupsFragment = new GroupsFragment();

        thingsFragment.setUpdateThing(true, null);
        mFragmentList.clear();
        mFragmentList.add(thingsFragment);
        mFragmentList.add(groupsFragment);
        mFragmentList.add(accountSettingsFragment);
        mHomeFragmentAdapter = new HomeFragmentAdapter(getChildFragmentManager(), mFragmentList);

        mViewPager.setOffscreenPageLimit(mFragmentList.size());
        mViewPager.setAdapter(mHomeFragmentAdapter);
        mViewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(mTabLayout));
        mTabLayout.setupWithViewPager(mViewPager);
    }

    private void setSelectTab() {
        TabLayout.Tab tab = null;
        if (mTabType.equals(TabType.TAB_TYPE_DEVICE)) {
            tab = mTabLayout.getTabAt(0);
        } else if (mTabType.equals(TabType.TAB_TYPE_GROUP)) {
            tab = mTabLayout.getTabAt(1);
        } else {
            tab = mTabLayout.getTabAt(2);
        }
        tab.select();
        refreshActionBar(tab.getPosition());
    }

    private void setTabIcon() {
        /*新建Tabspec选项卡并设置Tab菜单栏的内容和绑定对应的Fragment*/
        int count = mTextViewArray.length;
        for (int i = 0; i < count; i++) {
            TabLayout.Tab tab = mTabLayout.getTabAt(i);
            if (i == 0) {
                tab.setCustomView(getTabItemView(i, true));
            } else {
                tab.setCustomView(getTabItemView(i, false));
            }
        }
        mTabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                tab.getCustomView().findViewById(R.id.tab_imageview).setBackgroundResource(mIvSelectedArray[tab.getPosition()]);
                refreshActionBar(tab.getPosition());
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {
                tab.getCustomView().findViewById(R.id.tab_imageview).setBackgroundResource(mIvIdleArray[tab.getPosition()]);
            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {
                tab.getCustomView().findViewById(R.id.tab_imageview).setBackgroundResource(mIvSelectedArray[tab.getPosition()]);
            }
        });
    }

    private View getTabItemView(int i, boolean isSelected) {
        //将xml布局转换为view对象
        View view = mLayoutInflater.inflate(R.layout.tab_item_main, null);
        //利用view对象，找到布局中的组件,并设置内容，然后返回视图
        ImageView mImageView = (ImageView) view
                .findViewById(R.id.tab_imageview);
        TextView mTextView = (TextView) view.findViewById(R.id.tab_textview);
        if (isSelected) {
            mImageView.setBackgroundResource(mIvSelectedArray[i]);
        } else {
            mImageView.setBackgroundResource(mIvIdleArray[i]);
        }
        mTextView.setText(getActivity().getString(mTextViewArray[i]));
        return view;
    }


    @Override
    public void onPageScrollStateChanged(int arg0) {

    }//arg0 ==1的时候表示正在滑动，arg0==2的时候表示滑动完毕了，arg0==0的时候表示什么都没做，就是停在那。

    @Override
    public void onPageScrolled(int arg0, float arg1, int arg2) {

    }//表示在前一个页面滑动到后一个页面的时候，在前一个页面滑动前调用的方法

    @Override
    public void onPageSelected(int arg0) {//arg0是表示你当前选中的页面位置Postion，这事件是在你页面跳转完毕的时候调用的。
        refreshActionBar(arg0);
    }

    private void refreshActionBar(int index) {
        FNActionBar actionBar = getActionBar();
        actionBar.setCoverMode(false);
        actionBar.resetAll();
        actionBar.show();
        switch (index) {
            case 0:
                actionBar.setTitle(getString(R.string.fn_title_tabbar_device), Gravity.CENTER, null);
                actionBar.addEndImage(R.drawable.btn_add_thing, new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        SmartConnectionFragment.openFragment(Objects.requireNonNull(getActivity()),false, SmartConnectionManager.SMART_CONNECTION_CREATE_THING);
                    }
                });
                break;
            case 1:
                actionBar.setTitle(getString(R.string.fn_title_tabbar_group), Gravity.CENTER, null);
                actionBar.addEndImage(R.drawable.btn_add_thing, new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        RoomListFragment.openFragment(false, null);
                    }
                });
                break;
            case 2:
                actionBar.setTitle(getString(R.string.fn_tittle_settings_account_settings), Gravity.CENTER, null);
                break;
        }
    }

    private void checkActivityIntent() {
        Intent intent;

        if (mCheckActivityIntent && (intent = getActivity().getIntent()) != null) {
            Bundle data = intent.getExtras();
            if (data != null) {
                String type = data.getString("type");
                String thingId = data.getString("thing_id");
                String thingName = data.getString("thing_name");

                if (type != null && type.equals("event")) {
                    String event = data.getString("event");
                    if (event != null && event.equals("call")) {
                        String callSlotId = data.getString("call_slot_id");
                        CallFragment.openFragment(thingId, thingName, "sound1.wav", callSlotId);
                    }
                }
            }
            mCheckActivityIntent = false;
        }
    }
}
