package com.fusionnext.cloud.iot.objects;

import android.util.Log;

import com.fusionnext.cloud.datalink.json.FNJsonArray;
import com.fusionnext.cloud.datalink.json.FNJsonObject;
import com.fusionnext.cloud.utils.Times;

import org.json.JSONException;

import java.util.ArrayList;
import java.util.TimeZone;

/**
 * Created by Mike Chang on 2016/6/12
 */
public class Setting {

    protected final static String TAG = "Setting";

    public Setting() {
        _settingId = "";
        _thingId = "";
        _name = "";
        _value = "";
        _createdAt = "";
        _updatedAt = "";
    }

    public String _settingId;
    public String _thingId;
    public String _name;
    public String _value;
    public String _createdAt;  // user's created_at
    public String _updatedAt; // user's updated_at

    public void clean() {
        _settingId = "";
        _thingId = "";
        _name = "";
        _value = "";
        _createdAt = "";
        _updatedAt = "";
    }

    public boolean getDataFromJSONObject(FNJsonObject settingObj) {
        clean();
        if (settingObj == null) {
            Log.w(TAG, " No JSON information");
            return false;
        }
        try {
            _settingId = settingObj.getStringDefault("id");
            _thingId = settingObj.getStringDefault("thing_id");
            _name = settingObj.getStringDefault("name");
            _value = settingObj.getStringDefault("value");
            _createdAt = settingObj.getStringDefault("created_at");
            _createdAt = Times.converTime(_createdAt, TimeZone.getDefault());
            _updatedAt = settingObj.getStringDefault("updated_at");
            _updatedAt = Times.converTime(_updatedAt, TimeZone.getDefault());
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
        return true;
    }

    public static Setting parseSetting(String json) {
        try {
            FNJsonObject settingObject = new FNJsonObject(json);

            Setting setting = new Setting();
            setting.getDataFromJSONObject(settingObject);
            return setting;
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static ArrayList<Setting> parseSettingArray(String json) {
        try {
            ArrayList<Setting> settingList = new ArrayList<Setting>();
            FNJsonObject settingObject = new FNJsonObject(json);
            FNJsonArray settingArray = settingObject.getJSONArray("items");
            if (settingArray == null) {
                return null;
            }
            for (int i = 0; i < settingArray.length(); i++) {
                FNJsonObject object = settingArray.getJSONObject(i);
                Setting setting = new Setting();
                setting.getDataFromJSONObject(object);
                if (setting == null) return null;
                settingList.add(setting);
            }
            return settingList;
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return null;
    }
}
