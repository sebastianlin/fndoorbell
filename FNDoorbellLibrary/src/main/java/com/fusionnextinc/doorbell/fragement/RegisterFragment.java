package com.fusionnextinc.doorbell.fragement;

import android.content.Context;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.text.method.HideReturnsTransformationMethod;
import android.text.method.PasswordTransformationMethod;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;

import com.fusionnextinc.doorbell.MyApplication;
import com.fusionnextinc.doorbell.R;
import com.fusionnextinc.doorbell.manager.account.AaccountHandler;
import com.fusionnextinc.doorbell.manager.account.AccountManager;
import com.fusionnextinc.doorbell.objects.FNError;
import com.fusionnextinc.doorbell.utils.Utils;
import com.fusionnextinc.doorbell.widget.FNDialog;
import com.fusionnextinc.doorbell.widget.FNLayoutUtil;
import com.fusionnextinc.doorbell.widget.FNProgressBarCustom;

/**
 * Created by Mike Chang on 2017/6/22
 */
public class RegisterFragment extends FNFragment implements View.OnClickListener {
    private static final String TAG = RegisterFragment.class.getSimpleName();

    private FNLayoutUtil layoutUtil;
    private EditText editAccount;
    private EditText editPassword;
    private ImageView ivShowPwd;
    private FNProgressBarCustom progressBar;
    private AccountManager accountManager;

    public static void openFragment(boolean clearOtherStack) {
        FNFragmentManager.getInstance().openFragment(new RegisterFragment(), clearOtherStack);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        layoutUtil = new FNLayoutUtil(getActivity(), MyApplication.LAYOUT_WIDTH, MyApplication.LAYOUT_HEIGHT, FNLayoutUtil.BASE_WIDTH);
        accountManager = AccountManager.getInstance(getActivity());
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        final ViewGroup nullParent = null;

        getActivity().setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        getActionBar().resetAll();
        getActionBar().setTitle(getString(R.string.fn_title_register_register), Gravity.CENTER, null);
        getActionBar().setCoverMode(false);
        getActionBar().show();
        View view = inflater.inflate(R.layout.fragment_register, nullParent);
        layoutUtil.fitAllView(view);

        editAccount = (EditText) view.findViewById(R.id.et_account);
        editPassword = (EditText) view.findViewById(R.id.et_password);
        Button btnRegister = (Button) view.findViewById(R.id.btn_register);
        Button btnResend = (Button) view.findViewById(R.id.btn_resend);
        ivShowPwd = (ImageView) view.findViewById(R.id.btn_show_password);
        ivShowPwd.setTag(R.drawable.show_password);
        ivShowPwd.setImageDrawable(getResources().getDrawable(R.drawable.show_password));
        progressBar = new FNProgressBarCustom(getActivity(), (ViewGroup) view.findViewById(R.id.fragment_register));

        btnRegister.setOnClickListener(this);
        btnResend.setOnClickListener(this);
        ivShowPwd.setOnClickListener(this);
        return view;
    }

    @Override
    public void onClick(View view) {
        if (Utils.isNetworkConnected(getActivity())) {
            if (view.getId() == R.id.btn_resend) {
                String email = editAccount.getText().toString().trim();
                accountManager.userResend(email);
                return;
            }

            if (view.getId() == R.id.btn_register) {
                String email = editAccount.getText().toString().trim();
                String pwd = editPassword.getText().toString().trim();

                if (!Utils.isNetworkConnected(getActivity())) {
                    FNDialog.showNoNetworkDialog(getActivity());
                    return;
                }
                // must be a check for not null, for empty field and for email validity(matchs email pattern)
                if (Utils.emptyFieldsRemaining(editAccount, editPassword)) {
                    FNDialog.showErrorDialog(getActivity(), getString(R.string.fn_msg_register_failed), getString(R.string.fn_msg_login_fields_are_miss));
                    return;
                }

                if (!Utils.emailIsValid(email)) {
                    FNDialog.showErrorDialog(getActivity(), getString(R.string.fn_msg_register_failed), getString(R.string.fn_msg_login_email_is_invaild));
                    return;
                }

                /*if (!Utils.passwordIsValid(pwd)) {
                    FNDialog.showErrorDialog(getActivity(), getString(R.string.fn_msg_register_failed), getString(R.string.fn_msg_login_pwd_is_invalid));
                    return;
                }*/

                //mLogin.loginWithEmail(((EditText) findViewById(R.id.et_email)).getText().toString(),((EditText) findViewById(R.id.et_password)).getText().toString());
                if (email.length() > 0 && pwd.length() > 0) {
                    startWait();
                    accountManager.setAccountListener(accountListener);
                    accountManager.setUserAccount(email);
                    accountManager.setUserPassword(pwd);
                    accountManager.userRegister();
                }
            } else if (view.getId() == R.id.btn_show_password) {
                if ((Integer) ivShowPwd.getTag() == R.drawable.show_password) {
                    ivShowPwd.setImageDrawable(getResources().getDrawable(R.drawable.show_password_pressed));
                    ivShowPwd.setTag(R.drawable.show_password_pressed);
                    editPassword.setTransformationMethod(HideReturnsTransformationMethod.getInstance());
                } else {
                    ivShowPwd.setImageDrawable(getResources().getDrawable(R.drawable.show_password));
                    ivShowPwd.setTag(R.drawable.show_password);
                    editPassword.setTransformationMethod(PasswordTransformationMethod.getInstance());
                }
            }
        } else {
            FNDialog.showNoNetworkDialog(getActivity());
        }
    }

    private void closeKeyboard() {
        if (getActivity().getCurrentFocus() != null && getActivity().getCurrentFocus().getWindowToken() != null) {
            InputMethodManager inputManager = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
            inputManager.hideSoftInputFromWindow(getActivity().getCurrentFocus().getWindowToken(), 0);
        }
    }

    private void startWait() {
        progressBar.show();
    }


    private void stopWait() {
        progressBar.hide();
    }

    final AaccountHandler.AccountListener accountListener = new AaccountHandler.AccountListener() {
        @Override
        public void onUserLogined(AccountManager instance, boolean result, FNError error) {

        }

        @Override
        public void onGetUserInfo(AccountManager instance, boolean result, FNError error) {

        }

        @Override
        public void onUpdateUserInfo(AccountManager instance, boolean result, FNError error) {

        }

        @Override
        public void onUserLogout(AccountManager instance, boolean result, FNError error) {

        }

        @Override
        public void onResetPassword(AccountManager instance, boolean result, FNError error) {

        }

        @Override
        public void onSetPassword(AccountManager instance, boolean result, FNError error) {

        }

        @Override
        public void onUserRegistered(AccountManager instance, boolean result, FNError error) {
            if (isAdded()) {
                closeKeyboard();
                stopWait();
                if (result) {
                    FNDialog.showSuccessDialog(getActivity(), getString(R.string.fn_title_register_register), getString(R.string.fn_msg_check_register_email));
                    onBackPressed();
                } else {
                    FNDialog.showErrorDialog(getActivity(), getString(R.string.fn_msg_register_failed), error);
                }
            }
        }

    };
}
