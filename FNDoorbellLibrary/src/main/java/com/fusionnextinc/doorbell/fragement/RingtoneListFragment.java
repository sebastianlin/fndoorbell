package com.fusionnextinc.doorbell.fragement;

import android.content.pm.ActivityInfo;
import android.content.res.AssetFileDescriptor;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.ListView;

import com.fusionnextinc.doorbell.Adapter.RingtoneListAdapter;
import com.fusionnextinc.doorbell.MyApplication;
import com.fusionnextinc.doorbell.R;
import com.fusionnextinc.doorbell.manager.data.AppDataThing;
import com.fusionnextinc.doorbell.manager.thingsManager.ThingsHandler;
import com.fusionnextinc.doorbell.manager.thingsManager.ThingsManager;
import com.fusionnextinc.doorbell.objects.FNError;
import com.fusionnextinc.doorbell.objects.FNEvent;
import com.fusionnextinc.doorbell.objects.FNFirmware;
import com.fusionnextinc.doorbell.objects.FNSchedule;
import com.fusionnextinc.doorbell.objects.FNThing;
import com.fusionnextinc.doorbell.objects.FNUserProfile;
import com.fusionnextinc.doorbell.utils.Utils;
import com.fusionnextinc.doorbell.widget.FNDialog;
import com.fusionnextinc.doorbell.widget.FNLayoutUtil;
import com.fusionnextinc.doorbell.widget.FNProgressBarCustom;

import java.io.IOException;
import java.util.ArrayList;

/**
 * Created by Mike Chang on 2017/6/22
 */
public class RingtoneListFragment extends FNFragment implements AbsListView.OnItemClickListener {
    private static final String TAG = RingtoneListFragment.class.getSimpleName();

    private FNLayoutUtil layoutUtil;
    private FNProgressBarCustom progressBar;
    private ThingsManager thingsManager;
    private AppDataThing appDataThing;
    private RingtoneListAdapter ringtoneListAdapter = null;
    private ListView ringtoneListView;
    private static FNThing curFNThing;
    private static MediaPlayer mediaPlayer;

    public static void openFragment(boolean clearOtherStack, FNThing fnThing) {
        curFNThing = fnThing;
        FNFragmentManager.getInstance().openFragment(new RingtoneListFragment(), clearOtherStack);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        layoutUtil = new FNLayoutUtil(getActivity(), MyApplication.LAYOUT_WIDTH, MyApplication.LAYOUT_HEIGHT, FNLayoutUtil.BASE_WIDTH);
        thingsManager = ThingsManager.getInstance(getActivity());
        appDataThing = AppDataThing.getInstance(getActivity());
        mediaPlayer = MediaPlayer.create(getActivity(), thingsManager.getRingtoneRawById(0));
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        final ViewGroup nullParent = null;

        getActivity().setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        getActionBar().resetAll();
        getActionBar().setTitle(getString(R.string.fn_title_ringtone_settings), Gravity.CENTER, null);
        getActionBar().setCoverMode(false);
        getActionBar().setStartImage(R.drawable.back, new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        getActionBar().addEndImage(R.drawable.btn_done, onDoneClickListener);
        getActionBar().show();
        View view = inflater.inflate(R.layout.fragment_ringtone_list, nullParent);
        layoutUtil.fitAllView(view);
        ringtoneListView = (ListView) view.findViewById(R.id.ringtone_list);
        ringtoneListView.setOnItemClickListener(this);
        progressBar = new FNProgressBarCustom(getActivity(), (ViewGroup) view.findViewById(R.id.fragment_ringtone_list));
        initRingtoneList();
        //appDataThing.loadAllThingsList(true, true);
        return view;
    }

    private View.OnClickListener onDoneClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            if (Utils.isNetworkConnected(getActivity())) {
                thingsManager.setThingsListener(thingsListener);
                thingsManager.changeRingtone(curFNThing.getThingId(), ringtoneListAdapter.getSelectedRingtone());
                startWait();
                mediaPlayer.stop();
            } else {
                FNDialog.showNoNetworkDialog(getActivity());
            }
        }
    };

    private void startWait() {
        progressBar.show();
    }

    private void stopWait() {
        progressBar.hide();
    }

    private boolean initRingtoneList() {
        if (ringtoneListView == null) return false;
        if (ringtoneListAdapter == null) {
            int ringtoneId;
            curFNThing = appDataThing.getThing(curFNThing.getThingId());
            try {
                ringtoneId = Integer.parseInt(curFNThing.getRingtoneId());
            } catch (NumberFormatException e) {
                ringtoneId = 0;
                e.printStackTrace();
            }
            //Setup the adapter
            ringtoneListAdapter = new RingtoneListAdapter(getActivity(), thingsManager.getRingtoneList(), ringtoneId);
        }
        ringtoneListView.setAdapter(ringtoneListAdapter);
        return true;
    }

    @Override
    public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
        ringtoneListAdapter.setSelectedRingtone(i);
        AssetFileDescriptor afd = getActivity().getResources().openRawResourceFd(thingsManager.getRingtoneRawById(i));
        if (afd == null) return;
        try {
            mediaPlayer.reset();
            mediaPlayer.setDataSource(afd.getFileDescriptor(), afd.getStartOffset(), afd.getLength());
            mediaPlayer.setLooping(true);
            mediaPlayer.prepare();
            mediaPlayer.start();
            afd.close();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    @Override
    public void onBackPressed() {
        if (progressBar!= null && !progressBar.isShown()) {
            super.onBackPressed(); // Comment this super call to avoid calling finish() or fragmentmanager's backstack pop operation.
        }
        mediaPlayer.stop();
    }



    final ThingsHandler.ThingsListener thingsListener = new ThingsHandler.ThingsListener() {

        @Override
        public void onListUserOwnedThings(ThingsManager instance, boolean result, String userId, ArrayList<FNThing> thingArrayList, FNError error) {

        }

        @Override
        public void onListUserInvitedThings(ThingsManager instance, boolean result, String userId, ArrayList<FNThing> thingArrayList, FNError error) {

        }

        @Override
        public void onListUserSharedThings(ThingsManager instance, boolean result, String userId, ArrayList<FNThing> thingArrayList, FNError error) {

        }

        @Override
        public void onListUserAllThings(ThingsManager instance, boolean result, String userId, ArrayList<FNThing> ownedThingArrayList, ArrayList<FNThing> sharedThingArrayList, ArrayList<FNThing> invitedThingArrayList, FNError error) {

        }

        @Override
        public void onListThingEvents(ThingsManager instance, boolean result, String thingId, String eventName, ArrayList<FNEvent> eventList, FNError error) {

        }

        @Override
        public void onListThingEventsByTime(ThingsManager instance, boolean result, String thingId, String eventName, String startTime, String endTime, ArrayList<FNEvent> eventList, FNError error) {

        }

        @Override
        public void onCurBatteryLevel(ThingsManager instance, boolean result, String thingId, String batteryLevel, FNError error) {

        }

        @Override
        public void onCurFirmwareVersion(ThingsManager instance, boolean result, String thingId, String fwVersion, FNError error) {

        }

        @Override
        public void onCurSwitchedStatus(ThingsManager instance, boolean result, String thingId, boolean isSwitch, FNError error) {

        }

        @Override
        public void onNewFirmwareVersion(ThingsManager instance, boolean result, String thingId, FNFirmware fnFirmware) {

        }

        @Override
        public void onStartFwUpdate(ThingsManager instance, boolean result, String thingId, String fwId) {

        }

        @Override
        public void onCurRingTone(ThingsManager instance, boolean result, String thingId, String ringtoneId, FNError error) {

        }

        @Override
        public void onGetScheduleList(ThingsManager instance, boolean result, String thingId, ArrayList<FNSchedule> scheduleList, FNError error) {

        }

        @Override
        public void onListInvitedUsers(ThingsManager instance, boolean result, String thingId, ArrayList<FNUserProfile> userList, FNError error) {

        }

        @Override
        public void onListSharedUsers(ThingsManager instance, boolean result, String thingId, ArrayList<FNUserProfile> userList, FNError error) {

        }

        @Override
        public void onCreateClaimCode(ThingsManager instance, boolean result, String claimCode, FNError error) {

        }

        @Override
        public void onCreateThing(ThingsManager instance, boolean result, FNThing fnThing, FNError error) {

        }

        @Override
        public void onUpdateThing(ThingsManager instance, boolean result, FNThing fnThing, FNError error) {

        }

        @Override
        public void onDeleteThing(ThingsManager instance, boolean result, FNError error) {

        }

        @Override
        public void onGetThing(ThingsManager instance, boolean result, FNThing fnThing, FNError error) {

        }

        @Override
        public void onChangeRingtone(ThingsManager instance, boolean result, String thingId, int ringtoneId) {
            if (isAdded()) {
                stopWait();
                mediaPlayer.stop();
                if (result) {
                    FNDialog.showSuccessDialog(getActivity(),  getString(R.string.fn_title_ringtone_settings), getString(R.string.fn_msg_ringtone_change_successfully));
                    thingsManager.updateThingRingtoneToDB(thingId, String.valueOf(ringtoneId));
                    onBackPressed();
                } else {
                    FNDialog.showErrorDialog(getActivity(), getString(R.string.fn_title_ringtone_settings), getString(R.string.fn_msg_ringtone_change_failed));
                }
            }
        }

        @Override
        public void onSetTimer(ThingsManager instance, boolean result, String thingId, String value) {

        }

        @Override
        public void onSetSchedule(ThingsManager instance, boolean result, String thingId, String value) {

        }

        @Override
        public void onShareInvited(ThingsManager instance, boolean result, String thingId, FNError error) {

        }

        @Override
        public void onShareAccepted(ThingsManager instance, boolean result, String thingId, FNError error) {

        }

        @Override
        public void onShareRejected(ThingsManager instance, boolean result, String thingId, FNError error) {

        }

        @Override
        public void onSwitchOnOff(ThingsManager instance, boolean result, String thingId, boolean isTurnOn) {

        }

        @Override
        public void onCloseCallSlot(ThingsManager instance, boolean result, FNError error) {

        }

        @Override
        public void onTakeCallSlot(ThingsManager instance, boolean result, String thingId, String callSlot, FNError error) {

        }

    };

}
