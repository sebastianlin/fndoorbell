package com.fusionnext.cloud.social;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.fusionnext.cloud.fusionapi.Public;
import com.fusionnext.cloud.social.objects.UserProfile;

/**
 * Created by Mike Chang on 2016/2/22.
 */
public class DataManager extends SQLiteOpenHelper {
    private final static int _PARAM_ACCOUNT_USER_ID = 20120;
    private final static int _PARAM_ACCOUNT_USER_NAME = 20130;
    private final static int _PARAM_ACCOUNT_USER_AVATAR = 20140;
    private final static int _PARAM_ACCOUNT_USER_BIRTHDAY = 20150;
    private final static int _PARAM_ACCOUNT_USER_GENDER = 20160;
    private final static int _PARAM_ACCOUNT_USER_LOCALE = 20170;

    private final static int _PARAM_ACCOUNT_USER_ACCESS_TOKEN = 20180;
    private final static int _PARAM_ACCOUNT_USER_REFRESH_TOKEN = 20190;
    private final static int _PARAM_ACCOUNT_USER_EXPIRE_IN = 20200;

    private final static int _PARAM_ACCOUNT_CLIENT_ACCESS_TOKEN = 20230;
    private final static int _PARAM_ACCOUNT_CLIENT_REFRESH_TOKEN = 20240;
    private final static int _PARAM_ACCOUNT_CLIENT_EXPIRE_IN = 20250;
    private final static int _PARAM_ACCOUNT_CLIENT_ID = 20260;

    private final static String NO_SEARCH_ID = "8888";

    private SQLiteDatabase sdb;

    //Setting table
    final String CREATE_SETTINGS_TABLE = "CREATE TABLE IF NOT EXISTS t_settings " +
            "(_id integer primary key autoincrement, " +
            "f_param integer, " +
            "f_str_value nvarchar(8192), " +
            "f_int_value integer)";

    public DataManager(Context context, String name,
                       SQLiteDatabase.CursorFactory factory, int version) {
        super(context, name, factory, version);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(CREATE_SETTINGS_TABLE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        //Used tables
        db.execSQL("DROP TABLE IF EXISTS t_settings");
        db.execSQL("DROP TABLE IF EXISTS t_user");
        onCreate(db);
    }

    public static DataManager newData(Context context) {
        return new DataManager(context, Public.Defines._DB_NAME, null, Public.Defines._DB_VERSION);
    }

    private String getCursorString(Cursor cursor, String name) {
        if (cursor == null) return "";
        int index = cursor.getColumnIndex(name);
        if (index < 0) return "";
        return cursor.getString(index);
    }

    private int getCursorInt(Cursor cursor, String name, int defaultInt) {
        if (cursor == null) return defaultInt;
        int index = cursor.getColumnIndex(name);
        if (index < 0) return defaultInt;
        return cursor.getInt(index);
    }

    //Check the param setting is exist or not
    private boolean isParamExist(int param) {
        sdb = getReadableDatabase();
        String sql = "select _id from t_settings where f_param = " + param;
        Cursor cursor = null;
        try {
            int count;

            cursor = sdb.rawQuery(sql, null);
            count = cursor.getCount();

            return count > 0;
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            if (cursor != null) cursor.close();
        }

        return false;
    }

    //Get the param value for integer type
    private int getIntValue(int param, int defaultValue) {
        sdb = getReadableDatabase();
        String sql = "select f_param, f_int_value from t_settings where f_param = " + param;
        Cursor cursor = null;
        try {
            int ret = defaultValue;

            cursor = sdb.rawQuery(sql, null);
            if (cursor.getCount() > 0) {
                cursor.moveToFirst();
                ret = cursor.getInt(cursor.getColumnIndex("f_int_value"));
            }
            return ret;

        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            if (cursor != null) cursor.close();
        }

        return defaultValue;
    }

    //Save the param value for integer type
    private boolean setIntValue(int param, int value) {
        sdb = getWritableDatabase();
        String sql;
        Object[] data;
        try {
            if (isParamExist(param)) {
                sql = "update t_settings set f_int_value = ? where f_param = ?";
                data = new Object[]{value, param};
            } else {
                sql = "insert into t_settings (f_param, f_str_value, f_int_value) values(?, ?, ?)";
                data = new Object[]{param, "", value};
            }

            sdb.execSQL(sql, data);
            return true;
        } catch (Exception ex) {
            ex.printStackTrace();
        }

        return false;
    }

    //Get the param value for String type
    private String getStringValue(int param, String defaultValue) {
        sdb = getReadableDatabase();
        String sql = "select f_param, f_str_value from t_settings where f_param = " + param;
        Cursor cursor = null;
        try {
            String ret = defaultValue;

            cursor = sdb.rawQuery(sql, null);
            if (cursor.getCount() > 0) {
                cursor.moveToFirst();
                ret = cursor.getString(cursor.getColumnIndex("f_str_value"));
            }

            return ret;
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            if (cursor != null) cursor.close();
        }

        return defaultValue;
    }

    //Save the param value for integer type
    private boolean setStringValue(int param, String value) {
        sdb = getWritableDatabase();
        String sql;
        Object[] data;
        try {
            if (isParamExist(param)) {
                sql = "update t_settings set f_str_value = ? where f_param = ?";
                data = new Object[]{value, param};
            } else {
                sql = "insert into t_settings (f_param, f_str_value, f_int_value) values(?, ?, ?)";
                data = new Object[]{param, value, 0};
            }

            sdb.execSQL(sql, data);
            return true;
        } catch (Exception ex) {
            ex.printStackTrace();
        }

        return false;
    }

    //Save account information
    public boolean saveUserProfile(UserProfile profile) {
        boolean b1 = setStringValue(_PARAM_ACCOUNT_USER_ID, profile._userId);
        boolean b2 = setStringValue(_PARAM_ACCOUNT_USER_NAME, profile._name);
        boolean b3 = setStringValue(_PARAM_ACCOUNT_USER_AVATAR, profile._avatar);
        boolean b4 = setStringValue(_PARAM_ACCOUNT_USER_BIRTHDAY, profile._birthday);
        boolean b5 = setStringValue(_PARAM_ACCOUNT_USER_GENDER, profile._gender);
        boolean b6 = setStringValue(_PARAM_ACCOUNT_USER_LOCALE, profile._locale);

        return b1 && b2 && b3 && b4 && b5 && b6;
    }

    public boolean saveUserToken(Authorization.UserToken userToken) {
        boolean b1 = setStringValue(_PARAM_ACCOUNT_USER_ACCESS_TOKEN, userToken.accessToken);
        boolean b2 = setStringValue(_PARAM_ACCOUNT_CLIENT_ID, userToken.clientId);
        boolean b3 = setStringValue(_PARAM_ACCOUNT_USER_EXPIRE_IN, userToken.expireIn);
        boolean b4 = setStringValue(_PARAM_ACCOUNT_USER_REFRESH_TOKEN, userToken.refreshToken);
        boolean b5 = setStringValue(_PARAM_ACCOUNT_USER_ID, userToken.userId);
        return b1 && b2 && b3 && b4 && b5;
    }

    public boolean saveClientToken(Authorization.ClientToken clientToken) {
        boolean b1 = setStringValue(_PARAM_ACCOUNT_CLIENT_ACCESS_TOKEN, clientToken.accessToken);
        boolean b2 = setStringValue(_PARAM_ACCOUNT_CLIENT_ID, clientToken.clientId);
        boolean b3 = setStringValue(_PARAM_ACCOUNT_CLIENT_EXPIRE_IN, clientToken.expireIn);
        boolean b4 = setStringValue(_PARAM_ACCOUNT_CLIENT_REFRESH_TOKEN, clientToken.refreshToken);

        return b1 && b2 && b3 && b4;
    }

    //Get account base information
    public UserProfile loadUserProfile() {
        UserProfile userProfile = new UserProfile();

        userProfile._userId = getStringValue(_PARAM_ACCOUNT_USER_ID, "");
        userProfile._name = getStringValue(_PARAM_ACCOUNT_USER_NAME, "");
        userProfile._avatar = getStringValue(_PARAM_ACCOUNT_USER_AVATAR, "");
        userProfile._birthday = getStringValue(_PARAM_ACCOUNT_USER_BIRTHDAY, "");
        userProfile._gender = getStringValue(_PARAM_ACCOUNT_USER_GENDER, "");
        userProfile._locale = getStringValue(_PARAM_ACCOUNT_USER_LOCALE, "");
        return userProfile;
    }

    //Get device token
    public Authorization.ClientToken loadClientToken() {
        Authorization.ClientToken clientToken = new Authorization.ClientToken();

        clientToken.accessToken = getStringValue(_PARAM_ACCOUNT_CLIENT_ACCESS_TOKEN, "");
        clientToken.clientId = getStringValue(_PARAM_ACCOUNT_CLIENT_ID, "");
        clientToken.expireIn = getStringValue(_PARAM_ACCOUNT_CLIENT_EXPIRE_IN, "");
        clientToken.refreshToken = getStringValue(_PARAM_ACCOUNT_CLIENT_REFRESH_TOKEN, "");
        return clientToken;
    }

    //Get user token
    public Authorization.UserToken loadUserToken() {
        Authorization.UserToken userToken = new Authorization.UserToken();

        userToken.accessToken = getStringValue(_PARAM_ACCOUNT_USER_ACCESS_TOKEN, "");
        userToken.clientId = getStringValue(_PARAM_ACCOUNT_CLIENT_ID, "");
        userToken.expireIn = getStringValue(_PARAM_ACCOUNT_USER_EXPIRE_IN, "");
        userToken.refreshToken = getStringValue(_PARAM_ACCOUNT_USER_REFRESH_TOKEN, "");
        userToken.userId = getStringValue(_PARAM_ACCOUNT_USER_ID, "");
        return userToken;
    }
}
