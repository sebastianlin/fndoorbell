package com.fusionnextinc.doorbell.objects;


import java.util.ArrayList;

/**
 * Created by Mike Chang on 2018/8/13
 */

public class FNGroup {
    protected final static String TAG = "FNGroup";

    private boolean mIsDeviceOn;
    private int mGroupId;
    private String mGroupType;
    private String mGroupName;
    private int mDeviceCount;

    public FNGroup(boolean isDeviceOn, int groupId, String groupType, String groupName, int deviceCount) {
        mIsDeviceOn = isDeviceOn;
        mGroupId = groupId;
        mGroupType = groupType;
        mGroupName = groupName;
        mDeviceCount = deviceCount;
    }

    public void clean() {
        mIsDeviceOn = false;
        mGroupId = mGroupId;
        mGroupType = "";
        mGroupName = "";
        mDeviceCount = mDeviceCount;
    }

    public boolean isDeviceOn() {
        return mIsDeviceOn;
    }

    public int getGroupId() {
        return mGroupId;
    }

    public String getGroupType() {
        return mGroupType;
    }

    public String getGroupName() {
        return mGroupName;
    }

    public void setDeviceCount(int deviceCount) {
        this.mDeviceCount = deviceCount;
    }

    public int getDeviceCount() {
        return mDeviceCount;
    }

}
