package com.fusionnextinc.doorbell.widget;

import android.app.Activity;
import android.content.Context;

import com.fusionnext.cloud.iot.objects.ErrorObj;
import com.fusionnextinc.doorbell.R;
import com.fusionnextinc.doorbell.objects.FNError;
import com.fusionnextinc.doorbell.widget.FNAlert.FNAlertDialog;

/**
 * Created by Mike Chang on 2017/8/10
 */
public class FNDialog {
    private static final String TAG = "FNErrorDialog";

    public static void showNoNetworkDialog(Context context) {
        new FNAlertDialog(context, FNAlertDialog.WARNING_TYPE)
                .setTitleText(context.getString(R.string.fn_dialog_no_network_title))
                .setContentText(context.getString(R.string.fn_dialog_no_network_description))
                .setConfirmText(context.getString(R.string.fn_dialog_ok))
                .showCancelButton(false)
                .setConfirmClickListener(new FNAlertDialog.OnSweetClickListener() {
                    @Override
                    public void onClick(FNAlertDialog sDialog) {
                        sDialog.dismiss();
                    }
                })
                .show();
    }

    public static void showSuccessDialog(Context context, String title) {
        new FNAlertDialog(context, FNAlertDialog.SUCCESS_TYPE)
                .setTitleText(title)
                .setConfirmText(context.getString(R.string.fn_dialog_ok))
                .showCancelButton(false)
                .setConfirmClickListener(new FNAlertDialog.OnSweetClickListener() {
                    @Override
                    public void onClick(FNAlertDialog sDialog) {
                        sDialog.dismiss();
                    }
                })
                .show();
    }

    public static void showSuccessDialog(Context context, String title, String message) {
        new FNAlertDialog(context, FNAlertDialog.SUCCESS_TYPE)
                .setTitleText(title)
                .setContentText(message)
                .setConfirmText(context.getString(R.string.fn_dialog_ok))
                .showCancelButton(false)
                .setConfirmClickListener(new FNAlertDialog.OnSweetClickListener() {
                    @Override
                    public void onClick(FNAlertDialog sDialog) {
                        sDialog.dismiss();
                    }
                })
                .show();
    }

    public static void showSuccessDialog(Context context, String title, String message, FNAlertDialog.OnSweetClickListener listener) {
        new FNAlertDialog(context, FNAlertDialog.SUCCESS_TYPE)
                .setTitleText(title)
                .setContentText(message)
                .setConfirmText(context.getString(R.string.fn_dialog_ok))
                .showCancelButton(false)
                .setConfirmClickListener(listener)
                .show();
    }

    public static void showErrorDialog(Context context, String title, String message) {

        ((Activity) context).runOnUiThread(() -> new FNAlertDialog(context, FNAlertDialog.ERROR_TYPE)
                .setTitleText(title)
                .setContentText(message)
                .setConfirmText(context.getString(R.string.fn_dialog_ok))
                .showCancelButton(false)
                .setConfirmClickListener(new FNAlertDialog.OnSweetClickListener() {
                    @Override
                    public void onClick(FNAlertDialog sDialog) {
                        sDialog.dismiss();
                    }
                })
                .show());
    }

    public static void showErrorDialog(Context context, String title, FNError fnError) {

        ((Activity) context).runOnUiThread(() -> new FNAlertDialog(context, FNAlertDialog.ERROR_TYPE)
                .setTitleText(title)
                .setContentText(transferErrorCodeToString(context, fnError))
                .setConfirmText(context.getString(R.string.fn_dialog_ok))
                .showCancelButton(false)
                .setConfirmClickListener(new FNAlertDialog.OnSweetClickListener() {
                    @Override
                    public void onClick(FNAlertDialog sDialog) {
                        sDialog.dismiss();
                    }
                })
                .show());

    }

    public static void showErrorDialog(Context context, String title, FNError fnError, FNAlertDialog.OnSweetClickListener listener) {
        ((Activity) context).runOnUiThread(() -> new FNAlertDialog(context, FNAlertDialog.ERROR_TYPE)
                .setTitleText(title)
                .setContentText(transferErrorCodeToString(context, fnError))
                .setConfirmText(context.getString(R.string.fn_dialog_ok))
                .showCancelButton(false)
                .setConfirmClickListener(listener)
                .show());
    }

    private static String transferErrorCodeToString(Context context, FNError fnError) {
        if (fnError != null) {
            switch (fnError.getErrorCode()) {
                //General Error
                case ErrorObj.ERROR_GENERAL_INSUFFICIENT_PERMISSIONS:
                    return context.getString(R.string.fn_msg_error_general_insufficient_permission);
                case ErrorObj.ERROR_GENERAL_THING_NOT_EXIST:
                    return context.getString(R.string.fn_msg_error_general_thing_not_exist);
                case ErrorObj.ERROR_GENERAL_NOT_THING_OWNER:
                    return context.getString(R.string.fn_msg_error_general_not_thing_owner);
                case ErrorObj.ERROR_GENERAL_USER_NOT_EXIST:
                    return context.getString(R.string.fn_msg_error_general_user_not_exist);
                case ErrorObj.ERROR_GENERAL_USER_NOT_FOUND:
                    return context.getString(R.string.fn_msg_error_general_user_not_found);
                case ErrorObj.ERROR_GENERAL_ACCOUNT_FORMAT_WRONG:
                    return context.getString(R.string.fn_msg_error_general_account_format_wrong);
                case ErrorObj.ERROR_GENERAL_SORT_OPTION_INVALID:
                    return context.getString(R.string.fn_msg_error_general_sort_option_invalid);
                case ErrorObj.ERROR_GENERAL_ACCOUNT_ERROR:
                    return context.getString(R.string.fn_msg_error_general_account_error);
                case ErrorObj.ERROR_GENERAL_UNKNOWN:
                    return context.getString(R.string.fn_msg_error_general_unknown);
                //System Error
                case ErrorObj.ERROR_SYSTEM_INTERNET_NOT_AVALIABLE:
                    return context.getString(R.string.fn_msg_error_system_internet_not_avaliable);
                case ErrorObj.ERROR_SYSTEM_REQUEST_TIMEOUT:
                    return context.getString(R.string.fn_msg_error_system_internet_request_timeout);
                //Regester Error
                case ErrorObj.ERROR_REGISTER_EMAIL_HAS_BEEN_TAKEN:
                    return context.getString(R.string.fn_msg_error_register_email_has_been_taken);
                case ErrorObj.ERROR_REGISTER_PASSWORD_FORMAT_WRONG:
                    return context.getString(R.string.fn_msg_error_register_password_format_wrong);
                //Login Error
                case ErrorObj.ERROR_LOGIN_CREATE_DEVICE_FAIL:
                    return context.getString(R.string.fn_msg_error_login_create_device_fail).replace("${Thing}", context.getString(R.string.fn_title_thing_name));
                case ErrorObj.ERROR_LOGIN_IDENTIFIER_NOT_AVAILABLE:
                    return context.getString(R.string.fn_msg_error_login_identifier_not_available);
                case ErrorObj.ERROR_LOGIN_PASSWORD_WRONG:
                    return context.getString(R.string.fn_msg_error_login_password_wrong);
                case ErrorObj.ERROR_LOGIN_USER_ALREADY_LOGIN:
                    return context.getString(R.string.fn_msg_error_login_user_already_login);
                case ErrorObj.ERROR_LOGIN_EMAIL_NOT_FOUND:
                    return context.getString(R.string.fn_msg_error_login_emai_not_found);
                case ErrorObj.ERROR_LOGIN_EMAIL_NOT_ACTIVATE:
                    return context.getString(R.string.fn_msg_error_login_emai_not_active);
                //Logout Error
                case ErrorObj.ERROR_LOGOUT_NO_USER_LOGIN:
                    return context.getString(R.string.fn_msg_error_logout_no_user_login);
                case ErrorObj.ERROR_LOGOUT_DEVICE_NOT_FOUND:
                    return context.getString(R.string.fn_msg_error_logout_device_not_found).replace("${Thing}", context.getString(R.string.fn_title_thing_name));
                //Set or Reset Password Error
                case ErrorObj.ERROR_CHANGE_PASSWORD_ACCOUNT_NOT_MATCH:
                    return context.getString(R.string.fn_msg_error_change_password_account_not_match);
                case ErrorObj.ERROR_CHANGE_PASSWORD_TOKEN_INVALID:
                    return context.getString(R.string.fn_msg_error_change_password_token_invalid);
                //User Profile  Error
                case ErrorObj.ERROR_USERPROFILE_OLD_PASSWORD_WRONG:
                    return context.getString(R.string.fn_msg_error_userprofile_old_password_wrong);
                case ErrorObj.ERROR_USERPROFILE_USER_NOT_LOGIN:
                    return context.getString(R.string.fn_msg_error_userprofile_user_not_login);
                //Creatre Thing  Error
                case ErrorObj.ERROR_CREATE_THING_ALREADY_EXIST:
                    return context.getString(R.string.fn_msg_error_create_thing_already_exist).replace("${Thing}", context.getString(R.string.fn_title_thing_name));
                case ErrorObj.ERROR_CREATE_THING_UNKNOWN_PRODUCT:
                    return context.getString(R.string.fn_msg_error_create_thing_unknown_product);
                case ErrorObj.ERROR_CREATE_THING_IDENTIFIER_FORMAT_ERROR:
                    return context.getString(R.string.fn_msg_error_create_thing_identifier_format_error);
                //Update/Delete Thing  Error
                case ErrorObj.ERROR_UPDATE_THING_NOT_THING_OWNER:
                    return context.getString(R.string.fn_msg_error_update_thing_not_thing_owner);
                case ErrorObj.ERROR_DELETE_THING_NOT_THING_OWNER:
                    return context.getString(R.string.fn_msg_error_delete_thing_not_thing_owner);
                //Invite User Error
                case ErrorObj.ERROR_INVITE_USER_NOT_THING_OWNER:
                    return context.getString(R.string.fn_msg_error_invite_user_not_thing_owner);
                case ErrorObj.ERROR_INVITE_USER_ALREADY_INVITED:
                    return context.getString(R.string.fn_msg_error_invite_user_already_invited);
                case ErrorObj.ERROR_INVITE_USER_SELF_NOT_ALLOW:
                    return context.getString(R.string.fn_msg_error_invite_user_self_not_allow);
                //Accept/Reject User Error
                case ErrorObj.ERROR_ACCEPT_REJECT_NOT_INVITED:
                    return context.getString(R.string.fn_msg_error_accept_reject_not_invited);
                case ErrorObj.ERROR_ACCEPT_REJECT_THING_NOT_EXIST:
                    return context.getString(R.string.fn_msg_error_accept_reject_thing_not_exist);
                //List State Error
                case ErrorObj.ERROR_LIST_STATE_NOT_THING_OWNER:
                    return context.getString(R.string.fn_msg_error_list_state_not_thing_owner);
                //List Setting Error
                case ErrorObj.ERROR_LIST_SETTING_NOT_THING_OWNER:
                    return context.getString(R.string.fn_msg_error_list_setting_not_thing_owner);
                //List Setting Error
                case ErrorObj.ERROR_LIST_ITEM_NOT_THING_OWNER:
                    return context.getString(R.string.fn_msg_error_list_item_not_thing_owner);
                case ErrorObj.ERROR_CALL_SLOT_HAS_ALREADY_BEEN_TAKEN:
                    return context.getString(R.string.fn_msg_error_call_slot_has_been_taken);
                default:
                    return context.getString(R.string.fn_msg_error_general_unknown);
            }
        }
        return context.getString(R.string.fn_msg_error_general_unknown);
    }
}
