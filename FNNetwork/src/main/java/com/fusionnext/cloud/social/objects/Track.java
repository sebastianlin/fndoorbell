package com.fusionnext.cloud.social.objects;

import android.util.Log;

import com.fusionnext.cloud.datalink.json.FNJsonArray;
import com.fusionnext.cloud.datalink.json.FNJsonObject;
import com.fusionnext.cloud.social.Public;

import org.json.JSONException;

import java.util.ArrayList;

/**
 * Created by Mike Chang on 2016/1/19.
 */
public class Track {
    protected final static String _TAG = "Track";
    public final static int TRACK_TYPE_NONE = 0;

    public Track() {
        _type = TRACK_TYPE_NONE;
        _trackId = "";
        _userId = "";
        _title = "";
        _description = "";
        _place = "";
        _cover = "";
        _days = "";
        _distance = "";
        _privacy = "";
        _createdAt = "";
        _updatedAt = "";
        _commentsMeta = new CommentsMeta();
        _likesMeta = new LikesMeta();
        _userProfile = new UserProfile();
    }

    public int _type;
    public String _trackId;
    public String _userId;
    public String _title;
    public String _description;
    public String _place;
    public String _cover;
    public String _days;
    public String _distance;
    public String _privacy;
    public String _createdAt;
    public String _updatedAt;
    public LikesMeta _likesMeta;
    public CommentsMeta _commentsMeta;
    public UserProfile _userProfile;

    public void clean() {
        _type = TRACK_TYPE_NONE;
        _trackId = "";
        _userId = "";
        _title = "";
        _description = "";
        _place = "";
        _cover = "";
        _days = "";
        _distance = "";
        _privacy = "";
        _createdAt = "";
        _updatedAt = "";
        _commentsMeta.clean();
        _likesMeta.clean();
        _userProfile.clean();
    }

    public boolean getDataFromJSONObject(FNJsonObject trackObject) {
        clean();
        if (trackObject == null) {
            Log.w(_TAG, " No JSON information");
            return false;
        }
        try {
            _trackId = trackObject.getStringDefault("id");
            _userId = trackObject.getStringDefault("user_id");
            _title = trackObject.getStringDefault("title");
            _description = trackObject.getStringDefault("description");
            _place = trackObject.getStringDefault("place");

            FNJsonObject coverObj = trackObject.getJSONObject("cover");
            if (coverObj != null) {
                _cover = Public._HOST_V1 + coverObj.getStringDefault("href");
            }
            _days = trackObject.getStringDefault("days");
            _distance = trackObject.getStringDefault("distance");
            _privacy = trackObject.getStringDefault("privacy");
            _createdAt = trackObject.getStringDefault("created_at");
            _updatedAt = trackObject.getStringDefault("updated_at");

            FNJsonObject commentsMetaObj = trackObject.getJSONObject("commentsmeta");
            if (commentsMetaObj == null) {
                Log.i(_TAG, "No commentsmeta tag");
                //return false;
            }
            _commentsMeta.getDataFromJSONObject(commentsMetaObj);

            FNJsonObject likesMetaObj = trackObject.getJSONObject("likesmeta");
            if (likesMetaObj == null) {
                Log.i(_TAG, "No likesmeta tag");
                //return false;
            }
            _likesMeta.getDataFromJSONObject(likesMetaObj);

            FNJsonObject userObj = trackObject.getJSONObject("user");
            if (userObj == null) {
                Log.i(_TAG, "No user tag");
                //return false;
            }
            //Get the user information
            _userProfile.getDataFromJSONObject(userObj);
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
        return true;
    }


    public static Track parseTrack(String json) {
        try {
            FNJsonObject trackObject = new FNJsonObject(json);

            Track track = new Track();
            track.getDataFromJSONObject(trackObject);
            return track;
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static ArrayList<Track> parseTrackArray(String json) {
        try {
            ArrayList<Track> trackList = new ArrayList<Track>();
            FNJsonObject trackObject = new FNJsonObject(json);
            FNJsonArray tracksArray = trackObject.getJSONArray("items");
            if (tracksArray == null) {
                return null;
            }
            for (int i = 0; i < tracksArray.length(); i++) {
                FNJsonObject object = tracksArray.getJSONObject(i);
                Track track = new Track();
                track.getDataFromJSONObject(object);
                if (track == null) return null;
                trackList.add(track);
            }
            return trackList;
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return null;
    }

}
