package com.fusionnextinc.doorbell.manager.qrCode.qrcodeReaderView;

public enum Orientation {
  PORTRAIT, LANDSCAPE
}
