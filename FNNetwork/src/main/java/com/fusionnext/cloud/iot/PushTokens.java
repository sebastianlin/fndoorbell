package com.fusionnext.cloud.iot;

import android.content.Context;

import com.fusionnext.cloud.datalink.http.HttpPost;
import com.fusionnext.cloud.iot.objects.ErrorObj;
import com.fusionnext.cloud.iot.objects.PushToken;

import java.util.HashMap;

/**
 * Created by Mike Chang on 2016/4/13.
 */
public class PushTokens {
    protected final static String _TAG = "PushTokens";
    private Context context;

    public PushTokens(Context context) {
        this.context = context;
    }

    public void createPushToken(final PushToken pushToken, final IOTHandler.OnPushTokensListener onPushTokensListener) {
        if (pushToken == null) {
            return;
        }
        HttpPost.PostParam[] params = new HttpPost.PostParam[1];
        params[0] = new HttpPost.PostParam();
        params[0].accessToken = Public.getAuthorization(context).getUserAccessToken();
        params[0].url = HttpData.generateHttpPostUrl(HttpData._POST_TYPE_PUSHTOKENS_CREATE_PUSHTOKEN);
        params[0].type = HttpData._POST_TYPE_PUSHTOKENS_CREATE_PUSHTOKEN;
        params[0].param = new HashMap<>();
        params[0].param.put("device_id", Public.getDevices(context).getDeviceId());
        params[0].param.put("type", pushToken._type);
        params[0].param.put("token", pushToken._pushToken);

        HttpPost post = new HttpPost();
        post.setContext(context);
        post.setOnParseJsonListener(new HttpPost.OnParseJsonListener() {
            @Override
            public void onParse(boolean returnResult, int postType, String id, String returnStr) {
                ErrorObj errorObj = null;

                if (returnStr != null && !returnResult) {
                    errorObj = ErrorObj.parseErrorObj(returnStr);
                }
                onPushTokensListener.onResult(returnResult, id, postType, errorObj);
            }
        });
        post.execute(params);
    }

}
