package com.fusionnextinc.doorbell.fragement;

import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.ListView;

import com.fusionnext.cloud.social.HttpData;
import com.fusionnextinc.doorbell.Adapter.ScheduleListAdapter;
import com.fusionnextinc.doorbell.MyApplication;
import com.fusionnextinc.doorbell.R;
import com.fusionnextinc.doorbell.manager.data.AppDataBase;
import com.fusionnextinc.doorbell.manager.data.AppDataSchedule;
import com.fusionnextinc.doorbell.manager.data.ListMap;
import com.fusionnextinc.doorbell.manager.thingsManager.ThingsHandler;
import com.fusionnextinc.doorbell.manager.thingsManager.ThingsManager;
import com.fusionnextinc.doorbell.objects.FNError;
import com.fusionnextinc.doorbell.objects.FNEvent;
import com.fusionnextinc.doorbell.objects.FNFirmware;
import com.fusionnextinc.doorbell.objects.FNSchedule;
import com.fusionnextinc.doorbell.objects.FNThing;
import com.fusionnextinc.doorbell.objects.FNUserProfile;
import com.fusionnextinc.doorbell.widget.FNDialog;
import com.fusionnextinc.doorbell.widget.FNLayoutUtil;
import com.fusionnextinc.doorbell.widget.FNProgressBarCustom;

import java.util.ArrayList;
import java.util.Map;

/**
 * Created by Mike Chang on 2018/8/10
 */
public class ScheduleListFragment extends FNFragment implements AbsListView.OnItemClickListener {
    private static final String TAG = ScheduleListFragment.class.getSimpleName();

    private FNLayoutUtil mLayoutUtil;
    private ScheduleListAdapter mScheduleListAdapter = null;
    private ListView mScheduleListView;
    private FNThing mCurFNThing;
    private ThingsManager mThingsManager;
    private FNProgressBarCustom mProgressBar;

    public static void openFragment(boolean clearOtherStack, FNThing curFNThing) {
        ScheduleListFragment scheduleListFragment = new ScheduleListFragment();
        FNFragmentManager.getInstance().openFragment(scheduleListFragment, clearOtherStack);
        scheduleListFragment.mCurFNThing = curFNThing;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mLayoutUtil = new FNLayoutUtil(getActivity(), MyApplication.LAYOUT_WIDTH, MyApplication.LAYOUT_HEIGHT, FNLayoutUtil.BASE_WIDTH);
        mThingsManager = ThingsManager.getInstance(getActivity());
    }

    @Override
    public void onResume() {
        super.onResume();
        initScheduleList();
        mThingsManager.setThingsListener(mThingsListener);
        ThingsManager.getInstance(getActivity()).getSettingsList(mCurFNThing.getThingId(),
                mThingsManager.SETTINGS_SMARTPLUG_SCHEDULE, HttpData._GET_TYPE_SORT_ORDER_DESC, null, null);
        startWait();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        final ViewGroup nullParent = null;

        getActivity().setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        refreshActionBar(false);
        View view = inflater.inflate(R.layout.fragment_schedule_list, nullParent);
        mLayoutUtil.fitAllView(view);
        mScheduleListView = (ListView) view.findViewById(R.id.schedule_list);
        mScheduleListView.setOnItemClickListener(this);
        mScheduleListView.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> adapterView, View view, int i, long l) {
                mScheduleListAdapter.setLongPressed(true);
                refreshActionBar(true);
                return true;
            }
        });
        mProgressBar = new FNProgressBarCustom(getActivity(), (ViewGroup) view.findViewById(R.id.fragment_schedule_list));
        initScheduleList();
        //appDataThing.loadAllThingsList(true, true);
        return view;
    }

    private void startWait() {
        mProgressBar.show();
    }

    private void stopWait() {
        mProgressBar.hide();
    }

    private boolean initScheduleList() {
        if (mScheduleListView == null) return false;
        ListMap scheduleList = AppDataSchedule.getInstance(getActivity()).getAdapterData(AppDataBase.SCHEDULE_TYPE_NORMAL, mCurFNThing.getThingId());
        if (mScheduleListAdapter == null) {
            //Setup the adapter
            mScheduleListAdapter = new ScheduleListAdapter(getActivity(), mCurFNThing.getThingId(), scheduleList);
        } else {
            mScheduleListAdapter.updateScheduleList(scheduleList);
        }
        mScheduleListView.setAdapter(mScheduleListAdapter);
        return true;
    }

    @Override
    public void onItemClick(AdapterView<?> adapterView, View view, final int i, long l) {
        if (mScheduleListAdapter != null && mScheduleListAdapter.isLongPressed()) {
            mScheduleListAdapter.setSelectChkBox(i);
        } else {
            Map<String, Object> mapSchedule = (Map<String, Object>) mScheduleListAdapter.getItem(i);
            int scheduleId = (int) mapSchedule.get(AppDataSchedule.TAG_SCHEDULE_ID);
            FNSchedule fnSchedule = AppDataSchedule.getInstance(getActivity()).loadSchedule(mCurFNThing.getThingId(), scheduleId);
            AddTimeFragment.openFragment(false, mCurFNThing, fnSchedule);
        }
    }

    @Override
    public void onBackPressed() {
        if (mScheduleListAdapter != null && mScheduleListAdapter.isLongPressed()) {
            mScheduleListAdapter.setLongPressed(false);
            refreshActionBar(false);
        } else {
            super.onBackPressed(); // Comment this super call to avoid calling finish() or fragmentmanager's backstack pop operation.
        }
    }

    private void refreshActionBar(boolean isLongPressed) {
        getActionBar().resetAll();
        getActionBar().setTitle(getString(R.string.fn_txt_plug_schedule), Gravity.CENTER, null);
        getActionBar().setCoverMode(false);
        getActionBar().setStartImage(R.drawable.back, new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        if (isLongPressed) {
            getActionBar().setEndText(getString(R.string.btn_delete), getResources().getColor(R.color.button_text_color), new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (mScheduleListAdapter != null) {
                        ArrayList<Integer> scheduleList = mScheduleListAdapter.getSelectedScheduleList();
                        for (Integer scheduleId : scheduleList) {
                            AppDataSchedule.getInstance(getActivity()).deleteSchedule(AppDataBase.SCHEDULE_TYPE_NORMAL, mCurFNThing.getThingId(), String.valueOf(scheduleId));
                        }
                        initScheduleList();
                        mScheduleListAdapter.setLongPressed(false);
                        refreshActionBar(false);
                        mScheduleListAdapter.sendScheduleSettings();
                    }
                }
            });
        } else {
            getActionBar().addEndImage(R.drawable.btn_add_thing, new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    AddTimeFragment.openFragment(false, mCurFNThing, null);
                }
            });
        }
        getActionBar().show();
    }

    final ThingsHandler.ThingsListener mThingsListener = new ThingsHandler.ThingsListener() {
        @Override
        public void onListUserOwnedThings(ThingsManager instance, boolean result, String userId, ArrayList<FNThing> thingArrayList, FNError error) {

        }

        @Override
        public void onListUserInvitedThings(ThingsManager instance, boolean result, String userId, ArrayList<FNThing> thingArrayList, FNError error) {

        }

        @Override
        public void onListUserSharedThings(ThingsManager instance, boolean result, String userId, ArrayList<FNThing> thingArrayList, FNError error) {

        }

        @Override
        public void onListUserAllThings(ThingsManager instance, boolean result, String userId, ArrayList<FNThing> ownedThingArrayList, ArrayList<FNThing> sharedThingArrayList, ArrayList<FNThing> invitedThingArrayList, FNError error) {

        }

        @Override
        public void onListThingEvents(ThingsManager instance, boolean result, String thingId, String eventName, ArrayList<FNEvent> eventList, FNError error) {

        }

        @Override
        public void onListThingEventsByTime(ThingsManager instance, boolean result, String thingId, String eventName, String startTime, String endTime, ArrayList<FNEvent> eventList, FNError error) {

        }

        @Override
        public void onCurBatteryLevel(ThingsManager instance, boolean result, String thingId, String batteryLevel, FNError error) {

        }

        @Override
        public void onCurFirmwareVersion(ThingsManager instance, boolean result, String thingId, String fwVersion, FNError error) {

        }

        @Override
        public void onCurSwitchedStatus(ThingsManager instance, boolean result, String thingId, boolean isSwitch, FNError error) {

        }

        @Override
        public void onNewFirmwareVersion(ThingsManager instance, boolean result, String thingId, FNFirmware fnFirmware) {

        }

        @Override
        public void onStartFwUpdate(ThingsManager instance, boolean result, String thingId, String fwId) {

        }

        @Override
        public void onCurRingTone(ThingsManager instance, boolean result, String thingId, String ringtoneId, FNError error) {

        }

        @Override
        public void onGetScheduleList(ThingsManager instance, boolean result, String thingId, ArrayList<FNSchedule> scheduleList, FNError error) {
            if (result) {
                initScheduleList();
            }
            stopWait();
        }

        @Override
        public void onListSharedUsers(ThingsManager instance, boolean result, String thingId, ArrayList<FNUserProfile> userList, FNError error) {

        }

        @Override
        public void onCreateClaimCode(ThingsManager instance, boolean result, String claimCode, FNError error) {

        }

        @Override
        public void onCreateThing(ThingsManager instance, boolean result, FNThing fnThing, FNError error) {

        }

        @Override
        public void onUpdateThing(ThingsManager instance, boolean result, FNThing fnThing, FNError error) {

        }

        @Override
        public void onChangeRingtone(ThingsManager instance, boolean result, String thingId, int ringtoneId) {

        }

        @Override
        public void onSetTimer(ThingsManager instance, boolean result, String thingId, String value) {

        }

        @Override
        public void onSetSchedule(ThingsManager instance, boolean result, String thingId, String value) {
            if (result) {
                FNDialog.showSuccessDialog(getActivity(), getString(R.string.fn_txt_plug_schedule), getString(R.string.fn_msg_schedule_change_successfully));

            } else {
                FNDialog.showErrorDialog(getActivity(), getString(R.string.fn_txt_plug_schedule), getString(R.string.fn_msg_schedule_change_failed));
            }
        }

        @Override
        public void onShareInvited(ThingsManager instance, boolean result, String thingId, FNError error) {

        }

        @Override
        public void onShareAccepted(ThingsManager instance, boolean result, String thingId, FNError error) {

        }

        @Override
        public void onShareRejected(ThingsManager instance, boolean result, String thingId, FNError error) {

        }

        @Override
        public void onSwitchOnOff(ThingsManager instance, boolean result, String thingId, boolean isTurnOn) {

        }

        @Override
        public void onCloseCallSlot(ThingsManager instance, boolean result, FNError error) {

        }

        @Override
        public void onTakeCallSlot(ThingsManager instance, boolean result, String thingId, String callSlot, FNError error) {

        }

        @Override
        public void onListInvitedUsers(ThingsManager instance, boolean result, String thingId, ArrayList<FNUserProfile> userList, FNError error) {

        }

        @Override
        public void onDeleteThing(ThingsManager instance, final boolean result, final FNError error) {

        }

        @Override
        public void onGetThing(ThingsManager instance, boolean result, FNThing fnThing, FNError error) {

        }
    };
}
