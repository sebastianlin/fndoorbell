package com.fusionnextinc.doorbell.fragement;

import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;

import com.fusionnextinc.doorbell.Adapter.GroupListAdapter;
import com.fusionnextinc.doorbell.MyApplication;
import com.fusionnextinc.doorbell.R;
import com.fusionnextinc.doorbell.manager.data.AppDataBase;
import com.fusionnextinc.doorbell.manager.data.AppDataGroup;
import com.fusionnextinc.doorbell.manager.data.ListMap;
import com.fusionnextinc.doorbell.widget.FNLayoutUtil;

import java.util.ArrayList;

/**
 * Created by Mike Chang on 2018/8/7
 */
public class GroupsFragment extends FNFragment {
    private static final String TAG = GroupsFragment.class.getSimpleName();

    private FNLayoutUtil mLayoutUtil;
    private GroupListAdapter mGroupListAdapter = null;
    private ListView mGroupsListView;

    public static void openFragment(boolean clearOtherStack) {
        GroupsFragment thingsFragment = new GroupsFragment();
        FNFragmentManager.getInstance().openFragment(thingsFragment, clearOtherStack);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mLayoutUtil = new FNLayoutUtil(getActivity(), MyApplication.LAYOUT_WIDTH, MyApplication.LAYOUT_HEIGHT, FNLayoutUtil.BASE_WIDTH);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        final ViewGroup nullParent = null;

        getActivity().setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        View view = inflater.inflate(R.layout.fragment_groups, nullParent);
        mLayoutUtil.fitAllView(view);
        mGroupsListView = (ListView) view.findViewById(R.id.groups_list);
        initGroupsList();
        return view;
    }

    private View.OnClickListener onAddGroupClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            RoomListFragment.openFragment(false, null);
        }
    };

    private boolean initGroupsList() {
        if (mGroupsListView == null) return false;

        final ListMap groupList = AppDataGroup.getInstance(getActivity()).getAdapterData(AppDataBase.GROUP_TYPE_NORMAL);
        //Setup the adapter
        mGroupListAdapter = new GroupListAdapter(getActivity(), groupList);
        mGroupsListView.setAdapter(mGroupListAdapter);
        mGroupsListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                if (mGroupListAdapter != null && mGroupListAdapter.isLongPressed()) {
                    mGroupListAdapter.setSelectChkBox(i);
                } else {
                    int groupId = (int) groupList.get(i).get(AppDataGroup.TAG_GROUP_ID);
                    GroupThingsFragment.openFragment(false, groupId);
                }
            }
        });
        mGroupsListView.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> adapterView, View view, int i, long l) {
                mGroupListAdapter.setLongPressed(true);
                refreshActionBar(true);
                return true;
            }
        });
        return true;
    }

    private void refreshActionBar(boolean isLongPressed) {
        getActionBar().resetAll();
        getActionBar().setTitle(getString(R.string.fn_title_tabbar_group), Gravity.CENTER, null);
        getActionBar().setCoverMode(false);
        getActionBar().setStartImage(R.drawable.back, new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        getActionBar().addEndImage(R.drawable.btn_add_thing, new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                RoomListFragment.openFragment(false, null);
            }
        });
        if (isLongPressed) {
            getActionBar().resetAll();
            getActionBar().setEndText(getString(R.string.btn_delete), getResources().getColor(R.color.button_text_color), new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (mGroupListAdapter != null) {
                        ArrayList<Integer> selectedList = mGroupListAdapter.getSelectedList();
                        for (Integer groupId : selectedList) {
                            AppDataGroup.getInstance(getActivity()).deleteGroup(AppDataBase.GROUP_TYPE_NORMAL, String.valueOf(groupId));
                        }
                        initGroupsList();
                        mGroupListAdapter.setLongPressed(false);
                        refreshActionBar(false);
                    }
                }
            });
            getActionBar().setStartImage(R.drawable.back, new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    onBackPressed();
                }
            });
        }
        getActionBar().show();
    }

    @Override
    public void onBackPressed() {
        if (mGroupListAdapter != null && mGroupListAdapter.isLongPressed()) {
            mGroupListAdapter.setLongPressed(false);
            getActionBar().resetAll();
            getActionBar().addEndImage(R.drawable.btn_add_thing, onAddGroupClickListener);
        } else {
            super.onBackPressed(); // Comment this super call to avoid calling finish() or fragmentmanager's backstack pop operation.
        }
    }
}
