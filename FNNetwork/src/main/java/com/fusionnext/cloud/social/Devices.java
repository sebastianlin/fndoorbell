package com.fusionnext.cloud.social;

import android.content.Context;

import com.fusionnext.cloud.datalink.http.HttpPost;
import com.fusionnext.cloud.datalink.http.HttpPut;
import com.fusionnext.cloud.social.objects.Device;
import com.fusionnext.cloud.utils.Utils;

import java.util.HashMap;

/**
 * Created by Mike Chang on 2017/3/23
 */
public class Devices {
    protected final static String _TAG = "Devices";
    private Context context;

    public Devices(Context context) {
        this.context = context;
    }

    public void createDevice(final SocialAPIHandler.OnDevicesListener onDevicesListener) {
        String macAddr = Utils.getMacAddress(context);
        String version = Utils.getAPIVerison();
        String deviceName = Utils.getDeviceName(context);
        String model = Utils.getModelName();
        String manufacturer = Utils.getManufacturer(context);

        HttpPost.PostParam[] params = new HttpPost.PostParam[1];
        params[0] = new HttpPost.PostParam();
        params[0].accessToken = Public.getAuthorization(context).getClientAccessToken();
        params[0].url = HttpData.generateHttpPostUrl(HttpData._POST_TYPE_DEVICES_CREATE_DEVICE);
        params[0].type = HttpData._POST_TYPE_DEVICES_CREATE_DEVICE;
        params[0].param = new HashMap<Object, Object>();
        params[0].param.put("identifier", macAddr);
        params[0].param.put("name", deviceName);
        params[0].param.put("manufacturer", manufacturer);
        params[0].param.put("model", model);
        params[0].param.put("platform", "android");
        params[0].param.put("platform_ver", version);

        HttpPost post = new HttpPost();
        post.setContext(context);
        post.setOnParseJsonListener(new HttpPost.OnParseJsonListener() {
            @Override
            public void onParse(boolean returnResult, int postType, String id, String returnStr) {
                onDevicesListener.onResult(returnResult, postType, returnStr);
            }
        });
        post.execute(params);
    }

    public void updateDevice(Device device, final SocialAPIHandler.OnDevicesListener onDevicesListener) {
        if (device == null || device.deviceId == null) {
            return;
        }

        HttpPut.PutParam[] params = new HttpPut.PutParam[1];
        params[0] = new HttpPut.PutParam();
        params[0].accessToken = Public.getAuthorization(context).getClientAccessToken();
        params[0].url = HttpData.generateHttpPutUrl(HttpData._PUT_TYPE_DEVICES_UPDATE_DEVICE, device.deviceId);
        params[0].type = HttpData._PUT_TYPE_DEVICES_UPDATE_DEVICE;
        params[0].param = new HashMap<Object, Object>();

        if (device.deviceIdentifer != null) {
            params[0].param.put("identifier", device.deviceIdentifer);
        }
        if (device.deviceName != null) {
            params[0].param.put("name", device.deviceName);
        }
        if (device.platformVersion != null) {
            params[0].param.put("platform_ver", device.platformVersion);
        }
        if (device.pushToken != null) {
            params[0].param.put("push_token", device.pushToken);
        }

        HttpPut put = new HttpPut();
        put.setHttpRequestType("PUT");
        put.setOnParseJsonListener(new HttpPut.OnParseJsonListener() {
            @Override
            public void onParse(boolean returnResult, int putType, String returnStr) {
                onDevicesListener.onResult(returnResult, putType, returnStr);
            }
        });
        put.setContext(context);
        put.execute(params);
    }

}
