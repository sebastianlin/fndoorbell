package com.fusionnext.cloud.social;

import android.content.Context;

import com.fusionnext.cloud.datalink.http.HttpDelete;
import com.fusionnext.cloud.datalink.http.HttpGet;
import com.fusionnext.cloud.datalink.http.HttpParam;
import com.fusionnext.cloud.datalink.http.HttpPost;
import com.fusionnext.cloud.datalink.http.HttpPut;
import com.fusionnext.cloud.social.objects.Comment;
import com.fusionnext.cloud.social.objects.Likes;
import com.fusionnext.cloud.social.objects.Photo;
import com.fusionnext.cloud.social.objects.Tag;

import java.util.HashMap;

/**
 * Created by Mike Chang on 2016/4/13.
 */
public class Photos {
    protected final static String _TAG = "Photos";
    private Context context;

    public enum PhotoSize {
        PHOTO_SIZE_SMALL, PHOTO_SIZE_NORMAL, PHOTO_SIZE_LARGE
    }

    public Photos(Context context) {
        this.context = context;
    }

    public void listAllPhotos(int order, String pageSize, String pageNum, final SocialAPIHandler.OnPhotosListener onPhotosListener) {
        HttpParam.HttpSubParam param = new HttpParam.HttpSubParam();

        param.accessToken = Public.getAuthorization(context).getUserAccessToken();
        param.url = HttpData.generateHttpGetUrl(HttpData._GET_TYPE_PHOTOS_LIST_ALL_PHOTOS, order, pageSize, pageNum);
        param.cacheName = "";
        HttpParam[] params = new HttpParam[1];
        params[0] = new HttpParam();
        params[0].params.clear();
        params[0].params.add(param);

        HttpGet httpGet = new HttpGet();
        httpGet.setContext(context);
        httpGet.setTag(HttpGet._HTTP_T_GET_NORMAL_TYPE);
        httpGet.setOnParseJsonListener(new HttpGet.OnParseJsonListener() {
            @Override
            public void onParse(boolean result, String returnStr) {
                onPhotosListener.onListAllPhotos(result, Photo.parsePhotoArray(returnStr));
            }
        });
        httpGet.execute(params);
    }

    public void getPhoto(final String photoId, final SocialAPIHandler.OnPhotosListener onPhotosListener) {
        HttpParam.HttpSubParam param = new HttpParam.HttpSubParam();

        param.accessToken = Public.getAuthorization(context).getUserAccessToken();
        param.url = HttpData.generateHttpGetUrl(HttpData._GET_TYPE_PHOTOS_GET_PHOTO, photoId, 0, null, null);
        param.cacheName = "";
        HttpParam[] params = new HttpParam[1];
        params[0] = new HttpParam();
        params[0].params.clear();
        params[0].params.add(param);

        HttpGet httpGet = new HttpGet();
        httpGet.setContext(context);
        httpGet.setTag(HttpGet._HTTP_T_GET_NORMAL_TYPE);
        httpGet.setOnParseJsonListener(new HttpGet.OnParseJsonListener() {
            @Override
            public void onParse(boolean result, String returnStr) {
                onPhotosListener.onGetPhoto(result, photoId, Photo.parsePhoto(returnStr));
            }
        });
        httpGet.execute(params);
    }


    public void savePhoto(String id, final Photo photo, final SocialAPIHandler.OnPhotosListener onPhotosListener) {
        if (photo == null) {
            return;
        }
        HttpPost.PostParam[] params = new HttpPost.PostParam[1];
        params[0] = new HttpPost.PostParam();
        params[0].accessToken = Public.getAuthorization(context).getUserAccessToken();
        params[0].url = HttpData.generateHttpPostUrl(HttpData._POST_TYPE_PHOTOS_SAVE_PHOTO);
        params[0].type = HttpData._POST_TYPE_PHOTOS_SAVE_PHOTO;
        params[0].param = new HashMap<Object, Object>();
        params[0].param.put("track_id", id);
        params[0].param.put("filename", photo._fileName);
        params[0].param.put("description", photo._description);
        params[0].param.put("place", photo._place);
        params[0].param.put("lat", photo._lat);
        params[0].param.put("lng", photo._lng);
        params[0].param.put("captured_at", photo._capturedAt);

        HttpPost post = new HttpPost();
        post.setContext(context);
        post.setOnParseJsonListener(new HttpPost.OnParseJsonListener() {
            @Override
            public void onParse(boolean returnResult, int postType, String id, String returnStr) {
                onPhotosListener.onSavePhoto(returnResult, id, Photo.parsePhoto(returnStr));
            }
        });
        post.execute(params);
    }

    public void updatePhoto(final String photoId, final Photo photo, final SocialAPIHandler.OnPhotosListener onPhotosListener) {
        if (photo == null) {
            return;
        }

        HttpPut.PutParam[] params = new HttpPut.PutParam[1];
        params[0] = new HttpPut.PutParam();
        params[0].accessToken = Public.getAuthorization(context).getUserAccessToken();
        params[0].url = HttpData.generateHttpPutUrl(HttpData._PUT_TYPE_TRACKS_UPDATE_TRACK, photoId);
        params[0].type = HttpData._PUT_TYPE_TRACKS_UPDATE_TRACK;
        params[0].param = new HashMap<Object, Object>();
        params[0].param.put("description", photo._description);
        params[0].param.put("place", photo._place);
        params[0].param.put("lat", photo._lat);
        params[0].param.put("lng", photo._lng);
        params[0].param.put("captured_at", photo._capturedAt);

        HttpPut put = new HttpPut();
        put.setHttpRequestType("PUT");
        put.setOnParseJsonListener(new HttpPut.OnParseJsonListener() {
            @Override
            public void onParse(boolean returnResult, int putType, String returnStr) {
                onPhotosListener.onUpdatePhoto(returnResult, photoId, Photo.parsePhoto(returnStr));
            }
        });
        put.setContext(context);
        put.execute(params);
    }

    public void listComments(final String photoId, int order, String pageSize, String pageNum, final SocialAPIHandler.OnPhotosListener onPhotosListener) {
        HttpParam.HttpSubParam param = new HttpParam.HttpSubParam();

        param.accessToken = Public.getAuthorization(context).getUserAccessToken();
        param.url = HttpData.generateHttpGetUrl(HttpData._GET_TYPE_PHOTOS_LIST_COMMENTS, photoId, order, pageSize, pageNum);
        param.cacheName = "";
        HttpParam[] params = new HttpParam[1];
        params[0] = new HttpParam();
        params[0].params.clear();
        params[0].params.add(param);

        HttpGet httpGet = new HttpGet();
        httpGet.setContext(context);
        httpGet.setTag(HttpGet._HTTP_T_GET_NORMAL_TYPE);
        httpGet.setOnParseJsonListener(new HttpGet.OnParseJsonListener() {
            @Override
            public void onParse(boolean result, String returnStr) {
                onPhotosListener.onListComments(result, photoId, Comment.parseCommentArray(returnStr));
            }
        });
        httpGet.execute(params);
    }

    public void like(String photoId, final SocialAPIHandler.OnPhotosListener onPhotosListener) {
        HttpPost.PostParam[] params = new HttpPost.PostParam[1];
        params[0] = new HttpPost.PostParam();
        params[0].accessToken = Public.getAuthorization(context).getUserAccessToken();
        params[0].url = HttpData.generateHttpPostUrl(HttpData._POST_TYPE_PHOTOS_LIKE, photoId);
        params[0].type = HttpData._POST_TYPE_PHOTOS_LIKE;

        HttpPost post = new HttpPost();
        post.setContext(context);
        post.setOnParseJsonListener(new HttpPost.OnParseJsonListener() {
            @Override
            public void onParse(boolean returnResult, int postType, String id, String returnStr) {
                onPhotosListener.onResult(returnResult, id, postType, returnStr);
            }
        });
        post.execute(params);
    }

    public void unlike(final String photoId, final SocialAPIHandler.OnPhotosListener onPhotosListener) {
        HttpDelete.DelParam[] params = new HttpDelete.DelParam[1];
        params[0] = new HttpDelete.DelParam();
        params[0].accessToken = Public.getAuthorization(context).getUserAccessToken();
        params[0].url = HttpData.generateHttpDeleteUrl(HttpData._DEL_TYPE_PHOTOS_UNLIKE, photoId);
        params[0].type = HttpData._DEL_TYPE_PHOTOS_UNLIKE;
        params[0].param = new HashMap<Object, Object>();

        HttpDelete del = new HttpDelete();
        del.setContext(context);
        del.setOnParseJsonListener(new HttpDelete.OnParseJsonListener() {
            @Override
            public void onParse(boolean returnResult, int delType, String returnStr) {
                if (returnStr != null) {
                    onPhotosListener.onResult(returnResult, photoId, delType, returnStr);
                }
            }
        });
        del.execute(params);
    }

    public void listLikes(final String photoId, int order, String pageSize, String pageNum, final SocialAPIHandler.OnPhotosListener onPhotosListener) {
        HttpParam.HttpSubParam param = new HttpParam.HttpSubParam();
        param.accessToken = Public.getAuthorization(context).getUserAccessToken();
        param.url = HttpData.generateHttpGetUrl(HttpData._GET_TYPE_PHOTOS_LIST_LIKES, photoId, order, pageSize, pageNum);
        param.cacheName = "";
        HttpParam[] params = new HttpParam[1];
        params[0] = new HttpParam();
        params[0].params.clear();
        params[0].params.add(param);

        HttpGet httpGet = new HttpGet();
        httpGet.setContext(context);
        httpGet.setTag(HttpGet._HTTP_T_GET_NORMAL_TYPE);
        httpGet.setOnParseJsonListener(new HttpGet.OnParseJsonListener() {
            @Override
            public void onParse(boolean result, String returnStr) {
                onPhotosListener.onListLikes(result, photoId, Likes.parseLikeArray(returnStr));
            }
        });
        httpGet.execute(params);
    }

    public void listTags(final String photoId, int order, String pageSize, String pageNum, final SocialAPIHandler.OnPhotosListener onSocialAPIListener) {
        HttpParam.HttpSubParam param = new HttpParam.HttpSubParam();

        param.accessToken = Public.getAuthorization(context).getUserAccessToken();
        param.url = HttpData.generateHttpGetUrl(HttpData._GET_TYPE_PHOTOS_LIST_TAGS, photoId, order, pageSize, pageNum);
        param.cacheName = "";
        HttpParam[] params = new HttpParam[1];
        params[0] = new HttpParam();
        params[0].params.clear();
        params[0].params.add(param);

        HttpGet httpGet = new HttpGet();
        httpGet.setContext(context);
        httpGet.setTag(HttpGet._HTTP_T_GET_NORMAL_TYPE);
        httpGet.setOnParseJsonListener(new HttpGet.OnParseJsonListener() {
            @Override
            public void onParse(boolean result, String returnStr) {
                onSocialAPIListener.onListTags(result, photoId, Tag.parseTagArray(returnStr));
            }
        });
        httpGet.execute(params);
    }

    public String getPhotoUrl(String fileName, PhotoSize photoSize) {
        String picUrl = Public._HOST_V1 + "/photos/" + fileName + ".jpg";
        switch (photoSize) {
            case PHOTO_SIZE_LARGE:
                picUrl = picUrl + "?size=large";
                break;
            case PHOTO_SIZE_NORMAL:
                picUrl = picUrl + "?size=medium";
                break;
            case PHOTO_SIZE_SMALL:
                picUrl = picUrl + "?size=small";
                break;
            default:
                break;
        }
        return picUrl;
    }

}
