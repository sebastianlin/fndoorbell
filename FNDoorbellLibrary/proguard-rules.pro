# Add project specific ProGuard rules here.
# By default, the flags in this file are appended to flags specified
# in D:\Users\user\AppData\Local\Android\sdk/tools/proguard/proguard-android.txt
# You can edit the include path and order by changing the proguardFiles
# directive in build.gradle.
#
# For more details, see
#   http://developer.android.com/guide/developing/tools/proguard.html

# Add any project specific keep options here:

# If your project uses WebView with JS, uncomment the following
# and specify the fully qualified class name to the JavaScript interface
# class:
#-keepclassmembers class fqcn.of.javascript.interface.for.webview {
#   public *;
#}
-dontwarn com.fusionnext.cloud.**
-keep class com.fusionnext.cloud.** { public *; }
-keep interface com.fusionnext.cloud.** {public  *; }
-keep enum com.fusionnext.cloud.** {public *;}

-dontwarn com.fusionnextinc.doorbell.widget.FNAlert.**
-keep class com.fusionnextinc.doorbell.widget.FNAlert.** { public *; }
-keep interface com.fusionnextinc.doorbell.widget.FNAlert.** {public  *; }
-keep enum com.fusionnextinc.doorbell.widget.FNAlert.** {public *;}

-dontwarn org.eclipse.paho.client.mqttv3.**
-keep class org.eclipse.paho.client.mqttv3.** { public *; }
-keep interface org.eclipse.paho.client.mqttv3.** {public  *; }
-keep enum org.eclipse.paho.client.mqttv3.** {public *;}

-dontwarn com.iptnet.**
-keep class com.iptnet.** { *; }
-keep interface com.iptnet.** {*; }
-keep enum com.iptnet.** {*;}

-dontwarn com.github.erdalceylan.**
-keep class com.github.erdalceylan.** { *; }
-keep interface com.github.erdalceylan.** {*; }
-keep enum com.github.erdalceylan.** {*;}

-keepclasseswithmembernames class * {
    native <methods>;
}

-keep class com.fusionnextinc.doorbell.DefaultAppConfig {public *;}