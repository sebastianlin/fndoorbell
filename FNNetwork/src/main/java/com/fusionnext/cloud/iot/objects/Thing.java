package com.fusionnext.cloud.iot.objects;

import android.util.Log;

import com.fusionnext.cloud.datalink.json.FNJsonArray;
import com.fusionnext.cloud.datalink.json.FNJsonObject;
import com.fusionnext.cloud.utils.Times;

import org.json.JSONException;

import java.util.ArrayList;
import java.util.TimeZone;


/**
 * Created by Mike Chang on 2016/5/4
 */
public class Thing {
    protected final static String _TAG = "Thing";

    public Thing() {
        _thingId = "";
        _productId = "";
        _productSlug = "";
        _identifier = "";
        _name = "";
        _firmwareVer = "";
        _batteryLevel = "";
        _ringtone = "";
        _inviteesCount = 0;
        _createdAt = "";
        _updatedAt = "";
        _userId = "";
        _organizationId = "";
        _userName = "";
        _userEmail = "";
        _lastestEventTime = "";
    }

    public String _thingId;
    public String _productId;
    public String _productSlug;
    public String _identifier;
    public String _name;
    public String _firmwareVer;
    public String _batteryLevel;
    public String _ringtone;
    public int _inviteesCount;
    public String _createdAt;
    public String _updatedAt;
    public String _userId;
    public String _organizationId;
    public String _userName;
    public String _userEmail;
    public String _lastestEventTime;
    public int _switched;
    public THINGSTATUS _online;

    public void clean() {
        _thingId = "";
        _productId = "";
        _productSlug = "";
        _identifier = "";
        _name = "";
        _firmwareVer = "";
        _batteryLevel = "";
        _ringtone = "";
        _inviteesCount = 0;
        _createdAt = "";
        _updatedAt = "";
        _userId = "";
        _organizationId = "";
        _userName = "";
        _userEmail = "";
        _lastestEventTime = "";
        _switched = 0;
        _online = THINGSTATUS.OFFLINE;
    }

    public boolean getDataFromJSONObject(FNJsonObject thingObj) {
        clean();
        if (thingObj == null) {
            Log.w(_TAG, " No JSON information");
            return false;
        }
        try {
            _thingId = thingObj.getStringDefault("id");
            _productId = thingObj.getStringDefault("product_id");
            _productSlug = thingObj.getStringDefault("product_slug");
            _identifier = thingObj.getStringDefault("identifier");
            _name = thingObj.getStringDefault("name");
            _createdAt = thingObj.getStringDefault("created_at");
            _createdAt = Times.converTime(_createdAt, TimeZone.getDefault());
            _updatedAt = thingObj.getStringDefault("updated_at");
            _updatedAt = Times.converTime(_updatedAt, TimeZone.getDefault());
            _online = THINGSTATUS.setScore( Integer.valueOf(thingObj.getStringDefault("online")).intValue());


            FNJsonObject ownersObj = thingObj.getJSONObject("owners");
            parseThingOwners(ownersObj);
            FNJsonObject statesObj = thingObj.getJSONObject("last_unique_states");
            parseThingStates(statesObj);
            FNJsonObject settingsObj = thingObj.getJSONObject("settings");
            parseThingSettings(settingsObj);
            FNJsonObject inviteesObj = thingObj.getJSONObject("invitees");
            parseThingInvitees(inviteesObj);
            FNJsonObject eventsObj = thingObj.getJSONObject("events");
            parseThingEvents(eventsObj);
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
        return true;
    }

    public static Thing parseThing(String json) {
        try {
            FNJsonObject thingObject = new FNJsonObject(json);

            Thing thing = new Thing();
            thing.getDataFromJSONObject(thingObject);
            return thing;
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static ArrayList<Thing> parseThingArray(String json) {
        try {
            ArrayList<Thing> thingList = new ArrayList<Thing>();
            FNJsonObject thingObject = new FNJsonObject(json);
            FNJsonArray thingsArray = null;

            if (thingObject.has("items")) {
                thingsArray = thingObject.getJSONArray("items");
            } else if (thingObject.has("data")) {
                thingsArray = thingObject.getJSONArray("data");
            }
            if (thingsArray == null) {
                return null;
            }
            for (int i = 0; i < thingsArray.length(); i++) {
                FNJsonObject object = thingsArray.getJSONObject(i);
                Thing thing = new Thing();
                thing.getDataFromJSONObject(object);
                if (thing == null) return null;
                thingList.add(thing);
            }
            return thingList;
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return null;
    }

    private void parseThingOwners(FNJsonObject ownersObj) {
        if (ownersObj != null) {
            FNJsonArray dataArray = null;
            try {
                dataArray = ownersObj.getJSONArray("items");
                if (dataArray != null && dataArray.length() > 0) {
                    FNJsonObject dataObj = dataArray.getJSONObject(0);
                    _userId = dataObj.getStringDefault("id");
                    _organizationId = dataObj.getStringDefault("organization_id");
                    _userName = dataObj.getStringDefault("name");
                    _userEmail = dataObj.getStringDefault("email");
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

    private void parseThingStates(FNJsonObject statesObj) {
        if (statesObj != null) {
            FNJsonArray dataArray = null;
            try {
                dataArray = statesObj.getJSONArray("items");
                if (dataArray != null && dataArray.length() > 0) {
                    for (int i = 0; i < dataArray.length(); i++) {
                        FNJsonObject dataObj = dataArray.getJSONObject(i);
                        if (dataObj != null) {
                            String stateName = dataObj.getStringDefault("name");
                            if (stateName.equals("battery_level")) {
                                String battery = dataObj.getStringDefault("value");
                                if (battery != null) {
                                    _batteryLevel = battery;
                                }
                            } else if (stateName.equals("firmware_ver")) {
                                String fwVersion = dataObj.getStringDefault("value");
                                if (fwVersion != null) {
                                    _firmwareVer = fwVersion;
                                }
                            } else if (stateName.equals("on")) {
                                _switched = dataObj.getIntDefault("value", 0);
                            }

                        }
                    }
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

    private void parseThingSettings(FNJsonObject settingsObj) {
        if (settingsObj != null) {
            FNJsonArray dataArray = null;
            try {
                dataArray = settingsObj.getJSONArray("items");
                if (dataArray != null && dataArray.length() > 0) {
                    for (int i = 0; i < dataArray.length(); i++) {
                        FNJsonObject dataObj = dataArray.getJSONObject(i);
                        if (dataObj != null) {
                            String stateName = dataObj.getStringDefault("name");
                            if (stateName.equals("ringtone")) {
                                String sound = dataObj.getStringDefault("value");
                                if (sound != null) {
                                    try {
                                        FNJsonObject ringJsonObj = new FNJsonObject(sound);
                                        _ringtone = ringJsonObj.getString("ringtone");
                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                    }
                                }
                            }
                        }
                    }
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

    private void parseThingInvitees(FNJsonObject inviteesObj) {
        if (inviteesObj != null) {
            FNJsonArray dataArray = null;
            try {
                dataArray = inviteesObj.getJSONArray("items");
                if (dataArray != null && dataArray.length() > 0) {
                    _inviteesCount = dataArray.length();
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

    private void parseThingEvents(FNJsonObject inviteesObj) {
        if (inviteesObj != null) {
            FNJsonArray dataArray = null;
            try {
                dataArray = inviteesObj.getJSONArray("items");
                if (dataArray != null && dataArray.length() > 0) {
                    FNJsonObject dataObj = dataArray.getJSONObject(0);
                    _lastestEventTime = dataObj.getStringDefault("created_at");
                    _lastestEventTime = Times.converTime(_lastestEventTime, TimeZone.getDefault());
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }
}
