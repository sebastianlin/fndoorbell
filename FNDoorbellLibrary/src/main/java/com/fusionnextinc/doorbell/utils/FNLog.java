package com.fusionnextinc.doorbell.utils;

import android.content.Context;
import android.os.Environment;
import android.util.Log;
import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by choco on 2014/12/5.
 */
public class FNLog {
    private static final String TAG = "Fusionnext";

    public static boolean SHOW_LOG = true;

    private static Thread logThread;

    public static void i(String tag, String msg) {
        if (SHOW_LOG)
            Log.i(TAG, tag + ": " + msg);
    }

    public static void e(String tag, String msg) {
        if (SHOW_LOG)
            Log.e(TAG, tag + ": " + msg);
    }

    public static void d(String tag, String msg) {
        if (SHOW_LOG)
            Log.d(TAG, tag + ": " + msg);
    }

    public static void w(String tag, String msg) {
        if (SHOW_LOG)
            Log.w(TAG, tag + ": " + msg);
    }

    public static void startWriteLog(Context context) {
        File logFolder = new File(Environment.getExternalStorageDirectory(), "FNLog");
        if (!logFolder.exists() && !logFolder.mkdirs()) {
            return;
        }
        final File logFile = new File(logFolder, context.getPackageName() + "_" + new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date()));
        if (logThread != null) {
            logThread.interrupt();
        }
        logThread = new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    Runtime.getRuntime().exec("logcat -c");
                    Runtime.getRuntime().exec("logcat -f " + logFile.getAbsolutePath());
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        });
        logThread.start();
    }
}
