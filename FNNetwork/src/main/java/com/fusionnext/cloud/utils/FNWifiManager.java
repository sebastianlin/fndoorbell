package com.fusionnext.cloud.utils;

import android.content.Context;
import android.net.DhcpInfo;
import android.net.wifi.ScanResult;
import android.net.wifi.WifiConfiguration;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.os.Build;
import android.os.Looper;
import androidx.annotation.Nullable;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.net.InetAddress;
import java.net.NetworkInterface;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;

/**
 * Created by chocoyang on 2016/3/15.
 */
public class FNWifiManager {
    private static final String TAG = "FNWifiManager";

    public static final int SECURITY_NONE = 0;
    public static final int SECURITY_WPA_PSK = 1;
    public static final int SECURITY_WPA_EAP = 2;
    public static final int SECURITY_WEP = 3;
    public static final int SECURITY_WPA2_PSK = 4;

    public static final int WIFI_STATE_DISABLING = WifiManager.WIFI_STATE_DISABLING;
    public static final int WIFI_STATE_DISABLED = WifiManager.WIFI_STATE_DISABLED;
    public static final int WIFI_STATE_ENABLING = WifiManager.WIFI_STATE_ENABLING;
    public static final int WIFI_STATE_ENABLED = WifiManager.WIFI_STATE_ENABLED;
    public static final int WIFI_STATE_UNKNOWN = WifiManager.WIFI_STATE_UNKNOWN;

    public static final int WIFI_AP_STATE_DISABLING = 10;
    public static final int WIFI_AP_STATE_DISABLED = 11;
    public static final int WIFI_AP_STATE_ENABLING = 12;
    public static final int WIFI_AP_STATE_ENABLED = 13;
    public static final int WIFI_AP_STATE_FAILED = 14;

    private static FNWifiManager fnWifiManager;

    private Context context;
    private WifiManager wifiManager;

    public static FNWifiManager getInstance(Context context) {
        if (context == null)
            return null;
        synchronized (FNWifiManager.class) {
            if (fnWifiManager == null)
                fnWifiManager = new FNWifiManager(context.getApplicationContext());
        }
        return fnWifiManager;
    }

    private FNWifiManager(Context context) {
        this.context = context;
        wifiManager = (WifiManager) context.getSystemService(Context.WIFI_SERVICE);
    }

    public WifiManager getWifiManager() {
        return wifiManager;
    }

    public boolean isWifiEnabled() {
        return wifiManager.isWifiEnabled();
    }

    public boolean setWifiEnabled(boolean enabled) {
        return wifiManager.setWifiEnabled(enabled);
    }

    @Nullable
    public String getGatewaySSID() {
        WifiInfo wifiInfo = wifiManager.getConnectionInfo();
        String ssid;
        if (wifiInfo != null && (ssid = wifiInfo.getSSID()) != null) {
            ssid = ssid.replace("\"", "");
            return ssid.equalsIgnoreCase("<unknown ssid>") || ssid.equalsIgnoreCase("0x") ? null : ssid;
        } else {
            return null;
        }
    }

    @Nullable
    public String getGatewayIp() {
        DhcpInfo dhcpInfo = wifiManager.getDhcpInfo();
        if (dhcpInfo != null) {
            if (dhcpInfo.gateway != 0) {
                return formatIP(dhcpInfo.gateway);
            } else if (dhcpInfo.ipAddress != 0 && dhcpInfo.serverAddress != 0) {
                return formatIP(dhcpInfo.serverAddress);
            }
        }
        return null;
    }

    @Nullable
    public String getGatewayMac() {
        WifiInfo wifiInfo = wifiManager.getConnectionInfo();
        if (wifiInfo != null) {
            String mac = wifiInfo.getBSSID();
            return mac != null ? mac.toUpperCase() : null;
        } else {
            return null;
        }
    }

    @Nullable
    public String getIp() {
        WifiInfo wifiInfo = wifiManager.getConnectionInfo();
        return wifiInfo == null || wifiInfo.getIpAddress() == 0 ? null : formatIP(wifiInfo.getIpAddress());
    }

    @Nullable
    public String getMac() {
        WifiInfo wifiInfo = wifiManager.getConnectionInfo();
        if (wifiInfo != null) {
            String mac = wifiInfo.getMacAddress();
            return mac != null ? mac.toUpperCase() : null;
        } else {
            return null;
        }
    }

    public List<ScanResult> getScanResults() {
        List<ScanResult> scanResults = wifiManager.getScanResults();
        return scanResults != null ? scanResults : new ArrayList<ScanResult>();
    }

    public List<WifiConfiguration> getConfiguredNetworks() {
        List<WifiConfiguration> configuredNetworks = wifiManager.getConfiguredNetworks();
        return configuredNetworks != null ? configuredNetworks : new ArrayList<WifiConfiguration>();
    }

    public boolean startScan() {
        return wifiManager.startScan();
    }

    private String formatIP(int address) {
        return String.format(Locale.ENGLISH, "%d.%d.%d.%d", (address & 0xff), (address >> 8 & 0xff), (address >> 16 & 0xff), (address >> 24 & 0xff));
    }

    public int getSecurity(ScanResult scanResult) {
        if (scanResult.capabilities.contains("WEP")) {
            return SECURITY_WEP;
        } else if (scanResult.capabilities.contains("PSK")) {
            return SECURITY_WPA_PSK;
        } else if (scanResult.capabilities.contains("EAP")) {
            return SECURITY_WPA_EAP;
        }
        return SECURITY_NONE;
    }

    public int getSecurity(WifiConfiguration config) {
        if (config.allowedKeyManagement.get(WifiConfiguration.KeyMgmt.WPA_PSK))
            return SECURITY_WPA_PSK;
        if (config.allowedKeyManagement.get(WifiConfiguration.KeyMgmt.WPA_EAP) || config.allowedKeyManagement.get(WifiConfiguration.KeyMgmt.IEEE8021X))
            return SECURITY_WPA_EAP;
        return (config.wepKeys[0] != null) ? SECURITY_WEP : SECURITY_NONE;
    }

    public int addNetWork(String ssid, String password, int security) {
        List<WifiConfiguration> configurationList = wifiManager.getConfiguredNetworks();
        if (configurationList != null) {
            for (WifiConfiguration wifiConfiguration : configurationList) {
                if (wifiConfiguration.SSID.equals("\"" + ssid + "\"")) {
                    if (!wifiManager.removeNetwork(wifiConfiguration.networkId)) {
                        return -1;
                    }
                    wifiManager.saveConfiguration();
                }
            }
        }
        WifiConfiguration config = new WifiConfiguration();
        config.SSID = "\"" + ssid + "\"";
        config.status = WifiConfiguration.Status.ENABLED;
        config.allowedPairwiseCiphers.set(WifiConfiguration.PairwiseCipher.TKIP);
        config.allowedPairwiseCiphers.set(WifiConfiguration.PairwiseCipher.CCMP);
        config.allowedProtocols.set(WifiConfiguration.Protocol.RSN);
        config.allowedProtocols.set(WifiConfiguration.Protocol.WPA);
        if (security == SECURITY_NONE) {
            config.allowedKeyManagement.set(WifiConfiguration.KeyMgmt.NONE);
            config.allowedGroupCiphers.set(WifiConfiguration.GroupCipher.WEP40);
            config.allowedGroupCiphers.set(WifiConfiguration.GroupCipher.WEP104);
            config.allowedGroupCiphers.set(WifiConfiguration.GroupCipher.CCMP);
            config.allowedGroupCiphers.set(WifiConfiguration.GroupCipher.TKIP);
        } else if (security == SECURITY_WEP) {
            config.wepKeys[0] = "\"" + password + "\"";
            config.wepTxKeyIndex = 0;
            config.allowedAuthAlgorithms.set(WifiConfiguration.AuthAlgorithm.SHARED);
            config.allowedKeyManagement.set(WifiConfiguration.KeyMgmt.NONE);
            config.allowedGroupCiphers.set(WifiConfiguration.GroupCipher.WEP40);
            config.allowedGroupCiphers.set(WifiConfiguration.GroupCipher.WEP104);
        } else if (security == SECURITY_WPA_EAP) {
            config.preSharedKey = "\"" + password + "\"";
            config.allowedKeyManagement.set(WifiConfiguration.KeyMgmt.WPA_EAP);
            config.allowedGroupCiphers.set(WifiConfiguration.GroupCipher.CCMP);
            config.allowedGroupCiphers.set(WifiConfiguration.GroupCipher.TKIP);
        } else if (security == SECURITY_WPA_PSK) {
            config.preSharedKey = "\"" + password + "\"";
            config.allowedAuthAlgorithms.set(WifiConfiguration.AuthAlgorithm.OPEN);
            config.allowedKeyManagement.set(WifiConfiguration.KeyMgmt.WPA_PSK);
            config.allowedGroupCiphers.set(WifiConfiguration.GroupCipher.WEP40);
            config.allowedGroupCiphers.set(WifiConfiguration.GroupCipher.WEP104);
            config.allowedGroupCiphers.set(WifiConfiguration.GroupCipher.CCMP);
            config.allowedGroupCiphers.set(WifiConfiguration.GroupCipher.TKIP);
        } else {
            return -1;
        }
        int networkId = wifiManager.addNetwork(config);
        wifiManager.saveConfiguration();
        return networkId;
    }

    public void connect(int networkId) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
            try {
                Class ActionListener = Class.forName("android.net.wifi.WifiManager$ActionListener");
                Method connect = wifiManager.getClass().getDeclaredMethod("connect", int.class, ActionListener);
                connect.invoke(wifiManager, networkId, null);
            } catch (ClassNotFoundException e) {
                e.printStackTrace();
            } catch (NoSuchMethodException e) {
                e.printStackTrace();
            } catch (InvocationTargetException e) {
                e.printStackTrace();
            } catch (IllegalAccessException e) {
                e.printStackTrace();
            }
        } else if (Build.VERSION.SDK_INT == Build.VERSION_CODES.JELLY_BEAN) {
            try {
                Class ChannelListener = Class.forName("android.net.wifi.WifiManager$ChannelListener");
                Method initialize = wifiManager.getClass().getDeclaredMethod("initialize", Context.class, Looper.class, ChannelListener);
                Object channel = initialize.invoke(wifiManager, context, context.getMainLooper(), null);
                Class ActionListener = Class.forName("android.net.wifi.WifiManager$ActionListener");
                Method connect = wifiManager.getClass().getDeclaredMethod("connect", channel.getClass(), int.class, ActionListener);
                connect.invoke(wifiManager, channel, networkId, null);
            } catch (ClassNotFoundException e) {
                e.printStackTrace();
            } catch (NoSuchMethodException e) {
                e.printStackTrace();
            } catch (InvocationTargetException e) {
                e.printStackTrace();
            } catch (IllegalAccessException e) {
                e.printStackTrace();
            }
        } else if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.ICE_CREAM_SANDWICH) {
            try {
                Method connect = wifiManager.getClass().getDeclaredMethod("connectNetwork", int.class);
                connect.invoke(wifiManager, networkId);
            } catch (NoSuchMethodException e) {
                e.printStackTrace();
            } catch (InvocationTargetException e) {
                e.printStackTrace();
            } catch (IllegalAccessException e) {
                e.printStackTrace();
            }
        } else {
            try {
                Method reconnect = wifiManager.getClass().getDeclaredMethod("reconnect");
                reconnect.invoke(wifiManager);
            } catch (NoSuchMethodException e) {
                e.printStackTrace();
            } catch (InvocationTargetException e) {
                e.printStackTrace();
            } catch (IllegalAccessException e) {
                e.printStackTrace();
            }
        }
    }

    @Nullable
    public WifiConfiguration getWifiApConfiguration() {
        try {
            Method method = wifiManager.getClass().getMethod("getWifiApConfiguration");
            return (WifiConfiguration) method.invoke(wifiManager);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public boolean setWifiApConfiguration(WifiConfiguration config) {
        try {
            Method method = wifiManager.getClass().getMethod("setWifiApConfiguration", WifiConfiguration.class);
            return (Boolean) method.invoke(wifiManager, config);
        } catch (Exception e) {
            return false;
        }
    }

    public boolean isWifiApEnabled() {
        try {
            Method method = wifiManager.getClass().getMethod("isWifiApEnabled");
            return (Boolean) method.invoke(wifiManager);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }

    public int getWifiApState() {
        try {
            Method method = wifiManager.getClass().getMethod("getWifiApState");
            return (Integer) method.invoke(wifiManager);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return WIFI_AP_STATE_FAILED;
    }

    public boolean setWifiApEnabled(WifiConfiguration config, boolean enable) {
        try {
            Method method = wifiManager.getClass().getMethod("setWifiApEnabled", WifiConfiguration.class, Boolean.TYPE);
            if (enable && wifiManager.isWifiEnabled())
                wifiManager.setWifiEnabled(false);
            return (boolean) method.invoke(wifiManager, enable ? config : null, enable);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }

    public boolean setWifiApEnabled(String SSID, String preSharedKey, int securityType, boolean enable) {
        WifiConfiguration config = getWifiApConfiguration();
        if (config == null) {
            config = new WifiConfiguration();
        }
        switch (securityType) {
            case SECURITY_NONE:
                if (SSID == null)
                    return false;
                config.SSID = SSID;
                config.preSharedKey = null;
                config.wepKeys = new String[4];
                config.hiddenSSID = false;
                config.allowedKeyManagement.clear();
                config.allowedKeyManagement.set(WifiConfiguration.KeyMgmt.NONE, true);
                config.allowedProtocols.clear();
                config.allowedAuthAlgorithms.clear();
                config.allowedPairwiseCiphers.clear();
                config.allowedGroupCiphers.clear();
                break;
            case SECURITY_WPA_PSK:
                if (SSID == null || preSharedKey == null)
                    return false;
                config.SSID = SSID;
                config.preSharedKey = preSharedKey;
                config.wepKeys = new String[4];
                config.hiddenSSID = false;
                config.allowedKeyManagement.clear();
                config.allowedKeyManagement.set(WifiConfiguration.KeyMgmt.NONE, false);
                config.allowedKeyManagement.set(WifiConfiguration.KeyMgmt.WPA_PSK, true);
                config.allowedProtocols.clear();
                config.allowedAuthAlgorithms.clear();
                config.allowedAuthAlgorithms.set(WifiConfiguration.AuthAlgorithm.OPEN, true);
                config.allowedPairwiseCiphers.clear();
                config.allowedGroupCiphers.clear();
                break;
            case SECURITY_WPA2_PSK:
                if (SSID == null || preSharedKey == null)
                    return false;
                config.SSID = SSID;
                config.preSharedKey = preSharedKey;
                config.wepKeys = new String[4];
                config.hiddenSSID = false;
                config.allowedKeyManagement.clear();
                config.allowedKeyManagement.set(WifiConfiguration.KeyMgmt.NONE, false);
                config.allowedKeyManagement.set(WifiConfiguration.KeyMgmt.WPA_PSK, false);
                config.allowedKeyManagement.set(WifiConfiguration.KeyMgmt.WPA_EAP, false);
                config.allowedKeyManagement.set(WifiConfiguration.KeyMgmt.IEEE8021X, false);
                config.allowedKeyManagement.set(4, true);  // WifiConfiguration.KeyMgmt.WPA2_PSK
                config.allowedProtocols.clear();
                config.allowedAuthAlgorithms.clear();
                config.allowedAuthAlgorithms.set(WifiConfiguration.AuthAlgorithm.OPEN, true);
                config.allowedPairwiseCiphers.clear();
                config.allowedGroupCiphers.clear();
                break;
            default:
                if (enable)
                    return false;
                break;
        }
        return setWifiApEnabled(enable ? config : null, enable);
    }

    @Nullable
    public String getWifiApGatewayIP() {
        if (isWifiApEnabled()) {
            try {
                for (Enumeration<NetworkInterface> networkInterface = NetworkInterface.getNetworkInterfaces(); networkInterface.hasMoreElements(); ) {
                    NetworkInterface singleInterface = networkInterface.nextElement();
                    String displayName = singleInterface.getDisplayName();
                    if (displayName.contains("wlan0") || displayName.contains("eth0") || displayName.contains("ap0")) {
                        for (Enumeration<InetAddress> IpAddresses = singleInterface.getInetAddresses(); IpAddresses.hasMoreElements(); ) {
                            InetAddress inetAddress = IpAddresses.nextElement();
                            if (!inetAddress.isLoopbackAddress() && !inetAddress.getHostName().contains(":"))
                                return inetAddress.getHostName();
                        }
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return null;
    }

    public HashMap<String, String> getWifiListFromArp(boolean onlyReachables, final int reachableTimeout) {
        final HashMap<String, String> list = new HashMap<String, String>();
        BufferedReader br;
        try {
            br = new BufferedReader(new FileReader("/proc/net/arp"));
            final FNWaitSignal wait = new FNWaitSignal();
            final ArrayList<String> waitList = new ArrayList<>();
            String line;
            while ((line = br.readLine()) != null) {
                final String[] split = line.split(" +");
                if (split.length > 3 && split[2].equals("0x2") && split[3].matches("..:..:..:..:..:..")) {
                    split[3] = split[3].toUpperCase();
                    if (onlyReachables) {
                        waitList.add(split[0]);
                        new Thread(new Runnable() {
                            @Override
                            public void run() {
                                try {
                                    if (InetAddress.getByName(split[0]).isReachable(reachableTimeout)) {
                                        list.put(split[0], split[3]);
                                    }
                                } catch (IOException e) {
                                    e.printStackTrace();
                                }
                                waitList.remove(split[0]);
                                if (waitList.size() == 0) {
                                    wait.notifySuccess();
                                }
                            }
                        }).start();
                    } else {
                        list.put(split[0], split[3]);
                    }
                }
            }
            br.close();
            if (waitList.size() > 0) {
                wait.waitNotify();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return list;
    }
}
