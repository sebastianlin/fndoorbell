package com.fusionnext.cloud.fusionapi;

import android.content.Context;

import com.fusionnext.cloud.datalink.http.HttpPost;
import com.fusionnext.cloud.datalink.json.FNJsonObject;
import com.fusionnext.cloud.utils.Utils;

import org.json.JSONException;

import java.util.HashMap;

/**
 * Created by Mike Chang on 2017/3/7
 */
public class Devices {
    protected final static String _TAG = "Devices";

    public static class Device {
        public String deviceId = "";
        public String clientId = "";
        public String userId = "";
        public String deviceIdentifer = "";
        public String deviceName = "";
        public String manufacturer = "";
        public String model = "";
        public String platform = "";
        public String platformVersion = "";
        public String pushToken = "";
        public String createAt = "";
        public String updatedAt = "";
        public String user = "";
    }

    private Context context;
    private Device device = new Device();

    public Devices(Context context) {
        this.context = context;
        setDevice(Public.getDataMgr(context).loadDevice());
    }

    public Device getDevice() {
        return device;
    }

    public void setDevice(Device newDevice) {
        if (device == null) device = new Device();
        if (device.deviceId != null) device.deviceId = newDevice.deviceId;
        if (device.clientId != null) device.clientId = newDevice.clientId;
        if (device.userId != null) device.userId = newDevice.userId;
        if (device.deviceIdentifer != null) device.deviceIdentifer = newDevice.deviceIdentifer;
        if (device.deviceName != null) device.deviceName = newDevice.deviceName;
        if (device.manufacturer != null) device.manufacturer = newDevice.manufacturer;
        if (device.model != null) device.model = newDevice.model;
        if (device.platform != null) device.platform = newDevice.platform;
        if (device.platformVersion != null) device.platformVersion = newDevice.platformVersion;
        if (device.pushToken != null) device.pushToken = newDevice.pushToken;
        if (device.createAt != null) device.createAt = newDevice.createAt;
        if (device.updatedAt != null) device.updatedAt = newDevice.updatedAt;
        if (device.user != null) device.user = newDevice.user;
    }

    public void createDevice(final FusionAPIHandler.OnActivateAPIListener onActivateAPIListener) {
        String macAddr = Utils.getMacAddress(context);
        String version = Utils.getAPIVerison();
        String deviceName = Utils.getDeviceName(context);
        String model = Utils.getModelName();
        String manufacturer = Utils.getManufacturer(context);

        HttpPost.PostParam[] params = new HttpPost.PostParam[1];
        params[0] = new HttpPost.PostParam();
        params[0].accessToken = Public.getAuthorization(context).getDeviceAccessToken();
        params[0].url = HttpData.generateHttpPostUrl(HttpData._POST_TYPE_CREATE_DEVICE);
        params[0].type = HttpData._POST_TYPE_CREATE_DEVICE;
        params[0].param = new HashMap<Object, Object>();

        params[0].param.put("identifier", macAddr);
        params[0].param.put("name", deviceName);
        params[0].param.put("manufacturer", manufacturer);
        params[0].param.put("model", model);
        params[0].param.put("platform", "android");
        params[0].param.put("platform_ver", version);

        HttpPost post = new HttpPost();
        post.setContext(context);
        post.setOnParseJsonListener(new HttpPost.OnParseJsonListener() {
            @Override
            public void onParse(boolean returnResult, int postType, String id, String returnStr) {
                if (returnStr != null) {
                    device = parseDevice(returnStr);
                }
                onActivateAPIListener.onCreateDevice(returnResult, device);
            }
        });
        post.execute(params);
    }

    /*public void updateDevice(Device device, final FusionAPIHandler.OnFusionAPIListener onFusionAPIListener) {
        if (device == null || device.deviceId == null) {
            return;
        }

        HttpPut.PutParam[] params = new HttpPut.PutParam[1];
        params[0] = new HttpPut.PutParam();
        params[0].accessToken = Public.getAuthorization(context).getDeviceAccessToken();
        params[0].url = HttpData.generateHttpPutUrl(HttpData._PUT_TYPE_UPDATE_DEVICE, device.deviceId);
        params[0].type = HttpData._PUT_TYPE_UPDATE_DEVICE;
        params[0].param = new HashMap<Object, Object>();

        if (device.deviceIdentifer != null) {
            params[0].param.put("identifier", device.deviceIdentifer);
        }
        if (device.deviceName != null) {
            params[0].param.put("name", device.deviceName);
        }
        if (device.platformVersion != null) {
            params[0].param.put("platform_ver", device.platformVersion);
        }
        if (device.pushToken != null) {
            params[0].param.put("push_token", device.pushToken);
        }

        HttpPut put = new HttpPut();
        put.setHttpRequestType("PUT");
        put.setOnParseJsonListener(new HttpPut.OnParseJsonListener() {
            @Override
            public void onParse(boolean returnResult, int putType, String returnStr) {
                onFusionAPIListener.onResult(returnResult, putType, returnStr);
            }
        });
        put.setContext(context);
        put.execute(params);
    }*/

    private Device parseDevice(String jsonStr) {
        if (jsonStr == null || jsonStr.equals("")) return null;

        try {
            FNJsonObject object = new FNJsonObject(jsonStr);
            device.deviceId = object.getStringDefault("id");
            device.clientId = object.getStringDefault("client_id");
            device.userId = object.getStringDefault("user_id");
            device.deviceIdentifer = object.getStringDefault("device_identifier");
            device.deviceName = object.getStringDefault("device_name");
            device.manufacturer = object.getStringDefault("manufacturer");
            device.userId = object.getStringDefault("user_id");
            device.platform = object.getStringDefault("platform");
            device.platformVersion = object.getStringDefault("platform_ver");
            device.pushToken = object.getStringDefault("push_token");
            device.createAt = object.getStringDefault("created_at");
            device.updatedAt = object.getStringDefault("updated_at");
            device.user = object.getStringDefault("user");
            Public.getDataMgr(context).saveDevice(device);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return device;
    }

}
